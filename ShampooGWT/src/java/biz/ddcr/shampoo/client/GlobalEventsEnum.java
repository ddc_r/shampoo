package biz.ddcr.shampoo.client;

public enum GlobalEventsEnum {

        //Bind the different parts of the UI via the viewport
        CHANGE_LOGINBOX("changeLoginBox"),
        CHANGE_MENUBOX("changeMenuBox"),
        CHANGE_HEADERBOX("changeHeaderBox"),
        CHANGE_FOOTERBOX("changeFooterBox"),
        CHANGE_NOTIFICATIONBOX("changeNotificationBox"),
        CLEAR_NOTIFICATIONBOX("clearNotificationBox"),
        //Bind a new webpage
        CHANGE_CONTENTBOX("changeContentBox"),
        SHOW_POPUP("showPopup"),
        CLOSE_POPUP("closePopup"),
        SHOW_FLOATINGBOX("showFloatingBox"),
        CLOSE_FLOATINGBOX("closeFloatingBox"),
        //Send a message to the currently active webpage
        MESSAGES_TO_CONTENTBOX("messagesToContentBox"),
        
        START_REVERSEAJAX("startReverseAjaxListener"),
        STOP_REVERSEAJAX("stopReverseAjaxListener"),

        //UI initialization
	SHOW_FOOTER("showFooter"),
        SHOW_HEADER("showHeader"),
        SHOW_MENU("showMenu"),
        SHOW_NOTIFICATION("showNotification"),
        SHOW_LOGGED_IN("showLoggedIn"),
        SHOW_LOGGED_OUT("showLoggedOut"),
        SHOW_RADIOPLAYER("showRadioPlayer"),
        CLOSE_RADIOPLAYER("closeRadioPlayer"),

        //Message notification support
        DISPLAY_ERROR("displayError"),
        DISPLAY_WARNING("displayWarning"),
        DISPLAY_INFO("displayInfo"),
        //Show or hide a global "loading..." notification
        SHOW_HOURGLASS("showHourglass"),
        HIDE_HOURGLASS("hideHourglass");
		
	private String eventType = null;
	
	private GlobalEventsEnum(String eventType){
		this.eventType = eventType;
	}
	
	@Override
	public String toString(){
		return eventType;
	}
}
