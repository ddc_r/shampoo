package biz.ddcr.shampoo.client;

public enum InterWindowEventsEnum {

        //Send events to targeted presenters
        //Pending track reviews
        SHOW_REVIEW_EDITOR("showReviewEditor"),
        SEND_REVIEW("sendReview"),
        SHOW_BATCH_REVIEW_EDITOR("showBatchReviewEditor"),
        SEND_BATCH_REVIEW("sendBatchReview"),
        SHOW_REQUEST_EDITOR("showRequestEditor"),
        SEND_REQUEST("sendRequest"),
        //Single track added to a playlist entry
        SHOW_PLAYLIST_SINGLEBROADCASTTRACKSELECT("showPlaylistSingleBroadcastTrackSelect"),
        SEND_ADD_PLAYLIST_SINGLEBROADCASTTRACKSELECT("sendAddPlaylistSingleBroadcastTrack"),
        SEND_EDIT_PLAYLIST_SINGLEBROADCASTTRACKSELECT("sendEditPlaylistSingleBroadcastTrack"),
        SHOW_PLAYLIST_FILTERS_EDITOR("showPlaylistFiltersEditor"),
        SEND_ADD_PLAYLIST_FILTERS("sendAddPlaylistFilters"),
        SEND_EDIT_PLAYLIST_FILTERS("sendEditPlaylistFilters");
		
	private String eventType = null;
	
	private InterWindowEventsEnum(String eventType){
		this.eventType = eventType;
	}
	
	@Override
	public String toString(){
		return eventType;
	}
}
