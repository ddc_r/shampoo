package biz.ddcr.shampoo.client;

import java.io.Serializable;

public enum MessageEventsEnum implements Serializable {

    //Refresh bits of the UI
    REFRESH_MYPROFILE("refreshMyProfile"),
    REFRESH_USERS("refreshUsers"),
    REFRESH_CHANNELS("refreshChannels"),
    REFRESH_STREAMERS("refreshStreamers"),
    REFRESH_PROGRAMMES("refreshProgrammes"),
    REFRESH_TIMETABLES("refreshTimetables"),
    REFRESH_PLAYLISTS("refreshPlaylists"),
    REFRESH_BROADCASTTRACKS("refreshBroadcastTracks"),
    REFRESH_PENDINGTRACKS("refreshPendingTracks"),
    REFRESH_LOGS("refreshLogs"),
    REFRESH_NOTIFICATIONS("refreshNotifications"),
    REFRESH_ARCHIVES("refreshArchives"),
    REFRESH_REPORTS("refreshReports"),
    REFRESH_QUEUES("refreshQueues"),
    REFRESH_WEBSERVICES("refreshWebservices");
    
    private String eventType = null;

    private MessageEventsEnum(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return eventType;
    }
}
