package biz.ddcr.shampoo.client;

public enum RedirectionEventsEnum {

    //Navigation
    SHOW_INDEX("showIndex", "index"),
    //User
    SHOW_USERLIST("showUserList", "users"),
    SHOW_RESTRICTEDUSERADD("showRestrictedUserAdd", null),
    SHOW_RESTRICTEDUSEREDIT("showRestrictedUserEdit", null),
    SHOW_RESTRICTEDUSERDISPLAY("showRestrictedUserDisplay", null),
    SHOW_ADMINADD("showAdminAdd", null),
    SHOW_ADMINEDIT("showAdminEdit", null),
    SHOW_ADMINDISPLAY("showAdminDisplay", null),
    SHOW_CHANGEMYPROFILE("showChangeMyProfile", null),
    SHOW_CREATEMYPROFILE("showCreateMyProfile", null),
    SHOW_RETRIEVEMYPASSWORD("showForgotMyPassword", null),
    SHOW_NOTIFICATIONLIST("showNotificationList", "notifications"),
    //Channel
    SHOW_CHANNELLIST("showChannelList", "channels"),
    SHOW_CHANNELADD("showChannelAdd", null),
    SHOW_CHANNELEDIT("showChannelEdit", null),
    SHOW_CHANNELDISPLAY("showChannelDisplay", null),
    //Programme
    SHOW_PROGRAMMELIST("showProgrammeList", "programmes"),
    SHOW_PROGRAMMEADD("showProgrammeAdd", null),
    SHOW_PROGRAMMEEDIT("showProgrammeEdit", null),
    SHOW_PROGRAMMEDISPLAY("showProgrammeDisplay", null),
    //Timetables
    SHOW_TIMETABLELIST("showTimetableList", "timetables"),
    SHOW_TIMETABLEADD("showTimetableAdd", null),
    SHOW_TIMETABLEMOVE("showTimetableMove", null),
    SHOW_TIMETABLERESIZE("showTimetableResize", null),
    SHOW_TIMETABLEDECOMISSIONING("showTimetableDecommissioning", null),
    SHOW_TIMETABLECLONE("showTimetableClone", null),
    SHOW_TIMETABLEPLAYLISTSELECT("showTimetablePlaylistSelect", null),
    SHOW_TIMETABLEDISPLAY("showTimetableDisplay", null),
    //Playlists
    SHOW_PLAYLISTLIST("showPlaylistList", "playlists"),
    SHOW_PLAYLISTADD("showPlaylistAdd", null),
    SHOW_PLAYLISTEDIT("showPlaylistEdit", null),
    SHOW_PLAYLISTDISPLAY("showPlaylistDisplay", null),
    //Tracks
    SHOW_TRACKLIST("showTrackList", "tracks"),
    SHOW_BROADCASTTRACKADD("showBroadcastTrackAdd", null),
    SHOW_PENDINGTRACKADD("showPendingTrackAdd", null),
    SHOW_BROADCASTTRACKEDIT("showBroadcastTrackEdit", null),
    SHOW_BROADCASTTRACKBULKEDIT("showBroadcastTrackBulkEdit", null),
    SHOW_PENDINGTRACKEDIT("showPendingTrackEdit", null),
    SHOW_BROADCASTTRACKDISPLAY("showBroadcastTrackDisplay", null),
    SHOW_PENDINGTRACKDISPLAY("showPendingTrackDisplay", null),
    SHOW_PENDINGTRACKBULKEDIT("showPendingTrackBulkEdit", null),
    //Journals
    SHOW_LOGLIST("showLogList", "logs"),
    //Archives
    SHOW_ARCHIVELIST("showArchiveList", "archives"),
    SHOW_REPORTADD("showReportAdd", null),
    //Queues
    SHOW_QUEUELIST("showQueueList", "queues"),
    //Webservices
    //Playlists
    SHOW_WEBSERVICELIST("showWebserviceList", "webservices"),
    SHOW_WEBSERVICEADD("showWebserviceAdd", null),
    SHOW_WEBSERVICEEDIT("showWebserviceEdit", null),
    SHOW_WEBSERVICEDISPLAY("showWebserviceDisplay", null);

    private String eventType = null;
    private String historyURLPart = null;

    private RedirectionEventsEnum(String eventType, String historyURLPart) {
        this.eventType = eventType;
        this.historyURLPart = historyURLPart;
    }

    public String getEventType() {
        return eventType;
    }

    public String getHistoryURLPart() {
        return historyURLPart;
    }

    @Override
    public String toString() {
        return eventType;
    }

    public String toFullString() {
        return eventType + "#" + historyURLPart;
    }
}
