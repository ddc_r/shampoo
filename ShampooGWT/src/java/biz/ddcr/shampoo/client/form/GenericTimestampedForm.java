/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form;

import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinute;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;

/**
 *
 * The basic and generic class inherited by all View forms
 *
 * @author okay_awright
 **/
public abstract class GenericTimestampedForm implements GenericTimestampedFormInterface {

    private String creatorId;
    private String editorId;
    private JSYearMonthDayHourMinute latestModificationDate;
    private JSYearMonthDayHourMinute creationDate;

    @Override
    public void setLatestModificationDate(YearMonthDayHourMinuteInterface latestModificationDate) {
        if (this.latestModificationDate==null)
            this.latestModificationDate = new JSYearMonthDayHourMinute(latestModificationDate);
    }

    @Override
    public void setCreationDate(YearMonthDayHourMinuteInterface creationDate) {
        if (this.creationDate==null)
            this.creationDate = new JSYearMonthDayHourMinute(creationDate);
    }

    @Override
    public YearMonthDayHourMinuteInterface getLatestModificationDate() {
        return latestModificationDate;
    }

    @Override
    public YearMonthDayHourMinuteInterface getCreationDate() {
        return creationDate;
    }

    //Do only update the creator if none is already linked to
    @Override
    public void setCreatorId(String creatorId) {
        if (this.creatorId==null)
            this.creatorId = creatorId;
    }

    @Override
    public String getCreatorId() {
        return creatorId;
    }

    @Override
    public void setLatestEditorId(String editorId) {
        if (this.editorId==null)
            this.editorId = editorId;
    }

    @Override
    public String getLatestEditorId() {
        return editorId;
    }

}
