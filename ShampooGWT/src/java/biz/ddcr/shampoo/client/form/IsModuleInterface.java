/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form;

/**
 *
 * A module is an object, not a primitive type, that is part of a form
 *
 * @author okay_awright
 **/
public interface IsModuleInterface/*<T>*/ extends Comparable<IsModuleInterface> {

    /**
     * Check if two ModuleInterfaces possess the exact same working content, business keys and primary keys are irrelevant here, only user-related data is verified.
     * This is not equivalent to equals()
     * @param otherForm
     * @return
     */
    //public boolean isContentEqual(T otherForm);

}
