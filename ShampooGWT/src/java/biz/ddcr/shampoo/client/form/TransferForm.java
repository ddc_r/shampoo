/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form;

/**
 *
 * Transfer object used for trans-GWT windows communication.
 * A window X asks a window Y to edit a specific form via Y's constructor, the form is then sent back to X via an MVP4G event.
 * It behaves like a regular GenericTimestampedForm but adds a location info so as to track down where the call came from once passed back (eg. which row of a specific grid in X)
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TransferForm<E extends Comparable, F extends GenericFormInterface> implements GenericFormInterface {

    private E location;
    private F form;

    public TransferForm(E location, F form) {
        this.location = location;
        this.form = form;
    }

    public F getForm() {
        return form;
    }

    public void setForm(F form) {
        this.form = form;
    }

    public E getLocation() {
        return location;
    }

    public void setLocation(E location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransferForm<E, F> other = (TransferForm<E, F>) obj;
        if (this.location != other.location && (this.location == null || !this.location.equals(other.location))) {
            return false;
        }
        if (this.form != other.form && (this.form == null || !this.form.equals(other.form))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.location != null ? this.location.hashCode() : 0);
        hash = 83 * hash + (this.form != null ? this.form.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof TransferForm) {
            TransferForm anotherTransferForm = (TransferForm)o;
            //Sort by location, then by form
            int diff = this.location!=null ? this.location.compareTo(anotherTransferForm.location) : -1;
            return (diff==0) ? (this.form != null ? this.form.compareTo(anotherTransferForm.form) : -1) : 0;
        }
        return -1;
    }
    
}
