/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.archive;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public abstract class ArchiveForm extends GenericTimestampedForm {

    public static enum ARCHIVE_TAGS implements TAGS{
        type, duration, programme, playlist, time;

        private ARCHIVE_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case type: return I18nTranslator.getInstance().type();
                case duration: return I18nTranslator.getInstance().duration();
                case programme: return I18nTranslator.getInstance().programme();
                case playlist: return I18nTranslator.getInstance().playlist();
                case time: return I18nTranslator.getInstance().time();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                //Unsupported
                case type: return null;
                //Unsupported
                case duration: return LOOSE_TYPE.numeric;
                case programme: return LOOSE_TYPE.alphanumeric;
                case playlist: return LOOSE_TYPE.alphanumeric;
                //Unsupported
                case time: return null;
                default: return null;
            }
        }
    };

    private ArchiveFormID location;
    private String playlistId;
    private String programmeId;
    private JSDuration duration;

    public JSDuration getDuration() {
        return duration;
    }

    public void setDuration(JSDuration duration) {
        this.duration = duration;
    }

    public ArchiveFormID getRefId() {
        return location;
    }

    public void setRefId(ArchiveFormID location) {
        this.location = location;
    }

    public String getPlaylistFriendlyId() {
        return playlistId;
    }

    public void setPlaylistFriendlyId(String playlistCaption) {
        this.playlistId = playlistCaption;
    }

    public String getProgrammeFriendlyId() {
        return programmeId;
    }

    public void setProgrammeFriendlyId(String programmeId) {
        this.programmeId = programmeId;
    }

    public abstract String getContentCaption();
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ArchiveForm other = (ArchiveForm) obj;
        if (this.location != other.location) {
            return false;
        }
        /*if (this.playlistId != other.playlistId && (this.playlistId == null || !this.playlistId.equals(other.playlistId))) {
            return false;
        }
        if ((this.endTime == null) ? (other.endTime != null) : !this.endTime.equals(other.endTime)) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.location != null ? this.location.hashCode() : 0);
        /*hash = 47 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
        hash = 47 * hash + (this.playlistId != null ? this.playlistId.hashCode() : 0);**/
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof ArchiveForm) {
            ArchiveForm anotherArchiveForm = (ArchiveForm)o;
            //Sort by id
            return this.location!=null ? this.location.compareTo(anotherArchiveForm.location) : -1;
        }
        return -1;
    }

}
