/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.archive;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.archive.format.TextModule;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 *
 */
public class ReportForm extends GenericTimestampedForm {

    public static enum REPORT_TAGS implements TAGS {

        state, from, to, channel;

        private REPORT_TAGS() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case state:
                    return I18nTranslator.getInstance().state();
                case from:
                    return I18nTranslator.getInstance().from();
                case to:
                    return I18nTranslator.getInstance().to();
                case channel:
                    return I18nTranslator.getInstance().channel();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch (this) {
                case state:
                    return null;
                //Unsupported
                case from:
                    return null;
                //Unsupported
                case to:
                    return null;
                case channel:
                    return LOOSE_TYPE.alphanumeric;
                default:
                    return null;
            }
        }
    };

    private ReportFormID location;
    private String dataDownloadURL;
    
    private TextModule textFile;
    
    private boolean ready;

    public ReportFormID getRefId() {
        return location;
    }

    public void setRefId(ReportFormID location) {
        this.location = location;
    }

    public String getDataDownloadURL() {
        return dataDownloadURL;
    }

    public void setDataDownloadURL(String dataDownloadURL) {
        this.dataDownloadURL = dataDownloadURL;
    }

    public ReportFormID getLocation() {
        return location;
    }

    public void setLocation(ReportFormID location) {
        this.location = location;
    }

    public TextModule getTextFile() {
        return textFile;
    }

    public void setTextFile(TextModule textFile) {
        this.textFile = textFile;
    }

    public boolean hasText() {
        return getTextFile()!=null;
    }
    
    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public String getFriendlyCaption() {
        return getRefId() != null ? //
                (//
                getRefId().getFromTime() != null && getRefId().getToTime() != null && getRefId().getChannelId() != null
                ? getRefId().getFromTime().getI18nSyntheticFriendlyString() + "-" + getRefId().getToTime().getI18nSyntheticFriendlyString() + "@" + getRefId().getChannelId()//
                : getRefId().getRefID()//
                )//
                : I18nTranslator.getInstance().undefined();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReportForm other = (ReportForm) obj;
        if (this.location != other.location) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.location != null ? this.location.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return -1;
        }
        if (o instanceof ReportForm) {
            ReportForm anotherArchiveForm = (ReportForm) o;
            //Sort by id
            return this.location != null ? this.location.compareTo(anotherArchiveForm.location) : -1;
        }
        return -1;
    }
}
