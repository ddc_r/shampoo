/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.archive;

import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class ReportFormID implements Serializable, Comparable<ReportFormID> {

    private String refID;
    private YearMonthDayHourMinuteSecondMillisecondInterface fromTime;
    private YearMonthDayHourMinuteSecondMillisecondInterface toTime;
    private String channelId;

    public ReportFormID() {
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public YearMonthDayHourMinuteSecondMillisecondInterface getFromTime() {
        return fromTime;
    }

    public void setFromTime(YearMonthDayHourMinuteSecondMillisecondInterface fromTime) {
        this.fromTime = fromTime;
    }

    public YearMonthDayHourMinuteSecondMillisecondInterface getToTime() {
        return toTime;
    }

    public void setToTime(YearMonthDayHourMinuteSecondMillisecondInterface toTime) {
        this.toTime = toTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReportFormID other = (ReportFormID) obj;
        if (this.fromTime != other.fromTime && (this.fromTime == null || !this.fromTime.equals(other.fromTime))) {
            return false;
        }
        if (this.toTime != other.toTime && (this.toTime == null || !this.toTime.equals(other.toTime))) {
            return false;
        }
        if ((this.channelId == null) ? (other.channelId != null) : !this.channelId.equals(other.channelId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.fromTime != null ? this.fromTime.hashCode() : 0);
        hash = 59 * hash + (this.toTime != null ? this.toTime.hashCode() : 0);
        hash = 59 * hash + (this.channelId != null ? this.channelId.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(ReportFormID anotherArchiveFormID) {
        //Sort by channel, then by scheduling, finally by id
        int diff = this.channelId!=null ? this.channelId.compareTo(anotherArchiveFormID.channelId) : (anotherArchiveFormID.channelId==null ? 0 : -1);
        diff = diff==0 ? (this.fromTime!=null ? this.fromTime.compareTo(anotherArchiveFormID.fromTime) : (anotherArchiveFormID.fromTime==null ? 0 : -1)) : diff;
        diff = diff==0 ? (this.toTime!=null ? this.toTime.compareTo(anotherArchiveFormID.toTime) : (anotherArchiveFormID.toTime==null ? 0 : -1)) : diff;
        return diff;
    }
}
