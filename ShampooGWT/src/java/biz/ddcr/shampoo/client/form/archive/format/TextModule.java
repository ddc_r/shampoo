/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.archive.format;

import biz.ddcr.shampoo.client.form.track.format.*;
import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.form.archive.HasTextInterface;

/**
 *
 * @author okay_awright
 *
 */
public class TextModule implements FormatModule, HasTextInterface {

    private TEXT_TYPE format;
    /**
     * size in bytes
     */
    private long size;
    /**
     * URL *
     */
    private String url;

    public TextModule() {
        //GWT serialization
    }

    @Override
    public TEXT_TYPE getFormat() {
        return format;
    }

    public void setFormat(TEXT_TYPE format) {
        this.format = format;
    }

    public void setURL(String url) {
        this.url = url;
    }

    @Override
    public String getTextDownloadURL() {
        return getURL();
    }

    @Override
    public String getURL() {
        return url;
    }

    @Override
    public TEXT_TYPE getTextFormat() {
        return getFormat();
    }

    @Override
    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TextModule other = (TextModule) obj;
        if (this.format != other.format) {
            return false;
        }
        if (this.size != other.size) {
            return false;
        }
        if ((this.url == null) ? (other.url != null) : !this.url.equals(other.url)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (this.format != null ? this.format.hashCode() : 0);
        hash = 71 * hash + (int) (this.size ^ (this.size >>> 32));
        hash = 71 * hash + (this.url != null ? this.url.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) {
            return -1;
        }
        if (o instanceof FormatModule) {
            FormatModule o2 = (FormatModule) o;
            //Sort by format, then by size, then by url
            int diff = this.format != null && this.format.getI18nFriendlyString() != null ? this.format.getI18nFriendlyString().compareTo(o2.getFormat() != null ? o2.getFormat().getI18nFriendlyString() : null) : (o2.getFormat() == null ? 0 : -1);
            diff = diff == 0 ? (int) (this.size - o2.getSize()) : diff;
            return (diff == 0) ? (this.url != null ? this.url.compareTo(o2.getURL()) : -1) : diff;
        }
        return -1;
    }
}
