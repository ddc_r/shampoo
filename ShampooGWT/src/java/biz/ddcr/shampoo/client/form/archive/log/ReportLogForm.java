/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.archive.log;

import biz.ddcr.shampoo.client.form.archive.log.ReportLogForm.REPORT_LOG_OPERATION;
import biz.ddcr.shampoo.client.form.journal.LOG_OPERATION;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ReportLogForm extends LogForm<REPORT_LOG_OPERATION> {

    @Override
    public String getFriendlyTypeCaption() {
        return I18nTranslator.getInstance().report();
    }

    public enum REPORT_LOG_OPERATION implements LOG_OPERATION {

        read,
        add,
        delete;

        private REPORT_LOG_OPERATION() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case read:
                    return I18nTranslator.getInstance().read();
                case add:
                    return I18nTranslator.getInstance().add();
                case delete:
                    return I18nTranslator.getInstance().delete();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }

    private YearMonthDayHourMinuteSecondMillisecondInterface fromDateStamp;
    private YearMonthDayHourMinuteSecondMillisecondInterface toDateStamp;
    private String channelId;

    private ReportLogForm() {}

    public ReportLogForm(YearMonthDayHourMinuteSecondMillisecondInterface fromDateStamp, YearMonthDayHourMinuteSecondMillisecondInterface toDateStamp, String channelId) {
        this.fromDateStamp = fromDateStamp;
        this.toDateStamp = toDateStamp;
        this.channelId = channelId;
    }

    @Override
    public String getFriendlyActeeCaption() {
        //return the ID plus the date stamp and channel
        StringBuilder output = new StringBuilder();
        if (fromDateStamp != null) {
            output.append(fromDateStamp.getI18nSyntheticFriendlyString());
        }
        if (toDateStamp != null) {
            if (fromDateStamp != null) output.append(" - ");
            output.append(toDateStamp.getI18nSyntheticFriendlyString());
        }
        if ((fromDateStamp != null || toDateStamp != null) && channelId != null) {
            output.append(" @ ");
        }
        if (channelId != null) {
            output.append(channelId);
        }
        if (fromDateStamp != null || toDateStamp != null || channelId != null) {
            output.append(" [");
        }
        output.append(getActee());
        if (fromDateStamp != null || toDateStamp != null || channelId != null) {
            output.append("]");
        }

        return output.toString();
    }
}
