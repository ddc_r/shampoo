/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.archive.notification;

import biz.ddcr.shampoo.client.form.archive.notification.ReportContentNotificationForm.REPORT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NOTIFICATION_CONTENT_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NotificationContentForm;
import biz.ddcr.shampoo.client.form.notification.NotificationFormVisitor;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ReportContentNotificationForm extends NotificationContentForm<REPORT_NOTIFICATION_OPERATION, String> {

    private YearMonthDayHourMinuteSecondMillisecondInterface fromDateStamp;
    private YearMonthDayHourMinuteSecondMillisecondInterface toDateStamp;
    private String channelLabel;
    private String metadataFormat;

    protected String getFriendlyReportCaption() {
        //return the ID plus the date stamp and channel
        StringBuilder output = new StringBuilder();
        if (fromDateStamp != null) {
            output.append(fromDateStamp.getI18nSyntheticFriendlyString());
        }
        if (toDateStamp != null) {
            if (fromDateStamp != null) output.append(" - ");
            output.append(toDateStamp.getI18nSyntheticFriendlyString());
        }
        if ((fromDateStamp != null || toDateStamp != null) && channelLabel != null) {
            output.append(" @ ");
        }
        if (channelLabel != null) {
            output.append(channelLabel);
        }
        if (metadataFormat!=null) {
            if ((fromDateStamp != null || toDateStamp != null) && channelLabel != null) output.append(" / ");
            output.append(metadataFormat);
        }
        if (fromDateStamp != null || toDateStamp != null || channelLabel != null || metadataFormat != null) {
            output.append(" [");
        }
        output.append(getContentModified());
        if (fromDateStamp != null || toDateStamp != null || channelLabel != null || metadataFormat != null) {
            output.append("]");
        }

        return output.toString();
    }

    protected ReportContentNotificationForm() {
    }

    public ReportContentNotificationForm(REPORT_NOTIFICATION_OPERATION operation, String reportIDModfied, YearMonthDayHourMinuteSecondMillisecondInterface associatedFromDateStamp, YearMonthDayHourMinuteSecondMillisecondInterface associatedToDateStamp, String associatedChannelLabel, String associatedMetadataFormatLabel) {
        super(operation, reportIDModfied);
        this.fromDateStamp = associatedFromDateStamp;
        this.toDateStamp = associatedToDateStamp;
        this.channelLabel = associatedChannelLabel;
        this.metadataFormat = associatedMetadataFormatLabel;
    }

    @Override
    public String getFriendlyMessage() {
        switch (getType()) {
                default:
                    return I18nTranslator.getInstance().unknown();
            }
    }

    public enum REPORT_NOTIFICATION_OPERATION implements NOTIFICATION_CONTENT_OPERATION {

        add,
        delete;
    }

    @Override
    public void acceptVisit(NotificationFormVisitor visitor) {
        visitor.visit(this);
    }
}
