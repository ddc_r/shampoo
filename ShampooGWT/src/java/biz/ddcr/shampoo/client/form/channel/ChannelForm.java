/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.channel;

import biz.ddcr.shampoo.client.form.DomainForm;
import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class ChannelForm extends GenericTimestampedForm implements Serializable, DomainForm {

    public static enum CHANNEL_TAGS implements TAGS{
        label, description, programmes, rights;

        private CHANNEL_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case label: return I18nTranslator.getInstance().label();
                case description: return I18nTranslator.getInstance().description();
                case programmes: return I18nTranslator.getInstance().programmes();
                case rights: return I18nTranslator.getInstance().rights();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                case label: return LOOSE_TYPE.alphanumeric;
                case description: return LOOSE_TYPE.alphanumeric;
                case programmes: return LOOSE_TYPE.alphanumeric;
                case rights: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }



    };

    private String label;
    private String description;
    private String timezone;
    private ActionCollection<String> programmes;
    private ActionCollection<String> channelAdministratorRights;
    private ActionCollection<String> programmeManagerRights;
    private ActionCollection<String> listenerRights;
    private ActionCollectionEntry<Long> seatNumber;

    private Integer maxDailyRequestLimitPerUser;

    //Misc data
    private String url;
    private String tag;
    private boolean open;

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public Integer getMaxDailyRequestLimitPerUser() {
        return maxDailyRequestLimitPerUser;
    }

    public void setMaxDailyRequestLimitPerUser(Integer maxDailyRequestLimitPerUser) {
        this.maxDailyRequestLimitPerUser = maxDailyRequestLimitPerUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    public ActionCollection<String> getProgrammes() {
        if (programmes==null) programmes = new ActionSortedMap<String>();
        return programmes;
    }

    public void setProgrammes(ActionCollection<String> programmes) {
        this.programmes = programmes;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezone() {
        return timezone;
    }

    public ActionCollection<String> getListenerRights() {
        if (listenerRights == null) listenerRights = new ActionSortedMap<String>();
        return listenerRights;
    }

    public void setListenerRights(ActionCollection<String> listenerRights) {
        this.listenerRights = listenerRights;
    }

    public ActionCollection<String> getProgrammeManagerRights() {
        if (programmeManagerRights == null) programmeManagerRights = new ActionSortedMap<String>();
        return programmeManagerRights;
    }

    public void setProgrammeManagerRights(ActionCollection<String> programmeManagerRights) {
        this.programmeManagerRights = programmeManagerRights;
    }

    public ActionCollection<String> getChannelAdministratorRights() {
        if (channelAdministratorRights == null) channelAdministratorRights = new ActionSortedMap<String>();
        return channelAdministratorRights;
    }

    public void setChannelAdministratorRights(ActionCollection<String> channelAdministratorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
    }

    public ActionCollection<RightForm> getRightForms() {
        ActionMap<RightForm> rightForms = new ActionSortedMap<RightForm>();
        for (ActionCollectionEntry<String> userEntry : getChannelAdministratorRights())
            rightForms.set(new ChannelRightForm(userEntry.getKey(), getLabel(), ChannelRole.channelAdministrator), userEntry.getValue());
        for (ActionCollectionEntry<String> userEntry : getProgrammeManagerRights())
            rightForms.set(new ChannelRightForm(userEntry.getKey(), getLabel(), ChannelRole.programmeManager), userEntry.getValue());
        for (ActionCollectionEntry<String> userEntry : getListenerRights())
            rightForms.set(new ChannelRightForm(userEntry.getKey(), getLabel(), ChannelRole.listener), userEntry.getValue());
        return rightForms;
    }

    public void setRightForms(ActionCollection<RightForm> rightForms) {
        getChannelAdministratorRights().clear();
        getProgrammeManagerRights().clear();
        getListenerRights().clear();
        for (final ActionCollectionEntry<RightForm> rightFormEntry: rightForms) {
            RightForm rightForm = rightFormEntry.getKey();
            if (rightForm.getRole().getClass()==ChannelRole.class) {
                switch ((ChannelRole)rightForm.getRole()) {
                    case channelAdministrator:
                        getChannelAdministratorRights().merge(rightForm.getRestrictedUserId(), rightFormEntry.getValue());
                        break;
                    case programmeManager:
                        getProgrammeManagerRights().merge(rightForm.getRestrictedUserId(), rightFormEntry.getValue());
                        break;
                    case listener:
                        getListenerRights().merge(rightForm.getRestrictedUserId(), rightFormEntry.getValue());
                        break;
                }
            }
        }
    }

    public ActionCollectionEntry<Long> getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(ActionCollectionEntry<Long> seatNumber) {
        this.seatNumber = seatNumber;
    }

    public ChannelForm() {
        //GWT serialization
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChannelForm other = (ChannelForm) obj;
        if ((this.label == null) ? (other.label != null) : !this.label.equals(other.label)) {
            return false;
        }
        /*if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if (this.programmes != other.programmes && (this.programmes == null || !this.programmes.equals(other.programmes))) {
            return false;
        }
        if (this.channelAdministratorRights != other.channelAdministratorRights && (this.channelAdministratorRights == null || !this.channelAdministratorRights.equals(other.channelAdministratorRights))) {
            return false;
        }
        if (this.programmeManagerRights != other.programmeManagerRights && (this.programmeManagerRights == null || !this.programmeManagerRights.equals(other.programmeManagerRights))) {
            return false;
        }
        if (this.listenerRights != other.listenerRights && (this.listenerRights == null || !this.listenerRights.equals(other.listenerRights))) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.label != null ? this.label.hashCode() : 0);
        /*hash = 47 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 47 * hash + (this.programmes != null ? this.programmes.hashCode() : 0);
        hash = 47 * hash + (this.channelAdministratorRights != null ? this.channelAdministratorRights.hashCode() : 0);
        hash = 47 * hash + (this.programmeManagerRights != null ? this.programmeManagerRights.hashCode() : 0);
        hash = 47 * hash + (this.listenerRights != null ? this.listenerRights.hashCode() : 0);**/
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof ChannelForm) {
            ChannelForm anotherChannelForm = (ChannelForm)o;
            //Sort by label
            return this.label!=null ? this.label.compareTo(anotherChannelForm.label) : -1;
        }
        return -1;
    }
    
}
