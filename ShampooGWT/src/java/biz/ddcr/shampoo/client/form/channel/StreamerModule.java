package biz.ddcr.shampoo.client.form.channel;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT;
import java.io.Serializable;

/**
 * A streamer wrapper
 *
 * @author okay_awright
 **/
public class StreamerModule implements Serializable, IsModuleInterface {
    
    //Read-only properties
    
    private SERIALIZATION_METADATA_FORMAT metadataFormat;
    private int queueMinItems;
    /** Do lives can be picked by getNextTrack() and queued like normal items; set to false for liquidsoap 0.91 **/
    private boolean queueableLive;
    /** Do off-air events (no timetable slot) can be picked by getNextTrack() and queued like normal items; set to true for liquidsoap 0.91 **/
    private boolean queueableBlank;
     /** If a live can be queued, specify its URI path; it's a a string where variables are substituted **/
    private String queueableLiveURI;
    /** Should the live be split into multiple queue items of queueableLiveChunkSize minutes or queued as a whole; 0 or null means do not split it **/
    private Long queueableLiveChunkSize;
    /** If a blank can be queued, specify its URI path; it's a a string where variables are substituted **/
    private String queueableBlankURI;
    /** Should the blank be split into multiple queue items of queueableBlankChunkSize milliseconds or queued as a whole; 0 or null means do not split it **/
    private Long queueableBlankChunkSize;
    /** Loose reference to a playlist media that contains the streams that this streamer operates */
    private String streamURI;
    /** current streamer id handling this stream */
    private String userAgentID;
    /** latest recorded heartbeat, Unix Epoch; null means none yet **/
    private Long latestHeartbeat;
    /** max. interval between heartbeats before declaring a streamer dead and its seat free for use; O or null means don't use this feature*/
    private Long ttl;
    /** Is the attached channel on-air? void means connecting */
    private Boolean streaming;
    
    //Read/Write properties

    /** TODO For whatever reason, if the application cannot pick a track to send to the streamer, should we send a default file instead of an exception **/
    private boolean enableEmergencyItems;    
    /** seat allocation number; 0 or null means auto-allocation on the fly **/
    private Long seatNumber;
    
    public String getI18nFriendlyName() {
        return I18nTranslator.getInstance().streamer_technical_info(
            getSeatNumber() != null ? Long.toString(getSeatNumber()) : I18nTranslator.getInstance().none(),
            getUserAgentID() != null ? getUserAgentID() : I18nTranslator.getInstance().none(),
            getStreamURI() != null ? getStreamURI() : I18nTranslator.getInstance().none(),
            getMetadataFormat() != null ? getMetadataFormat().getI18nFriendlyString() : I18nTranslator.getInstance().not_applicable(),
            Integer.toString(getQueueMinItems()),
            isQueueableBlank() ? I18nTranslator.getInstance().yes() : I18nTranslator.getInstance().no(),
            getQueueableBlankChunkSize() != null ? Long.toString(getQueueableBlankChunkSize()) : I18nTranslator.getInstance().not_applicable(),
            getQueueableBlankURI() != null ? getQueueableBlankURI() : I18nTranslator.getInstance().none(),
            isQueueableLive() ? I18nTranslator.getInstance().yes() : I18nTranslator.getInstance().no(),
            getQueueableLiveChunkSize() != null ? Long.toString(getQueueableLiveChunkSize()) : I18nTranslator.getInstance().not_applicable(),
            getQueueableLiveURI() != null ? getQueueableLiveURI() : I18nTranslator.getInstance().none(),
            isEnableEmergencyItems() ? I18nTranslator.getInstance().yes() : I18nTranslator.getInstance().no(),
            getLatestHeartbeat() != null ? Long.toString(getLatestHeartbeat()) : I18nTranslator.getInstance().none(),
            getTtl() != null ? Long.toString(getTtl()) : I18nTranslator.getInstance().not_applicable());
    }
    
    public StreamerModule() {
        //GWT serialization
    }

    public StreamerModule(SERIALIZATION_METADATA_FORMAT metadataFormat, int queueMinItems, boolean queueableLive, boolean queueableBlank, String queueableLiveURI, Long queueableLiveChunkSize, String queueableBlankURI, Long queueableBlankChunkSize, String streamURI, String userAgentID, Long latestHeartbeat, Long ttl, Boolean streaming) {
        this.metadataFormat = metadataFormat;
        this.queueMinItems = queueMinItems;
        this.queueableLive = queueableLive;
        this.queueableBlank = queueableBlank;
        this.queueableLiveURI = queueableLiveURI;
        this.queueableLiveChunkSize = queueableLiveChunkSize;
        this.queueableBlankURI = queueableBlankURI;
        this.queueableBlankChunkSize = queueableBlankChunkSize;
        this.streamURI = streamURI;
        this.userAgentID = userAgentID;
        this.latestHeartbeat = latestHeartbeat;
        this.ttl = ttl;
        this.streaming = streaming;
    }

    public boolean isEnableEmergencyItems() {
        return enableEmergencyItems;
    }

    public Long getLatestHeartbeat() {
        return latestHeartbeat;
    }

    public SERIALIZATION_METADATA_FORMAT getMetadataFormat() {
        return metadataFormat;
    }

    public int getQueueMinItems() {
        return queueMinItems;
    }

    public boolean isQueueableBlank() {
        return queueableBlank;
    }

    public Long getQueueableBlankChunkSize() {
        return queueableBlankChunkSize;
    }

    public String getQueueableBlankURI() {
        return queueableBlankURI;
    }

    public boolean isQueueableLive() {
        return queueableLive;
    }

    public Long getQueueableLiveChunkSize() {
        return queueableLiveChunkSize;
    }

    public String getQueueableLiveURI() {
        return queueableLiveURI;
    }

    public Long getSeatNumber() {
        return seatNumber;
    }

    public String getStreamURI() {
        return streamURI;
    }

    public String getUserAgentID() {
        return userAgentID;
    }

    public Long getTtl() {
        return ttl;
    }

    public Boolean isStreaming() {
        return streaming;
    }

    public void setEnableEmergencyItems(boolean enableEmergencyItems) {
        this.enableEmergencyItems = enableEmergencyItems;
    }

    public void setSeatNumber(Long seatNumber) {
        this.seatNumber = seatNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StreamerModule other = (StreamerModule) obj;
        if (this.seatNumber != other.seatNumber && (this.seatNumber == null || !this.seatNumber.equals(other.seatNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.seatNumber != null ? this.seatNumber.hashCode() : 0);        
        /*hash = 97 * hash + (this.privateKey != null ? this.privateKey.hashCode() : 0);*/
        return hash;
    }
    
    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof StreamerModule) {
            StreamerModule o2 = (StreamerModule)o;
            //Sort by seat, then by key
            return this.seatNumber != null ? this.seatNumber.compareTo(o2.seatNumber) : -1;
            /*return (diff == 0) ? (this.privateKey != null ? this.privateKey.compareTo(o2.privateKey) : -1) : diff;*/
        }
        return -1;
    }
}
