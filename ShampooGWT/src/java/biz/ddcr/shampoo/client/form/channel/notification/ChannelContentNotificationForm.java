/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.channel.notification;

import biz.ddcr.shampoo.client.form.channel.notification.ChannelContentNotificationForm.CHANNEL_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NOTIFICATION_CONTENT_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NotificationContentForm;
import biz.ddcr.shampoo.client.form.notification.NotificationFormVisitor;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelContentNotificationForm extends NotificationContentForm<CHANNEL_NOTIFICATION_OPERATION, String> {

    protected ChannelContentNotificationForm() {
    }

    public ChannelContentNotificationForm(CHANNEL_NOTIFICATION_OPERATION operation, String entityIDModfied) {
        super(operation, entityIDModfied);
    }

    @Override
    public String getFriendlyMessage() {
        switch (getType()) {
            case edit:
                return I18nTranslator.getInstance().channel_content_edited(getContentModified());
            default:
                return I18nTranslator.getInstance().unknown();
        }
    }

    public enum CHANNEL_NOTIFICATION_OPERATION implements NOTIFICATION_CONTENT_OPERATION {

        add,
        edit,
        delete;
    }

    @Override
    public void acceptVisit(NotificationFormVisitor visitor) {
        visitor.visit(this);
    }
}
