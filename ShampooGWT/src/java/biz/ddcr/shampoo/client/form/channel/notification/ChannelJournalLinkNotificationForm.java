/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.channel.notification;

import biz.ddcr.shampoo.client.form.channel.notification.ChannelJournalLinkNotificationForm.CHANNELJOURNAL_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NOTIFICATION_LINK_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NotificationFormVisitor;
import biz.ddcr.shampoo.client.form.notification.NotificationLinkForm;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelJournalLinkNotificationForm extends NotificationLinkForm<CHANNELJOURNAL_NOTIFICATION_OPERATION, String, String> {

    private YearMonthDayHourMinuteSecondMillisecondInterface dateStamp;
    private String channelLabel;

    protected String getFriendlyJournalCaption() {
        //return the ID plus the date stamp and channel
        StringBuilder output = new StringBuilder();
        if (dateStamp != null) {
            output.append(dateStamp.getI18nSyntheticFriendlyString());
        }
        if (dateStamp != null && channelLabel != null) {
            output.append(" @ ");
        }
        if (channelLabel != null) {
            output.append(channelLabel);
        }
        if (dateStamp != null || channelLabel != null) {
            output.append(" [");
        }
        output.append(getLinkModifiedSideB());
        if (dateStamp != null || channelLabel != null) {
            output.append("]");
        }

        return output.toString();
    }

    protected ChannelJournalLinkNotificationForm() {}
    public ChannelJournalLinkNotificationForm(CHANNELJOURNAL_NOTIFICATION_OPERATION operation, String channelIDModfied, String logIDModfied, YearMonthDayHourMinuteSecondMillisecondInterface associatedDateStamp, String associatedChannelLabel) {
        super(operation, channelIDModfied, logIDModfied);
        this.dateStamp = associatedDateStamp;
        this.channelLabel = associatedChannelLabel;
    }

    @Override
    public String getFriendlyMessage() {
        switch (getType()) {
                case delete:
                    return I18nTranslator.getInstance().channel_journal_link_removed(getLinkModifiedSideA(), getFriendlyJournalCaption());
                case add:
                    return I18nTranslator.getInstance().channel_journal_link_added(getLinkModifiedSideA(), getFriendlyJournalCaption());
                default:
                    return I18nTranslator.getInstance().unknown();
            }
    }

    public enum CHANNELJOURNAL_NOTIFICATION_OPERATION implements NOTIFICATION_LINK_OPERATION {
        delete,
        add;
    }

    @Override
    public void acceptVisit(NotificationFormVisitor visitor) {
        visitor.visit(this);
    }
}
