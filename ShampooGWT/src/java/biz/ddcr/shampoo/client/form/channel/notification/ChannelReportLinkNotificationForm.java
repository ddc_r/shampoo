/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.channel.notification;

import biz.ddcr.shampoo.client.form.channel.notification.ChannelReportLinkNotificationForm.CHANNELREPORT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NOTIFICATION_LINK_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NotificationFormVisitor;
import biz.ddcr.shampoo.client.form.notification.NotificationLinkForm;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelReportLinkNotificationForm extends NotificationLinkForm<CHANNELREPORT_NOTIFICATION_OPERATION, String, String> {

    private YearMonthDayHourMinuteSecondMillisecondInterface startDateStamp;
    private YearMonthDayHourMinuteSecondMillisecondInterface endDateStamp;
    private String channelLabel;

    protected String getFriendlyReportCaption() {
        //return the ID plus the date stamp and channel
        StringBuilder output = new StringBuilder();
        if (startDateStamp != null) {
            output.append(startDateStamp.getI18nSyntheticFriendlyString());
        }
        if (startDateStamp != null && endDateStamp != null) {
            output.append(" - ");
        }
        if (endDateStamp != null) {
            output.append(endDateStamp.getI18nSyntheticFriendlyString());
        }
        if ((startDateStamp != null || endDateStamp != null) && channelLabel != null) {
            output.append(" @ ");
        }
        if (channelLabel != null) {
            output.append(channelLabel);
        }
        if (startDateStamp != null || endDateStamp != null || channelLabel != null) {
            output.append(" [");
        }
        output.append(getLinkModifiedSideB());
        if (startDateStamp != null || endDateStamp != null || channelLabel != null) {
            output.append("]");
        }

        return output.toString();
    }

    protected ChannelReportLinkNotificationForm() {}
    public ChannelReportLinkNotificationForm(CHANNELREPORT_NOTIFICATION_OPERATION operation, String channelIDModfied, String archiveIDModfied, YearMonthDayHourMinuteSecondMillisecondInterface associatedStartDateStamp, YearMonthDayHourMinuteSecondMillisecondInterface associatedEndDateStamp, String associatedChannelLabel) {
        super(operation, channelIDModfied, archiveIDModfied);
        this.startDateStamp = associatedStartDateStamp;
        this.endDateStamp = associatedEndDateStamp;
        this.channelLabel = associatedChannelLabel;
    }

    @Override
    public String getFriendlyMessage() {
        switch (getType()) {
                case delete:
                    return I18nTranslator.getInstance().channel_report_link_removed(getLinkModifiedSideA(), getFriendlyReportCaption());
                case add:
                    return I18nTranslator.getInstance().channel_report_link_added(getLinkModifiedSideA(), getFriendlyReportCaption());
                default:
                    return I18nTranslator.getInstance().unknown();
            }
    }

    public enum CHANNELREPORT_NOTIFICATION_OPERATION implements NOTIFICATION_LINK_OPERATION {
        add,
        delete;
    }

    @Override
    public void acceptVisit(NotificationFormVisitor visitor) {
        visitor.visit(this);
    }

}
