/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.channel.notification;

import biz.ddcr.shampoo.client.form.channel.notification.StreamerStatusNotificationForm.STREAMER_STATUS_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NOTIFICATION_CONTENT_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NotificationContentForm;
import biz.ddcr.shampoo.client.form.notification.NotificationFormVisitor;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class StreamerStatusNotificationForm extends NotificationContentForm<STREAMER_STATUS_OPERATION, String> {

    protected StreamerStatusNotificationForm() {}
    public StreamerStatusNotificationForm(STREAMER_STATUS_OPERATION operation, String channel) {
        super(operation, channel);
    }

    @Override
    public String getFriendlyMessage() {
        switch (getType()) {
                case channel_reserved:
                    return I18nTranslator.getInstance().channel_reserved(getContentModified());
                case channel_free:
                    return I18nTranslator.getInstance().channel_free(getContentModified());
                case channel_onair:
                    return I18nTranslator.getInstance().channel_online(getContentModified());
                case channel_offair:
                    return I18nTranslator.getInstance().channel_offline(getContentModified());
                default:
                    return I18nTranslator.getInstance().unknown();
            }
    }

    public enum STREAMER_STATUS_OPERATION implements NOTIFICATION_CONTENT_OPERATION {

        channel_reserved,
        channel_free,
        channel_onair,
        channel_offair;
    }

    @Override
    public void acceptVisit(NotificationFormVisitor visitor) {
        visitor.visit(this);
    }
}
