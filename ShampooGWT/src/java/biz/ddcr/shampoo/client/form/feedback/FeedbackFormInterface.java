/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.feedback;

import java.io.Serializable;

/**
 * Lightweight feedback signals used by MessageBroadcaster
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface FeedbackFormInterface extends Serializable, Comparable<FeedbackFormInterface> {
   
    public String getSenderId();
    public void setSenderId(String senderId);
    /** used for sorting feedback items in a collction based on the time they were created*/
    public long getTimestamp();
    /** allows to sort items whose timestamp is identical*/
    public int getChunkIndex();
    
}
