/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.feedback;

/**
 * Lightweight feedback signals used by MessageBroadcaster
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class FeedbackMessageForm implements FeedbackFormInterface {
    public enum TYPE {
        error,
        warning,
        info
    }
    private TYPE type;
    private String text;
    private String senderId;
    private long timestamp;
    private int chunkIndex;

    public FeedbackMessageForm() {
        //GWT serialization
    }
    
    public FeedbackMessageForm(TYPE type, String text, String senderId, long timestamp, int sequenceOrder) {
        this.type = type;
        this.text = text;
        this.senderId = senderId;
        this.timestamp = timestamp;
        this.chunkIndex = sequenceOrder;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    @Override
    public String getSenderId() {
        return senderId;
    }

    @Override
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int getChunkIndex() {
        return chunkIndex;
    }

    public void setChunkIndex(int chunkIndex) {
        this.chunkIndex = chunkIndex;
    }

    @Override
    public int compareTo(FeedbackFormInterface o) {
        long diff = (getTimestamp() - o.getTimestamp());
        if (diff==0)
            diff = getChunkIndex() - o.getChunkIndex();
        return (int)diff;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeedbackMessageForm other = (FeedbackMessageForm) obj;
        if (this.type != other.type) {
            return false;
        }
        if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
            return false;
        }
        if ((this.senderId == null) ? (other.senderId != null) : !this.senderId.equals(other.senderId)) {
            return false;
        }
        if (this.timestamp != other.timestamp) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 17 * hash + (this.text != null ? this.text.hashCode() : 0);
        hash = 17 * hash + (this.senderId != null ? this.senderId.hashCode() : 0);
        hash = 17 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        return hash;
    }

}
