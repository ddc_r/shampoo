/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.journal;

import biz.ddcr.shampoo.client.form.journal.ExceptionLogForm.EXCEPTION_LOG_OPERATION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ExceptionLogForm extends LogForm<EXCEPTION_LOG_OPERATION> {

    @Override
    public String getFriendlyTypeCaption() {
        return I18nTranslator.getInstance().error();
    }

    public enum EXCEPTION_LOG_OPERATION implements LOG_OPERATION {

        data,
        io,
        general;

        private EXCEPTION_LOG_OPERATION() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case data:
                    return I18nTranslator.getInstance().data_exception();
                case io:
                    return I18nTranslator.getInstance().io_exception();
                case general:
                    return I18nTranslator.getInstance().general_exception();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }

    private String exceptionMessage;

    private ExceptionLogForm() {}

    public ExceptionLogForm(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public String getFriendlyActeeCaption() {
        //return the exceptionclass plus its detailed message
        StringBuilder output = new StringBuilder();
        if (exceptionMessage!=null) {
            output.append(exceptionMessage).append(" [");
        }
        output.append(getActee());
        if (exceptionMessage!=null) {
            output.append("]");
        }

        return output.toString();
    }

}
