/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.journal;

import biz.ddcr.shampoo.client.form.GenericFormInterface;
import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.LooseTypeInterface.LOOSE_TYPE;

/**
 *
 * @author okay_awright
 **/
public abstract class LogForm<T extends LOG_OPERATION> implements GenericFormInterface {

    public static enum LOG_TAGS implements TAGS{
        time, type, actor, action, actee;

        private LOG_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case time: return I18nTranslator.getInstance().time();
                case type: return I18nTranslator.getInstance().type();
                case actor: return I18nTranslator.getInstance().actor();
                case action: return I18nTranslator.getInstance().action();
                case actee: return I18nTranslator.getInstance().actee();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                //Unsupported
                case time: return null;
                //Unsupported
                case type: return null;
                //Unsupported
                case actor: return LOOSE_TYPE.alphanumeric;
                case action: return LOOSE_TYPE.alphanumeric;
                case actee: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }

    };

    private String refID;
    private YearMonthDayHourMinuteSecondMillisecondInterface time;
    private ActionCollection<String> channels;
    private String actor;
    private String actee;
    private T action;

    protected LogForm() {}

    public ActionCollection<String> getChannels() {
        return channels;
    }

    public void setChannels(ActionCollection<String> channels) {
        this.channels = channels;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public YearMonthDayHourMinuteSecondMillisecondInterface getTime() {
        return time;
    }

    public void setTime(YearMonthDayHourMinuteSecondMillisecondInterface time) {
        this.time = time;
    }

    public String getActee() {
        return actee;
    }

    public void setActee(String actee) {
        this.actee = actee;
    }

    public T getAction() {
        return action;
    }

    public void setAction(T action) {
        this.action = action;
    }

    public abstract String getFriendlyActeeCaption();

    public abstract String getFriendlyTypeCaption();

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LogForm<T> other = (LogForm<T>) obj;
        if ((this.refID == null) ? (other.refID != null) : !this.refID.equals(other.refID)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.refID != null ? this.refID.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) return -1;
        if (o instanceof LogForm) {
            LogForm anotherLogForm = (LogForm)o;
            //Sort by time, then by by actee, then by actor, then by id
            int diff = this.time != null ? this.time.compareTo(anotherLogForm.time) : (anotherLogForm.time == null ? 0 : -1);
            if (diff==0) {
                diff = this.actee != null ? this.actee.compareTo(anotherLogForm.actee) : (anotherLogForm.actee == null ? 0 : -1);
                if (diff==0) {
                    diff = this.actor != null ? this.actor.compareTo(anotherLogForm.actor) : (anotherLogForm.actor == null ? 0 : -1);
                    return (diff == 0) ? (this.refID != null ? this.refID.compareTo(anotherLogForm.refID) : -1) : diff;
                }
            }
            return diff;
        }
        return -1;
    }

}
