/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.notification;

/**
 *
 * @author okay_awright
 **/
public abstract class NotificationContentForm<T extends NOTIFICATION_CONTENT_OPERATION, U> extends NotificationForm<T> {

    private U contentModified;

    protected NotificationContentForm() {}

    public NotificationContentForm(T operation, U entityIDModified) {
        super(operation);
        contentModified = entityIDModified;
    }

    public U getContentModified() {
        return contentModified;
    }

    public void setContentModified(U contentModified) {
        this.contentModified = contentModified;
    }

}
