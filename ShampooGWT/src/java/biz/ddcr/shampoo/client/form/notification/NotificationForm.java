/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.notification;

import biz.ddcr.shampoo.client.form.GenericFormInterface;
import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.LooseTypeInterface.LOOSE_TYPE;

/**
 *
 * @author okay_awright
 *
 */
public abstract class NotificationForm<T extends NOTIFICATION_OPERATION> implements GenericFormInterface {

    public static enum NOTIFICATION_TAGS implements TAGS {

        time, message;

        private NOTIFICATION_TAGS() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case time:
                    return I18nTranslator.getInstance().time();
                case message:
                    return I18nTranslator.getInstance().message();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch (this) {
                //Unsupported
                case time:
                    return null;
                //Unsupported
                case message:
                    return null;
                default:
                    return null;
            }
        }
    };
    private String refID;
    private YearMonthDayHourMinuteSecondMillisecondInterface time;
    private ActionCollectionEntry<String> recipient;
    private T type;
    private String freeFormText;
    private ActionCollectionEntry<String> sender;
    private boolean recipientNotified;

    protected NotificationForm() {
    }

    public NotificationForm(T operation) {
        type = operation;
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public YearMonthDayHourMinuteSecondMillisecondInterface getTime() {
        return time;
    }

    public void setTime(YearMonthDayHourMinuteSecondMillisecondInterface time) {
        this.time = time;
    }

    public ActionCollectionEntry<String> getRecipient() {
        return recipient;
    }

    public void setRecipient(ActionCollectionEntry<String> recipient) {
        this.recipient = recipient;
    }

    public T getType() {
        return type;
    }

    public void setType(T type) {
        this.type = type;
    }

    public String getFreeFormText() {
        return freeFormText;
    }

    public void setFreeFormText(String freeFormText) {
        this.freeFormText = freeFormText;
    }

    public abstract String getFriendlyMessage();

    public ActionCollectionEntry<String> getSender() {
        return sender;
    }

    public void setSender(ActionCollectionEntry<String> sender) {
        this.sender = sender;
    }

    public boolean isRecipientNotified() {
        return recipientNotified;
    }

    public void setRecipientNotified(boolean recipientNotified) {
        this.recipientNotified = recipientNotified;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotificationForm<T> other = (NotificationForm<T>) obj;
        if ((this.refID == null) ? (other.refID != null) : !this.refID.equals(other.refID)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.refID != null ? this.refID.hashCode() : 0);
        return hash;
    }

    public abstract void acceptVisit(NotificationFormVisitor visitor);

    @Override
    public int compareTo(Object o) {
        if (o == null) return -1;
        if (o instanceof NotificationForm) {
            NotificationForm anotherNotificationForm = (NotificationForm)o;
            //Sort by time, then by id
            int diff = this.time != null ? this.time.compareTo(anotherNotificationForm.time) : (anotherNotificationForm.time == null ? 0 : -1);
            return (diff == 0) ? (this.refID != null ? this.refID.compareTo(anotherNotificationForm.refID) : -1) : diff;
        }
        return -1;
    }
}
