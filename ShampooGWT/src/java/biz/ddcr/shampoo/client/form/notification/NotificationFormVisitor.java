/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.notification;

import biz.ddcr.shampoo.client.form.archive.notification.ArchiveContentNotificationForm;
import biz.ddcr.shampoo.client.form.archive.notification.ReportContentNotificationForm;
import biz.ddcr.shampoo.client.form.channel.notification.*;
import biz.ddcr.shampoo.client.form.journal.notification.LogContentNotificationForm;
import biz.ddcr.shampoo.client.form.playlist.notification.PlaylistContentNotificationForm;
import biz.ddcr.shampoo.client.form.playlist.notification.PlaylistProgrammeLinkNotificationForm;
import biz.ddcr.shampoo.client.form.playlist.notification.PlaylistTimetableSlotLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeAnimatorLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContentNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContributorLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeCuratorLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammePendingTrackLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeTimetableSlotLinkNotificationForm;
import biz.ddcr.shampoo.client.form.timetable.notification.TimetableSlotContentNotificationForm;
import biz.ddcr.shampoo.client.form.track.notification.BroadcastTrackContentNotificationForm;
import biz.ddcr.shampoo.client.form.track.notification.PendingTrackContentNotificationForm;
import biz.ddcr.shampoo.client.form.user.notification.AdministratorContentNotificationForm;
import biz.ddcr.shampoo.client.form.user.notification.RestrictedUserContentNotificationForm;
import biz.ddcr.shampoo.client.form.webservice.notification.WebserviceContentNotificationForm;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface NotificationFormVisitor {

    public void visit(AdministratorContentNotificationForm message);
    
    public void visit(LogContentNotificationForm message);
    
    public void visit(ArchiveContentNotificationForm message);
    
    public void visit(ReportContentNotificationForm message);
    
    public void visit(ChannelArchiveLinkNotificationForm message);

    public void visit(ChannelChannelAdministratorLinkNotificationForm message);

    public void visit(ChannelContentNotificationForm message);

    public void visit(ChannelJournalLinkNotificationForm message);

    public void visit(ChannelListenerLinkNotificationForm message);

    public void visit(ChannelNotificationNotificationForm message);

    public void visit(ChannelProgrammeLinkNotificationForm message);

    public void visit(ChannelProgrammeManagerLinkNotificationForm message);

    public void visit(ChannelQueueItemLinkNotificationForm message);

    public void visit(ChannelTimetableSlotLinkNotificationForm message);

    public void visit(ChannelWebserviceLinkNotificationForm message);
    
    public void visit(ChannelReportLinkNotificationForm message);

    public void visit(StreamerStatusNotificationForm message);

    public void visit(ExceptionNotificationForm message);

    public void visit(PlaylistContentNotificationForm message);

    public void visit(PlaylistProgrammeLinkNotificationForm message);

    public void visit(PlaylistTimetableSlotLinkNotificationForm message);

    public void visit(ProgrammeAnimatorLinkNotificationForm message);

    public void visit(ProgrammeBroadcastableTrackLinkNotificationForm message);

    public void visit(ProgrammeContentNotificationForm message);

    public void visit(ProgrammeContributorLinkNotificationForm message);

    public void visit(ProgrammeCuratorLinkNotificationForm message);

    public void visit(ProgrammePendingTrackLinkNotificationForm message);

    public void visit(ProgrammeTimetableSlotLinkNotificationForm message);

    public void visit(TimetableSlotContentNotificationForm message);

    public void visit(BroadcastTrackContentNotificationForm message);

    public void visit(PendingTrackContentNotificationForm message);

    public void visit(RestrictedUserContentNotificationForm message);

    public void visit(WebserviceContentNotificationForm message);
}
