/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.notification;

/**
 *
 * @author okay_awright
 **/
public abstract class NotificationLinkForm<T extends NOTIFICATION_LINK_OPERATION, U, V> extends NotificationForm<T> {

    private U linkModifiedSideA;
    private V linkModifiedSideB;

    protected NotificationLinkForm() {}

    public NotificationLinkForm(T operation, U SideAEntityIDModified, V SideBEntityIDModified) {
        super(operation);
        linkModifiedSideA = SideAEntityIDModified;
        linkModifiedSideB = SideBEntityIDModified;
    }

    public U getLinkModifiedSideA() {
        return linkModifiedSideA;
    }

    public void setLinkModifiedSideA(U linkModifiedSideA) {
        this.linkModifiedSideA = linkModifiedSideA;
    }

    public V getLinkModifiedSideB() {
        return linkModifiedSideB;
    }

    public void setLinkModifiedSideB(V linkModifiedSideB) {
        this.linkModifiedSideB = linkModifiedSideB;
    }

}
