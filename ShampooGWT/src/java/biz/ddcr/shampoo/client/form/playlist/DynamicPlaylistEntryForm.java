package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.form.track.SELECTION;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import java.util.Collection;
import java.util.HashSet;


/**
 * A dynamic Slot is an Playlist item with Selection
 *
 * @author okay_awright
 */
public class DynamicPlaylistEntryForm extends PlaylistEntryForm {

    private SELECTION selection;
    private SubSelectionModule subSelection;
    private Long noReplayDelay;
    private boolean noReplayInPlaylist;
    private Collection<FilterForm> filters;

    public DynamicPlaylistEntryForm() {
        //GWT serialization
    }

    public Long getNoReplayDelay() {
        return noReplayDelay;
    }

    public void setNoReplayDelay(Long noReplayDelay) {
        this.noReplayDelay = noReplayDelay;
    }

    public SubSelectionModule getSubSelection() {
        return subSelection;
    }

    public void setSubSelection(SubSelectionModule subSelection) {
        this.subSelection = subSelection;
    }

    public Collection<FilterForm> getFilters() {
        if (filters==null) filters = new HashSet<FilterForm>();
        return filters;
    }

    public void setFilters(Collection<FilterForm> filters) {
        this.filters = filters;
    }

    public boolean isNoReplayInPlaylist() {
        return noReplayInPlaylist;
    }

    public void setNoReplayInPlaylist(boolean noReplayInPlaylist) {
        this.noReplayInPlaylist = noReplayInPlaylist;
    }

    public SELECTION getSelection() {
        return selection;
    }

    public void setSelection(SELECTION selection) {
        this.selection = selection;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DynamicPlaylistEntryForm other = (DynamicPlaylistEntryForm) obj;
        if (this.selection != other.selection && (this.selection == null || !this.selection.equals(other.selection))) {
            return false;
        }
        if (this.subSelection != other.subSelection && (this.subSelection == null || !this.subSelection.equals(other.subSelection))) {
            return false;
        }
        if (this.filters != other.filters && (this.filters == null || !this.filters.equals(other.filters))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.selection != null ? this.selection.hashCode() : 0);
        hash = 53 * hash + (this.subSelection != null ? this.subSelection.hashCode() : 0);
        hash = 53 * hash + (this.filters != null ? this.filters.hashCode() : 0);
        return hash;
    }

}
