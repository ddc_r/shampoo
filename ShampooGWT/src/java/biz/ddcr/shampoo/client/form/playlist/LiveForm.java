package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.track.HasPEGIRatingInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import java.io.Serializable;

/**
 * A Live
 *
 * @author okay_awright
 **/
public class LiveForm extends GenericTimestampedForm implements Serializable, HasPEGIRatingInterface {
   
    private String broadcaster;
    private String login;
    private String password;
    private PEGIRatingModule advisory;

    public LiveForm() {
        //GWT serialization
    }

    @Override
    public PEGIRatingModule getAdvisory() {
        return advisory;
    }

    public void setAdvisory(PEGIRatingModule advisory) {
        this.advisory = advisory;
    }

    public String getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        this.broadcaster = broadcaster;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LiveForm other = (LiveForm) obj;     
        if ((this.broadcaster == null) ? (other.broadcaster != null) : !this.broadcaster.equals(other.broadcaster)) {
            return false;
        }
        /*if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }**/
        if ((this.login == null) ? (other.login != null) : !this.login.equals(other.login)) {
            return false;
        }
        /*if ((this.password == null) ? (other.password != null) : !this.password.equals(other.password)) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.broadcaster != null ? this.broadcaster.hashCode() : 0);
        //hash = 29 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 29 * hash + (this.login != null ? this.login.hashCode() : 0);
        //hash = 29 * hash + (this.password != null ? this.password.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof LiveForm) {
            LiveForm anotherLiveForm = (LiveForm)o;
            //Sort by broadcaster, then by login
            int diff = this.broadcaster!=null ? this.broadcaster.compareTo(anotherLiveForm.broadcaster) : (anotherLiveForm.broadcaster==null ? 0 : -1);
            return (diff==0) ? (this.login != null ? this.login.compareTo(anotherLiveForm.login) : -1) : diff;
        }
        return -1;
    }
    
}
