package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import java.io.Serializable;

/**
 * A slot in a playlist
 *
 * @author okay_awright
 **/
public abstract class PlaylistEntryForm extends GenericTimestampedForm implements Serializable {

    private long sequenceIndex;
    /** in seconds*/
    private Integer fadeIn;
    private Long loop;
    private boolean requestAllowed;

    public PlaylistEntryForm() {
        //GWT serialization
    }

    public Long getLoop() {
        return loop;
    }

    public void setLoop(Long loop) {
        this.loop = loop;
    }

    public long getSequenceIndex() {
        return sequenceIndex;
    }

    public void setSequenceIndex(long sequenceIndex) {
        this.sequenceIndex = sequenceIndex;
    }

    public boolean isRequestAllowed() {
        return requestAllowed;
    }

    public void setRequestAllowed(boolean requestAllowed) {
        this.requestAllowed = requestAllowed;
    }

    public Integer getFadeIn() {
        return fadeIn;
    }

    public void setFadeIn(Integer fadeIn) {
        this.fadeIn = fadeIn;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlaylistEntryForm other = (PlaylistEntryForm) obj;
        if (this.sequenceIndex != other.sequenceIndex) {
            return false;
        }
        if (this.fadeIn != other.fadeIn && (this.fadeIn == null || !this.fadeIn.equals(other.fadeIn))) {
            return false;
        }
        if (this.loop != other.loop && (this.loop == null || !this.loop.equals(other.loop))) {
            return false;
        }
        if (this.requestAllowed != other.requestAllowed) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (int) (this.sequenceIndex ^ (this.sequenceIndex >>> 32));
        hash = 37 * hash + (this.fadeIn != null ? this.fadeIn.hashCode() : 0);
        hash = 37 * hash + (this.loop != null ? this.loop.hashCode() : 0);
        hash = 37 * hash + (this.requestAllowed ? 1 : 0);
        return hash;
    }    
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof PlaylistEntryForm) {
            PlaylistEntryForm anotherPlaylistEntryForm = (PlaylistEntryForm)o;
            //Sort by index
            return (int) (this.sequenceIndex - anotherPlaylistEntryForm.sequenceIndex);
        }
        return -1;
    }

}
