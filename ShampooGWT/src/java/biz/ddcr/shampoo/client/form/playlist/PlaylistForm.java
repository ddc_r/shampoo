package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 * A playlist bound to both a programme and a timetable
 *
 * @author okay_awright
 **/
public class PlaylistForm extends GenericTimestampedForm implements Serializable {

    public static enum PLAYLIST_TAGS implements TAGS{
        label, description, timetableSlots, live, programme;

        private PLAYLIST_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case label: return I18nTranslator.getInstance().label();
                case description: return I18nTranslator.getInstance().description();
                case timetableSlots: return I18nTranslator.getInstance().timetables();
                case live: return I18nTranslator.getInstance().live();
                case programme: return I18nTranslator.getInstance().programme();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                case label: return LOOSE_TYPE.alphanumeric;
                case description: return LOOSE_TYPE.alphanumeric;
                case timetableSlots: return LOOSE_TYPE.alphanumeric;
                //Unsupported
                case live: return null;
                case programme: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }
    };

    private PlaylistFormID refID;
    private String description;
    private Collection<PlaylistEntryForm> playlistEntries;
    private ActionMap<TimetableSlotFormID> timetableSlotIds;
    private LiveForm live;
    private ActionCollectionEntry<String> programmeId;
    private PlaylistTagSetForm globalTagSet;

    private Integer maxUserRequestLimit;
    private Long noRequestReplayDelay;
    private boolean noRequestReplayInPlaylist;

    private CoverArtModule coverArtFile;

    private boolean ready;

    public PlaylistForm() {
        //GWT serialization
        refID = new PlaylistFormID();
    }

    public String getRefID() {
        return refID.getRefID();
    }

    public void setRefID(String refID) {
        this.refID.setRefID(refID);
    }

    public PlaylistFormID getPlaylistFormID() {
        return refID;
    }

    public CoverArtModule getCoverArtFile() {
        return coverArtFile;
    }

    public void setCoverArtFile(CoverArtModule file) {
        this.coverArtFile = file;
    }

    public boolean hasPicture() {
        return getCoverArtFile()!=null;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean isReady) {
        ready = isReady;
    }

    public Integer getMaxUserRequestLimit() {
        return maxUserRequestLimit;
    }

    public void setMaxUserRequestLimit(Integer maxUserRequestLimit) {
        this.maxUserRequestLimit = maxUserRequestLimit;
    }

    public Long getNoRequestReplayDelay() {
        return noRequestReplayDelay;
    }

    public void setNoRequestReplayDelay(Long noRequestReplayDelay) {
        this.noRequestReplayDelay = noRequestReplayDelay;
    }

    public boolean isNoRequestReplayInPlaylist() {
        return noRequestReplayInPlaylist;
    }

    public void setNoRequestReplayInPlaylist(boolean noRequestReplayInPlaylist) {
        this.noRequestReplayInPlaylist = noRequestReplayInPlaylist;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel() {
        return refID.getLabel();
    }

    public void setLabel(String label) {
        this.refID.setLabel(label);
    }

    public String getFriendlyID() {
        return this.refID.getFriendlyID();
    }

    public Collection<PlaylistEntryForm> getPlaylistEntries() {
        if (playlistEntries==null) playlistEntries = new HashSet<PlaylistEntryForm>();
        return playlistEntries;
    }

    public void setPlaylistEntries(Collection<PlaylistEntryForm> playlistEntries) {
        this.playlistEntries = playlistEntries;
    }

    public LiveForm getLive() {
        return live;
    }

    public void setLive(LiveForm live) {
        this.live = live;
    }

    public PlaylistTagSetForm getGlobalTagSet() {
        return globalTagSet;
    }

    public void setGlobalTagSet(PlaylistTagSetForm globalTagSet) {
        this.globalTagSet = globalTagSet;
    }

    public ActionCollectionEntry<String> getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(ActionCollectionEntry<String> programmeId) {
        this.programmeId = programmeId;
    }

    public ActionMap<TimetableSlotFormID> getTimetableSlotIds() {
        if (timetableSlotIds==null) timetableSlotIds = new ActionSortedMap<TimetableSlotFormID>();
        return timetableSlotIds;
    }

    public void setTimetableSlotIds(ActionMap<TimetableSlotFormID> ids) {
        this.timetableSlotIds = ids;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlaylistForm other = (PlaylistForm) obj;
        if (this.getRefID() != other.getRefID() && (this.getRefID() == null || !this.getRefID().equals(other.getRefID()))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.getRefID() != null ? this.getRefID().hashCode() : 0);
        return hash;
    }
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof PlaylistForm) {
            PlaylistForm anotherPlaylistForm = (PlaylistForm)o;
            //Sort by id
            return this.refID!=null ? this.refID.compareTo(anotherPlaylistForm.refID) : -1;
        }
        return -1;
    }

}
