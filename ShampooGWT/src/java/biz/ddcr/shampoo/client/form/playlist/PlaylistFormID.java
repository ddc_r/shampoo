/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class PlaylistFormID implements Serializable, Comparable<PlaylistFormID> {
    private String refID;
    private String label;

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFriendlyID() {
        return (getLabel()!=null && getLabel().length()!=0) ? (getLabel() + (getRefID()!=null ? " [" + getRefID() + "]" : "")) : (getRefID()!=null ? getRefID() : I18nTranslator.getInstance().unknown());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlaylistFormID other = (PlaylistFormID) obj;
        if ((this.refID == null) ? (other.refID != null) : !this.refID.equals(other.refID)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.refID != null ? this.refID.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(PlaylistFormID o) {
        //Sort by label, then by id
        int diff = this.label!=null ? this.label.compareTo(o.label) : (o.label==null ? 0 : -1);
        return (diff==0) ? (this.refID!=null ? this.refID.compareTo(o.refID) : -1) : diff; 
    }

}
