package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.form.track.TagModule;

/**
 * A tag set overriding each of a playlist own track's
 *
 * @author okay_awright
 **/
public class PlaylistTagSetForm extends TagModule {
   
    public PlaylistTagSetForm() {
        //GWT serialization
    }
}
