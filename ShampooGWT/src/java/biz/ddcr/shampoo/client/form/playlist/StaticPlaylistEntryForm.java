package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;


/**
 * A static Slot is an Playlist item
 *
 * @author okay_awright
 */
public class StaticPlaylistEntryForm extends PlaylistEntryForm {

    private ActionCollectionEntry<String> broadcastTrackId;

    public StaticPlaylistEntryForm() {
        //GWT serialization
    }

    public ActionCollectionEntry<String> getBroadcastTrackId() {
        return broadcastTrackId;
    }

    public void setBroadcastTrackId(ActionCollectionEntry<String> broadcastTrackId) {
        this.broadcastTrackId = broadcastTrackId;
    }

}
