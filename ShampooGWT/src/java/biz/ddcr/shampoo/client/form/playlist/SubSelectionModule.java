/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.playlist;

import biz.ddcr.shampoo.client.form.track.SORT;
import biz.ddcr.shampoo.client.form.IsModuleInterface;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class SubSelectionModule implements Serializable, IsModuleInterface {

    private SORT sort;
    private Long from;
    private Long to;

    public SubSelectionModule() {
        //GWT serialization
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public SORT getSort() {
        return sort;
    }

    public void setSort(SORT sort) {
        this.sort = sort;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubSelectionModule other = (SubSelectionModule) obj;
        if (this.sort != other.sort) {
            return false;
        }
        if (this.from != other.from && (this.from == null || !this.from.equals(other.from))) {
            return false;
        }
        if (this.to != other.to && (this.to == null || !this.to.equals(other.to))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.sort != null ? this.sort.hashCode() : 0);
        hash = 67 * hash + (this.from != null ? this.from.hashCode() : 0);
        hash = 67 * hash + (this.to != null ? this.to.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof SubSelectionModule) {
            SubSelectionModule o2 = (SubSelectionModule)o;
            //Sort by sort, then by from, finally by to
            int diff = this.sort != null ? this.sort.compareTo(o2.sort) : (o2.sort == null ? 0 : -1);
            diff = diff==0 ? (this.from != null ? this.from.compareTo(o2.from) : (o2.from == null ? 0 : -1)) : diff;
            return (diff == 0) ? (this.to != null ? this.to.compareTo(o2.to) : -1) : diff;
        }
        return -1;
    }
    
}
