/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.playlist.filter;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class AlphabeticalFilterForm extends FilterForm {

    public static enum ALPHABETICAL_FEATURE implements FEATURE {

        title,
        author,
        album,
        genre,
        flag,
        publisher,
        copyright,
        advisory;

        private ALPHABETICAL_FEATURE() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case title:
                    return I18nTranslator.getInstance().title();
                case author:
                    return I18nTranslator.getInstance().author();
                case album:
                    return I18nTranslator.getInstance().album();
                case genre:
                    return I18nTranslator.getInstance().genre();
                case flag:
                    return I18nTranslator.getInstance().tag();
                case publisher:
                    return I18nTranslator.getInstance().publisher();
                case copyright:
                    return I18nTranslator.getInstance().copyright();
                case advisory:
                    return I18nTranslator.getInstance().advisory();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    };
    private ALPHABETICAL_FEATURE feature;
    private String contains;

    public String getContains() {
        return contains;
    }

    public void setContains(String contains) {
        this.contains = contains;
    }

    @Override
    public ALPHABETICAL_FEATURE getFeature() {
        return feature;
    }

    public void setFeature(ALPHABETICAL_FEATURE feature) {
        this.feature = feature;
    }

    @Override
    public String getI18nFriendlyString() {
        return feature != null
                ? feature.getI18nFriendlyString() + " " + (isInclude()?I18nTranslator.getInstance().contains():I18nTranslator.getInstance().does_not_contain()) + " " + getContains()
                : I18nTranslator.getInstance().unknown();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlphabeticalFilterForm other = (AlphabeticalFilterForm) obj;
        if (this.feature != other.feature) {
            return false;
        }
        if ((this.contains == null) ? (other.contains != null) : !this.contains.equals(other.contains)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (this.feature != null ? this.feature.hashCode() : 0);
        hash = 73 * hash + (this.contains != null ? this.contains.hashCode() : 0);
        return hash;
    }
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof FilterForm) {
            FilterForm anotherFilterForm = (FilterForm)o;
            //Sort by feature
            int diff = this.feature!=null && this.feature.getI18nFriendlyString()!=null ? this.feature.getI18nFriendlyString().compareTo(anotherFilterForm.getFeature()!=null ? anotherFilterForm.getFeature().getI18nFriendlyString() : null) : (anotherFilterForm.getFeature()==null ? 0 : -1);
            if (diff==0) {
                //then by contains
                if (anotherFilterForm instanceof AlphabeticalFilterForm) {
                    AlphabeticalFilterForm anotherAlphabeticalFilterForm = (AlphabeticalFilterForm)anotherFilterForm;
                    return this.contains!=null ? this.contains.compareTo(anotherAlphabeticalFilterForm.contains) : -1;
                }
                return -1;
            }
            return diff;
        }
        return -1;
    }
}
