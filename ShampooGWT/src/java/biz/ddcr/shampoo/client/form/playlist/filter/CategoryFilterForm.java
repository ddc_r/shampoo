/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.playlist.filter;

import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class CategoryFilterForm extends FilterForm {

    private TYPE feature;

    @Override
    public TYPE getFeature() {
        return feature;
    }

    public void setFeature(TYPE feature) {
        this.feature = feature;
    }

    @Override
    public String getI18nFriendlyString() {
        return feature != null
                ? (isInclude()?I18nTranslator.getInstance().is():I18nTranslator.getInstance().is_not()) + " " + feature.getI18nFriendlyString()
                : I18nTranslator.getInstance().unknown();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategoryFilterForm other = (CategoryFilterForm) obj;
        if (this.feature != other.feature) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.feature != null ? this.feature.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof FilterForm) {
            FilterForm anotherFilterForm = (FilterForm)o;
            //Sort by feature
            return this.feature!=null && this.feature.getI18nFriendlyString()!=null ? this.feature.getI18nFriendlyString().compareTo(anotherFilterForm.getFeature()!=null ? anotherFilterForm.getFeature().getI18nFriendlyString() : null) : -1;
        }
        return -1;
    }
    
}
