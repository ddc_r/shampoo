/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.playlist.filter;

import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public enum FILTER_FEATURE implements I18nFriendlyInterface, Serializable {
        alphabetical,
        numerical,
        category;

        private FILTER_FEATURE() {}

    @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case alphabetical: return I18nTranslator.getInstance().alphabetical();
                case numerical: return I18nTranslator.getInstance().numeric();
                case category: return I18nTranslator.getInstance().type();
                default: return I18nTranslator.getInstance().unknown();
            }
        }
    };
