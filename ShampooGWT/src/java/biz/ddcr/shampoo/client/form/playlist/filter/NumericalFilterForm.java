/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.playlist.filter;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class NumericalFilterForm extends FilterForm {

    public static enum NUMERICAL_FEATURE implements FEATURE {

        averageVote,
        dateOfSubmission,
        dateOfRelease,
        rotationCount;

        private NUMERICAL_FEATURE() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case averageVote:
                    return I18nTranslator.getInstance().average_vote();
                case dateOfSubmission:
                    return I18nTranslator.getInstance().submission_date();
                case dateOfRelease:
                    return I18nTranslator.getInstance().release_date();
                case rotationCount:
                    return I18nTranslator.getInstance().rotation_count();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }
    private NUMERICAL_FEATURE feature;
    private short min;
    private short max;

    @Override
    public NUMERICAL_FEATURE getFeature() {
        return feature;
    }

    public void setFeature(NUMERICAL_FEATURE feature) {
        this.feature = feature;
    }

    public short getMax() {
        return max;
    }

    public void setMax(short max) {
        this.max = max;
    }

    public short getMin() {
        return min;
    }

    public void setMin(short min) {
        this.min = min;
    }

    @Override
    public String getI18nFriendlyString() {
        return feature != null
                ? feature.getI18nFriendlyString() + " "
                + (isInclude() ? I18nTranslator.getInstance().in_interval_x_to_y_inclusive(String.valueOf(getMin()), String.valueOf(getMax())) : I18nTranslator.getInstance().outside_interval_x_to_y_exclusive(String.valueOf(getMin()), String.valueOf(getMax())))
                : I18nTranslator.getInstance().unknown();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NumericalFilterForm other = (NumericalFilterForm) obj;
        if (this.feature != other.feature) {
            return false;
        }
        if (this.min != other.min) {
            return false;
        }
        if (this.max != other.max) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.feature != null ? this.feature.hashCode() : 0);
        hash = 41 * hash + this.min;
        hash = 41 * hash + this.max;
        return hash;
    }
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof FilterForm) {
            FilterForm anotherFilterForm = (FilterForm)o;
            //Sort by feature
            int diff = this.feature!=null && this.feature.getI18nFriendlyString()!=null ? this.feature.getI18nFriendlyString().compareTo(anotherFilterForm.getFeature()!=null ? anotherFilterForm.getFeature().getI18nFriendlyString() : null) : (anotherFilterForm.getFeature()==null ? 0 : -1);
            if (diff==0) {
                //then by min, then by max
                if (anotherFilterForm instanceof NumericalFilterForm) {
                    NumericalFilterForm anotherNumericalFilterForm = (NumericalFilterForm)anotherFilterForm;
                    diff = this.min - anotherNumericalFilterForm.min;
                    return (diff==0) ? this.max - anotherNumericalFilterForm.max : diff;
                }
                return -1;
            }
            return diff;
        }
        return -1;
    }
}
