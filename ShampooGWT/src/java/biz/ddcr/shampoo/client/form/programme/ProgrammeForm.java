package biz.ddcr.shampoo.client.form.programme;

import biz.ddcr.shampoo.client.form.DomainForm;
import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRightForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import java.io.Serializable;

/**
 * A programme scheduled on a specific channel
 *
 * @author okay_awright
 **/
public class ProgrammeForm extends GenericTimestampedForm implements Serializable, DomainForm {

    public static enum PROGRAMME_TAGS implements TAGS{
        label, description, channels, rights;

        private PROGRAMME_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case label: return I18nTranslator.getInstance().label();
                case description: return I18nTranslator.getInstance().description();
                case channels: return I18nTranslator.getInstance().channels();
                case rights: return I18nTranslator.getInstance().rights();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                case label: return LOOSE_TYPE.alphanumeric;
                case description: return LOOSE_TYPE.alphanumeric;
                case channels: return LOOSE_TYPE.alphanumeric;
                case rights: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }
    };

    private String label;
    private String description;
    private ActionCollection<String> channels;
    private ActionCollection<String> animatorRights;
    private ActionCollection<String> curatorRights;
    private ActionCollection<String> contributorRights;
    private String metaInfoPattern;
    private boolean overlappingPlaylist;
    private ActionCollection<String> playlists;
    //Tracks are not handled on this side of the relation

    public ProgrammeForm() {
        //GWT serialization
    }

    public ActionCollection<String> getChannels() {
        if (channels==null) channels = new ActionSortedMap<String>();
        return channels;
    }

    public void setChannels(ActionCollection<String> channels) {
        this.channels = channels;
    }

    public ActionCollection<String> getAnimatorRights() {
        if (animatorRights == null) animatorRights = new ActionSortedMap<String>();
        return animatorRights;
    }

    public void setAnimatorRights(ActionCollection<String> animatorRights) {
        this.animatorRights = animatorRights;
    }

    public ActionCollection<String> getContributorRights() {
        if (contributorRights == null) contributorRights = new ActionSortedMap<String>();
        return contributorRights;
    }

    public void setContributorRights(ActionCollection<String> contributorRights) {
        this.contributorRights = contributorRights;
    }

    public ActionCollection<String> getCuratorRights() {
        if (curatorRights == null) curatorRights = new ActionSortedMap<String>();
        return curatorRights;
    }

    public void setCuratorRights(ActionCollection<String> curatorRights) {
        this.curatorRights = curatorRights;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    public String getMetaInfoPattern() {
        return metaInfoPattern;
    }

    public void setMetaInfoPattern(String metaInfoPattern) {
        this.metaInfoPattern = metaInfoPattern;
    }

    public boolean isOverlappingPlaylist() {
        return overlappingPlaylist;
    }

    public void setOverlappingPlaylist(boolean overlappingPlaylist) {
        this.overlappingPlaylist = overlappingPlaylist;
    }

    public ActionCollection<String> getPlaylists() {
        if (playlists==null) playlists = new ActionSortedMap<String>();
        return playlists;
    }

    public void setPlaylists(ActionCollection<String> playlists) {
        this.playlists = playlists;
    }

    public ActionCollection<RightForm> getRightForms() {
        ActionMap<RightForm> rightForms = new ActionSortedMap<RightForm>();
        for (ActionCollectionEntry<String> userEntry : getAnimatorRights())
            rightForms.set(new ProgrammeRightForm(userEntry.getKey(), getLabel(), ProgrammeRole.animator), userEntry.getValue());
        for (ActionCollectionEntry<String> userEntry : getCuratorRights())
            rightForms.set(new ProgrammeRightForm(userEntry.getKey(), getLabel(), ProgrammeRole.curator), userEntry.getValue());
        for (ActionCollectionEntry<String> userEntry : getContributorRights())
            rightForms.set(new ProgrammeRightForm(userEntry.getKey(), getLabel(), ProgrammeRole.contributor), userEntry.getValue());
        return rightForms;
    }

    public void setRightForms(ActionCollection<RightForm> rightForms) {
        getAnimatorRights().clear();
        getCuratorRights().clear();
        getContributorRights().clear();
        for (final ActionCollectionEntry<RightForm> rightFormEntry: rightForms) {
            RightForm rightForm = rightFormEntry.getKey();
            if (rightForm.getRole().getClass()==ProgrammeRole.class) {
                switch ((ProgrammeRole)rightForm.getRole()) {
                    case animator:
                        getAnimatorRights().merge(rightForm.getRestrictedUserId(), rightFormEntry.getValue());
                        break;
                    case curator:
                        getCuratorRights().merge(rightForm.getRestrictedUserId(), rightFormEntry.getValue());
                        break;
                    case contributor:
                        getContributorRights().merge(rightForm.getRestrictedUserId(), rightFormEntry.getValue());
                        break;
                }
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProgrammeForm other = (ProgrammeForm) obj;
        if ((this.label == null) ? (other.label != null) : !this.label.equals(other.label)) {
            return false;
        }
        /*if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if ((this.metaInfoPattern == null) ? (other.metaInfoPattern != null) : !this.metaInfoPattern.equals(other.metaInfoPattern)) {
            return false;
        }
        if (this.channels != other.channels && (this.channels == null || !this.channels.equals(other.channels))) {
            return false;
        }
        if (this.animatorRights != other.animatorRights && (this.animatorRights == null || !this.animatorRights.equals(other.animatorRights))) {
            return false;
        }
        if (this.curatorRights != other.curatorRights && (this.curatorRights == null || !this.curatorRights.equals(other.curatorRights))) {
            return false;
        }
        if (this.contributorRights != other.contributorRights && (this.contributorRights == null || !this.contributorRights.equals(other.contributorRights))) {
            return false;
        }
        if (this.playlists != other.playlists && (this.playlists == null || !this.playlists.equals(other.playlists))) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.label != null ? this.label.hashCode() : 0);
        /*hash = 61 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 61 * hash + (this.metaInfoPattern != null ? this.metaInfoPattern.hashCode() : 0);
        hash = 61 * hash + (this.channels != null ? this.channels.hashCode() : 0);
        hash = 61 * hash + (this.animatorRights != null ? this.animatorRights.hashCode() : 0);
        hash = 61 * hash + (this.curatorRights != null ? this.curatorRights.hashCode() : 0);
        hash = 61 * hash + (this.contributorRights != null ? this.contributorRights.hashCode() : 0);
        hash = 61 * hash + (this.playlists != null ? this.playlists.hashCode() : 0);**/
        return hash;
    }
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof ProgrammeForm) {
            ProgrammeForm anotherProgrammeForm = (ProgrammeForm)o;
            //Sort by label
            return this.label!=null ? this.label.compareTo(anotherProgrammeForm.label) : -1;
        }
        return -1;
    }
}
