/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.programme.notification;

import biz.ddcr.shampoo.client.form.notification.NOTIFICATION_LINK_OPERATION;
import biz.ddcr.shampoo.client.form.notification.NotificationFormVisitor;
import biz.ddcr.shampoo.client.form.notification.NotificationLinkForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ProgrammeBroadcastableTrackLinkNotificationForm extends NotificationLinkForm<PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION, String, String> {

    private String trackAuthorLabel;
    private String trackTitleLabel;

    protected String getFriendlyTrackCaption() {
        //return the ID plus the author-title combo
        StringBuilder output = new StringBuilder();
        if (trackAuthorLabel!=null)
            output.append(trackAuthorLabel);
        if (trackAuthorLabel!=null && trackTitleLabel!=null)
            output.append(" - ");
        if (trackTitleLabel!=null)
            output.append(trackTitleLabel);
        if (trackAuthorLabel != null || trackTitleLabel != null) {
            output.append(" [");
        }
        output.append(getLinkModifiedSideB());
        if (trackAuthorLabel != null || trackTitleLabel != null) {
            output.append("]");
        }

        return output.toString();
    }

    protected ProgrammeBroadcastableTrackLinkNotificationForm() {}
    public ProgrammeBroadcastableTrackLinkNotificationForm(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION operation, String sideAEntityIDModfied, String trackIDModfied, String associatedTrackAuthorLabel, String associatedTrackTitleLabel) {
        super(operation, sideAEntityIDModfied, trackIDModfied);
        trackAuthorLabel = associatedTrackAuthorLabel;
        trackTitleLabel = associatedTrackTitleLabel;
    }

    @Override
    public String getFriendlyMessage() {
        switch (getType()) {
                case delete:
                    return I18nTranslator.getInstance().programme_broadcast_track_link_removed(getLinkModifiedSideA(), getFriendlyTrackCaption());
                case add:
                    return I18nTranslator.getInstance().programme_broadcast_track_link_added(getLinkModifiedSideA(), getFriendlyTrackCaption());
                case validate:
                    return I18nTranslator.getInstance().programme_broadcast_track_link_validated(getLinkModifiedSideA(), getLinkModifiedSideB());
                default:
                    return I18nTranslator.getInstance().unknown();
            }
    }

    public enum PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION implements NOTIFICATION_LINK_OPERATION {

        delete,
        add,
        validate;
    }

    @Override
    public void acceptVisit(NotificationFormVisitor visitor) {
        visitor.visit(this);
    }
}
