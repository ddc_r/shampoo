/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.queue;

import biz.ddcr.shampoo.client.form.track.AverageVoteModule;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class BroadcastableTrackQueueForm extends QueueForm implements Serializable {

    private String trackId;
    private String trackAuthorCaption;
    private String trackTitleCaption;

    private boolean canBeVoted;
    /** Average rating from all users who voted; setting this property is useless after creation */
    private AverageVoteModule averageVote;

    public AverageVoteModule getAverageVote() {
        return averageVote;
    }

    public void setAverageVote(AverageVoteModule averageVote) {
        this.averageVote = averageVote;
    }

    public boolean isCanBeVoted() {
        return canBeVoted;
    }

    public void setCanBeVoted(boolean canBeVoted) {
        this.canBeVoted = canBeVoted;
    }

    public String getTrackAuthorCaption() {
        return trackAuthorCaption;
    }

    public void setTrackAuthorCaption(String trackAuthorCaption) {
        this.trackAuthorCaption = trackAuthorCaption;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackTitleCaption() {
        return trackTitleCaption;
    }

    public void setTrackTitleCaption(String trackTitleCaption) {
        this.trackTitleCaption = trackTitleCaption;
    }

    @Override
    public String getContentCaption() {
        return getTrackAuthorCaption() + " - " + getTrackTitleCaption();

    }
    
}
