/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.queue;

import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.archive.*;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.date.DurationMillisecondsInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.LooseTypeInterface.LOOSE_TYPE;

/**
 *
 * @author okay_awright
 **/
public abstract class QueueForm extends GenericTimestampedForm {

    public static enum QUEUE_TAGS implements TAGS{
        type, duration, time;

        private QUEUE_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case type: return I18nTranslator.getInstance().type();
                case duration: return I18nTranslator.getInstance().duration();
                case time: return I18nTranslator.getInstance().time();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                //Unsupported
                case type: return null;
                case duration: return LOOSE_TYPE.numeric;
                //Unsupported
                case time: return null;
                default: return null;
            }
        }
    };

    private ArchiveFormID location;
    private String playlistId;
    private String programmeId;
    private DurationMillisecondsInterface duration;
    private boolean pooled;

    /** attached flyer or cover art, if it's not a track with a correspongin cover art then use the default playlist flyer **/
    private CoverArtModule coverArtContainer;

    public boolean isPooled() {
        return pooled;
    }

    public void setPooled(boolean pooled) {
        this.pooled = pooled;
    }

    public DurationMillisecondsInterface getDuration() {
        return duration;
    }

    public void setDuration(DurationMillisecondsInterface duration) {
        this.duration = duration;
    }

    public ArchiveFormID getRefId() {
        return location;
    }

    public void setRefId(ArchiveFormID location) {
        this.location = location;
    }

    public String getPlaylistFriendlyId() {
        return playlistId;
    }

    public void setPlaylistFriendlyId(String playlistCaption) {
        this.playlistId = playlistCaption;
    }

    public String getProgrammeLabel() {
        return programmeId;
    }

    public void setProgrammeLabel(String programmeId) {
        this.programmeId = programmeId;
    }

    public abstract String getContentCaption();

    public CoverArtModule getCoverArtFile() {
        return coverArtContainer;
    }

    public void setCoverArtFile(CoverArtModule coverArtContainer) {
        this.coverArtContainer = coverArtContainer;
    }

    public boolean hasPicture() {
        return getCoverArtFile()!=null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QueueForm other = (QueueForm) obj;
        if (this.location != other.location) {
            return false;
        }
        /*if (this.playlistId != other.playlistId && (this.playlistId == null || !this.playlistId.equals(other.playlistId))) {
            return false;
        }
        if ((this.endTime == null) ? (other.endTime != null) : !this.endTime.equals(other.endTime)) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.location != null ? this.location.hashCode() : 0);
        /*hash = 47 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
        hash = 47 * hash + (this.playlistId != null ? this.playlistId.hashCode() : 0);**/
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof QueueForm) {
            QueueForm anotherQueueForm = (QueueForm)o;
            //Sort by id
            return this.location!=null ? this.location.compareTo(anotherQueueForm.location) : -1;
        }
        return -1;
    }

}
