/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.right;

import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class ChannelRightForm extends RightForm implements Serializable {

    private ChannelRole role;

    @Override
    public ChannelRole getRole() {
        return role;
    }

    public void setRole(ChannelRole role) {
        this.role = role;
    }

    //Don't drop the empty constructor, GWT needs it for its marshalling compliance
    private ChannelRightForm(){}

    public ChannelRightForm(String restrictedUserId, String channelId, ChannelRole role) {
        this.restrictedUserId = restrictedUserId;
        this.domainId = channelId;
        this.role = role;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChannelRightForm other = (ChannelRightForm) obj;
        if (this.domainId != other.domainId && (this.domainId == null || !this.domainId.equals(other.domainId))) {
            return false;
        }
        if (this.restrictedUserId != other.restrictedUserId && (this.restrictedUserId == null || !this.restrictedUserId.equals(other.restrictedUserId))) {
            return false;
        }
        if (this.role != other.role && (this.role == null || !this.role.equals(other.role))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.domainId != null ? this.domainId.hashCode() : 0);
        hash = 89 * hash + (this.restrictedUserId != null ? this.restrictedUserId.hashCode() : 0);
        hash = 89 * hash + (this.role != null ? this.role.hashCode() : 0);
        return hash;
    }

}
