/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.right;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public enum ChannelRole implements Role {
    channelAdministrator,
    programmeManager,
    listener;

    //Don't drop the empty constructor, GWT needs it for its marshalling compliance
    private ChannelRole(){}

    @Override
    public String getI18nFriendlyString() {
        switch(this) {
            case channelAdministrator: return I18nTranslator.getInstance().channel_administrator();
            case programmeManager: return I18nTranslator.getInstance().programme_manager();
            case listener: return I18nTranslator.getInstance().listener();
            default: return I18nTranslator.getInstance().unknown();
        }
    }
}
