/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.right;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public abstract class RightForm extends GenericTimestampedForm implements Serializable {

    protected String domainId;
    protected String restrictedUserId;

     public RightForm() {
        //Empty constructor; mandatory for GWT marshalling
    }

    public String getDomainId() {
        return domainId;
    }

    public String getRestrictedUserId() {
        return restrictedUserId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public void setRestrictedUserId(String restrictedUserId) {
        this.restrictedUserId = restrictedUserId;
    }

    public abstract Role getRole();

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RightForm other = (RightForm) obj;
        if ((this.domainId == null) ? (other.domainId != null) : !this.domainId.equals(other.domainId)) {
            return false;
        }
        if ((this.getRole() == null) ? (other.getRole() != null) : !this.getRole().equals(other.getRole())) {
            return false;
        }
        if ((this.restrictedUserId == null) ? (other.restrictedUserId != null) : !this.restrictedUserId.equals(other.restrictedUserId)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.domainId != null ? this.domainId.hashCode() : 0);
        hash = 97 * hash + (this.getRole() != null ? this.getRole().hashCode() : 0);
        hash = 97 * hash + (this.restrictedUserId != null ? this.restrictedUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof RightForm) {
            RightForm anotherRightForm = (RightForm)o;
            //Sort by domain, then by role, finally by user
            int diff = this.domainId!=null ? this.domainId.compareTo(anotherRightForm.domainId) : (anotherRightForm.domainId==null ? 0 : -1);
            diff = diff==0 ? (this.getRole()!=null && this.getRole().getI18nFriendlyString()!=null ? this.getRole().getI18nFriendlyString().compareTo(anotherRightForm.getRole()!=null ? anotherRightForm.getRole().getI18nFriendlyString() : null) : (anotherRightForm.getRole()==null ? 0 : -1)) : diff;
            return (diff==0) ? (this.restrictedUserId!=null ? this.restrictedUserId.compareTo(anotherRightForm.restrictedUserId) : -1) : diff;
        }
        return -1;
    }
    
}
