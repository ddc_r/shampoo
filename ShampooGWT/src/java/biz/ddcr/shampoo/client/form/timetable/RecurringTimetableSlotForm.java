/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.timetable;

import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class RecurringTimetableSlotForm extends TimetableSlotForm implements Serializable {

    public static enum RECURRING_EVENT implements I18nFriendlyInterface, Serializable {
        daily,
        weekly,
        monthly,
        yearly;

        private RECURRING_EVENT() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case daily: return I18nTranslator.getInstance().daily();
                case weekly: return I18nTranslator.getInstance().weekly();
                case monthly: return I18nTranslator.getInstance().monthly();
                case yearly: return I18nTranslator.getInstance().yearly();
                default: return I18nTranslator.getInstance().unknown();
            }
        }
    }

    private RECURRING_EVENT recurringEvent;
    private YearMonthDayInterface decommissioningTime;

    public YearMonthDayInterface getDecommissioningTime() {
        return decommissioningTime;
    }

    public void setDecommissioningTime(YearMonthDayInterface date) {
        this.decommissioningTime = date;
    }

    public RECURRING_EVENT getRecurringEvent() {
        if (recurringEvent==null) recurringEvent = RECURRING_EVENT.daily;
        return recurringEvent;
    }

    public void setRecurringEvent(RECURRING_EVENT recurringEvent) {
        this.recurringEvent = recurringEvent;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RecurringTimetableSlotForm other = (RecurringTimetableSlotForm) obj;
        if (!super.equals(obj)) {
            return false;
        }
        if (this.recurringEvent != other.recurringEvent && (this.recurringEvent == null || !this.recurringEvent.equals(other.recurringEvent))) {
            return false;
        }
        if (this.decommissioningTime != other.decommissioningTime && (this.decommissioningTime == null || !this.decommissioningTime.equals(other.decommissioningTime))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + super.hashCode();
        hash = 71 * hash + (this.recurringEvent != null ? this.recurringEvent.hashCode() : 0);
        hash = 71 * hash + (this.decommissioningTime != null ? this.decommissioningTime.hashCode() : 0);
        return hash;
    }

}
