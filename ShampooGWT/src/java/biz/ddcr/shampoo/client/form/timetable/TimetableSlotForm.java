/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.timetable;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistFormID;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMapEntry;

/**
 *
 * @author okay_awright
 **/
public abstract class TimetableSlotForm extends GenericTimestampedForm {

    public static enum TIMETABLE_TAGS implements TAGS{
        type, enabled, channel, programme, playlist, time;

        private TIMETABLE_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case type: return I18nTranslator.getInstance().type();
                case enabled: return I18nTranslator.getInstance().enabled();
                case channel: return I18nTranslator.getInstance().channel();
                case programme: return I18nTranslator.getInstance().programme();
                case playlist: return I18nTranslator.getInstance().playlist();
                case time: return I18nTranslator.getInstance().time();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                //Unsupported
                case type: return null;
                //Unsupported
                case enabled: return null;
                case channel: return LOOSE_TYPE.alphanumeric;
                case programme: return LOOSE_TYPE.alphanumeric;
                case playlist: return LOOSE_TYPE.alphanumeric;
                //Unsupported
                case time: return null;
                default: return null;
            }
        }
    };

    private TimetableSlotFormID location;
    private ActionCollectionEntry<PlaylistFormID> playlistId;
    private ActionCollectionEntry<String> programmeId;
    private YearMonthDayHourMinuteSecondMillisecondInterface endTime;

    private boolean nowPlaying;

    public YearMonthDayHourMinuteSecondMillisecondInterface getEndTime() {
        return endTime;
    }

    public void setEndTime(YearMonthDayHourMinuteSecondMillisecondInterface time) {
        this.endTime = time;
    }

    public TimetableSlotFormID getRefId() {
        return location;
    }

    public void setRefId(TimetableSlotFormID location) {
        this.location = location;
    }

    public ActionCollectionEntry<String> getPlaylistId() {
        return playlistId!=null ? new ActionMapEntry<String>(playlistId.getItem().getRefID(), playlistId.getValue()) : null;
    }

    public ActionCollectionEntry<String> getPlaylistFriendlyId() {
        return playlistId!=null ? new ActionMapEntry<String>(playlistId.getItem().getFriendlyID(), playlistId.getValue()) : null;
    }

    public void setPlaylistId(ActionCollectionEntry<PlaylistFormID> playlistId) {
        this.playlistId = playlistId;
    }
    public ActionCollectionEntry<PlaylistFormID> getPlaylistFormId() {
        return playlistId;
    }

    public ActionCollectionEntry<String> getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(ActionCollectionEntry<String> programmeId) {
        this.programmeId = programmeId;
    }

    public boolean isNowPlaying() {
        return nowPlaying;
    }

    public void setNowPlaying(boolean nowPlaying) {
        this.nowPlaying = nowPlaying;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimetableSlotForm other = (TimetableSlotForm) obj;
        if (this.location != other.location) {
            return false;
        }
        /*if (this.playlistId != other.playlistId && (this.playlistId == null || !this.playlistId.equals(other.playlistId))) {
            return false;
        }
        if ((this.endTime == null) ? (other.endTime != null) : !this.endTime.equals(other.endTime)) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.location != null ? this.location.hashCode() : 0);
        /*hash = 47 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
        hash = 47 * hash + (this.playlistId != null ? this.playlistId.hashCode() : 0);**/
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof TimetableSlotForm) {
            TimetableSlotForm anotherTimetableSlotForm = (TimetableSlotForm)o;
            //Sort by id
            return this.location!=null ? this.location.compareTo(anotherTimetableSlotForm.location) : -1;
        }
        return -1;
    }

}
