/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.timetable;

import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class TimetableSlotFormID implements Serializable, Comparable<TimetableSlotFormID> {

    private String refID;
    private YearMonthDayHourMinuteSecondMillisecondInterface schedulingTime;
    private String channelId;
    private boolean enabled;

    public TimetableSlotFormID() {
    }

    public String getRefID() {
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public YearMonthDayHourMinuteSecondMillisecondInterface getSchedulingTime() {
        return schedulingTime;
    }

    public void setSchedulingTime(YearMonthDayHourMinuteSecondMillisecondInterface time) {
        this.schedulingTime = time;
    }

    public String getFullFriendlyID() {
        return getSchedulingTime().getI18nFullFriendlyString() + " " + I18nTranslator.getInstance().belonging_to() + " " + getChannelId();
    }

    public String getSyntheticFriendlyID() {
        return getSchedulingTime().getI18nSyntheticFriendlyString() + " @ " + getChannelId();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TimetableSlotFormID other = (TimetableSlotFormID) obj;
        if ((this.channelId == null) ? (other.channelId != null) : !this.channelId.equals(other.channelId)) {
            return false;
        }
        if (this.schedulingTime != other.schedulingTime && (this.schedulingTime == null || !this.schedulingTime.equals(other.schedulingTime))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + (this.schedulingTime != null ? this.schedulingTime.hashCode() : 0);
        hash = 61 * hash + (this.channelId != null ? this.channelId.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(TimetableSlotFormID anotherTimetableSlotFormID) {
        //Sort by channel, then by scheduling, finally by id
        int diff = this.channelId!=null ? this.channelId.compareTo(anotherTimetableSlotFormID.channelId) : (anotherTimetableSlotFormID.channelId==null ? 0 : -1);
        if (diff==0) {
            diff = this.schedulingTime!=null ? this.schedulingTime.compareTo(anotherTimetableSlotFormID.schedulingTime) : (anotherTimetableSlotFormID.schedulingTime==null ? 0 : -1);
            return (diff==0) ? (this.refID!=null ? this.refID.compareTo(anotherTimetableSlotFormID.refID) : -1) : diff; 
        }
        return diff;
    }
}
