/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.timetable.log;

import biz.ddcr.shampoo.client.form.journal.LOG_OPERATION;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TimetableLogForm extends LogForm<TIMETABLE_LOG_OPERATION> {

    @Override
    public String getFriendlyTypeCaption() {
        return I18nTranslator.getInstance().timetable();
    }

    public enum TIMETABLE_LOG_OPERATION implements LOG_OPERATION {

        read,
        edit,
        delete,
        add,
        clone,
        shift,
        resize,
        cancel,
        confirm;

        private TIMETABLE_LOG_OPERATION() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case read:
                    return I18nTranslator.getInstance().read();
                case edit:
                    return I18nTranslator.getInstance().update();
                case delete:
                    return I18nTranslator.getInstance().delete();
                case add:
                    return I18nTranslator.getInstance().create();
                case clone:
                    return I18nTranslator.getInstance().clone();
                case shift:
                    return I18nTranslator.getInstance().shift();
                case resize:
                    return I18nTranslator.getInstance().resize();
                case cancel:
                    return I18nTranslator.getInstance().cancel();
                case confirm:
                    return I18nTranslator.getInstance().confirm();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }
    private YearMonthDayHourMinuteSecondMillisecondInterface dateStamp;
    private String channelId;

    private TimetableLogForm() {}

    public TimetableLogForm(YearMonthDayHourMinuteSecondMillisecondInterface dateStamp, String channelId) {
        this.dateStamp = dateStamp;
        this.channelId = channelId;
    }

    @Override
    public String getFriendlyActeeCaption() {
        //return the ID plus the date stamp and channel
        StringBuilder output = new StringBuilder();
        if (dateStamp != null) {
            output.append(dateStamp.getI18nSyntheticFriendlyString());
        }
        if (dateStamp != null && channelId != null) {
            output.append(" @ ");
        }
        if (channelId != null) {
            output.append(channelId);
        }
        if (dateStamp != null || channelId != null) {
            output.append(" [");
        }
        output.append(getActee());
        if (dateStamp != null || channelId != null) {
            output.append("]");
        }

        return output.toString();
    }
}
