/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.i18n.client.NumberFormat;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class AverageVoteModule implements Serializable, IsModuleInterface {

    private static NumberFormat fmtFloat;
    private Float averageScore;
    private long numberOfVotes;

    public String getI18nFriendlyName() {
        if (numberOfVotes<1 || averageScore==null) {
            return I18nTranslator.getInstance().none();
        } else {
             return I18nTranslator.getInstance().format_score(
                     getFmtFloat().format(averageScore),
                     Integer.toString(VOTE.values().length-1),
                     Long.toString(numberOfVotes));
        }
    }

    public static NumberFormat getFmtFloat() {
        if (fmtFloat==null)
            fmtFloat = NumberFormat.getFormat("#.0");
        return fmtFloat;
    }

    public Float getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(Float averageScore) {
        this.averageScore = averageScore;
    }

    public long getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(long numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AverageVoteModule other = (AverageVoteModule) obj;
        if (this.averageScore != other.averageScore && (this.averageScore == null || !this.averageScore.equals(other.averageScore))) {
            return false;
        }
        if (this.numberOfVotes != other.numberOfVotes) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.averageScore != null ? this.averageScore.hashCode() : 0);
        hash = 47 * hash + (int) (this.numberOfVotes ^ (this.numberOfVotes >>> 32));
        return hash;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof AverageVoteModule) {
            AverageVoteModule o2 = (AverageVoteModule)o;
            //Sort by averageScore, then by numberOfVotes
            int diff = this.averageScore != null ? this.averageScore.compareTo(o2.averageScore) : (o2.averageScore == null ? 0 : -1);
            return (diff == 0) ? (int)(this.numberOfVotes - o2.numberOfVotes) : diff;
        }
        return -1;
    }
    
}
