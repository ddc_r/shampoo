/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;


/**
 *
 * A song, a jingle or an advert, whatever: it's an audio track that can be broadcast
 *
 * @author okay_awright
 **/
public interface BroadcastTrackForm extends TrackForm<BroadcastTrackProgrammeModule>, Serializable {

    public static enum BROADCASTABLE_TRACK_TAGS implements TAGS{
        type, enabled, title, author, album, duration, rotationCount, averageVote, myVote, myRequest, genre, yearOfRelease, description, programmes;

        private BROADCASTABLE_TRACK_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case type: return I18nTranslator.getInstance().type();
                case enabled: return I18nTranslator.getInstance().enabled();
                case title: return I18nTranslator.getInstance().title();
                case author: return I18nTranslator.getInstance().author();
                case album: return I18nTranslator.getInstance().album();
                case duration: return I18nTranslator.getInstance().duration();
                case rotationCount: return I18nTranslator.getInstance().rotation_count();
                //Deactivated MySQL +Hibernate bug with aggregate functions
                /*BUG SQL not portable, temporarly removed feature*/
                case averageVote: return null;
                case myVote: return I18nTranslator.getInstance().my_vote();
                case myRequest: return I18nTranslator.getInstance().my_request();
                case genre: return I18nTranslator.getInstance().genre();
                case yearOfRelease: return I18nTranslator.getInstance().release_date();
                case description: return I18nTranslator.getInstance().description();
                case programmes: return I18nTranslator.getInstance().programmes();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                case type: return null;
                case enabled: return null;
                case title: return LOOSE_TYPE.alphanumeric;
                case author: return LOOSE_TYPE.alphanumeric;
                case album: return LOOSE_TYPE.alphanumeric;
                case duration: return LOOSE_TYPE.numeric;
                case rotationCount: return LOOSE_TYPE.numeric;
                /*BUG SQL not portable, temporarly removed feature*/
                //case averageVote: return LOOSE_TYPE.numeric;
                case averageVote: return null;
                case myVote: return LOOSE_TYPE.numeric;
                //Not handled
                case myRequest: return null;
                case genre: return LOOSE_TYPE.alphanumeric;
                case yearOfRelease: return LOOSE_TYPE.numeric;
                case description: return LOOSE_TYPE.alphanumeric;
                case programmes: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }

    };

    public boolean isActivated();

    public void setActivated(boolean activated);

    public boolean isNowPlaying();

    public void setNowPlaying(boolean nowPlaying);

}
