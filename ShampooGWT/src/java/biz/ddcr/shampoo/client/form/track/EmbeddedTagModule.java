/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.track.format.PICTURE_TYPE;

/**
 *
 * @author okay_awright
 **/
public class EmbeddedTagModule extends TagModule implements HasCoverArtInterface {

    private boolean embeddedCoverArt;
    public String coverArtURL;
    public PICTURE_TYPE coverArtFormat;

    public void setEmbeddedCoverArt(boolean embeddedCoverArt) {
        this.embeddedCoverArt = embeddedCoverArt;
    }

    public boolean hasPicture() {
        return embeddedCoverArt;
    }

    public void setCoverArtURL(String coverArtURL) {
        this.coverArtURL = coverArtURL;
    }

    @Override
    public String getCoverArtDownloadURL() {
        return coverArtURL;
    }

    public void setCoverArtFormat(PICTURE_TYPE coverArtFormat) {
        this.coverArtFormat = coverArtFormat;
    }

    @Override
    public PICTURE_TYPE getCoverArtFormat() {
        return coverArtFormat;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + super.hashCode();
        hash = 53 * hash + (this.embeddedCoverArt ? 1 : 0);
        hash = 53 * hash + (this.coverArtURL != null ? this.coverArtURL.hashCode() : 0);
        hash = 53 * hash + (this.coverArtFormat != null ? this.coverArtFormat.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmbeddedTagModule other = (EmbeddedTagModule) obj;
        if (!super.equals(obj)) {
            return false;
        }
        if (this.embeddedCoverArt != other.embeddedCoverArt) {
            return false;
        }
        if ((this.coverArtURL == null) ? (other.coverArtURL != null) : !this.coverArtURL.equals(other.coverArtURL)) {
            return false;
        }
        if (this.coverArtFormat != other.coverArtFormat) {
            return false;
        }
        return true;
    }

}
