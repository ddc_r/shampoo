/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class PEGIRatingModule implements Serializable, IsModuleInterface/*<PEGIRatingModule>*/ {

    public enum PEGI_AGE implements I18nFriendlyInterface, Serializable {

        earlyChildhood(3),
        everyone(7),
        teen(12),
        mature(16),
        adultsOnly(18);
        private final int age;

        PEGI_AGE(int age) {
            this.age = age;
        }

        public int getAge() {
            return age;
        }

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case earlyChildhood: return I18nTranslator.getInstance().advisory_early_childhood();
                case everyone: return I18nTranslator.getInstance().advisory_everyone();
                case teen: return I18nTranslator.getInstance().advisory_teen();
                case mature: return I18nTranslator.getInstance().advisory_mature();
                case adultsOnly: return I18nTranslator.getInstance().advisory_adults_only();
                default: return I18nTranslator.getInstance().unknown();
            }
        }
    }

    private PEGI_AGE age;
    private Boolean violence;
    private Boolean profanity;
    private Boolean fear;
    private Boolean sex;
    private Boolean drugs;
    private Boolean discrimination;

    public PEGIRatingModule() {
        //GWT serialization
    }

    public PEGI_AGE getRating() {
        return age;
    }

    public void setRating(PEGI_AGE approximateAge) {
        this.age = approximateAge;
    }

    public void setEarlyChildhood() {
        this.age = PEGI_AGE.earlyChildhood;
    }

    public void setEveryone() {
        this.age = PEGI_AGE.everyone;
    }

    public void setTeen() {
        this.age = PEGI_AGE.teen;
    }

    public void setMature() {
        this.age = PEGI_AGE.mature;
    }

    public void setAdultsOnly() {
        this.age = PEGI_AGE.adultsOnly;
    }

    public Boolean hasDiscrimination() {
        return discrimination;
    }

    public void setDiscrimination(Boolean discrimination) {
        this.discrimination = discrimination;
    }

    public Boolean hasDrugs() {
        return drugs;
    }

    public void setDrugs(Boolean drugs) {
        this.drugs = drugs;
    }

    public Boolean hasFear() {
        return fear;
    }

    public void setFear(Boolean fear) {
        this.fear = fear;
    }

    public Boolean hasProfanity() {
        return profanity;
    }

    public void setProfanity(Boolean profanity) {
        this.profanity = profanity;
    }

    public Boolean hasSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Boolean hasViolence() {
        return violence;
    }

    public void setViolence(Boolean violence) {
        this.violence = violence;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PEGIRatingModule other = (PEGIRatingModule) obj;
        if (this.age != other.age) {
            return false;
        }
        if (this.violence != other.violence && (this.violence == null || !this.violence.equals(other.violence))) {
            return false;
        }
        if (this.profanity != other.profanity && (this.profanity == null || !this.profanity.equals(other.profanity))) {
            return false;
        }
        if (this.fear != other.fear && (this.fear == null || !this.fear.equals(other.fear))) {
            return false;
        }
        if (this.sex != other.sex && (this.sex == null || !this.sex.equals(other.sex))) {
            return false;
        }
        if (this.drugs != other.drugs && (this.drugs == null || !this.drugs.equals(other.drugs))) {
            return false;
        }
        if (this.discrimination != other.discrimination && (this.discrimination == null || !this.discrimination.equals(other.discrimination))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.age != null ? this.age.hashCode() : 0);
        hash = 61 * hash + (this.violence != null ? this.violence.hashCode() : 0);
        hash = 61 * hash + (this.profanity != null ? this.profanity.hashCode() : 0);
        hash = 61 * hash + (this.fear != null ? this.fear.hashCode() : 0);
        hash = 61 * hash + (this.sex != null ? this.sex.hashCode() : 0);
        hash = 61 * hash + (this.drugs != null ? this.drugs.hashCode() : 0);
        hash = 61 * hash + (this.discrimination != null ? this.discrimination.hashCode() : 0);
        return hash;
    }

    /*public boolean isContentEqual(PEGIRatingModule otherForm) {
        if (otherForm==null) return false;
        if (!getRating().equals(otherForm.getRating())) return false;
        if (hasViolence()!=otherForm.hasViolence()) return false;
        if (hasProfanity()!=otherForm.hasProfanity()) return false;
        if (hasFear()!=otherForm.hasFear()) return false;
        if (hasSex()!=otherForm.hasSex()) return false;
        if (hasDrugs()!=otherForm.hasDrugs()) return false;
        if (hasDiscrimination()!=otherForm.hasDiscrimination()) return false;
        return true;
    }*/
    
    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof PEGIRatingModule) {
            PEGIRatingModule o2 = (PEGIRatingModule)o;
            //Sort by rating, then by features
            int diff = this.age != null ? this.age.compareTo(o2.age) : (o2.age == null ? 0 : -1);    
            diff = diff==0 ? (this.violence != null ? this.violence.compareTo(o2.violence) : (o2.violence == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.profanity != null ? this.profanity.compareTo(o2.profanity) : (o2.profanity == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.fear != null ? this.fear.compareTo(o2.fear) : (o2.fear == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.sex != null ? this.sex.compareTo(o2.sex) : (o2.sex == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.drugs != null ? this.drugs.compareTo(o2.drugs) : (o2.drugs == null ? 0 : -1)) : diff;        
            return (diff == 0) ? (this.discrimination != null ? this.discrimination.compareTo(o2.discrimination) : -1) : diff;
        }
        return -1;
    }
}
