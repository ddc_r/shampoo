/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;


/**
 *
 * A song, a jingle or an advert that cannot be broadcast since it was not (yet) approved by a curator
 *
 * @author okay_awright
 **/
public class PendingTrackForm extends GenericTimestampedForm implements TrackForm<PendingTrackProgrammeModule>, Serializable {

    private TYPE type;

    private String refID;

    private TagModule metadata;

    private ActionCollectionEntry<ContainerModule> trackFile;
    private ActionCollectionEntry<CoverArtModule> coverArtFile;

    private ActionCollection<PendingTrackProgrammeModule> programmes;

    private boolean ready;

    public static enum PENDING_TRACK_TAGS implements TAGS{
        type, title, author, album, duration, genre, yearOfRelease, comment, description, programmes;

        private PENDING_TRACK_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case type: return I18nTranslator.getInstance().type();
                case title: return I18nTranslator.getInstance().title();
                case author: return I18nTranslator.getInstance().author();
                case album: return I18nTranslator.getInstance().album();
                case duration: return I18nTranslator.getInstance().duration();
                case genre: return I18nTranslator.getInstance().genre();
                case comment: return I18nTranslator.getInstance().comment();
                case yearOfRelease: return I18nTranslator.getInstance().release_date();
                case description: return I18nTranslator.getInstance().description();
                case programmes: return I18nTranslator.getInstance().programmes();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                //Unsupported
                case type: return null;
                case title: return LOOSE_TYPE.alphanumeric;
                case author: return LOOSE_TYPE.alphanumeric;
                case album: return LOOSE_TYPE.alphanumeric;
                case duration: return LOOSE_TYPE.numeric;
                case genre: return LOOSE_TYPE.alphanumeric;
                case comment: return LOOSE_TYPE.alphanumeric;
                case yearOfRelease: return LOOSE_TYPE.numeric;
                case description: return LOOSE_TYPE.alphanumeric;
                case programmes: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }

    };

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    @Override
    public String getRefID() {
        return refID;
    }

    @Override
    public void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public String getFriendlyID() {
        return getMetadata().getAuthor() + " - " + getMetadata().getTitle() + (getRefID()!=null ? " [" + getRefID() + "]" : "");
    }

    @Override
    public String getFriendlyCaption() {
        return getMetadata().getAuthor() + " - " + getMetadata().getTitle();
    }

    @Override
    public TagModule getMetadata() {
        if (metadata==null) metadata = new TagModule();
        return metadata;
    }

    @Override
    public void setMetadata(TagModule metadata) {
        this.metadata = metadata;
    }

    @Override
    public ActionCollection<PendingTrackProgrammeModule> getTrackProgrammes() {
        if (programmes==null) programmes = new ActionSortedMap<PendingTrackProgrammeModule>();
        return programmes;
    }

    @Override
    public void setTrackProgrammes(ActionCollection<PendingTrackProgrammeModule> programmes) {
        this.programmes = programmes;
    }

    @Override
    public ActionCollection<String> getImmutableProgrammes() {
        ActionCollection<String> rawProgrammes = new ActionSortedMap<String>();
        for (ActionCollectionEntry<PendingTrackProgrammeModule> trackProgrammeEntry : getTrackProgrammes()) {
            rawProgrammes.merge(
                            trackProgrammeEntry.getItem().getProgramme(),
                            trackProgrammeEntry.getValue()
                        );
        }
        return rawProgrammes;
    }

    @Override
    public void removeProgrammes(Collection<String> programmes) {
        if (programmes!=null && !programmes.isEmpty()) {
            Iterator<ActionCollectionEntry<PendingTrackProgrammeModule>> i =  getTrackProgrammes().iterator();
            ActionCollectionEntry<PendingTrackProgrammeModule> trackProgrammeEntry;
            while (i.hasNext()) {
                trackProgrammeEntry = i.next();
                if (programmes.contains(trackProgrammeEntry.getItem().getProgramme())) {
                    i.remove();
                }
            }
        }
    }

    @Override
    public ActionCollectionEntry<ContainerModule> getTrackFile() {
        return trackFile;
    }

    @Override
    public void setTrackFile(ActionCollectionEntry<ContainerModule> file) {
        this.trackFile = file;
    }

    @Override
    public ActionCollectionEntry<CoverArtModule> getCoverArtFile() {
        return coverArtFile;
    }

    @Override
    public void setCoverArtFile(ActionCollectionEntry<CoverArtModule> file) {
        this.coverArtFile = file;
    }

    @Override
    public boolean hasPicture() {
        return getCoverArtFile()!=null && getCoverArtFile().getItem()!=null;
    }

    @Override
    public boolean hasAudio() {
        return getTrackFile()!=null && getTrackFile().getItem()!=null;
    }

    @Override
    public PEGIRatingModule getAdvisory() {
        return getMetadata().getAdvisory();
    }

    @Override
    public String getAlbum() {
        return getMetadata().getAlbum();
    }

    @Override
    public String getAuthor() {
        return getMetadata().getAuthor();
    }

    @Override
    public String getPublisher() {
        return getMetadata().getPublisher();
    }
    
    @Override
    public String getCopyright() {
        return getMetadata().getCopyright();
    }

    @Override
    public Short getYearOfRelease() {
        return getMetadata().getYearOfRelease();
    }

    @Override
    public String getDescription() {
        return getMetadata().getDescription();
    }

    @Override
    public String getGenre() {
        return getMetadata().getGenre();
    }

    @Override
    public String getTitle() {
        return getMetadata().getTitle();
    }

    @Override
    public String getTag() {
        return getMetadata().getTag();
    }

    @Override
    public boolean isReady() {
        return ready;
    }

    @Override
    public void setReady(boolean isReady) {
        ready = isReady;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PendingTrackForm other = (PendingTrackForm) obj;
        if ((this.refID == null) ? (other.refID != null) : !this.refID.equals(other.refID)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.refID != null ? this.refID.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof TrackForm) {
            TrackForm anotherTrackForm = (TrackForm)o;
            //Sort by friendly caption, then by id
            String friendlyCaption = this.getFriendlyCaption();
            int diff = friendlyCaption!=null ? friendlyCaption.compareTo(anotherTrackForm.getFriendlyCaption()) : (anotherTrackForm.getFriendlyCaption()==null ? 0 : -1);
            return diff==0 ? (this.refID!=null ? this.refID.compareTo(anotherTrackForm.getRefID()) : (anotherTrackForm.getRefID()==null ? 0 : -1)) : diff;
        }
        return -1;
    }
    
}
