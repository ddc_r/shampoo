/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import java.io.Serializable;


/**
 *
 * A user request
 *
 * @author okay_awright
 **/
public class RequestModule implements Serializable, IsModuleInterface {

    protected String channel;
    protected String user;
    protected String message;
    protected boolean streaming;
    protected boolean queued;

    public RequestModule() {
        //Mandatory for GWT marshalling
    }

    public RequestModule(String channel, String user, String message, boolean streaming, boolean queued) {
        this.user = user;
        this.channel = channel;
        this.message = message;
        this.streaming = streaming;
        this.queued = queued;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }

    public boolean isQueued() {
        return queued;
    }

    public boolean isStreaming() {
        return streaming;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RequestModule other = (RequestModule) obj;
        if ((this.channel == null) ? (other.channel != null) : !this.channel.equals(other.channel)) {
            return false;
        }
        if ((this.user == null) ? (other.user != null) : !this.user.equals(other.user)) {
            return false;
        }
        if ((this.message == null) ? (other.message != null) : !this.message.equals(other.message)) {
            return false;
        }
        if (this.streaming != other.streaming) {
            return false;
        }
        if (this.queued != other.queued) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.channel != null ? this.channel.hashCode() : 0);
        hash = 17 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 17 * hash + (this.message != null ? this.message.hashCode() : 0);
        hash = 17 * hash + (this.streaming ? 1 : 0);
        hash = 17 * hash + (this.queued ? 1 : 0);
        return hash;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof RequestModule) {
            RequestModule o2 = (RequestModule)o;
            //Sort by channel, then by user, then by message
            int diff = this.channel != null ? this.channel.compareTo(o2.channel) : (o2.channel == null ? 0 : -1);    
            diff = diff==0 ? (this.user != null ? this.user.compareTo(o2.user) : (o2.user == null ? 0 : -1)) : diff;
            return (diff == 0) ? (this.message != null ? this.message.compareTo(o2.message) : -1) : diff;
        }
        return -1;
    }
    
}
