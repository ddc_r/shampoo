/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.track;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * A broadcastSongForm with all votes and requests from users
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class RollbackBroadcastSongForm extends BroadcastSongForm implements RollbackBroadcastTrackForm, Serializable {

    private Collection<VoteModule> votes;
    private Collection<RequestModule> requests;

    public Collection<RequestModule> getRequests() {
        return requests;
    }

    public void setRequests(Collection<RequestModule> requests) {
        this.requests = requests;
    }

    public Collection<VoteModule> getVotes() {
        return votes;
    }

    public void setVotes(Collection<VoteModule> votes) {
        this.votes = votes;
    }

    //Dummy value
    @Override
    public boolean isNowPlaying() {
        return true;
    }
    //Dummy value
    @Override
    public void setNowPlaying(boolean nowPlaying) {
        //Do nothing
    }
    
}
