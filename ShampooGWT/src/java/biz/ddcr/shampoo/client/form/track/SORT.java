/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public enum SORT implements I18nFriendlyInterface, Serializable {

    submissionDateAsc,
    submissionDateDesc,
    releaseDateAsc,
    releaseDateDesc,
    rotationCountAsc,
    rotationCountDesc,
    averageRatingAsc,
    averageRatingDesc;

    private SORT() {
    }

    @Override
    public String getI18nFriendlyString() {
        switch (this) {
            case submissionDateAsc:
                return I18nTranslator.getInstance().submission_date_asc();
            case submissionDateDesc:
                return I18nTranslator.getInstance().submission_date_desc();
            case releaseDateAsc:
                return I18nTranslator.getInstance().release_date_asc();
            case releaseDateDesc:
                return I18nTranslator.getInstance().release_date_desc();
            case rotationCountAsc:
                return I18nTranslator.getInstance().rotation_count_asc();
            case rotationCountDesc:
                return I18nTranslator.getInstance().rotation_count_desc();
            case averageRatingAsc:
                return I18nTranslator.getInstance().average_rating_asc();
            case averageRatingDesc:
                return I18nTranslator.getInstance().average_rating_desc();
            default:
                return I18nTranslator.getInstance().unknown();
        }
    }
}
