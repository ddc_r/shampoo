/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class TagModule implements Serializable, CommonTagInterface, IsModuleInterface/*<TagModule>*/ {

    private String title;
    private String author;
    private String album;
    private String publisher;
    private String genre;
    private Short yearOfRelease;
    private String description;
    /** user-defined field */
    private String tag;
    private String copyright;
    private PEGIRatingModule advisory;

    @Override
    public PEGIRatingModule getAdvisory() {
        return advisory;
    }

    public void setAdvisory(PEGIRatingModule advisory) {
        this.advisory = advisory;
    }

    @Override
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }    
    
    @Override
    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    @Override
    public Short getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(Short yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TagModule other = (TagModule) obj;
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.author == null) ? (other.author != null) : !this.author.equals(other.author)) {
            return false;
        }
        if ((this.album == null) ? (other.album != null) : !this.album.equals(other.album)) {
            return false;
        }
        if ((this.publisher == null) ? (other.publisher != null) : !this.publisher.equals(other.publisher)) {
            return false;
        }
        if ((this.genre == null) ? (other.genre != null) : !this.genre.equals(other.genre)) {
            return false;
        }
        if (this.yearOfRelease != other.yearOfRelease && (this.yearOfRelease == null || !this.yearOfRelease.equals(other.yearOfRelease))) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if ((this.tag == null) ? (other.tag != null) : !this.tag.equals(other.tag)) {
            return false;
        }
        if ((this.copyright == null) ? (other.copyright != null) : !this.copyright.equals(other.copyright)) {
            return false;
        }
        if (this.advisory != other.advisory && (this.advisory == null || !this.advisory.equals(other.advisory))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 67 * hash + (this.author != null ? this.author.hashCode() : 0);
        hash = 67 * hash + (this.album != null ? this.album.hashCode() : 0);
        hash = 67 * hash + (this.publisher != null ? this.publisher.hashCode() : 0);
        hash = 67 * hash + (this.genre != null ? this.genre.hashCode() : 0);
        hash = 67 * hash + (this.yearOfRelease != null ? this.yearOfRelease.hashCode() : 0);
        hash = 67 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 67 * hash + (this.tag != null ? this.tag.hashCode() : 0);
        hash = 67 * hash + (this.copyright != null ? this.copyright.hashCode() : 0);
        hash = 67 * hash + (this.advisory != null ? this.advisory.hashCode() : 0);
        return hash;
    }

    /*public boolean isContentEqual(TagModule otherForm) {
        if (otherForm==null) return false;
        if (!getTitle().equals(otherForm.getTitle())) return false;
        if (!getAuthor().equals(otherForm.getAuthor())) return false;
        if (!getAlbum().equals(otherForm.getAlbum())) return false;
        if (!getGenre().equals(otherForm.getGenre())) return false;
        if (getYearOfRelease()!=otherForm.getYearOfRelease()) return false;
        if (!getDescription().equals(otherForm.getDescription())) return false;
        if (!getTag().equals(otherForm.getTag())) return false;
        if (!getCopyright().equals(otherForm.getCopyright())) return false;
        if (!getAdvisory().isContentEqual(otherForm.getAdvisory())) return false;
        return true;
    }*/

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof TagModule) {
            TagModule o2 = (TagModule)o;
            
            //Sort by all features
            int diff = this.title != null ? this.title.compareTo(o2.title) : (o2.title == null ? 0 : -1);    
            diff = diff==0 ? (this.author != null ? this.author.compareTo(o2.author) : (o2.author == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.album != null ? this.album.compareTo(o2.album) : (o2.album == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.publisher != null ? this.publisher.compareTo(o2.publisher) : (o2.publisher == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.genre != null ? this.genre.compareTo(o2.genre) : (o2.genre == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.yearOfRelease != null ? this.yearOfRelease.compareTo(o2.yearOfRelease) : (o2.yearOfRelease == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.description != null ? this.description.compareTo(o2.description) : (o2.description == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.tag != null ? this.tag.compareTo(o2.tag) : (o2.tag == null ? 0 : -1)) : diff;
            diff = diff==0 ? (this.copyright != null ? this.copyright.compareTo(o2.copyright) : (o2.copyright == null ? 0 : -1)) : diff;
            return (diff == 0) ? (this.advisory != null ? this.advisory.compareTo(o2.advisory) : -1) : diff;
        }
        return -1;
    }
    
}
