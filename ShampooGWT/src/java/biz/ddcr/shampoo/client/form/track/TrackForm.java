/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.GenericTimestampedFormInterface;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import java.io.Serializable;
import java.util.Collection;


/**
 *
 * A song, a jingle or an advert, whatever: it's an audio track that can be broadcast
 *
 * @author okay_awright
 **/
public interface TrackForm<T extends TrackProgrammeModule> extends CommonTagInterface, GenericTimestampedFormInterface, Serializable {

    public static interface TRACK_TAGS extends TAGS {}

    public void setMetadata(TagModule metadata);

    public TagModule getMetadata();

    public String getRefID();

    public void setRefID(String refID);

    public String getFriendlyID();

    public String getFriendlyCaption();

    public ActionCollectionEntry<ContainerModule> getTrackFile();

    public void setTrackFile(ActionCollectionEntry<ContainerModule> file);

    public ActionCollectionEntry<CoverArtModule> getCoverArtFile();

    public void setCoverArtFile(ActionCollectionEntry<CoverArtModule> file);

    public boolean isReady();

    public void setReady(boolean isReady);

    public ActionCollection<T> getTrackProgrammes();

    public void setTrackProgrammes(ActionCollection<T> trackProgrammes);

    public ActionCollection<String> getImmutableProgrammes();

    public void removeProgrammes(Collection<String> programmes);

    public boolean hasPicture();
    public boolean hasAudio();

}
