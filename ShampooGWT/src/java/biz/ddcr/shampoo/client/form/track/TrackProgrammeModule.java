/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import java.io.Serializable;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class TrackProgrammeModule implements Serializable, IsModuleInterface {

    private String programme;

    public TrackProgrammeModule() {
        //Empty constructor; mandatory for GWT marshalling
    }

    public TrackProgrammeModule(String programme) {
        setProgramme(programme);
    }

    public String getProgramme() {
        return programme;
    }
    public void setProgramme(String programme) {
        this.programme = programme;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrackProgrammeModule other = (TrackProgrammeModule) obj;
        if ((this.programme == null) ? (other.programme != null) : !this.programme.equals(other.programme)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + (this.programme != null ? this.programme.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o==null) return -1;
        if (o instanceof TrackProgrammeModule) {
            TrackProgrammeModule o2 = (TrackProgrammeModule)o;
            //Sort by programme
            return this.programme != null ? this.programme.compareTo(o2.programme) : -1;
        }
        return -1;
    }
    
}
