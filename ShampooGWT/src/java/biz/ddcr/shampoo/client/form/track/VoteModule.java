/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;


/**
 *
 * A user request
 *
 * @author okay_awright
 **/
public class VoteModule implements Serializable, IsModuleInterface {

    public static enum VOTE implements I18nFriendlyInterface, Serializable {

        bad(0),
        poor(1),
        average(2),
        good(3),
        excellent(4);

        private VOTE() {}

        private VOTE(int score) {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case bad:
                    return I18nTranslator.getInstance().bad();
                case poor:
                    return I18nTranslator.getInstance().poor();
                case average:
                    return I18nTranslator.getInstance().average();
                case good:
                    return I18nTranslator.getInstance().good();
                case excellent:
                    return I18nTranslator.getInstance().excellent();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }

    protected String user;
    protected VOTE vote;

    public VoteModule() {
        //Mandatory for GWT marshalling
    }

    public VoteModule(String user, VOTE vote) {
        this.user = user;
        this.vote = vote;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public VOTE getVote() {
        return vote;
    }

    public void setVote(VOTE vote) {
        this.vote = vote;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VoteModule other = (VoteModule) obj;
        if ((this.user == null) ? (other.user != null) : !this.user.equals(other.user)) {
            return false;
        }
        if (this.vote != other.vote) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + (this.user != null ? this.user.hashCode() : 0);
        hash = 11 * hash + (this.vote != null ? this.vote.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof VoteModule) {
            VoteModule o2 = (VoteModule)o;
            
            //Sort by user, then by vote
            int diff = this.user != null ? this.user.compareTo(o2.user) : (o2.user == null ? 0 : -1);    
            return (diff == 0) ? (this.vote != null ? this.vote.compareTo(o2.vote) : -1) : diff;
        }
        return -1;
    }
    
}
