/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.track.format;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 *
 */
public enum AUDIO_TYPE implements FILE_TYPE {

    mp3,
    mp4_alac,
    mp4_aac,
    mp4_drmaac,
    native_flac,
    ogg_vorbis;

    private AUDIO_TYPE() {
    }

    @Override
    public String getI18nFriendlyString() {
        switch (this) {
            case mp3:
                return I18nTranslator.getInstance().mp3();
            case mp4_alac:
                return I18nTranslator.getInstance().mp4_alac();
            case mp4_aac:
                return I18nTranslator.getInstance().mp4_aac();
            case mp4_drmaac:
                return I18nTranslator.getInstance().mp4_drmaac();
            case native_flac:
                return I18nTranslator.getInstance().native_flac();
            case ogg_vorbis:
                return I18nTranslator.getInstance().ogg_vorbis();
            default:
                return I18nTranslator.getInstance().unknown();
        }
    }
}
