/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track.format;

import biz.ddcr.shampoo.client.helper.date.JSDuration;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class AllowedContainerModule implements Serializable {

    private Collection<AUDIO_TYPE> allowedFormats;
    /** length in milliseconds */
    private JSDuration minDuration;
    private JSDuration maxDuration;
    /** bitrate in bps */
    private long minBitrate;
    private long maxBitrate;
    /** bitrate in hz */
    private int minSamplerate;
    private int maxSamplerate;
    /** 1 for mono, 2 for stereo, can be more; shouldn't be 0 */
    private boolean acceptMono;
    /** variable bit rate? otherwise could constant or average for example */
    private boolean acceptVBR;

    public AllowedContainerModule() {
    }

    public boolean isAcceptMono() {
        return acceptMono;
    }

    public void setAcceptMono(boolean acceptMono) {
        this.acceptMono = acceptMono;
    }

    public boolean isAcceptVBR() {
        return acceptVBR;
    }

    public void setAcceptVBR(boolean acceptVBR) {
        this.acceptVBR = acceptVBR;
    }

    public Collection<AUDIO_TYPE> getAllowedFormats() {
        if (allowedFormats==null) allowedFormats = new HashSet<AUDIO_TYPE>();
        return allowedFormats;
    }

    public void setAllowedFormats(Collection<AUDIO_TYPE> allowedFormats) {
        this.allowedFormats = allowedFormats;
    }

    public long getMaxBitrate() {
        return maxBitrate;
    }

    public void setMaxBitrate(long maxBitrate) {
        this.maxBitrate = maxBitrate;
    }

    public JSDuration getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(JSDuration maxDuration) {
        this.maxDuration = maxDuration;
    }

    public int getMaxSamplerate() {
        return maxSamplerate;
    }

    public void setMaxSamplerate(int maxSamplerate) {
        this.maxSamplerate = maxSamplerate;
    }

    public long getMinBitrate() {
        return minBitrate;
    }

    public void setMinBitrate(long minBitrate) {
        this.minBitrate = minBitrate;
    }

    public JSDuration getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(JSDuration minDuration) {
        this.minDuration = minDuration;
    }

    public int getMinSamplerate() {
        return minSamplerate;
    }

    public void setMinSamplerate(int minSamplerate) {
        this.minSamplerate = minSamplerate;
    }
    
}
