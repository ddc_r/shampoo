/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.track.format;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.form.track.HasAudioInterface;
import biz.ddcr.shampoo.client.helper.date.JSDuration;

/**
 *
 * @author okay_awright
 **/
public class ContainerModule implements HasAudioInterface, FormatModule {

    private AUDIO_TYPE format;
    /** physical size in bytes */
    private long size;
    /** length in milliseconds */
    private JSDuration duration;
    /** bitrate in bps */
    private long bitrate;
    /** bitrate in hz */
    private int samplerate;
    /** 1 for mono, 2 for stereo, can be more; shouldn't be 0 */
    private byte channels;
    /** variable bit rate? otherwise could constant or average for example */
    private boolean vbr;
    /** URL **/
    private String url;

    public ContainerModule() {
        //GWT serialization
    }

    @Override
    public AUDIO_TYPE getFormat() {
        return format;
    }

    public void setFormat(AUDIO_TYPE format) {
        this.format = format;
    }

    public void setURL(String url) {
        this.url = url;
    }

    @Override
    public String getTrackDownloadURL() {
        return getURL();
    }
    
    @Override
    public String getURL() {
        return url;
    }

    @Override
    public AUDIO_TYPE getTrackFormat() {
        return getFormat();
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public long getSize() {
        return size;
    }

    public long getBitrate() {
        return bitrate;
    }

    public void setBitrate(long bitrate) {
        this.bitrate = bitrate;
    }

    public byte getChannels() {
        return channels;
    }

    public void setChannels(byte channels) {
        this.channels = channels;
    }

    public JSDuration getDuration() {
        return duration;
    }

    public void setDuration(JSDuration duration) {
        this.duration = duration;
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
    }

    public boolean isVbr() {
        return vbr;
    }

    public void setVbr(boolean vbr) {
        this.vbr = vbr;
    }

    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof FormatModule) {
            FormatModule o2 = (FormatModule)o;
            //Sort by format, then by size, then by url
            int diff = this.format != null && this.format.getI18nFriendlyString()!=null ? this.format.getI18nFriendlyString().compareTo(o2.getFormat()!=null ? o2.getFormat().getI18nFriendlyString() : null) : (o2.getFormat() == null ? 0 : -1);
            diff = diff==0 ? (int)(this.size - o2.getSize()) : diff;
            return (diff == 0) ? (this.url != null ? this.url.compareTo(o2.getURL()) : -1) : diff;
        }
        return -1;
    }
    
}
