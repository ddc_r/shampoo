/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.track.format;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 *
 */
public enum PICTURE_TYPE implements FILE_TYPE {

    jpeg,
    gif,
    png,
    wbmp;

    private PICTURE_TYPE() {
    }

    @Override
    public String getI18nFriendlyString() {
        switch (this) {
            case jpeg:
                return I18nTranslator.getInstance().jpeg();
            case gif:
                return I18nTranslator.getInstance().gif();
            case png:
                return I18nTranslator.getInstance().png();
            case wbmp:
                return I18nTranslator.getInstance().wbmp();
            default:
                return I18nTranslator.getInstance().unknown();
        }
    }
}
