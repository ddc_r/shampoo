/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.track.log;

import biz.ddcr.shampoo.client.form.journal.LOG_OPERATION;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TrackLogForm extends LogForm<TRACK_LOG_OPERATION> {

    @Override
    public String getFriendlyTypeCaption() {
        return I18nTranslator.getInstance().track();
    }

    public enum TRACK_LOG_OPERATION implements LOG_OPERATION {

        read,
        edit,
        delete,
        add,
        reject,
        validate;

        private TRACK_LOG_OPERATION() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case read:
                    return I18nTranslator.getInstance().read();
                case edit:
                    return I18nTranslator.getInstance().update();
                case delete:
                    return I18nTranslator.getInstance().delete();
                case add:
                    return I18nTranslator.getInstance().create();
                case reject:
                    return I18nTranslator.getInstance().reject();
                case validate:
                    return I18nTranslator.getInstance().validate();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }

    private String author;
    private String title;

    private TrackLogForm() {}

    public TrackLogForm(String author, String title) {
        this.author = author;
        this.title = title;
    }

    @Override
    public String getFriendlyActeeCaption() {
        //return the ID plus the author-title combo
        StringBuilder output = new StringBuilder();
        if (author!=null)
            output.append(author);
        if (author!=null && title!=null)
            output.append(" - ");
        if (title!=null)
            output.append(title);
        if (author != null || title != null) {
            output.append(" [");
        }
        output.append(getActee());
        if (author != null || title != null) {
            output.append("]");
        }

        return output.toString();
    }
}
