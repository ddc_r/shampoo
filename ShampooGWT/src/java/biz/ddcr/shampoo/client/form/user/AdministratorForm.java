package biz.ddcr.shampoo.client.form.user;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import java.io.Serializable;

/**
 * An individual who can login and actually manage data through the frontend according to his credentials
 *
 * @author okay_awright
 */
public class AdministratorForm extends GenericTimestampedForm implements UserForm, Serializable {

    private String username;
    private String firstAndLastName;
    private boolean enabled;
    private String timezone;
    private String email;

    public AdministratorForm() {
        //Empty constructor; mandatory for GWT marshalling
    }

    @Override
    public String getFirstAndLastName() {
        return firstAndLastName;
    }

    @Override
    public void setFirstAndLastName(String name) {
        firstAndLastName = name;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String login) {
        username = login;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean active) {
        enabled = active;
    }

    @Override
    public boolean isAccountNonExpired() {
        //Not actually used
        return isEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        //Not actually used
        return isEnabled();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //Not actually used
        return isEnabled();
    }

    @Override
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String getTimezone() {
        return timezone;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdministratorForm other = (AdministratorForm) obj;
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        /*if ((this.firstAndLastName == null) ? (other.firstAndLastName != null) : !this.firstAndLastName.equals(other.firstAndLastName)) {
            return false;
        }
        if (this.enabled != other.enabled) {
            return false;
        }
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.username != null ? this.username.hashCode() : 0);
        /*hash = 29 * hash + (this.firstAndLastName != null ? this.firstAndLastName.hashCode() : 0);
        hash = 29 * hash + (this.enabled ? 1 : 0);
        hash = 29 * hash + (this.email != null ? this.email.hashCode() : 0);**/
        return hash;
    }
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof UserForm) {
            UserForm anotherUserForm = (UserForm)o;
            //Sort by login
            return this.username != null  ? this.username.compareTo(anotherUserForm.getUsername()) : -1;
        }
        return -1;
    }
}
