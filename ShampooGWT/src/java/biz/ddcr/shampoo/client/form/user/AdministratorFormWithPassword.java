package biz.ddcr.shampoo.client.form.user;

import java.io.Serializable;

/**
 * An individual who can login and actually manage data through the frontend according to his credentials
 *
 * @author okay_awright
 */
public class AdministratorFormWithPassword extends AdministratorForm implements UserFormWithPassword, Serializable {

    private String password;

    public AdministratorFormWithPassword() {
        //Empty constructor; mandatory for GWT marshalling
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdministratorFormWithPassword other = (AdministratorFormWithPassword) obj;
        if (!super.equals(other)) {
            return false;
        }
        /*if ((this.password == null) ? (other.password != null) : !this.password.equals(other.password)) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + super.hashCode();
        //hash = 37 * hash + (this.password != null ? this.password.hashCode() : 0);
        return hash;
    }

}
