/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.user;

import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class LoginForm implements Serializable, Comparable<LoginForm> {

    private String username;
    private String password;
    private boolean rememberme;

    //Don't drop the empty constructor, GWT needs it for its marshalling compliance
    private LoginForm(){}

    public LoginForm(String username, String password, boolean rememberme) {
        this.username = username;
        this.password = password;
        this.rememberme = rememberme;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberme() {
        return rememberme;
    }

    public void setRememberme(boolean rememberme) {
        this.rememberme = rememberme;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LoginForm other = (LoginForm) obj;
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 17 * hash + (this.password != null ? this.password.hashCode() : 0);
        hash = 17 * hash + (this.rememberme ? 1 : 0);
        return hash;
    }

    @Override
    public int compareTo(LoginForm o) {
        if (o == null) return -1;
        //Sort by username, then by password, then by rememberme
        int diff = this.username != null ? this.username.compareTo(o.username) : (o.username == null ? 0 : -1);    
        diff = diff==0 ? (this.password != null ? this.password.compareTo(o.password) : (o.password == null ? 0 : -1)) : diff;
        return (diff == 0) ? (this.rememberme != o.rememberme ? (this.rememberme ? 1 : -1) : 0) : diff;
    }
    
}
