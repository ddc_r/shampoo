/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.user;

import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class ProfileDetailForm implements Serializable, Comparable<ProfileDetailForm> {

    private String email;
    private String firstAndLastName;
    private String timezone;
    private boolean emailNotification;

    public ProfileDetailForm() {
        //GWT serialization
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstAndLastName() {
        return firstAndLastName;
    }

    public void setFirstAndLastName(String firstAndLastName) {
        this.firstAndLastName = firstAndLastName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public boolean isEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProfileDetailForm other = (ProfileDetailForm) obj;
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }
        if ((this.firstAndLastName == null) ? (other.firstAndLastName != null) : !this.firstAndLastName.equals(other.firstAndLastName)) {
            return false;
        }
        if (this.timezone != other.timezone && (this.timezone == null || !this.timezone.equals(other.timezone))) {
            return false;
        }
        if (this.emailNotification != other.emailNotification) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 47 * hash + (this.firstAndLastName != null ? this.firstAndLastName.hashCode() : 0);
        hash = 47 * hash + (this.timezone != null ? this.timezone.hashCode() : 0);
        hash = 47 * hash + (this.emailNotification ? 1 : 0);
        return hash;
    }

    @Override
    public int compareTo(ProfileDetailForm o) {
        if (o == null) return -1;
        //Sort by email, then by firstAndLastName, then by timezone, then by emailNotification
        int diff = this.email != null ? this.email.compareTo(o.email) : (o.email == null ? 0 : -1);    
        diff = diff==0 ? (this.firstAndLastName != null ? this.firstAndLastName.compareTo(o.firstAndLastName) : (o.firstAndLastName == null ? 0 : -1)) : diff;
        diff = diff==0 ? (this.timezone != null ? this.timezone.compareTo(o.timezone) : (o.timezone == null ? 0 : -1)) : diff;
        return (diff == 0) ? (this.emailNotification != o.emailNotification ? (this.emailNotification ? 1 : -1) : 0) : diff;
    }
    
}
