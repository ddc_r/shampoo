/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.user;

import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class ProfilePasswordForm implements Serializable, Comparable<ProfilePasswordForm> {

    private String oldPassword;
    private String newPassword;

    public ProfilePasswordForm() {
        //GWT Serialization
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProfilePasswordForm other = (ProfilePasswordForm) obj;
        if ((this.oldPassword == null) ? (other.oldPassword != null) : !this.oldPassword.equals(other.oldPassword)) {
            return false;
        }
        if ((this.newPassword == null) ? (other.newPassword != null) : !this.newPassword.equals(other.newPassword)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.oldPassword != null ? this.oldPassword.hashCode() : 0);
        hash = 53 * hash + (this.newPassword != null ? this.newPassword.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(ProfilePasswordForm o) {
        if (o == null) return -1;
        //Sort by oldPassword, then by newPassword
        int diff = this.oldPassword != null ? this.oldPassword.compareTo(o.oldPassword) : (o.oldPassword == null ? 0 : -1);    
        return (diff == 0) ? (this.newPassword != null ? this.newPassword.compareTo(o.newPassword) : (o.newPassword == null ? 0 : -1)) : diff;
    }
    
}
