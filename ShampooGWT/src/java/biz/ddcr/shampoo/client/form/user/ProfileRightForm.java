/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.form.user;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ProfileRightForm implements Serializable, Comparable<ProfileRightForm> {
    private Collection<String> channelAdministratorRights;
    private Collection<String> programmeManagerRights;
    private Collection<String> listenerRights;
    private Collection<String> animatorRights;
    private Collection<String> curatorRights;
    private Collection<String> contributorRights;

    public ProfileRightForm() {
        //GWT Serialization
    }

    public Collection<String> getAnimatorRights() {
        return animatorRights;
    }

    public void setAnimatorRights(Collection<String> animatorRights) {
        this.animatorRights = animatorRights;
    }

    public Collection<String> getContributorRights() {
        return contributorRights;
    }

    public void setContributorRights(Collection<String> contributorRights) {
        this.contributorRights = contributorRights;
    }

    public Collection<String> getCuratorRights() {
        return curatorRights;
    }

    public void setCuratorRights(Collection<String> curatorRights) {
        this.curatorRights = curatorRights;
    }

    public Collection<String> getListenerRights() {
        return listenerRights;
    }

    public void setListenerRights(Collection<String> listenerRights) {
        this.listenerRights = listenerRights;
    }

    public Collection<String> getProgrammeManagerRights() {
        return programmeManagerRights;
    }

    public void setProgrammeManagerRights(Collection<String> programmeManagerRights) {
        this.programmeManagerRights = programmeManagerRights;
    }

    public Collection<String> getChannelAdministratorRights() {
        return channelAdministratorRights;
    }

    public void setChannelAdministratorRights(Collection<String> channelAdministratorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProfileRightForm other = (ProfileRightForm) obj;
        if (this.channelAdministratorRights != other.channelAdministratorRights && (this.channelAdministratorRights == null || !this.channelAdministratorRights.equals(other.channelAdministratorRights))) {
            return false;
        }
        if (this.programmeManagerRights != other.programmeManagerRights && (this.programmeManagerRights == null || !this.programmeManagerRights.equals(other.programmeManagerRights))) {
            return false;
        }
        if (this.listenerRights != other.listenerRights && (this.listenerRights == null || !this.listenerRights.equals(other.listenerRights))) {
            return false;
        }
        if (this.animatorRights != other.animatorRights && (this.animatorRights == null || !this.animatorRights.equals(other.animatorRights))) {
            return false;
        }
        if (this.curatorRights != other.curatorRights && (this.curatorRights == null || !this.curatorRights.equals(other.curatorRights))) {
            return false;
        }
        if (this.contributorRights != other.contributorRights && (this.contributorRights == null || !this.contributorRights.equals(other.contributorRights))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.channelAdministratorRights != null ? this.channelAdministratorRights.hashCode() : 0);
        hash = 59 * hash + (this.programmeManagerRights != null ? this.programmeManagerRights.hashCode() : 0);
        hash = 59 * hash + (this.listenerRights != null ? this.listenerRights.hashCode() : 0);
        hash = 59 * hash + (this.animatorRights != null ? this.animatorRights.hashCode() : 0);
        hash = 59 * hash + (this.curatorRights != null ? this.curatorRights.hashCode() : 0);
        hash = 59 * hash + (this.contributorRights != null ? this.contributorRights.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(ProfileRightForm o) {
        if (o == null) return -1;
        //Sort by hashcode (it's stupid, but otherwise any other check would be too expensive)
        return this.hashCode()-o.hashCode();
    }
    
}
