package biz.ddcr.shampoo.client.form.user;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRightForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRole;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import java.io.Serializable;

/**
 * An individual who can login and actually manage data through the frontend according to his credentials
 *
 * @author okay_awright
 */
public class RestrictedUserForm extends GenericTimestampedForm implements UserForm, Serializable {

    private String username;
    private String firstAndLastName;
    private boolean enabled;
    private String timezone;
    private String email;
    private boolean emailNotification;
    //Rights
    private ActionCollection<String> channelAdministratorRights;
    private ActionCollection<String> programmeManagerRights;
    private ActionCollection<String> listenerRights;
    private ActionCollection<String> animatorRights;
    private ActionCollection<String> curatorRights;
    private ActionCollection<String> contributorRights;

    public RestrictedUserForm() {
        //Empty constructor; mandatory for GWT marshalling
    }

    public boolean isEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    @Override
    public String getFirstAndLastName() {
        return firstAndLastName;
    }

    @Override
    public void setFirstAndLastName(String name) {
        firstAndLastName = name;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String login) {
        username = login;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean active) {
        enabled = active;
    }

    @Override
    public boolean isAccountNonExpired() {
        //Not actually used
        return isEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        //Not actually used
        return isEnabled();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //Not actually used
        return isEnabled();
    }

    @Override
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String getTimezone() {
        return timezone;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public ActionCollection<String> getAnimatorRights() {
        if (animatorRights == null) animatorRights = new ActionSortedMap<String>();
        return animatorRights;
    }

    public void setAnimatorRights(ActionCollection<String> animatorRights) {
        this.animatorRights = animatorRights;
    }

    public ActionCollection<String> getContributorRights() {
        if (contributorRights == null) contributorRights = new ActionSortedMap<String>();
        return contributorRights;
    }

    public void setContributorRights(ActionCollection<String> contributorRights) {
        this.contributorRights = contributorRights;
    }

    public ActionCollection<String> getCuratorRights() {
        if (curatorRights == null) curatorRights = new ActionSortedMap<String>();
        return curatorRights;
    }

    public void setCuratorRights(ActionCollection<String> curatorRights) {
        this.curatorRights = curatorRights;
    }

    public ActionCollection<String> getListenerRights() {
        if (listenerRights == null) listenerRights = new ActionSortedMap<String>();
        return listenerRights;
    }

    public void setListenerRights(ActionCollection<String> listenerRights) {
        this.listenerRights = listenerRights;
    }

    public ActionCollection<String> getProgrammeManagerRights() {
        if (programmeManagerRights == null) programmeManagerRights = new ActionSortedMap<String>();
        return programmeManagerRights;
    }

    public void setProgrammeManagerRights(ActionCollection<String> programmeManagerRights) {
        this.programmeManagerRights = programmeManagerRights;
    }

    public ActionCollection<String> getChannelAdministratorRights() {
        if (channelAdministratorRights == null) channelAdministratorRights = new ActionSortedMap<String>();
        return channelAdministratorRights;
    }

    public void setChannelAdministratorRights(ActionCollection<String> channelAdministratorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
    }

    public ActionCollection<RightForm> getRightForms() {
        ActionMap<RightForm> rightForms = new ActionSortedMap<RightForm>();
        for (ActionCollectionEntry<String> channelEntry : getChannelAdministratorRights())
            rightForms.set(new ChannelRightForm(getUsername(), channelEntry.getKey(), ChannelRole.channelAdministrator), channelEntry.getValue());
        for (ActionCollectionEntry<String> channelEntry : getProgrammeManagerRights())
            rightForms.set(new ChannelRightForm(getUsername(), channelEntry.getKey(), ChannelRole.programmeManager), channelEntry.getValue());
        for (ActionCollectionEntry<String> channelEntry : getListenerRights())
            rightForms.set(new ChannelRightForm(getUsername(), channelEntry.getKey(), ChannelRole.listener), channelEntry.getValue());
        for (ActionCollectionEntry<String> programmeEntry : getAnimatorRights())
            rightForms.set(new ProgrammeRightForm(getUsername(), programmeEntry.getKey(), ProgrammeRole.animator), programmeEntry.getValue());
        for (ActionCollectionEntry<String> programmeEntry : getCuratorRights())
            rightForms.set(new ProgrammeRightForm(getUsername(), programmeEntry.getKey(), ProgrammeRole.curator), programmeEntry.getValue());
        for (ActionCollectionEntry<String> programmeEntry : getContributorRights())
            rightForms.set(new ProgrammeRightForm(getUsername(), programmeEntry.getKey(), ProgrammeRole.contributor), programmeEntry.getValue());
        return rightForms;
    }

    public void setRightForms(ActionCollection<RightForm> rightForms) {
        getChannelAdministratorRights().clear();
        getProgrammeManagerRights().clear();
        getListenerRights().clear();
        getAnimatorRights().clear();
        getCuratorRights().clear();
        getContributorRights().clear();
       for (final ActionCollectionEntry<RightForm> rightFormEntry: rightForms) {
            RightForm rightForm = rightFormEntry.getKey();
            if (rightForm.getRole().getClass()==ChannelRole.class) {
                switch ((ChannelRole)rightForm.getRole()) {
                    case channelAdministrator:
                        getChannelAdministratorRights().merge(rightForm.getDomainId(), rightFormEntry.getValue());
                        break;
                    case programmeManager:
                        getProgrammeManagerRights().merge(rightForm.getDomainId(), rightFormEntry.getValue());
                        break;
                    case listener:
                        getListenerRights().merge(rightForm.getDomainId(), rightFormEntry.getValue());
                        break;
                }
            } else if (rightForm.getRole().getClass()==ProgrammeRole.class) {
                switch ((ProgrammeRole)rightForm.getRole()) {
                    case animator:
                        getAnimatorRights().merge(rightForm.getDomainId(), rightFormEntry.getValue());
                        break;
                    case curator:
                        getCuratorRights().merge(rightForm.getDomainId(), rightFormEntry.getValue());
                        break;
                    case contributor:
                        getContributorRights().merge(rightForm.getDomainId(), rightFormEntry.getValue());
                        break;
                }
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RestrictedUserForm other = (RestrictedUserForm) obj;
        if ((this.username == null) ? (other.username != null) : !this.username.equals(other.username)) {
            return false;
        }
        /*if ((this.firstAndLastName == null) ? (other.firstAndLastName != null) : !this.firstAndLastName.equals(other.firstAndLastName)) {
            return false;
        }
        if (this.enabled != other.enabled) {
            return false;
        }
        if ((this.email == null) ? (other.email != null) : !this.email.equals(other.email)) {
            return false;
        }
        if (this.channelAdministratorRights != other.channelAdministratorRights && (this.channelAdministratorRights == null || !this.channelAdministratorRights.equals(other.channelAdministratorRights))) {
            return false;
        }
        if (this.programmeManagerRights != other.programmeManagerRights && (this.programmeManagerRights == null || !this.programmeManagerRights.equals(other.programmeManagerRights))) {
            return false;
        }
        if (this.listenerRights != other.listenerRights && (this.listenerRights == null || !this.listenerRights.equals(other.listenerRights))) {
            return false;
        }
        if (this.animatorRights != other.animatorRights && (this.animatorRights == null || !this.animatorRights.equals(other.animatorRights))) {
            return false;
        }
        if (this.curatorRights != other.curatorRights && (this.curatorRights == null || !this.curatorRights.equals(other.curatorRights))) {
            return false;
        }**/
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.username != null ? this.username.hashCode() : 0);
        /*hash = 23 * hash + (this.firstAndLastName != null ? this.firstAndLastName.hashCode() : 0);
        hash = 23 * hash + (this.enabled ? 1 : 0);
        hash = 23 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 23 * hash + (this.channelAdministratorRights != null ? this.channelAdministratorRights.hashCode() : 0);
        hash = 23 * hash + (this.programmeManagerRights != null ? this.programmeManagerRights.hashCode() : 0);
        hash = 23 * hash + (this.listenerRights != null ? this.listenerRights.hashCode() : 0);
        hash = 23 * hash + (this.animatorRights != null ? this.animatorRights.hashCode() : 0);
        hash = 23 * hash + (this.curatorRights != null ? this.curatorRights.hashCode() : 0);
        hash = 23 * hash + (this.contributorRights != null ? this.contributorRights.hashCode() : 0);**/
        return hash;
    }
    
    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof UserForm) {
            UserForm anotherUserForm = (UserForm)o;
            //Sort by login
            return this.username!=null ? this.username.compareTo(anotherUserForm.getUsername()) : -1;
        }
        return -1;
    }
}
