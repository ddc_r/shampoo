package biz.ddcr.shampoo.client.form.user;

import biz.ddcr.shampoo.client.form.GenericTimestampedFormInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 * An individual who can login and actually manage data through the frontend according to his credentials
 *
 * @author okay_awright
 */
public interface UserForm extends GenericTimestampedFormInterface, Serializable {

    public static enum USER_TAGS implements TAGS{
        type, username, firstAndLastName, enabled, rights;

        private USER_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case type: return I18nTranslator.getInstance().type();
                case enabled: return I18nTranslator.getInstance().enabled();
                case firstAndLastName: return I18nTranslator.getInstance().first_and_last_name();
                case username: return I18nTranslator.getInstance().username();
                case rights: return I18nTranslator.getInstance().rights();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                //Unsupported
                case type: return null;
                //Unsupported
                case enabled: return null;
                case firstAndLastName: return LOOSE_TYPE.alphanumeric;
                case username: return LOOSE_TYPE.alphanumeric;
                case rights: return LOOSE_TYPE.alphanumeric;
                default: return null;
            }
        }
    };

    public String getFirstAndLastName();

    public void setFirstAndLastName(String name);

    public String getUsername();

    public void setUsername(String login);

    public String getEmail();

    public void setEmail(String email);

    public boolean isEnabled();

    public void setEnabled(boolean active);

    public boolean isAccountNonExpired();

    public boolean isAccountNonLocked();

    public boolean isCredentialsNonExpired();

    public String getTimezone();

    public void setTimezone(String timezone);

}
