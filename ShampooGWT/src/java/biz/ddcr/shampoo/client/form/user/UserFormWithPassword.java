package biz.ddcr.shampoo.client.form.user;

import java.io.Serializable;

/**
 * An individual who can login and actually manage data through the frontend according to his credentials
 *
 * @author okay_awright
 */
public interface UserFormWithPassword extends UserForm, Serializable {

    public String getPassword();

    public void setPassword(String password);

}
