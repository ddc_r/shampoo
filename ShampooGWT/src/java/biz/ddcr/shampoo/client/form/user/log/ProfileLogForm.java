/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.user.log;

import biz.ddcr.shampoo.client.form.journal.LOG_OPERATION;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.user.log.ProfileLogForm.PROFILE_LOG_OPERATION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ProfileLogForm extends LogForm<PROFILE_LOG_OPERATION> {

    @Override
    public String getFriendlyTypeCaption() {
        return I18nTranslator.getInstance().profile();
    }

    public enum PROFILE_LOG_OPERATION implements LOG_OPERATION {

        read,
        edit,
        login,
        logout;

        private PROFILE_LOG_OPERATION() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case read:
                    return I18nTranslator.getInstance().read();
                case edit:
                    return I18nTranslator.getInstance().update();
                case login:
                    return I18nTranslator.getInstance().log_in();
                case logout:
                    return I18nTranslator.getInstance().log_out();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }

    public ProfileLogForm() {}

    @Override
    public String getFriendlyActeeCaption() {
        //nothing to output
        return null;
    }

}
