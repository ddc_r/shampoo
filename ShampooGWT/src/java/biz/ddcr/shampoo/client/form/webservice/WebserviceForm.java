package biz.ddcr.shampoo.client.form.webservice;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 * A programme scheduled on a specific channel
 *
 * @author okay_awright
 **/
public class WebserviceForm extends GenericTimestampedForm implements Serializable {

    public static enum WEBSERVICE_TAGS implements TAGS{
        apiKey, channel, maxFireRate, maxDailyQuota, modules;

        private WEBSERVICE_TAGS() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case apiKey: return I18nTranslator.getInstance().public_key();
                case channel: return I18nTranslator.getInstance().channel();
                case maxFireRate: return I18nTranslator.getInstance().channel();
                case maxDailyQuota: return I18nTranslator.getInstance().channel();
                case modules: return I18nTranslator.getInstance().modules();
                default: return I18nTranslator.getInstance().unknown();
            }
        }

        @Override
        public LOOSE_TYPE getLooseType() {
            switch(this) {
                case apiKey: return LOOSE_TYPE.alphanumeric;
                case channel: return LOOSE_TYPE.alphanumeric;
                case maxFireRate: return LOOSE_TYPE.numeric;
                case maxDailyQuota: return LOOSE_TYPE.numeric;
                //Not supported yet
                case modules: return null;
                default: return null;
            }
        }
    };
    
    private String apiKey;
    private String privateKey;
    private String channel;
    private Long maxFireRate;
    private Long maxDailyQuota;
    private String whitelistRegexp;
    /** Activated features**/
    private boolean nowPlayingModule;
    private boolean comingNextModule;
    private boolean timetableModule;
    private boolean archiveModule;
    private boolean requestModule;
    private boolean voteModule;
  
    public WebserviceForm() {
        //GWT serialization
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isArchiveModule() {
        return archiveModule;
    }

    public void setArchiveModule(boolean archiveModule) {
        this.archiveModule = archiveModule;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public boolean isComingNextModule() {
        return comingNextModule;
    }

    public void setComingNextModule(boolean comingNextModule) {
        this.comingNextModule = comingNextModule;
    }

    public Long getMaxDailyQuota() {
        return maxDailyQuota;
    }

    public void setMaxDailyQuota(Long maxDailyQuota) {
        this.maxDailyQuota = maxDailyQuota;
    }

    public Long getMaxFireRate() {
        return maxFireRate;
    }

    public void setMaxFireRate(Long maxFireRate) {
        this.maxFireRate = maxFireRate;
    }

    public boolean isNowPlayingModule() {
        return nowPlayingModule;
    }

    public void setNowPlayingModule(boolean nowPlayingModule) {
        this.nowPlayingModule = nowPlayingModule;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public boolean isRequestModule() {
        return requestModule;
    }

    public void setRequestModule(boolean requestModule) {
        this.requestModule = requestModule;
    }

    public boolean isTimetableModule() {
        return timetableModule;
    }

    public void setTimetableModule(boolean timetableModule) {
        this.timetableModule = timetableModule;
    }

    public boolean isVoteModule() {
        return voteModule;
    }

    public void setVoteModule(boolean voteModule) {
        this.voteModule = voteModule;
    }

    public String getWhitelistRegexp() {
        return whitelistRegexp;
    }

    public void setWhitelistRegexp(String whitelistRegexp) {
        this.whitelistRegexp = whitelistRegexp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WebserviceForm other = (WebserviceForm) obj;
        if ((this.apiKey == null) ? (other.apiKey != null) : !this.apiKey.equals(other.apiKey)) {
            return false;
        }
        if ((this.channel == null) ? (other.channel != null) : !this.channel.equals(other.channel)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.apiKey != null ? this.apiKey.hashCode() : 0);
        hash = 83 * hash + (this.channel != null ? this.channel.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o==null) return -1;
        if (o instanceof WebserviceForm) {
            WebserviceForm anotherWebserviceForm = (WebserviceForm)o;
            //Sort by apiKey, then by channel
            int diff = this.apiKey!=null ? this.apiKey.compareTo(anotherWebserviceForm.apiKey) : (anotherWebserviceForm.apiKey==null ? 0 : -1);
            return (diff==0) ? (this.channel != null ? this.channel.compareTo(anotherWebserviceForm.channel) : -1) : 0;
        }
        return -1;
    }
    
}
