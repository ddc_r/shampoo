package biz.ddcr.shampoo.client.form.webservice;

import biz.ddcr.shampoo.client.form.IsModuleInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.i18n.client.NumberFormat;
import java.io.Serializable;

/**
 * A webservice metadata wrapper
 *
 * @author okay_awright
 **/
public class WebserviceModule implements Serializable, IsModuleInterface {
   
    private static NumberFormat fmtFloat;
    
    //Read-only properties
    
    private Long past5MinutesHits; // total number of hits for the last 5 minutes
    private Long past24HoursHits; // total number of hits for the past 24 hours
    
    public WebserviceModule() {
        //GWT serialization
    }

    public String getI18nFriendlyName() {
        return I18nTranslator.getInstance().webservice_technical_info(
            getPast5MinutesHits() != null ? WebserviceModule.getFmtFloat().format(getPast5MinutesHits()/300.) : I18nTranslator.getInstance().not_applicable(),
            getPast24HoursHits() != null ? Long.toString(getPast24HoursHits()) : I18nTranslator.getInstance().not_applicable());
    }

    public static NumberFormat getFmtFloat() {
        if (fmtFloat==null)
            fmtFloat = NumberFormat.getFormat("#.0");
        return fmtFloat;
    }    
    
    public WebserviceModule(Long currentFireRate, Long currentDailyNumberOfHits) {
        this.past5MinutesHits = currentFireRate;
        this.past24HoursHits = currentDailyNumberOfHits;
    }

    public Long getPast5MinutesHits() {
        return past5MinutesHits;
    }

    public Long getPast24HoursHits() {
        return past24HoursHits;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WebserviceModule other = (WebserviceModule) obj;
        if (this.past5MinutesHits != other.past5MinutesHits && (this.past5MinutesHits == null || !this.past5MinutesHits.equals(other.past5MinutesHits))) {
            return false;
        }
        if (this.past24HoursHits != other.past24HoursHits && (this.past24HoursHits == null || !this.past24HoursHits.equals(other.past24HoursHits))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.past5MinutesHits != null ? this.past5MinutesHits.hashCode() : 0);        
        hash = 97 * hash + (this.past24HoursHits != null ? this.past24HoursHits.hashCode() : 0);
        return hash;
    }
    
    @Override
    public int compareTo(IsModuleInterface o) {
        if (o == null) return -1;
        if (o instanceof WebserviceModule) {
            WebserviceModule o2 = (WebserviceModule)o;
            //Sort by seat, then by key
            int diff = this.past5MinutesHits != null ? this.past5MinutesHits.compareTo(o2.past5MinutesHits) : -1;
            return (diff == 0) ? (this.past24HoursHits != null ? this.past24HoursHits.compareTo(o2.past24HoursHits) : -1) : diff;
        }
        return -1;
    }
}
