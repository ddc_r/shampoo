/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.form.webservice.log;

import biz.ddcr.shampoo.client.form.journal.LOG_OPERATION;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.webservice.log.WebserviceLogForm.WEBSERVICE_LOG_OPERATION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class WebserviceLogForm extends LogForm<WEBSERVICE_LOG_OPERATION> {

    @Override
    public String getFriendlyTypeCaption() {
        return I18nTranslator.getInstance().webservice();
    }

    public enum WEBSERVICE_LOG_OPERATION implements LOG_OPERATION {

        edit,
        delete,
        add;

        private WEBSERVICE_LOG_OPERATION() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case edit:
                    return I18nTranslator.getInstance().update();
                case delete:
                    return I18nTranslator.getInstance().delete();
                case add:
                    return I18nTranslator.getInstance().create();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
    }

    public WebserviceLogForm() {}

    @Override
    public String getFriendlyActeeCaption() {
        //the apiKey is enough
        return getActee();
    }

}
