/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.helper;

/**
 *
 * Class used to coordinate events between multiple asynchronous calls.
 * Detects when the loop reaches the given maximum value.
 * It must be declared final when shared between asynchronous calls.
 *
 * @author okay_awright
 **/
public class AsyncCounter {

    private final int maxValue;
    private int currentValue;

    public AsyncCounter(int startValue, int maxValue) {
        this.maxValue = maxValue;
        currentValue = startValue;
    }
    public boolean next() {
        incCounter();
        return isDone();
    }
    protected void incCounter() {
        currentValue++;
    }
    public boolean isDone() {
        return (currentValue>=maxValue);
    }
}
