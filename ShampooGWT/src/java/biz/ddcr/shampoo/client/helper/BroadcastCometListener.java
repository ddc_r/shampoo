/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackMessageForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.helper.errors.CometClientException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.core.client.GWT;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;
import java.util.TreeSet;
import org.atmosphere.gwt20.client.Atmosphere;
import org.atmosphere.gwt20.client.AtmosphereCloseHandler;
import org.atmosphere.gwt20.client.AtmosphereErrorHandler;
import org.atmosphere.gwt20.client.AtmosphereMessageHandler;
import org.atmosphere.gwt20.client.AtmosphereOpenHandler;
import org.atmosphere.gwt20.client.AtmosphereReopenHandler;
import org.atmosphere.gwt20.client.AtmosphereRequestConfig;
import org.atmosphere.gwt20.client.AtmosphereResponse;
import org.atmosphere.gwt20.client.GwtRpcClientSerializer;
import org.atmosphere.gwt20.client.GwtRpcSerialTypes;

/**
 *
 * Default listener for reverse-AJAX message-driven facilities
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BroadcastCometListener {

    @GwtRpcSerialTypes({
        FeedbackObjectForm.class,
        FeedbackMessageForm.class
    })
    public static abstract class BroadcastCometMarshaller extends GwtRpcClientSerializer {
    }
    final Atmosphere atmosphere = Atmosphere.create();
    final private BroadcastCometMarshaller rpcSerializer = GWT.create(BroadcastCometMarshaller.class);
    final private AtmosphereRequestConfig rpcRequestConfig;
    private EventBusWithLookup eventBus;

    public BroadcastCometListener(String url, EventBusWithLookup eventBus) {
        this.eventBus = eventBus;

        rpcRequestConfig = AtmosphereRequestConfig.create(rpcSerializer);
        rpcRequestConfig.setUrl(url);
        rpcRequestConfig.setTransport(AtmosphereRequestConfig.Transport.WEBSOCKET);
        rpcRequestConfig.setFallbackTransport(AtmosphereRequestConfig.Transport.LONG_POLLING);
        rpcRequestConfig.setOpenHandler(new AtmosphereOpenHandler() {
            @Override
            public void onOpen(AtmosphereResponse response) {
                BroadcastCometListener.this.onOpen(response);
            }
        });
        rpcRequestConfig.setReopenHandler(new AtmosphereReopenHandler() {
            @Override
            public void onReopen(AtmosphereResponse response) {
                BroadcastCometListener.this.onReopen(response);
            }
        });
        rpcRequestConfig.setErrorHandler(new AtmosphereErrorHandler() {

            @Override
            public void onError(AtmosphereResponse response) {
                BroadcastCometListener.this.onError(response);
            }
        });
        rpcRequestConfig.setCloseHandler(new AtmosphereCloseHandler() {
            @Override
            public void onClose(AtmosphereResponse response) {
                BroadcastCometListener.this.onClose(response);
            }
        });
        rpcRequestConfig.setMessageHandler(new AtmosphereMessageHandler() {
            @Override
            public void onMessage(AtmosphereResponse response) {
                BroadcastCometListener.this.onMessage(response);
            }
        });
        rpcRequestConfig.setFlags(AtmosphereRequestConfig.Flags.enableXDR);
        rpcRequestConfig.setFlags(AtmosphereRequestConfig.Flags.trackMessageLength);
    }

    public void setEventBus(EventBusWithLookup eventBus) {
        this.eventBus = eventBus;
    }

    public void onOpen(AtmosphereResponse response) {
        GWT.log("Comet open");
        //Do nothing
    }

    public void onReopen(AtmosphereResponse response) {
        GWT.log("Comet reopened");
        //Do nothing
    }

    public void onClose(AtmosphereResponse response) {
        GWT.log("Comet closed");
        //Do nothing
    }

    public void onError(AtmosphereResponse response) {
        GWT.log("Comet error detected: " + response);
        if (eventBus != null) {
            eventBus.dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.translateException(//                        
                    new CometClientException(response.getReasonPhrase())
            ));
        }
    }

    /**
     * Filter and sort out reverse-AJAX notifications and translate them into
     * GUI events *
     */
    public void onMessage(AtmosphereResponse response) {
        GWT.log("Comet message received: " + response);
        //Use a treeset so to make sure feedback items are sorted by their timestamp (FIFO)
        Collection<FeedbackObjectForm> validMessages = new TreeSet<FeedbackObjectForm>();
        for (Object message : response.getMessages()) {
            if (message instanceof FeedbackMessageForm) {
                GlobalEventsEnum event = GlobalEventsEnum.DISPLAY_INFO;
                if (((FeedbackMessageForm) message).getType() == FeedbackMessageForm.TYPE.error) {
                    event = GlobalEventsEnum.DISPLAY_ERROR;
                } else if (((FeedbackMessageForm) message).getType() == FeedbackMessageForm.TYPE.warning) {
                    event = GlobalEventsEnum.DISPLAY_WARNING;
                }
                if (eventBus != null) {
                    eventBus.dispatch(event, ((FeedbackMessageForm) message).getText());
                }
            } else if (message instanceof FeedbackObjectForm) {
                //Always display a small info banner when a new notification is incoming, except when the sender is the current user
                if (((FeedbackObjectForm) message).getType() == FeedbackObjectForm.TYPE.notification//
                        && ((FeedbackObjectForm) message).getFlag() == FeedbackObjectForm.FLAG.add//
                        && (((FeedbackObjectForm) message).getSenderId() != null && !((FeedbackObjectForm) message).getSenderId().equals(UserIn.authenticatedUserName))) {
                    if (eventBus != null) {
                        eventBus.dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().new_incoming_notification());
                    }
                }
                validMessages.add((FeedbackObjectForm) message);
            } else {
                GWT.log("Unknown message type received: " + message.getClass());
            }
        }
        if (!validMessages.isEmpty()) {
            if (eventBus != null) {
                eventBus.dispatch(GlobalEventsEnum.MESSAGES_TO_CONTENTBOX, validMessages);
            }
        }
    }

    public void start() {
        GWT.log("Starting comet");
        atmosphere.subscribe(rpcRequestConfig);
    }

    public void stop() {
        GWT.log("Stopping comet");
        atmosphere.unsubscribe();
    }
}
