/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.helper.errors.SessionClosedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GWTCSSFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.PluggableWidgetInterface;
import com.mvp4g.client.event.EventBusWithLookup;

/**
 * Base object for asynchronous RPC and HTTP calls
 *
 * @author okay_awright
 **/
public abstract class ExtendedCallback {

    //The event bus used to broadcast failure messages
    protected EventBusWithLookup eventBus = null;
    //The widget that is used to hold and display the hourglass until the RPC call is finished
    protected PluggableWidgetInterface accessoryContainer = null;
    protected GWTCSSFlowPanel hourglassWidget = null;

    public ExtendedCallback(EventBusWithLookup eventBus) {
        super();
        this.eventBus = eventBus;
        onInit();
    }

    public ExtendedCallback(EventBusWithLookup eventBus, PluggableWidgetInterface accessoryContainer) {
        this(eventBus);
        this.accessoryContainer = accessoryContainer;
        plugHourglassWidgetToContainer();
    }

    public void onFailure(Throwable caught) {
        unplugHourglassWidgetToContainer();

        if (isFailureCatchable(caught)) {
            //Is it a problem with an HTTP session that has timed out?
            if (caught instanceof SessionClosedException) {
                //Force the user to log off and redraw the UI, unless he can be automatically reconnected via cookie, in this case just send a small message notification
                logOut();
            }

            //Exception raised or access denied: update the notification box and stall
            if (eventBus != null) {
                eventBus.dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(caught));
            }
            onError();
        }
    }

    private void logOut() {
        if (eventBus != null) {
            //Rebuild the UI
            //Show the logged out version of the login box
            eventBus.dispatch(GlobalEventsEnum.SHOW_LOGGED_OUT);
            //Refresh the menu
            eventBus.dispatch(GlobalEventsEnum.SHOW_MENU);
            //Close the radio player
            eventBus.dispatch(GlobalEventsEnum.CLOSE_RADIOPLAYER);
            //Then go back to the main index page
            eventBus.dispatch(RedirectionEventsEnum.SHOW_INDEX);
        }
    }

    /**
     * Override to handle whether the failure should be displayed
     * @param caught
     * @return
     **/
    public boolean isFailureCatchable(Throwable caught) {
        //By default, always show it
        return true;
    }

    public void onSuccess() {
        unplugHourglassWidgetToContainer();
    }

    public void onInit() {
        //By default, do nothing
    }

    public void onError() {
        //By default, do nothing
    }

    private void plugHourglassWidgetToContainer() {
        if (accessoryContainer != null) {
            hourglassWidget = new GWTCSSFlowPanel("GWTHourglass");
            accessoryContainer.attachContextualWidget(hourglassWidget);
        } else {
            //If there is no accessorycontainer then the hourglass can been plugged in the rootpanel directly
            if (eventBus != null) {
                eventBus.dispatch(GlobalEventsEnum.SHOW_HOURGLASS);
            }
        }
    }

    private void unplugHourglassWidgetToContainer() {
        if (accessoryContainer != null) {
            accessoryContainer.detachContextualWidget();
        } else {
            //If there is no accessorycontainer then the hourglass may have been plugged in the rootpanel directly
            if (eventBus != null) {
                eventBus.dispatch(GlobalEventsEnum.HIDE_HOURGLASS);
            }
        }
    }
}
