/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.PluggableWidgetInterface;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.mvp4g.client.event.EventBusWithLookup;

/**
 * This extended version of the RequestCallback, it fulfills two main requirements:
 * -Transparently display a notification to the user only when the REST call is still in progress
 * -Automatically handle broadcasting of error messages when communicating with the server
 *
 * @author okay_awright
 **/
public abstract class ExtendedRESTCallback extends ExtendedCallback implements RequestCallback {

    public ExtendedRESTCallback(EventBusWithLookup eventBus) {
        super(eventBus);
    }

    public ExtendedRESTCallback(EventBusWithLookup eventBus, PluggableWidgetInterface accessoryContainer) {
        super(eventBus, accessoryContainer);
    }

    @Override
    public void onError(Request request, Throwable exception) {
        super.onFailure(exception);
    }

    @Override
    public void onResponseReceived(Request request, Response response) {
        super.onSuccess();
        //HTTP Code 2xx == OK
        if (response!=null && (response.getStatusCode()>199 && response.getStatusCode()< 300)) 
            onResult(response);
        else {
            //HTTP Code NOT OK
            //Convert errorCodes into Exceptions
            if (eventBus != null) {
                eventBus.dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(translateErrorCodeIntoException(response.getStatusCode())));
            }
            onError();
        }
    }

    public abstract void onResult(Response response);
    
    public abstract RuntimeException translateErrorCodeIntoException(int errorCode);    
    
}
