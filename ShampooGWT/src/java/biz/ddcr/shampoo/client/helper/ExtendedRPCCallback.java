/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper;

import biz.ddcr.shampoo.client.view.widgets.PluggableWidgetInterface;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.mvp4g.client.event.EventBusWithLookup;

/**
 * This extended version of the AsyncCallback fullfills two main requirements:
 * -Transparently display a notification to the user only when the RPC call is still in progress
 * -Automatically handle brodcasting of error messages when communcating with the server
 *
 * @author okay_awright
 **/
public abstract class ExtendedRPCCallback<T> extends ExtendedCallback implements AsyncCallback<T> {

    public ExtendedRPCCallback(EventBusWithLookup eventBus) {
        super(eventBus);
    }

    public ExtendedRPCCallback(EventBusWithLookup eventBus, PluggableWidgetInterface accessoryContainer) {
        super(eventBus, accessoryContainer);
    }

    @Override
    public void onSuccess(T result) {
        super.onSuccess();
        onResult(result);
    }

    public abstract void onResult(T result);

}
