/*
 *  Copyright (C) 2014 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper;

import com.google.gwt.http.client.URL;

/**
 * Handles URL fragments representing browser history states
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class HistoryPartsContainer {
    final String[] parts;
    public HistoryPartsContainer(String[] parts) {        
        this.parts = decode(parts);
    }
    public HistoryPartsContainer(String history) {
        this.parts = decode(history.split("/+"));
    }
    public String[] getParts() {
        return parts;
    }
    private String[] decode(final String[] encodedTokens) {
        final String[] unencodedTokens = new String[encodedTokens.length];
        for (int i=0; i<encodedTokens.length; i++) {
            //hash is a reserved keyword for GWT history
            unencodedTokens[i] = URL.decodeQueryString(encodedTokens[i]);
        }
        return unencodedTokens;
    }
    
    private String joinURLSafe(String glue, String... s) {
        int k = s.length;
        if (k == 0) {
            return null;
        }
        StringBuilder out = new StringBuilder();
        out.append(s[0]);
        for (int x = 1; x < k; ++x) {
            //hash is a reserved keyword for GWT history
            out.append(glue).append(URL.encodeQueryString(s[x]));
        }
        return out.toString();
    }
    
    @Override
    public String toString() {
        return joinURLSafe("/", parts);
    }
}
