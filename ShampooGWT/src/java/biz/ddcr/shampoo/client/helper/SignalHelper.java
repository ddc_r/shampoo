/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper;

import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.TYPE;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.FLAG;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class SignalHelper {
    //use a linkedHashSet to be sure the order of storing is conserved BUT no duplicate exists
    //Or a list but make sure that conditions are checked within the add() method
    List<MessageEventsEnum> signals = new ArrayList<MessageEventsEnum>();

    public SignalHelper(Collection<FeedbackObjectForm> messages) {
        process(messages);
    }

    public void process(Collection<FeedbackObjectForm> messages) {
        if (messages != null) {
            for (FeedbackObjectForm message : messages) {
                process(message.getType(), message.getObjectId(), message.getFlag(), message.getSenderId());
            }
        }
    }

    public abstract void process(TYPE type, String objectId, FLAG flag, String senderId);

    public void add(MessageEventsEnum signal) {
        //No null values, no duplicate
        if (signal != null && !signals.contains(signal)) {
            signals.add(signal);
        }
    }

    public List<MessageEventsEnum> get() {
        return signals;
    }
}
