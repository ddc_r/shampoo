/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper;

/**
 *
 * Store in the JavaScript thread the name of the currently logged user, null if none
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class UserIn {

    /** Can be static and un-final because it's meant to be translated in JavaScript in which it will produce a legal declaration*/
    public static String authenticatedUserName = null;

    private UserIn() {
    }

}
