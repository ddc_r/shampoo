/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.helper;

import biz.ddcr.shampoo.client.form.GenericTimestampedForm;
import biz.ddcr.shampoo.client.helper.date.YearMonthInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GWTLabelWarning;
import biz.ddcr.shampoo.client.view.widgets.PluggableWidgetInterface;


/**
 *
 * @author okay_awright
 **/
public class Validation {

    private static final String EMAIL_REGEXP =
            "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$";
    private static final int PASSWORD_TOO_SHORT = 6;
    private static final int MAX_TEXT = 256;

    public static String trim(String text) {
        return (text!=null ? text.trim() : null);
    }
    public static String lowercase(String text) {
        return (text!=null ? text.toLowerCase() : null);
    }

    public static String checkMaxText(String text) {
        if (text!=null && text.length()>MAX_TEXT)
            return I18nTranslator.getInstance().text_too_long(String.valueOf(MAX_TEXT));
        else
            return null;
    }

    public static String checkUsername(String username) {
        if (username==null || username.length()<1)
            return I18nTranslator.getInstance().login_empty();
        else
            return checkMaxText(username);
    }

    public static String checkLabel(String label) {
        if (label==null || label.length()<1)
            return I18nTranslator.getInstance().label_empty();
        else
            return checkMaxText(label);
    }

    public static String checkPassword(String password) {
        if (password==null || password.length()<1)
            return I18nTranslator.getInstance().password_empty();
        else if (password.length()<PASSWORD_TOO_SHORT)
            return I18nTranslator.getInstance().weak_password(String.valueOf(PASSWORD_TOO_SHORT));
        else
            return checkMaxText(password);
    }

    public static String checkConfirmationPassword(String password, String confimationPassword) {
        if (confimationPassword==null || confimationPassword.length()<1)
            return I18nTranslator.getInstance().password_empty();
        else if (!password.equals(confimationPassword))
            return I18nTranslator.getInstance().mismatch_password();
        else
            return checkMaxText(confimationPassword);
    }

    public static String checkConfirmationEmail(String email, String confimationEmail) {
        if (confimationEmail==null || confimationEmail.length()<1)
            return I18nTranslator.getInstance().email_empty();
        else if (!email.equals(confimationEmail))
            return I18nTranslator.getInstance().mismatch_email();
        else
            return checkMaxText(confimationEmail);
    }

    public static String checkEmail(String email) {
        if (email==null || email.length()<1)
            return I18nTranslator.getInstance().email_empty();
        else if (!email.matches(EMAIL_REGEXP))
            return I18nTranslator.getInstance().email_incorrect();
        else
            return checkMaxText(email);
    }

    public static String checkTimezone(String timezone) {
        if (timezone==null || timezone.length()<1)
            return I18nTranslator.getInstance().timezone_empty();
        else return null;
    }

    public static String checkDate(YearMonthInterface date) {
        if (date==null) {
            return I18nTranslator.getInstance().date_empty();
        }
        //TODO: check date validity, with very simple methods if possible
        else
            return null;
    }

    public static String checkPositiveIntegerNonZero(Integer value) {
        if (value==null)
            return I18nTranslator.getInstance().positiveintegernonzero_empty();
        else if (value<1)
            return I18nTranslator.getInstance().positiveintegernonzero_incorrect();
        else return null;
    }

    public static String checkEntityIsSet(GenericTimestampedForm entity) {
        if (entity==null)
            return I18nTranslator.getInstance().null_value();
        else
            return null;
    }

    public static String checkCollectionIsSet(Integer collectionEntryCount) {
        if (collectionEntryCount==null || collectionEntryCount<1)
            return I18nTranslator.getInstance().collection_is_empty();
        else
            return null;
    }
    public static String checkCollectionItemIsSet(Boolean itemChecked) {
        if (itemChecked==null || itemChecked==false)
            return I18nTranslator.getInstance().item_is_malformed_or_unset();
        else
            return null;
    }


    public static String checkSecondLengthLessThanDay(Integer milliseconds) {
        if (milliseconds==null)
            return I18nTranslator.getInstance().null_value();
        else if (milliseconds>(24 * 60 * 60))
            return I18nTranslator.getInstance().more_than_a_day();
        else return null;
    }
    public static String checkSecondLengthLessThanWeek(Integer milliseconds) {
        if (milliseconds==null)
            return I18nTranslator.getInstance().null_value();
        else if (milliseconds>(7 * 24 * 60 * 60))
            return I18nTranslator.getInstance().more_than_a_week();
        else return null;
    }
    public static String checkSecondLengthLessThanMonth(Integer milliseconds) {
        if (milliseconds==null)
            return I18nTranslator.getInstance().null_value();
        else if (milliseconds>(31 * 7 * 24 * 60 * 60))
            return I18nTranslator.getInstance().more_than_a_month();
        else return null;
    }
    public static String checkSecondLengthLessThanYear(Integer milliseconds) {
        if (milliseconds==null)
            return I18nTranslator.getInstance().null_value();
        else if (milliseconds>(366 * 24 * 60 * 60))
            return I18nTranslator.getInstance().more_than_a_year();
        else return null;
    }

    /**
     *
     * Binds a String to a specific Widget via its setText() property.
     *
     * Returns true if text has been bound and false if there was no String to bind
     *
     * @param validationError
     * @param labelWidget
     * @return
     **/
    public static boolean bindValidationError(String validationError, PluggableWidgetInterface validationWidget) {
        if (validationError!=null && validationError.length()!=0) {
            validationWidget.attachContextualWidget(new GWTLabelWarning(validationError));
            return true;
        } else {
            validationWidget.detachContextualWidget();
            return false;
        }
    }

    private Validation() {
    }

}
