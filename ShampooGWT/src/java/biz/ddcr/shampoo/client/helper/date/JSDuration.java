/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class JSDuration implements DurationMillisecondsInterface {

    private long milliseconds;

    protected JSDuration() {
    }

    public JSDuration(DurationMillisecondsInterface millisecond) {
        this.milliseconds = millisecond!=null ? millisecond.getMilliseconds() : 0;
    }

    public JSDuration(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public JSDuration(float seconds) {
        this.milliseconds = (long) (seconds * 1000);
    }

    public JSDuration(long hour, byte minute, byte second, short millisecond) {
        milliseconds = 0;

        milliseconds += (hour * 60 * 60 * 1000L);
        if (minute > -1 && minute < 60) {
            milliseconds += (minute * 60 * 1000L);
        }
        if (second > -1 && second < 60) {
            milliseconds += (second * 1000L);
        }
        if (millisecond > -1 && millisecond < 1000) {
            milliseconds += millisecond;
        }
    }

    @Override
    public float getHours() {
        return milliseconds / (float)1000 / (float)60 / (float)60;
    }

    @Override
    public long getMilliseconds() {
        return milliseconds;
    }

    @Override
    public float getMinutes() {
        return milliseconds / (float)1000 / (float)60;
    }

    @Override
    public float getSeconds() {
        return milliseconds / (float)1000;
    }

    @Override
    public String getI18nFriendlyString() {
        long rest = milliseconds;
        long hour = (long) Math.floor(rest / 1000.0 / 60.0 / 60.0);
        String hoursString = Long.toString(hour);
        rest -= (hour * 1000L * 60 * 60);
        long minute = (long) Math.floor(rest / 1000.0 / 60.0);
        String minutesString = Long.toString(minute);
        rest -= (minute * 1000L * 60);
        long second = (long) Math.floor(rest / 1000.0);
        String secondsString = Long.toString(second);
        rest -= (second * 1000L);
        String millisecondsString = Long.toString(rest);
        
        boolean isH = hour>0;
        boolean isM = minute>0;
        boolean isS = second>0;
        boolean isMM = rest>0;
        
        if (!isH && isM && isMM) {
            return I18nTranslator.getInstance().synthetic_duration_minutes_seconds_milliseconds(minutesString, secondsString, millisecondsString);
        } else if (!isH && !isM && isS && isMM) {
            return I18nTranslator.getInstance().synthetic_duration_seconds_milliseconds(secondsString, millisecondsString);
        } else if (!isH && !isM && !isS) {
            return I18nTranslator.getInstance().synthetic_duration_milliseconds(millisecondsString);
        } else if (isH && isS && !isMM) {
            return I18nTranslator.getInstance().synthetic_duration_hours_minutes_seconds(hoursString, minutesString, secondsString);
        } else if (isH && isM && !isS && !isMM) {
            return I18nTranslator.getInstance().synthetic_duration_hours_minutes(hoursString, minutesString);
        } else if (isH && !isM && !isS && !isMM) {
            return I18nTranslator.getInstance().synthetic_duration_hours(hoursString);
        } else if (!isH && isM && isS && !isMM) {
            return I18nTranslator.getInstance().synthetic_duration_minutes_seconds(minutesString, secondsString);
        } else if (!isH && isM && !isS && !isMM) {
            return I18nTranslator.getInstance().synthetic_duration_minutes(minutesString);
        } else if (!isH && !isM && isS && !isMM) {
            return I18nTranslator.getInstance().synthetic_duration_seconds(secondsString);
        }
        //Default: isH && isMM
        return I18nTranslator.getInstance().synthetic_duration_hours_minutes_seconds_milliseconds(hoursString, minutesString, secondsString, millisecondsString);

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSDuration other = (JSDuration) obj;
        if (this.milliseconds != other.milliseconds) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.milliseconds ^ (this.milliseconds >>> 32));
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof JSDuration) {
            //Sort by hour, then by minute
            JSDuration anotherMilliseconds = (JSDuration) o;
            return (int) (milliseconds - anotherMilliseconds.getMilliseconds());
        }
        return 0;
    }

    @Override
    public String toString() {
        return Long.toString(milliseconds);
    }
}
