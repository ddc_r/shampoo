/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class JSHourMinute implements HourMinuteInterface {

    private byte hour;
    private byte minutes;

    protected JSHourMinute() {
    }

    public JSHourMinute(short minutesFromStartOfDay) {
        this(
                (byte) ((minutesFromStartOfDay > -1 && minutesFromStartOfDay < (60 * 24)) ? Math.floor(minutesFromStartOfDay / 60.0) : 0),
                (byte) ((minutesFromStartOfDay > -1 && minutesFromStartOfDay < (60 * 24)) ? minutesFromStartOfDay % 60 : 0));
    }

    public JSHourMinute(byte hour, byte minutes) {
        if (hour > -1 && hour < 24) {
            this.hour = hour;
        } else {
            this.hour = 0;
        }
        if (minutes > -1 && minutes < 60) {
            this.minutes = minutes;
        } else {
            this.minutes = 0;
        }
    }

    public JSHourMinute(HourMinuteInterface hourMinute) {
        this(
                hourMinute!=null?hourMinute.getMinuteFromStartOfDay():0);
    }

    @Override
    public byte getHourOfDay() {
        return hour;
    }

    @Override
    public byte getMinuteOfHour() {
        return minutes;
    }

    @Override
    public short getMinuteFromStartOfDay() {
        return (short) ((hour * 60) + minutes);
    }

    public static String toDoubleDigitIfRequired(Byte digit) {
        String digitString = Byte.toString(digit);
        if (digitString.length() < 2) {
            return "0" + digitString;
        }
        return digitString;
    }

    @Override
    public String getI18nFriendlyString() {
        String hourString = toDoubleDigitIfRequired(hour);
        String minutesString = toDoubleDigitIfRequired(minutes);
        return I18nTranslator.getInstance().short_hour_minute(hourString, minutesString);
    }

    @Override
    public String getI18nDayTimeFriendlyString() {
        return getI18nFriendlyString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSHourMinute other = (JSHourMinute) obj;
        if (this.hour != other.hour) {
            return false;
        }
        if (this.minutes != other.minutes) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.hour;
        hash = 31 * hash + this.minutes;
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof JSHourMinute) {
            //Sort by hour, then by minute
            JSHourMinute anotherHourAndMinutes = (JSHourMinute) o;
            return this.getMinuteFromStartOfDay() - anotherHourAndMinutes.getMinuteFromStartOfDay();
        }
        return 0;
    }

    @Override
    public String toString() {
        return getHourOfDay() + ":" + getMinuteOfHour();
    }
}
