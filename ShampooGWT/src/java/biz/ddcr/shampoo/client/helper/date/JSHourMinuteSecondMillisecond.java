/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.io.Serializable;

/**
 *
 * Embed Year, Month, Day, Hour, Minute, Second, Millisecond fractions of a timestamp with minimal time zone converting features
 * This class does NOT handle daylight saving times, only the corresponding classes on the server do
 *
 * @author okay_awright
 **/
public class JSHourMinuteSecondMillisecond implements I18nFriendlyInterface, Serializable, Comparable, HourMinuteSecondMillisecondInterface {

    private JSHourMinute hourMinute;
    private JSSecondMillisecond secondMillisecond;

    protected JSHourMinuteSecondMillisecond() {}

    public JSHourMinuteSecondMillisecond(byte hour, byte minute, byte second, short millisecond) {
        this.hourMinute = new JSHourMinute(hour, minute);
        this.secondMillisecond = new JSSecondMillisecond(second, millisecond);
    }

    public JSHourMinuteSecondMillisecond(HourMinuteInterface hourMinute, SecondMillisecondInterface secondMilisecond) {
        this.hourMinute = new JSHourMinute(hourMinute);
        this.secondMillisecond = new JSSecondMillisecond(secondMilisecond);
    }

    public JSHourMinuteSecondMillisecond(YearMonthDayHourMinuteSecondMillisecondInterface yearMonthDayHourMinuteSecondMillisecond) {
        this.hourMinute = new JSHourMinute(
                    yearMonthDayHourMinuteSecondMillisecond!=null ? yearMonthDayHourMinuteSecondMillisecond.getHourOfDay() : 0,
                    yearMonthDayHourMinuteSecondMillisecond!=null ? yearMonthDayHourMinuteSecondMillisecond.getMinuteOfHour() : 0
                );
        this.secondMillisecond = new JSSecondMillisecond(
                    yearMonthDayHourMinuteSecondMillisecond!=null ? yearMonthDayHourMinuteSecondMillisecond.getSecondOfMinute() : 0,
                    yearMonthDayHourMinuteSecondMillisecond!=null ? yearMonthDayHourMinuteSecondMillisecond.getMillisecondOfSecond() : 0
                );
    }

    @Override
    public HourMinuteInterface getHourMinute() {
        return hourMinute;
    }
    
    @Override
    public SecondMillisecondInterface getSecondMillisecond() {
        return secondMillisecond;
    }

    @Override
    public byte getHourOfDay() {
        return hourMinute.getHourOfDay();
    }

    @Override
    public byte getMinuteOfHour() {
        return hourMinute.getMinuteOfHour();
    }

    @Override
    public short getMinuteFromStartOfDay() {
        return hourMinute.getMinuteFromStartOfDay();
    }
    
    @Override
    public int getMillisecondOfMinute() {
        return secondMillisecond.getMillisecondOfMinute();
    }

    @Override
    public short getMillisecondOfSecond() {
        return secondMillisecond.getMillisecondOfSecond();
    }

    @Override
    public byte getSecondOfMinute() {
        return secondMillisecond.getSecondOfMinute();
    }
    
    @Override
    public String getI18nFriendlyString() {
        return I18nTranslator.getInstance().short_hour_minute_second_milliseconds(
                getHourMinute().getI18nFriendlyString(),
                secondMillisecond.getI18nFriendlyString());
    }

    @Override
    public String getI18nDayTimeFriendlyString() {
        return getI18nFriendlyString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSHourMinuteSecondMillisecond test = (JSHourMinuteSecondMillisecond) obj;
        return super.equals(test)
                && (hourMinute == test.hourMinute || (hourMinute != null && hourMinute.equals(test.hourMinute)))
                && (secondMillisecond == test.secondMillisecond || (secondMillisecond != null && secondMillisecond.equals(test.secondMillisecond)))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + super.hashCode();
        hash = 41 * hash + (this.hourMinute != null ? this.hourMinute.hashCode() : 0);
        hash = 41 * hash + (this.secondMillisecond != null ? this.secondMillisecond.hashCode() : 0);
        return hash;
    }

    /**
     * Beware that time zones are currently not handled!
     * @param o
     * @return
     **/
    @Override
    public int compareTo(Object o) {
        int diff = 0;
        if (o instanceof JSHourMinuteSecondMillisecond) {
            diff = hourMinute.compareTo(((JSHourMinuteSecondMillisecond)o).hourMinute);
            if (diff==0)
                diff = secondMillisecond.compareTo(((JSHourMinuteSecondMillisecond)o).secondMillisecond);
        } else if (o instanceof JSYearMonthDayHourMinuteSecondMillisecond) {
            diff = hourMinute.compareTo(((JSYearMonthDayHourMinuteSecondMillisecond)o).getHourMinute());
            if (diff==0)
                diff = secondMillisecond.compareTo(((JSYearMonthDayHourMinuteSecondMillisecond)o).getSecondMillisecond());
        }
        return diff;
    }

    @Override
    public String toString() {
        return getHourOfDay() + ":" + getMinuteOfHour() + ":" + getSecondOfMinute() + "." + getMillisecondOfSecond();
    }

}
