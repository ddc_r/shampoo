/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 *
 */
public class JSSecondMillisecond implements SecondMillisecondInterface {

    private byte second;
    private short milliseconds;

    protected JSSecondMillisecond() {
    }

    public JSSecondMillisecond(int millisecondsFromStartOfMinute) {
        this(//
                (byte) ((millisecondsFromStartOfMinute > -1 && millisecondsFromStartOfMinute < (1000 * 60)) ? Math.floor(millisecondsFromStartOfMinute / 1000.0) : 0),//
                (short) ((millisecondsFromStartOfMinute > -1 && millisecondsFromStartOfMinute < (1000 * 60)) ? millisecondsFromStartOfMinute % 1000 : 0));
    }

    public JSSecondMillisecond(byte second, short milliseconds) {
        if (second > -1 && second < 60) {
            this.second = second;
        } else {
            this.second = 0;
        }
        if (milliseconds > -1 && milliseconds < 1000) {
            this.milliseconds = milliseconds;
        } else {
            this.milliseconds = 0;
        }
    }

    public JSSecondMillisecond(SecondMillisecondInterface secondMillisecond) {
        this(
                secondMillisecond != null ? secondMillisecond.getMillisecondOfMinute() : 0);
    }

    @Override
    public byte getSecondOfMinute() {
        return second;
    }

    @Override
    public short getMillisecondOfSecond() {
        return milliseconds;
    }

    @Override
    public int getMillisecondOfMinute() {
        return (second * 1000) + milliseconds;
    }

    public static String toTripleDigitIfRequired(Short digit) {
        String digitString = Short.toString(digit);
        if (digitString.length() == 1) {
            return "00" + digitString;
        }
        if (digitString.length() == 2) {
            return "0" + digitString;
        }
        return digitString;
    }

    @Override
    public String getI18nFriendlyString() {
        String secondString = JSHourMinute.toDoubleDigitIfRequired(second);
        String millisecondString = toTripleDigitIfRequired(milliseconds);
        return I18nTranslator.getInstance().short_second_millisecond(secondString, millisecondString);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSSecondMillisecond other = (JSSecondMillisecond) obj;
        if (this.second != other.second) {
            return false;
        }
        if (this.milliseconds != other.milliseconds) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.second;
        hash = 31 * hash + this.milliseconds;
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof JSSecondMillisecond) {
            //Sort by hour, then by minute
            JSSecondMillisecond anotherHourAndMinutes = (JSSecondMillisecond) o;
            return this.getMillisecondOfMinute() - anotherHourAndMinutes.getMillisecondOfMinute();
        }
        return 0;
    }

    @Override
    public String toString() {
        return getSecondOfMinute() + "." + getMillisecondOfSecond();
    }
}
