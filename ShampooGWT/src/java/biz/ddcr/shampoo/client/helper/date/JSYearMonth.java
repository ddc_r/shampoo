/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * Year + Month date management utility class, with time zone
 * Two limitations worth noticing:
 * - DSTs are NOT handled
 * - UTC is approximated to GMT
 *
 * @author okay_awright
 **/
public class JSYearMonth implements YearMonthInterface {

    private String timeZone;
    private short year;
    private byte month;

    protected JSYearMonth() {
    }

    public JSYearMonth(String timeZone, short year, byte month) {
        if (timeZone == null) {
            timeZone = "UTC";
        }
        this.timeZone = timeZone;
        this.year = year;
        if (month > 0 && month < 13) {
            this.month = month;
        } else {
            this.month = 1;
        }
    }

    public JSYearMonth(YearMonthInterface yearMonth) {
        this(
                yearMonth!=null?yearMonth.getTimeZone():"UTC",
                yearMonth!=null?yearMonth.getYear():1970,
                yearMonth!=null?yearMonth.getMonth():1
                );
    }

    @Override
    public byte getMonth() {
        return month;
    }

    @Override
    public short getYear() {
        return year;
    }

    @Override
    public String getTimeZone() {
        return timeZone;
    }

    @Override
    public String getI18nFriendlyString() {
        return getI18nSyntheticFriendlyString();
    }

    @Override
    public String getI18nFullFriendlyString() {
        String yearString = Short.toString(year);
        String monthString = null;
        switch (month) {
            case 1:
                monthString = I18nTranslator.getInstance().january();
                break;
            case 2:
                monthString = I18nTranslator.getInstance().february();
                break;
            case 3:
                monthString = I18nTranslator.getInstance().march();
                break;
            case 4:
                monthString = I18nTranslator.getInstance().april();
                break;
            case 5:
                monthString = I18nTranslator.getInstance().may();
                break;
            case 6:
                monthString = I18nTranslator.getInstance().june();
                break;
            case 7:
                monthString = I18nTranslator.getInstance().july();
                break;
            case 8:
                monthString = I18nTranslator.getInstance().august();
                break;
            case 9:
                monthString = I18nTranslator.getInstance().september();
                break;
            case 10:
                monthString = I18nTranslator.getInstance().october();
                break;
            case 11:
                monthString = I18nTranslator.getInstance().november();
                break;
            case 12:
                monthString = I18nTranslator.getInstance().december();
                break;
        }

        return I18nTranslator.getInstance().full_year_month(yearString, monthString);
    }

    @Override
    public String getI18nSyntheticFriendlyString() {
        String yearString = Short.toString(year);
        String monthString = Byte.toString(month);
        if (monthString.length() < 2) {
            monthString = "0" + monthString;
        }

        return I18nTranslator.getInstance().synthetic_year_month(yearString, monthString);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSYearMonth other = (JSYearMonth) obj;
        if ((this.timeZone == null) ? (other.timeZone != null) : !this.timeZone.equals(other.timeZone)) {
            return false;
        }
        if (this.year != other.year) {
            return false;
        }
        if (this.month != other.month) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.timeZone != null ? this.timeZone.hashCode() : 0);
        hash = 47 * hash + this.year;
        hash = 47 * hash + this.month;
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof JSYearMonth) {
            int diff = getYear() - ((JSYearMonth)o).getYear();
            if (diff == 0) {
                return getMonth() - ((JSYearMonth)o).getMonth();
            } else {
                return diff;
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return getYear() + "/" + getMonth();
    }
}
