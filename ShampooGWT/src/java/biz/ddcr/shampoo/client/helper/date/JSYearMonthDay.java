/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import java.util.Date;

/**
 *
 * @author okay_awright
 **/
public class JSYearMonthDay extends JSYearMonth implements YearMonthDayInterface {

    private byte dayOfMonth;

    protected JSYearMonthDay() {
    }

    public JSYearMonthDay(String timeZone, short year, byte month, byte dayOfMonth) {
        this(
                new JSYearMonth(timeZone, year, month),
                dayOfMonth);
    }

    public JSYearMonthDay(YearMonthInterface yearMonth, byte dayOfMonth) {
        super(yearMonth);
        if (dayOfMonth > 0 && dayOfMonth < 32) {
            this.dayOfMonth = dayOfMonth;
        } else {
            this.dayOfMonth = 1;
        }
    }

    public JSYearMonthDay(YearMonthDayInterface yearMonthDay) {
        this(
                yearMonthDay!=null?(YearMonthInterface)yearMonthDay:null,
                yearMonthDay!=null?yearMonthDay.getDayOfMonth():(byte)1);
    }

    @Override
    public byte getDayOfMonth() {
        return dayOfMonth;
    }

    /**
     * Get the day number within a week for the Gregorian calendar.
     * Century number needs to be adjusted if BC dates are to be computed
     * Monday is 1 and Sunday is 7.
     * 
     * adapted from original author: sakamoto@sm.sony.co.jp
     * 
     * @return
     **/
    @Override
    public byte getDayOfWeek() {

        int min_century = getYear() / 100;
        int year = getYear() - min_century * 100;

        int shifted_month = getMonth() - 2;
        if (shifted_month < 1) {
            shifted_month += 12;
            year--;
        }

        double abs_day = getDayOfMonth() + Math.floor(2.6 * shifted_month - 0.2) - 2.0 * min_century + year + Math.floor(year / 4.0) + Math.floor(min_century / 4.0);
        //Real modulo, not a remainder
        double unshifted_day = (abs_day < 0) ? (7 - (Math.abs(abs_day) % 7)) : (abs_day % 7);
        return (byte) (unshifted_day == 0 ? 7 : unshifted_day);
    }

    /** Get the index of the week in the the month.
     * http://en.wikipedia.org/wiki/Talk:ISO_week_date#Algorithms
     * Based on the ISO week day numbering: Monday is 1 and Sunday is 7
     * Returns a value greater or equal to 1
     * @return 
     */
    @Override
    public int getWeekOfYear() {
        // getDayOfWeek() : ISO week day (Mon=1 to Sun=7)
        final Date nearestThu = addDays(new Date(getYear(), getMonth()-1, getDayOfMonth()), 4/*ISO thursday day index*/ - getDayOfWeek());
        final int year = nearestThu.getYear();
        final Date jan1 = new Date(year, 0, 1);
        return 1 + dayDiff(nearestThu, jan1) / 7;
    }
    private static Date addDays(final Date sourceDate, final long days) {
        return new Date(sourceDate.getTime() + (days * 86400000 /* milliseconds in day*/));
    }
    private static int dayDiff(final Date firstDate, final Date secondDate) {
        return (int) ((firstDate.getTime() - secondDate.getTime()) / 86400000);
    }
    
    @Override
    public short getDayOfYear() {
        final Date jan1 = new Date(getYear(), 0, 1);
        return (short) dayDiff(new Date(getYear(), getMonth()-1, getDayOfMonth()), jan1);
    }

    public static String getI18nMonthFriendlyName(byte monthNumber) {
        String monthString = null;
        switch (monthNumber) {
            case 1:
                monthString = I18nTranslator.getInstance().january();
                break;
            case 2:
                monthString = I18nTranslator.getInstance().february();
                break;
            case 3:
                monthString = I18nTranslator.getInstance().march();
                break;
            case 4:
                monthString = I18nTranslator.getInstance().april();
                break;
            case 5:
                monthString = I18nTranslator.getInstance().may();
                break;
            case 6:
                monthString = I18nTranslator.getInstance().june();
                break;
            case 7:
                monthString = I18nTranslator.getInstance().july();
                break;
            case 8:
                monthString = I18nTranslator.getInstance().august();
                break;
            case 9:
                monthString = I18nTranslator.getInstance().september();
                break;
            case 10:
                monthString = I18nTranslator.getInstance().october();
                break;
            case 11:
                monthString = I18nTranslator.getInstance().november();
                break;
            case 12:
                monthString = I18nTranslator.getInstance().december();
                break;
        }
        return monthString;
    }

    public static String getI18nDayFriendlyName(byte dayNumber) {
        String dayString = null;
        switch (dayNumber) {
            case 1:
                dayString = I18nTranslator.getInstance().monday();
                break;
            case 2:
                dayString = I18nTranslator.getInstance().tuesday();
                break;
            case 3:
                dayString = I18nTranslator.getInstance().wednesday();
                break;
            case 4:
                dayString = I18nTranslator.getInstance().thursday();
                break;
            case 5:
                dayString = I18nTranslator.getInstance().friday();
                break;
            case 6:
                dayString = I18nTranslator.getInstance().saturday();
                break;
            case 7:
                dayString = I18nTranslator.getInstance().sunday();
                break;
        }
        return dayString;
    }

    public static String getI18nDayNumberFriendlyName(int dayNumber) {
        String dayString = null;
        switch (dayNumber) {
            case 1:
                dayString = I18nTranslator.getInstance().first_index();
                break;
            case 2:
                dayString = I18nTranslator.getInstance().second_index();
                break;
            case 3:
                dayString = I18nTranslator.getInstance().third_index();
                break;
            default:
                dayString = I18nTranslator.getInstance().number_index(Integer.toString(dayNumber));
                break;
        }
        return dayString;
    }

    public String getYearMonthDayI18nFullFriendlyString() {
        String yearString = Short.toString(getYear());
        String monthString = getI18nMonthFriendlyName(getMonth());
        String dayString = Byte.toString(dayOfMonth);

        return I18nTranslator.getInstance().full_year_month_day(yearString, monthString, dayString);
    }

    @Override
    public String getI18nFullFriendlyString() {
        return getYearMonthDayI18nFullFriendlyString();
    }

    public String getYearMonthDayI18nSyntheticFriendlyString() {
        String yearString = Short.toString(getYear());
        String monthString = Byte.toString(getMonth());
        if (monthString.length() < 2) {
            monthString = "0" + monthString;
        }
        String dayString = Byte.toString(dayOfMonth);
        if (dayString.length() < 2) {
            dayString = "0" + dayString;
        }

        return I18nTranslator.getInstance().synthetic_year_month_day(yearString, monthString, dayString);
    }

    @Override
    public String getI18nSyntheticFriendlyString() {
        return getYearMonthDayI18nSyntheticFriendlyString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSYearMonthDay test = (JSYearMonthDay) obj;
        return super.equals(test)
                && (dayOfMonth == test.dayOfMonth)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + super.hashCode();
        hash = 89 * hash + this.dayOfMonth;
        return hash;
    }

    @Override
    public int compareTo(Object o) {
        int diff = super.compareTo(o);
        if (diff==0 && (o instanceof JSYearMonthDay))
            diff = getDayOfMonth() - ((JSYearMonthDay)o).getDayOfMonth();
        return diff;
    }

    @Override
    public String toString() {
        return getYear() + "/" + getMonth() + "/" + getDayOfMonth() + " " + getTimeZone();
    }

}
