/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * Embed Year, Month, Day, Hour, Minute fractions of a timestamp with minimal time zone converting features
 * This class does NOT handle daylight saving times, only the corresponding classes on the server do
 *
 * @author okay_awright
 **/
public class JSYearMonthDayHourMinute extends JSYearMonthDay implements YearMonthDayHourMinuteInterface {

    private JSHourMinute hourMinute;

    protected JSYearMonthDayHourMinute() {}

    public JSYearMonthDayHourMinute(String timeZone, short year, byte month, byte dayOfMonth, byte hour, byte minutes) {
        this(new JSYearMonthDay(timeZone, year, month, dayOfMonth), new JSHourMinute(hour, minutes));
    }
    
    public JSYearMonthDayHourMinute(String timeZone, short year, byte month, byte dayOfMonth, short minutesFromStartOfDay) {
        this(new JSYearMonthDay(timeZone, year, month, dayOfMonth), new JSHourMinute(minutesFromStartOfDay));
    }

    public JSYearMonthDayHourMinute(YearMonthDayInterface yearMonthDay, HourMinuteInterface hourAndMinutes) {
        super(yearMonthDay);
        this.hourMinute = new JSHourMinute(hourAndMinutes);
    }

    public JSYearMonthDayHourMinute(YearMonthDayInterface yearMonthDay) {
        super(yearMonthDay);
        this.hourMinute = new JSHourMinute((short)0);
    }

    public JSYearMonthDayHourMinute(YearMonthDayHourMinuteInterface yearMonthDayHourMinute) {
        super(yearMonthDayHourMinute);
        this.hourMinute = new JSHourMinute(yearMonthDayHourMinute);
    }

    @Override
    public YearMonthDayInterface getYearMonthDay() {
        return new JSYearMonthDay(this);
    }

    @Override
    public JSHourMinute getHourMinute() {
        return hourMinute;
    }

    @Override
    public byte getHourOfDay() {
        return getHourMinute().getHourOfDay();
    }

    @Override
    public byte getMinuteOfHour() {
        return getHourMinute().getMinuteOfHour();
    }

    @Override
    public short getMinuteFromStartOfDay() {
        return getHourMinute().getMinuteFromStartOfDay();
    }

    @Override
    public String getI18nFullFriendlyString() {
        return I18nTranslator.getInstance().year_month_day_hour_minutes(
                getYearMonthDayI18nFullFriendlyString(),
                hourMinute.getI18nFriendlyString(),
                super.getTimeZone());
    }

    @Override
    public String getI18nSyntheticFriendlyString() {
        return I18nTranslator.getInstance().year_month_day_hour_minutes(
                getYearMonthDayI18nSyntheticFriendlyString(),
                hourMinute.getI18nFriendlyString(),
                super.getTimeZone());
    }

    @Override
    public String getI18nDayTimeFriendlyString() {
        return getHourMinute().getI18nFriendlyString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSYearMonthDayHourMinute test = (JSYearMonthDayHourMinute) obj;
        return super.equals(test)
                && (hourMinute == test.hourMinute || (hourMinute != null && hourMinute.equals(test.hourMinute)))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + super.hashCode();
        hash = 41 * hash + (this.hourMinute != null ? this.hourMinute.hashCode() : 0);
        return hash;
    }

    /**
     * Beware that time zones are currently not handled!
     * @param o
     * @return
     **/
    @Override
    public int compareTo(Object o) {
        int diff = super.compareTo(o);
        if (diff==0 && (o instanceof JSYearMonthDayHourMinute))
            diff = getHourMinute().compareTo(((JSYearMonthDayHourMinute)o).getHourMinute());
        return diff;
    }

    @Override
    public String toString() {
        return getYear() + "/" + getMonth() + "/" + getDayOfMonth() + " " + getHourOfDay() + ":" + getMinuteOfHour() + " " + getTimeZone();
    }

}
