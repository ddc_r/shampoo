/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.date;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * Embed Year, Month, Day, Hour, Minute, Second, Millisecond fractions of a
 * timestamp with minimal time zone converting features This class does NOT
 * handle daylight saving times, only the corresponding classes on the server do
 *
 * @author okay_awright
 *
 */
public class JSYearMonthDayHourMinuteSecondMillisecond extends JSYearMonthDayHourMinute implements YearMonthDayHourMinuteSecondMillisecondInterface {

    private JSSecondMillisecond secondMillisecond;

    protected JSYearMonthDayHourMinuteSecondMillisecond() {
    }

    public JSYearMonthDayHourMinuteSecondMillisecond(String timeZone, short year, byte month, byte dayOfMonth, byte hour, byte minute, byte second, short millisecond) {
        super(timeZone, year, month, dayOfMonth, hour, minute);
        secondMillisecond = new JSSecondMillisecond(second, millisecond);
    }

    public JSYearMonthDayHourMinuteSecondMillisecond(YearMonthDayInterface yearMonthDay, HourMinuteInterface hourMinute, SecondMillisecondInterface secondMilisecond) {
        super(yearMonthDay, hourMinute);
        secondMillisecond = new JSSecondMillisecond(secondMilisecond);
    }

    public JSYearMonthDayHourMinuteSecondMillisecond(YearMonthDayHourMinuteSecondMillisecondInterface yearMonthDayHourMinuteSecondMillisecond) {
        super(yearMonthDayHourMinuteSecondMillisecond);
        secondMillisecond = new JSSecondMillisecond(
                yearMonthDayHourMinuteSecondMillisecond != null ? yearMonthDayHourMinuteSecondMillisecond.getSecondOfMinute() : 0,
                yearMonthDayHourMinuteSecondMillisecond != null ? yearMonthDayHourMinuteSecondMillisecond.getMillisecondOfSecond() : 0);
    }

    public JSYearMonthDayHourMinuteSecondMillisecond(YearMonthDayHourMinuteInterface yearMonthDayHourMinute) {
        super(yearMonthDayHourMinute);
        secondMillisecond = new JSSecondMillisecond(0);
    }

    public JSYearMonthDayHourMinuteSecondMillisecond(YearMonthDayInterface yearMonthDay) {
        super(yearMonthDay);
        secondMillisecond = new JSSecondMillisecond(0);
    }

    @Override
    public HourMinuteSecondMillisecondInterface getHourMinuteSecondMillisecond() {
        return new JSHourMinuteSecondMillisecond(getHourMinute(), getSecondMillisecond());
    }

    @Override
    public JSSecondMillisecond getSecondMillisecond() {
        return secondMillisecond;
    }

    @Override
    public int getMillisecondOfMinute() {
        return secondMillisecond.getMillisecondOfMinute();
    }

    @Override
    public short getMillisecondOfSecond() {
        return secondMillisecond.getMillisecondOfSecond();
    }

    @Override
    public byte getSecondOfMinute() {
        return secondMillisecond.getSecondOfMinute();
    }

    @Override
    public String getI18nFullFriendlyString() {
        return I18nTranslator.getInstance().year_month_day_hour_minute_second_milliseconds(
                getYearMonthDayI18nFullFriendlyString(),
                getHourMinute().getI18nFriendlyString(),
                secondMillisecond.getI18nFriendlyString(),
                super.getTimeZone());
    }

    @Override
    public String getI18nSyntheticFriendlyString() {
        return I18nTranslator.getInstance().year_month_day_hour_minute_second_milliseconds(
                getYearMonthDayI18nSyntheticFriendlyString(),
                getHourMinute().getI18nFriendlyString(),
                secondMillisecond.getI18nFriendlyString(),
                super.getTimeZone());
    }

    @Override
    public String getI18nDayTimeFriendlyString() {
        return I18nTranslator.getInstance().short_hour_minute_second_milliseconds(
                getHourMinute().getI18nFriendlyString(),
                secondMillisecond.getI18nFriendlyString());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSYearMonthDayHourMinuteSecondMillisecond test = (JSYearMonthDayHourMinuteSecondMillisecond) obj;
        return super.equals(test)
                && (secondMillisecond == test.secondMillisecond || (secondMillisecond != null && secondMillisecond.equals(test.secondMillisecond)));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + super.hashCode();
        hash = 41 * hash + (this.secondMillisecond != null ? this.secondMillisecond.hashCode() : 0);
        return hash;
    }

    /**
     * Beware that time zones are currently not handled!
     *
     * @param o
     * @return
     *
     */
    @Override
    public int compareTo(Object o) {
        int diff = super.compareTo(o);
        if (diff == 0 && (o instanceof JSYearMonthDayHourMinuteSecondMillisecond)) {
            diff = secondMillisecond.compareTo(((JSYearMonthDayHourMinuteSecondMillisecond) o).secondMillisecond);
        }
        return diff;
    }

    @Override
    public String toString() {
        return getYear() + "/" + getMonth() + "/" + getDayOfMonth() + " " + getHourOfDay() + ":" + getMinuteOfHour() + ":" + getSecondOfMinute() + "." + getMillisecondOfSecond() + " " + getTimeZone();
    }
}
