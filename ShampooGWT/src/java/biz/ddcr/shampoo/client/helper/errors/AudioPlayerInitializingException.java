package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class AudioPlayerInitializingException extends GenericPresentationException {

    public AudioPlayerInitializingException() {
        super();
    }

    public AudioPlayerInitializingException(String message) {
        super(message);
    }

}