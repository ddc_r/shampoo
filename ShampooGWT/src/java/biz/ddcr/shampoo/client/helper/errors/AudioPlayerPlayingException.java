package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class AudioPlayerPlayingException extends GenericPresentationException {

    public AudioPlayerPlayingException() {
        super();
    }

    public AudioPlayerPlayingException(String message) {
        super(message);
    }

}