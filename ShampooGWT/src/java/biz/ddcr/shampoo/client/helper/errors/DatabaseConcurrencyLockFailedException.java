package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class DatabaseConcurrencyLockFailedException extends GenericPresentationException {

    public DatabaseConcurrencyLockFailedException() {
        super();
    }

    public DatabaseConcurrencyLockFailedException(String message) {
        super(message);
    }

}