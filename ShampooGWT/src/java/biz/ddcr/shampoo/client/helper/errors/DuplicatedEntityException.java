package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class DuplicatedEntityException extends GenericPresentationException {

    public DuplicatedEntityException() {
        super();
    }

    public DuplicatedEntityException(String message) {
        super(message);
    }

}