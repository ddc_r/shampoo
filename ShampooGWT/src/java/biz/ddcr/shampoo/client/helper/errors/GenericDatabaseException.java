package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class GenericDatabaseException extends GenericPresentationException {

    public GenericDatabaseException() {
        super();
    }

    public GenericDatabaseException(String message) {
        super(message);
    }

}