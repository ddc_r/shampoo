package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class NoEntityException extends GenericPresentationException {

    public NoEntityException() {
        super();
    }

    public NoEntityException(String message) {
        super(message);
    }

}
