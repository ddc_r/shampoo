package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class TrackFormatException extends GenericPresentationException {

    public TrackFormatException() {
        super();
    }

    public TrackFormatException(String message) {
        super(message);
    }

}