package biz.ddcr.shampoo.client.helper.errors;

/**
 *
 * @author okay_awright
 **/
public class UploadingGenericException extends GenericPresentationException {

    public UploadingGenericException() {
        super();
    }

    public UploadingGenericException(String message) {
        super(message);
    }

}