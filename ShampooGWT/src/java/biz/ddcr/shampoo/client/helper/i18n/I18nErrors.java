/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.helper.i18n;

import com.google.gwt.i18n.client.ConstantsWithLookup;

/**
 *
 * @author okay_awright
 **/
public interface I18nErrors extends ConstantsWithLookup {

//The one and only (fallback if the actual error is not defined herein)
String Exception();

//HTTP Error through
String StatusCodeException();

//Global errors
String IllegalArgumentException();
//Data binding errors
String GenericDatabaseException();
String DatabaseConcurrencyLockFailedException();
//Extended data binding errors
String DuplicatedEntityException();
String NoEntityException();
//Security errors
String AccessDeniedException();
String SessionClosedException();
String UsernameNotFoundException();
String AuthenticationException();
String BadCredentialsException();
String UserDisabledException();
String UserLockedException();
String ConcurrentLoginException();
String LoggingFailedException();
//Date errors
String DataFormatException();
//View filtering errors
String MalformedFilterExpressionException();
//Message communication errors
String MessageException();
String MessageSendException();
String MessageAuthenticateException();

//User
String CannotRemoveHimselfException();

//Timetable slot
String TimetableSlotMalformedException();

//HTTP upload
String UploadingGenericException();
String UploadingCanceledException();
String UploadingMalformedException();
String UploadingNoDataException();
String UploadingNotFoundException();
String UploadingOversizedException();
String UploadingTimedOut();
String UploadingTrackingException();
String UploadingWrongFormatException();
String UploadingWrongFeaturesException();
//HTTP download
String RequestTimeoutException();
String RequestException();

//Track format errors
String TrackBrokenException();
String TrackBitrateTooLowException();
String TrackBitrateTooHighException();
String TrackNotStereoException();
String TrackSamplerateTooLowException();
String TrackSamplerateTooHighException();
String TrackNotCBRException();
String TrackTooShortException();
String TrackTooLongException();

//Track error
String TrackNotReadyException();
String TrackInQueueException();
String TrackNowPlayingException();

//Track request error
String RequestLimitReachedException();
String RequestAlreadyScheduledException();
String NoRequestAllowedException();

//Substitutable variable pattern error
String MalformedTextPatternExpressionException();

//Playlist error
String PlaylistNotReadyException();

//Timetable slot error
String TimetableSlotNowPlayingException();

//Reverse AJAX error
String CometClientException();
String CometUnknownException();

//Malformed URIs
String MalformedURIException();

//Streamer/Channel connection exceptions
String ChannelAlreadyStreamedException();
String SeatAlreadyTakenException();

//Audio player
String AudioPlayerInitializingException();
String AudioPlayerPlayingException();

}
