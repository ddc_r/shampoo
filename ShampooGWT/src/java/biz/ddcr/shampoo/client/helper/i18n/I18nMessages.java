/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.helper.i18n;

import com.google.gwt.i18n.client.Messages;

/**
 *
 * @author okay_awright
 **/
public interface I18nMessages extends Messages {

//Application-related
String greetings();
String versioning();
//#menu
String index_menu();
//Common
//#Actions
String add();
String edit();
String bulk_edit();
String change();
String delete();
String validate();
String reject();
String edit_and_validate();
String edit_and_reject();
String back();
String cancel();
String abort();
String upload();
String download();
String confirm();
String clone();
String update();
String select();
String ignore();
String ok();
String yes();
String yes_to_all();
String no();
String no_to_all();
String all();
String none();
String next();
String previous();
String invert_selection();
String is_true();
String is_false();
String operation();
String log_in();
String log_out();
String sign_up();
String play();
String stop();
String shift_up();
String shift_down();
String shift_top();
String shift_bottom();
//#Misc
String drafts();
String draft();
String working();
String available();
String until();
String bound();
String contains();
String does_not_contain();
String is();
String is_not();
String is_equal_or_lower_than();
String is_not_equal_or_lower_than();
String is_equal_or_greater_than();
String is_not_equal_or_greater_than();
String is_equal_to();
String is_not_equal_to();
String inside_interval();
String outside_interval();
String in_interval_x_to_y_inclusive(String from, String to);
String outside_interval_x_to_y_exclusive(String from, String to);
String belonging_to();
String logged_in_as();
String change_my_profile();
String create_my_profile();
String view_messages();
String my_vote();
String my_request();
String programme_x_on_channel_y(String programme, String channel);
String request();
String withdraw_request();
String queued_up();
String not_applicable();
String unknown();
String undefined();
String connecting();
String disconnected();
String on_air();
String off_air();
String on();
String off();
String online_status();
String infinite();
String remember_me();
String change_my_password();
String forgot_my_password();
String i_remember_my_username();
String i_remember_my_email_address();
String password_sent_to_user_email_address();
String successfully_logged_out();
String successfully_logged_in();
String confirmation();
String do_you_want_to_delete_x(String id);
String do_you_want_to_delete_selected_items();
String do_you_want_to_validate_x(String id);
String do_you_want_to_validate_selected_items();
String do_you_want_to_reject_x(String id);
String do_you_want_to_reject_selected_items();
String do_you_want_to_cancel_x(String id);
String do_you_want_to_cancel_selected_items();
String misc_options();
String type();
String monday();
String tuesday();
String wednesday();
String thursday();
String friday();
String saturday();
String sunday();
String january();
String february();
String march();
String april();
String may();
String june();
String july();
String august();
String september();
String october();
String november();
String december();
String short_hour_minute(String hour, String minute);
String short_second_millisecond(String second, String millisecond);
String short_weekday_hour_minute(String weekday, String hour_minute);
String short_weekday_hour_minute_second_millisecond(String weekday, String hour_minute, String second_millisecond);
String second();
String seconds();
String minute();
String minutes();
String millisecond();
String milliseconds();
String hour();
String hours();
String day();
String days();
String yours();
String month();
String year();
String full_year_month(String year, String month);
String full_year_month_day(String year, String month, String day);
String synthetic_year_month(String year, String month);
String synthetic_year_month_day(String year, String month, String day);
String year_month_day_hour_minutes(String yearMonthDay, String hourMinutes, String timeZone);
String year_month_day_hour_minute_second_milliseconds(String yearMonthDay, String hourMinute, String SecondMilliseconds, String timeZone);
String short_hour_minute_second_milliseconds(String hourMinute, String SecondMilliseconds);
String synthetic_duration_hours_minutes_seconds_milliseconds(String hours, String minutes, String seconds, String milliseconds);
String synthetic_duration_hours_minutes_seconds(String hours, String minutes, String seconds);
String synthetic_duration_hours_minutes(String hours, String minutes);
String synthetic_duration_hours(String hours);
String synthetic_duration_minutes_seconds_milliseconds(String minutes, String seconds, String milliseconds);
String synthetic_duration_minutes_seconds(String minutes, String seconds);
String synthetic_duration_minutes(String minutes);
String synthetic_duration_seconds_milliseconds(String seconds, String milliseconds);
String synthetic_duration_seconds(String seconds);
String synthetic_duration_milliseconds(String milliseconds);
String week_year(String week, String year);
String pick_date();
String next_week();
String previous_week();
String random();
String weighted_random();
String ordered();
String random_selection();
String weighted_random_selection();
String ordered_selection();
String submission_date_asc();
String submission_date_desc();
String release_date_asc();
String release_date_desc();
String rotation_count_asc();
String rotation_count_desc();
String average_rating_asc();
String average_rating_desc();
String feature();
String alphabetical();
String numeric();
String order();
String descending();
String ascending();
String sort_first();
String likeliness();
String subset();
String from();
String to();
String selection();
String limit();
String limit_selection_from_index_x(String from);
String limit_selection_to_index_x(String to);
String limit_selection_from_index_x_to_index_y(String from, String to);
String method_of_selection();
String no_replay_within_playlist();
String no_replay_until_time();
String no_replay_until_x_minutes(String minutes);
String bad();
String poor();
String average();
String good();
String excellent();
String file_successfully_uploaded();
String file_successfully_downloaded();
String filter();
String filters();
String created_by();
String created_on();
String last_modified_by();
String last_modified_on();
String on_date();
String split_into_chunks_of();
String uri_scheme();
String new_incoming_notification();
String stream_uri();
String from_to();
String format();
String csv();
String json();
String xml();
String liquidsoap_json();
String liquidsoap_annotate();
String state();
String ready();
String processing();
//Generic validation errors
String text_too_long(String condition);

//Permissions
String channel_administrator();
String programme_manager();
String listener();
String animator();
String curator();
String contributor();
String can_be_read();
String can_be_edited();
String can_be_deleted();

//User management
//#menu
String user_menu();
//#heading
String user_heading();
//#captions
String username();
String label();
String old_password();
String new_password();
String new_password_bis();
String password();
String password_bis();
String first_and_last_name();
String email();
String email_bis();
String enabled();
String admin();
String admins();
String restricted_user();
String restricted_users();
String account();
String domain();
String roles();
String role();
String rights();
String right();
String user();
String users();
String timezone();
String email_notification();
//#Title
String add_user();
String add_admin();
String edit_user();
String view_user();
String edit_admin();
String view_admin();
String add_right();
String users_list();
String rights_list();
String delete_user();
//#Validation errors
String login_empty();
String password_empty();
String weak_password(String condition);
String mismatch_password();
String mismatch_email();
String email_empty();
String email_incorrect();
String label_empty();
String active_unassigned();
String admin_unassigned();
String timezone_empty();
String timezone_incorrect();
String null_value();
//#Notifications
//#Success messages
String user_added(String id);
String user_deleted(String id);
String users_deleted(String ids);
String user_updated(String id);
//#Notifications
String user_content_edited(String userId);

//Channel management
//#menu
String channel_menu();
//#heading
String channel_heading();
//#captions
String description();
String programmes();
String programme();
String streamer_metadata_format();
String streamer_seat_protection_key();
String streamer_queue_minimal_number_of_items_queue();
String streamer_is_live_queueable();
String streamer_is_blank_queueable();
String streamer_are_emergency_files_on();
String streamer_seat_number();
String streamer_find_first_available_seat();
String streamer_automatic_seat_allocation();
String streamer_heartbeat_supported();
String streamer_max_ttl();
String no_available_streamer_seat();
String max_daily_requests_per_user();
String channel_open_for_listener_registration();
String channel_closed_for_listener_registration();
String listener_self_registration();
String links_list();
String links();
String link();
//#Title
String add_channel();
String edit_channel();
String view_channel();
String channels_list();
String delete_channel();
//#Validation errors
//#Notifications
//#Success messages
String channel_added(String id);
String channel_updated(String id);
String channel_deleted(String id);
String channels_deleted(String ids);
//#Notifications
String channel_content_edited(String channelId);
String channel_channel_administrator_link_removed(String channelId, String restrictedUserId);
String channel_channel_administrator_link_added(String channelId, String restrictedUserId);
String channel_programme_manager_link_removed(String channelId, String restrictedUserId);
String channel_programme_manager_link_added(String channelId, String restrictedUserId);
String channel_listener_link_removed(String channelId, String restrictedUserId);
String channel_listener_link_added(String channelId, String restrictedUserId);
String channel_programme_link_removed(String channelId, String programmeId);
String channel_programme_link_added(String channelId, String programmeId);
String channel_timetable_slot_link_removed(String channelId, String timetableSlotId);
String channel_timetable_slot_link_added(String channelId, String timetableSlotId);
String channel_archive_link_removed(String channelId, String archiveId);
String channel_archive_link_added(String channelId, String archiveId);
String channel_journal_link_removed(String channelId, String logId);
String channel_journal_link_added(String channelId, String logId);
String channel_notification_link_removed(String channelId, String notificationId);
String channel_notification_link_added(String channelId, String notificationId);
String channel_queue_item_link_removed(String channelId, String queueItemId);
String channel_queue_item_link_added(String channelId, String queueItemId);
String channel_free(String channelId);
String channel_reserved(String channelId);
String channel_offline(String channelId);
String channel_online(String channelId);
String channel_webservice_link_removed(String channelId, String apiKey);
String channel_webservice_link_added(String channelId, String apiKey);
String channel_report_link_removed(String channelId, String reportId);
String channel_report_link_added(String channelId, String reportId);

//Timetable management
//#menu
String timetable_menu();
//#heading
String timetable_heading();
//#captions
String timetable();
String timetables();
String timescale();
String move();
String shift();
String reschedule();
String postponed_by();
String moved_forward_by();
String resize();
String new_size();
String start();
String end();
String original_start();
String original_end();
String new_start();
String new_end();
String max_end();
String length();
String date();
String time();
String event_kind();
String unique();
String recurring();
String daily();
String weekly();
String monthly();
String yearly();
String repeat();
String first_index();
String second_index();
String third_index();
String number_index(String number);
String decommission();
String repeated_every_day();
String repeated_every_week(String dayOfWeek);
String repeated_every_month(String dayOfMonth);
String repeated_every_year(String dayOfYear);
String activate_decommissioning_date();
String reset_decommissioning_date();
String freeform();
//#Title
String timetables_list();
String add_timetable();
String move_timetable();
String resize_timetable();
String view_timetable();
String grid();
String list();
//#Validation errors
String date_empty();
String date_incorrect();
String positiveintegernonzero_empty();
String positiveintegernonzero_incorrect();
String more_than_a_day();
String more_than_a_week();
String more_than_a_month();
String more_than_a_year();
//#Success messages
String timetable_added(String id);
String timetable_updated(String id);
String timetable_deleted(String id);
String timetables_deleted(String ids);
String timetable_cloned(String id, String clonedDates);
//#Notifications
String timetable_content_edited(String timetableSlotId);

//Programme management
//#menu
String programme_menu();
//#heading
String programme_heading();
//#captions
String channels();
String channel();
String metadata_pattern();
String overlapping_playlist_allowed();
//#Title
String add_programme();
String edit_programme();
String view_programme();
String programmes_list();
String delete_programme();
//#Validation errors
//#Notifications
//#Success messages
String programme_added(String id);
String programme_updated(String id);
String programme_deleted(String id);
String programmes_deleted(String ids);
//#Notifications
String programme_content_edited(String programmeId);
String programme_animator_link_removed(String programmeId, String restrictedUserId);
String programme_animator_link_added(String programmeId, String restrictedUserId);
String programme_contributor_link_removed(String programmeId, String restrictedUserId);
String programme_contributor_link_added(String programmeId, String restrictedUserId);
String programme_curator_link_removed(String programmeId, String restrictedUserId);
String programme_curator_link_added(String programmeId, String restrictedUserId);
String programme_pending_track_link_removed(String programmeId, String pendingTrackId);
String programme_pending_track_link_added(String programmeId, String pendingTrackId);
String programme_pending_track_link_rejected(String programmeId, String pendingTrackId);
String programme_broadcast_track_link_removed(String programmeId, String broadcastTrackId);
String programme_broadcast_track_link_added(String programmeId, String broadcastTrackId);
String programme_broadcast_track_link_validated(String programmeId, String broadcastTrackId);
String programme_timetable_slot_link_removed(String programmeId, String timetableSlotId);
String programme_timetable_slot_link_added(String programmeId, String timetableSlotId);

//Playlist management
//#menu
String playlist_menu();
//#heading
String playlist_heading();
//#captions
String playlists();
String playlist();
String playlist_items();
String playlist_item();
String static_item();
String dynamic_item();
String live();
String activate_live();
String broadcaster_name();
String live_login();
String live_password();
String index();
String slot();
String predefined_list_of_tracks();
String no_predefined_list_of_tracks();
String filtering();
String request_allowed();
String activate_global_tagset();
String max_user_requests();
String fade_in();
String default_value();
//#Title
String add_playlist();
String edit_playlist();
String view_playlist();
String select_playlist();
String playlists_list();
String delete_playlist();
//#Validation errors
String collection_is_empty();
String item_is_malformed_or_unset();
//#Notifications
//#Success messages
String playlist_added(String id);
String playlist_updated(String id);
String playlist_deleted(String id);
String playlists_deleted(String ids);
//#Notifications
String playlist_content_edited(String playlistId);
String playlist_programme_link_removed(String playlistId, String programmeId);
String playlist_programme_link_added(String playlistId, String programmeId);
String playlist_timetable_slot_link_removed(String playlistId, String timetableSlotId);
String playlist_timetable_slot_link_added(String playlistId, String timetableSlotId);

//Track management
//#menu
String track_menu();
//#heading
String track_heading();
String submission_pending_queue();
String broadcastable_list();
//#captions
String tracks();
String no_track();
String x_filters(String count);
String track();
String zero_or_one_filter(String count);
String cover_art();
String one_track_among();
String song();
String songs();
String advert();
String adverts();
String jingle();
String jingles();
String submission_date();
String release_date();
String format_score(String averageScore, String maximumScore, String numberOfVotes);
String rating();
String ratings();
String unrated();
String vote();
String votes();
String average_vote();
String author();
String title();
String rotation_count();
String duration();
String album();
String genre();
String review();
String pending();
String rejected();
String validated();
String comment();
String reset_empty_tags_from_track();
String upload_new_audio();
String upload_new_coverart();
String advisory();
String advisory_early_childhood();
String advisory_everyone();
String advisory_teen();
String advisory_mature();
String advisory_adults_only();
String violence_advisory();
String profanity_advisory();
String fear_advisory();
String sex_advisory();
String drugs_advisory();
String discrimination_advisory();
String publisher();
String copyright();
String tag();
String url();
String technical_info();
String audio_track_technical_info(String format, String size, String bitrate, String samplerate, String channels, String vbr);
String full_audio_track_technical_info(String format, String size, String duration, String bitrate, String samplerate, String channels, String vbr);
String streamer_technical_info(String seatNumber, String userAgentID, String streamURI, String metadataFormat, String queueMinItemsNumber, String queueableBlank, String queueableBlankChunkSize, String queueableBlankURI, String queueableLive, String queueableLiveChunkSize, String queueableLiveURI, String emergencyItemsEnabled, String latestRecordedHeartbeat, String ttl);
String webservice_technical_info(String currentFireRate, String currentDailyHits);
String append_tracks();
String mp3();
String mp4_alac();
String mp4_aac();
String mp4_drmaac();
String native_flac();
String ogg_vorbis();
String jpeg();
String gif();
String png();
String wbmp();
String text();
String compressed_text();
//#Title
String add_track();
String suggest_track();
String reject_track();
String edit_track();
String bulk_edit_tracks();
String view_track();
String tracks_list();
String delete_track();
String select_track();
String select_tracks();
String filters_list();
String request_track();
//#Validation errors
String no_track_uploaded();
//#Notifications
String duplicated_track_detected(String trackAuthor, String trackTitle);
String do_you_want_to_allow_track_duplicates();
//#Success messages
String track_added(String id);
String track_submitted(String id);
String track_updated(String id);
String tracks_updated(String ids);
String track_validated(String id);
String tracks_validated(String ids);
String track_rejected(String id);
String tracks_rejected(String ids);
String track_deleted(String id);
String tracks_deleted(String ids);
String vote_registered();
String request_registered(String id);
String request_unregistered(String id);
//#Notifications
String broadcast_track_content_edited(String broadcastTrackId);
String pending_track_content_edited(String pendingTrackId);

//#Log management
//##menu
String log_menu();
//##heading
String log_heading();
//##captions
String logs();
String log();
String error();
String profile();
String actor();
String action();
String actee();
String create();
String read();
String data_exception();
String io_exception();
String general_exception();
//##Title
String view_log();
String logs_list();
String delete_log();
//##Validation errors
//##Notifications
//##Success messages
String log_deleted(String id);
String logs_deleted(String id);

//#Notification management
//##menu
String notification_menu();
//##heading
String notification_heading();
//##captions
String notifications();
String notification();
String message();
//##Title
String view_notification();
String notifications_list();
String delete_notification();
//##Validation errors
//##Notifications
//##Success messages
String notification_deleted(String id);
String notifications_deleted(String id);

//#Feedback warnings
String entity_concurrently_edited();
String programmes_concurrently_edited();
String channels_concurrently_edited();
String playlists_concurrently_edited();
String timetables_concurrently_edited();
String tracks_concurrently_edited();
String users_concurrently_edited();
String webservices_concurrently_edited();

//#Archive management
//##menu
String archive_menu();
//##heading
String archive_heading();
//##captions
String history_items();
String history_item();
String reports();
String report();
//##Title
String view_history_item();
String history_items_list();
String delete_history_item();
String view_report();
String reports_list();
String delete_report();
String add_report();
//##Validation errors
//##Notifications
//##Success messages
String history_item_deleted(String id);
String history_items_deleted(String id);
String report_added(String id);
String report_deleted(String id);
String reports_deleted(String id);

//Queue management
//#menu
String queue_menu();
//#heading
String queue_heading();
//#captions
String queues();
String queue();
String blank();
String end_of_programme();
String requested_by();
//#Title
String queues_list();
//#Validation errors
//#Notifications
//#Success messages
//#Notifications

//Webservice management
//#menu
String webservice_menu();
//#heading
String webservice_heading();
//#captions
String webservice();
String webservices();
String public_key();
String private_key();
String maximum_fire_rate();
String maximum_daily_quota();
String hits();
String hits_per_second();
String ip_whitelist_regexp();
String modules();
String now_playing_module();
String coming_next_module();
String archive_module();
String timetable_module();
String vote_module();
String request_module();
//#Title
String webservices_list();
String add_webservice();
String edit_webservice();
String delete_webservice();
String view_webservice();
//#Validation errors
//#Success messages
String webservice_added(String id);
String webservice_updated(String id);
String webservice_deleted(String id);
String webservices_deleted(String ids);
//#Notifications
String webservice_content_edited(String timetableSlotId);

//Radio player
//#menu
//#heading
String player_heading();
//#captions
String player();
//#Title
//#Validation errors
//#Notifications
//#Success messages
//#Notifications

}
