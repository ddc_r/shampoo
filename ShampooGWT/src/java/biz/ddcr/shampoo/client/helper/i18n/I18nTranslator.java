/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.i18n;

import biz.ddcr.shampoo.client.helper.errors.CompositeErrorInterface;
import com.google.gwt.core.client.GWT;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.MissingResourceException;

/**
 *
 * The internationalization resource providers.
 * One dedicated to standard static resources (the methods are directly called within the code) and the other for Exception dynamic handling
 * Make them singletons, otherwise it would be a waste of resource.
 *
 * @author okay_awright
 **/
public class I18nTranslator {

    private static I18nMessages i18nMessages;
    private static I18nErrors i18nErrors;

    private I18nTranslator() {
    }

    public static synchronized I18nMessages getInstance() {
        if (i18nMessages == null) {
            i18nMessages = GWT.create(I18nMessages.class);
        }
        return i18nMessages;
    }

    public static synchronized String translateException(Throwable caught) {
        if (i18nErrors == null) {
            i18nErrors = GWT.create(I18nErrors.class);
        }

        //Transform the Exception into a formatted String
        String eClassNameString = toResolvableString(caught);

        //Lookup the formatted String in the resources and return the localized message if any
        if (eClassNameString != null && eClassNameString.length()!=0) {


            if (caught instanceof CompositeErrorInterface) {
                //This is an instance a exception with mapped errors
                //try to resolve the keys and transform the collection of appended key+value strings as a CSV string to will be in turn appended to the exception string
                CompositeErrorInterface extendedCaught = (CompositeErrorInterface) caught;

                if (extendedCaught.getSubErrors() != null) {
                    Collection<String> translatedErrors = new ArrayList<String>();
                    for (RuntimeException error : extendedCaught.getSubErrors()) {

                        String eSubClassNameString = toResolvableString(error);
                        if (eSubClassNameString != null && eSubClassNameString.length()!=0) {
                            try {
                                //This is either a regular exception or a CompositeErrorInterface instance not properly configured, resolve its name and append the exception message if applicable
                                translatedErrors.add(i18nErrors.getString(eSubClassNameString));
                            } catch (MissingResourceException e) {
                                //Do nothing; drop silently
                            }
                        }

                    }
                    if (!translatedErrors.isEmpty()) {
                        try {
                            //This is either a regular exception or a CompositeErrorInterface instance not properly configured, resolve its name and append the exception message if applicable
                            return i18nErrors.getString(eClassNameString) + ": " + toCSV(translatedErrors.toArray(new String[translatedErrors.size()]));

                        } catch (MissingResourceException e) {
                            //Do nothing; drop silently
                        } catch (ClassCastException e) {
                            //Should not happen; makes PMD shut up
                            //Do nothing; drop silently
                        }
                    }

                }

            }

            try {
                //This is either a regular exception or a CompositeErrorInterface instance not properly configured, resolve its name and append the exception message if applicable
                return i18nErrors.getString(eClassNameString) + (caught.getMessage() != null && caught.getMessage().length()!=0 ? ": " + caught.getMessage() : "");

            } catch (MissingResourceException e) {
                //Do nothing; drop silently
            }
        }

        //Default if the formatted string could not be generated or if it cannot be resolved
        return i18nErrors.Exception() + (caught!=null && caught.getMessage() != null && caught.getMessage().length()!=0 ? ": " + caught.getMessage() : (caught!=null ? ": " + toResolvableString(caught) : ""));
    }

    protected static String toResolvableString(Throwable caught) {
        //Transform the Exception into a formatted String
        String eFullClassNameString = caught.getClass().getName();
        if (eFullClassNameString != null) {
            int i = eFullClassNameString.lastIndexOf('.');
            try {
                if (i >= 0) {
                    return eFullClassNameString.substring(i + 1);
                }
            } catch (IndexOutOfBoundsException e) {
                //Do nothing; drop silently
            }
        }
        return null;
    }

    /**
     * Convenience method for transforming an array of String into a unique concatenated String with each sub-String delimited by a comma
     **/
    public static String toCSV(Collection<String> values) {
        StringBuffer _output = new StringBuffer();
        Iterator<String> i = values.iterator();
        int n = 1;
        while (i.hasNext()) {
            _output.append(i.next());
            if (n<values.size())
                _output.append(", ");
            n++;
        }
        return _output.toString();
    }
    public static String toCSV(String[] values) {
        StringBuffer _output = new StringBuffer();
        if (values != null && values.length > 0) {
            for (int i = 0; i < (values.length - 1); i++) {
                _output.append(values[i]).append(", ");
            }
            _output.append(values[values.length - 1]).toString();
        }
        return _output.toString();
    }
}
