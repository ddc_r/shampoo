/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.playlist;

import biz.ddcr.shampoo.client.helper.playlist.Playlist.Parser;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class M3UParser implements Parser {

    protected M3UParser() {
        //For GWT serialization
    }
    
    @Override
    public boolean isMIMETypeOk(String type) {
        if (type.equalsIgnoreCase("audio/x-mpegurl")
                || type.equalsIgnoreCase("audio/mpeg-url")//
                || type.equalsIgnoreCase("application/x-winamp-playlist")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isExtensionOk(String extension) {
        if (extension.equalsIgnoreCase("m3u")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<String> parse(String rawContent) {
        final List<String> processedContent = new ArrayList<String>();
        if (rawContent != null) {
            final String[] lines = rawContent.split("[\r\n]+");
            for (int i = 0; i < lines.length; i++) {
                final String trimmedLine = lines[i].trim();
                if (trimmedLine.length() > 1 && !trimmedLine.startsWith("#")) {
                    processedContent.add(trimmedLine);
                }
            }
        }
        return processedContent;
    }

}
