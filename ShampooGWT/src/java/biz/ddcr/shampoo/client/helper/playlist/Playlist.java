/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.playlist;

import biz.ddcr.shampoo.client.helper.errors.MalformedURIException;
import biz.ddcr.shampoo.client.serviceAsync.GenericHTTPService;
import biz.ddcr.shampoo.client.view.widgets.GenericFilePlayerInterface.HTTPCustomErrorCodes;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a list of Strings in a specific format
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class Playlist implements Iterator<String> {

    public interface Callback {

        public void onBuilt(Playlist builtPlaylist);

        public void onError(Throwable error);
    }

    public interface Parser {

        public boolean isMIMETypeOk(String type);

        public boolean isExtensionOk(String extension);

        public List<String> parse(String content);
    }

    private final static RegExp URL_PATH_REGEXP = RegExp.compile("^[hH][tT][tT][pP][sS]?://[^/?#;]*(/[^?#;]*).*$");
    private List<String> items = null;
    private Iterator<String> itemsIterator = null;

    private Playlist() {
        //For GWT serialization
    }

    public Playlist(List<String> items) {
        this.items = items;
    }

    private Iterator<String> getIterator() {
        if (items != null && itemsIterator == null) {
            itemsIterator = items.iterator();
        }
        return itemsIterator;
    }

    @Override
    public boolean hasNext() {
        return getIterator() != null ? getIterator().hasNext() : false;
    }

    @Override
    public String next() {
        return getIterator() != null ? getIterator().next() : null;
    }

    @Override
    public void remove() {
        if (getIterator() != null) {
            getIterator().remove();
        }
    }

    private static String getPathFromUrl(String url) {
        final MatchResult match = URL_PATH_REGEXP.exec(url);
        if (match != null) {
            final String group = match.getGroup(1);
            if (group != null && !group.isEmpty()) {
                return group;
            }
        }
        return null;
    }

    private static String getExtensionFromPath(String path) {
        return path != null ? path.substring(path.lastIndexOf(".") + 1, path.length()) : null;
    }

    private static void parse(final Parser parser, final String url, final Callback callback) {
        try {
            GenericHTTPService.doGet(url, new RequestCallback() {

                @Override
                public void onResponseReceived(Request request, Response response) {

                    final int code = response.getStatusCode();
                    //If status code == 0 it surely means that the server refused the CORS header, we're out of luck
                    if (code == 0 || code >= 400) {
                        callback.onError(HTTPCustomErrorCodes.toException(code));
                    } else {
                        callback.onBuilt(new Playlist(parser.parse(response.getText())));
                    }
                }

                @Override
                public void onError(Request request, Throwable exception) {
                    callback.onError(exception);
                }
            });
        } catch (RequestException ex) {
            callback.onError(ex);
        }
    }

    /**
     * Build a playlist from an URL, the new playlist is then available through
     * the callback
     *
     * @param url
     * @param callback
     */
    public static void build(final String url, final Callback callback) {
        if (callback == null) {
            throw new IllegalArgumentException("No callback set");
        }
        if (url == null || url.isEmpty()) {
            callback.onError(new MalformedURIException());
        } else {

            //Instantiate available parsers
            final M3UParser m3uParser = new M3UParser();
            final PLSParser plsParser = new PLSParser();

            //Check extension first
            String extension = getExtensionFromPath(getPathFromUrl(url));
            if (extension != null && m3uParser.isExtensionOk(extension)) {
                parse(m3uParser, url, callback);
            } else if (extension != null && plsParser.isExtensionOk(extension)) {
                parse(plsParser, url, callback);
            } else {
                try {
                    //Then check for the MIME type
                    GenericHTTPService.doHead(url, new RequestCallback() {

                        @Override
                        public void onResponseReceived(Request request, Response response) {

                            final int code = response.getStatusCode();
                            if (code >= 400) {
                                callback.onError(HTTPCustomErrorCodes.toException(code));
                            } else {
                                //If status code == 0 it surely means that the server refused the CORS header, we're out of luck
                                if (code == 0) {
                                    //We don't throw an exception because of an unfixed W3C bug: raw <audio> are not covered by CORS and can still play inside the browser using HTML5
                                    GWT.log("CORS likely not enabled or request blocked by a firewall; cannot guess media type using the returned HTTP content-type field");
                                }

                                String contentType = response.getHeader("Content-Type");
                                if (contentType != null && m3uParser.isMIMETypeOk(contentType)) {
                                    parse(m3uParser, url, callback);
                                } else if (contentType != null && plsParser.isMIMETypeOk(contentType)) {
                                    parse(plsParser, url, callback);
                                } else {
                                    //Maybe it's not a playlist afterall
                                    callback.onBuilt(new Playlist(Collections.singletonList(url)));
                                }
                            }
                        }

                        @Override
                        public void onError(Request request, Throwable exception) {
                            callback.onError(exception);
                        }
                    });
                } catch (RequestException ex) {
                    callback.onError(ex);
                }

            }
        }
    }

    /**
     * reset the iterator to the first item
     */
    public void rewind() {
        itemsIterator = null;
    }

}
