/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.resource;

import com.google.gwt.core.client.GWT;

/**
 *
 * The internationalization resource providers.
 * One dedicated to standard static resources (the methods are directly called within the code) and the other for Exception dynamic handling
 * Make them singletons, otherwise it would be a waste of resource.
 *
 * @author okay_awright
 **/
public class ResourceResolver {

    private static ResourceURL resourceURL;

    private ResourceResolver() {
    }

    public static synchronized ResourceURL getInstance() {
        if (resourceURL == null) {
            resourceURL = GWT.create(ResourceURL.class);
        }
        return resourceURL;
    }

}
