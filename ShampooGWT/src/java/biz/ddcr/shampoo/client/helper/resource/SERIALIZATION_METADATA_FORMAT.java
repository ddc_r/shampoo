/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.resource;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public enum SERIALIZATION_METADATA_FORMAT implements SERIALIZATION_FORMAT {

    liquidsoapJSON("LiquidsoapJSON"),
    liquidsoapAnnotate("liquidsoapAnnotate"),
    genericJSON("JSON"),
    genericXML("XML"),
    genericCSV("CSV");
    
    protected final String friendlyName;

    SERIALIZATION_METADATA_FORMAT(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getRawName() {
        return friendlyName;
    }

    @Override
    public String getI18nFriendlyString() {
        switch (this) {
            case liquidsoapJSON:
                return I18nTranslator.getInstance().liquidsoap_json();
            case liquidsoapAnnotate:
                return I18nTranslator.getInstance().liquidsoap_annotate();
            case genericJSON:
                return I18nTranslator.getInstance().json();
            case genericXML:
                return I18nTranslator.getInstance().xml();
            case genericCSV:
                return I18nTranslator.getInstance().csv();
            default:
                return I18nTranslator.getInstance().unknown();
        }
    }
}
