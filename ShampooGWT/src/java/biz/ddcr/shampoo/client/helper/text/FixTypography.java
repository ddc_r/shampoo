/*
 *  Copyright (C) 2015 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.text;

import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class FixTypography {
    static final HashSet<Character> TRIMMABLE_CHARACTERS = new HashSet<Character>();
    static {
        TRIMMABLE_CHARACTERS.add('\t');
        TRIMMABLE_CHARACTERS.add(' ');
        TRIMMABLE_CHARACTERS.add('\r');
        TRIMMABLE_CHARACTERS.add('\n');
        TRIMMABLE_CHARACTERS.add('\f');
        TRIMMABLE_CHARACTERS.add('\b');
    }
    
    static final HashSet<Character> SPECIAL_CHARACTERS = new HashSet<Character>();
    static {
        SPECIAL_CHARACTERS.addAll(TRIMMABLE_CHARACTERS);
        SPECIAL_CHARACTERS.add('.');
        SPECIAL_CHARACTERS.add('(');
        SPECIAL_CHARACTERS.add('[');
        SPECIAL_CHARACTERS.add('{');
        SPECIAL_CHARACTERS.add('"');
        SPECIAL_CHARACTERS.add('+');
        SPECIAL_CHARACTERS.add('!');
        SPECIAL_CHARACTERS.add('-');
        SPECIAL_CHARACTERS.add('#');
        SPECIAL_CHARACTERS.add('&');
        SPECIAL_CHARACTERS.add('*');
        SPECIAL_CHARACTERS.add('/');
        SPECIAL_CHARACTERS.add('\\');
        SPECIAL_CHARACTERS.add('=');
        SPECIAL_CHARACTERS.add('?');
    }

    /**
     * Fixes typography: trimming + word first letter upper case
     *
     * @param text
     * @return
     *
     */
    public static StringBuilder apply(String text) {

        // A state variable that tracks if we are inside
        // or outside a block of word characters
        boolean isInsideWord = false;
        /*
         * was the previously character a whitespace?
         **/
        boolean wasSpace = true;
        boolean isSpace;
        boolean isNotChr;
        StringBuilder newText = new StringBuilder();

        if (text != null) {
            for (int index = 0; index < text.length(); ++index) {

                /*
                 * Trim spaces, leading spaces and inner spaces, empty tail handled afterward
                 **/
                isSpace = TRIMMABLE_CHARACTERS.contains(text.charAt(index));
                if (!(wasSpace && isSpace)) {
                    wasSpace = isSpace;

                    // If the current character is a key for the map array,
                    // we know that the current character is a word character
                    isNotChr = SPECIAL_CHARACTERS.contains(text.charAt(index));

                    /* If the last character was not a word character
                     * but the current character is, convert the
                     * current character to uppercase
                     **/
                    newText.append(
                            (!isInsideWord && !isNotChr)
                            ? Character.toUpperCase(text.charAt(index))
                            : text.charAt(index));

                    // Track whether this character is a word or a non-word character
                    // for the next iteration of the loop
                    isInsideWord = !isNotChr;
                }
            }
        }

        //Trailing space
        if (newText.length() > 0 && TRIMMABLE_CHARACTERS.contains(newText.charAt(newText.length() - 1))) {
            newText.deleteCharAt(newText.length() - 1);
        }

        return newText;
    }
}
