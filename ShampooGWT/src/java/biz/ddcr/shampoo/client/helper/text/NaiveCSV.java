/**
Copyright 2005 Bytecode Pty Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 **/

package biz.ddcr.shampoo.client.helper.text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Stripped down library component originally from Glen Smith and Rainer Pruy
 *
 * A very very naive Collection parser for comma-separated Strings.
 * Don't use the library for anything fancy except for controlled parameters between Javascript/GWT and Applets
 **/
public class NaiveCSV {

    public static final char DEFAULT_SEPARATOR_CHARACTER = ',';
    public static final char DEFAULT_ESCAPE_CHARACTER = '\\';
    public static final char DEFAULT_QUOTE_CHARACTER = '"';
    /** are white space in front of a quote in a field ignored? **/
    private static final boolean ignoreLeadingWhiteSpace = true;

    /**
     * Parses an incoming String and returns an array of elements.
     * Items must be quoted (characters outisde quotes are ignored)
     *
     * @param text
     *            the string to parse
     * @param multi
     * @return the comma-tokenized list of elements, or null if text is null
     * @throws IOException if bad things happen during the read
     **/
    public static Collection<String> toCollection(String text) throws IOException {
        return toCollection(text, true);
    }
    /**
     * Parses an incoming String and returns an array of elements.
     * Items must be quoted if strictQuotes is true
     *
     * @param text
     *            the string to parse
     * @param multi
     * @return the comma-tokenized list of elements, or null if text is null
     * @throws IOException if bad things happen during the read
     **/
    public static Collection<String> toCollection(String text, boolean strictQuotes) throws IOException {

        boolean inField = false;

        List<String> tokens = new ArrayList<String>();
        StringBuilder sb = new StringBuilder();
        boolean inQuotes = false;
        for (int i = 0; i < text.length(); i++) {

            char c = text.charAt(i);
            if (c == DEFAULT_ESCAPE_CHARACTER) {
                if (isNextCharacterEscapable(text, inQuotes || inField, i)) {
                    sb.append(text.charAt(i + 1));
                    i++;
                }
            } else if (c == DEFAULT_QUOTE_CHARACTER) {
                if (isNextCharacterEscapedQuote(text, inQuotes || inField, i)) {
                    sb.append(text.charAt(i + 1));
                    i++;
                } else {
                    inQuotes = !inQuotes;

                    // the tricky case of an embedded quote in the middle: a,bc"d"ef,g
                    if (!strictQuotes
                            && i > 2 //not on the beginning of the line
                            && text.charAt(i - 1) != DEFAULT_SEPARATOR_CHARACTER //not at the beginning of an escape sequence
                            && text.length() > (i + 1)
                            && text.charAt(i + 1) != DEFAULT_SEPARATOR_CHARACTER //not at the	end of an escape sequence
                            ) {

                        if (ignoreLeadingWhiteSpace && sb.length() > 0 && isAllWhiteSpace(sb)) {
                            sb = new StringBuilder();  //discard white space leading up to quote
                        } else {
                            sb.append(c);
                        }

                    }
                }
                inField = !inField;
            } else if (c == DEFAULT_SEPARATOR_CHARACTER && !inQuotes) {
                tokens.add(sb.toString());
                sb = new StringBuilder(); // start work on next token
                inField = false;
            } else {
                if (!strictQuotes || inQuotes) {
                    sb.append(c);
                    inField = true;
                }
            }
        }
        // line is done - check status
        if (inQuotes) {
            throw new IOException("Un-terminated quoted field at the end of the CSV line");
        }
        if (sb != null) {
            tokens.add(sb.toString());
        }
        return tokens;//.toArray(new String[tokensOnThisLine.size()]);

    }

    protected static boolean isNextCharacterEscapable(String text, boolean inQuotes, int i) {
        return inQuotes // we are in quotes, therefore there can be escaped quotes in here.
                && text.length() > (i + 1) // there is indeed another character to check.
                && (text.charAt(i + 1) == DEFAULT_QUOTE_CHARACTER || text.charAt(i + 1) == DEFAULT_ESCAPE_CHARACTER);
    }

    private static boolean isNextCharacterEscapedQuote(String text, boolean inQuotes, int i) {
        return inQuotes // we are in quotes, therefore there can be escaped quotes in here.
                && text.length() > (i + 1) // there is indeed another character to check.
                && text.charAt(i + 1) == DEFAULT_QUOTE_CHARACTER;
    }

    protected static boolean isAllWhiteSpace(CharSequence sb) {
        boolean result = true;
        for (int i = 0; i < sb.length(); i++) {
            char c = sb.charAt(i);

            if (c != ' ') { //Keep it minimal
                return false;
            }
        }
        return result;
    }

    public static String toString(Collection<String> text) {
        return toString(text, true);
    }
    public static String toString(Collection<String> text, boolean strictQuotes) {

        if (text == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        Iterator<String> iter = text.iterator();
        int i = 0;
        while (iter.hasNext()) {

            if (i != 0) {
                sb.append(DEFAULT_SEPARATOR_CHARACTER);
            }

            String nextElement = iter.next();
            if (nextElement == null) {
                continue;
            }
            if (strictQuotes) sb.append(DEFAULT_QUOTE_CHARACTER);

            sb.append(stringContainsSpecialCharacters(nextElement) ? processLine(nextElement) : nextElement);

            if (strictQuotes) sb.append(DEFAULT_QUOTE_CHARACTER);

            i++;
        }

        return sb.toString();
    }

    private static boolean stringContainsSpecialCharacters(String text) {
        return text.indexOf(DEFAULT_QUOTE_CHARACTER) != -1 || text.indexOf(DEFAULT_ESCAPE_CHARACTER) != -1;
    }

    protected static StringBuilder processLine(String nextElement) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < nextElement.length(); j++) {
            char nextChar = nextElement.charAt(j);
            if (nextChar == DEFAULT_QUOTE_CHARACTER) {
                sb.append(DEFAULT_ESCAPE_CHARACTER).append(nextChar);
            } else if (nextChar == DEFAULT_ESCAPE_CHARACTER) {
                sb.append(DEFAULT_ESCAPE_CHARACTER).append(nextChar);
            } else {
                sb.append(nextChar);
            }
        }

        return sb;
    }

    private NaiveCSV() {
    }
}
