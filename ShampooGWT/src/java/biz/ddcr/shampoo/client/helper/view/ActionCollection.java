/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.helper.view;

import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author okay_awright
 **/
public interface ActionCollection<K> extends Collection<ActionCollectionEntry<K>>, Serializable {

    public Collection<ACTION> get(K key);

    /**
     * Return all the keys, discard the associated actions
     *
     * @return
     **/
    public Collection<K> keySet();

    /**
     * Add an object to the map with the specified collection of actions
     * If a collection of actions was already linked to the object, the new one will replace the existing one
     *
     * @param key
     * @param action
     * @return
     **/
    public boolean set(K key, Collection<ACTION> actions);
    public boolean set(ActionCollectionEntry<? extends K> entry);

    /**
     * Wraps the map in parameter within this current object
     *
     **/
    //public boolean set(LinkedHashMap<K, Collection<ACTION>> map);

    /**
     * Add an object to the map with the specified collection of actions.
     * The existing collection of actions (if it already exists) is merged with the new one.
     * @param key
     * @param value
     * @return
     **/
    public boolean merge(K key, Collection<ACTION> actions);

    /**
     * Add a collection of objects to the map, each object is linked to the same specified collection of actions
     * If a collection of actions was already linked to an object, the new one will replace the existing one
     *
     * @param key
     * @param action
     * @return
     **/
    public boolean set(Collection<ActionCollectionEntry<K>> entries);

    /**
     * Add a collection of objects to the map with the same collection of actions.
     * The collection of objects passed as a parameter will be merged with each object existing collection.
     * @param key
     * @param value
     * @return
     **/
    public boolean merge(Collection<K> keys, Collection<ACTION> actions);

    /**
     * Add or replace an object with a single action
     *
     * @param key
     **/
    public boolean set(K key, ACTION action);

    /**
     * Add an object with a single action
     *
     * @param key
     **/
    public boolean add(K key, ACTION action);

    /**
     * Add an object with no action
     *
     * @param key
     **/
    public boolean addNude(K key);

    /**
     * Add objects with no action
     *
     * @param key
     **/
    public boolean addAllNude(Collection<? extends K> keys);
    @Override
    public boolean addAll(Collection<? extends ActionCollectionEntry<K>> entries);


    /**
     *
     * Check if the key is already in the map
     *
     * @param key
     * @return
     **/
    @Override
    public boolean contains(Object key);

    /**
     *
     * Returns an Iterator over a Collection of Map.entries
     *
     * @return
     **/
    @Override
    public abstract Iterator<ActionCollectionEntry<K>> iterator();

    /**
     *
     * Outputs the keys of the map as an Array
     *
     * @return
     **/
    @Override
    public K[] toArray();

    @Override
    public <K> K[] toArray(K[] a);


    /**
     *
     * Remove an entry of the map
     *
     * @param o
     * @return
     **/
    @Override
    public boolean remove(Object o);

    /**
     *
     * Does the map contains all items from the Collection as keys?
     *
     * @param c
     * @return
     **/
    @Override
    public boolean containsAll(Collection<?> c);

    /**
     *
     * Remove all entries in the map that match the entries in the Collection
     **/
    @Override
    public boolean removeAll(Collection<?> c);

    /**
     *
     * Intersection of the map keys and the Collection
     *
     * @param c
     * @return
     **/
    @Override
    public boolean retainAll(Collection<?> c);

    @Override
    public int size();

    @Override
    public boolean isEmpty();

    @Override
    public void clear();
}
