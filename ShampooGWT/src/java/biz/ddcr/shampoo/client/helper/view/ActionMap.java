/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.view;

import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * A Map that is used to enforce how to display a collection of entities in a view.
 * Entities are keys and access rules are values.
 * Access rules are sets of Actions.
 * This class behaves like a pure Collection so as the be easily usable with both Hibernate filters and views as well
 *
 * @author okay_awright
 **/
public abstract class ActionMap<K> implements ActionCollection<K>, Serializable {

    public ActionMap() {
        //Empty constructor; mandatory for GWT marshalling
    }

    public ActionMap(Collection<? extends ActionCollectionEntry<K>> collection) {
        addAll(collection);
    }

    /**
     * Boxed Iterator for Entry<K, Collection<ACTION>>
     * Use a different next() method that does not ouput a classic Map.Entry
     * Avoids re-implementing much of the Collection classes but may be a bit slower with big collections due to instantiation overhead
     *
     **/
    protected class GenericActionMapIterator implements Iterator, Serializable {

        private Iterator<Entry<K, Collection<ACTION>>> boxedIterator;

        public Iterator<Entry<K, Collection<ACTION>>> getBoxedIterator() {
            return boxedIterator;
        }

        protected GenericActionMapIterator() {
        }

        GenericActionMapIterator(Iterator<Entry<K, Collection<ACTION>>> boxedIterator) {
            this.boxedIterator = boxedIterator;
        }

        @Override
        public boolean hasNext() {
            return boxedIterator.hasNext();
        }

        @Override
        public void remove() {
            boxedIterator.remove();
        }

        @Override
        public ActionCollectionEntry<K> next() {
            Entry<K, Collection<ACTION>> i = getBoxedIterator().next();
            return new ActionMapEntry<K>(i.getKey(), i.getValue());
        }
    }

    protected abstract Map<K, Collection<ACTION>> getMap();

    @Override
    public Collection<ACTION> get(K key) {
        return getMap().get(key);
    }

    /**
     * Beware that GWT refuses to serialize the underlying collection
     * @return
     **/
    @Override
    public Collection<K> keySet() {
        return getMap().keySet();
    }

    /**
     * Add an object to the map with the specified collection of actions
     * If a collection of actions was already linked to the object, the new one will replace the existing one
     *
     * @param key
     * @param action
     * @return
     **/
    @Override
    public boolean set(K key, Collection<ACTION> actions) {
        return (key != null && actions != null && getMap().put(key, actions) != null);
    }

    @Override
    public boolean set(ActionCollectionEntry<? extends K> entry) {
        return (entry != null && getMap().put(entry.getKey(), entry.getValue()) != null);
    }

    /**
     * Add an object to the map with the specified collection of actions.
     * The existing collection of actions (if it already exists) is merged with the new one.
     * @param key
     * @param value
     * @return
     **/
    @Override
    public boolean merge(K key, Collection<ACTION> actions) {
        if (key == null || actions == null) {
            return false;
        }
        Collection<ACTION> oldActions = getMap().get(key);
        if (oldActions != null) {
            actions.addAll(oldActions);
        }
        return ((getMap().put(key, actions) != null) ? true : false);
    }

    /**
     * Add a collection of objects to the map, each object is linked to the same specified collection of actions
     * If a collection of actions was already linked to an object, the new one will replace the existing one
     *
     * @param key
     * @param action
     * @return
     **/
    public boolean set(Collection<K> keys, Collection<ACTION> actions) {
        if (keys != null) {
            boolean result = true;
            for (K key : keys) {
                if (key == null || !set(key, actions)) {
                    result = false;
                }
            }
            return result;
        } else {
            return false;
        }
    }

    @Override
    public boolean set(Collection<ActionCollectionEntry<K>> entries) {
        if (entries != null) {
            boolean result = true;
            for (ActionCollectionEntry<K> entry : entries) {
                if (entry.getKey() == null || !set(entry.getKey(), entry.getValue())) {
                    result = false;
                }
            }
            return result;
        } else {
            return false;
        }
    }

    /**
     * Add a collection of objects to the map with the same collection of actions.
     * The collection of objects passed as a parameter will be merged with each object existing collection.
     * @param key
     * @param value
     * @return
     **/
    @Override
    public boolean merge(Collection<K> keys, Collection<ACTION> actions) {
        if (keys != null) {
            boolean result = true;
            for (K key : keys) {
                if (!merge(key, actions)) {
                    result = false;
                }
            }
            return result;
        } else {
            return false;
        }
    }

    public boolean merge(Collection<ActionCollectionEntry<K>> entries) {
        if (entries != null) {
            boolean result = true;
            for (ActionCollectionEntry<K> entry : entries) {
                if (!merge(entry.getKey(), entry.getValue())) {
                    result = false;
                }
            }
            return result;
        } else {
            return false;
        }
    }

    /**
     * Add or replace an object with a single action
     *
     * @param key
     **/
    @Override
    public boolean set(K key, ACTION action) {
        boolean result = false;
        if (key != null && action != null) {
            Collection<ACTION> actions = new HashSet<ACTION>();
            actions.add(action);
            if (getMap().put(key, actions) != null) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Wraps the map in parameter within this current object
     *
     **/
    public abstract boolean set(Map<K, Collection<ACTION>> map);

    /**
     * Add an object with a single action
     *
     * @param key
     **/
    @Override
    public boolean add(K key, ACTION action) {
        boolean result = false;
        if (key != null && action != null) {
            Collection<ACTION> actions = getMap().get(key);
            if (actions == null) {
                actions = new HashSet<ACTION>();
            }
            actions.add(action);
            if (getMap().put(key, actions) != null) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean add(ActionCollectionEntry<K> entry) {
        boolean result = false;
        if (entry != null) {
            Collection<ACTION> actions = getMap().get(entry.getKey());
            if (actions == null) {
                actions = new HashSet<ACTION>();
            }
            actions.addAll(entry.getValue());
            if (getMap().put(entry.getKey(), actions) != null) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Add an object with no action
     *
     * @param key
     **/
    @Override
    public boolean addNude(K key) {
        boolean result = false;
        if (key != null) {
            Collection<ACTION> actions = getMap().get(key);
            if (actions == null) {
                actions = new HashSet<ACTION>();
            }
            if (getMap().put(key, actions) != null) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Add objects with no action
     *
     * @param key
     **/
    @Override
    public boolean addAllNude(Collection<? extends K> keys) {
        if (keys != null) {
            boolean result = true;
            for (K key : keys) {
                if (!addNude(key)) {
                    result = false;
                }
            }
            return result;
        } else {
            return false;
        }
    }

    @Override
    public boolean addAll(Collection<? extends ActionCollectionEntry<K>> entries) {
        if (entries != null) {
            boolean result = true;
            for (ActionCollectionEntry<K> entry : entries) {
                if (!add(entry)) {
                    result = false;
                }
            }
            return result;
        } else {
            return false;
        }
    }

    /**
     *
     * Check if the key is already in the map
     *
     * @param key
     * @return
     **/
    @Override
    public boolean contains(Object entry) {
        return entry!=null ?
            getMap().containsKey(((ActionCollectionEntry<K>)entry).getKey())
        :
            false;
    }

    /**
     *
     * Returns an Iterator over a Collection of Map.entries
     *
     * @return
     **/
    @Override
    public Iterator<ActionCollectionEntry<K>> iterator() {
        //Use the boxed Iterator and benefit from the new next() method
        return new GenericActionMapIterator(getMap().entrySet().iterator());
    }

    /**
     *
     * Outputs the keys of the map as an Array
     *
     * @return
     **/
    @Override
    public K[] toArray() {
        return (K[]) getMap().entrySet().toArray();
    }

    @Override
    public <K> K[] toArray(K[] a) {
        return getMap().entrySet().toArray(a);
    }

    /**
     *
     * Remove an entry of the map by key
     *
     * @param o
     * @return
     **/
    @Override
    public boolean remove(Object entry) {
        if (entry!=null) {
            try {
                if (entry instanceof ActionCollectionEntry)
                    getMap().remove(((ActionCollectionEntry<K>)entry).getKey());
                else
                    getMap().remove((K)entry);
                return true;
            } catch (ClassCastException e) {
                //the entry is not made of a castable class, drop the exception but don't return true
            }
        }
        return false;
    }

    /**
     *
     * Does the map contains all items from the Collection as keys?
     *
     * @param c
     * @return
     **/
    @Override
    public boolean containsAll(Collection<?> c) {
        return getMap().keySet().containsAll((Collection<K>) c);
    }

    /**
     *
     * Remove all entries in the map with keys that match the values in the Collection
     **/
    @Override
    public boolean removeAll(Collection<?> c) {
        boolean modified = false;
        Iterator e = c.iterator();
        while (e.hasNext()) {
            K o = (K)e.next();
            if (getMap().remove(o) != null) {
                modified = true;
            }
        }
        return modified;
    }

    /**
     *
     * Intersection of the map keys and the Collection
     *
     * @param c
     * @return
     **/
    @Override
    public abstract boolean retainAll(Collection c);

    @Override
    public int size() {
        return getMap().size();
    }

    @Override
    public boolean isEmpty() {
        return getMap().isEmpty();
    }

    @Override
    public void clear() {
        getMap().clear();
    }
}

