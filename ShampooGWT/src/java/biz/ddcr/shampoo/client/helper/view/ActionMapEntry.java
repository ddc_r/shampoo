/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.view;

import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **//**
 * Mark ActionMapEntry with IsSerializable as the class won't be listed in any GWT service interface and as such cannot be serialized
 * Force it to be marshallable by extending IsSerializable
 *
 * Dropped because it cannot be properly serialized by GWT 2 when contained in ActionMap
 *
 * @param <K>
 **/
public class ActionMapEntry<K> implements ActionCollectionEntry<K>, Serializable {

    private K key;
    private Collection<ACTION> value;

    public ActionMapEntry() {
        //Empty constructor; mandatory for GWT marshalling
    }

    public ActionMapEntry(ActionCollectionEntry<K> entry) {
        if (entry != null) {
            key = entry.getKey();
            value = entry.getValue();
        }
    }

    public ActionMapEntry(K key, Collection<ACTION> value) {
        /* Drop constraint for keys that can legally be set to null */
        //if (key != null && value != null) {
            this.key = key;
            this.value = value;
        //}
    }

    public ActionMapEntry(K key) {
        //if (key != null)
            this.key = key;
        value = new HashSet<ACTION>();
    }

    public ActionCollectionEntry<K> getEntry() {
        return this;
    }

    public ActionCollectionEntry<K> getMapEntry() {
        return this;
    }

    @Override
    public boolean hasAction(ACTION action) {
        return value.contains(action);
    }

    @Override
    public K getItem() {
        return key;
    }

    @Override
    public boolean isReadable() {
        return value.contains(BASIC_ACTION.READ);
    }

    @Override
    public boolean isEditable() {
        return value.contains(BASIC_ACTION.EDIT);
    }

    @Override
    public boolean isDeletable() {
        return value.contains(BASIC_ACTION.DELETE);
    }

    @Override
    public K getKey() {
        return key;
    }

    protected void setKey(K key) {
        this.key = key;
    }

    @Override
    public Collection<ACTION> getValue() {
        return value;
    }

    @Override
    public Collection<ACTION> setValue(Collection<ACTION> value) {
        this.value = value;
        return this.value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ActionMapEntry<K> other = (ActionMapEntry<K>) obj;
        if (this.key != other.key && (this.key == null || !this.key.equals(other.key))) {
            return false;
        }
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.key != null ? this.key.hashCode() : 0);
        hash = 79 * hash + (this.value != null ? this.value.hashCode() : 0);
        return hash;
    }

}
