/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.helper.view;

import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * An ActionMap where items are unsorted but follow insertion order
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ActionUnsortedMap<K> extends ActionMap<K> {
    private LinkedHashMap<K, Collection<ACTION>> map;

    public ActionUnsortedMap() {
        //Empty constructor; mandatory for GWT marshalling
    }    
    
    public ActionUnsortedMap(Collection<? extends ActionCollectionEntry<K>> collection) {
        super(collection);
    }

    @Override
    protected Map<K, Collection<ACTION>> getMap() {
        if (map==null) map = new LinkedHashMap<K, Collection<ACTION>>();
        return map;
    }

    @Override
    public boolean set(Map<K, Collection<ACTION>> map) {
        this.map = new LinkedHashMap<K, Collection<ACTION>>(map);
        return true;
    }
 
    @Override
    public boolean retainAll(Collection c) {
        boolean modified = false;
        LinkedHashMap<K, Collection<ACTION>> tempMap = new LinkedHashMap<K, Collection<ACTION>>();

        Iterator<K> e = getMap().keySet().iterator();
        while (e.hasNext()) {
            K o = e.next();
            if (c.contains(o)) {
                //If key exists in both objects, add the Map.entry to the new map
                tempMap.put(o, getMap().get(o));
                modified = true;
            }
        }
        //Update the old map if at least an entry has been added to the new map
        if (modified) {
            map = tempMap;
        }

        return modified;
    }
    
}
