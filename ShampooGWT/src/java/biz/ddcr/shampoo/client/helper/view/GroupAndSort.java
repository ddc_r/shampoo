/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.view;

import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import java.io.Serializable;

/**
 *
 * Helper class for specifying the range and the order of SQL data to retrieve in the DAOs.
 * Allows to only select part of SQL results and sorting them out if required
 *
 * @author okay_awright
 **/
public class GroupAndSort<T extends TAGS> implements Serializable {

    //General options
    private Integer maxRows = null;
    private Integer startRow = null;
    private Boolean isDescendingOrder = null;

    //Sort by
    private T sortByFeature = null;
    
    //Filter by
    private T filterByFeature = null;
    private String filterCriterion = null;
    private String filterOperator = null;

    public GroupAndSort() {
    }

    public GroupAndSort(int maxRows, int startRow, T sortByFeature, boolean isDescendingOrder, T filterByFeature, String filterCriterion, String filterOperator) {
        setMaxRows(maxRows);
        setStartRow(startRow);
        setSortByFeature(sortByFeature);
        setSortByDescendingDirection(isDescendingOrder);
        setFilterByFeature(filterByFeature, filterOperator, filterCriterion);
    }
    public GroupAndSort(int maxRows, int startRow, T sortByFeature, boolean isDescendingOrder) {
        setMaxRows(maxRows);
        setStartRow(startRow);
        setSortByFeature(sortByFeature);
        setSortByDescendingDirection(isDescendingOrder);
    }
    public GroupAndSort(T sortByFeature, boolean isDescendingOrder) {
        setSortByFeature(sortByFeature);
        setSortByDescendingDirection(isDescendingOrder);
    }
    public GroupAndSort(T filterByFeature, String filterCriterion, String filterOperator) {
        setFilterByFeature(filterByFeature, filterOperator, filterCriterion);
    }
    
    /**
     * The maximum number of rows to output. A null value is used to withdraw this constraint
     **/
    public Integer getMaxRows() {
        return maxRows;
    }

    public void setMaxRows(Integer maxRows) {
        if (maxRows>0)
            this.maxRows = maxRows;
        else
            this.maxRows = null;
    }

    /**
     * Drop every row from output located before this index. The first row is located at index number 0. A null value is used to withdraw this constraint
     **/
    public Integer getStartRow() {
        return startRow;
    }

    public void setStartRow(Integer startRow) {
        if (startRow>-1)
            this.startRow = startRow;
        else
            this.startRow = null;
    }

    /**
     * Directionality of the sort by feature
     **/
    public Boolean getSortByDescendingDirection() {
        return isDescendingOrder;
    }

    public void setSortByDescendingDirection(Boolean isDescending) {
        this.isDescendingOrder = isDescending;
    }

    /**
     * The actual features (class property) used to sort the result set. No sorting is enforced if the property is null.
     * Multiple strngs can be output, their order is relevant
     **/
    public T getSortByFeature() {
        return sortByFeature;
    }

    public void setSortByFeature(T sortByFeature) {
        this.sortByFeature = sortByFeature;
    }

    /**
     * The actual features (class property) used to filter the result set. No filtering is enforced if the property is null.
     * Multiple strngs can be output, their order is irrelevant
     **/
    public T getFilterByFeature() {
        return filterByFeature;
    }

    public String getFilterOperator() {
        return filterOperator;
    }

    public String getFilterCriterion() {
        return filterCriterion;
    }

    public void setFilterByFeature(T filterByFeature, String operator, String criterion) {
        this.filterByFeature = filterByFeature;
        if (criterion!=null) {
            criterion = criterion.trim();
            if (criterion.length()==0) criterion = null;
        }
        this.filterOperator = operator;
        this.filterCriterion = criterion;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GroupAndSort<T> other = (GroupAndSort<T>) obj;
        if (this.maxRows != other.maxRows && (this.maxRows == null || !this.maxRows.equals(other.maxRows))) {
            return false;
        }
        if (this.startRow != other.startRow && (this.startRow == null || !this.startRow.equals(other.startRow))) {
            return false;
        }
        if (this.isDescendingOrder != other.isDescendingOrder && (this.isDescendingOrder == null || !this.isDescendingOrder.equals(other.isDescendingOrder))) {
            return false;
        }
        if (this.sortByFeature != other.sortByFeature && (this.sortByFeature == null || !this.sortByFeature.equals(other.sortByFeature))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.maxRows != null ? this.maxRows.hashCode() : 0);
        hash = 97 * hash + (this.startRow != null ? this.startRow.hashCode() : 0);
        hash = 97 * hash + (this.isDescendingOrder != null ? this.isDescendingOrder.hashCode() : 0);
        hash = 97 * hash + (this.sortByFeature != null ? this.sortByFeature.hashCode() : 0);
        return hash;
    }

}
