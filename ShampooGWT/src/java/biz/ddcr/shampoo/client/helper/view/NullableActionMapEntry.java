/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.helper.view;

import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **//**
 * Same as ActionMapEntry except that keys can be null
 *
 * @param <K>
 **/
public class NullableActionMapEntry<K> extends ActionMapEntry<K> {

    public NullableActionMapEntry() {
        //Empty constructor; mandatory for GWT marshalling
    }

    public NullableActionMapEntry(ActionCollectionEntry<K> entry) {
        if (entry != null) {
            setKey(entry.getKey());
            setValue(entry.getValue());
        }
    }

    public NullableActionMapEntry(K key, Collection<ACTION> value) {
        if (value != null) {
            setKey(key);
            setValue(value);
        }
    }

    public NullableActionMapEntry(K key) {
        setKey(key);
        setValue(new HashSet<ACTION>());
    }

}
