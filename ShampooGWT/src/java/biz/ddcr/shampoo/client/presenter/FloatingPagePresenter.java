/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.view.views.PageView;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

/**
 *
 * @author okay_awright
 **/
public abstract class FloatingPagePresenter<T extends PageView> extends PagePresenter<T> {

    protected DisclosurePanel disclosurePanel = new DisclosurePanel();
    /** Is the Panel attached to the root panel?*/
    protected boolean isPanelAttached = false;
    protected CloseHandler<DisclosurePanel> closeHandler = new CloseHandler<DisclosurePanel>() {

        @Override
        public void onClose(CloseEvent<DisclosurePanel> event) {
            FloatingPagePresenter.this.onMinimize();
        }
    };
    protected OpenHandler<DisclosurePanel> openHandler = new OpenHandler<DisclosurePanel>() {

        @Override
        public void onOpen(OpenEvent<DisclosurePanel> event) {
            FloatingPagePresenter.this.onRestore();
        }
    };
    /**
     * Instead of embedding the page within the website contentbox make it an independent floating box directly bound to the root panel
     */
    @Override
    protected void bindPage() {          
        disclosurePanel.setAnimationEnabled(true);
        //Custom CSS
        disclosurePanel.setStyleName("GWTFloatingBox");
        //Add a dummy label as a header
        disclosurePanel.setHeader(new Label(view.getTitleText()));
        disclosurePanel.setContent(view.getWidget());
        disclosurePanel.addCloseHandler(closeHandler);
        disclosurePanel.addOpenHandler(openHandler);
        isPanelAttached = true;
        onRefresh();
        getEventBus().dispatch(GlobalEventsEnum.SHOW_FLOATINGBOX, this);
        //Add the widget as a detached view
        RootPanel.get().add(disclosurePanel);        
        //force its intial state at closed
        //disclosurePanel.setOpen(false);
        //Let the flow of operations continue
        onPageVisible();
    }
    
    public void setTitle(final String title) {
        disclosurePanel.setHeader(new Label(title));
    }
    
    /** returns true if the panel is open and visible, false otherwise*/
    public boolean isViewVisible() {
        return isPanelAttached && disclosurePanel.isOpen();
    }
    
    protected void close() {
        //Force minimizing the panel first in order to trigger business rules
        onMinimize();
        isPanelAttached = false;
        getEventBus().dispatch(GlobalEventsEnum.CLOSE_FLOATINGBOX, this);
        //Remove the widget as a detached view
        RootPanel.get().remove(disclosurePanel);
    }

    public abstract void onMinimize();
    public abstract void onRestore();
    
}
