/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.templates.FooterBox;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.mvp4g.client.presenter.LazyXmlPresenter;

/**
 *
 * @author okay_awright
 **/
public class FooterPresenter extends LazyXmlPresenter<FooterBox> implements PresenterInterface {

    private HelperRPCServiceInterfaceAsync helperService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    @Override
    public void onShow() {
        getEventBus().dispatch( GlobalEventsEnum.CHANGE_FOOTERBOX, view.getWidget() );

        //Show version number
        helperService.getAppVersion(new AsyncCallback<String>() {

            @Override
            public void onSuccess(String result) {
                view.setVersioning(result);
            }

            @Override
            public void onFailure(Throwable caught) {
                view.setVersioning(I18nTranslator.getInstance().unknown());
            }
        });
        
        //Show copyright notice
        helperService.getCopyrightNotice(new AsyncCallback<String>() {

            @Override
            public void onSuccess(String result) {
                view.setCopyright(result);
            }

            @Override
            public void onFailure(Throwable caught) {
                view.setCopyright("Copyright © 2011- okay_awright/DDC(R) and contributors");
            }
        });

    }

}
