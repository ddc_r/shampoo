/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.form.GenericTimestampedFormInterface;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * Common implementation for GenericTimestampedForm data display in a popup, from the actual form
 *
 * @author okay_awright
 **/
public abstract class GenericFormPopupPagePresenter<T extends GenericFormPageView, U extends GenericTimestampedFormInterface> extends PopupPagePresenterWithFormParameter<T, U> {

    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    public abstract void formSpecificBind();

    @Override
    public void bindView() {
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });

        //delegate what must be bound for the form-implementation-specific data
        formSpecificBind();
    }

    public abstract void onFormSpecificRefresh();

    @Override
    public void onRefresh() {
        //Reset everything
        view.getCancel().clean();
        refreshCreationLabel();
        refreshLatestModificationLabel();
        //then delegate what must be bound for the form-implementation-specific data
        onFormSpecificRefresh();
    }

    private void refreshCreationLabel(boolean showCreator, boolean showCreationDate) {

        view.getCreationLabel().setVisible(showCreator || showCreationDate);

        String creator = null;
        String date = null;
        if (showCreator)
            creator = getForm().getCreatorId()!=null ? getForm().getCreatorId() : I18nTranslator.getInstance().none();
        if (showCreationDate)
            date = getForm().getCreationDate() != null ? getForm().getCreationDate().getI18nFriendlyString() : I18nTranslator.getInstance().none();

        if (showCreator && showCreationDate)
            view.getCreationLabel().setCaption(
                    I18nTranslator.getInstance().created_by() + " " + creator + " " + I18nTranslator.getInstance().on_date() + " " + date);
        else if (showCreator)
            view.getCreationLabel().setCaption(
                    I18nTranslator.getInstance().created_by() + " " + creator);
        else if (showCreationDate)
            view.getCreationLabel().setCaption(
                    I18nTranslator.getInstance().created_on() + " " + date);
    }

    private void refreshLatestModificationLabel(boolean showEditor, boolean showEditionDate) {

        view.getLatestModificationLabel().setVisible(showEditor || showEditionDate);

        String editor = null;
        String date = null;
        if (showEditor)
            editor = getForm().getLatestEditorId()!=null ? getForm().getLatestEditorId() : I18nTranslator.getInstance().none();
        if (showEditionDate)
            date = getForm().getLatestModificationDate() != null ? getForm().getLatestModificationDate().getI18nFriendlyString() : I18nTranslator.getInstance().none();

        if (showEditor && showEditionDate)
            view.getLatestModificationLabel().setCaption(
                    I18nTranslator.getInstance().last_modified_by() + " " + editor + " " + I18nTranslator.getInstance().on_date() + " " + date);
        else if (showEditor)
            view.getLatestModificationLabel().setCaption(
                    I18nTranslator.getInstance().last_modified_by() + " " + editor);
        else if (showEditionDate)
            view.getLatestModificationLabel().setCaption(
                    I18nTranslator.getInstance().last_modified_on() + " " + date);
    }

    private void refreshCreationLabel() {
        if (getForm() != null) {
            if (getForm().getCreatorId() != null) {
                authorizationService.checkAccessViewUser(getForm().getCreatorId(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getCreationLabel()) {

                    @Override
                    public void onResult(Boolean result) {
                        refreshCreationLabel(result, true);
                    }

                    @Override
                    public void onError() {
                        refreshCreationLabel(false, true);
                    }
                });
            } else {
                refreshCreationLabel(true, true);
            }
        }
    }

    private void refreshLatestModificationLabel() {
        if (getForm() != null) {
            if (getForm().getLatestEditorId() != null) {
                authorizationService.checkAccessViewUser(getForm().getLatestEditorId(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getLatestModificationLabel()) {

                    @Override
                    public void onResult(Boolean result) {
                        refreshLatestModificationLabel(result, true);
                    }

                    @Override
                    public void onError() {
                        refreshLatestModificationLabel(false, true);
                    }
                });
            } else {
                refreshLatestModificationLabel(true, true);
            }
        }
    }
}
