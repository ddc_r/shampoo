/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.templates.HeaderBox;
import com.mvp4g.client.presenter.LazyXmlPresenter;

/**
 *
 * @author okay_awright
 **/
public class HeaderPresenter extends LazyXmlPresenter<HeaderBox> implements PresenterInterface {

    private HelperRPCServiceInterfaceAsync helperService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    @Override
    public void onShow() {
        getEventBus().dispatch( GlobalEventsEnum.CHANGE_HEADERBOX, view.getWidget() );

        //Display some user-defined custom HTML
        helperService.getHTMLHeader(new ExtendedRPCCallback<String>(getEventBus(), view.getCustom()) {

            @Override
            public void onResult(String result) {
                if (result!=null && result.length()!=0) //There's something to display
                {
                    view.getCustom().setCaption(result);
                } //Otherwise do nothing
            }

        });
    }

}
