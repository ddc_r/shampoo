/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.templates.LoggedInBoxInterface;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.mvp4g.client.presenter.XmlPresenter;

/**
 *
 * @author okay_awright
 **/
public class LoggedInPresenter extends XmlPresenter<LoggedInBoxInterface> implements PresenterInterface {

    private LoginRPCServiceInterfaceAsync loginService;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    public void onShow() {
        loginService.getCurrentlyAuthenticatedUserLogin(new ExtendedRPCCallback<String>(getEventBus(), view.getUsernameLabel()) {

            @Override
            public void onResult(String username) {
                
                getEventBus().dispatch(GlobalEventsEnum.CHANGE_LOGINBOX, view.getWidget());
                //update the username on the panel

                view.getUsernameLabel().setCaption(username);
                
                if (UserIn.authenticatedUserName==null)
                    //Instantiate one single comet listener for the GUI lifetime
                    getEventBus().dispatch(GlobalEventsEnum.START_REVERSEAJAX);

                UserIn.authenticatedUserName = username;
                
                //Close the radio player
                getEventBus().dispatch(GlobalEventsEnum.SHOW_RADIOPLAYER);
            }

            @Override
            public void onError() {
                /** Valid once translated into JavaScript*/
                UserIn.authenticatedUserName = null;
            }

        });
        
        
    }

    @Override
    public void bind() {
        //Add link to the user profile
        view.getEditProfileLabel().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //Activate history
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_CHANGEMYPROFILE);
            }
        });
        //Add a link to the list of notifications he has access to
        view.getViewNotificationLabel().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //Activate history
                ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_NOTIFICATIONLIST.getHistoryURLPart());
            }
        });
        //Add a logout link
        view.getLogoutLabel().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                logOut();
            }
        });
    }

    public void logOut() {
        loginService.logOut(new ExtendedRPCCallback<Void>(getEventBus(), view.getLogoutLabel()) {

            @Override
            public void onResult(Void result) {
                //Rebuild the UI
                //Show the logged out version of the login box
                getEventBus().dispatch(GlobalEventsEnum.SHOW_LOGGED_OUT);
                //Refresh the menu
                getEventBus().dispatch(GlobalEventsEnum.SHOW_MENU);
                //Close the radio player
                getEventBus().dispatch(GlobalEventsEnum.CLOSE_RADIOPLAYER);
                //Then go back to the main index page
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_INDEX);
                //Finaly, notify the user that he has successfully logged out
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().successfully_logged_out());
            }

        });
    }

}
