/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.user.LoginForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.templates.NotLoggedInBoxInterface;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.History;
import com.mvp4g.client.presenter.XmlPresenter;

/**
 *
 * @author okay_awright
 **/
public class LoggedOutPresenter extends XmlPresenter<NotLoggedInBoxInterface> implements PresenterInterface {

    private LoginRPCServiceInterfaceAsync loginService;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    public void onShow() {
        getEventBus().dispatch(GlobalEventsEnum.CHANGE_LOGINBOX, view.getWidget());
        //Remove the password from the password text box
        view.getPasswordTextBox().setText("");        
       
        if (UserIn.authenticatedUserName!=null)
            //Instantiate one single comet listener for the GUI lifetime        
            getEventBus().dispatch(GlobalEventsEnum.STOP_REVERSEAJAX);

        //reset the currently authenticated user, if any
        /** Valid once translated into JavaScript*/
        UserIn.authenticatedUserName = null;        
        
        //Show the radio player
        getEventBus().dispatch(GlobalEventsEnum.CLOSE_RADIOPLAYER);
    }

    @Override
    public void bind() {
        //Login button
        view.getLoginButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                login(new LoginForm(
                        view.getUsernameTextBox().getText(),
                        view.getPasswordTextBox().getText(),
                        view.getRememberMeCheckBox().isChecked()));
            }
        });
        //Add link to the the "forgot my password" popup
        view.getForgotMyPasswordButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //Activate history
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_RETRIEVEMYPASSWORD, true);
            }
        });
        //Add link to the the "sign up" popup
        view.getSignUpButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                //Activate history
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_CREATEMYPROFILE, true);
            }
        });
        //Mimick the action of pushing the login button when enter is pressed within either the username or password text box
        KeyPressHandler mimickOnClick = new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                if (event.getCharCode() == KeyCodes.KEY_ENTER) {
                    login(new LoginForm(
                        view.getUsernameTextBox().getText(),
                        view.getPasswordTextBox().getText(),
                        view.getRememberMeCheckBox().isChecked()));
                }
            }
        };
        view.getUsernameTextBox().addKeyPressHandler(mimickOnClick);
        view.getPasswordTextBox().addKeyPressHandler(mimickOnClick);

    }

    public void login(LoginForm loginForm) {
        loginService.logIn(loginForm, new ExtendedRPCCallback<Void>(getEventBus(), view.getLoginButton()) {

            @Override
            public void onResult(Void result) {
                getEventBus().dispatch(GlobalEventsEnum.SHOW_LOGGED_IN);
                //Refresh the menu
                getEventBus().dispatch(GlobalEventsEnum.SHOW_MENU);
                //Finaly, notify the user that he has successfully logged out
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().successfully_logged_in());
                //Refire the history where we were when the disconnection happens
                History.fireCurrentHistoryState();
            }

        });
    }
}
