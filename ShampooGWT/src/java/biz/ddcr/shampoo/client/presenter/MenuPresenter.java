/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.SessionClosedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.templates.MenuBoxInterface;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.mvp4g.client.presenter.XmlPresenter;

/**
 *
 * @author okay_awright
 *
 */
public class MenuPresenter extends XmlPresenter<MenuBoxInterface> implements PresenterInterface {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;

    public AuthorizationRPCServiceInterfaceAsync getAuthorizationService() {
        return authorizationService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    public void onShow() {
        //By default disable all menus
        disableAllMenuEntries();
        //Bind the view to the viewport
        getEventBus().dispatch(GlobalEventsEnum.CHANGE_MENUBOX, view.getWidget());
        //Check which menus must remain hidden or be rendered according to current user credentials
        view.getMenu().resizeMenu(11);
        refreshIndexMenu();
        refreshUserMenu();
        refreshChannelMenu();
        refreshTimetableMenu();
        refreshProgrammeMenu();
        refreshPlaylistMenu();
        refreshQueueMenu();
        refreshTrackMenu();
        refreshWebserviceMenu();
        refreshArchiveMenu();
        refreshJournalMenu();
    }

    private void refreshIndexMenu() {
        //Rebuild the UI
        view.getMenu().setMenuEntryAt(
                0,
                I18nTranslator.getInstance().index_menu(),
                new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_INDEX.getHistoryURLPart());
                    }
                },
                "index");
    } 
    
    private void refreshUserMenu() {
        authorizationService.checkAccessViewUsers(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            1,
                            I18nTranslator.getInstance().user_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_USERLIST.getHistoryURLPart());
                                }
                            },
                            "user");
                }
            }
        });
    }

    private void refreshChannelMenu() {
        authorizationService.checkAccessViewChannels(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            2,
                            I18nTranslator.getInstance().channel_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_CHANNELLIST.getHistoryURLPart());
                                }
                            },
                            "channel");
                }
            }
        });

    }

    private void refreshProgrammeMenu() {
        authorizationService.checkAccessViewProgrammes(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            4,
                            I18nTranslator.getInstance().programme_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_PROGRAMMELIST.getHistoryURLPart());
                                }
                            },
                            "programme");
                }
            }
        });
    }

    private void refreshTimetableMenu() {
        authorizationService.checkAccessViewTimetableSlots(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            3,
                            I18nTranslator.getInstance().timetable_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_TIMETABLELIST.getHistoryURLPart());
                                }
                            },
                            "timetable");
                }
            }
        });
    }

    private void refreshPlaylistMenu() {
        authorizationService.checkAccessViewPlaylists(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            5,
                            I18nTranslator.getInstance().playlist_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_PLAYLISTLIST.getHistoryURLPart());
                                }
                            },
                            "playlist");
                }
            }
        });
    }

    private void refreshQueueMenu() {
        authorizationService.checkAccessViewQueues(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            6,
                            I18nTranslator.getInstance().queue_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_QUEUELIST.getHistoryURLPart());
                                }
                            },
                            "queue");
                }
            }
        });
    }

    private void refreshTrackMenu() {
        authorizationService.checkAccessViewTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            7,
                            I18nTranslator.getInstance().track_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_TRACKLIST.getHistoryURLPart());
                                }
                            },
                            "track");
                }
            }
        });

    }

    private void refreshWebserviceMenu() {
        authorizationService.checkAccessViewWebservices(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            8,
                            I18nTranslator.getInstance().webservice_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_WEBSERVICELIST.getHistoryURLPart());
                                }
                            },
                            "webservice");
                }
            }
        });

    }

    private void refreshArchiveMenu() {
        authorizationService.checkAccessViewArchives(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            9,
                            I18nTranslator.getInstance().archive_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_ARCHIVELIST.getHistoryURLPart());
                                }
                            },
                            "archive");
                }
            }
        });

    }

    private void refreshJournalMenu() {
        authorizationService.checkAccessViewJournals(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getMenu()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.getMenu().setMenuEntryAt(
                            10,
                            I18nTranslator.getInstance().log_menu(),
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    ViewportPresenter.redirectPageWithHistory(RedirectionEventsEnum.SHOW_LOGLIST.getHistoryURLPart());
                                }
                            },
                            "journal");
                }
            }
        });

    }

    private void disableAllMenuEntries() {
        //Hide all entries by default;
        //They are selectively re-activated upon user credential checking
        view.getMenu().clean();
    }
}
