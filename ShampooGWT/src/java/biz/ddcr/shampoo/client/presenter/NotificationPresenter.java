/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.view.widgets.ErrorItem;
import biz.ddcr.shampoo.client.view.widgets.InfoItem;
import biz.ddcr.shampoo.client.view.templates.NotificationBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.NotificationItem;
import biz.ddcr.shampoo.client.view.widgets.WarningItem;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.mvp4g.client.presenter.LazyXmlPresenter;

/**
 *
 * @author okay_awright
 **/
public class NotificationPresenter extends LazyXmlPresenter<NotificationBoxInterface> implements NotificationPresenterInterface {

    private static final int MAX_NUMBER_OF_NOTIFICATIONS_ON_SCREEN = 5;

    @Override
    public void onShow() {
        getEventBus().dispatch(GlobalEventsEnum.CHANGE_NOTIFICATIONBOX, view.getWidget());
        onCleanNotifications();
    }

    @Override
    public void onErrorMessage(String message) {
        addNotification(new ErrorItem(message));
    }
    @Override
    public void onInfoMessage(String message) {
        addNotification(new InfoItem(message));
    }
    @Override
    public void onWarningMessage(String message) {
        addNotification(new WarningItem(message));
    }
    private void addNotification(final NotificationItem i) {

        //Do only display MAX_NUMBER_OF_NOTIFICATIONS_ON_SCREEN notifications on-screen at max.
        if (view.getNumberOfCurrentNotifications()>=MAX_NUMBER_OF_NOTIFICATIONS_ON_SCREEN)
            onCleanNotifications();

        i.addClickMessageHandler(new ClickHandler() {
            //Make it possible to manually clear a notification when clicking on it
            @Override
            public void onClick(ClickEvent event) {
                view.removeNotification(i);
            }
        });
        
        view.addNotification(i);
    }

    @Override
    public void onCleanNotifications() {
        view.removeAllNotifications();
    }


}
