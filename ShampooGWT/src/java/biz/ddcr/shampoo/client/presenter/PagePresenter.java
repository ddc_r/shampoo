/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.view.views.PageView;
import com.mvp4g.client.presenter.LazyXmlPresenter;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public abstract class PagePresenter<T extends PageView> extends LazyXmlPresenter<T> implements PagePresenterInterface {

    /** 
     * Method called each time the page will be displayed
     * This is a good starting point for setting up widgets with dynamic behaviours and dependent on the context of the call
     * Contrary to bind() this method called every time the page is shown on screen, not just at creation tiume
     * OnRefresh() is called BEFORE the page is actually displayed, contrary to onPageVisible()
     **/
    protected abstract void onRefresh();

    /**
     * Method called each time the page is displayed
     * onPageVisible() is called just AFTER the page is actually displayed, contrary to onRefresh()
     **/
    protected abstract void onPageVisible();

    /**
         * Callback used to check if the page can be viewed by the currently authenticated user
     * Should be called within the onShow() method which means that the page is not yet displayed at this point
     * Call bindPage() within this method to grant the application to resume loading of the page, otherwise the page will actually not be displayed
     *
     **/
    protected abstract void onCheckAccess();

    @Override
    public void onShow() {
        onCheckAccess();
    }

    @Override
    public T getView() {
        return view;
    }

    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        //Do nothing by default
        return null;
    }
    /**
     * Method that intercepts broadcast messages from the underlying presentation layer and directed toward this specific implementation of the view
     * Presenters should take notice of those messages and act accordingly, but this is not mandatory
     * @param message
     **/
    public void onMessages(Collection<FeedbackObjectForm> messages) {
        if (messages!=null && !messages.isEmpty()) {
            List<MessageEventsEnum> signals = processMessages(messages);
            if (signals!=null)
                //Read the items in order of insertion: first in first out
                for (MessageEventsEnum signal : signals)
                    onSignal(signal);
        }
    }
    
    @Override
    public void onSignal(MessageEventsEnum signal) {
        //Do nothing by default
    }
    /**
     * The main method used to display the page on screen
     *
     **/
    protected void bindPage() {
        //Show the page
        onRefresh();
        getEventBus().dispatch(GlobalEventsEnum.CHANGE_CONTENTBOX, this);
        //Let the flow of operations continue
        onPageVisible();
    }

    /**
     * Display a new page
     * @param pageRedirectionEvent
     **/
    public void redirectPageNoHistory(RedirectionEventsEnum pageRedirectionEvent) {
        getEventBus().dispatch(pageRedirectionEvent);
    }

    protected void redirectPageNoHistory(RedirectionEventsEnum pageRedirectionEvent, Object parameter) {
        getEventBus().dispatch(pageRedirectionEvent, parameter);
    }

}
