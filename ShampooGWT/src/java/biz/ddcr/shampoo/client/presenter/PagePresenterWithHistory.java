/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.helper.HistoryPartsContainer;
import biz.ddcr.shampoo.client.view.views.PageView;

/**
 *
 * @author okay_awright
 *
 */
public abstract class PagePresenterWithHistory<T extends PageView> extends PagePresenter<T> implements PresenterHistoryFragmentsInterface {

    private HistoryPartsContainer historyParts;

    @Override
    /**
     * 0 based Returns null if no history part available for this level
     */
    public String getHistoryPartLevel(final int index) {
        return (historyParts != null && historyParts.getParts() != null && index > -1 && index < historyParts.getParts().length)
                ? historyParts.getParts()[index]
                : null;
    }

    public void onShow(final HistoryPartsContainer historyParts) {        
        this.historyParts = historyParts;
        this.onShow();
    }

}
