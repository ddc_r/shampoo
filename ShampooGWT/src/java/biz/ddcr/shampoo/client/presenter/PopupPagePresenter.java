/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.view.views.PageView;
import com.google.gwt.user.client.ui.DialogBox;

/**
 *
 * @author okay_awright
 **/
public abstract class PopupPagePresenter<T extends PageView> extends PagePresenter<T> {

    final DialogBox dialogBox = new DialogBox(false);

    /**
     * Instead of embedding the page within the website contentbox make it a dialog box
     */
    @Override
    protected void bindPage() {
        dialogBox.setText(view.getTitleText());
        dialogBox.setWidget(view.getWidget());
        //Custom CSS
        dialogBox.setStyleName("GWTDialogBox");
        
        // Enable animation.
        dialogBox.setAnimationEnabled(true);
        // Enable glass background.
        dialogBox.setGlassEnabled(true);
        dialogBox.setGlassStyleName("GWTModalBackground");
        
        onRefresh();
        getEventBus().dispatch(GlobalEventsEnum.SHOW_POPUP, this);
        dialogBox.center();
        onPageVisible();
    }

    protected void closePopup() {
        dialogBox.hide();        
        getEventBus().dispatch(GlobalEventsEnum.CLOSE_POPUP, this);        
    }

}
