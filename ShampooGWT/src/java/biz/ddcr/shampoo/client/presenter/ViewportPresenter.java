/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.helper.HistoryPartsContainer;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.helper.BroadcastCometListener;
import biz.ddcr.shampoo.client.helper.resource.ResourceResolver;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.templates.ViewportInterface;
import biz.ddcr.shampoo.client.view.widgets.FrameWidgetInterface;
import biz.ddcr.shampoo.client.view.widgets.GWTCSSFlowPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.mvp4g.client.presenter.XmlPresenter;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class ViewportPresenter extends XmlPresenter<ViewportInterface> implements PresenterInterface, ValueChangeHandler<String> {

    private LoginRPCServiceInterfaceAsync loginService;
    
    //Reverse AJAX listener
    private BroadcastCometListener cometListener;
    
    //Keeps track of the current "webpage" displayed in the content box (used to know where to broadcast messages)
    private PagePresenter currentContentBoxPresenter = null;
    //Keeps track of all the currently open popups
    private Collection<PopupPagePresenter> currentPopupsPresenters = new HashSet<PopupPagePresenter>();
    
    //Keeps track of all the currently open floating views
    private Collection<PagePresenter> currentFloatingViewsPresenters = new HashSet<PagePresenter>();
    
    //special incrementing value that keeps track of the successive calls to onShowHourglass()
    private int numberOfShowHourglassCalls = 0;
    private PopupPanel hourglassPopup = null;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void onChangeLoginBox(FrameWidgetInterface widget) {
        //Clear up the notification box
        //getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOX);
        //Update the widget docked to this location
        view.setLoginBoxWidget(widget);
    }

    public void onChangeMenuBox(FrameWidgetInterface widget) {
        //Clear up the notification box if required
        //getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOX);
        //Update the widget docked to this location
        view.setMenuBoxWidget(widget);
    }

    public void onChangeNotificationBox(FrameWidgetInterface widget) {
        //Clear up the notification box
        //getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOX);
        //Update the widget docked to this location
        view.setNotificationBoxWidget(widget);
    }

    public void onChangeContentBox(PagePresenter presenter) {
        //Clear up the notification box
        getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOX);
        //Update the widget docked to this location
        view.setContentBoxWidget(presenter.getView());
        //And keep track of the presenter that manages this view
        currentContentBoxPresenter = presenter;
    }

    public void onShowPopup(PopupPagePresenter presenter) {
        //Clear up the notification box
        getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOX);
        //And keep track of the presenter that manages this view
        currentPopupsPresenters.add(presenter);
    }
    public void onClosePopup(PopupPagePresenter presenter) {
        currentPopupsPresenters.remove(presenter);
    }
    
    public void onChangeFooterBox(FrameWidgetInterface widget) {
        //Clear up the notification box
        //getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOXES);
        //Update the widget docked to this location
        view.setFooterBoxWidget(widget);
    }

    public void onChangeHeaderBox(FrameWidgetInterface widget) {
        //Clear up the notification box
        //getEventBus().dispatch(GlobalEventsEnum.CLEAR_NOTIFICATIONBOXES);
        //Update the widget docked to this location
        view.setHeaderBoxWidget(widget);
    }

    public void onMessagesToContentBox(Collection<FeedbackObjectForm> messages) {
        //Broadcast the message to the currently visible page in the content box only
        if (currentContentBoxPresenter!=null) {
            currentContentBoxPresenter.onMessages(messages);
        }
        //And its popups
        for (PopupPagePresenter currentPopupsPresenter : currentPopupsPresenters)
            currentPopupsPresenter.onMessages(messages);
        //And its floating views
        for (PagePresenter currentFloatingViewsPresenter : currentFloatingViewsPresenters)
            currentFloatingViewsPresenter.onMessages(messages);
    }

    /** Add a view out of the viewport */
    public void onShowFloatingBox(PagePresenter presenter) {
        //And keep track of the presenter that manages this view
        currentFloatingViewsPresenters.add(presenter);
    }
    
    public void onCloseFloatingBox(PagePresenter presenter) {
        //And keep track of the presenter that manages this view
        currentFloatingViewsPresenters.remove(presenter);
    }
    
    public void onStartCometListener() {
        getCometListener().start();
    }
    
    public void onStopCometListener() {
        getCometListener().stop();
    }
    
    protected BroadcastCometListener getCometListener() {
        if (cometListener==null) {
            cometListener = new BroadcastCometListener(//
                GWT.getModuleBaseURL() + ResourceResolver.getInstance().FeedbackGetRelativeEndpoint(),//
                eventBus);
        }
        return cometListener;
    }
    
    /**
     * Display a global "Loading..." notification
     * The implementation keeps track of concurrent access to the method: the hourglass symbol remains on screen until all onShowHourglass() calls are unset by the same number of onHideHourglass()
     * 
     **/
    public void onShowHourglass() {
        if (numberOfShowHourglassCalls<1) {
            //No hourglass yet displayed, do it now
            if (hourglassPopup==null) {
                hourglassPopup = new PopupPanel(false);
                GWTCSSFlowPanel hourglass = new GWTCSSFlowPanel("GWTHourglass");
                hourglassPopup.setWidget(hourglass);
            }
            hourglassPopup.center();
            //just to be sure an odd number of coupled Hourglass events have been called
            numberOfShowHourglassCalls=0;
        }
        numberOfShowHourglassCalls++;
    }
    
    /**
     * Hide if necessary the global "Loading..." notification
     * The implementation keeps track of concurrent access to the method: the hourglass symbol remains on screen until all onShowHourglass() calls are unset by the same number of onHideHourglass()
     *
     **/
    public void onHideHourglass() {
        numberOfShowHourglassCalls--;
        if (numberOfShowHourglassCalls<1) {
            //All onShowHourglass() events have been consumed: the hourglass symbol can be safely disabled
            if (hourglassPopup!=null)
                hourglassPopup.hide();
            //just to be sure an odd number of coupled Hourglass events have been called
            numberOfShowHourglassCalls=0;
        }
    }

    @Override
    public void onShow() {

        //called only once, at startup
        
        //See if a user can be authenticated via cookies and switch the login box according to th result
        loginService.getCurrentlyAuthenticatedUserLogin(new AsyncCallback<String>() {
            
            @Override
            public void onFailure(Throwable caught) {
                //Do not output an error since it doesn't matter
                getEventBus().dispatch(GlobalEventsEnum.SHOW_LOGGED_OUT);
                _onShow();
            }

            @Override
            public void onSuccess(String result) {
                getEventBus().dispatch(GlobalEventsEnum.SHOW_LOGGED_IN);
                _onShow();
            }
        });

    }

    private void _onShow() {
        //Bind the default widgets for each part of the screen
        getEventBus().dispatch(GlobalEventsEnum.SHOW_FOOTER);
        getEventBus().dispatch(GlobalEventsEnum.SHOW_HEADER);
        getEventBus().dispatch(GlobalEventsEnum.SHOW_MENU);
        getEventBus().dispatch(GlobalEventsEnum.SHOW_NOTIFICATION);
        
        // If the application starts with no history token, redirect to the index
        String initToken = History.getToken();
        if (initToken.length() == 0) redirectPageWithHistory(RedirectionEventsEnum.SHOW_INDEX.getHistoryURLPart());
        
        // Add history listener
        History.addValueChangeHandler(this);

        //Hide the loading icon notification embedded into the host HTML page
        DOM.setInnerHTML(RootPanel.get("startupLoadingNotification").getElement(), "");
        
        // Now that we've setup our listener, fire the initial history state.
        History.fireCurrentHistoryState();        

    }
    
    public static void redirectPageWithHistory(final String... tokens) {
        if (tokens!=null) {
            final String newHistoryItem = new HistoryPartsContainer(tokens).toString();
            if (!History.getToken().equals(newHistoryItem)) {
                History.newItem(newHistoryItem);
            }
        }
    }
    
    @Override
    public void onValueChange(ValueChangeEvent<String> event) {        
        if (event!=null) {
            final HistoryPartsContainer eventHistory = new HistoryPartsContainer(event.getValue());
            if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_ARCHIVELIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_ARCHIVELIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_CHANNELLIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_CHANNELLIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_LOGLIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_LOGLIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_NOTIFICATIONLIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_NOTIFICATIONLIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_PLAYLISTLIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_PLAYLISTLIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_PROGRAMMELIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_PROGRAMMELIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_QUEUELIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_QUEUELIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_TIMETABLELIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_TIMETABLELIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_TRACKLIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_TRACKLIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_USERLIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_USERLIST, eventHistory);
            }
            else if (eventHistory.getParts()[0].equals(RedirectionEventsEnum.SHOW_WEBSERVICELIST.getHistoryURLPart())) {
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_WEBSERVICELIST, eventHistory);
            }
            else {
                //Start off with the "greetings" page
                getEventBus().dispatch(RedirectionEventsEnum.SHOW_INDEX, eventHistory);
            }
        }
    }
}
