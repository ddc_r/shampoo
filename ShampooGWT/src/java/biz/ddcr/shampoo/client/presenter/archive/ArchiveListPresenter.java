/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.archive;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.archive.ArchiveForm;
import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.FLAG;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.TYPE;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.archive.ArchiveRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.archive.ArchiveListView;
import biz.ddcr.shampoo.client.view.widgets.GenericCallbackInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class ArchiveListPresenter extends PagePresenterWithHistory<ArchiveListView> {

    private ArchiveRPCServiceInterfaceAsync archiveService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;
    protected ChannelRPCServiceInterfaceAsync channelService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected HelperRPCServiceInterfaceAsync helperService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setArchiveService(ArchiveRPCServiceInterfaceAsync archiveService) {
        this.archiveService = archiveService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewArchives(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {
            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        view.getTabPanel().clean();
        view.removeAllNavigationLinks();
        buildLists();
        //All other UI building are done once channels are retrieved and verified
        //Show the navigation bar links you are granted to see only
        refreshNavigationBar();
    }

    protected void buildLists() {
        authorizationService.checkAccessViewReports(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getTabPanel()) {
            @Override
            public boolean isFailureCatchable(Throwable caught) {
                _buildTabPanel(true, false);
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                _buildTabPanel(true, result);
            }
        });
    }

    private void _buildTabPanel(boolean canViewArchives, boolean canViewReports) {
        //Refresh either list when one is visible
        ClickHandler archiveClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                buildArchiveList();
            }
        };
        ClickHandler reportClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                buildReportList();
            }
        };
        
        //Look in the current history if a tab selection has been specified
        final boolean autoSelectReportsTab = "reports".equals(getHistoryPartLevel(1));
        
        //Build the tabs, only the ones that are visible to the user
        view.buildTabPanel(
                canViewArchives, archiveClickHandler,
                canViewReports, reportClickHandler,
                autoSelectReportsTab);

    }

    protected void buildArchiveList() {
        view.refreshArchivePanel();
        //refresh the UI as soon as possible with content
        //Change current channel
        view.getArchiveChannelComboBox().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                buildArchiveChannelList(view.getArchiveChannelComboBox().getSelectedValue());
            }
        });
        //First list the channels that are visible to the user
        refreshArchiveList(true);
    }

    protected void buildReportList() {
        final SortableTableClickHandler reportPanelHeaderClickHandler = new SortableTableClickHandler() {
            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshReportList();
            }
        };
        view.refreshReportPanel(reportPanelHeaderClickHandler);
        //refresh the UI as soon as possible with content
        refreshReportList();
    }

    protected void refreshNavigationBar() {

        view.removeAllNavigationLinks();

        //Can access channel reports
        final ClickHandler addReportClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_REPORTADD);
            }
        };
        authorizationService.checkAccessAddReports(new ExtendedRPCCallback<Boolean>(eventBus) {
            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddReportLink(addReportClickHandler);
                }
            }
        });

    }

    private void refreshArchiveList(final boolean forceRefresh) {

        //Okay the list must have been initialized beforhand, otherwise it would likely mean that either another tab panel is selected or that the user is denied acces to it
        if (view.getArchiveListPanel() != null) {

            //No need to check if the authenticated user can see channels, it's already been checked when the page opened
            channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getArchiveChannelComboBox()) {
                @Override
                public void onResult(ActionCollection<ChannelForm> result) {
                    boolean doesRefresh = forceRefresh;
                    
                    ChannelForm selectedChannelForm = view.getArchiveChannelComboBox().getSelectedValue();
                    
                    //Look in the current history if a Channel has been specified
                    final String historyChannelLabel = ArchiveListPresenter.this.getHistoryPartLevel(2);
                    if (historyChannelLabel!=null) {
                        //create a fake channel form (label is the only business key)
                        final ChannelForm historyChannelForm = new ChannelForm();
                        historyChannelForm.setLabel(historyChannelLabel);
                        if (selectedChannelForm==null || !selectedChannelForm.equals(historyChannelForm)) {
                            doesRefresh = true;
                            selectedChannelForm = historyChannelForm;
                        }
                    }
                    
                    view.refreshChannelList(result);
                    //Only perform an update if the previous and currently selected channels are different
                    if (doesRefresh | !view.getArchiveChannelComboBox().setSelectedValue(selectedChannelForm)) {
                        //Update the timetable properties immediately, according to the first selected channel
                        buildArchiveChannelList(view.getArchiveChannelComboBox().getSelectedValue());
                    } else {
                        //Don't give up refreshing just yet, timezones might still need an update
                        refreshListTimezoneList(selectedChannelForm);
                    }
                }

                /*@Override
                public void onInit() {
                    view.getArchiveChannelComboBox().clean();
                }*/
            });
        }
    }

    /**
     * Instiantiate a new tab panel Detect which of the grid or the list must be
     * updated
     *
     * @param currentChannel
     *
     */
    private void buildArchiveChannelList(final ChannelForm currentChannel) {
        if (currentChannel == null) {
            view.getArchiveListPanel().clean();
        } else {
            refreshArchivePanel(currentChannel);
        }
    }

    private void refreshArchivePanel(final ChannelForm currentChannel) {
        view.getArchiveListPanel().clean();

        if (currentChannel != null) {

            //Add the timezone changer
            final ChangeHandler workingTimezoneChangeHandler = new ChangeHandler() {
                @Override
                public void onChange(ChangeEvent event) {
                    refreshArchiveList(currentChannel, view.getArchiveTimezoneList().getSelectedValue());
                }
            };

            //Add the header click handler
            final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {
                @Override
                public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                    refreshArchiveList(currentChannel, view.getArchiveTimezoneList().getSelectedValue());
                }
            };
            authorizationService.checkAccessViewArchive(currentChannel.getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getArchiveListPanel()) {
                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    return (!(caught instanceof AccessDeniedException));
                }

                @Override
                public void onResult(Boolean result) {
                    //Activate the menu entries if access is granted
                    if (result) {
                        //Rebuild the UI
                        view.refreshArchiveListPanel(workingPanelHeaderClickHandler, workingTimezoneChangeHandler);
                        //And update it immediately
                        refreshListTimezoneList(currentChannel);
                    }
                }
            });
        }
    }

    protected void refreshArchiveList(ChannelForm currentChannel, final String currentTimezone) {
        final YesNoClickHandler deleteAllArchivesClickHandler = new YesNoClickHandler() {
            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllArchives(view.getArchiveList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteArchiveClickHandler = new YesNoClickHandler() {
            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getArchiveList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteArchive(view.getArchiveList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        archiveService.getSecuredArchivesForChannelId(view.getArchiveList().getCurrentGroupAndSortParameters(), currentChannel.getLabel(), currentTimezone, new ExtendedRPCCallback<ActionCollection<? extends ArchiveForm>>(getEventBus(), view.getArchiveList()) {
            @Override
            public void onResult(ActionCollection<? extends ArchiveForm> result) {
                view.refreshArchiveList(result,
                        deleteArchiveClickHandler,
                        deleteAllArchivesClickHandler);
            }

            /*@Override
            public void onInit() {
                view.getArchiveList().clean();
            }*/
        });
    }

    protected void refreshReportList() {

        //Okay the list must have been initialized beforhand, otherwise it would likely mean that either another tab panel is selected or that the user is denied acces to it
        if (view.getReportList() != null) {

            final GenericCallbackInterface<String, GenericPanelInterface, Void> downloadHandler = new GenericCallbackInterface<String, GenericPanelInterface, Void>() {
                @Override
                public void onCallBack(String url, GenericPanelInterface container, Void reserved) {
                    prepareDownloadLinks(url, container);
                }
            };

            final YesNoClickHandler deleteAllClickHandler = new YesNoClickHandler() {
                @Override
                public void onPreClick(ClickEvent event) {
                    //Do nothing
                }

                @Override
                public void onYesClick() {
                    deleteAllReports(view.getReportList());
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final YesNoClickHandler deleteClickHandler = new YesNoClickHandler() {
                int currentRow = -1;

                @Override
                public void onPreClick(ClickEvent event) {
                    currentRow = view.getReportList().getCurrentRow(event);
                }

                @Override
                public void onYesClick() {
                    deleteReport(view.getReportList(), currentRow);
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final ClickHandler channelDisplayClickHandler = new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    ReportForm report = view.getReportList().getValueAt(view.getReportList().getCurrentRow(event));
                    channelService.getSecuredChannel(report.getLocation().getChannelId(), new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null) {
                        @Override
                        public void onResult(ActionCollectionEntry<ChannelForm> result) {
                            if (result != null && result.isReadable()) {
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                            }
                        }
                    });

                }
            };

            archiveService.getSecuredReports(view.getReportList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<? extends ReportForm>>(getEventBus(), view.getReportList()) {
                /*@Override
                public void onInit() {
                    view.getReportList().clean();
                }*/

                @Override
                public void onResult(ActionCollection<? extends ReportForm> result) {
                    view.refreshReportList(result,
                            downloadHandler,
                            deleteClickHandler,
                            deleteAllClickHandler,
                            channelDisplayClickHandler);
                }
            });
        }
    }

    private void deleteArchive(final GenericFormTableInterface list, final int currentRow) {
        final ArchiveForm archive = (ArchiveForm) list.getValueAt(currentRow);
        if (archive != null) {
            archiveService.deleteSecuredArchive(archive.getRefId().getRefID(), new ExtendedRPCCallback(getEventBus(), list) {
                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().history_item_deleted(archive.getRefId().getFullFriendlyID()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_ARCHIVES);
                }
            });
        }
    }

    private void deleteAllArchives(final GenericFormTableInterface list) {
        final Collection<ArchiveForm> archives = list.getAllCheckedValues();
        if (archives != null && !archives.isEmpty()) {
            Collection<String> ids = new HashSet<String>();
            for (ArchiveForm form : archives) {
                ids.add(form.getRefId().getRefID());
            }
            archiveService.deleteSecuredArchives(ids, new ExtendedRPCCallback(getEventBus(), list) {
                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[archives.size()];
                    int i = 0;
                    for (ArchiveForm archive : archives) {
                        ids[i] = archive.getRefId().getRefID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().history_items_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_ARCHIVES);
                }
            });
        }
    }

    private void deleteReport(final GenericFormTableInterface list, final int currentRow) {
        final ReportForm report = (ReportForm) list.getValueAt(currentRow);
        if (report != null) {
            archiveService.deleteSecuredReport(report.getRefId().getRefID(), new ExtendedRPCCallback(getEventBus(), list) {
                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().report_deleted(report.getFriendlyCaption()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_ARCHIVES);
                }
            });
        }
    }

    private void deleteAllReports(final GenericFormTableInterface list) {
        final Collection<ReportForm> reports = list.getAllCheckedValues();
        if (reports != null && !reports.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (ReportForm form : reports) {
                ids.add(form.getRefId().getRefID());
            }
            archiveService.deleteSecuredReports(ids, new ExtendedRPCCallback(getEventBus(), list) {
                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[reports.size()];
                    int i = 0;
                    for (ReportForm report : reports) {
                        ids[i] = report.getRefId().getRefID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().reports_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_ARCHIVES);
                }
            });
        }
    }

    private void refreshListTimezoneList(final ChannelForm currentChannel) {
        if (currentChannel != null) {

            final String currentlySelectedTimezone = view.getArchiveTimezoneList().getSelectedValue();
            loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getArchiveTimezoneList()) {
                @Override
                public void onResult(String result) {
                    view.refreshListTimezoneList(
                            currentChannel.getTimezone(),
                            result);
                    //Only perform an update if required
                    if (!view.getArchiveTimezoneList().setSelectedValue(currentlySelectedTimezone)) {
                        //Update the whole items
                        refreshArchiveList(currentChannel, view.getArchiveTimezoneList().getSelectedValue());
                    }
                }
            });
        }
    }

    /**
     * Check whether the id given by the GUI event signal corresponds to a
     * playlist in the view list *
     */
    private boolean isInArchiveList(String id) {
        if (view.getArchiveList() != null) {
            for (ArchiveForm archiveForm : view.getArchiveList().getAllValues()) {
                if (archiveForm.getRefId() != null && archiveForm.getRefId().getRefID().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isInReportList(String id) {
        if (view.getReportList() != null) {
            for (ReportForm reportForm : view.getReportList().getAllValues()) {
                if (reportForm.getRefId() != null && reportForm.getRefId().getRefID().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {
            @Override
            public void process(TYPE type, String objectId, FLAG flag, String senderId) {
                //If a new channel is added reload the channel transparently
                //If a new channel is deleted reload the channel transparently as well, except if it's the currently selected channel, in this case refresh the whole list
                switch (type) {
                    case channel:
                        if (flag != FLAG.edit) {
                            add(MessageEventsEnum.REFRESH_CHANNELS);
                        } else {
                            if (objectId == null) {
                                add(MessageEventsEnum.REFRESH_ARCHIVES);
                                add(MessageEventsEnum.REFRESH_REPORTS);
                            } else if (view.getArchiveChannelComboBox() != null && view.getArchiveChannelComboBox().getSelectedValue() != null && objectId.equals(view.getArchiveChannelComboBox().getSelectedValue().getLabel())) {
                                add(MessageEventsEnum.REFRESH_ARCHIVES);
                            }
                        }
                        break;
                    //If an archive has been linked or unlinked to the currently selected channel then refresh the whole archive list
                    case archive:
                        if (flag == FLAG.add) {
                            //LIMITATION cannot detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_ARCHIVES);
                        } else {
                            if (isInArchiveList(objectId)) {
                                add(MessageEventsEnum.REFRESH_ARCHIVES);
                            }
                        }
                        break;
                    //If an archive has been linked or unlinked to the currently selected channel then refresh the whole archive list
                    case report:
                        if (flag == FLAG.add) {
                            //LIMITATION cannot detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_REPORTS);
                        } else {
                            if (isInReportList(objectId)) {
                                add(MessageEventsEnum.REFRESH_REPORTS);
                            }
                        }
                        break;
                    //If the currently authenticated user has been updated refresh his time zone selection
                    case user:
                        if (flag == FLAG.edit && UserIn.authenticatedUserName != null && UserIn.authenticatedUserName.equals(objectId)) {
                            add(MessageEventsEnum.REFRESH_MYPROFILE);
                        }
                        break;
                }
            }
        }.get();
    }

    private void prepareDownloadLinks(final String url, final GenericPanelInterface container) {
        //Retrieve all available serialization formats
        helperService.getTextSerializationFormats(new ExtendedRPCCallback<Collection<SERIALIZATION_METADATA_FORMAT>>(getEventBus(), container) {
            @Override
            public void onResult(Collection<SERIALIZATION_METADATA_FORMAT> result) {
                
                container.clean();
                
                if (result != null) {
                    for (SERIALIZATION_METADATA_FORMAT format : result) {
                        view.addDownloadLink(url, format, container);
                    }
                } else {
                    view.addDownloadLink(url, null, container);
                }
            }

            /*@Override
            public void onInit() {
                super.onInit();
                container.clean();
            }*/
        });
    }

    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_ARCHIVES:
                if (view.getArchiveList() != null) {
                    refreshArchiveList(
                            view.getArchiveChannelComboBox().getSelectedValue(),
                            view.getArchiveTimezoneList().getSelectedValue());
                }
                break;
            case REFRESH_REPORTS:
                refreshReportList();
                break;
            case REFRESH_CHANNELS:
                refreshArchiveList(false);
                break;
            case REFRESH_MYPROFILE:
                if (view.getArchiveList() != null) {
                    refreshListTimezoneList(view.getArchiveChannelComboBox().getSelectedValue());
                }
                break;
        }
    }
}
