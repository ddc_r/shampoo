/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.archive;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.form.archive.ReportFormID;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.archive.ArchiveRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.archive.ReportAddView;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDaySelectorInterface;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * @author okay_awright
 *
 */
public class ReportAddPresenter extends PopupPagePresenter<ReportAddView> {

    protected ArchiveRPCServiceInterfaceAsync archiveService;
    protected HelperRPCServiceInterfaceAsync helperService;
    protected ChannelRPCServiceInterfaceAsync channelService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public ArchiveRPCServiceInterfaceAsync getArchiveService() {
        return archiveService;
    }

    public void setArchiveService(ArchiveRPCServiceInterfaceAsync archiveService) {
        this.archiveService = archiveService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessAddReports(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        view.getChannelComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (Validation.bindValidationError(Validation.checkEntityIsSet(view.getChannelComboBox().getSelectedValue()), view.getChannelComboBox())) {
                    view.refreshTimezone();
                }
            }
        });

        view.getFromDateSelector().setTimeChangeHandler(new GenericYearMonthDaySelectorInterface.YearMonthDayTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getFromDateSelector().getCurrentTime()), view.getFromDateSelector())) {
                    onFromDateChanged();
                }
            }
        });
        view.getToDateSelector().setTimeChangeHandler(new GenericYearMonthDaySelectorInterface.YearMonthDayTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getToDateSelector().getCurrentTime()), view.getToDateSelector())) {
                    onToDateChanged();
                }
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateModule()) {
                    submitModule();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateModule() {
        boolean result = true;

        //Check if there's a valid channel that has been seelcted
        if (Validation.bindValidationError(Validation.checkEntityIsSet(view.getChannelComboBox().getSelectedValue()), view.getChannelComboBox())) {
            result = false;
        }

        if (Validation.bindValidationError(Validation.checkDate(view.getFromDateSelector().getCurrentTime()), view.getFromDateSelector())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkDate(view.getToDateSelector().getCurrentTime()), view.getToDateSelector())) {
            result = false;
        }

        if (Validation.bindValidationError(Validation.checkLabel(view.getTimezoneLabel().getCaption()), view.getTimezoneLabel())) {
            result = false;
        }

        return result;
    }  
    
    public void submitModule() {
        //We need a Unix epoch value so converting the YearMonthDay object is a solution
        final YearMonthDayInterface fromDate = view.getFromDateSelector().getCurrentTime();
        final String channelLabel = view.getChannelComboBox().getSelectedValue().getLabel();

        //Add a day to toDate so that the selected end boundary will be inclusive
        final YearMonthDayInterface toDateExclusive = view.getToDateSelector().getCurrentTime();
        helperService.addDays(toDateExclusive, 1, new ExtendedRPCCallback<YearMonthDayInterface>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(YearMonthDayInterface result) {
                if (result != null) {
                    YearMonthDayInterface toDate = result;

                    _submitModule(channelLabel, fromDate, toDate);                    
                }
            }
        });
    }

    protected void _submitModule(String channelLabel, YearMonthDayInterface fromDate, YearMonthDayInterface toDate) {
        //Build a new Playlist from the view
        final ReportForm newReport = new ReportForm();
        
        final ReportFormID newID = new ReportFormID();
        newID.setChannelId(channelLabel);
        newID.setFromTime(new JSYearMonthDayHourMinuteSecondMillisecond(fromDate));
        newID.setToTime(new JSYearMonthDayHourMinuteSecondMillisecond(toDate));
        
        newReport.setRefId(newID);
        
        //Make it persistant
        archiveService.addSecuredReport(newReport, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().report_added(newReport.getFriendlyCaption()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PLAYLISTS);
            }
        });
        
    }
    
    @Override
    protected void onRefresh() {
        view.getSubmit().clean();

        refreshChannelList();

        //The button might still be deactivated if the user closed the window before the export finished
        view.getSubmit().setEnabled(true);
    }

    private void onToDateChanged() {

        final String currentTimeZone = view.getChannelComboBox().getSelectedValue() != null ? view.getChannelComboBox().getSelectedValue().getTimezone() : "UTC" /*
                 * acceptable default value
                 */;

        //toDate must not be null and not be in the future
        helperService.getCurrentTime(currentTimeZone, new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getToDateSelector()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                if (result != null) {
                    //Update toDate if needed
                    if (view.getToDateSelector().getCurrentTime() == null || view.getToDateSelector().getCurrentTime().compareTo(result) > 0) {
                        view.getToDateSelector().setCurrentTime(result);
                    }

                    if (view.getFromDateSelector().getCurrentTime()!=null && view.getToDateSelector().getCurrentTime().compareTo(view.getFromDateSelector().getCurrentTime()) < 0) {
                        view.getToDateSelector().setCurrentTime(view.getFromDateSelector().getCurrentTime());
                    } else {

                        //Recompute fromDate if needed; it must not be distant for more than a year from toDate
                        helperService.addDays(view.getToDateSelector().getCurrentTime(), -365, new ExtendedRPCCallback<YearMonthDayInterface>(getEventBus(), view.getFromDateSelector()) {

                            @Override
                            public void onResult(YearMonthDayInterface result) {
                                if (result != null) {
                                    if (view.getFromDateSelector().getCurrentTime() == null || view.getFromDateSelector().getCurrentTime().compareTo(result) < 0) {
                                        view.getFromDateSelector().setCurrentTime(result);
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void onFromDateChanged() {

        final String currentTimeZone = view.getChannelComboBox().getSelectedValue() != null ? view.getChannelComboBox().getSelectedValue().getTimezone() : "UTC" /*
                 * acceptable default value
                 */;

        //fromDate must not be null and not be in the future
        helperService.getCurrentTime(currentTimeZone, new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getFromDateSelector()) {

            @Override
            public void onResult(final YearMonthDayHourMinuteSecondMillisecondInterface result) {
                if (result != null) {
                    //Update fromDate if needed
                    
                    //FromDate cannot be set in the future
                    if (view.getFromDateSelector().getCurrentTime() == null || view.getFromDateSelector().getCurrentTime().compareTo(result) > 0) {
                        view.getFromDateSelector().setCurrentTime(result);
                    }

                    //FromDate must be lower than ToDate
                    if (view.getToDateSelector().getCurrentTime() != null && view.getToDateSelector().getCurrentTime().compareTo(view.getFromDateSelector().getCurrentTime()) < 0) {
                        view.getToDateSelector().setCurrentTime(view.getFromDateSelector().getCurrentTime());
                    } else {
                        //Recompute toDate if needed; it must not be distant for more than a year from fromDate
                        //And toDate must not be in the future
                        helperService.addDays(view.getFromDateSelector().getCurrentTime(), 365, new ExtendedRPCCallback<YearMonthDayInterface>(getEventBus(), view.getFromDateSelector()) {

                            @Override
                            public void onResult(YearMonthDayInterface result2) {
                                if (result2 != null) {
                                    if (view.getToDateSelector().getCurrentTime() == null || view.getToDateSelector().getCurrentTime().compareTo(result2) > 0) {
                                        //Make it one year long but do no go into the future
                                        view.getToDateSelector().setCurrentTime(result.compareTo(result2)<0 ? result : result2);
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });

    }

    private void refreshChannelList() {
        //Retrieve all available channels from TrackProgrammeModules
        channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                view.refreshChannelList(result);
                view.refreshTimezone();
                onToDateChanged();
            }

            /*@Override
            public void onInit() {
                super.onInit();
                view.getChannelComboBox().clean();
            }*/
        });

    }
}
