/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.channel;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.FLAG;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionMapEntry;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.channel.ChannelAddView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class ChannelAddPresenter extends PopupPagePresenter<ChannelAddView> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private UserRPCServiceInterfaceAsync userService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessAddChannels(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {
            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Validation handlers for channel
        view.getLabelTextBox().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkLabel(view.getLabelTextBox().getText()), view.getLabelTextBox());
            }
        });

        //Add new programme
        view.getAddNewProgrammeButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addNewProgramme(view.getProgrammeComboBox().getSelectedValue());
            }
        });

        //Add new right
        view.getAddNewRightButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                addNewRight();
            }
        });

        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox());
            }
        });

        /*view.getStreamerPrivateKeyTextBox().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getStreamerPrivateKeyTextBox().getText()), view.getStreamerPrivateKeyTextBox());
            }
        });*/

        /*view.getStreamerQueueMinItems().addChangeHandler(new ChangeHandler() {

         public void onChange(ChangeEvent ce) {
         Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getStreamerQueueMinItems().getValue()), view.getStreamerQueueMinItems());
         }
         });
         view.getStreamerQueueableBlank().addClickHandler(new ClickHandler() {

         public void onClick(ClickEvent event) {
         view.toggleQueueableBlankOptionsChange();
         }
         });
         view.getStreamerQueueableLive().addClickHandler(new ClickHandler() {

         public void onClick(ClickEvent event) {
         view.toggleQueueableLiveOptionsChange();
         }
         });
         view.getStreamerQueueableBlankChunkDurationCheckBox().addClickHandler(new ClickHandler() {

         public void onClick(ClickEvent event) {
         view.toggleQueueableBlankChunkDurationChange();
         }
         });
         view.getStreamerQueueableLiveChunkDurationCheckBox().addClickHandler(new ClickHandler() {

         public void onClick(ClickEvent event) {
         view.toggleQueueableLiveChunkDurationChange();
         }
         });*/
        view.getStreamerSeatNumericBox().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent ce) {
                Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getStreamerSeatNumericBox().getValue()), view.getStreamerSeatNumericBox());
            }
        });
        view.getStreamerAutoSeatAllocationCheckBox().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                view.toggleStreamerSeatPanel();
            }
        });
        view.getStreamerDiscoverFreeSeatButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                discoverNextFreeSeat();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        //Check label
        if (Validation.bindValidationError(Validation.checkLabel(view.getLabelTextBox().getText()), view.getLabelTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionTextBox().getText()), view.getDescriptionTextBox())) {
            result = false;
        }
        /*if (Validation.bindValidationError(Validation.checkPassword(view.getStreamerPrivateKeyTextBox().getText()), view.getStreamerPrivateKeyTextBox())) {
            result = false;
        }*/
        if (!view.getStreamerAutoSeatAllocationCheckBox().isChecked() && Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getStreamerSeatNumericBox().getValue()), view.getStreamerSeatNumericBox())) {
            result = false;
        }
        /*if (Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getStreamerQueueMinItems().getValue()), view.getStreamerQueueMinItems())) {
         result = false;
         }*/
        if (Validation.bindValidationError(Validation.checkMaxText(view.getURLTextBox().getText()), view.getURLTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getTagTextBox().getText()), view.getTagTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new Channel from the view
        final ChannelForm newChannel = new ChannelForm();
        newChannel.setDescription(view.getDescriptionTextBox().getText());
        newChannel.setLabel(view.getLabelTextBox().getText());
        newChannel.setTimezone(view.getTimezoneComboBox().getSelectedValue());
        newChannel.setMaxDailyRequestLimitPerUser(view.getMaxDailyRequestPerUserSelector().getSelectedValue());
        //Misc data
        newChannel.setUrl(view.getURLTextBox().getText());
        newChannel.setTag(view.getTagTextBox().getText());
        newChannel.setOpen(view.getOpenCheckBox().isChecked());

        //Streamer
        //newStreamerModule.setMetadataFormat(view.getStreamerMetadataFormatDropDownListBox().getSelectedValue());
        //newStreamerModule.setPrivateKey(view.getStreamerPrivateKeyTextBox().getText());
        /*newStreamerModule.setQueueMinItems(view.getStreamerQueueMinItems().getValue());
         //queuable lives?
         newStreamerModule.setQueueableLive(view.getStreamerQueueableLive().isChecked());
         if (view.getStreamerQueueableLiveChunkDurationCheckBox().isChecked()) {
         newStreamerModule.setQueueableLiveChunkSize(view.getStreamerQueueableLiveChunkDurationSelector().getSelectedValue());
         } else {
         newStreamerModule.setQueueableLiveChunkSize(null);
         }
         newStreamerModule.setQueueableLiveURIPattern(view.getStreamerQueueableLiveURIPattern().getText());
         //Queueable blanks?
         newStreamerModule.setQueueableBlank(view.getStreamerQueueableBlank().isChecked());
         if (view.getStreamerQueueableBlankChunkDurationCheckBox().isChecked()) {
         newStreamerModule.setQueueableBlankChunkSize(view.getStreamerQueueableBlankChunkDurationSelector().getSelectedValue());
         } else {
         newStreamerModule.setQueueableBlankChunkSize(null);
         }
         newStreamerModule.setQueueableBlankURIPattern(view.getStreamerQueueableBlankURIPattern().getText());        
         newStreamerModule.setStreamURI(view.getStreamURLTextBox().getText());*/
        //newStreamerModule.setEnableEmergencyItems(view.getStreamerEnableEmergencyItems().isChecked());
        
        newChannel.setSeatNumber(
                new ActionMapEntry<Long>(
                        view.getStreamerAutoSeatAllocationCheckBox().isChecked() ? null : view.getStreamerSeatNumericBox().getValue().longValue()
                    )
                );

        //Convert a list to a set
        ActionMap<String> p = new ActionSortedMap<String>();
        p.addAllNude(view.getProgrammesListBox().getAllValues());
        newChannel.setProgrammes(p);

        ActionMap<RightForm> r = new ActionSortedMap<RightForm>();
        r.addAllNude(view.getRightsListBox().getAllValues());
        newChannel.setRightForms(r);

        //Make it persistant
        channelService.addSecuredChannel(newChannel, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {
            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().channel_added(newChannel.getLabel()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_CHANNELS);
            }
        });
    }

    @Override
    protected void onRefresh() {
        //Reset everything
        view.getLabelTextBox().clean();
        view.getDescriptionTextBox().clean();
        view.getSubmit().clean();
        //clear right list
        view.getRoleRightComboBox().clean();
        view.getUserRightComboBox().clean();
        view.getRightsListBox().clean();
        //Clear programmes list
        view.getProgrammeComboBox().clean();
        view.getProgrammesListBox().clean();
        //update available users for channels
        feedUsersList();
        feedRolesList();
        //Update programmes list
        feedProgrammesList();
        //Update streamer metadata options
        //feedStreamerMetadataFormatList();
        //view.getStreamerPrivateKeyTextBox().clean();
        view.getStreamerAutoSeatAllocationCheckBox().clean();
        view.getStreamerDiscoverFreeSeatButton().clean();
        view.getStreamerSeatNumericBox().clean();
        feedStreamerOptions();
        /*view.getStreamerQueueMinItems().clean();
         //view.getStreamerEnableEmergencyItems().clean();        
         //Queueable live?
         view.getStreamerQueueableLive().clean();
         view.toggleQueueableLiveOptionsChange();
         view.getStreamerQueueableLiveChunkDurationCheckBox().clean();
         view.toggleQueueableLiveChunkDurationChange();
         view.getStreamerQueueableLiveChunkDurationSelector().clean();
         view.refreshMaxQueueableLiveChunkDurationList();
         view.getStreamerQueueableLiveURIPattern().clean();
         view.getStreamerQueueableLiveURIPattern().setText("live:${queueID}");
         //Queueable blank?
         view.getStreamerQueueableBlank().clean();
         view.toggleQueueableBlankOptionsChange();
         view.getStreamerQueueableBlankChunkDurationCheckBox().clean();
         view.toggleQueueableBlankChunkDurationChange();
         view.getStreamerQueueableBlankChunkDurationSelector().clean();
         view.refreshMaxQueueableBlankChunkDurationList();
         view.getStreamerQueueableBlankURIPattern().clean();
         view.getStreamerQueueableBlankURIPattern().setText("blank:default");
         view.getStreamURLTextBox().clean();*/
        //Fill in timezones
        view.getTimezoneComboBox().clean();
        feedTimezoneList();
        //Fill in other misc options
        view.getMaxDailyRequestPerUserSelector().clean();
        view.refreshMaxDailyRequestsPerUserList();
        //Misc data
        view.getURLTextBox().clean();
        view.getTagTextBox().clean();
        view.getOpenCheckBox().clean();
    }

    /*private void feedStreamerMetadataFormatList() {
     //refresh the available streamer metadata format list
     view.refreshStreamerMetadataFormatList(SERIALIZATION_METADATA_FORMAT.values());
     }*/
    
    private void feedStreamerOptions() {
        view.getStreamerAutoSeatAllocationCheckBox().setChecked(false);
        //Auto find a new seat as soon as possible
        discoverNextFreeSeat();
        view.toggleStreamerSeatPanel();
    }
    
    private void feedRolesList() {
        //refresh the available roles list
        view.refreshRoleRightList(ChannelRole.values());
    }

    private void feedTimezoneList() {
        //refresh the available time zones list
        helperService.getTimezones(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getTimezoneComboBox()) {
            @Override
            public void onResult(Collection<String> result) {
                view.refreshTimezoneList(result);
            }

            /*@Override
             public void onInit() {
             view.getTimezoneComboBox().clean();
             }*/
        });

    }

    private void feedUsersList() {
        userService.getSecuredRestrictedUsers(null, new ExtendedRPCCallback<ActionCollection<RestrictedUserForm>>(getEventBus(), view.getUserRightComboBox()) {
            @Override
            public void onResult(ActionCollection<RestrictedUserForm> result) {
                view.refreshRestrictedUserRightList(result);
            }
        });
    }

    private void feedProgrammesList() {
        programmeService.getSecuredProgrammes(null, new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {
            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshProgrammeList(result);
            }
        });
    }

    private boolean addNewProgramme(final ProgrammeForm programme) {

        //Is it already present in the list?
        if (programme == null || view.getProgrammesListBox().isValueIn(programme.getLabel())) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                view.getProgrammesListBox().removeRow(view.getProgrammesListBox().getCurrentRow(event));
            }
        };
        view.addNewProgramme(programme, deleteClickHandler, new ClickHandler() {
            @Override
            public void onClick(ClickEvent ce) {
                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, programme);
            }
        });

        return true;

    }

    private boolean addNewRight() {

        final RestrictedUserForm user = view.getUserRightComboBox().getSelectedValue();
        ChannelRole role = (ChannelRole) view.getRoleRightComboBox().getSelectedValue();

        //Check if the role and either the channel or programme parts of the rights are properly filled in
        if (user == null || role == null) {
            return false;
        }

        ChannelRightForm newRight = new ChannelRightForm(user.getUsername(), null, role);

        //Is it already present in the list?
        if (view.getRightsListBox().isValueIn(newRight)) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                view.getRightsListBox().removeRow(view.getRightsListBox().getCurrentRow(event));
            }
        };
        view.addNewRight(newRight, deleteClickHandler, new ClickHandler() {
            @Override
            public void onClick(ClickEvent ce) {
                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERDISPLAY, user);
            }
        });

        return true;
    }
    
    private void discoverNextFreeSeat() {
        channelService.getUnsecuredNextAvailableStreamerSeatNumber(new ExtendedRPCCallback<Long>(getEventBus(), view.getStreamerDiscoverFreeSeatButton()) {

            @Override
            public void onResult(Long result) {
                if (result==null || result<1) {
                    //Warn the user that no seat is currently available
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().no_available_streamer_seat());
                } else
                    //Tricky long to int conversion
                    //TODO: proper cast
                    view.getStreamerSeatNumericBox().setValue(result.intValue());
            }
        });
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {
            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //If a new channel is added reload the channel transparently
                //If a new channel is deleted reload the channel transparently as well, except if it's the currently selected channel, in this case refresh the whole list
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId == null || !senderId.equals(UserIn.authenticatedUserName)) {
                    switch (type) {
                        case programme:
                            //If a programme has been added or removed then just notify the user
                            if (flag != FLAG.edit) //Show notification to user
                            {
                                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                            }
                            break;
                        case user:
                            //If an authenticated user has been added or removed then just notify the user
                            if (flag != FLAG.edit) //Show notification to user
                            {
                                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().users_concurrently_edited());
                            }
                    }
                }
            }
        }.get();
    }
}
