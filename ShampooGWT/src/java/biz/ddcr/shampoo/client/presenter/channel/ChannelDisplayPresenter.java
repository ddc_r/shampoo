/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.channel;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.channel.ChannelDisplayView;
import biz.ddcr.shampoo.client.view.widgets.ExternalURLClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class ChannelDisplayPresenter extends GenericFormPopupPagePresenter<ChannelDisplayView, ChannelForm> {

    private ChannelRPCServiceInterfaceAsync channelService;

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the user to view
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewChannel(getForm().getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {
                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {
        //Do noting
    }

    @Override
    public void onFormSpecificRefresh() {
        final GenericStatusChangeHandler<String, STATUS> streamerStatusChangeHandler = new GenericStatusChangeHandler<String, STATUS>() {
            @Override
            public void onGet(GenericStatusBoxInterface<String, STATUS> source) {
                fetchStreamerMetadata(source);
            }
        };

        //Reset everything
        view.getLabelLabel().setCaption(getForm().getLabel());
        view.getDescriptionLabel().setCaption(getForm().getDescription());
        view.getProgrammesGrid().clean();
        feedProgrammesList();
        view.getRightsTree().clean();
        feedRightsList();
        //Fill in timezones
        view.getTimezoneLabel().setCaption(getForm().getTimezone());
        //Fill other misc options
        view.getMaxDailyRequestPerUserLabel().setCaption(getForm().getMaxDailyRequestLimitPerUser() == null ? I18nTranslator.getInstance().infinite() : getForm().getMaxDailyRequestLimitPerUser().toString());
        view.refreshSeatNumber(getForm().getSeatNumber());
        //Private webservice
        view.initializePrivateWebserviceModuleStatus(getForm().getLabel(), streamerStatusChangeHandler);
        view.getStreamerProtectionKeyLabel().setVisible(false);
        view.getStreamerProtectionKeyLabel().clean();        
        fetchStreamerId();
        view.getStreamerMetadataPanel().setVisible(false);
        view.getStreamerMetadataPanel().clean();        
        fetchStreamerMetadata(view.getStreamerStatusWidget());
        //Misc data
        view.getURLLabel().setCaption(getForm().getUrl());
        view.getURLLabel().addClickHandler(new ExternalURLClickHandler(getForm().getUrl(), /*open in a new window*/ false));
        if (getForm().isOpen()) {
            view.getOpenLabel().setCaption(I18nTranslator.getInstance().channel_open_for_listener_registration());
        } else {
            view.getOpenLabel().setCaption(I18nTranslator.getInstance().channel_closed_for_listener_registration());
        }
    }

    private void feedRightsList() {

        //Clear the currently bound channel so as the isIn() method for checking if the same right cannot be added twice works
        ActionCollection<RightForm> newRights = new ActionSortedMap<RightForm>();
        for (ActionCollectionEntry<RightForm> rightFormEntry : getForm().getRightForms()) {
            RightForm newRight = rightFormEntry.getKey();
            newRight.setDomainId(null);
            newRights.merge(newRight, rightFormEntry.getValue());
        }
        view.refreshExistingRights(newRights);

    }

    private void feedProgrammesList() {
        view.refreshExistingProgrammes(getForm().getProgrammes());
    }

    /**
     *
     * Retrieve the current channel/streamer connection status, fetched as soon
     * as the component is drawn on screen
     *
     * @param voteWidget
     *
     */
    private void fetchStreamerMetadata(final GenericStatusBoxInterface<String, STATUS> statusWidget) {
        channelService.getSecuredDynamicChannelStreamerMetadata(getForm().getLabel(), new ExtendedRPCCallback<ActionCollectionEntry<StreamerModule>>(getEventBus(), view.getStreamerMetadataPanel()) {
            @Override
            public void onResult(ActionCollectionEntry<StreamerModule> result) {
                view.refreshStreamerStatus(statusWidget, result);
                view.refreshStreamerMetadata(result);
            }
        });
    }
    private void fetchStreamerId() {
        channelService.getSecuredChannelStreamerKey(getForm().getLabel(), new ExtendedRPCCallback<ActionCollectionEntry<String>>(getEventBus(), view.getStreamerProtectionKeyLabel()) {
            @Override
            public void onResult(ActionCollectionEntry<String> result) {
                view.refreshStreamerId(result);
            }
        });
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case streamer:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_STREAMERS);
                        } else {
                            //objectId is a seat number
                            if (objectId==null || (getForm().getSeatNumber()!=null && getForm().getSeatNumber().getItem()!=null && objectId.equals(getForm().getSeatNumber().getItem().toString())))
                                    add(MessageEventsEnum.REFRESH_STREAMERS);
                            }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_STREAMERS:
                //Its identifier
                fetchStreamerId();
                //And its metadata
                //Update the the small 'led' notifying whether the streamer is up or down
                fetchStreamerMetadata(view.getStreamerStatusWidget());
                break;
        }
    }
}
