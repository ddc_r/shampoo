/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.channel;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.channel.ChannelListView;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class ChannelListPresenter extends PagePresenterWithHistory<ChannelListView> {

    private UserRPCServiceInterfaceAsync userService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewChannels(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        //Show the navigation bar links you are granted to see only
        refreshNavigationBar();
        //Build all the tabs
        refreshLists();
    }

    private void refreshLists() {
        view.getChannelPanel().clean();

        //Add the header click handler
        final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshChannelList();
            }
        };
        authorizationService.checkAccessViewChannels(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getChannelPanel()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshChannelPanel(workingPanelHeaderClickHandler);
                    //And update it immediately
                    refreshChannelList();
                }
            }
        });

    }

    protected void refreshChannelList() {
        final GenericStatusChangeHandler<String,STATUS> streamerStatusChangeHandler = new GenericStatusChangeHandler<String,STATUS>() {

            @Override
                public void onGet(GenericStatusBoxInterface<String, STATUS> source) {
                    fetchStreamerInfo(source);
                }
            };
        final ClickHandler editChannelClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ChannelForm channel = view.getChannelList().getValueAt(view.getChannelList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELEDIT, channel);
            }
        };
        final ClickHandler displayChannelClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ChannelForm channel = view.getChannelList().getValueAt(view.getChannelList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, channel);
            }
        };
        final SerializableItemSelectionHandler<String> displayProgrammeSelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final SerializableItemSelectionHandler<String> displayRestrictedUserSelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        userService.getSecuredRestrictedUser(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<RestrictedUserForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<RestrictedUserForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final YesNoClickHandler deleteAllChannelsClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllChannels(view.getChannelList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteChannelClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getChannelList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteChannel(view.getChannelList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        channelService.getSecuredChannels(view.getChannelList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelList()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                view.refreshChannelList(result,
                        editChannelClickHandler,
                        deleteChannelClickHandler,
                        deleteAllChannelsClickHandler,
                        displayChannelClickHandler,
                        streamerStatusChangeHandler,
                        displayProgrammeSelectionHandler,
                        displayRestrictedUserSelectionHandler
                        );
            }

            /*@Override
            public void onInit() {
                view.getChannelList().clean();
            }*/
        });
    }

    protected void refreshNavigationBar() {

        view.removeAllNavigationLinks();

        final ClickHandler addRoleClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELADD);
            }
        };
        authorizationService.checkAccessAddChannels(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddChannelLink(addRoleClickHandler);
                }
            }
        });

    }

    private void deleteChannel(final GenericFormTableInterface list, final int currentRow) {
        final ChannelForm channel = (ChannelForm) list.getValueAt(currentRow);
        if (channel != null) {
            channelService.deleteSecuredChannel(channel.getLabel(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().channel_deleted(channel.getLabel()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_CHANNELS);
                }
            });
        }
    }

    private void deleteAllChannels(final GenericFormTableInterface list) {
        final Collection<ChannelForm> channels = list.getAllCheckedValues();
        if (channels != null && !channels.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (ChannelForm form : channels) {
                ids.add(form.getLabel());
            }
            channelService.deleteSecuredChannels(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[channels.size()];
                    int i = 0;
                    for (ChannelForm channel : channels) {
                        ids[i] = channel.getLabel();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().channels_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_CHANNELS);
                }
            });
        }
    }

    /**
     *
     * Retrieve the current channel/streamer connection status, fetched as soon as the component is drawn on screen
     *
     * @param voteWidget
     **/
    private void fetchStreamerInfo(final GenericStatusBoxInterface<String,STATUS> statusWidget) {
        if (statusWidget != null && statusWidget.getID() != null) {
            channelService.getSecuredDynamicChannelStreamerMetadata(statusWidget.getID(), new ExtendedRPCCallback<ActionCollectionEntry<StreamerModule>>(getEventBus(), statusWidget) {

                /*@Override
                public void onInit() {
                    super.onInit();
                    statusWidget.clean();
                }*/

                @Override
                public void onResult(ActionCollectionEntry<StreamerModule> result) {
                    view.refreshStreamerStatus(statusWidget, result);
                }
            });
        }
    }

    /** Check whether the id given by the GUI event signal corresponds to a channel in the view list **/
    private boolean isInChannelList(String id) {
        for (ChannelForm channelForm : view.getChannelList().getAllValues()) {
            if (channelForm.getLabel().equals(id))
                return true;
        }
        return false;
    }
    private boolean isInSeatList(String id) {
        for (ChannelForm channelForm : view.getChannelList().getAllValues()) {
            if ((channelForm.getSeatNumber()!=null && channelForm.getSeatNumber().getItem()!=null && channelForm.getSeatNumber().getItem().toString().equals(id)))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //If a new channel is added reload the channel transparently
                //If a new channel is deleted reload the channel transparently as well, except if it's the currently selected channel, in this case refresh the whole list
                switch (type) {
                    case channel:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_CHANNELS);
                        } else {
                            if (isInChannelList(objectId))
                                add(MessageEventsEnum.REFRESH_CHANNELS);                                
                        }
                        break;
                    case streamer:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_STREAMERS);
                        } else {
                            //objectId is a seat number
                            if (isInSeatList(objectId))
                                add(MessageEventsEnum.REFRESH_STREAMERS);                                
                        }
                        break;
                }
            }
        }.get();
    }
   
    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_CHANNELS:
            case REFRESH_STREAMERS:
                refreshChannelList();
                break;
        }
    }
}
