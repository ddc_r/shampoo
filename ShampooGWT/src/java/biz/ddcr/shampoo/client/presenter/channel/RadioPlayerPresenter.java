/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.channel;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.FLAG;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.TYPE;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.queue.BroadcastableTrackQueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm;
import biz.ddcr.shampoo.client.form.queue.RequestQueueForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotVoteException;
import biz.ddcr.shampoo.client.helper.errors.SessionClosedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.FloatingPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.queue.QueueRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.channel.RadioPlayerView;
import biz.ddcr.shampoo.client.view.widgets.GenericCallbackInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class RadioPlayerPresenter extends FloatingPagePresenter<RadioPlayerView> {

    /**
    * 1 second
    */
    private final static int UPDATE_CYCLE = 1000;
    
    private abstract class MergingAddRemoveWorkaroundSignalHelper extends SignalHelper {

        MergingAddRemoveWorkaroundSignalHelper(Collection<FeedbackObjectForm> messages) {
            super(messages);
        }

        /**
         * If two consecutive items are removal and then addition of a
         * streamitem, assume that it's an edition It's dirty but it helps
         * reducing the screen flickering
         *
         * @param messages
         */
        @Override
        public void process(Collection<FeedbackObjectForm> messages) {
            if (messages != null) {
                Iterator<FeedbackObjectForm> messageIterator = messages.iterator();
                if (messageIterator.hasNext()) {

                    FeedbackObjectForm previousItem = messageIterator.next();
                    FeedbackObjectForm currentItem = null;
                    while (messageIterator.hasNext()) {
                        currentItem = messageIterator.next();
                        if (previousItem.getType() == TYPE.streamitem && currentItem.getType() == TYPE.streamitem && currentItem.getFlag() == FLAG.add && previousItem.getFlag() == FLAG.delete && (currentItem.getSenderId() == null || currentItem.getSenderId().equals(previousItem.getSenderId()))) {
                            process(previousItem.getType(), previousItem.getObjectId(), FLAG.edit, previousItem.getSenderId());
                            if (messageIterator.hasNext()) {
                                previousItem = messageIterator.next();
                            }
                        } else {
                            process(previousItem.getType(), previousItem.getObjectId(), previousItem.getFlag(), previousItem.getSenderId());
                            previousItem = currentItem;
                        }
                    }
                    if (previousItem != null) {
                        process(previousItem.getType(), previousItem.getObjectId(), previousItem.getFlag(), previousItem.getSenderId());
                    }

                }
            }
        }
    }
    protected QueueRPCServiceInterfaceAsync queueService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;
    protected ChannelRPCServiceInterfaceAsync channelService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected TrackRPCServiceInterfaceAsync trackService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;
    protected HelperRPCServiceInterfaceAsync helperService;
    //Local JavaScript variable to store what's being played by the radio
    private QueueForm currentlyPlayingItem;
    //Local JavaScript variable that keeps track of the status of the form (open or close) and whether it can respond to events
    private boolean canRespondToEvents;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setQueueService(QueueRPCServiceInterfaceAsync queueService) {
        this.queueService = queueService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessListenStreamers(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {
            @Override
            public boolean isFailureCatchable(Throwable caught) {
                //We silently drop any error due to lack of credentials
                return (!(caught instanceof AccessDeniedException || caught instanceof SessionClosedException));
            }

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        //by default its state is close
        canRespondToEvents = false;
        //Change current channel
        view.getChannelComboBox().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                rebuildPlayer(view.getChannelComboBox().getSelectedValue());
            }
        });

    }

    @Override
    protected void onRefresh() {
        setTitle(I18nTranslator.getInstance().player());
        view.getDisplayPanel().clean();
        //First list the channels that are visible to the user
        refreshChannelList(true);
        //All other UI building are done once channels are retrieved and verified
    }

    private void refreshChannelList(final boolean forceRefresh) {

        //No need to check if the authenticated user can see channels, it's already been checked when the page opened
        channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()) {
            @Override
            public void onResult(ActionCollection<ChannelForm> result) {

                final ChannelForm selectedChannelForm = view.getChannelComboBox().getSelectedValue();

                view.refreshChannelList(result);
                //Only perform an update if the previous and currently selected channels are different
                //Or nothing is currently playing, so we disturb the user experience
                if (forceRefresh || !view.getChannelComboBox().setSelectedValue(selectedChannelForm)) {
                    //reset the currently playing item
                    currentlyPlayingItem = null;
                    //Update the timetable properties immediately, according to the first selected channel
                    rebuildPlayer(view.getChannelComboBox().getSelectedValue());
                }
            }

            /*@Override
             public void onInit() {
             view.getChannelComboBox().clean();
             }*/
        });
    }

    private void rebuildPlayer(final ChannelForm currentChannel) {
        stopPlayer();
        if (currentChannel == null) {
            view.getDisplayPanel().clean();
        } else {
            _rebuildPlayer(currentChannel);
        }
    }

    private void _rebuildPlayer(final ChannelForm currentChannel) {
        view.getDisplayPanel().clean();

        if (currentChannel != null) {

            channelService.getSecuredDynamicChannelStreamerMetadata(currentChannel.getLabel(), new ExtendedRPCCallback<ActionCollectionEntry<StreamerModule>>(getEventBus(), view.getDisplayPanel()) {
                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    return (!(caught instanceof AccessDeniedException));
                }

                @Override
                public void onResult(ActionCollectionEntry<StreamerModule> result) {
                    //Activate the menu entries if access is granted
                    if (result != null) {
                        //Rebuild the UI
                        view.refreshDisplayPanel(result, eventBus);
                        refreshPlayingItem(currentChannel);
                    }
                }
            });

        }
        refreshStreamerStatus(currentChannel);
    }

    private void prepareTimeTrackingPanel(final QueueForm form, final GenericPanelInterface container) {

        //We cannot trust the client's clock, so compute the accurate current track position server-side
        helperService.diffNowMilliseconds(form.getRefId().getSchedulingTime(), new ExtendedRPCCallback<Long>(getEventBus(), container) {
            @Override
            public void onResult(Long result) {
                container.clean();
                if (result != null) {
                    view.addTimetracker(result, form.getDuration().getMilliseconds(), UPDATE_CYCLE, container);
                }
            }

            /*@Override
             public void onInit() {
             super.onInit();
             container.clean();
             }*/
        });
    }

    protected void refreshPlayingItem(final ChannelForm currentChannel) {

        if (view.getItemPanel() != null) {
            final GenericCallbackInterface<QueueForm, GenericPanelInterface, Void> timeTrackerHandler = new GenericCallbackInterface<QueueForm, GenericPanelInterface, Void>() {
                @Override
                public void onCallBack(QueueForm form, GenericPanelInterface container, Void reserved) {
                    prepareTimeTrackingPanel(form, container);
                }
            };

            final ClickHandler trackDisplayClickHandler = new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    QueueForm item = currentlyPlayingItem;
                    if (item != null && (item instanceof BroadcastableTrackQueueForm || item instanceof RequestQueueForm)) {
                        trackService.getSecuredBroadcastTrack(((BroadcastableTrackQueueForm) item).getTrackId(), new ExtendedRPCCallback<ActionCollectionEntry<BroadcastTrackForm>>(getEventBus(), null) {
                            @Override
                            public void onResult(final ActionCollectionEntry<BroadcastTrackForm> result) {
                                if (result != null && result.isReadable()) {
                                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKDISPLAY, result.getItem());
                                }
                            }
                        });
                    }
                }
            };
            final ClickHandler programmeDisplayClickHandler = new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    QueueForm item = currentlyPlayingItem;
                    if (item != null && item.getProgrammeLabel() != null) {
                        programmeService.getSecuredProgramme(item.getProgrammeLabel(), new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {
                            @Override
                            public void onResult(final ActionCollectionEntry<ProgrammeForm> result) {
                                if (result != null && result.isReadable()) {
                                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                                }
                            }
                        });
                    }
                }
            };
            final VoteChangeHandler<String> myRatingChangeHandler = new VoteChangeHandler<String>() {
                @Override
                public void onSet(GenericMyVoteBoxInterface<String> source) {
                    updateMyVote(source);
                }

                @Override
                public void onGet(GenericMyVoteBoxInterface<String> source) {
                    fetchMyVote(source);
                }
            };

            if (currentChannel == null) {
                view.getItemPanel().clean();
            } else {
                queueService.getSecuredNowPlayingItemForChannelId(currentChannel.getLabel(), new ExtendedRPCCallback<ActionCollectionEntry<? extends QueueForm>>(getEventBus(), view.getItemPanel()) {
                    @Override
                    public void onResult(final ActionCollectionEntry<? extends QueueForm> result) {
                        currentlyPlayingItem = result != null ? result.getItem() : null;
                        view.refreshStreamItem(//
                                result,//
                                timeTrackerHandler,//
                                myRatingChangeHandler,//
                                trackDisplayClickHandler,//
                                programmeDisplayClickHandler);
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        super.onFailure(caught);
                        currentlyPlayingItem = null;
                    }
                });
            }
        }
    }

    /**
     *
     * Update a song rating with the data provided by a Rating widget, for the
     * currently authenticated user
     *
     * @param ratingWidget
     *
     */
    private void updateMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.setCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), voteWidget.getMyVote(), new ExtendedRPCCallback<Void>(getEventBus(), voteWidget) {
                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(Void result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().vote_registered());
                    //And refresh the back page
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    /**
     *
     * Retrieve the current user vote for a song, fetched as soon as the
     * component is drawn on screen
     *
     * @param voteWidget
     *
     */
    private void fetchMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.getCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), new ExtendedRPCCallback<VOTE>(getEventBus(), voteWidget) {
                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(VOTE result) {
                    voteWidget.setMyVote(result);
                }
            });
        }
    }

    /**
     *
     * Retrieve the current channel/streamer connection status, fetched as soon
     * as the component is drawn on screen
     *
     * @param voteWidget
     *
     */
    private void refreshStreamerStatus(final ChannelForm currentChannel) {

        if (currentChannel != null) {
            channelService.getSecuredDynamicChannelStreamerMetadata(currentChannel.getLabel(), new ExtendedRPCCallback<ActionCollectionEntry<StreamerModule>>(getEventBus(), view.getStreamerStatusWidget()) {
                @Override
                public void onResult(ActionCollectionEntry<StreamerModule> result) {
                    view.refreshStreamerStatus(currentChannel.getLabel(), result);
                }
            });
        } else {
            view.refreshStreamerStatus(null, null);
        }
    }

    /**
     * Check whether the id given by the GUI event signal corresponds to the
     * currently playing item
     */
    private boolean isPlayingItem(String id) {
        QueueForm queueForm = currentlyPlayingItem;
        if (id == null) {
            return queueForm == null;
        }
        return id.equals(queueForm != null ? queueForm.getRefId().getRefID() : null) ? true : false;
    }

    /**
     * Check whether the id given by the GUI event signal corresponds to the
     * track represented by the currently playing item
     */
    private boolean isPlayingTrack(String id) {
        final QueueForm queueForm = currentlyPlayingItem;
        if (id == null || queueForm == null || !(queueForm instanceof BroadcastableTrackQueueForm)) {
            return false;
        }
        return id.equals(((BroadcastableTrackQueueForm) queueForm).getTrackId());
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        //Do only bother processing messages if the view can be viewed on screen
        return canRespondToEvents ? new MergingAddRemoveWorkaroundSignalHelper(messages) {
            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case streamer:
                            if (objectId == null || (view.getChannelComboBox().getSelectedValue() != null && view.getChannelComboBox().getSelectedValue().getSeatNumber()!=null && objectId.equals(view.getChannelComboBox().getSelectedValue().getSeatNumber().getItem().toString()))) {
                                add(MessageEventsEnum.REFRESH_STREAMERS);
                            } else if (view.getChannelComboBox().getSelectedValue() == null) {
                                //Workaround when a channel has just turned on and none was available beforehand
                                add(MessageEventsEnum.REFRESH_CHANNELS);
                            }
                        break;
                    case channel:
                        if (flag != FeedbackObjectForm.FLAG.edit) {
                            add(MessageEventsEnum.REFRESH_CHANNELS);
                        } else if (objectId == null || (view.getChannelComboBox().getSelectedValue() != null && objectId.equals(view.getChannelComboBox().getSelectedValue().getLabel()))) {
                            add(MessageEventsEnum.REFRESH_CHANNELS);
                        }
                        break;
                    case streamitem:
                        if (flag != FeedbackObjectForm.FLAG.edit) {
                            //LIMITATION can detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_QUEUES);
                        } else {
                            if (isPlayingItem(objectId)) {
                                add(MessageEventsEnum.REFRESH_QUEUES);
                            }
                        }
                        break;
                    case broadcasttrack:
                        if (flag == FeedbackObjectForm.FLAG.edit) {
                            if (isPlayingTrack(objectId)) {
                                add(MessageEventsEnum.REFRESH_QUEUES);
                            }
                        }
                        break;
                }
            }
        }.get() : null;
    }

    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_STREAMERS:
                /*refreshStreamerStatus(
                        view.getChannelComboBox().getSelectedValue());*/
                _rebuildPlayer(view.getChannelComboBox().getSelectedValue());
                break;
            case REFRESH_BROADCASTTRACKS:
            case REFRESH_QUEUES:
                refreshPlayingItem(
                        view.getChannelComboBox().getSelectedValue());
                break;
            case REFRESH_CHANNELS:
                refreshChannelList(false);
                break;
        }
    }

    public void onClose() {
        close();
    }

    @Override
    public void onMinimize() {
        canRespondToEvents = false;
        stopPlayer();
    }

    protected void stopPlayer() {
        //Stop playing right now
        if (view.getPlayer() != null && view.getPlayer().isPlaying()) {
            view.getPlayer().playStop();
        }
    }

    @Override
    public void onRestore() {
        canRespondToEvents = true;        
        onRefresh();
    }
}
