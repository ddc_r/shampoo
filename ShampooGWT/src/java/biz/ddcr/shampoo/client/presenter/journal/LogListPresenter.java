/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.journal;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.journal.JournalRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.journal.LogListView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class LogListPresenter extends PagePresenterWithHistory<LogListView> {

    private JournalRPCServiceInterfaceAsync journalService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setJournalService(JournalRPCServiceInterfaceAsync journalService) {
        this.journalService = journalService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewJournals(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        //Build all the tabs
        refreshLists();
    }

    protected void refreshLists() {
        view.getLogPanel().clean();

        //Add the Working Users tab
        //Add the header click handler
        final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshLogList();
            }
        };
        //Rebuild the UI
        view.refreshLogPanel(workingPanelHeaderClickHandler);
        //And update it immediately
        refreshLogList();

    }

    protected void refreshLogList() {
        final YesNoClickHandler deleteAllLogsClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllLogs(view.getLogList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteLogClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getLogList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteLog(view.getLogList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        journalService.getSecuredLogs(view.getLogList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<? extends LogForm>>(getEventBus(), view.getLogList()) {

            @Override
            public void onResult(ActionCollection<? extends LogForm> result) {
                view.refreshLogList(result,
                        deleteLogClickHandler,
                        deleteAllLogsClickHandler);
            }

            /*@Override
            public void onInit() {
                view.getLogList().clean();
            }*/
        });
    }

    private void deleteLog(final GenericFormTableInterface list, final int currentRow) {
        final LogForm log = (LogForm) list.getValueAt(currentRow);
        if (log != null) {
            journalService.deleteSecuredLog(log.getRefID(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().log_deleted(log.getRefID()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_LOGS);
                }
            });
        }
    }

    private void deleteAllLogs(final GenericFormTableInterface list) {
        final Collection<LogForm> logs = list.getAllCheckedValues();
        if (logs != null && !logs.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (LogForm form : logs) {
                ids.add(form.getRefID());
            }
            journalService.deleteSecuredLogs(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[logs.size()];
                    int i = 0;
                    for (LogForm log : logs) {
                        ids[i] = log.getRefID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().logs_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_LOGS);
                }
            });
        }
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case journal:
                        add(MessageEventsEnum.REFRESH_LOGS);
                        break;
                }
            }
        }.get();
    }

    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_LOGS:
                refreshLogList();
                break;
        }
    }
}
