/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.notification;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.notification.NotificationForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.notification.NotificationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.notification.NotificationListView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class NotificationListPresenter extends PagePresenterWithHistory<NotificationListView> {

    private NotificationRPCServiceInterfaceAsync notificationService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setNotificationService(NotificationRPCServiceInterfaceAsync notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewNotifications(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        //Build all the tabs
        refreshLists();
    }

    protected void refreshLists() {
        view.getMessagePanel().clean();

        //Add the Working Users tab
        //Add the header click handler
        final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshMessageList();
            }
        };
        //Rebuild the UI
        view.refreshMessagePanel(workingPanelHeaderClickHandler);
        //And update it immediately
        refreshMessageList();

    }

    protected void refreshMessageList() {
        final YesNoClickHandler deleteAllMessagesClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllMessages(view.getMessageList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteMessageClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getMessageList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteMessage(view.getMessageList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        notificationService.getSecuredNotifications(view.getMessageList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<? extends NotificationForm>>(getEventBus(), view.getMessageList()) {

            @Override
            public void onResult(ActionCollection<? extends NotificationForm> result) {
                view.refreshMessageList(result,
                        deleteMessageClickHandler,
                        deleteAllMessagesClickHandler);
            }

            /*@Override
            public void onInit() {
                view.getMessageList().clean();
            }*/
            
        });
    }

    private void deleteMessage(final GenericFormTableInterface list, final int currentRow) {
        final NotificationForm message = (NotificationForm) list.getValueAt(currentRow);
        if (message!=null)
        notificationService.deleteSecuredNotification(message.getRefID(), new ExtendedRPCCallback(getEventBus(), list) {

            @Override
            public void onResult(Object result) {
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().notification_deleted(message.getRefID()));
                //Refresh the corresponding list
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_NOTIFICATIONS);
            }
        });
    }

    private void deleteAllMessages(final GenericFormTableInterface list) {
        final Collection<NotificationForm> messages = list.getAllCheckedValues();
        if (messages != null && !messages.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (NotificationForm form : messages) {
                ids.add(form.getRefID());
            }
            notificationService.deleteSecuredNotifications(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[messages.size()];
                    int i = 0;
                    for (NotificationForm message : messages) {
                        ids[i] = message.getRefID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().notifications_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_NOTIFICATIONS);
                }
            });
        }
    }

    private boolean isInNotificationList(String id) {
        if (view.getMessageList() != null) {
            for (NotificationForm notificationForm : view.getMessageList().getAllValues()) {
                if (notificationForm.getRefID() != null && notificationForm.getRefID().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case notification:
                        if (flag == FeedbackObjectForm.FLAG.add) {
                            //LIMITATION cannot detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_NOTIFICATIONS);
                        } else {
                            if (isInNotificationList(objectId)) {
                                add(MessageEventsEnum.REFRESH_NOTIFICATIONS);
                            }
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_NOTIFICATIONS:
                refreshMessageList();
                break;
        }
    }
}
