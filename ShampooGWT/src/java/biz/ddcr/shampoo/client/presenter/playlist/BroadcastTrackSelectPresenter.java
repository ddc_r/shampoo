/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.playlist;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotVoteException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.playlist.BroadcastTrackSelectView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackSelectPresenter extends PopupPagePresenterWithFormParameter<BroadcastTrackSelectView, BroadcastTrackSelectTransferObject> {

    protected TrackRPCServiceInterfaceAsync trackService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;
    protected HelperRPCServiceInterfaceAsync helperService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        //Check if user can view tracks AND the programme passed as a parameter
        if (getForm() != null && getForm().playlistProgrammeId != null) {
            authorizationService.checkAccessViewTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        authorizationService.checkAccessViewPlaylist(getForm().playlistProgrammeId, new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                            @Override
                            public void onResult(Boolean result) {
                                if (result) {
                                    bindPage();
                                } else {
                                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                                }
                            }
                        });
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        view.getBroadcastTrackList().setRefreshClickHandler(new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshBroadcastTrackList();
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    @Override
    protected void onRefresh() {
        view.getBroadcastTrackList().clean();
        //Build the track list
        //And ask for a refresh as soon as it's done
        refreshBroadcastTrackList();
        //No navigation bar links to refresh
    }

    protected void refreshBroadcastTrackList() {

        //Okay the list must have been initialized beforhand, otherwise it would likely mean that either another tab panel is selected or that the user is denied acces to it
        if (view.getBroadcastTrackList() != null) {

            final VoteChangeHandler<String> myRatingChangeHandler = new VoteChangeHandler<String>() {

                @Override
                public void onSet(GenericMyVoteBoxInterface<String> source) {
                    updateMyVote(source);
                }

                @Override
                public void onGet(GenericMyVoteBoxInterface<String> source) {
                    fetchMyVote(source);
                }
            };
            final ClickHandler displayTrackClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BroadcastTrackForm track = view.getBroadcastTrackList().getValueAt(view.getBroadcastTrackList().getCurrentRow(event));
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKDISPLAY, track);
                }
            };
            final SerializableItemSelectionHandler<String> displayProgrammeSelectionHandler = new SerializableItemSelectionHandler<String>() {

                @Override
                public void onItemSelected(String currentValue) {
                    if (currentValue != null) {
                        programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                    }
                }
            };
            final ClickHandler selectClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BroadcastTrackForm track = view.getBroadcastTrackList().getValueAt(view.getBroadcastTrackList().getCurrentRow(event));

                    if (track != null) {
                        //Then close popup
                        closePopup();
                        //And send back the selected track, embedded within the original form
                        getForm().trackForm = track;
                        //Send the feedback to whatever the original caller specified
                        getEventBus().dispatch(getForm().callerRedirectionMessage, getForm());
                    }

                }
            };

            trackService.getSecuredBroadcastTracksForProgrammeId(view.getBroadcastTrackList().getCurrentGroupAndSortParameters(), getForm().playlistProgrammeId, new ExtendedRPCCallback<ActionCollection<? extends BroadcastTrackForm>>(getEventBus(), view.getBroadcastTrackList()) {

                /*@Override
                public void onInit() {
                    view.getBroadcastTrackList().clean();
                }*/

                @Override
                public void onResult(ActionCollection<? extends BroadcastTrackForm> result) {
                    _refreshBroadcastTrackList(
                            result,
                            selectClickHandler,
                            myRatingChangeHandler,
                            displayTrackClickHandler,
                            displayProgrammeSelectionHandler,
                            eventBus);
                }
            });
        }
    }

    private void _refreshBroadcastTrackList(
            final ActionCollection<? extends BroadcastTrackForm> tracks,
            final ClickHandler selectClickHandler,
            final VoteChangeHandler<String> myRatingChangeHandler,
            final ClickHandler displayTrackClickHandler,
            final SerializableItemSelectionHandler<String> displayProgrammeSelectionHandler,
            final EventBusWithLookup _eventBus) {
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), view.getBroadcastTrackList()) {

            @Override
            public void onResult(String result) {
                        view.refreshBroadcastTrackList(
                            tracks,
                            selectClickHandler,
                            myRatingChangeHandler,
                            displayTrackClickHandler,
                            displayProgrammeSelectionHandler,
                            _eventBus,
                            result);
            }
        });

    }

    /**
     * 
     * Update a song rating with the data provided by a Rating widget, for the currently authenticated user
     * 
     * @param ratingWidget
     **/
    private void updateMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.setCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), voteWidget.getMyVote(), new ExtendedRPCCallback<Void>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(Void result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().vote_registered());
                    //And refresh the back page
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    /**
     * 
     * Retrieve the current user vote for a song, fetched as soon as the component is drawn on screen
     * 
     * @param voteWidget
     **/
    private void fetchMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.getCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), new ExtendedRPCCallback<VOTE>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(VOTE result) {
                    voteWidget.setMyVote(result);
                }
            });
        }
    }

    /** Check whether the id given by the GUI event signal corresponds to a channel in the view list **/
    private boolean isInTrackList(String id) {
        for (BroadcastTrackForm trackForm : view.getBroadcastTrackList().getAllValues()) {
            if (trackForm.getRefID().equals(id))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case broadcasttrack:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                        } else {
                            if (isInTrackList(objectId))
                                add(MessageEventsEnum.REFRESH_BROADCASTTRACKS);                                
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_BROADCASTTRACKS:
                refreshBroadcastTrackList();
                break;
        }
    }
}
