/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.playlist;

import biz.ddcr.shampoo.client.InterWindowEventsEnum;
import biz.ddcr.shampoo.client.form.GenericFormInterface;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;

/**
 * Used as a transfer object between two presenters via the even bus.
 * Some attributes may only be used as inputs, other as outputs, a few both ways.
 * Check the corresponding source code for more info.
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BroadcastTrackSelectTransferObject implements GenericFormInterface {
    /** input only **/
    public String playlistProgrammeId;
    /** input only **/
    public int entryNumber;
    /** input only **/
    public InterWindowEventsEnum callerRedirectionMessage;
    /** input/output **/
    public BroadcastTrackForm trackForm;

    public BroadcastTrackSelectTransferObject(String playlistProgrammeId, int entryNumber, InterWindowEventsEnum callerRedirectionMessage, BroadcastTrackForm trackForm) {
        this.playlistProgrammeId = playlistProgrammeId;
        this.entryNumber = entryNumber;
        this.callerRedirectionMessage = callerRedirectionMessage;
        this.trackForm = trackForm;
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) return -1;
        if (o instanceof BroadcastTrackSelectTransferObject) {
            BroadcastTrackSelectTransferObject anotherForm = (BroadcastTrackSelectTransferObject)o;
            //Sort by playlistId, then by prgrammeId, then by entryNumber
            int diff = this.trackForm != null ? this.trackForm.compareTo(anotherForm.trackForm) : (anotherForm.trackForm == null ? 0 : -1);
            if (diff==0) {
                diff = this.playlistProgrammeId != null ? this.playlistProgrammeId.compareTo(anotherForm.playlistProgrammeId) : (anotherForm.playlistProgrammeId == null ? 0 : -1);
                return (diff == 0) ? this.entryNumber - anotherForm.entryNumber : diff;
            }
            return diff;
        }
        return -1;
    }

}
