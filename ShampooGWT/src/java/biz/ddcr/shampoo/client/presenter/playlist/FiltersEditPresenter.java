/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.playlist;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.playlist.FiltersEditView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * @author okay_awright
 **/
public class FiltersEditPresenter extends PopupPagePresenterWithFormParameter<FiltersEditView, FiltersEditTransferObject> {

    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Override
    protected void onCheckAccess() {
        //Check if user can view the programme passed as a parameter
        if (getForm() != null && getForm().playlistProgrammeId != null) {
            authorizationService.checkAccessViewPlaylist(getForm().playlistProgrammeId, new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) {
                        bindPage();
                    } else {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //submit
        view.getOkLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                submit();
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    @Override
    protected void onRefresh() {
        //Build the track list
        //And ask for a refresh as soon as it's done
        view.refreshExistingFilters(getForm().filterForms);
        //No navigation bar links to refresh
    }

    protected void submit() {
        //Then close popup
        closePopup();
        //And send back the selected track, embedded within the original form
        getForm().filterForms = view.getFilterList().getAllValues();
        getEventBus().dispatch(getForm().callerRedirectionMessage, getForm());
    }

    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_PLAYLISTS:
                view.refreshExistingFilters(getForm().filterForms);
                break;
        }
    }
}
