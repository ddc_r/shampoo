/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.playlist;

import biz.ddcr.shampoo.client.InterWindowEventsEnum;
import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.DynamicPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.LiveForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistTagSetForm;
import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.playlist.PlaylistAddView;
import biz.ddcr.shampoo.client.view.widgets.GenericFileUploadInterface.UploadHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class PlaylistAddPresenter extends PopupPagePresenterWithFormParameter<PlaylistAddView, ProgrammeForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private TrackRPCServiceInterfaceAsync trackService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private HelperRPCServiceInterfaceAsync helperService;
    //private flags to keep track of what's been already uploaded
    private String currentCoverArtUploadId = null;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the playlist programme to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessAddPlaylist(getForm().getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Set up the cover art upload widget with presenter data
        setAllowedPictureFormats();
        //Get a fresh Java Session identifier
        rebindServletSessionIdToApplet();                
    }

    @Override
    public void bindView() {

        //Set up the cover art upload widget with presenter data
        view.getAlbumCoverUploadWidget().setEventBus(eventBus);
        view.getAlbumCoverUploadWidget().setUploadHandler(new UploadHandler() {

            @Override
            public void onSuccess(String uploadId) {
                setNewCoverArtPicture(uploadId);
            }

            @Override
            public void onFailure() {
                resetCoverArtPicture();
            }
        });
        view.getResetAlbumCoverArtLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetCoverArtPicture();
            }
        });
        view.getAlbumCoverPicture().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Directly call the upload method from within the upload applet
                view.getAlbumCoverUploadWidget().triggerUpload();
            }
        });

        //playlist widget hook
        view.getPlaylistContent().setSlotRefreshHandler(new SlotRefreshHandler<ActionCollectionEntry<String>, Collection<FilterForm>>() {

            @Override
            public void onTrackCaptionRefresh(ActionCollectionEntry<String> form, GenericPanelInterface container) {
                refreshStaticTrackContainer(form, container);
            }

            @Override
            public void onFiltersCaptionRefresh(Collection<FilterForm> form, GenericPanelInterface container) {
                refreshFiltersContainer(form, container);
            }
        });

        //dependencies
        view.getNoReplayTimeSelector().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.onNoReplayTimeSelectorClicker();
            }
        });
        view.getEnableGlobalTagSetCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.toggleActivateGlobalTagSet();
            }
        });
        view.getAdvisoryAgeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                view.toggleAdvisoryFeaturesPanel();
            }
        });
        //Form validation handlers
        view.getAuthorTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (view.getEnableGlobalTagSetCheckBox().isChecked())
                    Validation.bindValidationError(Validation.checkLabel(view.getAuthorTextBox().getText()), view.getAuthorTextBox());
            }
        });
        view.getTitleTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (view.getEnableGlobalTagSetCheckBox().isChecked())
                    Validation.bindValidationError(Validation.checkLabel(view.getTitleTextBox().getText()), view.getTitleTextBox());
            }
        });

        //Add new timetable slot
        view.getAddNewTimetableSlotButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewTimetableSlot(view.getTimetableSlotComboBox().getSelectedValue());
            }
        });

        //standard behaviours
        view.getEnableLiveCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.toggleActivateLive();
            }
        });

        view.getLiveLoginBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkUsername(view.getLiveLoginBox().getText()), view.getLiveLoginBox());
            }
        });

        view.getLivePasswordBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getLivePasswordBox().getText()), view.getLivePasswordBox());
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public void onUpdatePlaylistSingleBroadcastTrack(BroadcastTrackSelectTransferObject transferObject) {
        if (transferObject != null) {
            //Update the current form entry from the playlist with the new data
            PlaylistEntryForm oldForm;
            if (view.getPlaylistContent().isTemplateSlotAt(transferObject.entryNumber)) {
                oldForm = view.getPlaylistContent().getTemplateForm();
            } else {
                oldForm = view.getPlaylistContent().getFormAt(transferObject.entryNumber);
            }

            //Just to be on the safe side
            if (oldForm == null) {
                oldForm = new StaticPlaylistEntryForm();
            }
            if (oldForm instanceof StaticPlaylistEntryForm && transferObject.trackForm != null) {
                ((StaticPlaylistEntryForm) oldForm).setBroadcastTrackId(new ActionMapEntry<String>(transferObject.trackForm.getRefID()));

                //reinject the form
                if (view.getPlaylistContent().isTemplateSlotAt(transferObject.entryNumber)) {
                    view.getPlaylistContent().setTemplateForm(oldForm);
                } else {
                    view.getPlaylistContent().setFormAt(transferObject.entryNumber, oldForm);
                }
            }
        }
    }

    public void onUpdatePlaylistFilters(FiltersEditTransferObject transferObject) {
        if (transferObject != null) {
            //Update the current form entry from the playlist with the new data
            PlaylistEntryForm oldForm;
            if (view.getPlaylistContent().isTemplateSlotAt(transferObject.entryNumber)) {
                oldForm = view.getPlaylistContent().getTemplateForm();
            } else {
                oldForm = view.getPlaylistContent().getFormAt(transferObject.entryNumber);
            }

            //Just to be on the safe side
            if (oldForm == null) {
                oldForm = new DynamicPlaylistEntryForm();
            }
            if (oldForm instanceof DynamicPlaylistEntryForm && transferObject.filterForms != null) {
                ((DynamicPlaylistEntryForm) oldForm).setFilters(transferObject.filterForms);

                //reinject the form
                if (view.getPlaylistContent().isTemplateSlotAt(transferObject.entryNumber)) {
                    view.getPlaylistContent().setTemplateForm(oldForm);
                } else {
                    view.getPlaylistContent().setFormAt(transferObject.entryNumber, oldForm);
                }

            }

        }
    }

    public boolean validateForm() {
        boolean result = true;

        //Check if there's at least on entry within the playlist
        if (Validation.bindValidationError(Validation.checkCollectionIsSet(view.getPlaylistContent().getSlotCount()), view.getPlaylistContent())) {
            result = false;
        }

        //Check if each playlist item is valid
        for (int i = 0; i < view.getPlaylistContent().getSlotCount(); i++) {
            if (Validation.bindValidationError(Validation.checkCollectionItemIsSet(view.getPlaylistContent().validateSlotAt(i)), view.getPlaylistContent().getSlotAt(i))) {
                result = false;
            }
        }

        //Check that, if a live can be hooked in, every parameter of this live is set
        if (view.getEnableLiveCheckBox().isChecked()) {
            if (Validation.bindValidationError(Validation.checkMaxText(view.getLiveBroadcasterBox().getText()), view.getLiveBroadcasterBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkUsername(view.getLiveLoginBox().getText()), view.getLiveLoginBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkPassword(view.getLivePasswordBox().getText()), view.getLivePasswordBox())) {
                result = false;
            }
        }

        if (Validation.bindValidationError(Validation.checkMaxText(view.getLabelBox().getText()), view.getLabelBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionBox().getText()), view.getDescriptionBox())) {
            result = false;
        }

        //Check if global tag set are activated
        if (view.getEnableGlobalTagSetCheckBox().isChecked()) {
            if (Validation.bindValidationError(Validation.checkLabel(view.getAuthorTextBox().getText()), view.getAuthorTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkLabel(view.getTitleTextBox().getText()), view.getTitleTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkMaxText(view.getAlbumTextBox().getText()), view.getAlbumTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionTextBox().getText()), view.getDescriptionTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkMaxText(view.getGenreTextBox().getText()), view.getGenreTextBox())) {
                result = false;
            }
        }

        return result;
    }

    private void submitForm() {
        //Build a new Playlist from the view
        final PlaylistForm newPlaylist = new PlaylistForm();

        newPlaylist.setDescription(view.getDescriptionBox().getText());
        newPlaylist.setLabel(view.getLabelBox().getText());
        newPlaylist.setProgrammeId(new ActionMapEntry<String>(getForm().getLabel()));
        newPlaylist.setPlaylistEntries(view.getPlaylistContent().getAllForms());
        if (view.getEnableLiveCheckBox().isChecked()) {
            LiveForm newLive = new LiveForm();
            newLive.setBroadcaster(view.getLiveBroadcasterBox().getText());
            newLive.setLogin(view.getLiveLoginBox().getText());
            newLive.setPassword(view.getLivePasswordBox().getText());

            PEGIRatingModule liveRating = null;
            if (view.getLiveAdvisoryAgeComboBox().getSelectedValue() != null) {
                liveRating = new PEGIRatingModule();
                liveRating.setRating(view.getLiveAdvisoryAgeComboBox().getSelectedValue());
                liveRating.setDiscrimination(view.getLiveAdvisoryDiscriminationCheckBox().isChecked());
                liveRating.setDrugs(view.getLiveAdvisoryDrugsCheckBox().isChecked());
                liveRating.setSex(view.getLiveAdvisorySexCheckBox().isChecked());
                liveRating.setViolence(view.getLiveAdvisoryViolenceCheckBox().isChecked());
                liveRating.setFear(view.getLiveAdvisoryFearCheckBox().isChecked());
                liveRating.setProfanity(view.getLiveAdvisoryProfanityCheckBox().isChecked());
            }
            newLive.setAdvisory(liveRating);

            newPlaylist.setLive(newLive);
        }
        newPlaylist.setMaxUserRequestLimit(view.getMaxNumberOfUserRequestsSelector().getSelectedValue());
        newPlaylist.setNoRequestReplayInPlaylist(view.getNoReplayPlaylistCheckBox().isChecked());
        newPlaylist.setNoRequestReplayDelay(view.getNoReplayTimeSelector().isChecked() ? view.getNoReplayTimeThresholdBox().getValue().longValue() : null);

        //cover art
        //CoverArtModule is not read when adding a new playlist: the id of the file to bind is passed as a parameter to the insert callback instead

        //Convert a list to a set
        ActionMap<TimetableSlotFormID> tts = new ActionSortedMap<TimetableSlotFormID>();
        tts.addAllNude(view.getTimetableSlotListBox().getAllValues());
        newPlaylist.setTimetableSlotIds(tts);

        //Global playlist tags
        PlaylistTagSetForm tags = null;
        if (view.getEnableGlobalTagSetCheckBox().isChecked()) {
            tags = new PlaylistTagSetForm();
            PEGIRatingModule rating = null;
            if (view.getAdvisoryAgeComboBox().getSelectedValue() != null) {
                rating = new PEGIRatingModule();
                rating.setRating(view.getAdvisoryAgeComboBox().getSelectedValue());
                rating.setDiscrimination(view.getAdvisoryDiscriminationCheckBox().isChecked());
                rating.setDrugs(view.getAdvisoryDrugsCheckBox().isChecked());
                rating.setSex(view.getAdvisorySexCheckBox().isChecked());
                rating.setViolence(view.getAdvisoryViolenceCheckBox().isChecked());
                rating.setFear(view.getAdvisoryFearCheckBox().isChecked());
                rating.setProfanity(view.getAdvisoryProfanityCheckBox().isChecked());
            }
            tags.setAdvisory(rating);
            tags.setAlbum(view.getAlbumTextBox().getText());
            tags.setAuthor(view.getAuthorTextBox().getText());
            tags.setDescription(view.getDescriptionTextBox().getText());
            tags.setGenre(view.getGenreTextBox().getText());
            tags.setTitle(view.getTitleTextBox().getText());
            tags.setYearOfRelease(view.getYearOfReleaseNumericBox().getValue() != null ? view.getYearOfReleaseNumericBox().getValue().shortValue() : null);
        }
        newPlaylist.setGlobalTagSet(tags);

        //Make it persistant
        ExtendedRPCCallback<Void> callback = new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().playlist_added(newPlaylist.getFriendlyID()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PLAYLISTS);
            }
        };
        if (currentCoverArtUploadId!=null)
            programmeService.addSecuredPlaylist(newPlaylist, currentCoverArtUploadId, callback);
        else
            programmeService.addSecuredPlaylist(newPlaylist, callback);
    }

    @Override
    protected void onRefresh() {
        initializeCoverArtAppletURL();

        resetCoverArtPicture();

        //Reset everything
        view.getProgrammeLabel().clean();
        view.getProgrammeLabel().setCaption(getForm().getLabel());

        view.getDescriptionBox().clean();
        view.getLabelBox().clean();
        view.getLiveBroadcasterBox().clean();
        view.getLiveLoginBox().clean();
        view.getLivePasswordBox().clean();

        //Clear timetable slot list
        view.getTimetableSlotComboBox().clean();
        view.getTimetableSlotListBox().clean();
        //Update timetable list
        feedTimetableSlotList();
        //Update other misc options
        view.getMaxNumberOfUserRequestsSelector().clean();
        view.refreshUserRequestsLimitList();
        view.getNoReplayPlaylistCheckBox().clean();
        view.getNoReplayTimeSelector().clean();
        view.getNoReplayTimeThresholdBox().clean();
        view.onNoReplayTimeSelectorClicker();

        //Clear plylist content
        view.getPlaylistContent().clean();

        view.getLiveAdvisoryAgeComboBox().clean();
        feedLiveAdvisoryAgeList();
        view.toggleLiveAdvisoryFeaturesPanel();
        view.getLiveAdvisoryViolenceCheckBox().clean();
        view.getLiveAdvisoryViolenceCheckBox().setChecked(false);
        view.getLiveAdvisoryProfanityCheckBox().clean();
        view.getLiveAdvisoryProfanityCheckBox().setChecked(false);
        view.getLiveAdvisoryFearCheckBox().clean();
        view.getLiveAdvisoryFearCheckBox().setChecked(false);
        view.getLiveAdvisorySexCheckBox().clean();
        view.getLiveAdvisorySexCheckBox().setChecked(false);
        view.getLiveAdvisoryDrugsCheckBox().clean();
        view.getLiveAdvisoryDrugsCheckBox().setChecked(false);
        view.getLiveAdvisoryDiscriminationCheckBox().clean();
        view.getLiveAdvisoryDiscriminationCheckBox().setChecked(false);

        view.toggleActivateLive();

        //Misc. options
        view.getEnableGlobalTagSetCheckBox().setChecked(false);
        view.toggleActivateGlobalTagSet();
        view.getAuthorTextBox().clean();
        view.getTitleTextBox().clean();
        view.getAlbumTextBox().clean();
        view.getDescriptionTextBox().clean();
        view.getYearOfReleaseNumericBox().clean();
        view.getGenreTextBox().clean();
        view.getAdvisoryAgeComboBox().clean();
        feedAdvisoryAgeList();
        view.toggleAdvisoryFeaturesPanel();
        view.getAdvisoryViolenceCheckBox().clean();
        view.getAdvisoryViolenceCheckBox().setChecked(false);
        view.getAdvisoryProfanityCheckBox().clean();
        view.getAdvisoryProfanityCheckBox().setChecked(false);
        view.getAdvisoryFearCheckBox().clean();
        view.getAdvisoryFearCheckBox().setChecked(false);
        view.getAdvisorySexCheckBox().clean();
        view.getAdvisorySexCheckBox().setChecked(false);
        view.getAdvisoryDrugsCheckBox().clean();
        view.getAdvisoryDrugsCheckBox().setChecked(false);
        view.getAdvisoryDiscriminationCheckBox().clean();
        view.getAdvisoryDiscriminationCheckBox().setChecked(false);

        view.getSubmit().clean();
    }

    private void initializeCoverArtAppletURL() {
        
        helperService.getTemporaryPictureUploadURL(new ExtendedRPCCallback<String>(getEventBus(), view.getAlbumCoverUploadWidget()) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setAbsolutePostURL(result);
                
            }
        });
    }

    private void feedAdvisoryAgeList() {
        //refresh the available PEGI rating system
        PEGI_AGE[] ages = new PEGI_AGE[6];
        //Default value is null, i.e. unselected
        ages[0] = null;
        ages[1] = PEGI_AGE.earlyChildhood;
        ages[2] = PEGI_AGE.everyone;
        ages[3] = PEGI_AGE.teen;
        ages[4] = PEGI_AGE.mature;
        ages[5] = PEGI_AGE.adultsOnly;
        view.refreshAdvisoryAgeList(ages);
    }
    private void feedLiveAdvisoryAgeList() {
        //refresh the available PEGI rating system
        PEGI_AGE[] ages = new PEGI_AGE[6];
        //Default value is null, i.e. unselected
        ages[0] = null;
        ages[1] = PEGI_AGE.earlyChildhood;
        ages[2] = PEGI_AGE.everyone;
        ages[3] = PEGI_AGE.teen;
        ages[4] = PEGI_AGE.mature;
        ages[5] = PEGI_AGE.adultsOnly;
        view.refreshLiveAdvisoryAgeList(ages);
    }

    private void setAllowedPictureFormats() {
        helperService.getAllowedPictureFormatExtensions(new ExtendedRPCCallback<Collection<String>>(getEventBus(), null) {

            @Override
            public void onResult(Collection<String> types) {
                view.getAlbumCoverUploadWidget().setAllowedFileExtensions(types);
            }
        });
    }

    private void setNewCoverArtPicture(String uploadId) {
        currentCoverArtUploadId = uploadId;
        //Make the "reset" link visible
        view.getResetAlbumCoverArtLink().setVisible(true);
        //Update the picture
        fetchCoverArtInfo();
    }

    private void resetCoverArtPicture() {
        currentCoverArtUploadId = null;
        //Clean the upload box
        view.getAlbumCoverUploadWidget().clean();
        //Make the "reset" and "fill tags" links invisble
        view.getResetAlbumCoverArtLink().setVisible(false);
        //Reset the picture to an unindentified image
        view.unloadCoverArt();
    }

    private void fetchCoverArtInfo() {
        if (currentCoverArtUploadId != null) {
            trackService.getDraftPictureFileInfo(currentCoverArtUploadId, new ExtendedRPCCallback<CoverArtModule>(getEventBus(), view.getAlbumCoverPicture()) {

                @Override
                public void onResult(CoverArtModule result) {
                    view.loadCoverArt(result);
                }
            });
        }
    }

    private void feedTimetableSlotList() {
        channelService.getSecuredTimetableSlotsForProgrammeId(null, getForm().getLabel(), new ExtendedRPCCallback<ActionCollection<TimetableSlotForm>>(getEventBus(), view.getTimetableSlotComboBox()) {

            @Override
            public void onResult(ActionCollection<TimetableSlotForm> result) {
                view.refreshTimetableSlotList(result);
            }
        });
    }

    protected void refreshStaticTrackContainer(ActionCollectionEntry<String> track, final GenericPanelInterface container) {
        final ClickHandler changeTrackClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                int currentIndex = view.getPlaylistContent().getCurrentRow(event);
                BroadcastTrackSelectTransferObject transferObject = new BroadcastTrackSelectTransferObject(
                        //The programme identifier of the current playlist
                        getForm().getLabel(),
                        //The position of the entry within the playlist to update once done
                        currentIndex,
                        //Force the feeback message to be broadcast to the add presenter, not the edit presenter
                        InterWindowEventsEnum.SEND_ADD_PLAYLIST_SINGLEBROADCASTTRACKSELECT,
                        //The currently bound track to this entry; make it null it will be instantiated within the responder
                        null);
                getEventBus().dispatch(InterWindowEventsEnum.SHOW_PLAYLIST_SINGLEBROADCASTTRACKSELECT, transferObject);
            }
        };

        if (track != null) {
            trackService.getSecuredBroadcastTrack(track.getItem(), new ExtendedRPCCallback<ActionCollectionEntry<BroadcastTrackForm>>(getEventBus(), container) {

                @Override
                public void onResult(final ActionCollectionEntry<BroadcastTrackForm> result) {
                    view.refreshSingleTrackContainer(
                            container,
                            result,
                            changeTrackClickHandler,
                            new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent ce) {
                                    //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKDISPLAY, result.getItem());
                                }
                            });
                }
            });
        } else {
            view.refreshSingleTrackContainer(
                    container,
                    null,
                    changeTrackClickHandler,
                    null);
        }
    }

    protected void refreshFiltersContainer(final Collection<FilterForm> form, GenericPanelInterface container) {
        final ClickHandler changeTracksClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                int currentIndex = view.getPlaylistContent().getCurrentRow(event);
                FiltersEditTransferObject transferObject = new FiltersEditTransferObject(
                        //The current playlist identifier
                        null/*not yet persisted*/,
                        //The programme identifier of the current playlist
                        getForm().getLabel(),
                        //The position of the entry within the playlist to update once done
                        currentIndex,
                        //Force the feeback message to be broadcast to the add presenter, not the edit presenter
                        InterWindowEventsEnum.SEND_ADD_PLAYLIST_FILTERS,
                        //The currently bound filters to this entry; make it null it will be instantiated within the responder
                        form);
                getEventBus().dispatch(InterWindowEventsEnum.SHOW_PLAYLIST_FILTERS_EDITOR, transferObject);
            }
        };
        view.refreshFiltersContainer(
                container,
                form,
                changeTracksClickHandler);
    }

    private boolean addNewTimetableSlot(final TimetableSlotForm timetableSlot) {

        //Is it already present in the list?
        if (timetableSlot == null || view.getTimetableSlotListBox().isValueIn(timetableSlot.getRefId())) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getTimetableSlotListBox().removeRow(view.getTimetableSlotListBox().getCurrentRow(event));
            }
        };
        final ClickHandler displayTimetableSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDISPLAY, timetableSlot);
            }
        };
        view.addNewTimetableSlot(timetableSlot, deleteClickHandler, displayTimetableSlotClickHandler);

        return true;

    }

    protected void rebindServletSessionIdToApplet(){
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), null) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setCurrentSessionId(result);
            }

        });
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {                    
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                    case timetableslot:
                        //If a timetable slot has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().timetables_concurrently_edited());
                }
            }
        }.get();
    }
}
