/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.playlist;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.playlist.PlaylistDisplayView;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class PlaylistDisplayPresenter extends GenericFormPopupPagePresenter<PlaylistDisplayView, PlaylistForm> {

    private TrackRPCServiceInterfaceAsync trackService;
    private ChannelRPCServiceInterfaceAsync channelService;

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the playlist programme to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewPlaylist(getForm().getProgrammeId().getItem(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {

        //playlist widget hook
        view.getPlaylistContent().setSlotRefreshHandler(new SlotRefreshHandler<ActionCollectionEntry<String>, Collection<FilterForm>>() {

            @Override
            public void onTrackCaptionRefresh(ActionCollectionEntry<String> form, GenericPanelInterface container) {
                refreshStaticTrackContainer(form, container);
            }

            @Override
            public void onFiltersCaptionRefresh(Collection<FilterForm> form, GenericPanelInterface container) {
                refreshFiltersContainer(form, container);
            }
        });

    }

    @Override
    public void onFormSpecificRefresh() {

        //Load the cover art
        loadOriginalCoverArtPicture();

        //Reset everything

        view.getProgrammeLabel().setCaption(getForm().getProgrammeId().getItem());

        view.getDescriptionLabel().setCaption(getForm().getDescription());
        view.getLabelLabel().setCaption(getForm().getLabel());
        view.refreshExistingLive(getForm().getLive());
      
        //Update timetable list
        feedTimetableSlotList();

        //Clear playlist content
        view.getPlaylistContent().clean();
        //update content
        view.getPlaylistContent().appendForms(getForm().getPlaylistEntries());

        //update misc options
        view.refreshGlobalTagsData(getForm().getGlobalTagSet());
        view.getMaxNumberOfUserRequestsLabel().setCaption(getForm().getMaxUserRequestLimit()==null ? I18nTranslator.getInstance().infinite() : getForm().getMaxUserRequestLimit().toString());

    }

    private void loadOriginalCoverArtPicture() {
        view.loadCoverArt(getForm().getCoverArtFile());
    }
    
    private void feedTimetableSlotList() {

        channelService.getSecuredTimetableSlotsForPlaylistId(null, getForm().getRefID(), new ExtendedRPCCallback<ActionCollection<TimetableSlotForm>>(getEventBus(), view.getTimetableSlotTree()) {

            @Override
            public void onResult(ActionCollection<TimetableSlotForm> result) {
                view.refreshExistingTimetableSlots(
                        result);
            }

            /*@Override
            public void onInit() {
                //Clear timetable slot list
                view.getTimetableSlotTree().clean();
            }*/
        });

    }

    protected void refreshStaticTrackContainer(ActionCollectionEntry<String> trackIdEntry, final GenericPanelInterface container) {

        if (trackIdEntry != null) {
            trackService.getSecuredBroadcastTrack(trackIdEntry.getItem(), new ExtendedRPCCallback<ActionCollectionEntry<BroadcastTrackForm>>(getEventBus(), container) {

                @Override
                public void onResult(final ActionCollectionEntry<BroadcastTrackForm> result) {
                    view.refreshSingleTrackContainer(
                            container,
                            result);
                }
            });
        } else {
            view.refreshSingleTrackContainer(
                    container,
                    /** null means no track, case handled within the view **/
                    null);
        }
    }

    protected void refreshFiltersContainer(final Collection<FilterForm> filters, GenericPanelInterface container) {
        view.refreshFiltersContainer(
                container,
                filters);
    }

}
