/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.playlist;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.playlist.PlaylistListView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class PlaylistListPresenter extends PagePresenterWithHistory<PlaylistListView> {

    protected HelperRPCServiceInterfaceAsync helperService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected ChannelRPCServiceInterfaceAsync channelService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewPlaylists(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        //Change current channel
        view.getProgrammeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                buildList(view.getProgrammeComboBox().getSelectedValue());
            }
        });

    }

    @Override
    protected void onRefresh() {
        view.getListPanel().clean();
        view.removeAllNavigationLinks();
        //First list the channels that are visible to the user
        refreshProgrammeList(true);
        //All other UI building are done once channels are retrieved and verified
    }

    protected void refreshNavigationBar(final ProgrammeForm currentProgramme) {

        view.removeAllNavigationLinks();

        final ClickHandler addTimetableClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTADD, currentProgramme);
            }
        };

        authorizationService.checkAccessAddPlaylist(currentProgramme.getLabel(), new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddPlaylistLink(addTimetableClickHandler);
                }
            }
        });

    }

    private void refreshProgrammeList(final boolean forceRefresh) {
        //No need to check if the authenticated user can see channels, it's already been checked when the page opened
        programmeService.getSecuredProgrammes(null, new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                boolean doesRefresh = forceRefresh;
                        
                ProgrammeForm selectedProgrammeForm = view.getProgrammeComboBox().getSelectedValue();
                
                //Look in the current history if a Programme has been specified
                final String historyProgrammeLabel = PlaylistListPresenter.this.getHistoryPartLevel(1);
                if (historyProgrammeLabel!=null) {
                    //create a fake programme form (label is the only business key)
                    final ProgrammeForm historyProgrammeForm = new ProgrammeForm();
                    historyProgrammeForm.setLabel(historyProgrammeLabel);
                    if (selectedProgrammeForm==null || !selectedProgrammeForm.equals(historyProgrammeForm)) {
                        doesRefresh = true;
                        selectedProgrammeForm = historyProgrammeForm;
                    }
                }
                
                view.refreshProgrammeList(result);
                //Only perform an update if the previous and currently selected programmes are different
                if (doesRefresh | !view.getProgrammeComboBox().setSelectedValue(selectedProgrammeForm)) {
                    //Update the playlist properties immediately, according to the first selected programme
                    buildList(view.getProgrammeComboBox().getSelectedValue());
                }
            }

            /*@Override
            public void onInit() {
                view.getProgrammeComboBox().clean();
            }*/
        });
    }

    /**
     * Instiantiate a new tab panel
     * Detect which of the grid or the list must be updated
     * @param currentChannel
     **/
    private void buildList(final ProgrammeForm currentProgramme) {
        if (currentProgramme == null) {
            view.getListPanel().clean();
            view.removeAllNavigationLinks();
        } else {
            refreshPlaylistPanel(currentProgramme);
            refreshNavigationBar(currentProgramme);
        }
    }

    private void refreshPlaylistPanel(final ProgrammeForm currentProgramme) {
        view.getListPanel().clean();

        if (currentProgramme != null) {

            //Add the header click handler
            final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {

                @Override
                public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                    refreshPlaylistList(currentProgramme);
                }
            };
            authorizationService.checkAccessViewPlaylist(currentProgramme.getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getListPanel()) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    return (!(caught instanceof AccessDeniedException));
                }

                @Override
                public void onResult(Boolean result) {
                    //Activate the menu entries if access is granted
                    if (result) {
                        //Rebuild the UI
                        view.refreshPlaylistPanel(workingPanelHeaderClickHandler);
                        //And update it immediately
                        refreshPlaylistList(currentProgramme);
                    }
                }
            });
        }
    }

    protected void refreshPlaylistList(final ProgrammeForm currentProgramme) {

        final ClickHandler editClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                PlaylistForm playlistForm = view.getPlaylistList().getValueAt(view.getPlaylistList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTEDIT, playlistForm);
            }
        };
        final ClickHandler playlistDisplayClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                PlaylistForm playlistForm = view.getPlaylistList().getValueAt(view.getPlaylistList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, playlistForm);
            }
        };
        final SerializableItemSelectionHandler<TimetableSlotFormID> timetableSlotDisplaySelectionHandler = new SerializableItemSelectionHandler<TimetableSlotFormID>() {

            @Override
            public void onItemSelected(TimetableSlotFormID currentValue) {
                if (currentValue!=null) {
                        channelService.getSecuredTimetableSlot(currentValue.getChannelId(), currentValue.getSchedulingTime(), new ExtendedRPCCallback<ActionCollectionEntry<TimetableSlotForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<TimetableSlotForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final YesNoClickHandler deleteAllClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllPlaylists(view.getPlaylistList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getPlaylistList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deletePlaylist(view.getPlaylistList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        programmeService.getSecuredPlaylistsForProgrammeId(view.getPlaylistList().getCurrentGroupAndSortParameters(), currentProgramme.getLabel(), new ExtendedRPCCallback<ActionCollection<PlaylistForm>>(getEventBus(), view.getPlaylistList()) {

            @Override
            public void onResult(ActionCollection<PlaylistForm> result) {
                view.refreshPlaylistList(result,
                        editClickHandler,
                        deleteClickHandler,
                        deleteAllClickHandler,
                        playlistDisplayClickHandler,
                        timetableSlotDisplaySelectionHandler);
            }

            /*@Override
            public void onInit() {
                view.getPlaylistList().clean();
            }*/
        });
    }

    private void deletePlaylist(final GenericFormTableInterface<PlaylistForm, ?> list, final int currentRow) {
        final PlaylistForm playlistForm = list.getValueAt(currentRow);
        if (playlistForm!=null)
        programmeService.deleteSecuredPlaylist(playlistForm.getRefID(), new ExtendedRPCCallback(getEventBus(), list) {

            @Override
            public void onResult(Object result) {
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().playlist_deleted(playlistForm.getFriendlyID()));
                //Refresh the corresponding list
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PLAYLISTS);
            }
        });
    }

    private void deleteAllPlaylists(final GenericFormTableInterface list) {
        final Collection<PlaylistForm> playlistForms = list.getAllCheckedValues();
        if (playlistForms != null && !playlistForms.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (PlaylistForm form : playlistForms) {
                ids.add(form.getLabel());
            }
            programmeService.deleteSecuredPlaylists(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[playlistForms.size()];
                    int i = 0;
                    for (PlaylistForm playlistForm : playlistForms) {
                        ids[i] = playlistForm.getFriendlyID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().playlists_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PLAYLISTS);
                }
            });
        }
    }
    
    /** Check whether the id given by the GUI event signal corresponds to a playlist in the view list **/
    private boolean isInPlaylistList(String id) {
        for (PlaylistForm playlistForm : view.getPlaylistList().getAllValues()) {
            if (playlistForm.getRefID().equals(id))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case programme:
                        if (flag != FeedbackObjectForm.FLAG.edit) {
                            add(MessageEventsEnum.REFRESH_PROGRAMMES);
                        } else if (objectId == null || (view.getProgrammeComboBox().getSelectedValue() != null && objectId.equals(view.getProgrammeComboBox().getSelectedValue().getLabel()))) {
                            add(MessageEventsEnum.REFRESH_PROGRAMMES);
                        }
                        break;
                    case playlist:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            //LIMITATION can detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_PLAYLISTS);
                        } else {
                            if (isInPlaylistList(objectId))
                                add(MessageEventsEnum.REFRESH_PLAYLISTS);                                
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_PROGRAMMES:
                refreshProgrammeList(false);
                break;
            case REFRESH_PLAYLISTS:
            case REFRESH_CHANNELS:
            case REFRESH_TIMETABLES:
                refreshPlaylistList(view.getProgrammeComboBox().getSelectedValue());
                break;
        }
    }    
}
