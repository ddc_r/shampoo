/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.programme;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm.FLAG;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRightForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.programme.ProgrammeEditView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeEditPresenter extends PopupPagePresenterWithFormParameter<ProgrammeEditView, ProgrammeForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private UserRPCServiceInterfaceAsync userService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private ChannelRPCServiceInterfaceAsync channelService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the programme to modify
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessUpdateProgramme(getForm().getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Add new channel
        view.getAddNewChannelButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewChannel(view.getChannelComboBox().getSelectedValue());
            }
        });

        //Add new right
        view.getAddNewRightButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewRight();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionTextBox().getText()), view.getDescriptionTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new Programme from the view
        final ProgrammeForm newProgramme = new ProgrammeForm();
        newProgramme.setDescription(view.getDescriptionTextBox().getText());
        newProgramme.setLabel(getForm().getLabel());

        //Convert a list to a set
        for (ChannelForm channel : view.getChannelsListBox().getAllValues()) {
            newProgramme.getChannels().addNude(channel.getLabel());
        }

        ActionMap<RightForm> r = new ActionSortedMap<RightForm>();
        r.addAllNude(view.getRightsListBox().getAllValues());
        newProgramme.setRightForms(r);

        newProgramme.setMetaInfoPattern(view.getMetadataPatternTextBox().getText());
        newProgramme.setOverlappingPlaylist(view.getOverlappingPlaylistCheckBox().isChecked());

        //Re-inject playlists, as-is
        newProgramme.setPlaylists(getForm().getPlaylists());
        
        //Make it persistant
        programmeService.updateSecuredProgramme(newProgramme, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().programme_updated(newProgramme.getLabel()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX                
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PROGRAMMES);
            }
        });
    }

    @Override
    protected void onRefresh() {
        //Reset everything
        view.getLabelLabel().clean();
        view.getLabelLabel().setCaption(getForm().getLabel());
        view.getDescriptionTextBox().clean();
        view.getDescriptionTextBox().setText(getForm().getDescription());
        view.getSubmit().clean();
        view.getMetadataPatternTextBox().clean();
        view.getOverlappingPlaylistCheckBox().clean();
        //Clear channels list
        view.getChannelComboBox().clean();
        //Update channels list
        feedNewChannelsList();
        view.getChannelsListBox().clean();
        feedChannelsList();
        //clear right list
        view.getRoleRightComboBox().clean();
        view.getUserRightComboBox().clean();
        //Update default metadata pattern
        view.getMetadataPatternTextBox().setText(getForm().getMetaInfoPattern());
        view.getOverlappingPlaylistCheckBox().setChecked(getForm().isOverlappingPlaylist());
        //update available users for programmes
        feedUsersList();
        feedRolesList();
        view.getRightsListBox().clean();
        feedRightsList();

    }

    private void feedRolesList() {
        //refresh the available roles list
        view.refreshRoleRightList(ProgrammeRole.values());
    }

    private void feedUsersList() {
        userService.getSecuredRestrictedUsers(null, new ExtendedRPCCallback<ActionCollection<RestrictedUserForm>>(getEventBus(), view.getUserRightComboBox()) {

            @Override
            public void onResult(ActionCollection<RestrictedUserForm> result) {
                view.refreshRestrictedUserRightList(result);
            }
        });
    }

    private void feedNewChannelsList() {
        channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                view.refreshChannelList(result);
            }
        });
    }

    private boolean addNewChannel(final ChannelForm newChannel) {

        //Is it already present in the list?
        if (newChannel == null || view.getChannelsListBox().isValueIn(newChannel)) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getChannelsListBox().removeRow(view.getChannelsListBox().getCurrentRow(event));
            }
        };
        view.addNewChannel(newChannel, deleteClickHandler, new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, newChannel);
            }
        });

        return true;

    }

    private boolean addNewRight() {

        final RestrictedUserForm user = view.getUserRightComboBox().getSelectedValue();
        ProgrammeRole role = (ProgrammeRole) view.getRoleRightComboBox().getSelectedValue();

        //Check if the role and either the programme or channel parts of the rights are properly filled in
        if (user == null || role == null) {
            return false;
        }

        ProgrammeRightForm newRight = new ProgrammeRightForm(user.getUsername(), null, role);

        //Is it already present in the list?
        if (view.getRightsListBox().isValueIn(newRight)) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getRightsListBox().removeRow(view.getRightsListBox().getCurrentRow(event));
            }
        };
        view.addNewRight(newRight, deleteClickHandler, new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERDISPLAY, user);
            }
        });

        return true;
    }

    private void feedRightsList() {

        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getRightsListBox().removeRow(view.getRightsListBox().getCurrentRow(event));
            }
        };
        ClickHandler displayRestrictedUserClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                RightForm rightForm = view.getRightsListBox().getValueAt(view.getRightsListBox().getCurrentRow(event));
                userService.getSecuredRestrictedUser(rightForm.getRestrictedUserId(), new ExtendedRPCCallback<ActionCollectionEntry<RestrictedUserForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<RestrictedUserForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERDISPLAY, result.getItem());
                    }
                });
            }
        };

        //Clear the currently bound programme so as the isIn() method for checking if the same right cannot be added twice works
        ActionCollection<RightForm> newRights = new ActionSortedMap<RightForm>();
        for (ActionCollectionEntry<RightForm> rightFormEntry : getForm().getRightForms()) {
            RightForm newRight = rightFormEntry.getKey();
            newRight.setDomainId(null);
            newRights.merge(newRight, rightFormEntry.getValue());
        }
        view.refreshExistingRights(newRights, deleteClickHandler, displayRestrictedUserClickHandler);

    }

    private void feedChannelsList() {

        final ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getChannelsListBox().removeRow(view.getChannelsListBox().getCurrentRow(event));
            }
        };

        channelService.getSecuredChannelsForProgrammeId(null, getForm().getLabel(), new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelsListBox()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                view.refreshExistingChannels(
                        result,
                        deleteClickHandler,
                        new ClickHandler() {

                            @Override
                            public void onClick(ClickEvent event) {
                                ChannelForm channelForm = view.getChannelsListBox().getValueAt(view.getChannelsListBox().getCurrentRow(event));
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, channelForm);
                            }
                        });
            }

            /*@Override
            public void onInit() {
                view.getChannelsListBox().clean();
            }*/
        });

    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case programme:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag==FLAG.edit && (objectId==null || objectId.equals(getForm().getLabel())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                    case channel:
                        //If a channel has been added or removed then just notify the user
                        if (flag!=FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().channels_concurrently_edited());
                        break;
                    case user:
                        //If an authenticated user has been added or removed then just notify the user
                        if (flag!=FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().users_concurrently_edited());
                }
            }
        }.get();
    }
    
}
