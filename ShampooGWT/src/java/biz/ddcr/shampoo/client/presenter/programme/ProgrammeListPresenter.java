/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.programme;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.programme.ProgrammeListView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeListPresenter extends PagePresenterWithHistory<ProgrammeListView> {

    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private UserRPCServiceInterfaceAsync userService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewProgrammes(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        //Show the navigation bar links you are granted to see only
        refreshNavigationBar();
        //Build all the tabs
        refreshLists();
    }

    private void refreshLists() {
        view.getProgrammePanel().clean();

        //Add the header click handler
        final SortableTableClickHandler baseWorkingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshProgrammeList();
            }
        };
        authorizationService.checkAccessViewProgrammes(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getProgrammePanel()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshProgrammePanel(baseWorkingPanelHeaderClickHandler);
                    //And update it immediately
                    refreshProgrammeList();
                }
            }
        });
    }

    protected void refreshProgrammeList() {
        final ClickHandler editProgrammeClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ProgrammeForm programme = view.getProgrammeList().getValueAt(view.getProgrammeList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEEDIT, programme);
            }
        };
        final ClickHandler displayProgrammeClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                ProgrammeForm programme = view.getProgrammeList().getValueAt(view.getProgrammeList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, programme);
            }
        };
        final SerializableItemSelectionHandler<String> displayChannelSelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        channelService.getSecuredChannel(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<ChannelForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final SerializableItemSelectionHandler<String> displayRestrictedUserSelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        userService.getSecuredRestrictedUser(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<RestrictedUserForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<RestrictedUserForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final YesNoClickHandler deleteAllProgrammesClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllProgrammes(view.getProgrammeList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteProgrammeClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getProgrammeList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteProgramme(view.getProgrammeList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        programmeService.getSecuredProgrammes(view.getProgrammeList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeList()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshProgrammeList(result,
                        editProgrammeClickHandler,
                        deleteProgrammeClickHandler,
                        deleteAllProgrammesClickHandler,
                        displayProgrammeClickHandler,
                        displayChannelSelectionHandler,
                        displayRestrictedUserSelectionHandler);
            }

            /*@Override
            public void onInit() {
                view.getProgrammeList().clean();
            }*/
        });
    }

    protected void refreshNavigationBar() {

        view.removeAllNavigationLinks();

        final ClickHandler addRoleClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEADD);
            }
        };
        authorizationService.checkAccessAddProgrammes(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddProgrammeLink(addRoleClickHandler);
                }
            }
        });

    }

    private void deleteProgramme(final GenericFormTableInterface list, final int currentRow) {
        final ProgrammeForm programme = (ProgrammeForm) list.getValueAt(currentRow);
        if (programme != null) {
            programmeService.deleteSecuredProgramme(programme.getLabel(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().programme_deleted(programme.getLabel()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PROGRAMMES);
                }
            });
        }
    }

    private void deleteAllProgrammes(final GenericFormTableInterface list) {
        final Collection<ProgrammeForm> programmes = list.getAllCheckedValues();
        if (programmes != null && !programmes.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (ProgrammeForm form : programmes) {
                ids.add(form.getLabel());
            }
            programmeService.deleteSecuredProgrammes(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[programmes.size()];
                    int i = 0;
                    for (ProgrammeForm programme : programmes) {
                        ids[i] = programme.getLabel();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().programmes_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PROGRAMMES);
                }
            });
        }
    }

    /** Check whether the id given by the GUI event signal corresponds to a channel in the view list **/
    private boolean isInProgrammeList(String id) {
        for (ProgrammeForm programmeForm : view.getProgrammeList().getAllValues()) {
            if (programmeForm.getLabel().equals(id))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case programme:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_PROGRAMMES);
                        } else {
                            if (isInProgrammeList(objectId))
                                add(MessageEventsEnum.REFRESH_PROGRAMMES);                                
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_PROGRAMMES:
                refreshProgrammeList();
                break;
        }
    }
}
