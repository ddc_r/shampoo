/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.queue;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.queue.BroadcastableTrackQueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm.QUEUE_TAGS;
import biz.ddcr.shampoo.client.form.queue.RequestQueueForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotVoteException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.queue.QueueRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.queue.QueueListView;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class QueueListPresenter extends PagePresenterWithHistory<QueueListView> {

    private QueueRPCServiceInterfaceAsync queueService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;
    protected ChannelRPCServiceInterfaceAsync channelService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected TrackRPCServiceInterfaceAsync trackService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setQueueService(QueueRPCServiceInterfaceAsync queueService) {
        this.queueService = queueService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }
    /**
     * TODO: don't forget to drop this variable if I implement a full table for
     * queue items *
     */
    private static final GroupAndSort<QUEUE_TAGS> defaultGroupAndSort;

    static {
        defaultGroupAndSort = new GroupAndSort<QUEUE_TAGS>();
        defaultGroupAndSort.setSortByDescendingDirection(false);
        defaultGroupAndSort.setSortByFeature(QUEUE_TAGS.time);
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewQueues(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        //Change current channel
        view.getChannelComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                buildList(view.getChannelComboBox().getSelectedValue());
            }
        });

    }

    @Override
    protected void onRefresh() {
        view.getListPanel().clean();
        //First list the channels that are visible to the user
        refreshChannelList(true);
        //All other UI building are done once channels are retrieved and verified
    }

    private void refreshChannelList(final boolean forceRefresh) {
        //No need to check if the authenticated user can see channels, it's already been checked when the page opened
        channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                boolean doesRefresh = forceRefresh;
                        
                ChannelForm selectedChannelForm = view.getChannelComboBox().getSelectedValue();
                
                //Look in the current history if a Channel has been specified
                final String historyChannelLabel = QueueListPresenter.this.getHistoryPartLevel(1);
                if (historyChannelLabel!=null) {
                    //create a fake channel form (label is the only business key)
                    final ChannelForm historyChannelForm = new ChannelForm();
                    historyChannelForm.setLabel(historyChannelLabel);
                    if (selectedChannelForm==null || !selectedChannelForm.equals(historyChannelForm)) {
                        doesRefresh = true;
                        selectedChannelForm = historyChannelForm;
                    }
                }

                view.refreshChannelList(result);
                //Only perform an update if the previous and currently selected channels are different
                if (doesRefresh | !view.getChannelComboBox().setSelectedValue(selectedChannelForm)) {
                    //Update the timetable properties immediately, according to the first selected channel
                    buildList(view.getChannelComboBox().getSelectedValue());
                } else {
                    //Don't give up refreshing just yet, timezones might still need an update
                    refreshListTimezoneList(selectedChannelForm);
                }
            }

            /*@Override
            public void onInit() {
                view.getChannelComboBox().clean();
            }*/
        });
    }

    /**
     * Instiantiate a new tab panel Detect which of the grid or the list must be
     * updated
     *
     * @param currentChannel
     *
     */
    private void buildList(final ChannelForm currentChannel) {
        if (currentChannel == null) {
            view.getListPanel().clean();
        } else {
            refreshQueuePanel(currentChannel);
        }
    }

    private void refreshQueuePanel(final ChannelForm currentChannel) {
        view.getListPanel().clean();

        if (currentChannel != null) {

            //Add the timezone changer
            final ChangeHandler workingTimezoneChangeHandler = new ChangeHandler() {

                @Override
                public void onChange(ChangeEvent event) {
                    refreshQueueList(currentChannel, view.getTimezoneList().getSelectedValue());
                }
            };

            authorizationService.checkAccessViewQueue(currentChannel.getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getListPanel()) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    return (!(caught instanceof AccessDeniedException));
                }

                @Override
                public void onResult(Boolean result) {
                    //Activate the menu entries if access is granted
                    if (result) {
                        //Rebuild the UI
                        view.refreshQueuePanel(workingTimezoneChangeHandler);
                        //And update it immediately
                        refreshListTimezoneList(currentChannel);
                    }
                }
            });
        }
        refreshStreamerStatus(currentChannel);
    }

    protected void refreshQueueList(final ChannelForm currentChannel, final String currentTimezone) {

        final ClickHandler trackDisplayClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                QueueForm item = view.getQueueList().getValueAt(view.getQueueList().getCurrentRow(event));
                if (item != null && (item instanceof BroadcastableTrackQueueForm || item instanceof RequestQueueForm)) {
                    trackService.getSecuredBroadcastTrack(((BroadcastableTrackQueueForm) item).getTrackId(), new ExtendedRPCCallback<ActionCollectionEntry<BroadcastTrackForm>>(getEventBus(), null) {

                        @Override
                        public void onResult(final ActionCollectionEntry<BroadcastTrackForm> result) {
                            if (result != null && result.isReadable()) {
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKDISPLAY, result.getItem());
                            }
                        }
                    });
                }
            }
        };
        final ClickHandler programmeDisplayClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                QueueForm item = view.getQueueList().getValueAt(view.getQueueList().getCurrentRow(event));
                if (item != null && item.getProgrammeLabel() != null) {
                    programmeService.getSecuredProgramme(item.getProgrammeLabel(), new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                        @Override
                        public void onResult(final ActionCollectionEntry<ProgrammeForm> result) {
                            if (result != null && result.isReadable()) {
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        }
                    });
                }
            }
        };
        final VoteChangeHandler<String> myRatingChangeHandler = new VoteChangeHandler<String>() {

            @Override
            public void onSet(GenericMyVoteBoxInterface<String> source) {
                updateMyVote(source);
            }

            @Override
            public void onGet(GenericMyVoteBoxInterface<String> source) {
                fetchMyVote(source);
            }
        };

        if (currentChannel == null) {
            view.getQueueList().clean();
        } else {
            queueService.getSecuredNowPlayingItemForChannelId(currentChannel.getLabel(), currentTimezone, new ExtendedRPCCallback<ActionCollectionEntry<? extends QueueForm>>(getEventBus(), view.getQueueList()) {

                @Override
                public void onResult(final ActionCollectionEntry<? extends QueueForm> result) {

                    queueService.getSecuredQueueItemsForChannelId(defaultGroupAndSort, currentChannel.getLabel(), currentTimezone, new ExtendedRPCCallback<ActionCollection<? extends QueueForm>>(getEventBus(), view.getQueueList()) {

                        @Override
                        public void onResult(ActionCollection<? extends QueueForm> result2) {
                            view.refreshQueueList(result, result2, myRatingChangeHandler, trackDisplayClickHandler, programmeDisplayClickHandler);
                        }
                    });
                }
            });
        }

    }

    /**
     *
     * Update a song rating with the data provided by a Rating widget, for the
     * currently authenticated user
     *
     * @param ratingWidget
     *
     */
    private void updateMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.setCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), voteWidget.getMyVote(), new ExtendedRPCCallback<Void>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(Void result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().vote_registered());
                    //And refresh the back page
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    /**
     *
     * Retrieve the current user vote for a song, fetched as soon as the
     * component is drawn on screen
     *
     * @param voteWidget
     *
     */
    private void fetchMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.getCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), new ExtendedRPCCallback<VOTE>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(VOTE result) {
                    voteWidget.setMyVote(result);
                }
            });
        }
    }

    private void refreshListTimezoneList(final ChannelForm currentChannel) {
        if (currentChannel != null) {
            
            final String currentlySelectedTimezone = view.getTimezoneList().getSelectedValue();            
            loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneList()) {

                @Override
                public void onResult(String result) {
                    //Only perform an update if the previous and currently selected timezones are different
                    view.refreshListTimezoneList(
                            currentChannel.getTimezone(),
                            result);
                    //Only perform an update if required
                    if (!view.getTimezoneList().setSelectedValue(currentlySelectedTimezone)) {
                        //Update the whole items
                        refreshQueueList(currentChannel, view.getTimezoneList().getSelectedValue());
                    }
                }
            });
        }
    }

    /**
     *
     * Retrieve the current channel/streamer connection status, fetched as soon
     * as the component is drawn on screen
     *
     * @param voteWidget
     *
     */
    private void refreshStreamerStatus(final ChannelForm currentChannel) {

        if (currentChannel != null) {
            channelService.getSecuredDynamicChannelStreamerMetadata(currentChannel.getLabel(), new ExtendedRPCCallback<ActionCollectionEntry<StreamerModule>>(getEventBus(), view.getStreamerStatusWidget()) {

                @Override
                public void onResult(ActionCollectionEntry<StreamerModule> result) {
                    view.refreshStreamerStatus(currentChannel.getLabel(), result);
                }
            });
        } else {
            view.refreshStreamerStatus(null, null);
        }
    }

    /**
     * Check whether the id given by the GUI event signal corresponds to the currently playing item
     */
    private boolean isInQueueList(String id) {
        for (QueueForm queueForm : view.getQueueList().getAllValues()) {
            if (queueForm.getRefId().getRefID().equals(id)) {
                return true;
            }
        }
        return false;
    }
    /**
     * Check whether the id given by the GUI event signal corresponds to the track represented by the currently playing item
     */
    private boolean isPlayingTrack(String id) {
        for (QueueForm queueForm : view.getQueueList().getAllValues()) {
            if (queueForm instanceof BroadcastableTrackQueueForm) {
                if (((BroadcastableTrackQueueForm)queueForm).getTrackId().equals(id))
                    return true;
            }
        }
        return false;
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case streamer:
                            if (objectId == null || (view.getChannelComboBox().getSelectedValue() != null && view.getChannelComboBox().getSelectedValue().getSeatNumber()!=null && objectId.equals(view.getChannelComboBox().getSelectedValue().getSeatNumber().getItem().toString()))) {
                                add(MessageEventsEnum.REFRESH_STREAMERS);
                            }
                        break;
                    case channel:
                        if (flag != FeedbackObjectForm.FLAG.edit) {
                            add(MessageEventsEnum.REFRESH_CHANNELS);
                        } else if (objectId == null || (view.getChannelComboBox().getSelectedValue() != null && objectId.equals(view.getChannelComboBox().getSelectedValue().getLabel()))) {
                            add(MessageEventsEnum.REFRESH_QUEUES);
                        }
                        break;
                    case streamitem:
                    case queueitem:
                        if (flag == FeedbackObjectForm.FLAG.add) {
                            //LIMITATION can detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_QUEUES);
                        } else {
                            if (isInQueueList(objectId)) {
                                add(MessageEventsEnum.REFRESH_QUEUES);
                            }
                        }
                        break;
                    case broadcasttrack:
                        if (flag == FeedbackObjectForm.FLAG.edit) {
                            if (isPlayingTrack(objectId)) {
                                add(MessageEventsEnum.REFRESH_QUEUES);
                            }
                        }
                        break;
                    case user:
                        if (flag == FeedbackObjectForm.FLAG.edit && UserIn.authenticatedUserName != null && UserIn.authenticatedUserName.equals(objectId)) {
                            add(MessageEventsEnum.REFRESH_MYPROFILE);
                        }
                        break;
                }
            }
        }.get();
    }

    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_STREAMERS:
                refreshStreamerStatus(
                        view.getChannelComboBox().getSelectedValue());
                break;
            case REFRESH_BROADCASTTRACKS:
            case REFRESH_QUEUES:
                refreshQueueList(
                        view.getChannelComboBox().getSelectedValue(),
                        view.getTimezoneList().getSelectedValue());
                break;
            case REFRESH_CHANNELS:
                refreshChannelList(false);
                break;
            case REFRESH_MYPROFILE:
                refreshListTimezoneList(view.getChannelComboBox().getSelectedValue());
                break;
        }
    }
}
