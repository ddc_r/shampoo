/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistFormID;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm.RECURRING_EVENT;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.timetable.UniqueTimetableSlotForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionMapEntry;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableAddView;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableAddView.RecurringEvent;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDayHourMinuteSecondSelectorInterface.YearMonthDayHourMinuteSecondTimeChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDaySelectorInterface.YearMonthDayTimeChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TimetableAddPresenter extends PopupPagePresenterWithFormParameter<TimetableAddView, ChannelForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    private LoginRPCServiceInterfaceAsync loginService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the timetable channel to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            /*To be able to use this window, one must be able to:
             * -add timetables
             * -touch the corresponding channel
             **/
            authorizationService.checkAccessAddTimetableSlot(getForm().getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Channel caption
        view.getChannelLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, getForm());
            }
        });
        //Add new channel
        view.getEndDateRadioButton().addValueChangeHandler(new ValueChangeHandler() {

            @Override
            public void onValueChange(ValueChangeEvent event) {
                view.toggleEndFormatChange();
            }
        });
        view.getEndLengthRadioButton().addValueChangeHandler(new ValueChangeHandler() {

            @Override
            public void onValueChange(ValueChangeEvent event) {
                view.toggleEndFormatChange();
            }
        });

        //Playlist from programme
        view.getProgrammeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                feedPlaylistList();
            }
        });

        //Time slot boundaries
        view.getEndTimeSelector().setTimeChangeHandler(new YearMonthDayHourMinuteSecondTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayHourMinuteSecondMillisecondInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getEndTimeSelector().getCurrentTime()), view.getEndTimeSelector())) {
                    onEndTimeChanged();
                    onDecommissioningTimeChanged();
                }
            }
        });
        view.getLengthBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (!Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getLengthBox().getValue()), view.getRecurringEventComboBox())) {
                    onLengthChanged();
                }
            }
        });
        view.getStartTimeSelector().setTimeChangeHandler(new YearMonthDayHourMinuteSecondTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayHourMinuteSecondMillisecondInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getStartTimeSelector().getCurrentTime()), view.getStartTimeSelector())) {
                    onStartTimeChanged();
                }
            }
        });

        //Change timezone
        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                updateTimeZone();
            }
        });

        //Change event type
        view.getRecurringEventComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (checkRecurringEvent()) {
                    updateRecurringEventCaption();
                    view.toggleRecurringEventChange();
                }
            }
        });

        //Change decommissioning time
        view.getDecommissioningTimeCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.toggleDecommissioningEnabledChange();
            }
        });
        view.getDecommissioningTimeSelector().setTimeChangeHandler(new YearMonthDayTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getDecommissioningTimeSelector().getCurrentTime()), view.getDecommissioningTimeSelector())) {
                    onDecommissioningTimeChanged();
                }
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;

        //Check if there's a valid programme linked to this timetable slot
        if (Validation.bindValidationError(Validation.checkEntityIsSet(view.getProgrammeComboBox().getSelectedValue()), view.getProgrammeComboBox())) {
            result = false;
        }

        if (Validation.bindValidationError(Validation.checkDate(view.getStartTimeSelector().getCurrentTime()), view.getStartTimeSelector())) {
            result = false;
        }
        if (view.getEndDateRadioButton().isSelected()) {
            //True == end selected by Date
            if (Validation.bindValidationError(Validation.checkDate(view.getEndTimeSelector().getCurrentTime()), view.getEndTimeSelector())) {
                result = false;
            }
        } else {
            //False == end selected by Length
            if (Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getLengthBox().getValue()), view.getRecurringEventComboBox())) {
                result = false;
            }
        }
        if (!checkRecurringEvent()) {
            result = false;
        }

        return result;
    }

    private boolean checkRecurringEvent() {
        boolean result = true;
        //Verify that the the given length is compatible with the selected recurring event
        if (view.getRecurringEventComboBox().getSelectedValue() == null) {
            Validation.bindValidationError(I18nTranslator.getInstance().null_value(), view.getRecurringEventComboBox());
            return false;
        }
        if (view.getRecurringEventComboBox().getSelectedValue() != RecurringEvent.none) {
            switch (view.getRecurringEventComboBox().getSelectedValue()) {
                case daily:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanDay(view.getLengthBox().getValue()), view.getRecurringEventComboBox())) {
                        result = false;
                    }
                    break;
                case weekly:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanWeek(view.getLengthBox().getValue()), view.getRecurringEventComboBox())) {
                        result = false;
                    }
                    break;
                case monthly:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanMonth(view.getLengthBox().getValue()), view.getRecurringEventComboBox())) {
                        result = false;
                    }
                    break;
                case yearly:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanYear(view.getLengthBox().getValue()), view.getRecurringEventComboBox())) {
                        result = false;
                    }
                    break;
                default:
                    return false;
            }
            if (view.getDecommissioningTimeCheckBox().isChecked()
                    && Validation.bindValidationError(Validation.checkDate(view.getDecommissioningTimeSelector().getCurrentTime()), view.getDecommissioningTimeSelector())) {
                result = false;
            }
        }
        return result;
    }

    public void submitForm() {

        //Build a new timetable entry from the view
        final TimetableSlotForm newTimetableSlotForm;
        if (view.getRecurringEventComboBox().getSelectedValue() != RecurringEvent.none) {
            //Recurring slot
            newTimetableSlotForm = new RecurringTimetableSlotForm();
            switch (view.getRecurringEventComboBox().getSelectedValue()) {
                case daily:
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.daily);
                    break;
                case weekly:
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.weekly);
                    break;
                case monthly:
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.monthly);
                    break;
                case yearly:
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.yearly);
                    break;
            }

            //Check if decommissioning time has been set up
            if (view.getDecommissioningTimeCheckBox().isChecked()) {
                ((RecurringTimetableSlotForm) newTimetableSlotForm).setDecommissioningTime(view.getDecommissioningTimeSelector().getCurrentTime());
            } else //Otherwise explicitly mark it as unused
            {
                ((RecurringTimetableSlotForm) newTimetableSlotForm).setDecommissioningTime(null);
            }
        } else {
            //Unique slot
            newTimetableSlotForm = new UniqueTimetableSlotForm();
        }

        //Create the ID
        TimetableSlotFormID newId = new TimetableSlotFormID();
        newId.setChannelId(getForm().getLabel());
        newId.setSchedulingTime(view.getStartTimeSelector().getCurrentTime());
        newTimetableSlotForm.setRefId(newId);

        if (view.getPlaylistComboBox().getSelectedValue() != null) {
            newTimetableSlotForm.setPlaylistId(
                    new ActionMapEntry<PlaylistFormID>(view.getPlaylistComboBox().getSelectedValue().getPlaylistFormID()));
        } else {
            newTimetableSlotForm.setPlaylistId(null);
        }

        //by default all new timetable slots are active IFF there's a playlist
        newId.setEnabled(newTimetableSlotForm.getPlaylistId() != null);

        newTimetableSlotForm.setProgrammeId(
                new ActionMapEntry<String>(view.getProgrammeComboBox().getSelectedValue().getLabel()));

        newTimetableSlotForm.setEndTime(view.getEndTimeSelector().getCurrentTime());

        //The end time from the presenter must not be included because its the close-ended limit of the time interval
        //Remove one minute from it
        /*helperService.addMinutes(view.getEndTimeSelector().getCurrentTime(), -1, new ExtendedAsyncCallback<JSYearMonthDayHourMinute>(getEventBus(), view.getSubmit()) {

        @Override
        public void onResult(JSYearMonthDayHourMinute result) {
        if (result != null) {
        newTimetableSlotForm.setEndTime(result);
        submitForm(newTimetableSlotForm);
        }
        }
        });**/
        submitForm(newTimetableSlotForm);

    }

    private void submitForm(final TimetableSlotForm form) {
        if (form != null) //Make it persistant
        {
            channelService.addSecuredTimetableSlot(form, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                @Override
                public void onResult(Void result) {
                    //Then close popup
                    closePopup();
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_added(form.getRefId().getFullFriendlyID()));
                    //And refresh the back page
                    //UPDATE: Now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
                }
            });
        }
    }

    @Override
    protected void onRefresh() {
        //Reset everything
        view.getChannelLabel().clean();
        view.getChannelLabel().setCaption(getForm().getLabel());

        view.toggleEndFormatChange();

        view.getSubmit().clean();

        //Clear programme list
        view.getProgrammeComboBox().clean();
        //Update programme list & playlist list
        feedProgrammeList();

        view.getRecurringEventComboBox().clean();
        //Update recurring event types
        feedRecurringEventTypeList();

        view.toggleDecommissioningEnabledChange();

        view.getStartTimeSelector().clean();
        view.getEndTimeSelector().clean();
        view.getLengthBox().clean();
        view.getTimezoneComboBox().clean();
        //Update timezone list
        //and
        //Now update the current start time
        feedTimeZoneListAndSetCurrentTime();

    }

    private void onStartTimeChanged() {

        YearMonthDayHourMinuteSecondMillisecondInterface startTime = view.getStartTimeSelector().getCurrentTime();
        if (startTime != null) {
            if (view.getEndDateRadioButton().isSelected()) {
                //If the ending time is currently selected
                //then make sure that start time is less than ending time by the minimal step of length or ending time is not null and only update the length
                //otherwise change the ending time and the length by the minimal step of length

                YearMonthDayHourMinuteSecondMillisecondInterface endTime = view.getEndTimeSelector().getCurrentTime();
                if (endTime != null) {
                    helperService.diffMilliseconds(startTime, endTime, new ExtendedRPCCallback<Long>(getEventBus(), view.getEndTimeSelector()) {

                        @Override
                        public void onResult(Long result) {
                            //Result must be converted from milliseconds to seconds
                            if (result != null && (result/1000) >= view.getLengthBox().getMinValue()) {
                                view.getLengthBox().setValue((int)(result/1000));
                            } else {
                                view.getLengthBox().setValue(view.getLengthBox().getMinValue());
                                onLengthChanged();
                            }
                        }
                    });
                } else {
                    view.getLengthBox().setValue(view.getLengthBox().getMinValue());
                    onLengthChanged();
                }

            } else {
                //If the ending time is not currently selected but the global length
                //then update the ending time from the length + the new starting time
                onLengthChanged();
            }
        }
        updateRecurringEventCaption();
    }

    private void onLengthChanged() {

        YearMonthDayHourMinuteSecondMillisecondInterface startTime = view.getStartTimeSelector().getCurrentTime();
        YearMonthDayHourMinuteSecondMillisecondInterface endTime = view.getEndTimeSelector().getCurrentTime();

        //if length is less than the minimum value then update start time accordingly
        if (view.getLengthBox().getValue() < view.getLengthBox().getMinValue()) {
            view.getLengthBox().setValue(view.getLengthBox().getMinValue());
        }

        if (startTime != null) {

            //The new end time equals the length plus the start time
            helperService.addSeconds(startTime, view.getLengthBox().getValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getEndTimeSelector()) {

                @Override
                public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                    view.getEndTimeSelector().setCurrentTime(result);
                    onDecommissioningTimeChanged();
                }
            });
        } else if (endTime != null) {

            _recomputeStartTime(endTime, view.getLengthBox().getValue());

        }

    }

    private void _recomputeStartTime(YearMonthDayHourMinuteSecondMillisecondInterface endTime, long length) {
        //Parameters must be valid, they won't be checked.
        //start time has not been specified or is incorrect: compute it from the end time minus the length
        helperService.addSeconds(endTime, -length, new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getStartTimeSelector()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                view.getStartTimeSelector().setCurrentTime(result);
            }
        });
    }

    private void onEndTimeChanged() {
        //If start time is specified and start time is less than end time by the minimum length step, modify the length by computing the difference between the start and the end times
        //otherwise compute a new start time if length exists but not start time, or

        final YearMonthDayHourMinuteSecondMillisecondInterface endTime = view.getEndTimeSelector().getCurrentTime();
        if (endTime != null) {

            YearMonthDayHourMinuteSecondMillisecondInterface startTime = view.getStartTimeSelector().getCurrentTime();
            if (startTime != null) {
                helperService.diffMilliseconds(startTime, endTime, new ExtendedRPCCallback<Long>(getEventBus(), view.getEndTimeSelector()) {

                    @Override
                    public void onResult(Long result) {
                        //Result must be converted from milliseconds to seconds
                        if (result != null && (result/1000) >= view.getLengthBox().getMinValue()) {
                            view.getLengthBox().setValue((int)(result/1000));
                        } else {
                            view.getLengthBox().setValue(view.getLengthBox().getMinValue());
                            _recomputeStartTime(endTime, view.getLengthBox().getValue());
                        }
                    }
                });
            } else {
                view.getLengthBox().setValue(view.getLengthBox().getMinValue());
                _recomputeStartTime(endTime, view.getLengthBox().getValue());
            }

        }

    }

    private void onDecommissioningTimeChanged() {
        //decommissioning must always be greater than than end time
        YearMonthDayHourMinuteSecondMillisecondInterface endTime = view.getEndTimeSelector().getCurrentTime();
        if (endTime != null) {

            YearMonthDayInterface decommissioningTime = view.getDecommissioningTimeSelector().getCurrentTime();
            //We can directly compute time difference because they share the same time zone and then the same DST rules
            if (decommissioningTime == null || ((JSYearMonthDay) endTime).compareTo(decommissioningTime) > 0) {
                helperService.addDays(endTime, 1, new ExtendedRPCCallback<YearMonthDayInterface>(getEventBus(), view.getDecommissioningTimeSelector()) {

                    @Override
                    public void onResult(YearMonthDayInterface result) {
                        if (result != null) {
                            view.getDecommissioningTimeSelector().setCurrentTime(result);
                        }
                    }
                });
            }
        }
    }

    private void feedProgrammeList() {

        programmeService.getSecuredProgrammesForChannelId(null, getForm().getLabel(), new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshProgrammeList(result);
                feedPlaylistList();
            }

            /*@Override
            public void onInit() {
                view.getProgrammeComboBox().clean();
            }*/
        });
    }

    private void feedPlaylistList() {

        ProgrammeForm p = view.getProgrammeComboBox().getSelectedValue();
        if (p != null) {
            programmeService.getSecuredPlaylistsForProgrammeId(null, p.getLabel(), new ExtendedRPCCallback<ActionCollection<PlaylistForm>>(getEventBus(), view.getPlaylistComboBox()) {

                @Override
                public void onResult(ActionCollection<PlaylistForm> result) {
                    view.refreshPlaylistList(result);
                }

                /*@Override
                public void onInit() {
                    view.getPlaylistComboBox().clean();
                }*/
            });
        }

    }

    private void updateRecurringEventCaption() {
        RecurringEvent currentEvent = view.getRecurringEventComboBox().getSelectedValue();
        switch (currentEvent) {
            case none:
                view.getRecurringEventCaption().setCaption(null);
                break;
            case daily:
                view.getRecurringEventCaption().setCaption(
                        I18nTranslator.getInstance().repeated_every_day());
                break;
            case weekly:
                view.getRecurringEventCaption().setCaption(
                        I18nTranslator.getInstance().repeated_every_week(
                        JSYearMonthDay.getI18nDayFriendlyName(view.getStartTimeSelector().getCurrentTime().getDayOfWeek())));
                break;
            case monthly:
                view.getRecurringEventCaption().setCaption(
                        I18nTranslator.getInstance().repeated_every_month(
                        JSYearMonthDay.getI18nDayNumberFriendlyName(view.getStartTimeSelector().getCurrentTime().getDayOfMonth())));
                break;
            case yearly:
                view.getRecurringEventCaption().setCaption(
                        I18nTranslator.getInstance().repeated_every_year(
                        JSYearMonthDay.getI18nDayNumberFriendlyName(view.getStartTimeSelector().getCurrentTime().getDayOfYear())));
                break;
            default:
                view.getRecurringEventCaption().setCaption(null);
                break;
        }
    }

    private void feedRecurringEventTypeList() {
        view.refreshRecurringEventKindList(RecurringEvent.values());
        updateRecurringEventCaption();
        view.toggleRecurringEventChange();
    }

    private void feedTimeZoneListAndSetCurrentTime() {
        loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(String result) {
                view.refreshTimezoneList(
                        getForm().getTimezone(),
                        result);
                //Update time now
                setCurrentTime();
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });
    }

    //Update the start time with the selected time zone
    //Trigger the onChange() events for the end time when done
    private void updateTimeZone() {
        helperService.switchTimeZone(view.getStartTimeSelector().getCurrentTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getStartTimeSelector()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                if (result != null) {
                    //Update start time
                    view.getStartTimeSelector().setCurrentTime(result);
                    //Check if the time difference has changed and is still valid
                    onStartTimeChanged();
                }
            }

            /*@Override
            public void onInit() {
                view.getStartTimeSelector().clean();
            }*/
        });
    }

    private void setCurrentTime() {
        helperService.getCurrentTime(view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getStartTimeSelector()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                if (result != null) {
                    //Update start time
                    //BUT drop milliseconds
                    view.getStartTimeSelector().setCurrentTime(result);
                    //Check if the time difference has changed and is still valid
                    onStartTimeChanged();
                }
            }

            /*@Override
            public void onInit() {
                view.getStartTimeSelector().clean();
            }*/
        });
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                    case playlist:
                        //If an authenticated user has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().playlists_concurrently_edited());
                }
            }
        }.get();
    }
}
