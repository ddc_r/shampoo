/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableDecommissioningView;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDaySelectorInterface.YearMonthDayTimeChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TimetableDecommissioningPresenter extends PopupPagePresenterWithFormParameter<TimetableDecommissioningView, RecurringTimetableSlotForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    private LoginRPCServiceInterfaceAsync loginService;
    //private working copy of the original start and end time; tehse are the only ones that will be updated using user time zone data
    private YearMonthDayHourMinuteSecondMillisecondInterface originalEndTime;
    private YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the timetable channel to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessUpdateTimetableSlot(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Clickable captions
        view.getChannelLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String channelId = getForm().getRefId().getChannelId();
                channelService.getSecuredChannel(channelId, new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<ChannelForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                    }
                });
            }
        });
        view.getProgrammeLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String programmeId = getForm().getProgrammeId().getItem();
                programmeService.getSecuredProgramme(programmeId, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                    }
                });
            }
        });
        view.getPlaylistLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String playlistId = getForm().getPlaylistId().getItem();
                programmeService.getSecuredPlaylist(playlistId, new ExtendedRPCCallback<ActionCollectionEntry<PlaylistForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<PlaylistForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, result.getItem());
                    }
                });
            }
        });

        //Change timezone
        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                updateTimeZone();
            }
        });

        //Change decommissioning time
        view.getDecommissioningTimeCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.toggleDecommissioningEnabledChange();
            }
        });
        view.getDecommissioningTimeSelector().setTimeChangeHandler(new YearMonthDayTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getDecommissioningTimeSelector().getCurrentTime()), view.getDecommissioningTimeSelector())) {
                    onDecommissioningTimeChanged();
                }
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (view.getDecommissioningTimeCheckBox().isChecked()) {
            if (Validation.bindValidationError(Validation.checkDate(view.getDecommissioningTimeSelector().getCurrentTime()), view.getDecommissioningTimeSelector())) {
                result = false;
            }
        }
        return result;
    }

    public void submitForm() {

        //Check if decommissioning time has been set up
        YearMonthDayInterface newDecommissioningDate = null;
        if (view.getDecommissioningTimeCheckBox().isChecked()) {
            newDecommissioningDate = view.getDecommissioningTimeSelector().getCurrentTime();
        }

        //Make it persistant
        channelService.setDecommissioningSecuredTimetableSlot(getForm().getRefId().getChannelId(), getForm().getRefId().getSchedulingTime(), newDecommissioningDate, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_updated(getForm().getRefId().getFullFriendlyID()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
            }
        });
    }

    @Override
    protected void onRefresh() {
        originalEndTime = getForm().getEndTime();
        originalStartTime = getForm().getRefId().getSchedulingTime();

        //Reset everything
        view.getChannelLabel().clean();
        view.getChannelLabel().setCaption(getForm().getRefId().getChannelId());
        view.getProgrammeLabel().clean();
        if (getForm().getProgrammeId().isReadable()) {
            view.getProgrammeLabel().setCaption(getForm().getProgrammeId().getItem());
        }
        view.getPlaylistLabel().clean();
        if (getForm().getPlaylistFriendlyId() != null) {
            if (getForm().getPlaylistFriendlyId().isReadable()) {
                view.getPlaylistLabel().setCaption(getForm().getPlaylistFriendlyId().getItem());
            }
        }
        view.getStartTimeLabel().clean();
        view.getEndTimeLabel().clean();

        view.getSubmit().clean();

        if (getForm().getDecommissioningTime() != null) {
            view.getDecommissioningTimeCheckBox().setChecked(true);
            view.getDecommissioningTimeSelector().setCurrentTime(getForm().getDecommissioningTime());
        } else {
            view.getDecommissioningTimeCheckBox().setChecked(false);
        }
        view.toggleDecommissioningEnabledChange();

        view.getDecommissioningTimeSelector().clean();
        view.getTimezoneComboBox().clean();
        //Update timezone list
        //and
        //Now update the current start time
        feedTimeZoneListAndSetTime();

    }

    private void onDecommissioningTimeChanged() {

        YearMonthDayInterface decommissioningTime = view.getDecommissioningTimeSelector().getCurrentTime();
        //We can directly compute time difference because they share the same time zone and then the same DST rules
        if (decommissioningTime == null || ((YearMonthDayInterface) originalEndTime).compareTo(decommissioningTime) > 0) {
            helperService.addDays(originalEndTime, 1, new ExtendedRPCCallback<YearMonthDayInterface>(getEventBus(), view.getDecommissioningTimeSelector()) {

                @Override
                public void onResult(YearMonthDayInterface result) {
                    if (result != null) {
                        view.getDecommissioningTimeSelector().setCurrentTime(result);
                    }
                }
            });
        }

    }

    private void updateRecurringEventCaption() {
        switch (getForm().getRecurringEvent()) {
            case daily:
                view.getRecurringEventCaption().setCaption(
                        I18nTranslator.getInstance().repeated_every_day());
                break;
            case weekly:
                view.getRecurringEventCaption().setCaption(
                        I18nTranslator.getInstance().repeated_every_week(
                        JSYearMonthDay.getI18nDayFriendlyName(originalStartTime.getDayOfWeek())));
                break;
            case monthly:
                helperService.getWeekOfMonth(originalStartTime, new ExtendedRPCCallback<Byte>(getEventBus(), view.getRecurringEventCaption()) {

                    @Override
                    public void onResult(Byte result) {
                        view.getRecurringEventCaption().setCaption(
                                I18nTranslator.getInstance().repeated_every_month(
                                JSYearMonthDay.getI18nDayNumberFriendlyName(originalStartTime.getDayOfMonth())));
                    }
                });
                break;
            case yearly:
                helperService.getWeekOfMonth(originalStartTime, new ExtendedRPCCallback<Byte>(getEventBus(), view.getRecurringEventCaption()) {

                    @Override
                    public void onResult(Byte result) {
                        view.getRecurringEventCaption().setCaption(
                                I18nTranslator.getInstance().repeated_every_year(
                                JSYearMonthDay.getI18nDayNumberFriendlyName(originalStartTime.getDayOfYear())));
                    }
                });
                break;
            default:
                view.getRecurringEventCaption().setCaption(null);
                break;
        }
    }

    private void feedTimeZoneListAndSetTime() {
        loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(final String result1) {

                channelService.getChannelTimeZone(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

                    @Override
                    public void onResult(String result2) {
                        view.refreshTimezoneList(
                                result2,
                                result1);
                        //Update time now
                        updateTimeZone();
                    }
                });
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });
    }

    //Update the start time with the selected time zone
    //Trigger the onChange() events for the end time when done
    private void updateTimeZone() {
        //Update the original times too
        helperService.switchTimeZone(getForm().getRefId().getSchedulingTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getStartTimeLabel()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result1) {
                originalStartTime = result1;
                view.getStartTimeLabel().setCaption(originalStartTime.getI18nFullFriendlyString());
                updateRecurringEventCaption();

                helperService.switchTimeZone(getForm().getEndTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getEndTimeLabel()) {

                    @Override
                    public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result2) {
                        originalEndTime = result2;
                        view.getEndTimeLabel().setCaption(originalEndTime.getI18nFullFriendlyString());
                        onDecommissioningTimeChanged();
                    }
                });

            }
        });

    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case timetableslot:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag!=FeedbackObjectForm.FLAG.add && (objectId==null || objectId.equals(getForm().getRefId().getRefID())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
