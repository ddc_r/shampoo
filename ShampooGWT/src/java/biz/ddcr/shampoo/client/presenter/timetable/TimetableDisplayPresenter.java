/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableDisplayView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;

/**
 *
 * @author okay_awright
 **/
public class TimetableDisplayPresenter extends GenericFormPopupPagePresenter<TimetableDisplayView, TimetableSlotForm> {

    private ChannelRPCServiceInterfaceAsync channelService;
    private HelperRPCServiceInterfaceAsync helperService;
    private LoginRPCServiceInterfaceAsync loginService;
    //private working copy of the original start and end time; tehse are the only ones that will be updated using user time zone data
    private YearMonthDayHourMinuteSecondMillisecondInterface originalEndTime;
    private YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime;

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the timetable channel to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewTimetableSlot(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {
        //Change timezone
        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                updateTimeZone();
            }
        });

    }

    @Override
    public void onFormSpecificRefresh() {
        originalEndTime = getForm().getEndTime();
        originalStartTime = getForm().getRefId().getSchedulingTime();

        //Reset everything
        view.getChannelLabel().setCaption(getForm().getRefId().getChannelId());
        view.getProgrammeLabel().clean();
        if (getForm().getProgrammeId().isReadable()) {
            view.getProgrammeLabel().setCaption(getForm().getProgrammeId().getItem());
        }
        view.getPlaylistLabel().clean();
        if (getForm().getPlaylistFriendlyId() != null) {
            if (getForm().getPlaylistFriendlyId().isReadable()) {
                view.getPlaylistLabel().setCaption(getForm().getPlaylistFriendlyId().getItem());
            }
        }
        //Update timezone list
        //and
        //Now update the current start time
        feedTimeZoneListAndSetTime();

    }

    private void updateRecurringEventAndDecommissioningCaption() {
        if (getForm() instanceof RecurringTimetableSlotForm) {
            final RecurringTimetableSlotForm recurringTimetableSlotForm = (RecurringTimetableSlotForm) getForm();

            switch (recurringTimetableSlotForm.getRecurringEvent()) {
                case daily:
                    view.refreshRecurringEventAndDecommissioningTime(
                            I18nTranslator.getInstance().repeated_every_day(),
                            recurringTimetableSlotForm.getDecommissioningTime());
                    break;
                case weekly:
                    view.refreshRecurringEventAndDecommissioningTime(
                            I18nTranslator.getInstance().repeated_every_week(
                            JSYearMonthDay.getI18nDayFriendlyName(originalStartTime.getDayOfWeek())),
                            recurringTimetableSlotForm.getDecommissioningTime());
                    break;
                case monthly:
                    helperService.getWeekOfMonth(originalStartTime, new ExtendedRPCCallback<Byte>(getEventBus(), view.getRecurringEventAndDecommissioningTimeTree()) {

                        @Override
                        public void onResult(Byte result) {
                            view.refreshRecurringEventAndDecommissioningTime(
                                    I18nTranslator.getInstance().repeated_every_month(
                                    JSYearMonthDay.getI18nDayNumberFriendlyName(originalStartTime.getDayOfMonth())),
                                    recurringTimetableSlotForm.getDecommissioningTime());
                        }
                    });
                    break;
                case yearly:
                    helperService.getWeekOfMonth(originalStartTime, new ExtendedRPCCallback<Byte>(getEventBus(), view.getRecurringEventAndDecommissioningTimeTree()) {

                        @Override
                        public void onResult(Byte result) {
                            view.refreshRecurringEventAndDecommissioningTime(
                                    I18nTranslator.getInstance().repeated_every_year(
                                    JSYearMonthDay.getI18nDayNumberFriendlyName(originalStartTime.getDayOfYear())),
                                    recurringTimetableSlotForm.getDecommissioningTime());
                        }
                    });
                    break;
                default:
                    view.refreshRecurringEventAndDecommissioningTime(
                            I18nTranslator.getInstance().unknown(),
                            recurringTimetableSlotForm.getDecommissioningTime());
                    break;
            }
        } else {
            view.refreshRecurringEventAndDecommissioningTime(
                    null,
                    null);
        }
    }

    private void feedTimeZoneListAndSetTime() {
        loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(final String result1) {

                channelService.getChannelTimeZone(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

                    @Override
                    public void onResult(String result2) {
                        view.refreshTimezoneList(
                                result2,
                                result1);
                        //Update time now
                        updateTimeZone();
                    }
                });
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });
    }

    //Update the start time with the selected time zone
    //Trigger the onChange() events for the end time when done
    private void updateTimeZone() {
        //Update the original times too
        helperService.switchTimeZone(getForm().getRefId().getSchedulingTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getStartTimeLabel()) {

            /*@Override
            public void onInit() {
                view.getStartTimeLabel().clean();
            }*/

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result1) {
                view.getStartTimeLabel().clean();
                originalStartTime = result1;
                view.getStartTimeLabel().setCaption(originalStartTime.getI18nFullFriendlyString());
                updateRecurringEventAndDecommissioningCaption();

                helperService.switchTimeZone(getForm().getEndTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getEndTimeTree()) {

                    @Override
                    public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result2) {
                        originalEndTime = result2;
                        updateEndTime();
                    }
                });

            }
        });


    }

    private void updateEndTime() {

        helperService.diffMilliseconds(originalStartTime, originalEndTime, new ExtendedRPCCallback<Long>(getEventBus(), view.getEndTimeTree()) {

            @Override
            public void onResult(Long result) {
                //result is a number of milliseconds
                if (result != null) {
                    JSDuration length = new JSDuration(result);
                    view.refreshEndTime(originalEndTime, length);
                } else {
                    view.refreshEndTime(originalEndTime, null);
                }
            }
        });

    }
}
