/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.date.HourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableListView;
import biz.ddcr.shampoo.client.view.widgets.GenericExtendedFormTimetableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericExtendedFormTimetableInterface.RefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellCoordinates;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TimetableListPresenter extends PagePresenterWithHistory<TimetableListView> {

    protected HelperRPCServiceInterfaceAsync helperService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected ChannelRPCServiceInterfaceAsync channelService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewTimetableSlots(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        //Change current channel
        view.getChannelComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                buildTimetable(view.getChannelComboBox().getSelectedValue());
            }
        });

    }

    @Override
    protected void onRefresh() {
        view.getTabPanel().clean();
        view.removeAllNavigationLinks();
        //First list the channels that are visible to the user
        refreshChannelList();
        //All other UI building are done once channels are retrieved and verified
    }

    protected void refreshNavigationBar(final ChannelForm currentChannel) {

        view.removeAllNavigationLinks();

        final ClickHandler addTimetableClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEADD, currentChannel);
            }
        };

        authorizationService.checkAccessAddTimetableSlot(currentChannel.getLabel(), new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddTimetableLink(addTimetableClickHandler);
                }
            }
        });

    }

    private void refreshChannelList() {
        //No need to check if the authenticated user can see channels, it's already been checked when the page opened
        channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                
                ChannelForm selectedChannelForm = null;
                //Look in the current history if a Channel has been specified
                final String historyChannelLabel = TimetableListPresenter.this.getHistoryPartLevel(1);
                if (historyChannelLabel!=null) {
                    //create a fake channel form (label is the only business key)
                    final ChannelForm historyChannelForm = new ChannelForm();
                    historyChannelForm.setLabel(historyChannelLabel);
                    selectedChannelForm = historyChannelForm;
                }
                
                view.refreshChannelList(result);
                
                if (selectedChannelForm!=null) view.getChannelComboBox().setSelectedValue(selectedChannelForm);
                
                //Update the timetable properties immediately, according to the first selected channel
                buildTimetable(view.getChannelComboBox().getSelectedValue());
            }

            /*@Override
            public void onInit() {
                view.getChannelComboBox().clean();
            }*/
        });
    }

    /**
     * Instiantiate a new tab panel
     * Detect which of the grid or the list must be updated
     * @param currentChannel
     **/
    private void buildTimetable(final ChannelForm currentChannel) {
        if (currentChannel == null) {
            view.getTabPanel().clean();
            view.removeAllNavigationLinks();
        } else {
            //Refresh either the grid or the list when one is visible
            ClickHandler gridClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    refreshTimetableGrid(view.getChannelComboBox().getSelectedValue());
                }
            };
            ClickHandler listClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    refreshTimetableList(view.getChannelComboBox().getSelectedValue());
                }
            };
            
            //Look in the current history if a tab selection has been specified
            final boolean autoSelectListTab = "list".equals(getHistoryPartLevel(2));
            
            view.buildTimetables(gridClickHandler, listClickHandler, autoSelectListTab);
            //Fixed: the following is already handled via the onClick() events when the grid is built
            /*switch (view.getTabPanel().getSelectedTabIndex()) {
                case 0:
                    refreshTimetableGrid(view.getChannelComboBox().getSelectedValue());
                    break;
                case 1:
                    refreshTimetableList(view.getChannelComboBox().getSelectedValue());
                    break;
            }*/
            refreshNavigationBar(currentChannel);
        }
    }

    private void refreshCurrentTimezoneList() {
        switch (view.getTabPanel().getSelectedTabIndex()) {
            case 0:
                refreshGridTimezoneList(view.getChannelComboBox().getSelectedValue());
                break;
            case 1:
                refreshListTimezoneList(view.getChannelComboBox().getSelectedValue());
                break;
        }
    }

    private void refreshCurrentTimetableSlots() {
        switch (view.getTabPanel().getSelectedTabIndex()) {
            case 0:
                setGridCurrentTime();
                break;
            case 1:
                refreshTimetableSlotList(
                        view.getChannelComboBox().getSelectedValue(),
                        view.getTimezoneList().getSelectedValue());
                break;
        }
    }

    private void refreshTimetableList(final ChannelForm currentChannel) {
        view.getTabPanel().getTabWidgetAt(1).clean();

        if (currentChannel != null) {

            //Add the timezone changer
            final ChangeHandler workingTimezoneChangeHandler = new ChangeHandler() {

                @Override
                public void onChange(ChangeEvent event) {
                    refreshTimetableSlotList(currentChannel, view.getTimezoneList().getSelectedValue());
                }
            };
            //Add the header click handler
            final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {

                @Override
                public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                    refreshTimetableSlotList(currentChannel, view.getTimezoneList().getSelectedValue());
                }
            };
            authorizationService.checkAccessViewTimetableSlot(currentChannel.getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getTabPanel()) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    return (!(caught instanceof AccessDeniedException));
                }

                @Override
                public void onResult(Boolean result) {
                    //Activate the menu entries if access is granted
                    if (result) {
                        //Rebuild the UI
                        view.refreshTimetableListPanel(workingPanelHeaderClickHandler, workingTimezoneChangeHandler);
                        //And update it immediately
                        refreshListTimezoneList(currentChannel);
                    }
                }
            });
        }
    }

    protected void refreshTimetableSlotList(final ChannelForm currentChannel, final String currentTimezone) {

        final YesNoClickHandler cancelSlotClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getTimetableList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                activateListTimetableSlot(false, view.getTimetableList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final ClickHandler reactivateSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                activateListTimetableSlot(true, view.getTimetableList(), view.getTimetableList().getCurrentRow(event));
            }
        };
        final ClickHandler selectPlaylistClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableList().getValueAt(view.getTimetableList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEPLAYLISTSELECT, slot);
            }
        };
        final ClickHandler moveSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableList().getValueAt(view.getTimetableList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEMOVE, slot);
            }
        };
        final ClickHandler resizeSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableList().getValueAt(view.getTimetableList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLERESIZE, slot);
            }
        };
        final ClickHandler resetDecommissioningSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableList().getValueAt(view.getTimetableList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDECOMISSIONING, slot);
            }
        };
        final ClickHandler cloneSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableList().getValueAt(view.getTimetableList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLECLONE, slot);
            }
        };
        final ClickHandler timetableSlotDisplayClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableList().getValueAt(view.getTimetableList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDISPLAY, slot);
            }
        };
        final SerializableItemSelectionHandler<String> playlistDisplaySelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        programmeService.getSecuredPlaylist(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<PlaylistForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<PlaylistForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final YesNoClickHandler deleteAllSlotsClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllListTimetableSlots(view.getTimetableList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteSlotClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getTimetableList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteListTimetableSlot(view.getTimetableList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        channelService.getSecuredTimetableSlotsForChannelId(view.getTimetableList().getCurrentGroupAndSortParameters(), currentChannel.getLabel(), currentTimezone, new ExtendedRPCCallback<ActionCollection<TimetableSlotForm>>(getEventBus(), view.getTimetableList()) {

            @Override
            public void onResult(ActionCollection<TimetableSlotForm> result) {
                view.refreshTimetableList(result,
                        cancelSlotClickHandler,
                        reactivateSlotClickHandler,
                        selectPlaylistClickHandler,
                        moveSlotClickHandler,
                        resizeSlotClickHandler,
                        resetDecommissioningSlotClickHandler,
                        cloneSlotClickHandler,
                        deleteSlotClickHandler,
                        deleteAllSlotsClickHandler,
                        timetableSlotDisplayClickHandler,
                        playlistDisplaySelectionHandler,
                        programmeDisplaySelectionHandler);
            }

            /*@Override
            public void onInit() {
                view.getTimetableList().clean();
            }*/
        });
    }

    private void refreshTimetableGrid(final ChannelForm currentChannel) {
        view.getTabPanel().getTabWidgetAt(0).clean();

        if (currentChannel != null) {

            final RefreshHandler<TimetableSlotForm> timetableChangeHandler = new RefreshHandler<TimetableSlotForm>() {

                @Override
                public void onRefresh(YearMonthDayHourMinuteSecondMillisecondInterface startOfWeek) {
                    refreshGridTimetableSlots(currentChannel, startOfWeek);
                }
            };

            final CellRefreshHandler cellChangeHandler = new CellRefreshHandler<TimetableSlotForm,HourMinuteInterface>() {

                @Override
                public void onContentRefresh( ActionCollectionEntry<TimetableSlotForm> currentCellForm, GenericPanelInterface captionContainer) {
                    refreshGridCellContent(currentCellForm, captionContainer);
                }

                @Override
                public void onCaptionRefresh(HourMinuteInterface startTime, HourMinuteInterface endTime, ActionCollectionEntry<TimetableSlotForm> currentCellForm, GenericPanelInterface captionContainer) {
                    refreshGridCellCaption(currentCellForm, startTime, endTime, captionContainer);
                }
            };

            authorizationService.checkAccessViewTimetableSlot(currentChannel.getLabel(), new ExtendedRPCCallback<Boolean>(getEventBus(), view.getTabPanel()) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    return (!(caught instanceof AccessDeniedException));
                }

                @Override
                public void onResult(Boolean result) {
                    //Activate the menu entries if access is granted
                    if (result) {
                        //Rebuild the UI
                        view.refreshTimetableGridPanel(timetableChangeHandler, cellChangeHandler);
                        //Check dependencies
                        view.refreshGridTimescaleList(getDefaultTimetableTimescaleLists());
                        refreshGridTimezoneList(currentChannel);
                    }
                }
            });

        }

    }

    private byte[] getDefaultTimetableTimescaleLists() {
        byte[] timescales = new byte[5];
        timescales[0] = 60;
        timescales[1] = 30;
        timescales[2] = 10;
        timescales[3] = 5;
        timescales[4] = 1;
        return timescales;
    }

    private void refreshGridTimezoneList(final ChannelForm currentChannel) {

        if (currentChannel != null) {
            loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimetableGrid()) {

                @Override
                public void onResult(String result) {
                    view.refreshGridTimezoneList(
                            currentChannel.getTimezone(),
                            result);
                    //Update the whole time slots now
                    setGridCurrentTime();
                }
            });
        }
    }

    private void refreshListTimezoneList(final ChannelForm currentChannel) {

        if (currentChannel != null) {
            loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneList()) {

                @Override
                public void onResult(String result) {
                    view.refreshListTimezoneList(
                            currentChannel.getTimezone(),
                            result);
                    //Update the whole time slots now
                    refreshTimetableSlotList(currentChannel, view.getTimezoneList().getSelectedValue());
                }
            });
        }
    }

    private void setGridCurrentTime() {
        //Either use the last know date set by the user or compute the current time
        if (view.getTimetableGrid().getCurrentDate()==null)

            helperService.getCurrentTime(view.getTimetableGrid().getTimeZone(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getTimetableGrid()) {

                @Override
                public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                    view.getTimetableGrid().setCurrentDate(result);
                }
            });
        else
            view.getTimetableGrid().forceRefresh();
    }

    private void refreshGridTimetableSlots(final ChannelForm currentChannel, YearMonthDayHourMinuteSecondMillisecondInterface startOfWeek) {

        view.getTimetableGrid().clean();

        //Fill in the timetable
        if (currentChannel != null && startOfWeek != null) {
            channelService.getSecuredTimetableSlotsForChannelIdAndWeek(currentChannel.getLabel(), startOfWeek, new ExtendedRPCCallback<ActionCollection<TimetableSlotForm>>(getEventBus(), view.getTimetableGrid()) {

                @Override
                public void onResult(ActionCollection<TimetableSlotForm> result2) {
                    //Fill in the slots; all of them
                    view.getTimetableGrid().onRefreshData(result2);

                }
            });

        }

    }

    private void deleteListTimetableSlot(final GenericFormTableInterface<TimetableSlotForm, ?> list, final int currentRow) {
        final TimetableSlotForm timetableSlot = list.getValueAt(currentRow);
        if (timetableSlot != null) {
            channelService.deleteSecuredTimetableSlot(timetableSlot.getRefId(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_deleted(timetableSlot.getRefId().getFullFriendlyID()));
                    //Refresh the corresponding list
                    //UPDATE: Now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
                }
            });
        }
    }

    private void deleteAllListTimetableSlots(final GenericFormTableInterface list) {
        final Collection<TimetableSlotForm> slots = list.getAllCheckedValues();
        if (slots != null && !slots.isEmpty()) {
            Collection ids = new HashSet<TimetableSlotFormID>();
            for (TimetableSlotForm form : slots) {
                ids.add(form.getRefId());
            }
            channelService.deleteSecuredTimetableSlots(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[slots.size()];
                    int i = 0;
                    for (TimetableSlotForm slot : slots) {
                        ids[i] = slot.getRefId().getSyntheticFriendlyID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetables_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: Now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
                }
            });
        }
    }

    private void deleteGridTimetableSlot(final GenericExtendedFormTimetableInterface<TimetableSlotForm,HourMinuteInterface> list, final CellCoordinates currentCoordinates) {
        final ActionCollectionEntry<TimetableSlotForm> timetableSlot = list.getFormAt(currentCoordinates);
        if (timetableSlot.getItem() != null) {
            channelService.deleteSecuredTimetableSlot(timetableSlot.getItem().getRefId(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_deleted(timetableSlot.getItem().getRefId().getFullFriendlyID()));
                    //Refresh the corresponding list
                    //UPDATE: Now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
                }
            });
        }
    }

    private void activateGridTimetableSlot(boolean enabled, final GenericExtendedFormTimetableInterface<TimetableSlotForm,HourMinuteInterface> list, final CellCoordinates currentCoordinates) {
        final ActionCollectionEntry<TimetableSlotForm> timetableSlot = list.getFormAt(currentCoordinates);
        channelService.enableSecuredTimetableSlot(timetableSlot.getItem().getRefId().getChannelId(), timetableSlot.getItem().getRefId().getSchedulingTime(), enabled, new ExtendedRPCCallback(getEventBus(), list) {

            @Override
            public void onResult(Object result) {
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_updated(timetableSlot.getItem().getRefId().getFullFriendlyID()));
                //Refresh the corresponding list
                //UPDATE: Now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
            }
        });
    }

    private void activateListTimetableSlot(boolean enabled, final GenericFormTableInterface<TimetableSlotForm, ?> list, final int currentRow) {
        final TimetableSlotForm timetableSlot = list.getValueAt(currentRow);
        channelService.enableSecuredTimetableSlot(timetableSlot.getRefId().getChannelId(), timetableSlot.getRefId().getSchedulingTime(), enabled, new ExtendedRPCCallback(getEventBus(), list) {

            @Override
            public void onResult(Object result) {
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_updated(timetableSlot.getRefId().getFullFriendlyID()));
                //Refresh the corresponding list
                //UPDATE: Now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
            }
        });
    }

    private void refreshGridCellCaption(final ActionCollectionEntry<TimetableSlotForm> currentCellForm, HourMinuteInterface startTime, HourMinuteInterface endTime, GenericPanelInterface captionContainer) {
        final ClickHandler timetableSlotDisplayClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDISPLAY, currentCellForm.getItem());
            }
        };
        if (currentCellForm != null) {
            //Slot bound to a playlist

            //And its playlist can be seen
            view.refreshGridCellCaption(captionContainer,
                    currentCellForm,
                    startTime,
                    endTime,
                    timetableSlotDisplayClickHandler);

        }
    }
    
    private void refreshGridCellContent(final ActionCollectionEntry<TimetableSlotForm> currentCellForm, final GenericPanelInterface captionContainer) {

        final YesNoClickHandler cancelSlotClickHandler = new YesNoClickHandler() {

            CellCoordinates currentCoordinates;

            @Override
            public void onPreClick(ClickEvent event) {
                currentCoordinates = view.getTimetableGrid().getCurrentCellCoordinates(event);
            }

            @Override
            public void onYesClick() {
                activateGridTimetableSlot(false, view.getTimetableGrid(), currentCoordinates);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final ClickHandler reactivateSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                activateGridTimetableSlot(true, view.getTimetableGrid(), view.getTimetableGrid().getCurrentCellCoordinates(event));
            }
        };
        final ClickHandler selectPlaylistClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableGrid().getFormAt(view.getTimetableGrid().getCurrentCellCoordinates(event)).getItem();
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEPLAYLISTSELECT, slot);
            }
        };
        final ClickHandler moveSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableGrid().getFormAt(view.getTimetableGrid().getCurrentCellCoordinates(event)).getItem();
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEMOVE, slot);
            }
        };
        final ClickHandler resizeSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableGrid().getFormAt(view.getTimetableGrid().getCurrentCellCoordinates(event)).getItem();
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLERESIZE, slot);
            }
        };
        final ClickHandler resetDecommissioningSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableGrid().getFormAt(view.getTimetableGrid().getCurrentCellCoordinates(event)).getItem();
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDECOMISSIONING, slot);
            }
        };
        final ClickHandler cloneSlotClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                TimetableSlotForm slot = view.getTimetableGrid().getFormAt(view.getTimetableGrid().getCurrentCellCoordinates(event)).getItem();
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLECLONE, slot);
            }
        };
        final SerializableItemSelectionHandler<String> playlistDisplaySelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        programmeService.getSecuredPlaylist(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<PlaylistForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<PlaylistForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue!=null) {
                        programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                }
            }
        };
        final YesNoClickHandler deleteSlotClickHandler = new YesNoClickHandler() {

            CellCoordinates currentCoordinates;

            @Override
            public void onPreClick(ClickEvent event) {
                currentCoordinates = view.getTimetableGrid().getCurrentCellCoordinates(event);
            }

            @Override
            public void onYesClick() {
                deleteGridTimetableSlot(view.getTimetableGrid(), currentCoordinates);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        if (currentCellForm != null) {
            //Slot bound to a playlist

            //And its playlist can be seen
            view.refreshGridCellContent(captionContainer,
                    currentCellForm,
                    cancelSlotClickHandler,
                    reactivateSlotClickHandler,
                    selectPlaylistClickHandler,
                    moveSlotClickHandler,
                    resizeSlotClickHandler,
                    resetDecommissioningSlotClickHandler,
                    cloneSlotClickHandler,
                    deleteSlotClickHandler,
                    playlistDisplaySelectionHandler,
                    programmeDisplaySelectionHandler);

        } else {
            //Void slot
            view.refreshVoidGridCell(captionContainer);
        }

    }
    
    /** Check whether the id given by the GUI event signal corresponds to a channel in the view list **/
    private boolean isInTimetableSlotList(String id) {
        switch (view.getTabPanel().getSelectedTabIndex()) {
            case 0:
                for (TimetableSlotForm timetableSlotForm : view.getTimetableGrid().getAllForms()) {
                    if (timetableSlotForm.getRefId().getRefID().equals(id))
                        return true;
                    }
                break;
            case 1:
                for (TimetableSlotForm timetableSlotForm : view.getTimetableList().getAllValues()) {
                    if (timetableSlotForm.getRefId().getRefID().equals(id))
                        return true;
                    }
                break;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case channel:
                        if (flag != FeedbackObjectForm.FLAG.edit) {
                            add(MessageEventsEnum.REFRESH_CHANNELS);
                        } else if (objectId == null || (view.getChannelComboBox().getSelectedValue() != null && objectId.equals(view.getChannelComboBox().getSelectedValue().getLabel()))) {
                            add(MessageEventsEnum.REFRESH_TIMETABLES);
                        }
                        break;
                    //If a timetable slot has been linked or unlinked to the currently selected channel then refresh the whole list
                    case timetableslot:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            //LIMITATION can detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_TIMETABLES);
                        } else {
                            if (isInTimetableSlotList(objectId))
                                add(MessageEventsEnum.REFRESH_TIMETABLES);                                
                        }
                        break;
                    //If the currently authenticated user has been updated refresh his time zone selection
                    case user:
                        if (flag == FeedbackObjectForm.FLAG.edit && UserIn.authenticatedUserName!=null && UserIn.authenticatedUserName.equals(objectId)) {
                            add(MessageEventsEnum.REFRESH_MYPROFILE);
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_CHANNELS:
                refreshChannelList();
                break;
            case REFRESH_PROGRAMMES:
            case REFRESH_PLAYLISTS:
                refreshCurrentTimetableSlots();
                break;
            case REFRESH_MYPROFILE:
                refreshCurrentTimezoneList();
                break;
            case REFRESH_TIMETABLES:
                refreshCurrentTimetableSlots();
                break;
        }
    }
}
