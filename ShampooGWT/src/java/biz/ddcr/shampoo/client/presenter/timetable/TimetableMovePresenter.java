/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableMoveView;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDayHourMinuteSecondSelectorInterface.YearMonthDayHourMinuteSecondTimeChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TimetableMovePresenter extends PopupPagePresenterWithFormParameter<TimetableMoveView, TimetableSlotForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    private LoginRPCServiceInterfaceAsync loginService;
    //private working copy of the original time
    private YearMonthDayHourMinuteSecondMillisecondInterface originalTime;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the timetable slot to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessUpdateTimetableSlot(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Clickable captions
        view.getChannelLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String channelId = getForm().getRefId().getChannelId();
                channelService.getSecuredChannel(channelId, new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<ChannelForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                    }
                });
            }
        });
        view.getProgrammeLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String programmeId = getForm().getProgrammeId().getItem();
                programmeService.getSecuredProgramme(programmeId, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                    }
                });
            }
        });
        view.getPlaylistLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String playlistId = getForm().getPlaylistId().getItem();
                programmeService.getSecuredPlaylist(playlistId, new ExtendedRPCCallback<ActionCollectionEntry<PlaylistForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<PlaylistForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, result.getItem());
                    }
                });
            }
        });

        //Switch the way the shift will be defined
        view.getMoveTypeByReScheduleRadioButton().addValueChangeHandler(new ValueChangeHandler() {

            @Override
            public void onValueChange(ValueChangeEvent event) {
                view.toggleMoveTypeChange();
            }
        });
        view.getMoveTypeByShiftRadioButton().addValueChangeHandler(new ValueChangeHandler() {

            @Override
            public void onValueChange(ValueChangeEvent event) {
                view.toggleMoveTypeChange();
            }
        });

        //Time slot boundaries
        view.getNewStartTimeSelector().setTimeChangeHandler(new YearMonthDayHourMinuteSecondTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayHourMinuteSecondMillisecondInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getNewStartTimeSelector().getCurrentTime()), view.getNewStartTimeSelector())) {
                    onStartTimeChanged();
                }
            }
        });
        view.getShiftBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (!Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getShiftBox().getValue()), view.getShiftBox())) {
                    onShiftChanged();
                }
            }
        });
        view.getShiftTypeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                onShiftChanged();
            }
        });

        //Change timezone
        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                updateTimeZone();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;

        if (view.getMoveTypeByReScheduleRadioButton().isSelected()) {
            //True == shift selected by date
            if (Validation.bindValidationError(Validation.checkDate(view.getNewStartTimeSelector().getCurrentTime()), view.getNewStartTimeSelector())) {
                result = false;
            }
        } else {
            //False == shift selected by number of minutes
            if (Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getShiftBox().getValue()), view.getShiftBox())) {
                result = false;
            }
        }

        return result;
    }

    public void submitForm() {

        int shift = view.getShiftTypeComboBox().getSelectedValue() ? -view.getShiftBox().getValue() : view.getShiftBox().getValue();
        channelService.shiftSecuredTimetableSlot(getForm().getRefId().getChannelId(), getForm().getRefId().getSchedulingTime(), shift, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_updated(getForm().getRefId().getFullFriendlyID()));
                //And refresh the back page
                //UPDATE: Now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
            }
        });

    }

    @Override
    protected void onRefresh() {

        originalTime = getForm().getRefId().getSchedulingTime();

        //Reset everything
        view.getChannelLabel().clean();
        view.getChannelLabel().setCaption(getForm().getRefId().getChannelId());
        view.getProgrammeLabel().clean();
        if (getForm().getProgrammeId().isReadable()) {
            view.getProgrammeLabel().setCaption(getForm().getProgrammeId().getItem());
        }
        view.getPlaylistLabel().clean();
        if (getForm().getPlaylistFriendlyId() != null) {
            if (getForm().getPlaylistFriendlyId().isReadable()) {
                view.getPlaylistLabel().setCaption(getForm().getPlaylistFriendlyId().getItem());
            }
        }
        view.getOriginalSchedulingTimeLabel().clean();
        view.getOriginalSchedulingTimeLabel().setCaption(originalTime.getI18nFullFriendlyString());

        view.toggleMoveTypeChange();

        view.getSubmit().clean();

        view.getShiftTypeComboBox().clean();
        view.refreshShiftKindList();

        view.getNewStartTimeSelector().clean();
        view.getShiftBox().clean();
        view.getTimezoneComboBox().clean();
        //Update timezone list
        //and
        //Now update the current start time
        feedTimeZoneListAndSetTime();

    }

    private void onShiftChanged() {

        //if length is less than the minimum value then update start time accordingly
        if (view.getShiftBox().getValue() < view.getShiftBox().getMinValue()) {
            view.getShiftBox().setValue(view.getShiftBox().getMinValue());
        }
        int shift = view.getShiftBox().getValue();
        if (view.getShiftTypeComboBox().getSelectedValue()) {
            //if true then it is rescheduled ahead of original time
            shift = -shift;
        }

        //The new starting time equals the shift plus the original start time
        helperService.addSeconds(originalTime, shift, new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getNewStartTimeSelector()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                view.getNewStartTimeSelector().setCurrentTime(result);
            }
        });

    }

    private void onStartTimeChanged() {

        YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime = originalTime;
        YearMonthDayHourMinuteSecondMillisecondInterface newStartTime = view.getNewStartTimeSelector().getCurrentTime();

        helperService.diffMilliseconds(originalStartTime, newStartTime, new ExtendedRPCCallback<Long>(getEventBus(), view.getNewStartTimeSelector()) {

            @Override
            public void onResult(Long result) {
                //Result must be converted from milliseconds to seconds
                if (result != null && Math.abs(result/1000) >= view.getShiftBox().getMinValue()) {
                    if (result < 0) {
                        //Then newStartTime happens before originalStartTime: the slot has been moved forward
                        view.getShiftTypeComboBox().setSelectedValue(true);
                    } else { //newStartTime is always different than originalStartTime thanks to the conditional test at the start of block
                        //Then originalStartTime happens before newStartTime: the slot has been postponed
                        view.getShiftTypeComboBox().setSelectedValue(false);
                    }
                    view.getShiftBox().setValue(Math.abs((int)(result/1000)));
                } else {
                    view.getShiftBox().setValue(view.getShiftBox().getMinValue());
                    //Recompute the start time
                    onShiftChanged();
                }

            }
        });

    }

    private void feedTimeZoneListAndSetTime() {
        loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(final String result1) {

                channelService.getChannelTimeZone(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

                    @Override
                    public void onResult(String result2) {
                        view.refreshTimezoneList(
                                result2,
                                result1);
                        //Update time now
                        updateTimeZone();
                    }
                });
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });
    }

    //Update the start time with the selected time zone
    //Trigger the onShiftChange() event when done
    private void updateTimeZone() {
        //Update the original time too
        helperService.switchTimeZone(getForm().getRefId().getSchedulingTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getOriginalSchedulingTimeLabel()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                originalTime = result;
                view.getOriginalSchedulingTimeLabel().setCaption(originalTime.getI18nFullFriendlyString());

                onShiftChanged();
            }
        });

    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case timetableslot:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag!=FeedbackObjectForm.FLAG.add && (objectId==null || objectId.equals(getForm().getRefId().getRefID())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
