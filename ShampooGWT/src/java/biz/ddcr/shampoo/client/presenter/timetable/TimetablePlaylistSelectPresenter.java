/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetablePlaylistSelectView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TimetablePlaylistSelectPresenter extends PopupPagePresenterWithFormParameter<TimetablePlaylistSelectView, TimetableSlotForm> {

    protected ChannelRPCServiceInterfaceAsync channelService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        //Check if user can view playlists AND link a playlist to the current timetable slot
        if (getForm() != null && getForm().getProgrammeId() != null) {
            authorizationService.checkAccessViewPlaylists(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        authorizationService.checkAccessLimitedPlaylistDependencyUpdateTimetable(getForm().getProgrammeId().getItem(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                            @Override
                            public void onResult(Boolean result) {
                                if (result) {
                                    bindPage();
                                } else {
                                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                                }
                            }
                        });
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        view.getPlaylistList().setRefreshClickHandler(new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshPlaylistList();
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    @Override
    protected void onRefresh() {
        view.getPlaylistList().clean();
        //Build the playlist list
        //And ask for a refresh as soon as it's done
        refreshPlaylistList();
        //No navigation bar links to refresh
    }

    public boolean validateForm() {
        return true;
    }

    public void submitForm(PlaylistForm playlist) {
        if (playlist!=null)
        channelService.setPlaylistSecuredTimetableSlot(getForm().getRefId().getChannelId(), getForm().getRefId().getSchedulingTime(), playlist.getRefID(), new ExtendedRPCCallback<Void>(getEventBus(), view.getPlaylistList()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_updated(getForm().getRefId().getFullFriendlyID()));
                //And refresh the back page
                //UPDATE: Now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PLAYLISTS);
            }
        });
    }

    protected void refreshPlaylistList() {

        //Okay the list must have been initialized beforhand, otherwise it would likely mean that either another tab panel is selected or that the user is denied acces to it
        if (view.getPlaylistList() != null) {

            final ClickHandler selectClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    PlaylistForm playlist = view.getPlaylistList().getValueAt(view.getPlaylistList().getCurrentRow(event));

                    if (playlist!=null) {
                        if (validateForm()) {
                           submitForm(playlist);
                        }
                    }

                }
            };
            final ClickHandler playlistDisplayClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    PlaylistForm playlist = view.getPlaylistList().getValueAt(view.getPlaylistList().getCurrentRow(event));
                    //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, playlist);
                }
            };
            final SerializableItemSelectionHandler<TimetableSlotFormID> timetableSlotDisplaySelectionHandler = new SerializableItemSelectionHandler<TimetableSlotFormID>() {

                @Override
                public void onItemSelected(TimetableSlotFormID currentValue) {
                    if (currentValue != null) {
                        channelService.getSecuredTimetableSlot(currentValue.getChannelId(), currentValue.getSchedulingTime(), new ExtendedRPCCallback<ActionCollectionEntry<TimetableSlotForm>>(getEventBus(), null) {

                            @Override
                            public void onResult(ActionCollectionEntry<TimetableSlotForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_TIMETABLEDISPLAY, result.getItem());
                            }
                        });
                    }
                }
            };

            programmeService.getSecuredPlaylistsForProgrammeId(view.getPlaylistList().getCurrentGroupAndSortParameters(), getForm().getProgrammeId().getItem(), new ExtendedRPCCallback<ActionCollection<PlaylistForm>>(getEventBus(), view.getPlaylistList()) {

               /* @Override
                public void onInit() {
                    view.getPlaylistList().clean();
                }*/

                @Override
                public void onResult(ActionCollection<PlaylistForm> result) {
                    view.refreshPlaylistList(
                            result,
                            selectClickHandler,
                            playlistDisplayClickHandler,
                            timetableSlotDisplaySelectionHandler);
                }
            });
        }
    }

    /** Check whether the id given by the GUI event signal corresponds to a playlist in the view list **/
    private boolean isInPlaylistList(String id) {
        for (PlaylistForm playlistForm : view.getPlaylistList().getAllValues()) {
            if (playlistForm.getRefID().equals(id))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case timetableslot:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag!=FeedbackObjectForm.FLAG.add && (objectId==null || objectId.equals(getForm().getRefId().getRefID())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                    case playlist:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            //LIMITATION can detect whether the new entity should be visible to the user or not: hence force refresh
                            add(MessageEventsEnum.REFRESH_PLAYLISTS);
                        } else {
                            if (isInPlaylistList(objectId))
                                add(MessageEventsEnum.REFRESH_PLAYLISTS);                                
                        }
                        break;
                }
            }
        }.get();
    }    

    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_PLAYLISTS:
                refreshPlaylistList();
                break;
        }
    }
}
