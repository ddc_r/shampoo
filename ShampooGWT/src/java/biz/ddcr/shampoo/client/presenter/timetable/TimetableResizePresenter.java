/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.timetable;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.timetable.TimetableResizeView;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDayHourMinuteSecondSelectorInterface.YearMonthDayHourMinuteSecondTimeChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TimetableResizePresenter extends PopupPagePresenterWithFormParameter<TimetableResizeView, TimetableSlotForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    private LoginRPCServiceInterfaceAsync loginService;
    //private working copy of the original start and end time; tehse are the only ones that will be updated using user time zone data
    private YearMonthDayHourMinuteSecondMillisecondInterface originalEndTime;
    private YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the timetable slot to update
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessUpdateTimetableSlot(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Clickable captions
        view.getChannelLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String channelId = getForm().getRefId().getChannelId();
                channelService.getSecuredChannel(channelId, new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<ChannelForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                    }
                });
            }
        });
        view.getProgrammeLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String programmeId = getForm().getProgrammeId().getItem();
                programmeService.getSecuredProgramme(programmeId, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                    }
                });
            }
        });
        view.getPlaylistLabel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                String playlistId = getForm().getPlaylistId().getItem();
                programmeService.getSecuredPlaylist(playlistId, new ExtendedRPCCallback<ActionCollectionEntry<PlaylistForm>>(getEventBus(), null) {

                    @Override
                    public void onResult(ActionCollectionEntry<PlaylistForm> result) {
                        //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PLAYLISTDISPLAY, result.getItem());
                    }
                });
            }
        });

        //Switch the way the shift will be defined
        view.getResizeTypeByDateRadioButton().addValueChangeHandler(new ValueChangeHandler() {

            @Override
            public void onValueChange(ValueChangeEvent event) {
                view.toggleResizeTypeChange();
            }
        });
        view.getResizeTypeByLengthRadioButton().addValueChangeHandler(new ValueChangeHandler() {

            @Override
            public void onValueChange(ValueChangeEvent event) {
                view.toggleResizeTypeChange();
            }
        });

        //Time slot boundaries
        view.getNewEndTimeSelector().setTimeChangeHandler(new YearMonthDayHourMinuteSecondTimeChangeHandler() {

            @Override
            public void onTimeChange(YearMonthDayHourMinuteSecondMillisecondInterface newTime) {
                if (!Validation.bindValidationError(Validation.checkDate(view.getNewEndTimeSelector().getCurrentTime()), view.getNewEndTimeSelector())) {
                    onEndTimeChanged();
                }
            }
        });
        view.getResizeBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (!Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getResizeBox().getValue()), view.getResizeBox())) {
                    onResizeChanged();
                }
            }
        });

        //Change timezone
        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                updateTimeZone();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;

        if (view.getResizeTypeByDateRadioButton().isSelected()) {
            //True == shift selected by date
            if (Validation.bindValidationError(Validation.checkDate(view.getNewEndTimeSelector().getCurrentTime()), view.getNewEndTimeSelector())) {
                result = false;
            }
        } else {
            //False == shift selected by number of minutes
            if (Validation.bindValidationError(Validation.checkPositiveIntegerNonZero(view.getResizeBox().getValue()), view.getResizeBox())) {
                result = false;
            }
        }

        if (getForm() instanceof RecurringTimetableSlotForm) {
            switch (((RecurringTimetableSlotForm) getForm()).getRecurringEvent()) {
                case daily:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanDay(view.getResizeBox().getValue()), view.getResizeBox())) {
                        result = false;
                    }
                    break;
                case weekly:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanWeek(view.getResizeBox().getValue()), view.getResizeBox())) {
                        result = false;
                    }
                    break;
                case monthly:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanMonth(view.getResizeBox().getValue()), view.getResizeBox())) {
                        result = false;
                    }
                    break;
                case yearly:
                    if (Validation.bindValidationError(Validation.checkSecondLengthLessThanYear(view.getResizeBox().getValue()), view.getResizeBox())) {
                        result = false;
                    }
                    break;
                default:
                    return false;
            }
        }

        return result;
    }

    public void submitForm() {

        channelService.resizeSecuredTimetableSlot(getForm().getRefId().getChannelId(), getForm().getRefId().getSchedulingTime(), view.getResizeBox().getValue(), new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().timetable_updated(getForm().getRefId().getFullFriendlyID()));
                //And refresh the back page
                //UPDATE: Now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_TIMETABLES);
            }
        });

    }

    @Override
    protected void onRefresh() {

        originalEndTime = getForm().getEndTime();
        originalStartTime = getForm().getRefId().getSchedulingTime();

        //Reset everything
        view.getChannelLabel().clean();
        view.getChannelLabel().setCaption(getForm().getRefId().getChannelId());
        view.getProgrammeLabel().clean();
        if (getForm().getProgrammeId().isReadable()) {
            view.getProgrammeLabel().setCaption(getForm().getProgrammeId().getItem());
        }
        view.getPlaylistLabel().clean();
        if (getForm().getPlaylistFriendlyId() != null) {
            if (getForm().getPlaylistFriendlyId().isReadable()) {
                view.getPlaylistLabel().setCaption(getForm().getPlaylistFriendlyId().getItem());
            }
        }
        view.getStartTimeLabel().clean();
        view.getOriginalEndTimeLabel().clean();

        view.toggleResizeTypeChange();

        view.getSubmit().clean();

        view.getNewEndTimeSelector().clean();
        view.getResizeBox().clean();
        view.getTimezoneComboBox().clean();
        //Update timezone list
        //and
        //Now update the current start time
        feedTimeZoneListAndSetTime();

    }

    private void onResizeChanged() {

        //if length is less than the minimum value then update end time accordingly
        if (view.getResizeBox().getValue() < view.getResizeBox().getMinValue()) {
            view.getResizeBox().setValue(view.getResizeBox().getMinValue());
        }

        //The new starting time equals the shift plus the original start time
        helperService.addSeconds(originalStartTime, view.getResizeBox().getValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getNewEndTimeSelector()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result) {
                view.getNewEndTimeSelector().setCurrentTime(result);
            }
        });

    }

    private void onEndTimeChanged() {

        YearMonthDayHourMinuteSecondMillisecondInterface newEndTime = view.getNewEndTimeSelector().getCurrentTime();

        helperService.diffMilliseconds(originalStartTime, newEndTime, new ExtendedRPCCallback<Long>(getEventBus(), view.getNewEndTimeSelector()) {

            @Override
            public void onResult(Long result) {
                //Result must be converted from milliseconds to seconds
                if (result != null && (result/1000) >= view.getResizeBox().getMinValue()) {
                    //the new ending is greater than the start time (i.e. result is positive) and is at least equal to the minimum required length
                    view.getResizeBox().setValue((int)(result/1000));
                } else {
                    view.getResizeBox().setValue(view.getResizeBox().getMinValue());
                    //Recompute the start time
                    onResizeChanged();
                }

            }
        });

    }

    private void feedTimeZoneListAndSetTime() {
        loginService.getCurrentlyAuthenticatedUserTimezone(new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(final String result1) {

                channelService.getChannelTimeZone(getForm().getRefId().getChannelId(), new ExtendedRPCCallback<String>(getEventBus(), view.getTimezoneComboBox()) {

                    @Override
                    public void onResult(String result2) {
                        view.refreshTimezoneList(
                                result2,
                                result1);
                        //Update time now
                        updateTimeZone();
                    }
                });
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });
    }

    //Update the start time with the selected time zone
    //Trigger the onShiftChange() event when done
    private void updateTimeZone() {
        //Update the original times too
        helperService.switchTimeZone(getForm().getRefId().getSchedulingTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getStartTimeLabel()) {

            @Override
            public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result1) {
                originalStartTime = result1;
                view.getStartTimeLabel().setCaption(originalStartTime.getI18nFullFriendlyString());

                helperService.switchTimeZone(getForm().getEndTime(), view.getTimezoneComboBox().getSelectedValue(), new ExtendedRPCCallback<YearMonthDayHourMinuteSecondMillisecondInterface>(getEventBus(), view.getOriginalEndTimeLabel()) {

                    @Override
                    public void onResult(YearMonthDayHourMinuteSecondMillisecondInterface result2) {
                        originalEndTime = result2;
                        view.getOriginalEndTimeLabel().setCaption(originalEndTime.getI18nFullFriendlyString());

                        onResizeChanged();
                    }
                });
            }
        });

    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case timetableslot:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag!=FeedbackObjectForm.FLAG.add && (objectId==null || objectId.equals(getForm().getRefId().getRefID())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
