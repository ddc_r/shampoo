/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.BroadcastAdvertForm;
import biz.ddcr.shampoo.client.form.track.BroadcastJingleForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.form.track.TagModule;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.BroadcastTrackAddView;
import biz.ddcr.shampoo.client.view.widgets.GWTCustomizableConfirmationDialogBox;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GenericFileUploadInterface.UploadHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackAddPresenter extends PopupPagePresenter<BroadcastTrackAddView> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private TrackRPCServiceInterfaceAsync trackService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    //private flags to keep track of what's been already uploaded
    private String currentTrackUploadId = null;
    private String currentCoverArtUploadId = null;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessAddBroadcastTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Set up the cover art upload widget with presenter data
        setAllowedPictureFormats();
        //Set up the track upload widget with presenter data
        setAllowedAudioFormats();
        //Get a fresh Java Session identifier
        rebindServletSessionIdToApplet();
    }

    @Override
    public void bindView() {

        //Set up the cover art upload widget with presenter data
        view.getAlbumCoverUploadWidget().setEventBus(eventBus);
        view.getAlbumCoverUploadWidget().setUploadHandler(new UploadHandler() {

            @Override
            public void onSuccess(String uploadId) {
                setNewCoverArtPicture(uploadId);
            }

            @Override
            public void onFailure() {
                resetCoverArtPicture();
            }
        });
        view.getResetAlbumCoverArtLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetCoverArtPicture();
            }
        });
        view.getAlbumCoverPicture().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Directly call the upload method from within the upload applet
                view.getAlbumCoverUploadWidget().triggerUpload();
            }
        });

        //Set up the track upload widget with presenter data
        view.getTrackUploadWidget().setEventBus(eventBus);
        view.getTrackUploadWidget().setUploadHandler(new UploadHandler() {

            @Override
            public void onSuccess(String uploadId) {
                setNewAudioTrack(uploadId);
            }

            @Override
            public void onFailure() {
                resetAudioTrack();
            }
        });
        view.getResetTrackLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetAudioTrack();
            }
        });
        view.getFillTagsFromTrackLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                fetchEmbeddedTags();
            }
        });

        //dependencies
        view.getAdvisoryAgeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                view.toggleAdvisoryFeaturesPanel();
            }
        });

        //Form validation handlers
        view.getAuthorTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkLabel(view.getAuthorTextBox().getText()), view.getAuthorTextBox());
            }
        });
        view.getTitleTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkLabel(view.getTitleTextBox().getText()), view.getTitleTextBox());
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;

        //An uploaded track is of course mandatory
        if (Validation.bindValidationError(
                currentTrackUploadId == null || currentTrackUploadId.length()==0
                ? I18nTranslator.getInstance().no_track_uploaded()
                : null, view.getTrackUploadWidget())) {
            result = false;
        }
        //A type too
        if (Validation.bindValidationError(
                view.getTypeComboBox().getSelectedValue() == null
                ? I18nTranslator.getInstance().item_is_malformed_or_unset()
                : null, view.getTypeComboBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkLabel(view.getAuthorTextBox().getText()), view.getAuthorTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkLabel(view.getTitleTextBox().getText()), view.getTitleTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getAlbumTextBox().getText()), view.getAlbumTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionTextBox().getText()), view.getDescriptionTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getGenreTextBox().getText()), view.getGenreTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getPublisherTextBox().getText()), view.getPublisherTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getCopyrightTextBox().getText()), view.getCopyrightTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getTagTextBox().getText()), view.getTagTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new Track from the view
        BroadcastTrackForm newTrack = null;
        switch (view.getTypeComboBox().getSelectedValue()) {
            case song:
                newTrack = new BroadcastSongForm();
                break;
            case jingle:
                newTrack = new BroadcastJingleForm();
                break;
            case advert:
                newTrack = new BroadcastAdvertForm();
                break;
        }
        if (newTrack != null) {

            newTrack.setActivated(view.getEnabledCheckBox().isChecked());

            //tags
            TagModule tags = new TagModule();
            PEGIRatingModule rating = null;
            if (view.getAdvisoryAgeComboBox().getSelectedValue() != null) {
                rating = new PEGIRatingModule();
                rating.setRating(view.getAdvisoryAgeComboBox().getSelectedValue());
                rating.setDiscrimination(view.getAdvisoryDiscriminationCheckBox().isChecked());
                rating.setDrugs(view.getAdvisoryDrugsCheckBox().isChecked());
                rating.setSex(view.getAdvisorySexCheckBox().isChecked());
                rating.setViolence(view.getAdvisoryViolenceCheckBox().isChecked());
                rating.setFear(view.getAdvisoryFearCheckBox().isChecked());
                rating.setProfanity(view.getAdvisoryProfanityCheckBox().isChecked());
            }
            tags.setAdvisory(rating);
            tags.setAlbum(view.getAlbumTextBox().getText());
            tags.setAuthor(view.getAuthorTextBox().getText());
            tags.setPublisher(view.getPublisherTextBox().getText());
            tags.setCopyright(view.getCopyrightTextBox().getText());
            tags.setDescription(view.getDescriptionTextBox().getText());
            tags.setGenre(view.getGenreTextBox().getText());
            tags.setTag(view.getTagTextBox().getText());
            tags.setTitle(view.getTitleTextBox().getText());
            tags.setYearOfRelease(view.getYearOfReleaseNumericBox().getValue() != null ? view.getYearOfReleaseNumericBox().getValue().shortValue() : null);
            newTrack.setMetadata(tags);

            //cover art
            //CoverArtModule is not read when adding a new track: the id of the file to bind is passed as a parameter to the insert callback instead

            //track
            //ContainerModule is not read when adding a new track: the id of the file to bind is passed as a parameter to the insert callback instead

            //programmes
            ActionMap<BroadcastTrackProgrammeModule> p = new ActionSortedMap<BroadcastTrackProgrammeModule>();
            for (String programme : view.getProgrammeComboBox().getSelectedKeys()) {
                p.addNude(
                            new BroadcastTrackProgrammeModule(
                                programme
                            )
                        );
            }
            newTrack.setTrackProgrammes(p);

            //Other form fields can be left unspecified, they should be filled in with correct values when the track is persisted

            submitForm(newTrack);

        }
    }

    public void submitForm(final BroadcastTrackForm newTrack) {

        //Check if a similar track is not already bound to the same programmes, and in this case ask the user what to do
        ExtendedRPCCallback callback =
                new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getSubmit()) {

                    @Override
                    public void onResult(final ActionCollection<ProgrammeForm> result) {

                        //Make up the list of programme names already bound
                        final Collection<String> alreadyBoundProgrammeNames = new HashSet<String>();
                        if (result != null)
                            for (ActionCollectionEntry<ProgrammeForm> entry : result)
                                if (entry.isReadable())
                                    alreadyBoundProgrammeNames.add(entry.getItem().getLabel());

                        if (alreadyBoundProgrammeNames.isEmpty()) {
                            //No doublon in sight, add the track right now, doublons may appear thereafter in case there's a concurrent call though
                            _submitForm(newTrack);
                        } else {
                            //There are programmes already bound to a similar track, ask the user what to do
                            GWTCustomizableConfirmationDialogBox confirmationBox = new GWTCustomizableConfirmationDialogBox();
                            confirmationBox.addMessage(
                                    I18nTranslator.getInstance().duplicated_track_detected(newTrack.getAuthor(), newTrack.getTitle()));
                            confirmationBox.addMessage(
                                    I18nTranslator.toCSV(alreadyBoundProgrammeNames));
                            confirmationBox.addMessage(
                                    I18nTranslator.getInstance().do_you_want_to_allow_track_duplicates());
                            confirmationBox.addButton(I18nTranslator.getInstance().yes(), new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    //Update duplicates
                                    _submitForm(newTrack);
                                }
                            });
                            confirmationBox.addButton(I18nTranslator.getInstance().no(), new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    //Ignore duplicates, i.e. remove the doublons from the original list
                                    //Submit the modified form ONLY if at least one programme remains, otherwise just close the box

                                    newTrack.removeProgrammes(alreadyBoundProgrammeNames);
                                    if (!newTrack.getTrackProgrammes().isEmpty())
                                        submitForm(newTrack);
                                }
                            });

                            confirmationBox.show();

                        }
                    }
                };

        if (newTrack instanceof BroadcastSongForm) {
            programmeService.getSecuredProgrammesForSongAndProgrammeIds(
                    newTrack.getAuthor(),
                    newTrack.getTitle(),
                    newTrack.getImmutableProgrammes(),
                    callback);
        } else if (newTrack instanceof BroadcastAdvertForm) {
            programmeService.getSecuredProgrammesForAdvertAndProgrammeIds(
                    newTrack.getAuthor(),
                    newTrack.getTitle(),
                    newTrack.getImmutableProgrammes(),
                    callback);
        } else if (newTrack instanceof BroadcastJingleForm) {
            programmeService.getSecuredProgrammesForJingleAndProgrammeIds(
                    newTrack.getAuthor(),
                    newTrack.getTitle(),
                    newTrack.getImmutableProgrammes(),
                    callback);
        }

    }

    private void _submitForm(BroadcastTrackForm newTrack) {
        final String newTrackName = newTrack.getFriendlyID();
        //Make it persistant
        ExtendedRPCCallback<Void> callback = new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().track_added(newTrackName));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
            }
        };
        if (currentTrackUploadId!=null && currentCoverArtUploadId!=null)
            trackService.addSecuredBroadcastTrack(newTrack, currentTrackUploadId, currentCoverArtUploadId, callback);
        else if (currentTrackUploadId!=null)
            trackService.addSecuredBroadcastTrack(newTrack, currentTrackUploadId, callback);
        //don't bother picture alone
    }

    @Override
    protected void onRefresh() {

        initializeCoverArtAppletURL();
        initializeAudioAppletURL();

        resetAudioTrack();
        resetCoverArtPicture();

        view.getSubmit().clean();

        //Common tags
        view.getEnabledCheckBox().clean();
        //By default, every track to add should be enabled
        view.getEnabledCheckBox().setChecked(true);
        view.getTypeComboBox().clean();
        feedTypeList();
        view.getAuthorTextBox().clean();
        view.getTitleTextBox().clean();
        view.getAlbumTextBox().clean();
        view.getDescriptionTextBox().clean();
        view.getYearOfReleaseNumericBox().clean();
        view.getGenreTextBox().clean();

        //Misc. options
        view.getAdvisoryAgeComboBox().clean();
        feedAdvisoryAgeList();
        view.toggleAdvisoryFeaturesPanel();
        view.getAdvisoryViolenceCheckBox().clean();
        view.getAdvisoryViolenceCheckBox().setChecked(false);
        view.getAdvisoryProfanityCheckBox().clean();
        view.getAdvisoryProfanityCheckBox().setChecked(false);
        view.getAdvisoryFearCheckBox().clean();
        view.getAdvisoryFearCheckBox().setChecked(false);
        view.getAdvisorySexCheckBox().clean();
        view.getAdvisorySexCheckBox().setChecked(false);
        view.getAdvisoryDrugsCheckBox().clean();
        view.getAdvisoryDrugsCheckBox().setChecked(false);
        view.getAdvisoryDiscriminationCheckBox().clean();
        view.getAdvisoryDiscriminationCheckBox().setChecked(false);
        view.getPublisherTextBox().clean();
        view.getCopyrightTextBox().clean();
        view.getTagTextBox().clean();

        //Clear programmes list
        view.getProgrammeComboBox().clean();

        //Update programmes list
        feedProgrammesList();

    }

    private void initializeCoverArtAppletURL() {
        
        helperService.getTemporaryPictureUploadURL(new ExtendedRPCCallback<String>(getEventBus(), view.getAlbumCoverUploadWidget()) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setAbsolutePostURL(result);
                
            }
        });
    }
    private void initializeAudioAppletURL() {
        
        helperService.getTemporaryAudioUploadURL(new ExtendedRPCCallback<String>(getEventBus(), view.getAlbumCoverUploadWidget()) {

            @Override
            public void onResult(String result) {
                view.getTrackUploadWidget().setAbsolutePostURL(result);
                
            }
        });
    }

    private void feedProgrammesList() {
        programmeService.getSecuredProgrammes(null, new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshProgrammeList(result);
            }
        });
    }

    private void feedTypeList() {
        //refresh the available type list
        TYPE[] types = new TYPE[3];
        types[0] = TYPE.song;
        types[1] = TYPE.jingle;
        types[2] = TYPE.advert;
        view.refreshTypeList(types);
    }

    private void feedAdvisoryAgeList() {
        //refresh the available PEGI rating system
        PEGI_AGE[] ages = new PEGI_AGE[6];
        //Default value is null, i.e. unselected
        ages[0] = null;
        ages[1] = PEGI_AGE.earlyChildhood;
        ages[2] = PEGI_AGE.everyone;
        ages[3] = PEGI_AGE.teen;
        ages[4] = PEGI_AGE.mature;
        ages[5] = PEGI_AGE.adultsOnly;
        view.refreshAdvisoryAgeList(ages);
    }

    private void setAllowedPictureFormats() {
        helperService.getAllowedPictureFormatExtensions(new ExtendedRPCCallback<Collection<String>>(getEventBus(), null) {

            @Override
            public void onResult(Collection<String> types) {
                view.getAlbumCoverUploadWidget().setAllowedFileExtensions(types);
            }
        });
    }

    private void setAllowedAudioFormats() {
        helperService.getAllowedAudioFormatExtensions(new ExtendedRPCCallback<Collection<String>>(getEventBus(), null) {

            @Override
            public void onResult(Collection<String> types) {
                view.getTrackUploadWidget().setAllowedFileExtensions(types);
            }
        });
    }

    private void setNewAudioTrack(String uploadId) {
        currentTrackUploadId = uploadId;
        view.getTrackInfoPanel().setVisible(true);
        //Make the "reset" and "fill tags" links visible
        view.getResetTrackLink().setVisible(true);
        view.getFillTagsFromTrackLink().setVisible(true);
        //Auto-fill empty fields with embedded tags
        fetchEmbeddedTags();
        //Fill in technical info about the file
        fetchContainerInfo();
    }

    private void setNewCoverArtPicture(String uploadId) {
        currentCoverArtUploadId = uploadId;
        //Make the "reset" link visible
        view.getResetAlbumCoverArtLink().setVisible(true);
        //Update the picture
        //Fill in technical info about the file
        fetchCoverArtInfo();
    }
    private void setEmbeddedCoverArtPicture(HasCoverArtInterface module) {
        //Since it's embedded the cover art has been uploaded with the same id as the track
        currentCoverArtUploadId = currentTrackUploadId;
        //Make the "reset" link visible
        view.getResetAlbumCoverArtLink().setVisible(true);
        //Update the picture
        //Fill in technical info about the file
        view.loadCoverArt(module);
    }

    private void resetAudioTrack() {
        currentTrackUploadId = null;
        view.getTrackInfoPanel().setVisible(false);
        //Clean the upload box
        view.getTrackUploadWidget().clean();
        //Make the "reset" and "fill tags" links invisble
        view.getResetTrackLink().setVisible(false);
        view.getFillTagsFromTrackLink().setVisible(false);
    }

    private void resetCoverArtPicture() {
        currentCoverArtUploadId = null;
        //Clean the upload box
        view.getAlbumCoverUploadWidget().clean();
        //Make the "reset" and "fill tags" links invisble
        view.getResetAlbumCoverArtLink().setVisible(false);
        //Reset the picture to an unindentified image
        view.unloadCoverArt();
    }

    private void fetchEmbeddedTags() {
        if (currentTrackUploadId != null) {
            trackService.getDraftTags(currentTrackUploadId, new ExtendedRPCCallback<EmbeddedTagModule>(getEventBus(), view.getFillTagsFromTrackLink()) {

                @Override
                public void onResult(EmbeddedTagModule tags) {
                    refreshTags(tags);
                }
            });
        }
    }

    private void fetchContainerInfo() {
        if (currentTrackUploadId != null) {
            trackService.getDraftAudioFileInfo(currentTrackUploadId, new ExtendedRPCCallback<ContainerModule>(getEventBus(), view.getFillTagsFromTrackLink()) {

                @Override
                public void onResult(ContainerModule info) {
                    refreshInfo(info);
                }
            });
        }
    }

    private void fetchCoverArtInfo() {
        if (currentCoverArtUploadId != null) {
            trackService.getDraftPictureFileInfo(currentCoverArtUploadId, new ExtendedRPCCallback<CoverArtModule>(getEventBus(), view.getAlbumCoverPicture()) {

                @Override
                public void onResult(CoverArtModule result) {
                    view.loadCoverArt(result);
                }
            });
        }
    }

    private void refreshInfo(ContainerModule info) {
        if (info!=null) {
            view.getTrackInfoPanel().clean();
            view.getTrackInfoPanel().add(
                    new GWTLabel(
                        I18nTranslator.getInstance().full_audio_track_technical_info(
                            info.getFormat().getI18nFriendlyString(),
                            Long.toString(info.getSize()),
                            info.getDuration().getI18nFriendlyString(),
                            Long.toString(info.getBitrate()),
                            Integer.toString(info.getSamplerate()),
                            Byte.toString(info.getChannels()),
                            info.isVbr()?"VBR":"A/CBR"
                        )
                    )
            );
        }
    }

    private void refreshTags(EmbeddedTagModule tags) {
        if (tags != null) {
            //Do only import tags if the user has left the corresponding field blank
            if (view.getAuthorTextBox().getText().length()==0) {
                view.getAuthorTextBox().setText(tags.getAuthor());
            }
            if (view.getTitleTextBox().getText().length()==0) {
                view.getTitleTextBox().setText(tags.getTitle());
            }
            if (view.getAlbumTextBox().getText().length()==0) {
                view.getAlbumTextBox().setText(tags.getAlbum());
            }
            if (view.getDescriptionTextBox().getText().length()==0) {
                view.getDescriptionTextBox().setText(tags.getDescription());
            }
            if (view.getYearOfReleaseNumericBox().getValue() == null) {
                view.getYearOfReleaseNumericBox().setValue(tags.getYearOfRelease() != null ? tags.getYearOfRelease().intValue() : null);
            }
            if (view.getGenreTextBox().getText().length()==0) {
                view.getGenreTextBox().setText(tags.getGenre());
            }
            if (view.getPublisherTextBox().getText().length()==0) {
                view.getPublisherTextBox().setText(tags.getPublisher());
            }
            if (view.getCopyrightTextBox().getText().length()==0) {
                view.getCopyrightTextBox().setText(tags.getCopyright());
            }
            if (view.getTagTextBox().getText().length()==0) {
                view.getTagTextBox().setText(tags.getTag());
            }
            if (view.getAdvisoryAgeComboBox().getSelectedValue() == null && tags.getAdvisory() != null) {
                view.getAdvisoryAgeComboBox().setSelectedValue(tags.getAdvisory().getRating());
                view.getAdvisoryViolenceCheckBox().setChecked(tags.getAdvisory().hasViolence());
                view.getAdvisoryProfanityCheckBox().setChecked(tags.getAdvisory().hasProfanity());
                view.getAdvisoryDrugsCheckBox().setChecked(tags.getAdvisory().hasDrugs());
                view.getAdvisorySexCheckBox().setChecked(tags.getAdvisory().hasSex());
                view.getAdvisoryDiscriminationCheckBox().setChecked(tags.getAdvisory().hasDiscrimination());
                view.toggleAdvisoryFeaturesPanel();
            }
            if (currentCoverArtUploadId == null && tags.hasPicture() && currentTrackUploadId != null) {
                setEmbeddedCoverArtPicture(tags);
            }
        }
    }

    protected void rebindServletSessionIdToApplet(){
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), null) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setCurrentSessionId(result);
                view.getTrackUploadWidget().setCurrentSessionId(result);
            }

        });
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
