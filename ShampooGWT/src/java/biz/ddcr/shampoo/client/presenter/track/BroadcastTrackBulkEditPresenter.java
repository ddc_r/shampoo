/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.*;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormsParameter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.BroadcastTrackBulkEditView;
import biz.ddcr.shampoo.client.view.widgets.GenericFileUploadInterface.UploadHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackBulkEditPresenter extends PopupPagePresenterWithFormsParameter<BroadcastTrackBulkEditView, Collection<BroadcastTrackForm>> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private TrackRPCServiceInterfaceAsync trackService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    //private flags to keep track of what's been freshly uploaded
    private String newCoverArtUploadId = null;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        //Don't try to check whether we have access to edition of all the given forms, assume that we can
        if (getForms() != null) {
            authorizationService.checkAccessUpdateBroadcastTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Set up the cover art upload widget with presenter data
        setAllowedPictureFormats();
        //Get a fresh Java Session identifier
        rebindServletSessionIdToApplet();
    }

    @Override
    public void bindView() {

        //Enable or disable edition for the corresonding fields
        view.getEnableCoverPictureEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleCoverPictureEdition();
            }
        });

        view.getEnableEnabledEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleEnableEdition();
            }
        });
        view.getEnableAlbumEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleAlbumEdition();
            }
        });
        view.getEnableDescriptionEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleDescriptionEdition();
            }
        });
        view.getEnableYearOfReleaseEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleYearOfReleaseEdition();
            }
        });
        view.getEnableGenreEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleGenreEdition();
            }
        });
        view.getEnablePEGIEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.togglePEGIEdition();
            }
        });
        view.getEnableCopyrightEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleCopyrightEdition();
            }
        });
        view.getEnableTagEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleTagEdition();
            }
        });
        view.getEnableProgrammeEditionCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                view.toggleProgrammesEdition();
            }
        });

        //Set up the cover art upload widget with presenter data
        view.getAlbumCoverUploadWidget().setEventBus(eventBus);
        view.getAlbumCoverUploadWidget().setUploadHandler(new UploadHandler() {

            @Override
            public void onSuccess(String uploadId) {
                setNewCoverArtPicture(uploadId);
            }

            @Override
            public void onFailure() {
                resetCoverArtPicture();
            }
        });
        view.getResetAlbumCoverArtLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetCoverArtPicture();
            }
        });
        view.getAlbumCoverPicture().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Directly call the upload method from within the upload applet
                view.getAlbumCoverUploadWidget().triggerUpload();
            }
        });

        //dependencies
        view.getAdvisoryAgeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                view.toggleAdvisoryFeaturesPanel();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;

        if (view.getEnableAlbumEditionCheckBox().isChecked() && Validation.bindValidationError(Validation.checkMaxText(view.getAlbumTextBox().getText()), view.getAlbumTextBox())) {
            result = false;
        }
        if (view.getEnableDescriptionEditionCheckBox().isChecked() && Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionTextBox().getText()), view.getDescriptionTextBox())) {
            result = false;
        }
        if (view.getEnableGenreEditionCheckBox().isChecked() && Validation.bindValidationError(Validation.checkMaxText(view.getGenreTextBox().getText()), view.getGenreTextBox())) {
            result = false;
        }
        if (view.getEnablePublisherEditionCheckBox().isChecked() && Validation.bindValidationError(Validation.checkMaxText(view.getPublisherTextBox().getText()), view.getPublisherTextBox())) {
            result = false;
        }
        if (view.getEnableCopyrightEditionCheckBox().isChecked() && Validation.bindValidationError(Validation.checkMaxText(view.getCopyrightTextBox().getText()), view.getCopyrightTextBox())) {
            result = false;
        }
        if (view.getEnableTagEditionCheckBox().isChecked() && Validation.bindValidationError(Validation.checkMaxText(view.getTagTextBox().getText()), view.getTagTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {

        if (//
                view.getEnableAlbumEditionCheckBox().isChecked() ||//
                view.getEnableCopyrightEditionCheckBox().isChecked() ||//
                view.getEnableCoverPictureEditionCheckBox().isChecked() ||//
                view.getEnableDescriptionEditionCheckBox().isChecked() ||//
                view.getEnableEnabledEditionCheckBox().isChecked() ||//
                view.getEnableGenreEditionCheckBox().isChecked() ||//
                view.getEnablePEGIEditionCheckBox().isChecked() ||//
                view.getEnableProgrammeEditionCheckBox().isChecked() ||//
                view.getEnableTagEditionCheckBox().isChecked() ||//
                view.getEnableYearOfReleaseEditionCheckBox().isChecked()) {

            ExtendedRPCCallback<Void> submissionCallback = new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                @Override
                public void onResult(Void result) {
                    //Show notification to user
                    String[] ids = new String[getForms().size()];
                    int i = 0;
                    for (BroadcastTrackForm track : getForms()) {
                        ids[i] = track.getFriendlyID();
                        i++;
                    }
                    //Then close popup
                    closePopup();
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().tracks_updated(I18nTranslator.toCSV(ids)));
                    //And refresh the back page
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            };

            ActionMap<BroadcastTrackProgrammeModule> p = new ActionSortedMap<BroadcastTrackProgrammeModule>();
            if (view.getEnableProgrammeEditionCheckBox().isChecked()) {
                for (String programme : view.getProgrammeComboBox().getSelectedKeys()) {
                    p.addNude(
                            new BroadcastTrackProgrammeModule(
                            programme));
                }
            }

            Collection<BroadcastTrackForm> newTracks = new HashSet<BroadcastTrackForm>();
            for (BroadcastTrackForm oldTrack : getForms()) {

                //Build a new Track from the view
                BroadcastTrackForm newTrack = null;
                if (oldTrack instanceof BroadcastSongForm) {
                    newTrack = new BroadcastSongForm();
                } else if (oldTrack instanceof BroadcastJingleForm) {
                    newTrack = new BroadcastJingleForm();
                } else if (oldTrack instanceof BroadcastAdvertForm) {
                    newTrack = new BroadcastAdvertForm();
                }

                if (newTrack != null) {

                    //untouched fields
                    newTrack.setRefID(oldTrack.getRefID());

                    newTrack.setActivated(//
                            view.getEnableEnabledEditionCheckBox().isChecked()
                            ? view.getEnabledCheckBox().isChecked()
                            : oldTrack.isActivated());

                    //tags
                    TagModule tags = new TagModule();
                    //Untouched fields
                    tags.setAuthor(oldTrack.getAuthor());
                    tags.setTitle(oldTrack.getTitle());
                    if (view.getEnablePEGIEditionCheckBox().isChecked()) {
                        PEGIRatingModule rating = null;
                        if (view.getAdvisoryAgeComboBox().getSelectedValue() != null) {
                            rating = new PEGIRatingModule();
                            rating.setRating(view.getAdvisoryAgeComboBox().getSelectedValue());
                            rating.setDiscrimination(view.getAdvisoryDiscriminationCheckBox().isChecked());
                            rating.setDrugs(view.getAdvisoryDrugsCheckBox().isChecked());
                            rating.setSex(view.getAdvisorySexCheckBox().isChecked());
                            rating.setViolence(view.getAdvisoryViolenceCheckBox().isChecked());
                            rating.setFear(view.getAdvisoryFearCheckBox().isChecked());
                            rating.setProfanity(view.getAdvisoryProfanityCheckBox().isChecked());
                        }
                        tags.setAdvisory(rating);
                    } else {
                        tags.setAdvisory(oldTrack.getAdvisory());
                    }

                    tags.setAlbum(
                            view.getEnableAlbumEditionCheckBox().isChecked()
                            ? view.getAlbumTextBox().getText()
                            : oldTrack.getAlbum());
                    tags.setPublisher(view.getEnablePublisherEditionCheckBox().isChecked()
                            ? view.getPublisherTextBox().getText()
                            : oldTrack.getPublisher());
                    tags.setCopyright(view.getEnableCopyrightEditionCheckBox().isChecked()
                            ? view.getCopyrightTextBox().getText()
                            : oldTrack.getCopyright());
                    tags.setDescription(view.getEnableDescriptionEditionCheckBox().isChecked()
                            ? view.getDescriptionTextBox().getText()
                            : oldTrack.getDescription());
                    tags.setGenre(view.getEnableGenreEditionCheckBox().isChecked()
                            ? view.getGenreTextBox().getText()
                            : oldTrack.getGenre());
                    tags.setTag(view.getEnableTagEditionCheckBox().isChecked()
                            ? view.getTagTextBox().getText()
                            : oldTrack.getTag());
                    tags.setYearOfRelease(view.getEnableYearOfReleaseEditionCheckBox().isChecked()
                            ? (view.getYearOfReleaseNumericBox().getValue() != null ? view.getYearOfReleaseNumericBox().getValue().shortValue() : null)
                            : oldTrack.getYearOfRelease());
                    newTrack.setMetadata(tags);

                    //cover art
                    //handled elsewhere

                    //programmes
                    newTrack.setTrackProgrammes(view.getEnableProgrammeEditionCheckBox().isChecked()
                            ? p
                            : oldTrack.getTrackProgrammes());

                    //Other form fields can be left unspecified, they should be filled in with correct values when the track is persisted

                    newTracks.add(newTrack);
                }
            }
            if (!newTracks.isEmpty()) {
                //Make it persistant
                if (view.getEnableCoverPictureEditionCheckBox().isChecked()) {
                    trackService.updateSecuredBroadcastTracksAndPictureOnly(newTracks,
                            newCoverArtUploadId,
                            submissionCallback);
                } else {
                    trackService.updateSecuredBroadcastTracks(newTracks,
                            submissionCallback);
                }
            }
        }
    }

    @Override
    protected void onRefresh() {

        initializeCoverArtAppletURL();
        resetCoverArtPicture();
        view.toggleCoverPictureEdition();

        view.getSubmit().clean();

        //Common tags
        view.getEnabledCheckBox().clean();
        //By default, every track to add should be enabled
        view.getEnabledCheckBox().setChecked(true);
        view.toggleEnableEdition();

        refreshTracksList(getForms());
        view.getAlbumTextBox().clean();
        view.toggleAlbumEdition();
        view.getDescriptionTextBox().clean();
        view.toggleDescriptionEdition();
        view.getYearOfReleaseNumericBox().clean();
        view.getYearOfReleaseNumericBox().setValue(null);
        view.toggleYearOfReleaseEdition();
        view.getGenreTextBox().clean();
        view.toggleGenreEdition();

        //Misc. options
        view.getAdvisoryAgeComboBox().clean();
        feedAdvisoryAgeList();
        view.getAdvisoryAgeComboBox().setSelectedValue(null);
        view.toggleAdvisoryFeaturesPanel();
        view.getAdvisoryViolenceCheckBox().clean();
        view.getAdvisoryViolenceCheckBox().setChecked(false);
        view.getAdvisoryProfanityCheckBox().clean();
        view.getAdvisoryProfanityCheckBox().setChecked(false);
        view.getAdvisoryFearCheckBox().clean();
        view.getAdvisoryFearCheckBox().setChecked(false);
        view.getAdvisorySexCheckBox().clean();
        view.getAdvisorySexCheckBox().setChecked(false);
        view.getAdvisoryDrugsCheckBox().clean();
        view.getAdvisoryDrugsCheckBox().setChecked(false);
        view.getAdvisoryDiscriminationCheckBox().clean();
        view.getAdvisoryDiscriminationCheckBox().setChecked(false);
        view.togglePEGIEdition();
        view.getPublisherTextBox().clean();
        view.togglePublisherEdition();
        view.getCopyrightTextBox().clean();
        view.toggleCopyrightEdition();
        view.getTagTextBox().clean();
        view.toggleTagEdition();

        //Clear programmes list
        view.getProgrammeComboBox().clean();
        //Update programmes list
        feedProgrammesList();
        view.toggleProgrammesEdition();

    }

    private void initializeCoverArtAppletURL() {

        helperService.getTemporaryPictureUploadURL(new ExtendedRPCCallback<String>(getEventBus(), view.getAlbumCoverUploadWidget()) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setAbsolutePostURL(result);

            }
        });
    }

    private void feedAdvisoryAgeList() {
        //refresh the available PEGI rating system
        PEGI_AGE[] ages = new PEGI_AGE[6];
        //Default value is null, i.e. unselected
        ages[0] = null;
        ages[1] = PEGI_AGE.earlyChildhood;
        ages[2] = PEGI_AGE.everyone;
        ages[3] = PEGI_AGE.teen;
        ages[4] = PEGI_AGE.mature;
        ages[5] = PEGI_AGE.adultsOnly;
        view.refreshAdvisoryAgeList(ages);
    }

    private void setAllowedPictureFormats() {
        helperService.getAllowedPictureFormatExtensions(new ExtendedRPCCallback<Collection<String>>(getEventBus(), null) {

            @Override
            public void onResult(Collection<String> types) {
                view.getAlbumCoverUploadWidget().setAllowedFileExtensions(types);
            }
        });
    }

    private void setNewCoverArtPicture(String uploadId) {
        newCoverArtUploadId = uploadId;
        //Make the "reset" link visible
        view.getResetAlbumCoverArtLink().setVisible(true);
        //Update the picture
        fetchCoverArtInfo();
    }

    private void fetchCoverArtInfo() {
        if (newCoverArtUploadId != null) {
            trackService.getDraftPictureFileInfo(newCoverArtUploadId, new ExtendedRPCCallback<CoverArtModule>(getEventBus(), view.getAlbumCoverPicture()) {

                @Override
                public void onResult(CoverArtModule result) {
                    view.loadCoverArt(result);
                }
            });
        }
    }

    private void resetCoverArtPicture() {
        newCoverArtUploadId = null;
        //Clean the upload box
        view.getAlbumCoverUploadWidget().clean();
        //Make the "reset" and "fill tags" links invisble
        view.getResetAlbumCoverArtLink().setVisible(false);
        //Reset the picture to an unindentified image
        view.unloadCoverArt();
    }

    private void feedProgrammesList() {

        programmeService.getSecuredProgrammes(null, new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshProgrammeList(
                        result);
            }

            /*@Override
            public void onInit() {
                view.getProgrammeComboBox().clean();
            }*/
        });

    }

    protected void rebindServletSessionIdToApplet() {
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), null) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setCurrentSessionId(result);
            }
        });
    }

    protected void refreshTracksList(Collection<BroadcastTrackForm> forms) {
        final ClickHandler trackDisplayClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                BroadcastTrackForm track = view.getTracksListBox().getValueAt(view.getTracksListBox().getCurrentRow(event));
                //One CAN view a track which is not yet ready
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKDISPLAY, track);
            }
        };
        view.refreshTracksList(forms, trackDisplayClickHandler);
    }
    
    private boolean isIdInForms(String objectId) {
        for (BroadcastTrackForm form : getForms())
            if (form.getRefID().equals(objectId))
                    return true;
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case broadcasttrack:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag==FeedbackObjectForm.FLAG.edit && (objectId==null || isIdInForms(objectId)))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
