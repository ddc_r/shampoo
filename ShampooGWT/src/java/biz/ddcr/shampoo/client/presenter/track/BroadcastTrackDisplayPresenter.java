/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.track.BroadcastAdvertForm;
import biz.ddcr.shampoo.client.form.track.BroadcastJingleForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotVoteException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.BroadcastTrackDisplayView;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackDisplayPresenter extends GenericFormPopupPagePresenter<BroadcastTrackDisplayView, BroadcastTrackForm> {

    protected LoginRPCServiceInterfaceAsync loginService;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the track to modify
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewBroadcastTrack(getForm().getRefID(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {
        //Do nothing
    }

    @Override
    public void onFormSpecificRefresh() {
        //Load the player, misc info, and the cover art
        loadOriginalAudioTrack();
        loadOriginalCoverArtPicture();

        //Common tags
        view.getEnabledLabel().setCaption(getForm().isActivated()?I18nTranslator.getInstance().is_true():I18nTranslator.getInstance().is_false());
        if (getForm() instanceof BroadcastSongForm) {
            view.getTypeLabel().setCaption(I18nTranslator.getInstance().song());
        } else if (getForm() instanceof BroadcastJingleForm) {
            view.getTypeLabel().setCaption(I18nTranslator.getInstance().jingle());
        } else if (getForm() instanceof BroadcastAdvertForm) {
            view.getTypeLabel().setCaption(I18nTranslator.getInstance().advert());
        } else {
            view.getTypeLabel().setCaption(I18nTranslator.getInstance().unknown());
        }
        view.getAuthorLabel().setCaption(getForm().getAuthor());
        view.getTitleLabel().setCaption(getForm().getTitle());
        view.getAlbumLabel().setCaption(getForm().getAlbum());
        view.getDescriptionLabel().setCaption(getForm().getDescription());
        view.getDurationLabel().setCaption(getForm().getTrackFile()!=null && getForm().getTrackFile().getItem()!=null ? getForm().getTrackFile().getItem().getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown());
        
        view.getYearOfReleaseLabel().setCaption(
                getForm().getYearOfRelease() != null
                ? getForm().getYearOfRelease().toString()
                : I18nTranslator.getInstance().unknown());
        view.getGenreLabel().setCaption(getForm().getGenre());

        //Vote
        feedVotes();

        //Misc. options
        view.refreshAdvisoryData(getForm().getAdvisory());
        view.getPublisherLabel().setCaption(getForm().getPublisher());
        view.getCopyrightLabel().setCaption(getForm().getCopyright());
        view.getTagLabel().setCaption(getForm().getTag());

        //Programmes
        view.getProgrammesGrid().clean();
        feedProgrammesList();

    }

    private void feedVotes() {
        if (getForm() instanceof BroadcastSongForm)
            view.refreshVoteData(getForm().getRefID(), ((BroadcastSongForm)getForm()).getAverageVote(), new VoteChangeHandler<String>() {

                @Override
                public void onSet(GenericMyVoteBoxInterface<String> source) {
                    updateMyVote(source);
                }

                @Override
                public void onGet(GenericMyVoteBoxInterface<String> source) {
                    fetchMyVote(source);
                }
            });
        else
            view.refreshVoteData(null, null, null);
    }

    private void feedProgrammesList() {
        view.refreshExistingProgrammes(getForm().getImmutableProgrammes());
    }

    private void loadOriginalAudioTrack() {
        view.refreshTrackInfo(getForm().getTrackFile());
    }

    private void loadOriginalCoverArtPicture() {
        view.loadCoverArt(getForm().getCoverArtFile());
    }

     /**
     *
     * Update a song rating with the data provided by a Rating widget, for the currently authenticated user
     *
     * @param ratingWidget
     **/
    private void updateMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.setCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), voteWidget.getMyVote(), new ExtendedRPCCallback<Void>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(Void result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().vote_registered());
                    //And refresh the back page
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);                    
                }
            });
        }
    }

    /**
     *
     * Retrieve the current user vote for a song, fetched as soon as the component is drawn on screen
     *
     * @param voteWidget
     **/
    private void fetchMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.getCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), new ExtendedRPCCallback<VOTE>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(VOTE result) {
                    voteWidget.setMyVote(result);
                }
            });
        }
    }

}
