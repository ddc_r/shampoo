/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.InterWindowEventsEnum;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormsParameter;
import biz.ddcr.shampoo.client.view.views.track.DelegatedBatchReviewView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class DelegatedBatchReviewPresenter extends PopupPagePresenterWithFormsParameter<DelegatedBatchReviewView, Collection<PendingTrackForm>> {

    public class BatchReview implements Serializable {
        Collection<PendingTrackForm> forms;
        String comment;

        protected BatchReview() {
        }

        public BatchReview(Collection<PendingTrackForm> forms, String comment) {
            this.forms = forms;
            this.comment = comment;
        }

        public String getComment() {
            return comment;
        }

        public Collection<PendingTrackForm> getForms() {
            return forms;
        }

    }

    @Override
    protected void onCheckAccess() {
        if (getForms()!=null)
            bindPage();
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                submitModule();
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public void submitModule() {
        //Close popup
        closePopup();
        //And send those modifications to the competent presenter
        feedback(view.getCommentTextBox().getText());
    }

    protected void feedback(String message) {
        getEventBus().dispatch(InterWindowEventsEnum.SEND_BATCH_REVIEW, new BatchReview(getForms(), message));
    }

    @Override
    protected void onRefresh() {
        view.getSubmit().clean();

        view.getCommentTextBox().clean();

    }

}
