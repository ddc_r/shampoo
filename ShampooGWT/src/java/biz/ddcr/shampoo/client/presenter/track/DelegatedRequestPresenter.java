/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.InterWindowEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackProgrammeModule;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.DelegatedRequestView;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class DelegatedRequestPresenter extends PopupPagePresenterWithFormParameter<DelegatedRequestView, BroadcastSongForm> {

    protected ChannelRPCServiceInterfaceAsync channelService;

    public class Request implements Serializable {
        BroadcastSongForm form;
        ChannelForm channel;
        String message;

        protected Request() {
        }

        public Request(BroadcastSongForm form, ChannelForm channel, String message) {
            this.form = form;
            this.channel = channel;
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public BroadcastSongForm getForm() {
            return form;
        }

        public ChannelForm getChannel() {
            return channel;
        }

    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        if (getForm()!=null)
            bindPage();
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                submitModule();
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public void submitModule() {
        //Do onyl take actions if at least one channel is available and is selected
        if (view.getChannelComboBox().getSelectedValue()!=null && !Validation.bindValidationError(Validation.checkMaxText(view.getMessageTextBox().getText()), view.getMessageTextBox())) {
            //Close popup
            closePopup();
            //And send those modifications to the competent presenter
            feedback(view.getChannelComboBox().getSelectedValue(), view.getMessageTextBox().getText());
        }
    }

    protected void feedback(ChannelForm channelForm, String message) {
        getEventBus().dispatch(InterWindowEventsEnum.SEND_REQUEST, new Request(getForm(), channelForm, message));
    }

    @Override
    protected void onRefresh() {
        view.getSubmit().clean();

        view.getMessageTextBox().clean();

        refreshChannelList();

    }

    private void refreshChannelList() {
        view.getChannelComboBox().clean();

        //Retrieve available channels from TrackProgrammeModules
        ActionCollection<BroadcastTrackProgrammeModule> programmeIdEntries = getForm().getTrackProgrammes();
        if (programmeIdEntries!=null) {
            Collection<String> programmeIds = new HashSet<String>();
            for (ActionCollectionEntry<BroadcastTrackProgrammeModule> trackProgrammeEntry : programmeIdEntries) {
                if (trackProgrammeEntry.isReadable())
                    programmeIds.add(trackProgrammeEntry.getItem().getProgramme());
            }
            channelService.getSecuredChannelsForProgrammeIds(null, programmeIds, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()){

                @Override
                public void onResult(ActionCollection<ChannelForm> result) {
                    view.refreshChannelList(result);
                }
            });
        }
        
    }

}
