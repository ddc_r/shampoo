/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.PendingTrackDisplayView;

/**
 *
 * @author okay_awright
 **/
public class PendingTrackDisplayPresenter extends GenericFormPopupPagePresenter<PendingTrackDisplayView, PendingTrackForm> {

    protected LoginRPCServiceInterfaceAsync loginService;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the track to modify
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewPendingTrack(getForm().getRefID(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {
        //Do nothing
    }

    @Override
    public void onFormSpecificRefresh() {
        //Load the player, misc info, and the cover art
        loadOriginalAudioTrack();
        loadOriginalCoverArtPicture();

        //Common tags
        switch (getForm().getType()) {
            case song:
                view.getTypeLabel().setCaption(I18nTranslator.getInstance().song());
                break;
            case advert:
                view.getTypeLabel().setCaption(I18nTranslator.getInstance().advert());
                break;
            case jingle:
                view.getTypeLabel().setCaption(I18nTranslator.getInstance().jingle());
                break;
            default:
                view.getTypeLabel().setCaption(I18nTranslator.getInstance().unknown());
        }
        view.getAuthorLabel().setCaption(getForm().getAuthor());
        view.getTitleLabel().setCaption(getForm().getTitle());
        view.getAlbumLabel().setCaption(getForm().getAlbum());
        view.getDescriptionLabel().setCaption(getForm().getDescription());
        view.getDurationLabel().setCaption(getForm().getTrackFile()!=null && getForm().getTrackFile().getItem()!=null ? getForm().getTrackFile().getItem().getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown());
        view.getYearOfReleaseLabel().setCaption(
                getForm().getYearOfRelease() != null
                ? getForm().getYearOfRelease().toString()
                : I18nTranslator.getInstance().unknown());
        view.getGenreLabel().getCaption();

        //Misc. options
        view.refreshAdvisoryData(getForm().getAdvisory());
        view.getPublisherLabel().setCaption(getForm().getPublisher());
        view.getCopyrightLabel().setCaption(getForm().getCopyright());
        view.getTagLabel().setCaption(getForm().getTag());

        //Programmes
        view.getProgrammesGrid().clean();
        feedProgrammesList();

    }

    private void feedProgrammesList() {
        view.refreshExistingProgrammes(getForm().getImmutableProgrammes());
    }

    private void loadOriginalAudioTrack() {
        view.refreshTrackInfo(getForm().getTrackFile());
    }

    private void loadOriginalCoverArtPicture() {
        view.loadCoverArt(getForm().getCoverArtFile());
    }

}
