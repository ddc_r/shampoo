/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.form.track.TagModule;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.PendingTrackEditView;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GenericFileUploadInterface.UploadHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class PendingTrackEditPresenter extends PopupPagePresenterWithFormParameter<PendingTrackEditView, PendingTrackForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private TrackRPCServiceInterfaceAsync trackService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;
    //private flags to keep track of what's been freshly uploaded
    private String newTrackUploadId = null;
    private String newCoverArtUploadId = null;
    //Do the picture widget and track widget point to a file?
    //If yes and new*UploadId is null then it must be the original file
    private boolean isTrackLoaded;
    private boolean isCoverArtLoaded;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the track to modify
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessUpdatePendingTrack(getForm().getRefID(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Set up the cover art upload widget with presenter data
        setAllowedPictureFormats();
        //Set up the track upload widget with presenter data
        setAllowedAudioFormats();
        //Get a fresh Java Session identifier
        rebindServletSessionIdToApplet();        
    }

    @Override
    public void bindView() {

        //Set up the cover art upload widget with presenter data
        view.getAlbumCoverUploadWidget().setEventBus(eventBus);
        view.getAlbumCoverUploadWidget().setUploadHandler(new UploadHandler() {

            @Override
            public void onSuccess(String uploadId) {
                setNewCoverArtPicture(uploadId);
            }

            @Override
            public void onFailure() {
                resetCoverArtPicture();
            }
        });
        view.getResetAlbumCoverArtLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetCoverArtPicture();
            }
        });
        view.getAlbumCoverPicture().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Directly call the upload method from within the upload applet
                view.getAlbumCoverUploadWidget().triggerUpload();
            }
        });

        //Set up the track upload widget with presenter data
        view.getTrackUploadWidget().setEventBus(eventBus);
        view.getTrackUploadWidget().setUploadHandler(new UploadHandler() {

            @Override
            public void onSuccess(String uploadId) {
                setNewAudioTrack(uploadId);
            }

            @Override
            public void onFailure() {
                resetAudioTrack();
            }
        });
        view.getResetTrackLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                resetAudioTrack();
            }
        });
        view.getFillTagsFromTrackLink().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                fetchEmbeddedTags();
            }
        });

        //dependencies
        view.getAdvisoryAgeComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                view.toggleAdvisoryFeaturesPanel();
            }
        });

        //Form validation handlers
        view.getAuthorTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkLabel(view.getAuthorTextBox().getText()), view.getAuthorTextBox());
            }
        });
        view.getTitleTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkLabel(view.getTitleTextBox().getText()), view.getTitleTextBox());
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm(prepareForm());
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;

        //A track is of course mandatory, may it be the original one or a new one
        if (Validation.bindValidationError(
                isTrackLoaded == false
                ? I18nTranslator.getInstance().no_track_uploaded()
                : null, view.getTrackUploadWidget())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkLabel(view.getAuthorTextBox().getText()), view.getAuthorTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkLabel(view.getTitleTextBox().getText()), view.getTitleTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getAlbumTextBox().getText()), view.getAlbumTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getDescriptionTextBox().getText()), view.getDescriptionTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getGenreTextBox().getText()), view.getGenreTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getPublisherTextBox().getText()), view.getPublisherTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getCopyrightTextBox().getText()), view.getCopyrightTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getTagTextBox().getText()), view.getTagTextBox())) {
            result = false;
        }

        return result;
    }

    public PendingTrackForm prepareForm() {
        //Build a new Track from the view
        PendingTrackForm newTrack = new PendingTrackForm();
        newTrack.setType(getForm().getType());

        newTrack.setRefID(getForm().getRefID());

        //tags
        TagModule tags = new TagModule();
        PEGIRatingModule rating = null;
        if (view.getAdvisoryAgeComboBox().getSelectedValue() != null) {
            rating = new PEGIRatingModule();
            rating.setRating(view.getAdvisoryAgeComboBox().getSelectedValue());
            rating.setDiscrimination(view.getAdvisoryDiscriminationCheckBox().isChecked());
            rating.setDrugs(view.getAdvisoryDrugsCheckBox().isChecked());
            rating.setSex(view.getAdvisorySexCheckBox().isChecked());
            rating.setViolence(view.getAdvisoryViolenceCheckBox().isChecked());
            rating.setFear(view.getAdvisoryFearCheckBox().isChecked());
            rating.setProfanity(view.getAdvisoryProfanityCheckBox().isChecked());
        }
        tags.setAdvisory(rating);
        tags.setAlbum(view.getAlbumTextBox().getText());
        tags.setAuthor(view.getAuthorTextBox().getText());
        tags.setPublisher(view.getPublisherTextBox().getText());
        tags.setCopyright(view.getCopyrightTextBox().getText());
        tags.setDescription(view.getDescriptionTextBox().getText());
        tags.setGenre(view.getGenreTextBox().getText());
        tags.setTag(view.getTagTextBox().getText());
        tags.setTitle(view.getTitleTextBox().getText());
        tags.setYearOfRelease(view.getYearOfReleaseNumericBox().getValue() != null ? view.getYearOfReleaseNumericBox().getValue().shortValue() : null);
        newTrack.setMetadata(tags);

        //cover art
        //handled elsewhere

        //track
        //handled elsewhere

        //programmes
        ActionMap<PendingTrackProgrammeModule> p = new ActionSortedMap<PendingTrackProgrammeModule>();
            for (String programme : view.getProgrammeComboBox().getSelectedKeys()) {
                p.addNude(
                            new PendingTrackProgrammeModule(
                                programme
                            )
                        );
            }
            newTrack.setTrackProgrammes(p);

        //Other form fields can be left unspecified, they should be filled in with correct values when the track is persisted

        return newTrack;
    }

    public void submitForm(PendingTrackForm track) {
        final String newTrackName = track.getFriendlyID();

        //Determine if updating a file is required
        boolean doUpdateTrackFile =
                //The file has either been removed or added
                ((getForm().getTrackFile() != null) != isTrackLoaded)
                //Or the file must have been replaced by an external resource
                || ((getForm().getTrackFile() != null) == isTrackLoaded && newTrackUploadId != null);


        boolean doUpdateCoverArtFile =
                //The file has either been removed or added
                (getForm().hasPicture() != isCoverArtLoaded)
                //Or the file must have been replaced by an external resource
                || (getForm().hasPicture() == isCoverArtLoaded && newCoverArtUploadId != null);

        ExtendedRPCCallback<Void> submissionCalback = new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {
            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().track_updated(newTrackName));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);

            }
        };

        //Make it persistant
        if (doUpdateTrackFile && doUpdateCoverArtFile) {
            trackService.updateSecuredPendingTrack(track,
                    newTrackUploadId,
                    newCoverArtUploadId,
                    submissionCalback);
        } else if (doUpdateCoverArtFile) {
            trackService.updateSecuredPendingTrackAndPictureOnly(track,
                    newCoverArtUploadId,
                    submissionCalback);
        } else if (doUpdateTrackFile) {
            trackService.updateSecuredPendingTrackAndAudioOnly(track,
                    newTrackUploadId,
                    submissionCalback);
        } else {
            trackService.updateSecuredPendingTrack(track,
                    submissionCalback);
        }

    }

    @Override
    protected void onRefresh() {

        initializeCoverArtAppletURL();
        initializeAudioAppletURL();

        resetAudioTrack();
        resetCoverArtPicture();

        loadOriginalAudioTrack();
        loadOriginalCoverArtPicture();

        view.getSubmit().clean();

        //Common tags
        view.getTypeComboBox().clean();
        feedTypeList();

        if (getForm().getType() != null) {
            view.getTypeComboBox().setSelectedValue(getForm().getType());
        }
        view.getAuthorTextBox().clean();
        view.getAuthorTextBox().setText(getForm().getAuthor());
        view.getTitleTextBox().clean();
        view.getTitleTextBox().setText(getForm().getTitle());
        view.getAlbumTextBox().clean();
        view.getAlbumTextBox().setText(getForm().getAlbum());
        view.getDescriptionTextBox().clean();
        view.getDescriptionTextBox().setText(getForm().getDescription());
        view.getYearOfReleaseNumericBox().clean();
        view.getYearOfReleaseNumericBox().setValue(
                getForm().getYearOfRelease() != null
                ? getForm().getYearOfRelease().intValue()
                : null);
        view.getGenreTextBox().clean();
        view.getGenreTextBox().getText();

        //Misc. options
        view.getAdvisoryAgeComboBox().clean();
        feedAdvisoryAgeList();
        view.getAdvisoryAgeComboBox().setSelectedValue(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().getRating()
                : null);
        view.toggleAdvisoryFeaturesPanel();
        view.getAdvisoryViolenceCheckBox().clean();
        view.getAdvisoryViolenceCheckBox().setChecked(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().hasViolence()
                : false);
        view.getAdvisoryProfanityCheckBox().clean();
        view.getAdvisoryProfanityCheckBox().setChecked(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().hasProfanity()
                : false);
        view.getAdvisoryFearCheckBox().clean();
        view.getAdvisoryFearCheckBox().setChecked(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().hasFear()
                : false);
        view.getAdvisorySexCheckBox().clean();
        view.getAdvisorySexCheckBox().setChecked(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().hasSex()
                : false);
        view.getAdvisoryDrugsCheckBox().clean();
        view.getAdvisoryDrugsCheckBox().setChecked(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().hasDrugs()
                : false);
        view.getAdvisoryDiscriminationCheckBox().clean();
        view.getAdvisoryDiscriminationCheckBox().setChecked(
                getForm().getAdvisory() != null
                ? getForm().getAdvisory().hasDiscrimination()
                : false);
        view.getPublisherTextBox().clean();
        view.getPublisherTextBox().setText(getForm().getPublisher());
        view.getCopyrightTextBox().clean();
        view.getCopyrightTextBox().setText(getForm().getCopyright());
        view.getTagTextBox().clean();
        view.getTagTextBox().setText(getForm().getTag());

        //Clear programmes list
        view.getProgrammeComboBox().clean();
        //Update programmes list
        feedProgrammesList();

    }

    private void initializeCoverArtAppletURL() {
        
        helperService.getTemporaryPictureUploadURL(new ExtendedRPCCallback<String>(getEventBus(), view.getAlbumCoverUploadWidget()) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setAbsolutePostURL(result);
                
            }
        });
    }
    private void initializeAudioAppletURL() {
        
        helperService.getTemporaryAudioUploadURL(new ExtendedRPCCallback<String>(getEventBus(), view.getAlbumCoverUploadWidget()) {

            @Override
            public void onResult(String result) {
                view.getTrackUploadWidget().setAbsolutePostURL(result);
                
            }
        });
    }

    private void feedSelectedProgrammesList() {
        programmeService.getSecuredProgrammesForTrackId(null, getForm().getRefID(), new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshExistingProgrammes(result);
            }

        });

    }

    private void feedTypeList() {
        //refresh the available type list
        TYPE[] types = new TYPE[3];
        types[0] = TYPE.song;
        types[1] = TYPE.jingle;
        types[2] = TYPE.advert;
        view.refreshTypeList(types);

    }

    private void feedAdvisoryAgeList() {
        //refresh the available PEGI rating system
        PEGI_AGE[] ages = new PEGI_AGE[6];
        //Default value is null, i.e. unselected
        ages[0] = null;
        ages[1] = PEGI_AGE.earlyChildhood;
        ages[2] = PEGI_AGE.everyone;
        ages[3] = PEGI_AGE.teen;
        ages[4] = PEGI_AGE.mature;
        ages[5] = PEGI_AGE.adultsOnly;
        view.refreshAdvisoryAgeList(ages);

    }

    private void setAllowedPictureFormats() {
        helperService.getAllowedPictureFormatExtensions(new ExtendedRPCCallback<Collection<String>>(getEventBus(), null) {

            @Override
            public void onResult(Collection<String> types) {
                view.getAlbumCoverUploadWidget().setAllowedFileExtensions(types);


            }
        });


    }

    private void setAllowedAudioFormats() {
        helperService.getAllowedAudioFormatExtensions(new ExtendedRPCCallback<Collection<String>>(getEventBus(), null) {

            @Override
            public void onResult(Collection<String> types) {
                view.getTrackUploadWidget().setAllowedFileExtensions(types);


            }
        });


    }

    private void loadOriginalAudioTrack() {
        if (getForm().getTrackFile() != null) {
            newTrackUploadId = null;
            isTrackLoaded = true;
            view.getTrackInfoPanel().setVisible(true);
            //Make the "reset" and "fill tags" links visible
            view.getResetTrackLink().setVisible(true);
            view.getFillTagsFromTrackLink().setVisible(false);
            //Fill in technical info about the file
            fetchContainerInfo();


        } else {
            resetAudioTrack();


        }
    }

    private void setNewAudioTrack(String uploadId) {
        newTrackUploadId = uploadId;
        isTrackLoaded = true;
        view.getTrackInfoPanel().setVisible(true);
        //Make the "reset" and "fill tags" links visible
        view.getResetTrackLink().setVisible(true);
        view.getFillTagsFromTrackLink().setVisible(true);
        //Auto-fill empty fields with embedded tags
        fetchEmbeddedTags();
        //Fill in technical info about the file
        fetchContainerInfo();


    }

    private void setNewCoverArtPicture(String uploadId) {
        newCoverArtUploadId = uploadId;
        isTrackLoaded = true;
        //Make the "reset" link visible
        view.getResetAlbumCoverArtLink().setVisible(true);
        //Update the picture
        fetchCoverArtInfo();
    }
    private void setEmbeddedCoverArtPicture(HasCoverArtInterface module) {
        //Since it's embedded the cover art has been uploaded with the same id as the track
        newCoverArtUploadId = newTrackUploadId;
        isTrackLoaded = true;
        //Make the "reset" link visible
        view.getResetAlbumCoverArtLink().setVisible(true);
        //Update the picture
        //Fill in technical info about the file
        view.loadCoverArt(module);
    }

    private void loadOriginalCoverArtPicture() {
        if (getForm().hasPicture()) {
            newCoverArtUploadId = null;
            isCoverArtLoaded = true;
            //Make the "reset" link visible
            view.getResetAlbumCoverArtLink().setVisible(true);
            //Update the picture
            view.loadCoverArt(getForm().getCoverArtFile()!=null?getForm().getCoverArtFile().getItem():null);
        } else {
            resetCoverArtPicture();
        }
    }

    private void resetAudioTrack() {
        newTrackUploadId = null;
        isTrackLoaded = false;
        view.getTrackInfoPanel().setVisible(false);
        //Clean the upload box
        view.getTrackUploadWidget().clean();
        //Make the "reset" and "fill tags" links invisble
        view.getResetTrackLink().setVisible(false);
        view.getFillTagsFromTrackLink().setVisible(false);

    }

    private void resetCoverArtPicture() {
        newCoverArtUploadId = null;
        isCoverArtLoaded = false;
        //Clean the upload box
        view.getAlbumCoverUploadWidget().clean();
        //Make the "reset" and "fill tags" links invisble
        view.getResetAlbumCoverArtLink().setVisible(false);
        //Reset the picture to an unindentified image
        view.unloadCoverArt();

    }

    private void fetchCoverArtInfo() {
        if (newCoverArtUploadId != null) {
            trackService.getDraftPictureFileInfo(newCoverArtUploadId, new ExtendedRPCCallback<CoverArtModule>(getEventBus(), view.getAlbumCoverPicture()) {

                @Override
                public void onResult(CoverArtModule result) {
                    view.loadCoverArt(result);
                }
            });
        }
    }

    private void fetchEmbeddedTags() {
        if (newTrackUploadId != null) {
            trackService.getDraftTags(newTrackUploadId, new ExtendedRPCCallback<EmbeddedTagModule>(getEventBus(), view.getFillTagsFromTrackLink()) {

                @Override
                public void onResult(EmbeddedTagModule tags) {
                    refreshTags(tags);


                }
            });


        }
    }

    private void fetchContainerInfo() {
        if (isTrackLoaded) {
            //Ask the server session what kind of file it is if it's just been uploaded
            if (newTrackUploadId != null) {
                trackService.getDraftAudioFileInfo(newTrackUploadId, new ExtendedRPCCallback<ContainerModule>(getEventBus(), view.getFillTagsFromTrackLink()) {

                    @Override
                    public void onResult(ContainerModule info) {
                        refreshInfo(info);


                    }
                });


            } else {
                //Otherwise, directly get the info from the parameter form
                refreshInfo(getForm().getTrackFile()!=null?getForm().getTrackFile().getItem():null);


            }
        }
    }

    private void refreshInfo(ContainerModule info) {
        if (info != null) {
            view.getTrackInfoPanel().clean();
            view.getTrackInfoPanel().add(
                    new GWTLabel(
                    I18nTranslator.getInstance().full_audio_track_technical_info(
                    info.getFormat().getI18nFriendlyString(),
                    Long.toString(info.getSize()),
                    info.getDuration().getI18nFriendlyString(),
                    Long.toString(info.getBitrate()),
                    Integer.toString(info.getSamplerate()),
                    Byte.toString(info.getChannels()),
                    info.isVbr() ? "VBR" : "A/CBR")));


        }
    }

    private void refreshTags(EmbeddedTagModule tags) {
        if (tags != null) {
            //Do only import tags if the user has left the corresponding field blank
            if (view.getAuthorTextBox().getText().length()==0) {
                view.getAuthorTextBox().setText(tags.getAuthor());
            }
            if (view.getTitleTextBox().getText().length()==0) {
                view.getTitleTextBox().setText(tags.getTitle());
            }
            if (view.getAlbumTextBox().getText().length()==0) {
                view.getAlbumTextBox().setText(tags.getAlbum());
            }
            if (view.getDescriptionTextBox().getText().length()==0) {
                view.getDescriptionTextBox().setText(tags.getDescription());
            }
            if (view.getYearOfReleaseNumericBox().getValue() == null) {
                view.getYearOfReleaseNumericBox().setValue(tags.getYearOfRelease() != null ? tags.getYearOfRelease().intValue() : null);
            }
            if (view.getGenreTextBox().getText().length()==0) {
                view.getGenreTextBox().setText(tags.getGenre());
            }
            if (view.getPublisherTextBox().getText().length()==0) {
                view.getPublisherTextBox().setText(tags.getPublisher());
            }
            if (view.getCopyrightTextBox().getText().length()==0) {
                view.getCopyrightTextBox().setText(tags.getCopyright());
            }
            if (view.getTagTextBox().getText().length()==0) {
                view.getTagTextBox().setText(tags.getTag());
            }
            if (view.getAdvisoryAgeComboBox().getSelectedValue() == null && tags.getAdvisory() != null) {
                view.getAdvisoryAgeComboBox().setSelectedValue(tags.getAdvisory().getRating());
                view.getAdvisoryViolenceCheckBox().setChecked(tags.getAdvisory().hasViolence());
                view.getAdvisoryProfanityCheckBox().setChecked(tags.getAdvisory().hasProfanity());
                view.getAdvisoryDrugsCheckBox().setChecked(tags.getAdvisory().hasDrugs());
                view.getAdvisorySexCheckBox().setChecked(tags.getAdvisory().hasSex());
                view.getAdvisoryDiscriminationCheckBox().setChecked(tags.getAdvisory().hasDiscrimination());
                view.toggleAdvisoryFeaturesPanel();
            }
            if (!isCoverArtLoaded && tags.hasPicture() && newTrackUploadId != null) {
                setEmbeddedCoverArtPicture(tags);
            }
        }
    }

    private void feedProgrammesList() {

        programmeService.getSecuredProgrammes(null, new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getProgrammeComboBox()) {

            @Override
            public void onResult(ActionCollection<ProgrammeForm> result) {
                view.refreshProgrammeList(
                        result);
                feedSelectedProgrammesList();
            }

            /*@Override
            public void onInit() {
                view.getProgrammeComboBox().clean();
            }*/

        });


    }

    protected void rebindServletSessionIdToApplet(){
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), null) {

            @Override
            public void onResult(String result) {
                view.getAlbumCoverUploadWidget().setCurrentSessionId(result);
                view.getTrackUploadWidget().setCurrentSessionId(result);
            }

        });
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case pendingtrack:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag==FeedbackObjectForm.FLAG.edit && (objectId==null || objectId.equals(getForm().getRefID())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
    
}
