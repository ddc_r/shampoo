/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.track;

import biz.ddcr.shampoo.client.InterWindowEventsEnum;
import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotRequestException;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotVoteException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.presenter.track.DelegatedBatchReviewPresenter.BatchReview;
import biz.ddcr.shampoo.client.presenter.track.DelegatedRequestPresenter.Request;
import biz.ddcr.shampoo.client.presenter.track.DelegatedReviewPresenter.Review;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.track.TrackListView;
import biz.ddcr.shampoo.client.view.widgets.GWTCustomizableConfirmationDialogBox;
import biz.ddcr.shampoo.client.view.widgets.GenericCallbackInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author okay_awright
 *
 */
public class TrackListPresenter extends PagePresenterWithHistory<TrackListView> {

    protected TrackRPCServiceInterfaceAsync trackService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;
    protected LoginRPCServiceInterfaceAsync loginService;
    protected ProgrammeRPCServiceInterfaceAsync programmeService;
    protected HelperRPCServiceInterfaceAsync helperService;

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setTrackService(TrackRPCServiceInterfaceAsync trackService) {
        this.trackService = trackService;
    }

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        // can view either the pending track submission queue or the broadcastable track list, or both
        authorizationService.checkAccessViewTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        view.getTabPanel().clean();
        view.removeAllNavigationLinks();
        //Build all the available tabs
        //And ask for a refresh as soon as it's done
        buildLists();
        //Show the navigation bar links you are granted to see only
        refreshNavigationBar();
    }

    protected void buildLists() {
        _canBuildBroadcastList();
    }

    private void _canBuildBroadcastList() {
        authorizationService.checkAccessViewBroadcastTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getTabPanel()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                _canBuildPendingList(false);
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                _canBuildPendingList(result);
            }
        });
    }

    private void _canBuildPendingList(final boolean canViewBroadcastTracks) {
        authorizationService.checkAccessViewPendingTracks(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getTabPanel()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                _buildTabPanel(canViewBroadcastTracks, false);
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                _buildTabPanel(canViewBroadcastTracks, result);
            }
        });
    }

    private void _buildTabPanel(boolean canViewBroadcastTracks, boolean canViewPendingTracks) {
        //Refresh either list when one is visible
        ClickHandler broadcastTrackClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                buildBroadcastTrackList();
            }
        };
        ClickHandler pendingTrackClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                buildPendingTrackList();
            }
        };

        //Look in the current history if a tab selection has been specified
        final boolean autoSelectPendingTab = "pending".equals(getHistoryPartLevel(1));

        //Build the tabs, only the ones that are visible to the user
        view.buildTabPanel(
                canViewBroadcastTracks, broadcastTrackClickHandler,
                canViewPendingTracks, pendingTrackClickHandler,
                autoSelectPendingTab);

    }

    protected void buildBroadcastTrackList() {
        final SortableTableClickHandler broadcastPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshBroadcastTrackList();
            }
        };
        view.refreshBroadcastTrackPanel(broadcastPanelHeaderClickHandler);
        //refresh the UI as soon as possible with content
        refreshBroadcastTrackList();
    }

    protected void buildPendingTrackList() {
        final SortableTableClickHandler pendingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshPendingTrackList();
            }
        };
        view.refreshPendingTrackPanel(pendingPanelHeaderClickHandler);
        //refresh the UI as soon as possible with content
        refreshPendingTrackList();
    }

    protected void refreshNavigationBar() {

        view.removeAllNavigationLinks();

        //Can add broadcastable tracks in one go
        final ClickHandler addBroadcastTrackClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKADD);
            }
        };
        authorizationService.checkAccessAddBroadcastTracks(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddBroadcastTrackLink(addBroadcastTrackClickHandler);
                }
            }
        });

        //can add tracks that must first go through the validation process
        final ClickHandler addPendingTrackClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PENDINGTRACKADD);
            }
        };
        authorizationService.checkAccessAddPendingTracks(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddPendingTrackLink(addPendingTrackClickHandler);
                }
            }
        });

    }

    protected void refreshBroadcastTrackList() {

        //Okay the list must have been initialized beforhand, otherwise it would likely mean that either another tab panel is selected or that the user is denied acces to it
        if (view.getBroadcastTrackList() != null) {

            final ClickHandler addRequestClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BroadcastTrackForm track = view.getBroadcastTrackList().getValueAt(view.getBroadcastTrackList().getCurrentRow(event));
                    //One cannot touch a track which is not yet ready
                    //Failsafe: check if the track is a song as well, shouldn't be necessary
                    if (track.isReady() && (track instanceof BroadcastSongForm)) //Display a review editor
                    {
                        getEventBus().dispatch(InterWindowEventsEnum.SHOW_REQUEST_EDITOR, (BroadcastSongForm) track);
                    }
                }
            };
            final ClickHandler deleteRequestClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    deleteMyRequest(view.getBroadcastTrackList(), view.getBroadcastTrackList().getCurrentRow(event));
                }
            };
            final GenericCallbackInterface<String, GenericPanelInterface, GenericPanelInterface> requestHandler = new GenericCallbackInterface<String, GenericPanelInterface, GenericPanelInterface>() {

                @Override
                public void onCallBack(String id, GenericPanelInterface source1, GenericPanelInterface source2) {
                    isMyRequest(id, source1, source2, addRequestClickHandler, deleteRequestClickHandler);
                }

            };

            final VoteChangeHandler<String> myRatingChangeHandler = new VoteChangeHandler<String>() {

                @Override
                public void onSet(GenericMyVoteBoxInterface<String> source) {
                    updateMyVote(source);
                }

                @Override
                public void onGet(GenericMyVoteBoxInterface<String> source) {
                    fetchMyVote(source);
                }
            };
            final ClickHandler editAllClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    final Collection<BroadcastTrackForm> tracks = view.getBroadcastTrackList().getAllCheckedValues();
                    //Display a review editor
                    if (tracks != null && !tracks.isEmpty()) {
                        getEventBus().dispatch(RedirectionEventsEnum.SHOW_BROADCASTTRACKBULKEDIT, tracks);
                    }
                }
            };
            final ClickHandler editClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BroadcastTrackForm track = view.getBroadcastTrackList().getValueAt(view.getBroadcastTrackList().getCurrentRow(event));
                    //One cannot touch a track which is not yet ready
                    if (track.isReady()) {
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKEDIT, track);
                    }
                }
            };
            final ClickHandler trackDisplayClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    BroadcastTrackForm track = view.getBroadcastTrackList().getValueAt(view.getBroadcastTrackList().getCurrentRow(event));
                    //One CAN view a track which is not yet ready
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_BROADCASTTRACKDISPLAY, track);
                }
            };
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler = new SerializableItemSelectionHandler<String>() {

                @Override
                public void onItemSelected(String currentValue) {
                    if (currentValue != null) {
                        programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {
                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                    }
                }
            };
            final YesNoClickHandler deleteAllClickHandler = new YesNoClickHandler() {

                @Override
                public void onPreClick(ClickEvent event) {
                    //Do nothing
                }

                @Override
                public void onYesClick() {
                    deleteAllBroadcastTracks(view.getBroadcastTrackList());
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final YesNoClickHandler deleteClickHandler = new YesNoClickHandler() {

                int currentRow = -1;

                @Override
                public void onPreClick(ClickEvent event) {
                    currentRow = view.getBroadcastTrackList().getCurrentRow(event);
                }

                @Override
                public void onYesClick() {
                    deleteBroadcastTrack(view.getBroadcastTrackList(), currentRow);
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };

            trackService.getSecuredBroadcastTracks(view.getBroadcastTrackList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<? extends BroadcastTrackForm>>(getEventBus(), view.getBroadcastTrackList()) {

                /*@Override
                 public void onInit() {
                 view.getBroadcastTrackList().clean();
                 }*/
                @Override
                public void onResult(ActionCollection<? extends BroadcastTrackForm> result) {
                    _refreshBroadcastTrackList(
                            result,
                            editClickHandler,
                            editAllClickHandler,
                            deleteClickHandler,
                            deleteAllClickHandler,
                            myRatingChangeHandler,
                            trackDisplayClickHandler,
                            programmeDisplaySelectionHandler,
                            requestHandler,
                            eventBus);
                }
            });
        }
    }

    private void _refreshBroadcastTrackList(
            final ActionCollection<? extends BroadcastTrackForm> tracks,
            final ClickHandler editClickHandler,
            final ClickHandler editAllClickHandler,
            final YesNoClickHandler deleteClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            final VoteChangeHandler<String> myRatingChangeHandler,
            final ClickHandler trackDisplayClickHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler,
            final GenericCallbackInterface<String, GenericPanelInterface, GenericPanelInterface> requestHandler,
            EventBusWithLookup eventBus) {
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), view.getBroadcastTrackList()) {

            @Override
            public void onResult(String result) {
                view.refreshBroadcastTrackList(
                        tracks,
                        editClickHandler,
                        editAllClickHandler,
                        deleteClickHandler,
                        deleteAllClickHandler,
                        myRatingChangeHandler,
                        trackDisplayClickHandler,
                        programmeDisplaySelectionHandler,
                        requestHandler,
                        eventBus,
                        result);
            }
        });

    }

    /**
     *
     * Update a song rating with the data provided by a Rating widget, for the
     * currently authenticated user
     *
     * @param ratingWidget
     *
     */
    private void updateMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.setCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), voteWidget.getMyVote(), new ExtendedRPCCallback<Void>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(Void result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().vote_registered());
                    //And refresh the back page
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    /**
     *
     * Retrieve the current user vote for a song, fetched as soon as the
     * component is drawn on screen
     *
     * @param voteWidget
     *
     */
    private void fetchMyVote(final GenericMyVoteBoxInterface<String> voteWidget) {
        if (voteWidget != null && voteWidget.getSongID() != null) {
            loginService.getCurrentlyAuthenticatedUserVote(voteWidget.getSongID(), new ExtendedRPCCallback<VOTE>(getEventBus(), voteWidget) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot vote, disable the widget in this case
                    if (caught instanceof AdministratorsCannotVoteException) {
                        voteWidget.setEnabled(false);
                        voteWidget.setMyVote(null);
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the rating to an undefined value
                    voteWidget.setMyVote(null);
                }

                @Override
                public void onResult(VOTE result) {
                    voteWidget.setMyVote(result);
                }
            });
        }
    }

    private void isMyRequest(final String trackId, final GenericPanelInterface requestBox, final GenericPanelInterface operationWidget, final ClickHandler addRequestClickHandler, final ClickHandler deleteRequestClickHandler) {
        if (trackId != null) {
            loginService.getCurrentlyAuthenticatedUserRequest(trackId, new ExtendedRPCCallback<RequestModule>(getEventBus(), requestBox) {

                @Override
                public boolean isFailureCatchable(Throwable caught) {
                    //Yep, admins cannot request, disable the widget in this case
                    if (caught instanceof AdministratorsCannotRequestException) {
                        requestBox.clean();
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    super.onFailure(caught);
                    //Something wrong happened: reset the request to an undefined value
                    requestBox.clean();
                }

                @Override
                public void onResult(RequestModule result) {
                    if (result == null) {
                        //none yet
                        requestBox.clean();
                        view.refreshAddNewRequestLink(operationWidget, addRequestClickHandler);
                    } else {
                        //one is already pending
                        view.refreshAddRequestInfo(result, requestBox);
                        if (result != null && !result.isQueued() && !result.isStreaming()) {
                            view.refreshAddDeleteRequestLink(operationWidget, deleteRequestClickHandler);
                        }
                    }
                }

            });
        }
    }

    protected void refreshPendingTrackList() {

        //Okay the list must have been initialized beforhand, otherwise it would likely mean that either another tab panel is selected or that the user is denied acces to it
        if (view.getPendingTrackList() != null) {

            final ClickHandler editAllClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    final Collection<PendingTrackForm> tracks = view.getPendingTrackList().getAllCheckedValues();
                    //Display a review editor
                    if (tracks != null && !tracks.isEmpty()) {
                        getEventBus().dispatch(RedirectionEventsEnum.SHOW_PENDINGTRACKBULKEDIT, tracks);
                    }
                }
            };
            final ClickHandler editClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    PendingTrackForm track = view.getPendingTrackList().getValueAt(view.getPendingTrackList().getCurrentRow(event));
                    //One cannot touch a track which is not yet ready
                    if (track.isReady()) {
                        redirectPageNoHistory(RedirectionEventsEnum.SHOW_PENDINGTRACKEDIT, track);
                    }
                }
            };
            final YesNoClickHandler deleteAllClickHandler = new YesNoClickHandler() {

                @Override
                public void onPreClick(ClickEvent event) {
                    //Do nothing
                }

                @Override
                public void onYesClick() {
                    deleteAllPendingTracks(view.getPendingTrackList());
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final YesNoClickHandler deleteClickHandler = new YesNoClickHandler() {

                int currentRow = -1;

                @Override
                public void onPreClick(ClickEvent event) {
                    currentRow = view.getPendingTrackList().getCurrentRow(event);
                }

                @Override
                public void onYesClick() {
                    deletePendingTrack(view.getPendingTrackList(), currentRow);
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final YesNoClickHandler validateAllClickHandler = new YesNoClickHandler() {

                @Override
                public void onPreClick(ClickEvent event) {
                    //Do nothing
                }

                @Override
                public void onYesClick() {
                    validateAllPendingTracks(view.getPendingTrackList());
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final YesNoClickHandler validateClickHandler = new YesNoClickHandler() {

                int currentRow = -1;

                @Override
                public void onPreClick(ClickEvent event) {
                    currentRow = view.getPendingTrackList().getCurrentRow(event);
                }

                @Override
                public void onYesClick() {
                    validatePendingTrack(view.getPendingTrackList(), currentRow);
                }

                @Override
                public void onNoClick() {
                    //Do nothing
                }
            };
            final ClickHandler rejectClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    PendingTrackForm track = view.getPendingTrackList().getValueAt(view.getPendingTrackList().getCurrentRow(event));
                    //One cannot touch a track which is not yet ready
                    if (track.isReady()) //Display a review editor
                    {
                        getEventBus().dispatch(InterWindowEventsEnum.SHOW_REVIEW_EDITOR, track);
                    }
                }
            };
            final ClickHandler rejectAllClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    final Collection<PendingTrackForm> tracks = view.getPendingTrackList().getAllCheckedValues();
                    //Display a review editor
                    if (tracks != null && !tracks.isEmpty()) {
                        getEventBus().dispatch(InterWindowEventsEnum.SHOW_BATCH_REVIEW_EDITOR, tracks);
                    }
                }
            };

            final ClickHandler trackDisplayClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    PendingTrackForm track = view.getPendingTrackList().getValueAt(view.getPendingTrackList().getCurrentRow(event));
                    //One CAN view a track which is not yet ready
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_PENDINGTRACKDISPLAY, track);
                }
            };
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler = new SerializableItemSelectionHandler<String>() {

                @Override
                public void onItemSelected(String currentValue) {
                    if (currentValue != null) {
                        programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {
                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                    }
                }
            };

            trackService.getSecuredPendingTracks(view.getPendingTrackList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<? extends PendingTrackForm>>(getEventBus(), view.getPendingTrackList()) {

                /*@Override
                 public void onInit() {
                 view.getPendingTrackList().clean();
                 }*/
                @Override
                public void onResult(ActionCollection<? extends PendingTrackForm> result) {
                    _refreshPendingTrackList(
                            result,
                            editClickHandler,
                            editAllClickHandler,
                            deleteClickHandler,
                            validateClickHandler,
                            rejectClickHandler,
                            deleteAllClickHandler,
                            validateAllClickHandler,
                            rejectAllClickHandler,
                            trackDisplayClickHandler,
                            programmeDisplaySelectionHandler,
                            eventBus);
                }
            });
        }
    }

    private void _refreshPendingTrackList(
            final ActionCollection<? extends PendingTrackForm> tracks,
            final ClickHandler editClickHandler,
            final ClickHandler editAllClickHandler,
            final YesNoClickHandler deleteClickHandler,
            final YesNoClickHandler validateClickHandler,
            final ClickHandler rejectClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            final YesNoClickHandler validateAllClickHandler,
            final ClickHandler rejectAllClickHandler,
            final ClickHandler trackDisplayClickHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler,
            EventBusWithLookup eventBus) {
        helperService.getJSESSIONID(new ExtendedRPCCallback<String>(getEventBus(), view.getBroadcastTrackList()) {

            @Override
            public void onResult(String result) {
                view.refreshPendingTrackList(
                        tracks,
                        editClickHandler,
                        editAllClickHandler,
                        deleteClickHandler,
                        validateClickHandler,
                        rejectClickHandler,
                        deleteAllClickHandler,
                        validateAllClickHandler,
                        rejectAllClickHandler,
                        trackDisplayClickHandler,
                        programmeDisplaySelectionHandler,
                        eventBus,
                        result);
            }
        });

    }

    public void onRejectPendingTrack(Review review) {
        if (review != null && review.getForm() != null) {
            final String newTrackName = review.getForm().getFriendlyID();

            //Make it persistant
            trackService.rejectSecuredPendingTrack(
                    review.getForm().getRefID(),
                    review.getComment(),
                    new ExtendedRPCCallback<Void>(getEventBus(), view.getPendingTrackList()) {

                        @Override
                        public void onResult(Void result) {
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().track_rejected(newTrackName));
                            //And refresh the back page
                            //UPDATE: now directly handled by reverse AJAX
                            //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PENDINGTRACKS);
                        }
                    });
        }
    }

    public void onRejectPendingTracks(final BatchReview batchReview) {
        if (batchReview != null && batchReview.getForms() != null && !batchReview.getForms().isEmpty()) {
            Collection<String> trackIds = new HashSet<String>();
            for (PendingTrackForm track : batchReview.getForms()) {
                trackIds.add(track.getRefID());
            }
            //Make it persistant
            trackService.rejectSecuredPendingTracks(
                    trackIds,
                    batchReview.getComment(),
                    new ExtendedRPCCallback<Void>(getEventBus(), view.getPendingTrackList()) {

                        @Override
                        public void onResult(Void result) {
                            String[] ids = new String[batchReview.getForms().size()];
                            int i = 0;
                            for (TrackForm track : batchReview.getForms()) {
                                ids[i] = track.getFriendlyID();
                                i++;
                            }
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().tracks_rejected(I18nTranslator.toCSV(ids)));
                            //And refresh the back page
                            //UPDATE: now directly handled by reverse AJAX
                            //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PENDINGTRACKS);
                        }
                    });
        }
    }

    public void onRequestBroadcastSong(Request request) {
        if (request != null && request.getForm() != null && request.getChannel() != null) {
            final String requestedTrackName = request.getForm().getFriendlyID();

            //Make it persistant
            loginService.setCurrentlyAuthenticatedUserRequest(
                    request.getForm().getRefID(),
                    request.getChannel().getLabel(),
                    request.getMessage(),
                    new ExtendedRPCCallback<Void>(getEventBus(), view.getBroadcastPanel()) {

                        @Override
                        public void onResult(Void result) {
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().request_registered(requestedTrackName));
                            //And refresh the back page
                            //UPDATE: now directly handled by reverse AJAX
                            //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                        }
                    });
        }
    }

    private void deleteMyRequest(final GenericFormTableInterface<BroadcastTrackForm, ?> list, final int currentRow) {
        final BroadcastTrackForm track = list.getValueAt(currentRow);
        //One cannot touch a track which is not yet ready
        if (track.isReady()) {
            //We shouldn't care about multiple entries for a single track, the database schema prevents it
            loginService.resetCurrentlyAuthenticatedUserRequest(track.getRefID(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().request_unregistered(track.getFriendlyID()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    private void deleteBroadcastTrack(final GenericFormTableInterface<BroadcastTrackForm, ?> list, final int currentRow) {
        final BroadcastTrackForm track = list.getValueAt(currentRow);
        //One cannot touch a track which is not yet ready
        if (track.isReady()) {
            trackService.deleteSecuredBroadcastTrack(track.getRefID(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().track_deleted(track.getFriendlyID()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    private void deletePendingTrack(final GenericFormTableInterface<PendingTrackForm, ?> list, final int currentRow) {
        final PendingTrackForm track = list.getValueAt(currentRow);
        //One cannot touch a track which is not yet ready
        if (track.isReady()) {
            trackService.deleteSecuredPendingTrack(track.getRefID(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().track_deleted(track.getFriendlyID()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PENDINGTRACKS);
                }
            });
        }
    }

    public void validatePendingTrack(final GenericFormTableInterface<PendingTrackForm, ?> list, final int currentRow) {
        final PendingTrackForm track = list.getValueAt(currentRow);
        //Duplicated tracks for programmes must be checked first, just like creating a new Broadcastable Track
        //Do only output the Programmes in track.getProgrammes() that are not concurrently present in getForm().getProgrammes()
        if (track != null//One cannot touch a track which is not yet ready
                && track.isReady()) {
            if (track.getTrackProgrammes() != null && !track.getTrackProgrammes().isEmpty()) {
                dealWithDuplicatesThenEditAndValidate(list, track);
            }
            //Otherwise do nothing, one can only validate a track if at least a programme is attached to it
        }
    }

    private void dealWithDuplicatesThenEditAndValidate(final GenericFormTableInterface<PendingTrackForm, ?> list, final PendingTrackForm track) {

        //Check if a similar track is not already bound to the same programmes, and in this case ask the user what to do
        ExtendedRPCCallback callback
                = new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), list) {

                    @Override
                    public void onResult(final ActionCollection<ProgrammeForm> result) {

                        //Make up the list of programme names already bound
                        final Collection<String> alreadyBoundProgrammeNames = new HashSet<String>();
                        if (result != null) {
                            for (ActionCollectionEntry<ProgrammeForm> entry : result) {
                                if (entry.isReadable()) {
                                    alreadyBoundProgrammeNames.add(entry.getItem().getLabel());
                                }
                            }
                        }

                        if (alreadyBoundProgrammeNames.isEmpty()) {
                            //No doublon in sight, add the track right now, doublons may appear thereafter in case there's a concurrent call though
                            validate(list, track);
                        } else {

                            //There are programmes already bound to a similar track, ask the user what to do
                            GWTCustomizableConfirmationDialogBox confirmationBox = new GWTCustomizableConfirmationDialogBox();
                            confirmationBox.addMessage(
                                    I18nTranslator.getInstance().duplicated_track_detected(track.getAuthor(), track.getTitle()));
                            confirmationBox.addMessage(
                                    I18nTranslator.toCSV(alreadyBoundProgrammeNames));
                            confirmationBox.addMessage(
                                    I18nTranslator.getInstance().do_you_want_to_allow_track_duplicates());
                            confirmationBox.addButton(I18nTranslator.getInstance().yes(), new ClickHandler() {

                                @Override
                                public void onClick(ClickEvent event) {
                                    //Update duplicates
                                    validate(list, track);
                                }
                            });
                            //Same as "Cancel"
                            confirmationBox.addButton(I18nTranslator.getInstance().no(), null);

                            confirmationBox.show();

                        }
                    }
                };

        switch (track.getType()) {
            case song:
                programmeService.getSecuredProgrammesForSongAndProgrammeIds(
                        track.getAuthor(),
                        track.getTitle(),
                        track.getImmutableProgrammes(),
                        callback);
                break;
            case advert:
                programmeService.getSecuredProgrammesForAdvertAndProgrammeIds(
                        track.getAuthor(),
                        track.getTitle(),
                        track.getImmutableProgrammes(),
                        callback);
                break;
            case jingle:
                programmeService.getSecuredProgrammesForJingleAndProgrammeIds(
                        track.getAuthor(),
                        track.getTitle(),
                        track.getImmutableProgrammes(),
                        callback);
                break;
        }

    }

    private void validate(final GenericFormTableInterface<PendingTrackForm, ?> list, final PendingTrackForm track) {
        final String newTrackName = track.getFriendlyID();
        //Make it persistant
        //track files and cover art will not be updated
        trackService.validateSecuredPendingTrack(
                track.getRefID(),
                new ExtendedRPCCallback<Void>(getEventBus(), list) {

                    @Override
                    public void onResult(Void result) {
                        //Show notification to user
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().track_validated(newTrackName));
                        //Refresh the corresponding list
                        //UPDATE: now directly handled by reverse AJAX
                        //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PENDINGTRACKS);
                        //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                    }
                });
    }

    private void deleteAllBroadcastTracks(final GenericFormTableInterface<BroadcastTrackForm, ?> list) {
        final Collection<BroadcastTrackForm> tracks = list.getAllCheckedValues();
        if (tracks != null && !tracks.isEmpty()) {
            Collection<String> ids = new HashSet<String>();
            for (BroadcastTrackForm form : tracks) {
                ids.add(form.getRefID());
            }
            trackService.deleteSecuredBroadcastTracks(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[tracks.size()];
                    int i = 0;
                    for (TrackForm track : tracks) {
                        ids[i] = track.getFriendlyID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().tracks_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                }
            });
        }
    }

    private void deleteAllPendingTracks(final GenericFormTableInterface<PendingTrackForm, ?> list) {
        final Collection<PendingTrackForm> tracks = list.getAllCheckedValues();
        if (tracks != null && !tracks.isEmpty()) {
            Collection<String> ids = new HashSet<String>();
            for (PendingTrackForm form : tracks) {
                ids.add(form.getRefID());
            }
            trackService.deleteSecuredBroadcastTracks(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[tracks.size()];
                    int i = 0;
                    for (TrackForm track : tracks) {
                        ids[i] = track.getFriendlyID();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().tracks_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PENDINGTRACKS);
                }
            });
        }
    }

    private void validateAllPendingTracks(final GenericFormTableInterface<PendingTrackForm, ?> list) {
        final Collection<PendingTrackForm> tracks = list.getAllCheckedValues();

        //Duplicated tracks for programmes must be checked first, just like creating a new Broadcastable Track
        //Do only output the Programmes in track.getProgrammes() that are not concurrently present in getForm().getProgrammes()
        boolean yesToAll = false;
        boolean noToAll = false;
        Collection<PendingTrackForm> newTracksToValidate = new HashSet<PendingTrackForm>();

        if (tracks != null && !tracks.isEmpty()) {
            Iterator<PendingTrackForm> iterator = tracks.iterator();
            _recursiveDealWithDuplicatesThenEditAndValidate(list,
                    iterator,
                    newTracksToValidate,
                    yesToAll,
                    noToAll);
        }
    }

    private void _recursiveDealWithDuplicatesThenEditAndValidate(final GenericFormTableInterface<PendingTrackForm, ?> list,
            final Iterator<PendingTrackForm> iterator,
            final Collection<PendingTrackForm> tracksToValidate,
            final boolean yesToAll,
            final boolean noToAll) {

        if (iterator.hasNext()) {
            final PendingTrackForm currentPendingTrackForm = iterator.next();

            //Check if a similar track is not already bound to the same programmes, and in this case ask the user what to do
            ExtendedRPCCallback callback
                    = new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), list) {

                        @Override
                        public void onResult(final ActionCollection<ProgrammeForm> result) {

                            //Make up the list of programme names already bound
                            final Collection<String> alreadyBoundProgrammeNames = new HashSet<String>();
                            if (result != null) {
                                for (ActionCollectionEntry<ProgrammeForm> entry : result) {
                                    if (entry.isReadable()) {
                                        alreadyBoundProgrammeNames.add(entry.getItem().getLabel());
                                    }
                                }
                            }

                            if (yesToAll || alreadyBoundProgrammeNames.isEmpty()) {
                                //No doublon in sight, add the track right now, doublons may appear thereafter in case there's a concurrent call though
                                tracksToValidate.add(currentPendingTrackForm);
                                _recursiveDealWithDuplicatesThenEditAndValidate(list,
                                                    iterator,
                                                    tracksToValidate,
                                                    yesToAll,
                                                    noToAll);
                            } else {

                                if (noToAll) {
                                    //Bypass
                                    _recursiveDealWithDuplicatesThenEditAndValidate(list,
                                            iterator,
                                            tracksToValidate,
                                            yesToAll,
                                            noToAll);
                                } else {

                                    //There are programmes already bound to a similar track, ask the user what to do
                                    GWTCustomizableConfirmationDialogBox confirmationBox = new GWTCustomizableConfirmationDialogBox();
                                    confirmationBox.addMessage(
                                            I18nTranslator.getInstance().duplicated_track_detected(currentPendingTrackForm.getAuthor(), currentPendingTrackForm.getTitle()));
                                    confirmationBox.addMessage(
                                            I18nTranslator.toCSV(alreadyBoundProgrammeNames));
                                    confirmationBox.addMessage(
                                            I18nTranslator.getInstance().do_you_want_to_allow_track_duplicates());
                                    confirmationBox.addButton(I18nTranslator.getInstance().yes(), new ClickHandler() {

                                        @Override
                                        public void onClick(ClickEvent event) {
                                            //Update duplicates
                                            tracksToValidate.add(currentPendingTrackForm);
                                            _recursiveDealWithDuplicatesThenEditAndValidate(list,
                                                    iterator,
                                                    tracksToValidate,
                                                    false,
                                                    false);
                                        }
                                    });
                                    confirmationBox.addButton(I18nTranslator.getInstance().yes_to_all(), new ClickHandler() {

                                        @Override
                                        public void onClick(ClickEvent event) {
                                            //Update duplicates
                                            tracksToValidate.add(currentPendingTrackForm);
                                            _recursiveDealWithDuplicatesThenEditAndValidate(list,
                                                    iterator,
                                                    tracksToValidate,
                                                    true,
                                                    false);
                                        }
                                    });
                                    confirmationBox.addButton(I18nTranslator.getInstance().no(), new ClickHandler() {

                                        @Override
                                        public void onClick(ClickEvent event) {
                                            //Bypass
                                            _recursiveDealWithDuplicatesThenEditAndValidate(list,
                                                    iterator,
                                                    tracksToValidate,
                                                    false,
                                                    false);
                                        }
                                    });
                                    confirmationBox.addButton(I18nTranslator.getInstance().no_to_all(), new ClickHandler() {

                                        @Override
                                        public void onClick(ClickEvent event) {
                                            //Bypass
                                            _recursiveDealWithDuplicatesThenEditAndValidate(list,
                                                    iterator,
                                                    tracksToValidate,
                                                    false,
                                                    true);
                                        }
                                    });

                                    confirmationBox.show();

                                }
                            }
                        }
                    };

            switch (currentPendingTrackForm.getType()) {
                case song:
                    programmeService.getSecuredProgrammesForSongAndProgrammeIds(
                            currentPendingTrackForm.getAuthor(),
                            currentPendingTrackForm.getTitle(),
                            currentPendingTrackForm.getImmutableProgrammes(),
                            callback);
                    break;
                case advert:
                    programmeService.getSecuredProgrammesForAdvertAndProgrammeIds(
                            currentPendingTrackForm.getAuthor(),
                            currentPendingTrackForm.getTitle(),
                            currentPendingTrackForm.getImmutableProgrammes(),
                            callback);
                    break;
                case jingle:
                    programmeService.getSecuredProgrammesForJingleAndProgrammeIds(
                            currentPendingTrackForm.getAuthor(),
                            currentPendingTrackForm.getTitle(),
                            currentPendingTrackForm.getImmutableProgrammes(),
                            callback);
                    break;
            }

        } else {
            //Evrything has been processed, now validate all the tracks at once
            validate(list, tracksToValidate);
        }

    }

    private void validate(final GenericFormTableInterface<PendingTrackForm, ?> list, final Collection<PendingTrackForm> tracks) {
        //Make it persistant
        //track files and cover art will not be updated
        if (tracks != null && !tracks.isEmpty()) {
            Collection<String> pendingTrackIds = new HashSet<String>();
            for (PendingTrackForm track : tracks) {
                pendingTrackIds.add(track.getRefID());
            }

            trackService.validateSecuredPendingTracks(
                    pendingTrackIds,
                    new ExtendedRPCCallback<Void>(getEventBus(), list) {

                        @Override
                        public void onResult(Void result) {
                            String[] ids = new String[tracks.size()];
                            int i = 0;
                            for (TrackForm track : tracks) {
                                ids[i] = track.getFriendlyID();
                                i++;
                            }
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().tracks_validated(I18nTranslator.toCSV(ids)));
                            //Refresh the corresponding list
                            //UPDATE: now directly handled by reverse AJAX
                            //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_PENDINGTRACKS);
                            //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                        }
                    });
        }
    }

    /**
     * Check whether the id given by the GUI event signal corresponds to a
     * channel in the view list *
     */
    private boolean isInPendingTrackList(String id) {
        if (view.getPendingTrackList() != null) {
            for (PendingTrackForm trackForm : view.getPendingTrackList().getAllValues()) {
                if (trackForm.getRefID().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check whether the id given by the GUI event signal corresponds to a
     * channel in the view list *
     */
    private boolean isInBroadcastTrackList(String id) {
        if (view.getBroadcastTrackList() != null) {
            for (BroadcastTrackForm trackForm : view.getBroadcastTrackList().getAllValues()) {
                if (trackForm.getRefID().equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case broadcasttrack:
                        if (flag == FeedbackObjectForm.FLAG.add) {
                            add(MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                        } else {
                            if (isInBroadcastTrackList(objectId)) {
                                add(MessageEventsEnum.REFRESH_BROADCASTTRACKS);
                            }
                        }
                        break;
                    case pendingtrack:
                        if (flag == FeedbackObjectForm.FLAG.add) {
                            add(MessageEventsEnum.REFRESH_PENDINGTRACKS);
                        } else {
                            if (isInPendingTrackList(objectId)) {
                                add(MessageEventsEnum.REFRESH_PENDINGTRACKS);
                            }
                        }
                        break;
                }
            }
        }.get();
    }

    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_PROGRAMMES:
                refreshPendingTrackList();
                refreshBroadcastTrackList();
                break;
            case REFRESH_PENDINGTRACKS:
                refreshPendingTrackList();
                break;
            case REFRESH_BROADCASTTRACKS:
                refreshBroadcastTrackList();
                break;
        }
    }
}
