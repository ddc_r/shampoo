/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.user.AdministratorFormWithPassword;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.user.AdminAddView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class AdminAddPresenter extends PopupPagePresenter<AdminAddView> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private UserRPCServiceInterfaceAsync userService;
    private HelperRPCServiceInterfaceAsync helperService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessAddAdministrators(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Form validation handlers
        view.getConfirmPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkConfirmationPassword(view.getPasswordTextBox().getText(), view.getConfirmPasswordTextBox().getText()), view.getConfirmPasswordTextBox());
            }
        });
        view.getEmailTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox());
            }
        });
        view.getPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getPasswordTextBox().getText()), view.getPasswordTextBox());
            }
        });
        view.getUsernameTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkUsername(view.getUsernameTextBox().getText()), view.getUsernameTextBox());
            }
        });

        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox());
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (Validation.bindValidationError(Validation.checkUsername(view.getUsernameTextBox().getText()), view.getUsernameTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkPassword(view.getPasswordTextBox().getText()), view.getPasswordTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkConfirmationPassword(view.getPasswordTextBox().getText(), view.getConfirmPasswordTextBox().getText()), view.getConfirmPasswordTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getFirstAndLastNameTextBox().getText()), view.getFirstAndLastNameTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new User from the view
        final AdministratorFormWithPassword newUser = new AdministratorFormWithPassword();
        newUser.setEmail(view.getEmailTextBox().getText());
        newUser.setEnabled(view.getEnabledCheckBox().isChecked());
        newUser.setFirstAndLastName(view.getFirstAndLastNameTextBox().getText());
        newUser.setPassword(view.getPasswordTextBox().getText());
        newUser.setUsername(view.getUsernameTextBox().getText());
        newUser.setTimezone(view.getTimezoneComboBox().getSelectedValue());

        //Make it persistant
        userService.addSecuredUser(newUser, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user and make sure it will be visible during the page update
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().user_added(newUser.getUsername()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_USERS);
            }
        });
    }

    @Override
    public void onRefresh() {
        //Reset everything
        view.getEnabledCheckBox().clean();
        //By default, every user to create should be enabled
        view.getEnabledCheckBox().setChecked(true);
        view.getUsernameTextBox().clean();
        view.getEmailTextBox().clean();
        view.getPasswordTextBox().clean();
        view.getConfirmPasswordTextBox().clean();
        view.getFirstAndLastNameTextBox().clean();
        view.getSubmit().clean();
        //Fill in timezones
        view.getTimezoneComboBox().clean();
        feedTimezoneList();
    }

    private void feedTimezoneList() {
        //refresh the available time zones list
        helperService.getTimezones(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(Collection<String> result) {
                view.refreshTimezoneList(result);
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });

    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                    case channel:
                        //If a channel has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().channels_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
    
}
