/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.view.views.user.AdminDisplayView;

/**
 *
 * @author okay_awright
 **/
public class AdminDisplayPresenter extends GenericFormPopupPagePresenter<AdminDisplayView, AdministratorForm> {

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the user to view
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewAdministrator(getForm().getUsername(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {
        //Do noting
    }

    @Override
    public void onFormSpecificRefresh() {
        //Reset everything
        view.getEnabledLabel().setCaption(getForm().isEnabled()?I18nTranslator.getInstance().is_true():I18nTranslator.getInstance().is_false());
        view.getUsernameLabel().setCaption(getForm().getUsername());
        view.getEmailLabel().setCaption(getForm().getEmail());
        view.getFirstAndLastNameLabel().setCaption(getForm().getFirstAndLastName());
        //Fill in timezones
        view.getTimezoneLabel().setCaption(getForm().getTimezone());
    }

}
