/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.form.user.ProfileDetailForm;
import biz.ddcr.shampoo.client.form.user.ProfilePasswordForm;
import biz.ddcr.shampoo.client.form.user.ProfileRightForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.user.ChangeMyProfileView;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class ChangeMyProfilePresenter extends PopupPagePresenter<ChangeMyProfileView> {

    private LoginRPCServiceInterfaceAsync loginService;
    private HelperRPCServiceInterfaceAsync helperService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        //Every authenticated user can change his profile
        //No need to call on the authorization service
                if (UserIn.authenticatedUserName != null) //user exists
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Form validation handlers
        view.getConfirmNewPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkConfirmationPassword(view.getNewPasswordTextBox().getText(), view.getConfirmNewPasswordTextBox().getText()), view.getConfirmNewPasswordTextBox());
            }
        });
        view.getEmailTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox());
            }
        });
        view.getOldPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getOldPasswordTextBox().getText()), view.getOldPasswordTextBox());
            }
        });
        view.getNewPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getNewPasswordTextBox().getText()), view.getNewPasswordTextBox());
            }
        });
        view.getChangePasswordCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.togglePasswordChange();
            }
        });

        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox());
            }
        });

        //Automatic feed for domains from roles
        view.getEditableRoleRightComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                feedEditableDomainsfromRoleList(view.getEditableRoleRightComboBox().getSelectedValue());
            }
        });

        //Add new right
        view.getAddNewRightButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewRight();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (view.getChangePasswordCheckBox().isChecked()) {
            if (Validation.bindValidationError(Validation.checkPassword(view.getOldPasswordTextBox().getText()), view.getOldPasswordTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkPassword(view.getNewPasswordTextBox().getText()), view.getNewPasswordTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkConfirmationPassword(view.getNewPasswordTextBox().getText(), view.getConfirmNewPasswordTextBox().getText()), view.getConfirmNewPasswordTextBox())) {
                result = false;
            }
        }
        if (Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getFirstAndLastNameTextBox().getText()), view.getFirstAndLastNameTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new Profile from the view
        final ProfileDetailForm newProfile = new ProfileDetailForm();
        newProfile.setEmail(view.getEmailTextBox().getText());
        newProfile.setFirstAndLastName(view.getFirstAndLastNameTextBox().getText());
        newProfile.setTimezone(view.getTimezoneComboBox().getSelectedValue());
        newProfile.setEmailNotification(view.getEmailNotificationCheckBox().isChecked());

        //Fill it with rights
        Collection<String> rs = new HashSet<String>();
        for (RightForm r : view.getEditableRightsListBox().getAllValues()) {
            rs.add(r.getDomainId());
        }

        if (view.getChangePasswordCheckBox().isChecked()) {
            final ProfilePasswordForm newProfilePassword = new ProfilePasswordForm();
            newProfilePassword.setNewPassword(view.getNewPasswordTextBox().getText());
            newProfilePassword.setOldPassword(view.getOldPasswordTextBox().getText());
            loginService.updateCurrentlyAuthenticatedUserAndListenerRights(newProfilePassword, newProfile, rs, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                @Override
                public void onResult(Void result) {
                    _formSubmitted();
                }
            });
        } else {
            loginService.updateCurrentlyAuthenticatedUserAndListenerRights(newProfile, rs, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                @Override
                public void onResult(Void result) {
                    _formSubmitted();
                }
            });
        }
    }

    private void _formSubmitted() {
        //Then close popup
        closePopup();
        //Show notification to user
        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().user_updated(UserIn.authenticatedUserName));
        //Refresh bits of UI if appropriate
        //UDPATE: now directly handled by reverse AJAX
        //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_MYPROFILE);
        //Stop here, nowhere to explicitly redirect
    }

    @Override
    public void onRefresh() {

        //Hide right list until we sure know the user is not an admin
        view.showRightList(false);
        view.getAccountTypeLabel().clean();
        view.getEditableRightsListBox().clean();
        feedProfile();
        view.getUsernameLabel().clean();
        view.getUsernameLabel().setCaption(UserIn.authenticatedUserName);
        view.getChangePasswordCheckBox().clean();
        //Do not change password by default
        view.getChangePasswordCheckBox().setChecked(false);
        view.togglePasswordChange();
        view.getOldPasswordTextBox().clean();
        view.getNewPasswordTextBox().clean();
        view.getConfirmNewPasswordTextBox().clean();
        view.togglePasswordChange();
        view.getSubmit().clean();
    }

    private void feedEditableRightsList(final ProfileRightForm profileRightForm) {
        final ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getEditableRightsListBox().removeRow(view.getEditableRightsListBox().getCurrentRow(event));
            }
        };

        channelService.getUnsecuredOpenChannelLabels(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getEditableDomainRightComboBox()) {

            @Override
            public void onResult(Collection<String> result) {
                //Clear the currently bound user so as the isIn() method for checking if the same right cannot be added twice works
                ActionCollection<RightForm> newRights = new ActionSortedMap<RightForm>();
                for (String listenerRight : profileRightForm.getListenerRights()) {
                    newRights.addNude(new ChannelRightForm(null, listenerRight, ChannelRole.listener));
                }

                view.refreshEditableRightList(newRights, deleteClickHandler);
            }
        });
    }

    private void feedEditableRolesList() {
        //refresh the available roles list
        //Do only provide Listener
        HashSet<Role> roles = new HashSet<Role>();
        roles.add(ChannelRole.listener);
        view.refreshEditableRoleRightList(roles);
        //Then update the corresponding dependent enetiyt list
        feedEditableDomainsfromRoleList(view.getEditableRoleRightComboBox().getSelectedValue());
    }

    private void feedEditableDomainsfromRoleList(Role selectedValue) {
        if (selectedValue != null) {
            //See if the selectValue is specific to channels or programmes
            if (selectedValue.getClass() == ChannelRole.class) {
                channelService.getUnsecuredOpenChannelLabels(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getEditableDomainRightComboBox()) {

                    @Override
                    public void onResult(Collection<String> result) {
                        //refresh the available channel list
                        view.refreshEditableDomainRightList(result);
                    }

                    /*@Override
                    public void onInit() {
                        view.getEditableDomainRightComboBox().clean();
                    }*/
                });
            }

        }
    }

    private void feedProfile() {
        loginService.getCurrentlyAuthenticatedUserProfileDetails(new ExtendedRPCCallback<ProfileDetailForm>(getEventBus(), view.getUserPanel()) {

            @Override
            public void onResult(ProfileDetailForm result) {
                view.getEmailTextBox().clean();
                view.getFirstAndLastNameTextBox().clean();
                view.getTimezoneComboBox().clean();
                view.getEmailNotificationCheckBox().clean();
                if (result != null) {
                    view.getFirstAndLastNameTextBox().setText(result.getFirstAndLastName());
                    view.getEmailTextBox().setText(result.getEmail());
                    view.getEmailNotificationCheckBox().setChecked(result.isEmailNotification());
                    feedAccountType();
                    feedTimeZones(result.getTimezone());
                }
            }

            @Override
            public void onError() {
                feedTimeZones(null);
            }

            /*@Override
            public void onInit() {
                view.getEmailTextBox().clean();
                view.getFirstAndLastNameTextBox().clean();
                view.getTimezoneComboBox().clean();
                view.getEmailNotificationCheckBox().clean();
            }*/
        });
    }

    private void feedTimeZones(final String defaultTimeZone) {
        helperService.getTimezones(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(Collection<String> result) {
                view.refreshTimezoneList(result);
                if (defaultTimeZone != null) {
                    view.getTimezoneComboBox().setSelectedValue(defaultTimeZone);
                }
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });
    }

    private void feedAccountType() {
        loginService.isCurrentlyAuthenticatedUserAnAdministrator(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getAccountTypeLabel()) {

            @Override
            public void onResult(Boolean result) {
                view.getAccountTypeLabel().clean();
                if (result) {
                    view.getAccountTypeLabel().setCaption(I18nTranslator.getInstance().admin());
                    //Administrators get not email notifications
                    view.getEmailNotificationCheckBox().setChecked(false);
                    view.getEmailNotificationCheckBox().setEnabled(false);
                    view.getRightPanel().setVisible(false);
                    view.getEditableRightPanel().setVisible(false);
                } else {
                    view.getAccountTypeLabel().setCaption(I18nTranslator.getInstance().restricted_user());
                    view.getEmailNotificationCheckBox().setEnabled(true);
                    view.getRightPanel().setVisible(true);
                    view.getEditableRightPanel().setVisible(true);
                    feedEditableRolesList();
                    feedRightsList();
                }
            }

            /*@Override
            public void onInit() {
                view.getAccountTypeLabel().clean();
            }*/
        });
    }

    private void feedRightsList() {
        view.showRightList(true);
        final SerializableItemSelectionHandler<String> displayChannelSelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue != null) {
                    channelService.getSecuredChannel(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null) {

                        @Override
                        public void onResult(ActionCollectionEntry<ChannelForm> result) {
                            //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                            redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                        }
                    });

                }
            }
        };

        final SerializableItemSelectionHandler<String> displayProgrammeSelectionHandler = new SerializableItemSelectionHandler<String>() {

            @Override
            public void onItemSelected(String currentValue) {
                if (currentValue != null) {
                    programmeService.getSecuredProgramme(currentValue, new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                        @Override
                        public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                            //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                            redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                        }
                    });

                }
            }
        };

        loginService.getCurrentlyAuthenticatedRestrictedUserRights(new ExtendedRPCCallback<ProfileRightForm>(getEventBus(), view.getRightPanel()) {

            @Override
            public void onResult(ProfileRightForm result) {
                view.refreshReadOnlyRightList(result, displayProgrammeSelectionHandler, displayChannelSelectionHandler);
                feedEditableRightsList(result);
            }

        });
    }

    private boolean addNewRight() {

        Role role = view.getEditableRoleRightComboBox().getSelectedValue();
        String domain = view.getEditableDomainRightComboBox().getSelectedValue();

        //Check if the role and either the channel or programme parts of the rights are properly filled in
        if (role == null || domain == null) {
            return false;
        }

        RightForm newRight = null;
        if (role.getClass() == ChannelRole.class) {
            newRight = new ChannelRightForm(null, domain, (ChannelRole) role);
        } else {
            return false;
        }

        //Is it already present in the list?
        if (view.getEditableRightsListBox().isValueIn(newRight)) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getEditableRightsListBox().removeRow(view.getEditableRightsListBox().getCurrentRow(event));
            }
        };
        view.addNewRight(newRight, deleteClickHandler);

        return true;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case user:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag==FeedbackObjectForm.FLAG.edit && (objectId==null || objectId.equals(UserIn.authenticatedUserName)))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                    case channel:
                        //If a channel has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().channels_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
