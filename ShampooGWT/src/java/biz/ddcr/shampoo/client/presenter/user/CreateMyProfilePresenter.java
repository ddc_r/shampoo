/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.form.user.ProfileDetailForm;
import biz.ddcr.shampoo.client.form.user.ProfilePasswordForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.user.CreateMyProfileView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class CreateMyProfilePresenter extends PopupPagePresenter<CreateMyProfileView> {

    private ChannelRPCServiceInterfaceAsync channelService;
    private LoginRPCServiceInterfaceAsync loginService;
    private HelperRPCServiceInterfaceAsync helperService;
    //Bind open channels for registration to this local Javascript variable
    private Collection<String> availableChannelLabels = null;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    @Override
    protected void onCheckAccess() {
        //Every non-authenticated user can check which channels are open for registration
        //No need to call on the authorization service
        channelService.getUnsecuredOpenChannelLabels(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getDomainRightComboBox()) {

            @Override
            public void onResult(Collection<String> result) {
                if (result != null && !result.isEmpty()) //open channels exist
                {
                    //Update the local Javascript variable
                    availableChannelLabels = result;
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Form validation handlers        
        view.getEmailTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox());
            }
        });
        view.getConfirmEmailTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkConfirmationPassword(view.getEmailTextBox().getText(), view.getConfirmEmailTextBox().getText()), view.getConfirmEmailTextBox());
            }
        });
        view.getPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getPasswordTextBox().getText()), view.getPasswordTextBox());
            }
        });
        view.getConfirmPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkConfirmationPassword(view.getPasswordTextBox().getText(), view.getConfirmPasswordTextBox().getText()), view.getConfirmPasswordTextBox());
            }
        });
        view.getUsernameTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkUsername(view.getUsernameTextBox().getText()), view.getUsernameTextBox());
            }
        });
        //Automatic feed for domains from roles
        view.getRoleRightComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                feedDomainsfromRoleList(view.getRoleRightComboBox().getSelectedValue());
            }
        });

        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox());
            }
        });

        //Add new right
        view.getAddNewRightButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewRight();
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (Validation.bindValidationError(Validation.checkUsername(view.getUsernameTextBox().getText()), view.getUsernameTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkConfirmationEmail(view.getEmailTextBox().getText(), view.getConfirmEmailTextBox().getText()), view.getConfirmEmailTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkPassword(view.getPasswordTextBox().getText()), view.getPasswordTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkConfirmationPassword(view.getPasswordTextBox().getText(), view.getConfirmPasswordTextBox().getText()), view.getConfirmPasswordTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getFirstAndLastNameTextBox().getText()), view.getFirstAndLastNameTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkCollectionIsSet(view.getRightsListBox().getRowCount()), view.getRightsListBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        final ProfileDetailForm newProfile = new ProfileDetailForm();
        newProfile.setEmail(view.getEmailTextBox().getText());
        newProfile.setFirstAndLastName(view.getFirstAndLastNameTextBox().getText());
        newProfile.setTimezone(view.getTimezoneComboBox().getSelectedValue());
        newProfile.setEmailNotification(view.getEmailNotificationCheckBox().isChecked());
        final ProfilePasswordForm newProfilePassword = new ProfilePasswordForm();
        newProfilePassword.setNewPassword(view.getPasswordTextBox().getText());

        //Fill it with rights
        Collection<String> rs = new HashSet<String>();
        for (RightForm r : view.getRightsListBox().getAllValues()) {
            rs.add(r.getDomainId());
        }

        loginService.addSelfAuthenticatedUser(
                view.getUsernameTextBox().getText(),//
                newProfilePassword,//
                newProfile,//
                rs,
                new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                    @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().user_added(view.getUsernameTextBox().getText()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_USERS);
            }
        });

    }

    @Override
    public void onRefresh() {
        //Reset everything
        view.getUsernameTextBox().clean();
        view.getEmailTextBox().clean();
        view.getConfirmEmailTextBox().clean();
        view.getEmailNotificationCheckBox().clean();
        view.getPasswordTextBox().clean();
        view.getConfirmPasswordTextBox().clean();
        view.getFirstAndLastNameTextBox().clean();
        view.getSubmit().clean();
        //clear right list
        view.getDomainRightComboBox().clean();
        view.getRoleRightComboBox().clean();
        view.getRightsListBox().clean();
        //Fill in with available roles
        feedRolesList();
        //Fill in timezones
        view.getTimezoneComboBox().clean();
        feedTimezoneList();
    }

    private void feedRolesList() {
        //refresh the available roles list
        //Do only provide Listener
        HashSet<Role> roles = new HashSet<Role>();
        roles.add(ChannelRole.listener);
        view.refreshRoleRightList(roles);
        //Then update the corresponding dependent enetiyt list
        feedDomainsfromRoleList(view.getRoleRightComboBox().getSelectedValue());
    }

    private void feedTimezoneList() {
        //refresh the available time zones list
        helperService.getTimezones(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(Collection<String> result) {
                view.refreshTimezoneList(result);
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });

    }

    private void feedDomainsfromRoleList(Role selectedValue) {
        view.getDomainRightComboBox().clean();
        if (selectedValue != null) {
            //See if the selectValue is specific to channels or programmes
            if (selectedValue.getClass() == ChannelRole.class) {
                view.refreshDomainRightList(availableChannelLabels);
            }

        }
    }

    private boolean addNewRight() {

        Role role = view.getRoleRightComboBox().getSelectedValue();
        String domain = view.getDomainRightComboBox().getSelectedValue();

        //Check if the role and either the channel or programme parts of the rights are properly filled in
        if (role == null || domain == null) {
            return false;
        }

        RightForm newRight;
        if (role.getClass() == ChannelRole.class) {
            newRight = new ChannelRightForm(null, domain, (ChannelRole) role);
        } else {
            return false;
        }

        //Is it already present in the list?
        if (view.getRightsListBox().isValueIn(newRight)) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getRightsListBox().removeRow(view.getRightsListBox().getCurrentRow(event));
            }
        };
        view.addNewRight(newRight, deleteClickHandler);

        return true;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                    case channel:
                        //If a channel has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().channels_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
