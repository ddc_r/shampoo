/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.user.ForgotMyPasswordView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * @author okay_awright
 **/
public class ForgotMyPasswordPresenter extends PopupPagePresenter<ForgotMyPasswordView> {

    private LoginRPCServiceInterfaceAsync loginService;

    public void setLoginService(LoginRPCServiceInterfaceAsync loginService) {
        this.loginService = loginService;
    }

    @Override
    protected void onCheckAccess() {
        //Everyone can try to retrieve his password
        bindPage();
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Form validation handlers
        view.getTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                if (view.getEmailRadioButton().isSelected())
                    Validation.bindValidationError(Validation.checkEmail(view.getTextBox().getText()), view.getTextBox());
                else
                    Validation.bindValidationError(Validation.checkUsername(view.getTextBox().getText()), view.getTextBox());
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (view.getEmailRadioButton().isSelected()) {
            if (Validation.bindValidationError(Validation.checkEmail(view.getTextBox().getText()), view.getTextBox()))
                result = false;
        } else {
            if (Validation.bindValidationError(Validation.checkUsername(view.getTextBox().getText()), view.getTextBox()))
                result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new Profile from the view
        if (view.getEmailRadioButton().isSelected()) {
            loginService.sendUserPasswordToMailboxViaEmail(view.getTextBox().getText(), new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                @Override
                public void onResult(Void result) {
                    _formSubmitted();
                }
            });
        } else {
            loginService.sendUserPasswordToMailboxViaUsername(view.getTextBox().getText(), new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

                @Override
                public void onResult(Void result) {
                    _formSubmitted();
                }
            });
        }
    }

    private void _formSubmitted() {
        //Then close popup
        closePopup();
        //Show notification to user
        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().password_sent_to_user_email_address());
        //Stop here, nowhere to explicitly redirect
    }

    @Override
    public void onRefresh() {

        //Hide right list until we sure know the user is not an admin
        view.getTextBox().clean();
        view.getSubmit().clean();
    }


}
