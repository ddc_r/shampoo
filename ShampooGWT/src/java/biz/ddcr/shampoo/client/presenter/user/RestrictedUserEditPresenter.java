/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.DomainForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRightForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRole;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserFormWithPassword;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMap;
import biz.ddcr.shampoo.client.helper.view.ActionSortedMap;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenterWithFormParameter;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.user.RestrictedUserEditView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class RestrictedUserEditPresenter extends PopupPagePresenterWithFormParameter<RestrictedUserEditView, RestrictedUserForm> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private UserRPCServiceInterfaceAsync userService;
    private ChannelRPCServiceInterfaceAsync channelService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private HelperRPCServiceInterfaceAsync helperService;

    private ClickHandler displayDomainClickHandler = new ClickHandler() {

        @Override
            public void onClick(ClickEvent event) {
                RightForm rightForm = view.getRightsListBox().getValueAt(view.getRightsListBox().getCurrentRow(event));
                if (rightForm instanceof ProgrammeRightForm) {
                    programmeService.getSecuredProgramme(rightForm.getDomainId(), new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null){

                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }

                        });
                } else if (rightForm instanceof ChannelRightForm) {
                    channelService.getSecuredChannel(rightForm.getDomainId(), new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null){

                            @Override
                            public void onResult(ActionCollectionEntry<ChannelForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                            }

                        });
                }
            }
        };

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    public void setHelperService(HelperRPCServiceInterfaceAsync helperService) {
        this.helperService = helperService;
    }

    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the user to modify
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessUpdateRestrictedUser(getForm().getUsername(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Form validation handlers
        view.getConfirmPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkConfirmationPassword(view.getPasswordTextBox().getText(), view.getConfirmPasswordTextBox().getText()), view.getConfirmPasswordTextBox());
            }
        });
        view.getEmailTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox());
            }
        });
        view.getPasswordTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getPasswordTextBox().getText()), view.getPasswordTextBox());
            }
        });
        view.getChangePasswordCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.togglePasswordChange();
            }
        });
        //Automatic feed for domains from roles
        view.getRoleRightComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                feedDomainsfromRoleList(view.getRoleRightComboBox().getSelectedValue());
            }
        });

        //Add new right
        view.getAddNewRightButton().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                addNewRight();
            }
        });

        view.getTimezoneComboBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox());
            }
        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        if (Validation.bindValidationError(Validation.checkEmail(view.getEmailTextBox().getText()), view.getEmailTextBox())) {
            result = false;
        }
        if (view.getChangePasswordCheckBox().isChecked()) {
            if (Validation.bindValidationError(Validation.checkPassword(view.getPasswordTextBox().getText()), view.getPasswordTextBox())) {
                result = false;
            }
            if (Validation.bindValidationError(Validation.checkConfirmationPassword(view.getPasswordTextBox().getText(), view.getConfirmPasswordTextBox().getText()), view.getConfirmPasswordTextBox())) {
                result = false;
            }
        }
        if (Validation.bindValidationError(Validation.checkTimezone(view.getTimezoneComboBox().getSelectedValue()), view.getTimezoneComboBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkMaxText(view.getFirstAndLastNameTextBox().getText()), view.getFirstAndLastNameTextBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new User from the view
        RestrictedUserForm editedUser = null;
        if (view.getChangePasswordCheckBox().isChecked()) {
            editedUser = new RestrictedUserFormWithPassword();
            ((RestrictedUserFormWithPassword) editedUser).setPassword(view.getPasswordTextBox().getText());
        } else {
            editedUser = new RestrictedUserForm();
        }
        editedUser.setEmail(view.getEmailTextBox().getText());
        editedUser.setUsername(getForm().getUsername());
        final String finalUsername = editedUser.getUsername();
        editedUser.setEnabled(view.getEnabledCheckBox().isChecked());
        editedUser.setFirstAndLastName(view.getFirstAndLastNameTextBox().getText());
        editedUser.setTimezone(view.getTimezoneComboBox().getSelectedValue());
        editedUser.setEmailNotification(view.getEmailNotificationCheckBox().isChecked());

        //Fill it with rights
        ActionMap<RightForm> r = new ActionSortedMap<RightForm>();
        r.addAllNude(view.getRightsListBox().getAllValues());
        editedUser.setRightForms(r);

        //Make it persistant
        userService.updateSecuredUser(editedUser, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().user_updated(finalUsername));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_USERS);
            }
        });
    }

    @Override
    public void onRefresh() {
        //Reset everything
        view.getEnabledCheckBox().clean();
        view.getEnabledCheckBox().setChecked(getForm().isEnabled());
        view.getUsernameLabel().clean();
        view.getUsernameLabel().setCaption(getForm().getUsername());
        view.getEmailTextBox().clean();
        view.getEmailTextBox().setText(getForm().getEmail());
        view.getEmailNotificationCheckBox().clean();
        view.getEmailNotificationCheckBox().setChecked(getForm().isEmailNotification());
        view.getChangePasswordCheckBox().clean();
        //Do not change password by default
        view.getChangePasswordCheckBox().setChecked(false);
        view.togglePasswordChange();
        view.getPasswordTextBox().clean();
        view.getConfirmPasswordTextBox().clean();
        view.getFirstAndLastNameTextBox().clean();
        view.getFirstAndLastNameTextBox().setText(getForm().getFirstAndLastName());
        view.getSubmit().clean();
        //clear right list
        view.getDomainRightComboBox().clean();
        view.getRoleRightComboBox().clean();
        feedRolesList();
        view.getRightsListBox().clean();
        feedRightsList();
        //Fill in timezones
        view.getTimezoneComboBox().clean();
        feedTimezoneList();
    }

    private void feedRolesList() {
        HashSet<Role> roles = new HashSet<Role>();
        roles.addAll(Arrays.asList(ChannelRole.values()));
        roles.addAll(Arrays.asList(ProgrammeRole.values()));
        //refresh the available roles list
        view.refreshRoleRightList(roles);
        //Then update the corresponding dependent entity list
        feedDomainsfromRoleList(view.getRoleRightComboBox().getSelectedValue());
    }

    private void feedTimezoneList() {
        //refresh the available time zones list
        helperService.getTimezones(new ExtendedRPCCallback<Collection<String>>(getEventBus(), view.getTimezoneComboBox()) {

            @Override
            public void onResult(Collection<String> result) {
                view.refreshTimezoneList(result);
                selectDefaultTimezone();
            }

            /*@Override
            public void onInit() {
                view.getTimezoneComboBox().clean();
            }*/
        });

    }

    private void feedDomainsfromRoleList(Role selectedValue) {
        if (selectedValue != null) {
            //See if the selectValue is specific to channels or programmes
            if (selectedValue.getClass() == ProgrammeRole.class) {
                programmeService.getSecuredProgrammes(null, new ExtendedRPCCallback<ActionCollection<ProgrammeForm>>(getEventBus(), view.getDomainRightComboBox()) {

                    @Override
                    public void onResult(ActionCollection<ProgrammeForm> result) {
                        //refresh the available programme list
                        view.refreshDomainRightList(result);
                    }

                    /*@Override
                    public void onInit() {
                        view.getDomainRightComboBox().clean();
                    }*/
                });
            } else if (selectedValue.getClass() == ChannelRole.class) {
                channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getDomainRightComboBox()) {

                    @Override
                    public void onResult(ActionCollection<ChannelForm> result) {
                        //refresh the available channel list
                        view.refreshDomainRightList(result);
                    }

                    /*@Override
                    public void onInit() {
                        view.getDomainRightComboBox().clean();
                    }*/
                });
            }

        }
    }

    private boolean addNewRight() {

        Role role = view.getRoleRightComboBox().getSelectedValue();
        DomainForm domain = view.getDomainRightComboBox().getSelectedValue();

        //Check if the role and either the channel or programme parts of the rights are properly filled in
        if (role == null || domain == null) {
            return false;
        }

        RightForm newRight = null;
        if (role.getClass() == ChannelRole.class) {
            newRight = new ChannelRightForm(null, domain.getLabel(), (ChannelRole) role);
        } else if (role.getClass() == ProgrammeRole.class) {
            newRight = new ProgrammeRightForm(null, domain.getLabel(), (ProgrammeRole) role);
        } else {
            return false;
        }

        //Is it already present in the list?
        if (view.getRightsListBox().isValueIn(newRight)) {
            return false;
        }

        //Fnally add it to the list if we get this far
        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getRightsListBox().removeRow(view.getRightsListBox().getCurrentRow(event));
            }
        };
        view.addNewRight(newRight, deleteClickHandler, displayDomainClickHandler);

        return true;
    }

    private void feedRightsList() {

        ClickHandler deleteClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.getRightsListBox().removeRow(view.getRightsListBox().getCurrentRow(event));
            }
        };

        //Clear the currently bound user so as the isIn() method for checking if the same right cannot be added twice works
        ActionCollection<RightForm> newRights = new ActionSortedMap<RightForm>();
        for (ActionCollectionEntry<RightForm> rightFormEntry : getForm().getRightForms()) {
            RightForm newRight = rightFormEntry.getKey();
            newRight.setRestrictedUserId(null);
            newRights.merge(newRight, rightFormEntry.getValue());
        }
        view.refreshExistingRights(newRights, deleteClickHandler, displayDomainClickHandler);

    }

    private void selectDefaultTimezone() {
        if (getForm().getTimezone() != null) {
            view.getTimezoneComboBox().setSelectedValue(getForm().getTimezone());
        }
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case user:
                        //Warn the user the data he is currently editing are not up to date anymore
                        if (flag==FeedbackObjectForm.FLAG.edit && (objectId==null || objectId.equals(getForm().getUsername())))
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().entity_concurrently_edited());
                        break;
                    case programme:
                        //If a programme has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().programmes_concurrently_edited());
                        break;
                    case channel:
                        //If a channel has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().channels_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
}
