/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.presenter.user;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.right.ChannelRightForm;
import biz.ddcr.shampoo.client.form.right.ProgrammeRightForm;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.user.UserListView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class UserListPresenter extends PagePresenterWithHistory<UserListView> {

    private UserRPCServiceInterfaceAsync userService;
    private ProgrammeRPCServiceInterfaceAsync programmeService;
    private ChannelRPCServiceInterfaceAsync channelService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setUserService(UserRPCServiceInterfaceAsync userService) {
        this.userService = userService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    public void setProgrammeService(ProgrammeRPCServiceInterfaceAsync programmeService) {
        this.programmeService = programmeService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewUsers(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        //Show the navigation bar links you are granted to see only
        refreshNavigationBar();
        //Build all the tabs
        refreshLists();
    }

    protected void refreshLists() {
        view.getUserPanel().clean();

        //Add the Working Users tab
        //Add the header click handler
        final SortableTableClickHandler workingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshUserList();
            }
        };
        //Rebuild the UI
        view.refreshUserPanel(workingPanelHeaderClickHandler);
        //And update it immediately
        refreshUserList();

    }

    protected void refreshUserList() {
        final ClickHandler editUserClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserForm user = view.getUserList().getValueAt(view.getUserList().getCurrentRow(event));
                if (user instanceof AdministratorForm) {
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_ADMINEDIT, (AdministratorForm) user);
                } else {
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSEREDIT, (RestrictedUserForm) user);
                }
            }
        };
        final ClickHandler displayUserClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                UserForm user = view.getUserList().getValueAt(view.getUserList().getCurrentRow(event));
                if (user instanceof AdministratorForm) {
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_ADMINDISPLAY, (AdministratorForm) user);
                } else {
                    redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERDISPLAY, (RestrictedUserForm) user);
                }
            }
        };
        final SerializableItemSelectionHandler<RightForm> displayDomainSelectionHandler = new SerializableItemSelectionHandler<RightForm>() {

            @Override
            public void onItemSelected(RightForm currentValue) {
                if (currentValue != null) {
                    if (currentValue instanceof ChannelRightForm) {
                        channelService.getSecuredChannel(currentValue.getDomainId(), new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null) {

                            @Override
                            public void onResult(ActionCollectionEntry<ChannelForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                            }
                        });
                    } else if (currentValue instanceof ProgrammeRightForm) {
                        programmeService.getSecuredProgramme(currentValue.getDomainId(), new ExtendedRPCCallback<ActionCollectionEntry<ProgrammeForm>>(getEventBus(), null) {

                            @Override
                            public void onResult(ActionCollectionEntry<ProgrammeForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_PROGRAMMEDISPLAY, result.getItem());
                            }
                        });
                    }
                }
            }
        };
        final YesNoClickHandler deleteAllUsersClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllUsers(view.getUserList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteUserClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getUserList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteUser(view.getUserList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        userService.getSecuredUsers(view.getUserList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<? extends UserForm>>(getEventBus(), view.getUserList()) {

            @Override
            public void onResult(ActionCollection<? extends UserForm> result) {
                view.refreshUserList(result,
                        editUserClickHandler,
                        deleteUserClickHandler,
                        deleteAllUsersClickHandler,
                        displayUserClickHandler,
                        displayDomainSelectionHandler);
            }

            /*@Override
            public void onInit() {
                view.getUserList().clean();
            }*/
        });
    }

    protected void refreshNavigationBar() {

        view.removeAllNavigationLinks();

        final ClickHandler addAdminClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_ADMINADD);
            }
        };
        authorizationService.checkAccessAddAdministrators(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddAdminLink(addAdminClickHandler);
                }
            }
        });

        final ClickHandler addUserClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_RESTRICTEDUSERADD);
            }
        };
        authorizationService.checkAccessAddRestrictedUsers(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddUserLink(addUserClickHandler);
                }
            }
        });
    }

    private void deleteUser(final GenericFormTableInterface list, final int currentRow) {
        final UserForm user = (UserForm) list.getValueAt(currentRow);
        if (user != null) {
            userService.deleteSecuredUser(user.getUsername(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().user_deleted(user.getUsername()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_USERS);
                }
            });
        }
    }

    private void deleteAllUsers(final GenericFormTableInterface list) {
        final Collection<UserForm> users = list.getAllCheckedValues();
        if (users != null && !users.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (UserForm form : users) {
                ids.add(form.getUsername());
            }
            userService.deleteSecuredUsers(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[users.size()];
                    int i = 0;
                    for (UserForm user : users) {
                        ids[i] = user.getUsername();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().users_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_USERS);
                }
            });
        }
    }

    /** Check whether the id given by the GUI event signal corresponds to a channel in the view list **/
    private boolean isInUserList(String id) {
        for (UserForm userForm : view.getUserList().getAllValues()) {
            if (userForm.getUsername().equals(id))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case user:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_USERS);
                        } else {
                            if (isInUserList(objectId))
                                add(MessageEventsEnum.REFRESH_USERS);                                
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_USERS:
            case REFRESH_MYPROFILE:
                refreshUserList();
                break;
        }
    }
}
