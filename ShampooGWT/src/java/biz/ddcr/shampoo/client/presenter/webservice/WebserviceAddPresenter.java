/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.webservice;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.UserIn;
import biz.ddcr.shampoo.client.helper.Validation;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.presenter.PopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.webservice.WebserviceAddView;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class WebserviceAddPresenter extends PopupPagePresenter<WebserviceAddView> {

    private AuthorizationRPCServiceInterfaceAsync authorizationService;
    private ChannelRPCServiceInterfaceAsync channelService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessAddWebservices(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void bindView() {
        //Validation handlers for webservice
        view.getApiKeyTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkLabel(view.getApiKeyTextBox().getText()), view.getApiKeyTextBox());
            }
        });
        view.getPrivateKeyTextBox().addChangeHandler(new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                Validation.bindValidationError(Validation.checkPassword(view.getPrivateKeyTextBox().getText()), view.getPrivateKeyTextBox());
            }
        });

        //Misc gadgets
        view.getMaxDailyQuotaCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.toggleMaxDailyQuotaChange();
            }

        });
        view.getMaxFireRateCheckBox().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                view.toggleMaxFireRateChange();
            }

        });

        //Form submit
        view.getSubmit().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                if (validateForm()) {
                    submitForm();
                }
            }
        });
        //Close box
        view.getCancel().addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                closePopup();
            }
        });
    }

    public boolean validateForm() {
        boolean result = true;
        //Check label
        if (Validation.bindValidationError(Validation.checkLabel(view.getApiKeyTextBox().getText()), view.getApiKeyTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkPassword(view.getPrivateKeyTextBox().getText()), view.getPrivateKeyTextBox())) {
            result = false;
        }
        if (Validation.bindValidationError(Validation.checkLabel(view.getChannelComboBox().getSelectedValue()), view.getChannelComboBox())) {
            result = false;
        }

        return result;
    }

    public void submitForm() {
        //Build a new Programme from the view
        final WebserviceForm newWS = new WebserviceForm();

        newWS.setApiKey(view.getApiKeyTextBox().getText());
        newWS.setPrivateKey(view.getPrivateKeyTextBox().getText());
        newWS.setChannel(view.getChannelComboBox().getSelectedValue());

        newWS.setArchiveModule(view.getModuleArchiveCheckBox().isChecked());
        newWS.setTimetableModule(view.getModuleTimetableCheckBox().isChecked());
        newWS.setNowPlayingModule(view.getModuleNowPlayingCheckBox().isChecked());
        newWS.setComingNextModule(view.getModuleComingNextCheckBox().isChecked());
        newWS.setVoteModule(view.getModuleVoteCheckBox().isChecked());
        newWS.setRequestModule(view.getModuleRequestCheckBox().isChecked());

        if (view.getMaxDailyQuotaCheckBox().isChecked())
            newWS.setMaxDailyQuota(view.getMaxDailyQuotaNumericBox().getValue().longValue());
        else
            newWS.setMaxDailyQuota(null);
        if (view.getMaxFireRateCheckBox().isChecked())
            newWS.setMaxFireRate(view.getMaxFireRateNumericBox().getValue().longValue());
        else
            newWS.setMaxFireRate(null);
        newWS.setWhitelistRegexp(view.getWhitelistRegexpTextBox().getText().trim());

        //Make it persistant
        channelService.addSecuredWebservice(newWS, new ExtendedRPCCallback<Void>(getEventBus(), view.getSubmit()) {

            @Override
            public void onResult(Void result) {
                //Then close popup
                closePopup();
                //Show notification to user
                getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().webservice_added(newWS.getApiKey()));
                //And refresh the back page
                //UPDATE: now directly handled by reverse AJAX
                //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_WEBSERVICES);
            }
        });
    }

    @Override
    protected void onRefresh() {
        //Reset everything
        view.getApiKeyTextBox().clean();
        view.getPrivateKeyTextBox().clean();
        view.getSubmit().clean();
        view.getModuleArchiveCheckBox().clean();
        //TODO: re-activate this setting when it's been implemented
        view.getModuleArchiveCheckBox().setChecked(false);
        view.getModuleTimetableCheckBox().clean();
        //TODO: re-activate this setting when it's been implemented
        view.getModuleTimetableCheckBox().setChecked(false);
        view.getModuleNowPlayingCheckBox().clean();
        view.getModuleNowPlayingCheckBox().setChecked(true); //Provide a sensible default value
        view.getModuleComingNextCheckBox().clean();
        //TODO: re-activate this setting when it's been implemented
        view.getModuleComingNextCheckBox().setChecked(false);
        view.getModuleVoteCheckBox().clean();
        //TODO: re-activate this setting when it's been implemented
        view.getModuleVoteCheckBox().setChecked(false);
        view.getModuleRequestCheckBox().clean();
        //TODO: re-activate this setting when it's been implemented
        view.getModuleRequestCheckBox().setChecked(false);
        view.getChannelComboBox().clean();
        view.getMaxDailyQuotaCheckBox().clean();
        view.getMaxDailyQuotaNumericBox().clean();
        view.toggleMaxDailyQuotaChange();
        view.getMaxFireRateCheckBox().clean();
        view.getMaxFireRateNumericBox().clean();
        view.toggleMaxFireRateChange();
        view.getWhitelistRegexpTextBox().clean();
        //Update channels list
        feedChannelsList();
    }

    private void feedChannelsList() {
        channelService.getSecuredChannels(null, new ExtendedRPCCallback<ActionCollection<ChannelForm>>(getEventBus(), view.getChannelComboBox()) {

            @Override
            public void onResult(ActionCollection<ChannelForm> result) {
                view.refreshChannelList(result);
            }
        });
    }

    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                //Don't show notifications if the emitter of the feedback is the currently logged user
                if (senderId==null || !senderId.equals(UserIn.authenticatedUserName))
                switch (type) {
                    case channel:
                        //If a channel has been added or removed then just notify the user
                        if (flag!=FeedbackObjectForm.FLAG.edit)
                            //Show notification to user
                            getEventBus().dispatch(GlobalEventsEnum.DISPLAY_WARNING, I18nTranslator.getInstance().channels_concurrently_edited());
                        break;
                }
            }
        }.get();
    }
    
}
