/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.webservice;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.presenter.GenericFormPopupPagePresenter;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.webservice.WebserviceDisplayView;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class WebserviceDisplayPresenter extends GenericFormPopupPagePresenter<WebserviceDisplayView, WebserviceForm> {

    private ChannelRPCServiceInterfaceAsync channelService;

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }
    
    @Override
    protected void onCheckAccess() {
        //This form must have been launched with a parameter that is the user to view
        //Otherwise no need to go any further
        if (getForm() != null) {
            authorizationService.checkAccessViewWebservice(getForm().getChannel(), new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

                @Override
                public void onResult(Boolean result) {
                    if (result) //Access is granted
                    {
                        bindPage();
                    } else //Access denied: raise a new exception and update the notification box
                    {
                        getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                    }
                }
            });
        }
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    public void formSpecificBind() {
        //Do noting
    }

    @Override
    public void onFormSpecificRefresh() {
        //Reset everything
        view.getApiKeyLabel().clean();
        view.getApiKeyLabel().setCaption(getForm().getApiKey());
        view.getPrivateKeyLabel().clean();
        view.getPrivateKeyLabel().setCaption(getForm().getPrivateKey());
        view.getChannelLabel().setCaption(getForm().getChannel());
        view.refreshModulesList(//
                getForm().isArchiveModule(),//
                getForm().isComingNextModule(),//
                getForm().isNowPlayingModule(),//
                getForm().isRequestModule(),//
                getForm().isTimetableModule(),//
                getForm().isVoteModule());
        view.refreshMaxDailyQuotaLabel(getForm().getMaxDailyQuota());
        view.refreshMaxFireRateLabel(getForm().getMaxFireRate());
        view.refreshWhitelistRegexpLabel(getForm().getWhitelistRegexp());
        view.getMetadataPanel().setVisible(false);
        view.getMetadataPanel().clean();        
        fetchMetadata();
    }

    private void fetchMetadata() {
        channelService.getSecuredDynamicPublicWebserviceMetadata(getForm().getApiKey(), new ExtendedRPCCallback<ActionCollectionEntry<WebserviceModule>>(getEventBus(), view.getMetadataPanel()) {
            @Override
            public void onResult(ActionCollectionEntry<WebserviceModule> result) {
                view.refreshMetadata(result);
            }
        });
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case webservice:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_WEBSERVICES);
                        } else {
                            //objectId is an API key
                            if (objectId==null || (getForm().getApiKey()!=null && objectId.equals(getForm().getApiKey().toString())))
                                    add(MessageEventsEnum.REFRESH_WEBSERVICES);
                            }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        //Do only answers to these messages, appropriately
        switch (message) {
            case REFRESH_WEBSERVICES:
                //Refresh its metadata
                fetchMetadata();
                break;
        }
    }
}
