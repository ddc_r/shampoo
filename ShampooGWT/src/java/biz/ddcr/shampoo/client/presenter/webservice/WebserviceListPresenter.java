/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.presenter.webservice;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.MessageEventsEnum;
import biz.ddcr.shampoo.client.RedirectionEventsEnum;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.helper.ExtendedRPCCallback;
import biz.ddcr.shampoo.client.helper.SignalHelper;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.presenter.PagePresenterWithHistory;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterfaceAsync;
import biz.ddcr.shampoo.client.view.views.webservice.WebserviceListView;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class WebserviceListPresenter extends PagePresenterWithHistory<WebserviceListView> {

    private ChannelRPCServiceInterfaceAsync channelService;
    protected AuthorizationRPCServiceInterfaceAsync authorizationService;

    public void setAuthorizationService(AuthorizationRPCServiceInterfaceAsync authorizationService) {
        this.authorizationService = authorizationService;
    }

    public void setChannelService(ChannelRPCServiceInterfaceAsync channelService) {
        this.channelService = channelService;
    }

    @Override
    protected void onCheckAccess() {
        authorizationService.checkAccessViewWebservices(new ExtendedRPCCallback<Boolean>(getEventBus(), null) {

            @Override
            public void onResult(Boolean result) {
                if (result) //Access is granted
                {
                    bindPage();
                } else //Access denied: raise a new exception and update the notification box
                {
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AccessDeniedException()));
                }
            }
        });
    }

    @Override
    protected void onPageVisible() {
        //Do nothing
    }

    @Override
    protected void onRefresh() {
        //Show the navigation bar links you are granted to see only
        refreshNavigationBar();
        //Build all the tabs
        refreshLists();
    }

    private void refreshLists() {
        view.getWebservicePanel().clean();

        //Add the header click handler
        final SortableTableClickHandler baseWorkingPanelHeaderClickHandler = new SortableTableClickHandler() {

            @Override
            public void onClick(ClickEvent event, GroupAndSort groupAndSortObject) {
                refreshWebserviceList();
            }
        };
        authorizationService.checkAccessViewWebservices(new ExtendedRPCCallback<Boolean>(getEventBus(), view.getWebservicePanel()) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshWebservicePanel(baseWorkingPanelHeaderClickHandler);
                    //And update it immediately
                    refreshWebserviceList();
                }
            }
        });
    }

    protected void refreshWebserviceList() {
        final ClickHandler editWebserviceClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                WebserviceForm ws = view.getWebserviceList().getValueAt(view.getWebserviceList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_WEBSERVICEEDIT, ws);
            }
        };
        final ClickHandler displayWebserviceClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                WebserviceForm ws = view.getWebserviceList().getValueAt(view.getWebserviceList().getCurrentRow(event));
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_WEBSERVICEDISPLAY, ws);
            }
        };
        final ClickHandler displayChannelClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                WebserviceForm ws = view.getWebserviceList().getValueAt(view.getWebserviceList().getCurrentRow(event));
                    channelService.getSecuredChannel(ws.getChannel(), new ExtendedRPCCallback<ActionCollectionEntry<ChannelForm>>(getEventBus(), null){
                            @Override
                            public void onResult(ActionCollectionEntry<ChannelForm> result) {
                                //No need to check if the user can access this form, both the view and the popup initialisation method already filter out what is not readable
                                redirectPageNoHistory(RedirectionEventsEnum.SHOW_CHANNELDISPLAY, result.getItem());
                            }
                        });
            }
        };
        final YesNoClickHandler deleteAllWebservicesClickHandler = new YesNoClickHandler() {

            @Override
            public void onPreClick(ClickEvent event) {
                //Do nothing
            }

            @Override
            public void onYesClick() {
                deleteAllWebservices(view.getWebserviceList());
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };
        final YesNoClickHandler deleteWebserviceClickHandler = new YesNoClickHandler() {

            int currentRow = -1;

            @Override
            public void onPreClick(ClickEvent event) {
                currentRow = view.getWebserviceList().getCurrentRow(event);
            }

            @Override
            public void onYesClick() {
                deleteWebservice(view.getWebserviceList(), currentRow);
            }

            @Override
            public void onNoClick() {
                //Do nothing
            }
        };

        channelService.getSecuredWebservices(view.getWebserviceList().getCurrentGroupAndSortParameters(), new ExtendedRPCCallback<ActionCollection<WebserviceForm>>(getEventBus(), view.getWebserviceList()) {

            @Override
            public void onResult(ActionCollection<WebserviceForm> result) {
                view.refreshWebserviceList(result,
                        editWebserviceClickHandler,
                        deleteWebserviceClickHandler,
                        deleteAllWebservicesClickHandler,
                        displayWebserviceClickHandler,
                        displayChannelClickHandler);
            }

            /*@Override
            public void onInit() {
                view.getWebserviceList().clean();
            }*/
        });
    }

    protected void refreshNavigationBar() {

        view.removeAllNavigationLinks();

        final ClickHandler addWebserviceClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                redirectPageNoHistory(RedirectionEventsEnum.SHOW_WEBSERVICEADD);
            }
        };
        authorizationService.checkAccessAddWebservices(new ExtendedRPCCallback<Boolean>(eventBus) {

            @Override
            public boolean isFailureCatchable(Throwable caught) {
                return (!(caught instanceof AccessDeniedException));
            }

            @Override
            public void onResult(Boolean result) {
                //Activate the menu entries if access is granted
                if (result) {
                    //Rebuild the UI
                    view.refreshAddWebserviceLink(addWebserviceClickHandler);
                }
            }
        });

    }

    private void deleteWebservice(final GenericFormTableInterface list, final int currentRow) {
        final WebserviceForm ws = (WebserviceForm)list.getValueAt(currentRow);
        if (ws != null) {
            channelService.deleteSecuredWebservice(ws.getApiKey(), new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().webservice_deleted(ws.getApiKey()));
                    //Refresh the corresponding list
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_WEBSERVICES);
                }
            });
        }
    }

    private void deleteAllWebservices(final GenericFormTableInterface list) {
        final Collection<WebserviceForm> wses = list.getAllCheckedValues();
        if (wses != null && !wses.isEmpty()) {
            Collection ids = new HashSet<String>();
            for (WebserviceForm form : wses) {
                ids.add(form.getApiKey());
            }
            channelService.deleteSecuredWebservices(ids, new ExtendedRPCCallback(getEventBus(), list) {

                @Override
                public void onResult(Object result) {
                    //Show notification to user
                    String[] ids = new String[wses.size()];
                    int i = 0;
                    for (WebserviceForm programme : wses) {
                        ids[i] = programme.getApiKey();
                        i++;
                    }
                    getEventBus().dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().webservices_deleted(I18nTranslator.toCSV(ids)));
                    //Refresh the corresponding list if applicable
                    //UPDATE: now directly handled by reverse AJAX
                    //getEventBus().dispatch(GlobalEventsEnum.BROADCAST_MESSAGE_TO_CONTENTBOX, MessageEventsEnum.REFRESH_WEBSERVICES);
                }
            });
        }
    }

    /** Check whether the id given by the GUI event signal corresponds to a channel in the view list **/
    private boolean isInWebserviceList(String id) {
        for (WebserviceForm wsForm : view.getWebserviceList().getAllValues()) {
            if (wsForm.getApiKey().equals(id))
                return true;
        }
        return false;
    }
    
    @Override
    public List<MessageEventsEnum> processMessages(Collection<FeedbackObjectForm> messages) {
        return new SignalHelper(messages) {

            @Override
            public void process(FeedbackObjectForm.TYPE type, String objectId, FeedbackObjectForm.FLAG flag, String senderId) {
                switch (type) {
                    case webservice:
                        if (flag == FeedbackObjectForm.FLAG.add){
                            add(MessageEventsEnum.REFRESH_WEBSERVICES);
                        } else {
                            if (isInWebserviceList(objectId))
                                add(MessageEventsEnum.REFRESH_WEBSERVICES);                                
                        }
                        break;
                }
            }
        }.get();
    }
    
    @Override
    public void onSignal(MessageEventsEnum message) {
        switch (message) {
            case REFRESH_CHANNELS:
            case REFRESH_WEBSERVICES:
                refreshWebserviceList();
                break;
        }
    }
}
