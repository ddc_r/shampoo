/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.serviceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.*;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * Communicates with servlets via REST, bypass Google RPC services
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class GenericHTTPService {
        
    public static class URLFragment {

        private String name;
        private String value;

        public URLFragment(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

    private GenericHTTPService() {
    }

    static StringBuilder buildQueryString(Collection<URLFragment> queryEntries) {
        StringBuilder sb = new StringBuilder();

        if (queryEntries != null) {
            for (Iterator<URLFragment> iterator = queryEntries.iterator(); iterator.hasNext();) {
                URLFragment queryEntry = iterator.next();

                // encode the characters in the name
                sb.append(URL.encodeQueryString(queryEntry.getName()));
                sb.append("=");
                // encode the characters in the value
                sb.append(URL.encodeQueryString(queryEntry.getValue()));

                sb.append("&");
            }
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }

        return sb;
    }
    
    public static String makeURL(String relativeURLPath, Collection<URLFragment> urlFragments) {
        StringBuilder fragment = buildQueryString(urlFragments);
        if (fragment.length() != 0) {
            relativeURLPath = fragment.insert(0, "?").insert(0, relativeURLPath).toString();
        }
        return GWT.getModuleBaseURL() + relativeURLPath;
    }
    
    public static void doGet(String fullURL, RequestCallback callback) throws RequestException {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, fullURL);
        //wait 10m for the request to complete
        //builder.setTimeoutMillis(600000);
        builder.sendRequest(null/*
                 * XmlHttpRequest does not use parameter for send()
                 */, callback);
    }
    public static void doLocalGet(String relativeURLPath, Collection<URLFragment> urlFragments, RequestCallback callback) throws RequestException {
        doGet(makeURL(relativeURLPath, urlFragments), callback);
    }

    public static void doHead(String fullURL, RequestCallback callback) throws RequestException {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.HEAD, fullURL);
        //wait 10m for the request to complete
        //builder.setTimeoutMillis(600000);
        builder.sendRequest(null/*
                 * XmlHttpRequest does not use parameter for send()
                 */, callback);
    }
    public static void doLocalHead(String relativeURLPath, Collection<URLFragment> urlFragments, RequestCallback callback) throws RequestException {
        doHead(makeURL(relativeURLPath, urlFragments), callback);
    }
        
    public static void doPost(String fullURL, Collection<URLFragment> urlFragments, RequestCallback callback) throws RequestException {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, fullURL);
        builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
        //wait 10m for the request to complete
        //builder.setTimeoutMillis(600000);
        builder.sendRequest(buildQueryString(urlFragments).toString(), callback);
    }
    public static void doLocalPost(String relativeURLPath, Collection<URLFragment> urlFragments, RequestCallback callback) throws RequestException {
        doPost(GWT.getModuleBaseURL() + relativeURLPath, urlFragments, callback);
    }
}
