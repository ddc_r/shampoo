/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.archive;

import biz.ddcr.shampoo.client.form.archive.ArchiveForm;
import biz.ddcr.shampoo.client.form.archive.ArchiveForm.ARCHIVE_TAGS;
import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.form.archive.ReportForm.REPORT_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ArchiveRPCServiceInterface extends RemoteService {

    public void addSecuredReport(ReportForm reportForm) throws Exception;
    
    public void deleteSecuredArchive(String id) throws Exception;
    public void deleteSecuredArchives(Collection<String> ids) throws Exception;

    public void deleteSecuredReport(String id) throws Exception;
    public void deleteSecuredReports(Collection<String> ids) throws Exception;
    
    public ActionCollection<? extends ArchiveForm> getSecuredArchives(GroupAndSort<ARCHIVE_TAGS> constraint, String localTimeZone) throws Exception;
    public ActionCollection<? extends ArchiveForm> getSecuredArchives(GroupAndSort<ARCHIVE_TAGS> constraint) throws Exception;
    public ActionCollection<? extends ArchiveForm> getSecuredArchivesForChannelId(GroupAndSort<ARCHIVE_TAGS> constraint, String channelId, String localTimeZone) throws Exception;
    public ActionCollection<? extends ArchiveForm> getSecuredArchivesForChannelId(GroupAndSort<ARCHIVE_TAGS> constraint, String channelId) throws Exception;
    
    public ActionCollection<? extends ReportForm> getSecuredReports(GroupAndSort<REPORT_TAGS> constraint) throws Exception;
    public ActionCollection<? extends ReportForm> getSecuredReportsForChannelId(GroupAndSort<REPORT_TAGS> constraint, String channelId) throws Exception;
}
