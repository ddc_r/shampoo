/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ChannelRPCServiceInterface extends RemoteService {
    
    public void addSecuredChannel(ChannelForm channelForm) throws Exception;
    public void addSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception;
    public void addSecuredWebservice(WebserviceForm webserviceForm) throws Exception;
    //Specializations of inserts for timetable slots
    public void cloneSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime, Collection<YearMonthDayHourMinuteSecondMillisecondInterface> newDates) throws Exception;

    public void updateSecuredChannel(ChannelForm channelForm) throws Exception;
    public void updateSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception;
    public void updateSecuredWebservice(WebserviceForm webserviceForm) throws Exception;
    //Subsets of updates for timetable slots: i.e. only one or two fields are modified only, not the whole form
    public void shiftSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long shift) throws Exception;
    public void resizeSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long size) throws Exception;
    public void setDecommissioningSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, YearMonthDayInterface decommissioningDate) throws Exception;
    public void enableSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, boolean enabled) throws Exception;
    public void setPlaylistSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, String playlistId) throws Exception;

    public void deleteSecuredChannel(String id) throws Exception;
    public void deleteSecuredChannels(Collection<String> ids) throws Exception;
    public void deleteSecuredTimetableSlot(TimetableSlotFormID id) throws Exception;
    public void deleteSecuredTimetableSlots(Collection<TimetableSlotFormID> ids) throws Exception;
    public void deleteSecuredWebservice(String id) throws Exception;
    public void deleteSecuredWebservices(Collection<String> ids) throws Exception;
   
    public ActionCollectionEntry<ChannelForm> getSecuredChannel(String id) throws Exception;
    //helper
    public String getChannelTimeZone(String id) throws Exception;
    public ActionCollectionEntry<StreamerModule> getSecuredDynamicChannelStreamerMetadata(String id) throws Exception;
    public ActionCollectionEntry<String> getSecuredChannelStreamerKey(String id) throws Exception;
    public Long getUnsecuredNextAvailableStreamerSeatNumber() throws Exception;

    public ActionCollection<ChannelForm> getSecuredChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception;
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception;
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception;

    public ActionCollectionEntry<TimetableSlotForm> getSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime) throws Exception;

    public ActionCollectionEntry<WebserviceForm> getSecuredWebservice(String id) throws Exception;
    
    public ActionCollectionEntry<WebserviceModule> getSecuredDynamicPublicWebserviceMetadata(String id) throws Exception;

    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelId(
            GroupAndSort<TIMETABLE_TAGS> constraint,
            String channelId) throws Exception;
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelId(
            GroupAndSort<TIMETABLE_TAGS> constraint,
            String channelId,
            String localTimeZone) throws Exception;
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForProgrammeId(
            GroupAndSort<TIMETABLE_TAGS> constraint,
            String programmeId) throws Exception;
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForProgrammeId(
            GroupAndSort<TIMETABLE_TAGS> constraint,
            String programmeId,
            String localTimeZone) throws Exception;
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForPlaylistId(
            GroupAndSort<TIMETABLE_TAGS> constraint,
            String playlistId) throws Exception;
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForPlaylistId(
            GroupAndSort<TIMETABLE_TAGS> constraint,
            String playlistId,
            String localTimeZone) throws Exception;
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelIdAndWeek(
            String channelId,
            YearMonthDayHourMinuteInterface from) throws Exception;

    public ActionCollection<WebserviceForm> getSecuredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint) throws Exception;

    public Collection<String> getUnsecuredOpenChannelLabels() throws Exception;
}
