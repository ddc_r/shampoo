/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ChannelRPCServiceInterfaceAsync {

    public void updateSecuredChannel(ChannelForm channelForm, AsyncCallback<Void> asyncCallback);

    public void addSecuredChannel(ChannelForm channelForm, AsyncCallback<Void> asyncCallback);

    public void getSecuredChannels(GroupAndSort<CHANNEL_TAGS> constraint, AsyncCallback<ActionCollection<ChannelForm>> asyncCallback);

    public void getSecuredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId, AsyncCallback<ActionCollection<ChannelForm>> asyncCallback);

    public void getSecuredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds, AsyncCallback<ActionCollection<ChannelForm>> asyncCallback);

    public void addSecuredTimetableSlot(TimetableSlotForm timetableSlotForm, AsyncCallback<Void> asyncCallback);

    public void updateSecuredTimetableSlot(TimetableSlotForm timetableSlotForm, AsyncCallback<Void> asyncCallback);

    public void getSecuredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void getSecuredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId, String localTimeZone, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void getChannelTimeZone(String id, AsyncCallback<String> asyncCallback);

    public void deleteSecuredChannel(String id, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredChannels(Collection<String> ids, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredTimetableSlot(TimetableSlotFormID id, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredTimetableSlots(Collection<TimetableSlotFormID> ids, AsyncCallback<Void> asyncCallback);

    public void getSecuredChannel(String id, AsyncCallback<ActionCollectionEntry<ChannelForm>> asyncCallback);

    public void getSecuredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void getSecuredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId, String localTimeZone, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void getSecuredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void getSecuredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId, String localTimeZone, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void setDecommissioningSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, YearMonthDayInterface decommissioningDate, AsyncCallback<Void> asyncCallback);

    public void shiftSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long shift, AsyncCallback<Void> asyncCallback);

    public void resizeSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long size, AsyncCallback<Void> asyncCallback);

    public void setPlaylistSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, String playlistId, AsyncCallback<Void> asyncCallback);

    public void getSecuredTimetableSlotsForChannelIdAndWeek(String channelId, YearMonthDayHourMinuteInterface from, AsyncCallback<ActionCollection<TimetableSlotForm>> asyncCallback);

    public void getSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, AsyncCallback<ActionCollectionEntry<TimetableSlotForm>> asyncCallback);

    public void enableSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, boolean enabled, AsyncCallback<Void> asyncCallback);

    public void cloneSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime, Collection<YearMonthDayHourMinuteSecondMillisecondInterface> newDates, AsyncCallback<Void> asyncCallback);

    public void getSecuredDynamicChannelStreamerMetadata(String id, AsyncCallback<ActionCollectionEntry<StreamerModule>> asyncCallback);
    
    public void getSecuredChannelStreamerKey(String id, AsyncCallback<ActionCollectionEntry<String>> asyncCallback);

    public void getUnsecuredOpenChannelLabels(AsyncCallback<Collection<String>> asyncCallback);
    
    public void getUnsecuredNextAvailableStreamerSeatNumber(AsyncCallback<Long> asyncCallback);

    public void addSecuredWebservice(WebserviceForm webserviceForm, AsyncCallback<Void> asyncCallback);

    public void updateSecuredWebservice(WebserviceForm webserviceForm, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredWebservice(String id, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredWebservices(Collection<String> ids, AsyncCallback<Void> asyncCallback);

    public void getSecuredWebservice(String id, AsyncCallback<ActionCollectionEntry<WebserviceForm>> asyncCallback);

    public void getSecuredDynamicPublicWebserviceMetadata(String id, AsyncCallback<ActionCollectionEntry<WebserviceModule>> asyncCallback);
    
    public void getSecuredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint, AsyncCallback<ActionCollection<WebserviceForm>> asyncCallback);

}
