/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.helper;

import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm.RECURRING_EVENT;
import biz.ddcr.shampoo.client.form.track.format.AUDIO_TYPE;
import biz.ddcr.shampoo.client.form.track.format.AllowedContainerModule;
import biz.ddcr.shampoo.client.form.track.format.PICTURE_TYPE;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT;
import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface HelperRPCServiceInterface extends RemoteService {

    //Time conversion functions

    public Collection<String> getTimezones() throws Exception;
    public YearMonthDayHourMinuteSecondMillisecondInterface getCurrentTime(String localTimeZone) throws Exception;
    public byte getWeekOfMonth(YearMonthDayInterface currentDate) throws Exception;
    public YearMonthDayHourMinuteInterface switchTimeZone(YearMonthDayHourMinuteInterface currentTime, String localTimeZone) throws Exception;
    public YearMonthDayHourMinuteSecondMillisecondInterface switchTimeZone(YearMonthDayHourMinuteSecondMillisecondInterface currentTime, String localTimeZone) throws Exception;
    public YearMonthDayHourMinuteSecondMillisecondInterface addSeconds(YearMonthDayHourMinuteSecondMillisecondInterface currentTime, long secondsToAdd) throws Exception;
    public YearMonthDayInterface addDays(YearMonthDayInterface currentTime, long daysToAdd) throws Exception;
    public YearMonthDayHourMinuteInterface addDays(YearMonthDayHourMinuteInterface currentTime, long daysToAdd) throws Exception;
    public YearMonthDayHourMinuteSecondMillisecondInterface addDays(YearMonthDayHourMinuteSecondMillisecondInterface currentTime, long daysToAdd) throws Exception;
    public long diffMilliseconds(YearMonthDayHourMinuteSecondMillisecondInterface fromTime, YearMonthDayHourMinuteSecondMillisecondInterface toTime) throws Exception;
    /** Compute the number of milliseconds currently elapsed since the provided time */
    public long diffNowMilliseconds(YearMonthDayHourMinuteSecondMillisecondInterface fromTime) throws Exception;
    public Collection<YearMonthDayHourMinuteSecondMillisecondInterface> getInterimDates(YearMonthDayHourMinuteSecondMillisecondInterface fromTime, YearMonthDayInterface toTime, RECURRING_EVENT frequency) throws Exception;

    public Collection<AUDIO_TYPE> getAllowedAudioFormats();
    public Collection<PICTURE_TYPE> getAllowedPictureFormats();
    public AllowedContainerModule getAllowedAudioFeatures(AUDIO_TYPE audioType);

    //Versioning

    public String getAppVersion();
    public String getCopyrightNotice();

    //Branding

    //HTML to display in the GUI header box
    public String getHTMLHeader();
    //HTML to display in the main front page
    public String getHTMLFrontpage();

    //Tracks and Pictures

    public Collection<String> getAllowedPictureFormatExtensions() throws Exception;
    public Collection<String> getAllowedAudioFormatExtensions() throws Exception;
    public String getTemporaryAudioUploadURL();
    public String getTemporaryPictureUploadURL();

    //Session management
    public String getJSESSIONID();

    //Serilization formats
    public Collection<SERIALIZATION_METADATA_FORMAT> getTextSerializationFormats();
    
}
