/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.helper;

import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm.RECURRING_EVENT;
import biz.ddcr.shampoo.client.form.track.format.AUDIO_TYPE;
import biz.ddcr.shampoo.client.form.track.format.AllowedContainerModule;
import biz.ddcr.shampoo.client.form.track.format.PICTURE_TYPE;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface HelperRPCServiceInterfaceAsync {

    public void getTimezones(AsyncCallback<Collection<String>> asyncCallback);

    public void getAppVersion(AsyncCallback<String> asyncCallback);
    
    public void getCopyrightNotice(AsyncCallback<String> asyncCallback);

    public void getAllowedAudioFormats(AsyncCallback<Collection<AUDIO_TYPE>> asyncCallback);

    public void getAllowedPictureFormats(AsyncCallback<Collection<PICTURE_TYPE>> asyncCallback);

    public void getAllowedAudioFeatures(AUDIO_TYPE audioType, AsyncCallback<AllowedContainerModule> asyncCallback);

    public void getCurrentTime(String localTimeZone, AsyncCallback<YearMonthDayHourMinuteSecondMillisecondInterface> asyncCallback);

    public void getWeekOfMonth(YearMonthDayInterface currentDate, AsyncCallback<java.lang.Byte> asyncCallback);

    public void switchTimeZone(YearMonthDayHourMinuteInterface currentTime, String localTimeZone, AsyncCallback<YearMonthDayHourMinuteInterface> asyncCallback);

    public void addDays(YearMonthDayInterface currentTime, long daysToAdd, AsyncCallback<YearMonthDayInterface> asyncCallback);

    public void getInterimDates(YearMonthDayHourMinuteSecondMillisecondInterface fromTime, YearMonthDayInterface toTime, RECURRING_EVENT frequency, AsyncCallback<Collection<YearMonthDayHourMinuteSecondMillisecondInterface>> asyncCallback);

    public void switchTimeZone(YearMonthDayHourMinuteSecondMillisecondInterface currentTime, String localTimeZone, AsyncCallback<YearMonthDayHourMinuteSecondMillisecondInterface> asyncCallback);

    public void addDays(YearMonthDayHourMinuteInterface currentTime, long daysToAdd, AsyncCallback<YearMonthDayHourMinuteInterface> asyncCallback);

    public void addDays(YearMonthDayHourMinuteSecondMillisecondInterface currentTime, long daysToAdd, AsyncCallback<YearMonthDayHourMinuteSecondMillisecondInterface> asyncCallback);

    public void addSeconds(YearMonthDayHourMinuteSecondMillisecondInterface currentTime, long secondsToAdd, AsyncCallback<YearMonthDayHourMinuteSecondMillisecondInterface> asyncCallback);

    public void diffMilliseconds(YearMonthDayHourMinuteSecondMillisecondInterface fromTime, YearMonthDayHourMinuteSecondMillisecondInterface toTime, AsyncCallback<java.lang.Long> asyncCallback);
    
    public void diffNowMilliseconds(YearMonthDayHourMinuteSecondMillisecondInterface fromTime, AsyncCallback<java.lang.Long> asyncCallback);

    public void getHTMLHeader(AsyncCallback<String> asyncCallback);

    public void getHTMLFrontpage(AsyncCallback<String> asyncCallback);

    public void getAllowedPictureFormatExtensions(AsyncCallback<Collection<String>> asyncCallback);

    public void getAllowedAudioFormatExtensions(AsyncCallback<Collection<String>> asyncCallback);

    public void getTemporaryAudioUploadURL(AsyncCallback<String> asyncCallback);

    public void getTemporaryPictureUploadURL(AsyncCallback<String> asyncCallback);

    public void getJSESSIONID(AsyncCallback<String> asyncCallback);

    public void getTextSerializationFormats(AsyncCallback<Collection<SERIALIZATION_METADATA_FORMAT>> asyncCallback);
    
}
