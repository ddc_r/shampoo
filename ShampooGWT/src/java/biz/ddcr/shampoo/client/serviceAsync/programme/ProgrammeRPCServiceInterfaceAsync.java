/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ProgrammeRPCServiceInterfaceAsync {

    public void addSecuredProgramme(ProgrammeForm programmeForm, AsyncCallback<Void> asyncCallback);

    public void updateSecuredProgramme(ProgrammeForm programmeForm, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPlaylist(PlaylistForm playlistForm, AsyncCallback<Void> asyncCallback);

    public void addSecuredPlaylist(PlaylistForm playlistForm, AsyncCallback<Void> asyncCallback);

    public void getSecuredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint, AsyncCallback<ActionCollection<ProgrammeForm>> asyncCallback);

    public void getSecuredProgrammesForChannelId(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId, AsyncCallback<ActionCollection<ProgrammeForm>> asyncCallback);

    public void getSecuredProgrammesForTrackId(GroupAndSort<PROGRAMME_TAGS> constraint, String trackId, AsyncCallback<ActionCollection<ProgrammeForm>> asyncCallback);

    public void getSecuredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint, AsyncCallback<ActionCollection<PlaylistForm>> asyncCallback);

    public void getSecuredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId, AsyncCallback<ActionCollection<PlaylistForm>> asyncCallback);

    public void deleteSecuredProgramme(String id, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredProgrammes(Collection<String> ids, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredPlaylist(String id, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredPlaylists(Collection<String> ids, AsyncCallback<Void> asyncCallback);

    public void getSecuredProgramme(String id, AsyncCallback<ActionCollectionEntry<ProgrammeForm>> asyncCallback);

    public void getSecuredPlaylist(String id, AsyncCallback<ActionCollectionEntry<PlaylistForm>> asyncCallback);

    public void getSecuredPlaylist(String id, String localTimeZone, AsyncCallback<ActionCollectionEntry<PlaylistForm>> asyncCallback);

    public void getSecuredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint, String localTimeZone, AsyncCallback<ActionCollection<PlaylistForm>> asyncCallback);

    public void getSecuredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId, String localTimeZone, AsyncCallback<ActionCollection<PlaylistForm>> asyncCallback);

    public void addSecuredPlaylist(PlaylistForm playlistForm, String flyerUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPlaylist(PlaylistForm playlistForm, String flyerUploadId, AsyncCallback<Void> asyncCallback);

    public void getSecuredProgrammesForSongAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds, AsyncCallback<ActionCollection<ProgrammeForm>> asyncCallback);

    public void getSecuredProgrammesForAdvertAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds, AsyncCallback<ActionCollection<ProgrammeForm>> asyncCallback);

    public void getSecuredProgrammesForJingleAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds, AsyncCallback<ActionCollection<ProgrammeForm>> asyncCallback);

}
