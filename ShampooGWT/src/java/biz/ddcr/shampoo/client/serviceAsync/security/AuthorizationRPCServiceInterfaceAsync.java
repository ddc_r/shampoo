/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.serviceAsync.security;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author okay_awright
 *
 */
public interface AuthorizationRPCServiceInterfaceAsync {

    public void checkAccessAddAdministrators(AsyncCallback<Boolean> arg1);

    public void checkAccessAddBroadcastTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessAddChannels(AsyncCallback<Boolean> arg1);

    public void checkAccessAddPendingTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessAddPlaylist(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessAddPlaylists(AsyncCallback<Boolean> arg1);

    public void checkAccessAddProgrammes(AsyncCallback<Boolean> arg1);

    public void checkAccessAddReports(AsyncCallback<Boolean> arg1);

    public void checkAccessAddRestrictedUsers(AsyncCallback<Boolean> arg1);

    public void checkAccessAddTimetableSlot(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessAddTimetableSlots(AsyncCallback<Boolean> arg1);

    public void checkAccessAddWebservices(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteAdministrator(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteAdministrators(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteArchive(String archiveIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteArchives(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteBroadcastTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteBroadcastTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteChannel(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteChannels(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteJournal(String logIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteJournals(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteNotification(String messageIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteNotifications(AsyncCallback<Boolean> arg1);

    public void checkAccessDeletePendingTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeletePendingTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessDeletePlaylist(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeletePlaylists(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteProgramme(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteProgrammes(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteReport(String reportIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteReports(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteRestrictedUser(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteRestrictedUsers(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteTimetableSlot(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteTimetableSlots(AsyncCallback<Boolean> arg1);

    public void checkAccessDeleteWebservice(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessDeleteWebservices(AsyncCallback<Boolean> arg1);

    public void checkAccessLimitedPlaylistDependencyUpdateTimetable(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessLimitedTrackDependencyUpdateProgramme(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessListenStreamer(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessListenStreamers(AsyncCallback<Boolean> arg1);

    public void checkAccessPollStatusStreamer(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessReviewPendingTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessReviewPendingTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateAdministrator(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateAdministrators(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateBroadcastTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateBroadcastTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateChannel(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateChannels(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdatePendingTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdatePendingTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdatePlaylist(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdatePlaylists(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateProgramme(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateProgrammes(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateRestrictedUser(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateRestrictedUsers(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateTimetableSlot(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateTimetableSlots(AsyncCallback<Boolean> arg1);

    public void checkAccessUpdateWebservice(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessUpdateWebservices(AsyncCallback<Boolean> arg1);

    public void checkAccessViewAdministrator(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewAdministrators(AsyncCallback<Boolean> arg1);

    public void checkAccessViewArchive(String archiveIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewArchives(AsyncCallback<Boolean> arg1);

    public void checkAccessViewBroadcastTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewBroadcastTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessViewChannel(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewChannels(AsyncCallback<Boolean> arg1);

    public void checkAccessViewJournal(String logIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewJournals(AsyncCallback<Boolean> arg1);

    public void checkAccessViewNotification(String messageIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewNotifications(AsyncCallback<Boolean> arg1);

    public void checkAccessViewPendingTrack(String trackIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewPendingTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessViewPlaylist(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewPlaylists(AsyncCallback<Boolean> arg1);

    public void checkAccessViewProgramme(String programmeIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewProgrammes(AsyncCallback<Boolean> arg1);

    public void checkAccessViewQueue(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewQueues(AsyncCallback<Boolean> arg1);

    public void checkAccessViewReport(String reportIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewReports(AsyncCallback<Boolean> arg1);

    public void checkAccessViewRestrictedUser(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewRestrictedUsers(AsyncCallback<Boolean> arg1);

    public void checkAccessViewTimetableSlot(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewTimetableSlots(AsyncCallback<Boolean> arg1);

    public void checkAccessViewTracks(AsyncCallback<Boolean> arg1);

    public void checkAccessViewUser(String userIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewUsers(AsyncCallback<Boolean> arg1);

    public void checkAccessViewWebservice(String channelIdToCheck, AsyncCallback<Boolean> arg2);

    public void checkAccessViewWebservices(AsyncCallback<Boolean> arg1);
}
