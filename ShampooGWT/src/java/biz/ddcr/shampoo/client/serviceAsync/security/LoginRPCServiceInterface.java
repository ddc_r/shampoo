/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.security;

import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.form.user.LoginForm;
import biz.ddcr.shampoo.client.form.user.ProfileDetailForm;
import biz.ddcr.shampoo.client.form.user.ProfilePasswordForm;
import biz.ddcr.shampoo.client.form.user.ProfileRightForm;
import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface LoginRPCServiceInterface extends RemoteService {

    public void logIn(LoginForm loginForm) throws Exception;
    public void autoLogInFromCookie() throws Exception;
    public void logOut() throws Exception;

    public String getCurrentlyAuthenticatedUserLogin() throws Exception;
    public boolean isCurrentlyAuthenticatedUserAnAdministrator() throws Exception;

    public void updateCurrentlyAuthenticatedUser(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile) throws Exception;
    public void updateCurrentlyAuthenticatedUser(ProfilePasswordForm newProfilePassword) throws Exception;
    public void updateCurrentlyAuthenticatedUser(ProfileDetailForm newProfile) throws Exception;
    public ProfileRightForm getCurrentlyAuthenticatedRestrictedUserRights() throws Exception;

    public void updateCurrentlyAuthenticatedUserAndListenerRights(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception;
    public void updateCurrentlyAuthenticatedUserAndListenerRights(ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception;

    public void setCurrentlyAuthenticatedUserVote(String songId, VOTE score) throws Exception;
    public VOTE getCurrentlyAuthenticatedUserVote(String songId) throws Exception;

    public void setCurrentlyAuthenticatedUserRequest(String songId, String channelId, String message) throws Exception;
    public void resetCurrentlyAuthenticatedUserRequest(String songId) throws Exception;
    public RequestModule getCurrentlyAuthenticatedUserRequest(String songId) throws Exception;

    public ProfileDetailForm getCurrentlyAuthenticatedUserProfileDetails() throws Exception;
    public String getCurrentlyAuthenticatedUserTimezone() throws Exception;

    public void sendUserPasswordToMailboxViaUsername(String registeredUsername) throws Exception;
    public void sendUserPasswordToMailboxViaEmail(String registeredEmailAddress) throws Exception;

    public void addSelfAuthenticatedUser(String registeredUsername, ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception;
}
