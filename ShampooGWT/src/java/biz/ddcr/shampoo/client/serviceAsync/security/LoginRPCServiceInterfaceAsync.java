/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.security;

import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.form.user.LoginForm;
import biz.ddcr.shampoo.client.form.user.ProfileDetailForm;
import biz.ddcr.shampoo.client.form.user.ProfilePasswordForm;
import biz.ddcr.shampoo.client.form.user.ProfileRightForm;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface LoginRPCServiceInterfaceAsync {
    public void logIn(LoginForm loginForm, AsyncCallback<Void> asyncCallback);
    public void logOut(AsyncCallback<Void> asyncCallback);

    public void getCurrentlyAuthenticatedUserLogin(AsyncCallback<String> asyncCallback);

    public void isCurrentlyAuthenticatedUserAnAdministrator(AsyncCallback<java.lang.Boolean> asyncCallback);

    public void getCurrentlyAuthenticatedRestrictedUserRights(AsyncCallback<ProfileRightForm> asyncCallback);

    public void updateCurrentlyAuthenticatedUser(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, AsyncCallback<Void> asyncCallback);

    public void updateCurrentlyAuthenticatedUser(ProfileDetailForm newProfile, AsyncCallback<Void> asyncCallback);

    public void updateCurrentlyAuthenticatedUser(ProfilePasswordForm newProfilePassword, AsyncCallback<Void> asyncCallback);

    public void autoLogInFromCookie(AsyncCallback<Void> asyncCallback);

    public void getCurrentlyAuthenticatedUserProfileDetails(AsyncCallback<ProfileDetailForm> asyncCallback);

    public void getCurrentlyAuthenticatedUserTimezone(AsyncCallback<String> asyncCallback);

    public void sendUserPasswordToMailboxViaUsername(String registeredUsername, AsyncCallback<Void> asyncCallback);

    public void sendUserPasswordToMailboxViaEmail(String registeredEmailAddress, AsyncCallback<Void> asyncCallback);

    public void getCurrentlyAuthenticatedUserRequest(String songId, AsyncCallback<RequestModule> asyncCallback);

    public void resetCurrentlyAuthenticatedUserRequest(String songId, AsyncCallback<Void> asyncCallback);

    public void setCurrentlyAuthenticatedUserVote(String songId, VOTE score, AsyncCallback<Void> asyncCallback);

    public void getCurrentlyAuthenticatedUserVote(String songId, AsyncCallback<VOTE> asyncCallback);

    public void setCurrentlyAuthenticatedUserRequest(String songId, String channelId, String message, AsyncCallback<Void> asyncCallback);

    public void addSelfAuthenticatedUser(String registeredUsername, ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights, AsyncCallback<Void> asyncCallback);

    public void updateCurrentlyAuthenticatedUserAndListenerRights(ProfileDetailForm newProfile, Collection<String> listenerRights, AsyncCallback<Void> asyncCallback);

    public void updateCurrentlyAuthenticatedUserAndListenerRights(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights, AsyncCallback<Void> asyncCallback);

}
