/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import com.google.gwt.user.client.rpc.RemoteService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface TrackRPCServiceInterface extends RemoteService {

    //Editing objects
    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;
    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId) throws Exception;
    public void addSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;
    public void addSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId) throws Exception;

    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;
    public void updateSecuredBroadcastTrackAndAudioOnly(BroadcastTrackForm trackForm, String trackUploadId) throws Exception;
    public void updateSecuredBroadcastTrackAndPictureOnly(BroadcastTrackForm trackForm, String coverArtUploadId) throws Exception;
    public void updateSecuredBroadcastTracksAndPictureOnly(Collection<BroadcastTrackForm> trackForms, String coverArtUploadId) throws Exception;
    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm) throws Exception;
    public void updateSecuredBroadcastTracks(Collection<BroadcastTrackForm> trackForms) throws Exception;
    public void updateSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;
    public void updateSecuredPendingTrackAndAudioOnly(PendingTrackForm trackForm, String trackUploadId) throws Exception;
    public void updateSecuredPendingTrackAndPictureOnly(PendingTrackForm trackForm, String coverArtUploadId) throws Exception;
    public void updateSecuredPendingTracksAndPictureOnly(Collection<PendingTrackForm> trackForms, String coverArtUploadId) throws Exception;
    public void updateSecuredPendingTrack(PendingTrackForm trackForm) throws Exception;
    public void updateSecuredPendingTracks(Collection<PendingTrackForm> trackForms) throws Exception;
    //Subsets of updates for timetable slots: i.e. only one or two fields are modified only, not the whole form
    public void validateSecuredPendingTrack(String pendingTrackId) throws Exception;
    public void validateSecuredPendingTracks(Collection<String> pendingTrackIds) throws Exception;
    public void rejectSecuredPendingTrack(String pendingTrackId, String comment) throws Exception;
    public void rejectSecuredPendingTracks(Collection<String> pendingTrackIds, String comment) throws Exception;
    /*public void enableSecuredBroadcastTrack(String id, boolean enabled) throws Exception;
    public void enableSecuredBroadcastTracks(Collection<String> ids, boolean enabled) throws Exception;*/
    public void deleteSecuredBroadcastTrack(String trackId) throws Exception;
    public void deleteSecuredBroadcastTracks(Collection<String> trackIds) throws Exception;
    public void deleteSecuredPendingTrack(String trackId) throws Exception;
    public void deleteSecuredPendingTracks(Collection<String> trackIds) throws Exception;
   
    //Fetching objects
    public ActionCollectionEntry<? extends TrackForm> getSecuredTrack(String id) throws Exception;
    public ActionCollectionEntry<PendingTrackForm> getSecuredPendingTrack(String id) throws Exception;
    public ActionCollectionEntry<BroadcastTrackForm> getSecuredBroadcastTrack(String id) throws Exception;

    //Helpers
    public ActionCollection<? extends PendingTrackForm> getSecuredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint) throws Exception;
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws Exception;
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId) throws Exception;

    /*public ActionCollection<BroadcastSongForm> getSecuredBroadcastSongsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    public ActionCollection<BroadcastAdvertForm> getSecuredBroadcastAdvertsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    public ActionCollection<BroadcastJingleForm> getSecuredBroadcastJinglesForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;*/

    public EmbeddedTagModule getDraftTags(String id) throws Exception;
    public EmbeddedTagModule getTags(String id) throws Exception;
    public ContainerModule getDraftAudioFileInfo(String id) throws Exception;
    public CoverArtModule getDraftPictureFileInfo(String id) throws Exception;

}
