/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import java.util.Collection;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author okay_awright
 **/
public interface TrackRPCServiceInterfaceAsync {

    public void getSecuredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint, AsyncCallback<ActionCollection<? extends PendingTrackForm>> asyncCallback);

    public void getSecuredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, AsyncCallback<ActionCollection<? extends BroadcastTrackForm>> asyncCallback);

    public void getSecuredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId, AsyncCallback<ActionCollection<? extends BroadcastTrackForm>> asyncCallback);

    public void getDraftTags(String id, AsyncCallback<EmbeddedTagModule> asyncCallback);
    public void getTags(String id, AsyncCallback<EmbeddedTagModule> asyncCallback);

    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void addSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void getDraftAudioFileInfo(String id, AsyncCallback<ContainerModule> asyncCallback);

    public void deleteSecuredBroadcastTrack(String trackId, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredPendingTrack(String trackId, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredBroadcastTracks(Collection<String> trackIds, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredPendingTracks(Collection<String> trackIds, AsyncCallback<Void> asyncCallback);

    public void rejectSecuredPendingTrack(String pendingTrackId, String comment, AsyncCallback<Void> asyncCallback);

    public void rejectSecuredPendingTracks(Collection<String> pendingTrackIds, String comment, AsyncCallback<Void> asyncCallback);

    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId, AsyncCallback<Void> asyncCallback);

    public void addSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredBroadcastTrackAndAudioOnly(BroadcastTrackForm trackForm, String trackUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredBroadcastTrackAndPictureOnly(BroadcastTrackForm trackForm, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPendingTrackAndAudioOnly(PendingTrackForm trackForm, String trackUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPendingTrackAndPictureOnly(PendingTrackForm trackForm, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void validateSecuredPendingTrack(String pendingTrackId, AsyncCallback<Void> asyncCallback);

    public void validateSecuredPendingTracks(Collection<String> pendingTrackIds, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPendingTrack(PendingTrackForm trackForm, AsyncCallback<Void> asyncCallback);

    public void getSecuredTrack(String id, AsyncCallback<ActionCollectionEntry<? extends TrackForm>> asyncCallback);

    public void getSecuredPendingTrack(String id, AsyncCallback<ActionCollectionEntry<PendingTrackForm>> asyncCallback);

    public void getSecuredBroadcastTrack(String id, AsyncCallback<ActionCollectionEntry<BroadcastTrackForm>> asyncCallback);

    public void getDraftPictureFileInfo(String id, AsyncCallback<CoverArtModule> asyncCallback);

    public void updateSecuredBroadcastTracksAndPictureOnly(Collection<BroadcastTrackForm> trackForms, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredBroadcastTracks(Collection<BroadcastTrackForm> trackForms, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPendingTracksAndPictureOnly(Collection<PendingTrackForm> trackForms, String coverArtUploadId, AsyncCallback<Void> asyncCallback);

    public void updateSecuredPendingTracks(Collection<PendingTrackForm> trackForms, AsyncCallback<Void> asyncCallback);

}
