/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.serviceAsync.user;

import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.AdministratorFormWithPassword;
import biz.ddcr.shampoo.client.form.user.RestrictedUserFormWithPassword;
import biz.ddcr.shampoo.client.form.user.UserFormWithPassword;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.Collection;


/**
 *
 * @author okay_awright
 **/
public interface UserRPCServiceInterfaceAsync {

    public void getSecuredAdministrators(GroupAndSort<USER_TAGS> constraint, AsyncCallback<ActionCollection<AdministratorForm>> asyncCallback);

    public void getSecuredRestrictedUsers(GroupAndSort<USER_TAGS> constraint, AsyncCallback<ActionCollection<RestrictedUserForm>> asyncCallback);

    public void getSecuredUsers(GroupAndSort<USER_TAGS> constraint, AsyncCallback<ActionCollection<? extends UserForm>> asyncCallback);

    public void addSecuredUser(UserFormWithPassword userForm, AsyncCallback<Void> asyncCallback);

    public void updateSecuredUser(UserForm userForm, AsyncCallback<Void> asyncCallback);

    public void getSecuredUser(String id, AsyncCallback<ActionCollectionEntry<? extends UserForm>> asyncCallback);

    public void getSecuredRestrictedUser(String id, AsyncCallback<ActionCollectionEntry<RestrictedUserForm>> asyncCallback);

    public void getSecuredAdministrator(String id, AsyncCallback<ActionCollectionEntry<AdministratorForm>> asyncCallback);

    public void getSecuredUserWithPassword(String id, AsyncCallback<ActionCollectionEntry<? extends UserFormWithPassword>> asyncCallback);

    public void getSecuredRestrictedUserWithPassword(String id, AsyncCallback<ActionCollectionEntry<RestrictedUserFormWithPassword>> asyncCallback);

    public void getSecuredAdministratorWithPassword(String id, AsyncCallback<ActionCollectionEntry<AdministratorFormWithPassword>> asyncCallback);

    public void deleteSecuredUser(String userId, AsyncCallback<Void> asyncCallback);

    public void deleteSecuredUsers(Collection<String> userIds, AsyncCallback<Void> asyncCallback);

}
