/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.templates;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.FrameWidgetInterface;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.view.LazyView;

/**
 *
 * @author okay_awright
 **/
public class FooterBox extends Composite implements LazyView, FrameWidgetInterface {

    private GWTLabel versioningLabel;
    private GWTLabel copyrightLabel;

    public FooterBox() {}

    @Override
    public void createView() {
        GWTVerticalPanel footer = new GWTVerticalPanel();
        //Custom CSS
        footer.setStyleName("FooterBox");

        versioningLabel = new GWTLabel(null);
        footer.add(versioningLabel);
        copyrightLabel = new GWTLabel(null);
        footer.add(copyrightLabel);
        
        initWidget(footer);
    }

    public void setVersioning(String text) {
        versioningLabel.setCaption(I18nTranslator.getInstance().versioning()+": "+text);
    }

    public void setCopyright(String text) {
        copyrightLabel.setCaption(text);
    }
    
    @Override
    public Widget getWidget() {
        return this;
    }

}
