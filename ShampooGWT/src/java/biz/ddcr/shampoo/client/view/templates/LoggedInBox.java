/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.templates;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class LoggedInBox extends Composite implements LoggedInBoxInterface {

    private GWTLabel username = null;
    private GWTClickableLabel editProfile = null;
    private GWTClickableLabel logout = null;
    private GWTClickableLabel viewNotification = null;

    @Override
    public GenericHyperlinkInterface getEditProfileLabel() {
        return editProfile;
    }
    @Override
    public GenericHyperlinkInterface getLogoutLabel() {
        return logout;
    }
    @Override
    public GenericHyperlinkInterface getViewNotificationLabel() {
        return viewNotification;
    }
    @Override
    public GenericLabelInterface getUsernameLabel() {
        return username;
    }

    public LoggedInBox() {
        
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();
        //Custom CSS
        mainPanel.setStyleName("LoggedInBox");

        //Show the authenticated username
        username = new GWTLabel(null);
        GWTGrid grid = new GWTGrid();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().logged_in_as() + ":"), username);

        //Display the "Change My Profile" link
        editProfile = new GWTClickableLabel(I18nTranslator.getInstance().change_my_profile());

        //Display the link to the list of user messages
        viewNotification = new GWTClickableLabel(I18nTranslator.getInstance().view_messages());

        //Display the logout link
        logout = new GWTClickableLabel(I18nTranslator.getInstance().log_out());

        mainPanel.add(grid);
        mainPanel.add(editProfile);
        mainPanel.add(viewNotification);
        mainPanel.add(logout);
        
        initWidget(mainPanel);

    }

    @Override
    public Widget getWidget() {
        return this;
    }

}
