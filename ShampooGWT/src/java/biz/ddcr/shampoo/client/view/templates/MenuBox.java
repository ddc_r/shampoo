/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.templates;

import biz.ddcr.shampoo.client.view.widgets.GWTCSSTwoLevelsVerticalMenu;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericCSSMenuInterface;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class MenuBox extends Composite implements MenuBoxInterface {

    private GWTCSSTwoLevelsVerticalMenu menu = null;

    @Override
    public GenericCSSMenuInterface getMenu() {
        return menu;
    }

    public MenuBox() {

        GWTVerticalPanel mainPanel = new GWTVerticalPanel();
        //Custom CSS
        mainPanel.setStyleName("MenuBox");

        menu = new GWTCSSTwoLevelsVerticalMenu();
        mainPanel.add(menu);

        initWidget(mainPanel);
    }

    @Override
    public Widget getWidget() {
        return this;
    }

}
