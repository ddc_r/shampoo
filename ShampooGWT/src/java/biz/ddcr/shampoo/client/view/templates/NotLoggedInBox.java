/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.templates;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTCheckBox;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTPasswordTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericCheckBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTextBoxInterface;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class NotLoggedInBox extends Composite implements NotLoggedInBoxInterface {

    private GWTTextBox username = null;
    private GWTPasswordTextBox password = null;
    private GWTCheckBox rememberme = null;
    private GWTButton login = null;
    private GWTClickableLabel signup = null;
    private GWTClickableLabel passwordrecovery = null;

    @Override
    public GenericHyperlinkInterface getLoginButton() {
        return login;
    }
    @Override
    public GenericCheckBoxInterface getRememberMeCheckBox() {
        return rememberme;
    }
    @Override
    public GenericTextBoxInterface getPasswordTextBox() {
        return password;
    }
    @Override
    public GenericTextBoxInterface getUsernameTextBox() {
        return username;
    }
    @Override
    public GenericHyperlinkInterface getForgotMyPasswordButton() {
        return passwordrecovery;
    }
    @Override
    public GenericHyperlinkInterface getSignUpButton() {
        return signup;
    }

    public NotLoggedInBox() {

        GWTVerticalPanel mainPanel = new GWTVerticalPanel();
        //Custom CSS
        mainPanel.setStyleName("NotLoggedInBox");

        GWTGrid grid = new GWTGrid();

        username = new GWTTextBox();
        password = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().username() + ":"), username);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password() + ":"), password);

        rememberme = new GWTCheckBox(false);
        rememberme.setCaption(I18nTranslator.getInstance().remember_me());
        login = new GWTButton(I18nTranslator.getInstance().log_in());

        signup = new GWTClickableLabel(I18nTranslator.getInstance().sign_up());

        passwordrecovery = new GWTClickableLabel(I18nTranslator.getInstance().forgot_my_password());

        mainPanel.add(grid);
        mainPanel.add(login);
        mainPanel.add(rememberme);
        mainPanel.add(passwordrecovery);
        mainPanel.add(signup);

        initWidget(mainPanel);

    }

    @Override
    public Widget getWidget() {
        return this;
    }

}
