/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.templates;

import biz.ddcr.shampoo.client.view.widgets.NotificationItem;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class NotificationBox extends Composite implements NotificationBoxInterface {

    private GWTVerticalPanel notificationContainer;

    public NotificationBox() {}

    @Override
    public void createView() {
        notificationContainer = new GWTVerticalPanel();
        //Custom CSS
        notificationContainer.setStyleName("NotificationBox");
        initWidget(notificationContainer);
    }

    @Override
    public Widget getWidget() {
        return this;
    }

    @Override
    public void addNotification(NotificationItem item) {
        notificationContainer.add(item);
    }

    @Override
    public void removeNotification(NotificationItem item) {
        notificationContainer.remove(item);
    }
    
    @Override
    public void removeAllNotifications() {
        notificationContainer.clean();
    }

    @Override
    public int getNumberOfCurrentNotifications() {
        return notificationContainer.getNumberOfBoundFrameWidget();
    }
    
    public NotificationItem getNotification(int index) {
        return (NotificationItem) notificationContainer.getBoundFrameWidget(index);
    }

}
