package biz.ddcr.shampoo.client.view.templates;

import biz.ddcr.shampoo.client.view.widgets.FrameWidgetInterface;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockPanel;

public class Viewport extends Composite implements ViewportInterface {

    private GWTFlowPanel headerBox = new GWTFlowPanel();
    private GWTFlowPanel footerBox = new GWTFlowPanel();
    private GWTFlowPanel loginBox = new GWTFlowPanel();
    private GWTFlowPanel menuBox = new GWTFlowPanel();
    private GWTFlowPanel notificationBox = new GWTFlowPanel();
    private GWTFlowPanel contentBox = new GWTFlowPanel();

    public Viewport() {

        //The viewport
        DockPanel dock = new DockPanel();
        dock.setStyleName("DockPanel");

        //Dock a header
        dock.add(headerBox, DockPanel.NORTH);
        headerBox.setStyleName("DockTop");
        //Dock a footer
        dock.add(footerBox, DockPanel.SOUTH);
        footerBox.setStyleName("DockBottom");
        // Dock a left sidebar
        GWTVerticalPanel sidebarDock = new GWTVerticalPanel();
        dock.add(sidebarDock, DockPanel.WEST);
        sidebarDock.setStyleName("DockLeft");
        //The sidebar contains:
        // -A login box
        // -A menu
        // Wrap some content in a DecoratorPanel
        GWTFlowPanel decPanel1 = new GWTFlowPanel();
        decPanel1.add(loginBox);
        sidebarDock.add(decPanel1);
        decPanel1.setStyleName("DockTopLeftPart");
        GWTFlowPanel decPanel2 = new GWTFlowPanel();
        decPanel2.add(menuBox);
        decPanel2.setStyleName("DockBottomLeftPart");
        sidebarDock.add(decPanel2);
        // Dock a main page
        GWTVerticalPanel mainPageDock = new GWTVerticalPanel();
        dock.add(mainPageDock, DockPanel.CENTER);
        mainPageDock.setStyleName("DockCenter");
        //The main page contains:
        // -A message notification box
        // -A place where pages browsable by the user will be displayed
        mainPageDock.add(notificationBox);
        GWTFlowPanel decPanel3 = new GWTFlowPanel();
        decPanel3.add(contentBox);
        decPanel3.setStyleName("DockCenterPart");
        mainPageDock.add(decPanel3);

	initWidget( dock );
    }

    @Override
    public void setLoginBoxWidget(FrameWidgetInterface widget) {
	loginBox.clean();
	loginBox.add(widget);
    }

    @Override
    public void setMenuBoxWidget(FrameWidgetInterface widget) {
        menuBox.clean();
	menuBox.add(widget);
    }

    @Override
    public void setHeaderBoxWidget(FrameWidgetInterface widget) {
        headerBox.clean();
	headerBox.add(widget);
    }

    @Override
    public void setFooterBoxWidget(FrameWidgetInterface widget) {
        footerBox.clean();
	footerBox.add(widget);
    }

    @Override
    public void setContentBoxWidget(FrameWidgetInterface widget) {
        contentBox.clean();
	contentBox.add(widget);
    }

    @Override
    public void setNotificationBoxWidget(FrameWidgetInterface widget) {
        notificationBox.clean();
	notificationBox.add(widget);
    }

    @Override
    public FrameWidgetInterface getContentBoxWidget() {
        return contentBox.getFirstBoundFrameWidget();
    }

}
