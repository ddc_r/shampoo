/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class GenericFormPageView extends PageView {

    //Captions
    private GWTLabel creationLabel;
    private GWTLabel latestModificationLabel;

    //Button
    private GWTClickableLabel cancel;

    public GenericLabelInterface getCreationLabel() {
        return creationLabel;
    }

    public GenericLabelInterface getLatestModificationLabel() {
        return latestModificationLabel;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    /**
     * Implement to display form-implementation-specific data
     * @return
     */
    public abstract GWTVerticalPanel getFormSpecificPageContent();

    @Override
    public GWTVerticalPanel buildPageContent() {

        GWTVerticalPanel genericPanel = new GWTVerticalPanel();

        genericPanel.add(getFormSpecificPageContent());

        GWTDisclosurePanel logPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().log());
        GWTVerticalPanel subPanel = new GWTVerticalPanel();
        creationLabel = new GWTLabel(null);
        subPanel.add(creationLabel);
        latestModificationLabel = new GWTLabel(null);
        subPanel.add(latestModificationLabel);
        logPanel.add(subPanel);

        genericPanel.add(logPanel);

        return genericPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

}
