/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.views;

import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public abstract class PageView extends Composite implements ViewInterface {

    GWTFlowPanel navigationPanel = null;

    public PageView() {
        super();
    }

    @Override
    public void createView() {
        
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();
        //Custom CSS
        mainPanel.setStyleName("Page");
        mainPanel.setWidth("100%");

        //Header
        String headerText = getHeaderText();
        if (headerText!=null && headerText.length()!=0) {
            GWTLabel header = new GWTLabel(null);
            //Custom CSS
            header.setStyleName("PageHeader");
            header.setCaption(headerText);
            header.setWidth("100%");
            
            String headerCSSMarker = getHeaderCSSMarker();
            if (headerCSSMarker!=null && headerCSSMarker.length()!=0) {
                header.addStyleDependentName(headerCSSMarker);
            }
            
            mainPanel.add(header);
        }

        //Main content
        GenericPanelInterface content = buildPageContent();
        if (content!=null) {
            content.setWidth("100%");
            //Custom CSS
            content.setStyleName("PagePanel");
            mainPanel.add(content);
        }

        //Footer
        navigationPanel = new GWTFlowPanel();
        setNavigationLinks(getNavigationLinks());
        navigationPanel.setWidth("100%");
        mainPanel.add(navigationPanel);

        initWidget(mainPanel);
    }

    @Override
    public Widget getWidget() {
        return this;
    }

    @Override
    public String getTitleText() {
        //By default, no text to set
        return null;
    }

    @Override
    public void addNavigationLink(GenericHyperlinkInterface navigationLink) {
        if (navigationLink!=null) {
            navigationPanel.add(navigationLink);
            //Custom CSS
            navigationPanel.setStyleName("PageNavigation");
        }
    }

    protected void setNavigationLinks(Collection<GenericHyperlinkInterface> navigationLinks) {
        //clean beforehand
        removeAllNavigationLinks();
        //now fill in with link components
        if (navigationLinks!=null && !navigationLinks.isEmpty()) {
            for (GenericHyperlinkInterface navigationLink : navigationLinks) {
                addNavigationLink(navigationLink);
            }
            //Custom CSS
            if (!navigationLinks.isEmpty()) navigationPanel.setStyleName("PageNavigation");
        }
    }

    @Override
    public void removeAllNavigationLinks() {
        navigationPanel.clean();
        //Custom CSS
        navigationPanel.setStyleName(null);
    }

    @Override
    public Widget asWidget() {
        return this;
    }
    
}
