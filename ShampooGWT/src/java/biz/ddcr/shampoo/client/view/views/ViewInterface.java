/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views;

import biz.ddcr.shampoo.client.view.widgets.FrameWidgetInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import com.mvp4g.client.view.LazyView;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ViewInterface extends LazyView, FrameWidgetInterface {

    //Set the header of this page
    public String getHeaderText();
    //Set CSS specific marker
    public String getHeaderCSSMarker();
    
    //Set the page title
    public String getTitleText();

    //Set the main content of the page
    public GenericPanelInterface buildPageContent();

    //Build the navigation bar (i.e. sub-menu)
    public Collection<GenericHyperlinkInterface> getNavigationLinks();
    //Add links and clean them on demand
    public void addNavigationLink(GenericHyperlinkInterface navigationLink);
    public void removeAllNavigationLinks();

}
