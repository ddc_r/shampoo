/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.archive;

import biz.ddcr.shampoo.client.form.archive.ArchiveForm;
import biz.ddcr.shampoo.client.form.archive.ArchiveForm.ARCHIVE_TAGS;
import biz.ddcr.shampoo.client.form.archive.BroadcastableTrackArchiveForm;
import biz.ddcr.shampoo.client.form.archive.LiveArchiveForm;
import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.form.archive.ReportForm.REPORT_TAGS;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ArchiveListView extends PageView {

    GWTTabPanel tabPanel;
    GWTVerticalPanel archivePanel;
    GWTVerticalPanel reportPanel;
    /**Archives*/
    //The channel that is associated to the archives
    GWTPagedFormDropDownListBox<ChannelForm> archiveChannelComboBox;
    GWTVerticalPanel archiveListPanel;
    GWTFormSortableTable<ArchiveForm, Widget, ARCHIVE_TAGS> archiveList;
    GWTPagedFormDropDownListBox<String> archiveTimezoneList;
    /**Reports*/
    GWTFormSortableTable<ReportForm, Widget, REPORT_TAGS> reportList;
    GWTClickableLabel addReportLink;

    public GenericTabPanelInterface getTabPanel() {
        return tabPanel;
    }
    
    public GenericFormSortableTableInterface<ArchiveForm, Widget, ARCHIVE_TAGS> getArchiveList() {
        return archiveList;
    }
    
    public GenericFormSortableTableInterface<ReportForm, Widget, REPORT_TAGS> getReportList() {
        return reportList;
    }

    public GenericPanelInterface getArchivePanel() {
        return archivePanel;
    }
    public GenericPanelInterface getReportPanel() {
        return reportPanel;
    }
    
    public GenericPanelInterface getArchiveListPanel() {
        return archiveListPanel;
    }

    public GenericDropDownListBoxInterface<ChannelForm> getArchiveChannelComboBox() {
        return archiveChannelComboBox;
    }

    public GenericDropDownListBoxInterface<String> getArchiveTimezoneList() {
        return archiveTimezoneList;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().archive_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "archive";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        tabPanel = new GWTTabPanel();
        mainPanel.add(tabPanel);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    public void buildTabPanel(boolean canBuildArchiveList, ClickHandler archiveClickHandler, boolean canBuildReportList, ClickHandler reportClickHandler, final boolean autoSelectReportTab) {

        tabPanel.clean();
        if (canBuildArchiveList) {
            int archivePanelIndex = 0;
            archivePanel = new GWTVerticalPanel();
            tabPanel.insertAt(archivePanel, I18nTranslator.getInstance().history_items_list(), archivePanelIndex);
            tabPanel.addClickHandler(archiveClickHandler, archivePanelIndex);
        }
        if (canBuildReportList) {
            int reportPanelIndex = canBuildArchiveList ? 1 : 0;
            reportPanel = new GWTVerticalPanel();
            tabPanel.insertAt(reportPanel, I18nTranslator.getInstance().reports_list(), reportPanelIndex);
            tabPanel.addClickHandler(reportClickHandler, reportPanelIndex);
        }
        //Refresh the first available one immediately
        //selectFirstTab() will transparently call the click action associated with the said tab
        if (canBuildReportList && autoSelectReportTab)
            tabPanel.selectLastTab();
        else
            tabPanel.selectFirstTab();

    }
        
    
    //Respond to presenter requests
    public void refreshArchivePanel() {
        archivePanel.clean();
        
        GWTGrid grid1 = new GWTGrid();
        archiveChannelComboBox = new GWTPagedFormDropDownListBox<ChannelForm>();
        grid1.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), archiveChannelComboBox);
        archivePanel.add(grid1);

        archiveListPanel = new GWTVerticalPanel();
        archivePanel.add(archiveListPanel);
        
    }
    public void refreshArchiveListPanel(SortableTableClickHandler listHeaderClickHandler, ChangeHandler listTimezoneChangeHandler) {
        
        archiveList = new GWTFormSortableTable<ArchiveForm, Widget, ARCHIVE_TAGS>();
        //Generate list header
        archiveList.setHeader(
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().time(), ARCHIVE_TAGS.time),
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().type(), ARCHIVE_TAGS.type),
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().programme(), ARCHIVE_TAGS.programme),
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().playlist(), ARCHIVE_TAGS.playlist),
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().duration(), ARCHIVE_TAGS.duration),
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().label()),
                new ColumnHeader<ARCHIVE_TAGS>(I18nTranslator.getInstance().operation())
                );
        archiveList.setRefreshClickHandler(listHeaderClickHandler);

        archiveListPanel.add(archiveList);

        GWTGrid timezoneGrid = new GWTGrid();
        archiveTimezoneList = new GWTPagedFormDropDownListBox<String>();
        archiveTimezoneList.addChangeHandler(listTimezoneChangeHandler);
        timezoneGrid.addRow(
                new GWTLabel(I18nTranslator.getInstance().timezone() + ":"),
                archiveTimezoneList);
        archiveListPanel.add(timezoneGrid);
    }
    
    public void refreshReportPanel(SortableTableClickHandler listHeaderClickHandler) {

        reportPanel.clean();

        reportList = new GWTFormSortableTable<ReportForm, Widget, REPORT_TAGS>();
        //Generate list header
        reportList.setHeader(
                new ColumnHeader<REPORT_TAGS>(I18nTranslator.getInstance().state(), REPORT_TAGS.state),
                new ColumnHeader<REPORT_TAGS>(I18nTranslator.getInstance().channel(), REPORT_TAGS.channel),                
                new ColumnHeader<REPORT_TAGS>(I18nTranslator.getInstance().from(), REPORT_TAGS.from),
                new ColumnHeader<REPORT_TAGS>(I18nTranslator.getInstance().to(), REPORT_TAGS.to),
                new ColumnHeader<REPORT_TAGS>(I18nTranslator.getInstance().operation()));
        reportList.setRefreshClickHandler(listHeaderClickHandler);

        reportPanel.add(reportList);
    }

    public void refreshListTimezoneList(String channelTimezone, String userTimezone) {
        //Clear the list beforehand
        archiveTimezoneList.clean();

        //Generate the list
        if (channelTimezone != null) {
            archiveTimezoneList.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezone + ")",
                    channelTimezone);
        }
        if (userTimezone != null && !userTimezone.equals(channelTimezone)) {
            archiveTimezoneList.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezone + ")",
                    userTimezone);
        }
    }

    public void refreshChannelList(ActionCollection<ChannelForm> channels) {
        //Clear the list beforehand
        archiveChannelComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channels) {

            if (channelEntry.isReadable()) {
                ChannelForm channel = channelEntry.getKey();

                archiveChannelComboBox.addItem(channel.getLabel(), channel);
            }
        }

    }

    public void refreshArchiveList(ActionCollection<? extends ArchiveForm> archives, YesNoClickHandler deleteClickHandler, final YesNoClickHandler deleteAllClickHandler) {

        //Clear the list beforehand
        archiveList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        archiveList.setNextButtonVisible((archives.size() >= archiveList.getMaxVisibleRows()));

        archiveList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<? extends ArchiveForm> archiveEntry : archives) {

            if (archiveEntry.isReadable()) {
                ArchiveForm archive = archiveEntry.getKey();

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //Delete
                if (archiveEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(archive.getRefId().getFullFriendlyID()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                
                GWTLabel typeLabel = new GWTLabel(null);
                if (archive instanceof BroadcastableTrackArchiveForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().track());
                } else if (archive instanceof LiveArchiveForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().live());
                } else {
                    typeLabel.setCaption(I18nTranslator.getInstance().unknown());
                }
                
                archiveList.addRow(
                        archive.getRefId()!=null ? archive.getRefId().getRefID() : archive.toString(),
                        archive,
                        archiveEntry.isDeletable(),
                        new GWTLabel(archive.getRefId().getSchedulingTime().getI18nSyntheticFriendlyString()),
                        typeLabel,
                        new GWTLabel(archive.getProgrammeFriendlyId()),
                        new GWTLabel(archive.getPlaylistFriendlyId()),
                        new GWTLabel(archive.getDuration() != null ? archive.getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown()),
                        new GWTLabel(archive.getContentCaption()),
                        operationPanel);
            }
        }

    }

    public void addDownloadLink(String url, SERIALIZATION_METADATA_FORMAT format, GenericPanelInterface container) {
        GWTClickableLabelDownload downloadLink;
        if (url.indexOf('?')<0) url += '?'; else url += '&';
        //Force download as attachment
        url += "j=true";
        if (format!=null) {
            url=url+"&f="+format.getRawName();
            downloadLink = new GWTClickableLabelDownload(I18nTranslator.getInstance().download()+" "+format.getRawName(), url, false);
        } else {
            downloadLink = new GWTClickableLabelDownload(I18nTranslator.getInstance().download(), url, false);
        }
        container.add(downloadLink);
    }
    
    public void refreshReportList(
            ActionCollection<? extends ReportForm> reports,
            GenericCallbackInterface<String, GenericPanelInterface, Void> downloadHandler,
            YesNoClickHandler deleteClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            ClickHandler channelDisplayClickHandler) {

        //Clear the list beforehand
        reportList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        reportList.setNextButtonVisible((reports.size() >= reportList.getMaxVisibleRows()));

        //Default, uninitialized, values are false by default
        boolean canAtLeastDeleteOneItem = false;

        for (ActionCollectionEntry<? extends ReportForm> reportEntry : reports) {

            if (reportEntry.isReadable()) {
                ReportForm report = reportEntry.getKey();

                GWTLabel stateLabel = new GWTLabel(report.isReady()?//
                        I18nTranslator.getInstance().ready()//
                        :I18nTranslator.getInstance().processing()
                        );
                GWTClickableLabel channelWidget = new GWTClickableLabel(report.getRefId()!=null?//
                        report.getRefId().getChannelId()//
                        :null,//
                        channelDisplayClickHandler);

                GWTLabel fromWidget = new GWTLabel(report.getRefId()!=null && report.getRefId().getFromTime()!=null ?//
                        report.getRefId().getFromTime().getI18nSyntheticFriendlyString()//
                        :null
                        );
                GWTLabel toWidget = new GWTLabel(report.getRefId()!=null && report.getRefId().getToTime()!=null ?//
                        report.getRefId().getToTime().getI18nSyntheticFriendlyString()//
                        :null
                        );
                
                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //One can only remove a report if it's not yet in progress or once it's over
                if (report.isReady() &&//
                        reportEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(report.getFriendlyCaption()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                    canAtLeastDeleteOneItem = true;
                }
                if (report.isReady() &&//
                        report.getDataDownloadURL()!=null &&//
                        reportEntry.isReadable()) {
                    //Anonymous function that simulates on-th-fly binding fo links requiring Presenter operations
                    GenericPanelInterface myDownloadWidget = new GWTFlowPanel();
                    downloadHandler.onCallBack(report.getDataDownloadURL(), myDownloadWidget, null);
                    operationPanel.add(myDownloadWidget);
                }

                //the header must be built before adding a row since this latter method check its presence
                final boolean _canAtLeastDeleteOneItem = canAtLeastDeleteOneItem;
                reportList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

                    @Override
                    public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {

                        //Delete checked
                        if (_canAtLeastDeleteOneItem) {
                            GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                            deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                            if (deleteAllClickHandler != null) {
                                deleteLink.addClickHandler(deleteAllClickHandler);
                            }
                            captionContainer.add(deleteLink);
                        }
                    }
                });

                reportList.addRow(report,
                        report.isReady() && reportEntry.isDeletable(),
                        stateLabel,
                        channelWidget,
                        fromWidget,
                        toWidget,
                        operationPanel);
            }
        }

    }
    
    public void refreshAddReportLink(ClickHandler clickHandler) {
        //Generate a report
        addReportLink = new GWTClickableLabelReport(I18nTranslator.getInstance().add_report());
        if (clickHandler != null) {
            addReportLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addReportLink);
    }
    
}
