/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.archive;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 *
 */
public class ReportAddView extends PageView {

    //Reporting properties widgets
    //Channel
    private GWTPagedFormDropDownListBox<ChannelForm> channelComboBox;
    //Sample dates
    private GWTYearMonthDaySelector fromTimeSelector;
    private GWTYearMonthDaySelector toTimeSelector;
    //Misc. view features
    private GWTLabel timezoneLabel;
    private GWTButton submit;
    private GWTClickableLabel cancel;
    
    public GenericDropDownListBoxInterface<ChannelForm> getChannelComboBox() {
        return channelComboBox;
    }
    
    public GenericLabelInterface getTimezoneLabel() {
        return timezoneLabel;
    }
    
    public GenericYearMonthDaySelectorInterface getFromDateSelector() {
        return fromTimeSelector;
    }
    
    public GenericYearMonthDaySelectorInterface getToDateSelector() {
        return toTimeSelector;
    }
    
    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }
    
    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }
    
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().add_report();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "report";
    }
    
    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().archive_heading();
    }
    
    @Override
    public GWTVerticalPanel buildPageContent() {
        
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();
        
        GWTGrid channelGrid = new GWTGrid();
        channelComboBox = new GWTPagedFormDropDownListBox<ChannelForm>();
        channelGrid.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelComboBox);
        mainPanel.add(channelGrid);
        
        GWTGrid dateGrid = new GWTGrid();
        fromTimeSelector = new GWTYearMonthDaySelector();
        toTimeSelector = new GWTYearMonthDaySelector();
        timezoneLabel = new GWTLabel(null);
        dateGrid.addRow(new GWTLabel(I18nTranslator.getInstance().from() + ":"), fromTimeSelector, new GWTLabel(I18nTranslator.getInstance().to() + ":"), toTimeSelector);
        dateGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneLabel);
        mainPanel.add(dateGrid);
        
        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().ok());
        mainPanel.add(submit);
        
        return mainPanel;
    }
    
    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);
        
        return links;
    }

    //Respond to presenter requests
    public void refreshChannelList(ActionCollection<ChannelForm> channelEntries) {
        channelComboBox.clean();
        if (channelEntries != null) {
            for (ActionCollectionEntry<ChannelForm> channelEntry : channelEntries) {
                if (channelEntry.isEditable()) {
                    ChannelForm channelForm = channelEntry.getKey();
                    channelComboBox.addItem(channelForm.getLabel(), channelForm);
                }
            }
        }
    }
    
    public void refreshTimezone() {
        ChannelForm selectedChannelForm = channelComboBox.getSelectedValue();
        timezoneLabel.setCaption(selectedChannelForm != null ? selectedChannelForm.getTimezone() : null);        
    }
    
}
