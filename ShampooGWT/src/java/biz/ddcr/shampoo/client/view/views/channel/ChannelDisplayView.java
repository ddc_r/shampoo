/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.channel;

import biz.ddcr.shampoo.client.form.channel.STREAMER_ACTION;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;

/**
 *
 * @author okay_awright
 *
 */
public class ChannelDisplayView extends GenericFormPageView {

    //Channel widgets
    private GWTLabel labelLabel;
    private GWTTruncatedLabel descriptionLabel;
    private GWTLabel timezoneLabel;
    private GWTLabel maxDailyRequestPerUserLabel;
    private GWTGrid seatNumberGrid;
    //Streamer status
    private GWTStreamingStatusBox streamerStatusWidget;
    //Streamer protection key
    private GWTLabel streamerProtectionKeyLabel;
    //Streamer metadata
    private GWTDisclosurePanel streamerMetadataPanel;
    //Programmes widgets
    private GWTPagedSimpleLayer programmesGrid;
    //Rights widgets
    private GWTPagedTree rightsTree;
    //Misc data
    private GWTClickableLabel urlLabel;
    private GWTLabel openLabel;

    public GenericLabelInterface getOpenLabel() {
        return openLabel;
    }

    public GenericLabelInterface getDescriptionLabel() {
        return descriptionLabel;
    }

    public GenericLabelInterface getLabelLabel() {
        return labelLabel;
    }

    public GenericLabelInterface getMaxDailyRequestPerUserLabel() {
        return maxDailyRequestPerUserLabel;
    }

    public GenericLabelInterface getTimezoneLabel() {
        return timezoneLabel;
    }

    public GenericSimpleLayerInterface getProgrammesGrid() {
        return programmesGrid;
    }

    public GenericPanelInterface getStreamerMetadataPanel() {
        return streamerMetadataPanel;
    }

    public GenericStatusBoxInterface<String, STATUS> getStreamerStatusWidget() {
        return streamerStatusWidget;
    }

    public GenericTreeInterface getRightsTree() {
        return rightsTree;
    }

    public GenericHyperlinkInterface getURLLabel() {
        return urlLabel;
    }

    public GenericLabelInterface getStreamerProtectionKeyLabel() {
        return streamerProtectionKeyLabel;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_channel();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "channel";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().channel_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //Channel widgets
        GWTVerticalPanel channelPanel = new GWTVerticalPanel();

        //Streamer info
        streamerMetadataPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().technical_info());
        channelPanel.add(streamerMetadataPanel);

        //Basic info
        GWTGrid grid = new GWTGrid();

        labelLabel = new GWTLabel(null);
        streamerStatusWidget = new GWTStreamingStatusBox();
        streamerProtectionKeyLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelLabel, streamerStatusWidget, streamerProtectionKeyLabel);

        descriptionLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionLabel);

        urlLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().url() + ":"), urlLabel);

        channelPanel.add(grid);

        seatNumberGrid = new GWTGrid();

        channelPanel.add(seatNumberGrid);

        GWTGrid grid2 = new GWTGrid();

        openLabel = new GWTLabel(I18nTranslator.getInstance().channel_closed_for_listener_registration()); //Fill in some sensible default value
        grid2.addRow(openLabel);

        channelPanel.add(grid2);

        //Programme widgets
        GWTDisclosurePanel programmePanel = new GWTDisclosurePanel(I18nTranslator.getInstance().programmes_list());
        programmesGrid = new GWTPagedSimpleLayer();
        programmePanel.add(programmesGrid);

        channelPanel.add(programmePanel);

        //Right widgets
        GWTDisclosurePanel rightPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().rights_list());
        rightsTree = new GWTPagedTree();
        rightPanel.add(rightsTree);

        channelPanel.add(rightPanel);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        timezoneLabel = new GWTLabel(null);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneLabel);

        maxDailyRequestPerUserLabel = new GWTLabel(null);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().max_daily_requests_per_user() + ":"), maxDailyRequestPerUserLabel);

        miscOptionsPanel.add(miscOptionsGrid);

        channelPanel.add(miscOptionsPanel);

        return channelPanel;
    }

    //Respond to presenter requests
    public void refreshExistingRights(ActionCollection<RightForm> rightForms) {
        //clear the list beforehand
        rightsTree.clean();

        //generate the list
        for (ActionCollectionEntry<RightForm> rightEntry : rightForms) {
            if (rightEntry.isReadable()) {
                addNewRight(rightEntry.getKey());
            }
        }
    }

    public void addNewRight(RightForm rightForm) {

        //Add the new right to the list
        Long newBranchId = rightsTree.getItemByName(rightForm.getRole().getI18nFriendlyString());
        if (newBranchId == null) {
            newBranchId = rightsTree.addItemWithName(rightForm.getRole().getI18nFriendlyString(), new GWTLabel(rightForm.getRole().getI18nFriendlyString()));
        }
        rightsTree.addItem(new GWTLabel(rightForm.getRestrictedUserId()), newBranchId);
    }

    public void refreshExistingProgrammes(ActionCollection<String> programmeLabels) {
        //clear the list beforehand
        programmesGrid.clean();

        //generate the list
        for (ActionCollectionEntry<String> programmeLabelEntry : programmeLabels) {
            if (programmeLabelEntry.isReadable()) {
                addNewProgramme(programmeLabelEntry.getItem());
            }
        }
    }

    public void addNewProgramme(String newProgrammeLabel) {
        //Add the new programme to the list
        programmesGrid.addRow(new GWTLabel(newProgrammeLabel));
    }

    public void initializePrivateWebserviceModuleStatus(String id, GenericStatusChangeHandler<String, STATUS> streamerStatusChangeHandler) {
        streamerStatusWidget.setID(id);
        streamerStatusWidget.setStatusChangeHandler(streamerStatusChangeHandler);
    }

    public void refreshStreamerStatus(GenericStatusBoxInterface<String, STATUS> statusWidget, ActionCollectionEntry<StreamerModule> statusEntry) {
        if (statusEntry != null && (statusEntry.isReadable() || statusEntry.hasAction(STREAMER_ACTION.LISTEN))) {
            if (statusEntry.getItem()!=null) {
                if (statusEntry.getItem().isStreaming()!=null)
                    statusWidget.setStatus(statusEntry.getItem().isStreaming() ? STATUS.onair : STATUS.offair);
                else
                    statusWidget.setStatus(STATUS.reserved);
            } else 
                statusWidget.setStatus(STATUS.free);
        } else {
            statusWidget.clean();
        }
    }

    public void refreshStreamerId(ActionCollectionEntry<String> idEntry) {
        streamerProtectionKeyLabel.clean();
        streamerProtectionKeyLabel.setVisible(idEntry != null && idEntry.isReadable());
        if (idEntry != null && idEntry.isReadable()) {
            streamerProtectionKeyLabel.setCaption(I18nTranslator.getInstance().streamer_seat_protection_key() + ": " + (idEntry.getItem() != null ? idEntry.getItem() : I18nTranslator.getInstance().none()));
        }
    }

    public void refreshSeatNumber(ActionCollectionEntry<Long> seatNumberEntry) {
        //The eventBus will be bound using specificFormBind()
        seatNumberGrid.clean();
        seatNumberGrid.setVisible(seatNumberEntry != null && seatNumberEntry.isReadable());        
        if (seatNumberEntry != null && seatNumberEntry.isReadable()) {
            Long seatNumber = seatNumberEntry.getItem();
            seatNumberGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_seat_number() + ": "), new GWTLabel(seatNumber == null ? I18nTranslator.getInstance().streamer_automatic_seat_allocation() : seatNumber.toString()));
        }
    }

    public void refreshStreamerMetadata(ActionCollectionEntry<? extends StreamerModule> streamerEntry) {
        //The eventBus will be bound using specificFormBind()
        final StreamerModule container = streamerEntry != null ? streamerEntry.getItem() : null;
        streamerMetadataPanel.setVisible(container != null && streamerEntry.isReadable());
        streamerMetadataPanel.clean();
        if (container != null && streamerEntry.isReadable()) {
            streamerMetadataPanel.add(new GWTLabel(container.getI18nFriendlyName()));
        }
    }
}
