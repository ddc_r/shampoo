/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.channel;

import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class ChannelEditView extends PageView {

    //Channel widgets
    private GWTLabel labelLabel;
    private GWTExtendedTextBox descriptionTextBox;
    private GWTPagedComboBox timezoneComboBox;
    private GWTNullableFormDropDownListBox<Integer> maxDailyRequestPerUserSelector;
    //Misc data
    private GWTTextBox urlTextBox;
    private GWTTextBox tagTextBox;
    private GWTCheckBox openCheckBox;
    //Programmes widgets
    private GWTPagedFormDropDownListBox<ProgrammeForm> programmeComboBox;
    private GWTClickableLabel addNewProgrammeButton;
    private GWTPagedFormTable<ProgrammeForm, Widget> programmesListBox;
    //Rights widgets
    private GWTPagedFormDropDownListBox<Role> roleRightComboBox;
    private GWTPagedFormDropDownListBox<RestrictedUserForm> userRightComboBox;
    private GWTClickableLabel addNewRightButton;
    private GWTPagedFormTable<RightForm, Widget> rightsListBox;
    //Streamer widget
    private GWTGrid streamerGrid;
    //private GWTPagedFormDropDownListBox<SERIALIZATION_METADATA_FORMAT> streamerMetadataFormatDropDownListBox;
    /*private GWTGrid streamerPrivateKeyOptionsGrid;
    private GWTTextBox streamerPrivateKeyTextBox;*/
    /*private GWTNumericBox streamerQueueMinItems;
    //Queuing lives?
    private GWTCheckBox streamerQueueableLive;
    private GWTGrid streamerQueueableLiveOptions;
    private GWTTextBox streamerQueueableLiveURIPattern;
    private GWTCheckBox streamerQueueableLiveChunkDurationCheckBox;
    private GWTPagedFormDropDownListBox<Short> streamerQueueableLiveChunkDurationSelector;
    //Queuing blanks?
    private GWTCheckBox streamerQueueableBlank;
    private GWTGrid streamerQueueableBlankOptions;
    private GWTTextBox streamerQueueableBlankURIPattern;
    private GWTCheckBox streamerQueueableBlankChunkDurationCheckBox;
    private GWTPagedFormDropDownListBox<Short> streamerQueueableBlankChunkDurationSelector;
    
    private GWTTextBox streamURLTextBox;*/
    //private GWTCheckBox streamerEnableEmergencyItems;
    private GWTGrid streamerSeatOptionsGrid;
    private GWTNumericBox streamerSeatNumericBox;
    private GWTButton streamerDiscoverFreeSeatButton;
    private GWTCheckBox streamerAutoSeatAllocationCheckBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericTextBoxInterface getDescriptionTextBox() {
        return descriptionTextBox;
    }

    public GenericLabelInterface getLabelLabel() {
        return labelLabel;
    }

    public GenericComboBoxInterface getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericDropDownListBoxInterface<ProgrammeForm> getProgrammeComboBox() {
        return programmeComboBox;
    }

    public GenericFormTableInterface<ProgrammeForm, Widget> getProgrammesListBox() {
        return programmesListBox;
    }

    public GenericDropDownListBoxInterface<Role> getRoleRightComboBox() {
        return roleRightComboBox;
    }

    public GenericFormTableInterface<RightForm, Widget> getRightsListBox() {
        return rightsListBox;
    }

    public GenericDropDownListBoxInterface<RestrictedUserForm> getUserRightComboBox() {
        return userRightComboBox;
    }

    public GenericDropDownListBoxInterface<Integer> getMaxDailyRequestPerUserSelector() {
        return maxDailyRequestPerUserSelector;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getAddNewRightButton() {
        return addNewRightButton;
    }

    public GenericHyperlinkInterface getAddNewProgrammeButton() {
        return addNewProgrammeButton;
    }

    public GenericGridInterface getStreamerGrid() {
        return streamerGrid;
    }
    
    /*public GenericTextBoxInterface getStreamerPrivateKeyTextBox() {
        return streamerPrivateKeyTextBox;
    }*/

    /*public GenericDropDownListBoxInterface<SERIALIZATION_METADATA_FORMAT> getStreamerMetadataFormatDropDownListBox() {
        return streamerMetadataFormatDropDownListBox;
    }

    public GenericNumericBoxInterface<Integer> getStreamerQueueMinItems() {
        return streamerQueueMinItems;
    }*/

    /*public GenericCheckBoxInterface getStreamerEnableEmergencyItems() {
        return streamerEnableEmergencyItems;
    }*/

    /*public GenericCheckBoxInterface getStreamerQueueableLive() {
        return streamerQueueableLive;
    }

    public GenericCheckBoxInterface getStreamerQueueableBlank() {
        return streamerQueueableBlank;
    }*/

    public GenericTextBoxInterface getURLTextBox() {
        return urlTextBox;
    }
    public GenericTextBoxInterface getTagTextBox() {
        return tagTextBox;
    }
    public GenericCheckBoxInterface getOpenCheckBox() {
        return openCheckBox;
    }

    public GenericCheckBoxInterface getStreamerAutoSeatAllocationCheckBox() {
        return streamerAutoSeatAllocationCheckBox;
    }

    public GenericNumericBoxInterface<Integer> getStreamerSeatNumericBox() {
        return streamerSeatNumericBox;
    }

    public GenericHyperlinkInterface getStreamerDiscoverFreeSeatButton() {
        return streamerDiscoverFreeSeatButton;
    }

    /*public GenericCheckBoxInterface getStreamerQueueableBlankChunkDurationCheckBox() {
        return streamerQueueableBlankChunkDurationCheckBox;
    }

    public GenericDropDownListBoxInterface<Short> getStreamerQueueableBlankChunkDurationSelector() {
        return streamerQueueableBlankChunkDurationSelector;
    }

    public GenericTextBoxInterface getStreamerQueueableBlankURIPattern() {
        return streamerQueueableBlankURIPattern;
    }

    public GenericCheckBoxInterface getStreamerQueueableLiveChunkDurationCheckBox() {
        return streamerQueueableLiveChunkDurationCheckBox;
    }

    public GenericDropDownListBoxInterface<Short> getStreamerQueueableLiveChunkDurationSelector() {
        return streamerQueueableLiveChunkDurationSelector;
    }

    public GenericTextBoxInterface getStreamerQueueableLiveURIPattern() {
        return streamerQueueableLiveURIPattern;
    }

    public GenericTextBoxInterface getStreamURLTextBox() {
        return streamURLTextBox;
    }*/
    
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().edit_channel();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "channel";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().channel_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Channel widgets
        GWTVerticalPanel channelPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        labelLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelLabel);

        descriptionTextBox = new GWTExtendedTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionTextBox);

        channelPanel.add(grid);

        //Programme widgets
        GWTVerticalPanel programmePanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel programmeLabel = new GWTLabel(I18nTranslator.getInstance().programmes_list());
        programmePanel.add(programmeLabel);

        GWTVerticalPanel programmeContainerPanel = new GWTVerticalPanel();

        GWTGrid newProgrammeEntryPanel = new GWTGrid();

        GWTLabel programmeComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().programme());
        programmeComboBox = new GWTPagedFormDropDownListBox<ProgrammeForm>();

        addNewProgrammeButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newProgrammeEntryPanel.addRow(programmeComboBoxCaption);
        newProgrammeEntryPanel.addRow(
                programmeComboBox,
                addNewProgrammeButton);
        programmeContainerPanel.add(newProgrammeEntryPanel);

        programmesListBox = new GWTPagedFormTable<ProgrammeForm, Widget>();
        programmeContainerPanel.add(programmesListBox);
        programmePanel.add(programmeContainerPanel);

        channelPanel.add(programmePanel);

        //Right widgets
        GWTVerticalPanel rightPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel rightLabel = new GWTLabel(I18nTranslator.getInstance().rights_list());
        rightPanel.add(rightLabel);

        GWTVerticalPanel containerPanel = new GWTVerticalPanel();

        GWTGrid newEntryPanel = new GWTGrid();

        GWTLabel firstComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().role());
        roleRightComboBox = new GWTPagedFormDropDownListBox<Role>();

        GWTLabel secondComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().restricted_user());
        userRightComboBox = new GWTPagedFormDropDownListBox<RestrictedUserForm>();

        addNewRightButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newEntryPanel.addRow(
                firstComboBoxCaption,
                secondComboBoxCaption);
        newEntryPanel.addRow(
                roleRightComboBox,
                userRightComboBox,
                addNewRightButton);
        containerPanel.add(newEntryPanel);

        rightsListBox = new GWTPagedFormTable<RightForm, Widget>();
        rightsListBox.setHeader(firstComboBoxCaption.getCaption(), secondComboBoxCaption.getCaption());
        containerPanel.add(rightsListBox);
        rightPanel.add(containerPanel);

        channelPanel.add(rightPanel);

        //Streamer options
        streamerGrid = new GWTGrid();

        /*streamerMetadataFormatDropDownListBox = new GWTPagedFormDropDownListBox<SERIALIZATION_METADATA_FORMAT>();
        streamerGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_metadata_format() + ":"), streamerMetadataFormatDropDownListBox);*/

        /*streamerPrivateKeyOptionsGrid = new GWTGrid();
        streamerPrivateKeyTextBox = new GWTTextBox();
        streamerPrivateKeyOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_private_key() + ":"), streamerPrivateKeyTextBox);
        streamerGrid.addRow(streamerPrivateKeyOptionsGrid);*/
        
        /*streamerQueueMinItems = new GWTNumericBox(1, 1, 255);
        streamerGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_queue_minimal_number_of_items_queue() + ":"), streamerQueueMinItems);

        streamerQueueableLive = new GWTCheckBox(false); //Let it false by default; most likely the default streamer will be Liquidsoap
        streamerGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_is_live_queueable() + ":"), streamerQueueableLive);

        streamerQueueableLiveOptions = new GWTGrid();
        streamerQueueableLiveChunkDurationCheckBox = new GWTCheckBox(true); //true by default
        streamerQueueableLiveChunkDurationSelector = new GWTPagedFormDropDownListBox<Short>();
        streamerQueueableLiveOptions.addRow(streamerQueueableLiveChunkDurationCheckBox, new GWTLabel(I18nTranslator.getInstance().split_into_chunks_of()), streamerQueueableLiveChunkDurationSelector, new GWTLabel(I18nTranslator.getInstance().minutes()));
        streamerQueueableLiveURIPattern = new GWTTextBox();
        streamerQueueableLiveOptions.addRow(new GWTLabel(I18nTranslator.getInstance().uri_scheme()+":"), streamerQueueableLiveURIPattern);
        streamerGrid.addRow(streamerQueueableLiveOptions);

        streamerQueueableBlank = new GWTCheckBox(true); //Let it true by default; most likely the default streamer will be Liquidsoap
        streamerGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_is_blank_queueable() + ":"), streamerQueueableBlank);        

        streamerQueueableBlankOptions = new GWTGrid();
        streamerQueueableBlankChunkDurationCheckBox = new GWTCheckBox(true); //true by default
        streamerQueueableBlankChunkDurationSelector = new GWTPagedFormDropDownListBox<Short>();
        streamerQueueableBlankOptions.addRow(streamerQueueableBlankChunkDurationCheckBox, new GWTLabel(I18nTranslator.getInstance().split_into_chunks_of()), streamerQueueableBlankChunkDurationSelector, new GWTLabel(I18nTranslator.getInstance().minutes()));
        streamerQueueableBlankURIPattern = new GWTTextBox();
        streamerQueueableBlankOptions.addRow(new GWTLabel(I18nTranslator.getInstance().uri_scheme()+":"), streamerQueueableBlankURIPattern);
        streamerGrid.addRow(streamerQueueableBlankOptions);

        streamURLTextBox = new GWTTextBox();
        streamerGrid.addRow(new GWTLabel(I18nTranslator.getInstance().stream_uri()+":"), streamURLTextBox);*/
        
        /*streamerEnableEmergencyItems = new GWTCheckBox(false);
        streamerGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_are_emergency_files_on() + ":"), streamerEnableEmergencyItems);*/

        streamerSeatOptionsGrid = new GWTGrid();
        streamerSeatNumericBox = new GWTNumericBox(1,1,Integer.MAX_VALUE); //Only positive values accepted
        streamerDiscoverFreeSeatButton = new GWTButton(I18nTranslator.getInstance().streamer_find_first_available_seat());
        streamerAutoSeatAllocationCheckBox = new GWTCheckBox(false); //No auto allocation by default
        streamerAutoSeatAllocationCheckBox.setCaption(I18nTranslator.getInstance().streamer_automatic_seat_allocation());
        streamerSeatOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().streamer_seat_number() + ":"), streamerSeatNumericBox, streamerDiscoverFreeSeatButton);
        streamerSeatOptionsGrid.addRow(streamerAutoSeatAllocationCheckBox);
        streamerGrid.addRow(streamerSeatOptionsGrid);
               
        channelPanel.add(streamerGrid);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTVerticalPanel dummyPanel = new GWTVerticalPanel();
        GWTGrid miscOptionsGrid = new GWTGrid();

        timezoneComboBox = new GWTPagedComboBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);

        maxDailyRequestPerUserSelector = new GWTNullableFormDropDownListBox<Integer>();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().max_daily_requests_per_user() + ":"), maxDailyRequestPerUserSelector);

        urlTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().url() + ":"), urlTextBox);
        tagTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().tag() + ":"), tagTextBox);
        openCheckBox = new GWTCheckBox(false);//Fill in some sensible default value
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().listener_self_registration() + ":"), openCheckBox);
        
        dummyPanel.add(miscOptionsGrid);

        miscOptionsPanel.add(dummyPanel);

        channelPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        channelPanel.add(submit);

        return channelPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests
    
    public void toggleStreamerSeatPanel() {
        streamerSeatNumericBox.setEnabled(!streamerAutoSeatAllocationCheckBox.isChecked());
        streamerDiscoverFreeSeatButton.setEnabled(!streamerAutoSeatAllocationCheckBox.isChecked());
    }
    
    public void refreshRestrictedUserRightList(ActionCollection<RestrictedUserForm> users) {
        //Clear the list beforehand
        userRightComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<RestrictedUserForm> userEntry : users) {
            if (userEntry.isEditable()) {
                userRightComboBox.addItem(userEntry.getKey().getUsername(), userEntry.getKey());
            }
        }
    }

    public void refreshTimezoneList(Collection<String> timezones) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        for (String timezone : timezones) {
            timezoneComboBox.addItem(timezone, timezone);
        }

    }

    public void refreshMaxDailyRequestsPerUserList() {
        //Clear the list beforehand
        maxDailyRequestPerUserSelector.clean();

        //Generate the list
        maxDailyRequestPerUserSelector.addItem("0", 0);
        maxDailyRequestPerUserSelector.addItem("1", 1);
        maxDailyRequestPerUserSelector.addItem("2", 2);
        maxDailyRequestPerUserSelector.addItem("3", 3);
        maxDailyRequestPerUserSelector.addItem("4", 4);
        maxDailyRequestPerUserSelector.addItem("5", 5);
        maxDailyRequestPerUserSelector.addItem("10", 10);
        maxDailyRequestPerUserSelector.addItem("20", 20);
        maxDailyRequestPerUserSelector.addItem("50", 50);
        maxDailyRequestPerUserSelector.addItem(I18nTranslator.getInstance().infinite(), null);

    }

    /*public void refreshMaxQueueableLiveChunkDurationList() {
        //Clear the list beforehand
        streamerQueueableLiveChunkDurationSelector.clean();

        //Generate the list
        streamerQueueableLiveChunkDurationSelector.addItem("1", (short)1);
        streamerQueueableLiveChunkDurationSelector.addItem("2", (short)2);
        streamerQueueableLiveChunkDurationSelector.addItem("3", (short)3);
        streamerQueueableLiveChunkDurationSelector.addItem("4", (short)4);
        streamerQueueableLiveChunkDurationSelector.addItem("5", (short)5);
        streamerQueueableLiveChunkDurationSelector.addItem("10", (short)10);
        streamerQueueableLiveChunkDurationSelector.addItem("20", (short)20);
        streamerQueueableLiveChunkDurationSelector.addItem("30", (short)30);
        streamerQueueableLiveChunkDurationSelector.addItem("60", (short)60);
        streamerQueueableLiveChunkDurationSelector.addItem("120", (short)120);
        streamerQueueableLiveChunkDurationSelector.addItem("360", (short)360);

    }

    public void refreshMaxQueueableBlankChunkDurationList() {
        //Clear the list beforehand
        streamerQueueableBlankChunkDurationSelector.clean();

        //Generate the list
        streamerQueueableBlankChunkDurationSelector.addItem("1", (short)1);
        streamerQueueableBlankChunkDurationSelector.addItem("2", (short)2);
        streamerQueueableBlankChunkDurationSelector.addItem("3", (short)3);
        streamerQueueableBlankChunkDurationSelector.addItem("4", (short)4);
        streamerQueueableBlankChunkDurationSelector.addItem("5", (short)5);
        streamerQueueableBlankChunkDurationSelector.addItem("10", (short)10);
        streamerQueueableBlankChunkDurationSelector.addItem("20", (short)20);
        streamerQueueableBlankChunkDurationSelector.addItem("30", (short)30);
        streamerQueueableBlankChunkDurationSelector.addItem("60", (short)60);
        streamerQueueableBlankChunkDurationSelector.addItem("120", (short)120);
        streamerQueueableBlankChunkDurationSelector.addItem("360", (short)360);

    }

    public void setMaxQueueableLiveChunkDurationValue(Short value) {
        if (value != null) {
            switch (value) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 10:
                case 20:
                case 30:
                case 60:
                case 120:
                case 360:
                    streamerQueueableBlankChunkDurationSelector.setSelectedValue(value);
                    break;
                default:
                    // default value
                    streamerQueueableBlankChunkDurationSelector.setSelectedValue((short)1);
            }
        } else {
            streamerQueueableBlankChunkDurationSelector.setSelectedValue((short)1);
        }
    }

    public void setMaxQueueableBlankChunkDurationValue(Short value) {
        if (value != null) {
            switch (value) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 10:
                case 20:
                case 30:
                case 60:
                case 120:
                case 360:
                    streamerQueueableBlankChunkDurationSelector.setSelectedValue(value);
                    break;
                default:
                    // default value
                    streamerQueueableBlankChunkDurationSelector.setSelectedValue((short)1);
            }
        } else {
            streamerQueueableBlankChunkDurationSelector.setSelectedValue((short)1);
        }
    }*/

    public void setDefaultMaxDailyRequestsPerUserValue(Integer value) {
        if (value != null) {
            switch (value) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 10:
                case 20:
                case 50:
                case 0:
                    maxDailyRequestPerUserSelector.setSelectedValue(value);
                    break;
                default:
                    // default value
                    maxDailyRequestPerUserSelector.setSelectedValue(1);
            }
        } else {
            maxDailyRequestPerUserSelector.setSelectedValue(null);
        }
    }

    /*public void refreshStreamerMetadataFormatList(SERIALIZATION_METADATA_FORMAT[] formats) {
        //Clear the list beforehand
        streamerMetadataFormatDropDownListBox.clean();

        //Generate the list
        for (SERIALIZATION_METADATA_FORMAT format : formats) {
            streamerMetadataFormatDropDownListBox.addItem(format.getI18nFriendlyString(), format);
        }
    }*/

    public void refreshRoleRightList(Role[] roles) {
        //Clear the list beforehand
        roleRightComboBox.clean();

        //Generate the list
        for (Role role : roles) {
            roleRightComboBox.addItem(role.getI18nFriendlyString(), role);
        }
    }

    public void refreshProgrammeList(ActionCollection<ProgrammeForm> programmes) {
        //Clear the list beforehand
        programmeComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {
            if (programmeEntry.isEditable()) {
                programmeComboBox.addItem(programmeEntry.getKey().getLabel(), programmeEntry.getKey());
            }
        }
    }

    public void refreshExistingRights(ActionCollection<RightForm> rightForms, ClickHandler deleteClickHandler, ClickHandler displayRestrictedUserClickHandler) {
        //clear the list beforehand
        rightsListBox.clean();

        //generate the list
        for (ActionCollectionEntry<RightForm> rightEntry : rightForms) {
            if (rightEntry.isEditable()) {
                addNewRight(rightEntry.getKey(), deleteClickHandler, displayRestrictedUserClickHandler);
            }
        }
    }

    public void addNewRight(RightForm right, ClickHandler deleteClickHandler, ClickHandler displayRestrictedUserClickHandler) {

        //Create the right entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new right to the list
        rightsListBox.addRow(right, false, new GWTLabel(right.getRole().getI18nFriendlyString()), new GWTClickableLabel(right.getRestrictedUserId(), displayRestrictedUserClickHandler), operationPanel);
    }

    public void refreshExistingProgrammes(ActionCollection<ProgrammeForm> programmes, ClickHandler deleteClickHandler, ClickHandler displayProgrammeClickHandler) {
        //clear the list beforehand
        programmesListBox.clean();

        //generate the list
        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {
            if (programmeEntry.isEditable()) {
                addNewProgramme(programmeEntry.getKey(), deleteClickHandler, displayProgrammeClickHandler);
            }
        }
    }

    public void addNewProgramme(ProgrammeForm newProgramme, ClickHandler deleteClickHandler, ClickHandler displayProgrammeClickHandler) {

        //Create the programme entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new programme to the list
        programmesListBox.addRow(newProgramme, false, new GWTClickableLabel(newProgramme.getLabel(), displayProgrammeClickHandler), operationPanel);
    }

    /*public void toggleQueueableLiveOptionsChange() {
        streamerQueueableLiveOptions.setVisible(streamerQueueableLive.isChecked());
    }
    public void toggleQueueableBlankOptionsChange() {
        streamerQueueableBlankOptions.setVisible(streamerQueueableBlank.isChecked());
    }
    public void toggleQueueableLiveChunkDurationChange() {
        streamerQueueableLiveChunkDurationSelector.setEnabled(streamerQueueableLiveChunkDurationCheckBox.isChecked());
    }
    public void toggleQueueableBlankChunkDurationChange() {
        streamerQueueableBlankChunkDurationSelector.setEnabled(streamerQueueableBlankChunkDurationCheckBox.isChecked());
    }*/
}
