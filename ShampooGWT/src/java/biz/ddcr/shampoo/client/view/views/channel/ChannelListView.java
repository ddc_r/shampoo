/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.channel.STREAMER_ACTION;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ChannelListView extends PageView {

    GWTVerticalPanel channelPanel;
    GWTClickableLabel addChannelLink;
    GWTFormSortableTable<ChannelForm, Widget, CHANNEL_TAGS> channelList;

    public GenericFormSortableTableInterface<ChannelForm, Widget, CHANNEL_TAGS> getChannelList() {
        return channelList;
    }

    public GenericHyperlinkInterface getAddChannelLink() {
        return addChannelLink;
    }

    public GenericPanelInterface getChannelPanel() {
        return channelPanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().channel_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "channel";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        channelPanel = new GWTVerticalPanel();

        return channelPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void refreshChannelPanel(SortableTableClickHandler listHeaderClickHandler) {
        channelList = new GWTFormSortableTable<ChannelForm, Widget, CHANNEL_TAGS>();
        //Generate list header
        channelList.setHeader(
                new ColumnHeader<CHANNEL_TAGS>(I18nTranslator.getInstance().label(), CHANNEL_TAGS.label),
                new ColumnHeader<CHANNEL_TAGS>(I18nTranslator.getInstance().description(), CHANNEL_TAGS.description),
                new ColumnHeader<CHANNEL_TAGS>(I18nTranslator.getInstance().programmes(), CHANNEL_TAGS.programmes),
                new ColumnHeader<CHANNEL_TAGS>(I18nTranslator.getInstance().rights(), CHANNEL_TAGS.rights),
                new ColumnHeader<CHANNEL_TAGS>(I18nTranslator.getInstance().online_status()),
                new ColumnHeader<CHANNEL_TAGS>(I18nTranslator.getInstance().operation())
                );
        channelList.setRefreshClickHandler(listHeaderClickHandler);

        channelPanel.add(channelList);
    }

    public void refreshAddChannelLink(ClickHandler clickHandler) {
        //Add a regular user
        addChannelLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_channel());
        if (clickHandler != null) {
            addChannelLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addChannelLink);
    }

    public void refreshStreamerStatus(GenericStatusBoxInterface<String,STATUS> statusWidget, ActionCollectionEntry<StreamerModule> statusEntry) {
        if (statusEntry!=null && (statusEntry.isReadable() || statusEntry.hasAction(STREAMER_ACTION.LISTEN))) {
            if (statusEntry.getItem()!=null) {
                if (statusEntry.getItem().isStreaming()!=null)
                    statusWidget.setStatus(statusEntry.getItem().isStreaming() ? STATUS.onair : STATUS.offair);
                else
                    statusWidget.setStatus(STATUS.reserved);
            } else 
                statusWidget.setStatus(STATUS.free);
        } else {
            statusWidget.clean();
        }
    }

    public void refreshChannelList(
            ActionCollection<ChannelForm> channels,
            final ClickHandler editClickHandler,
            final YesNoClickHandler deleteClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            ClickHandler channelDisplayClickHandler,
            GenericStatusChangeHandler<String,STATUS> streamerStatusChangeHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler,
            final SerializableItemSelectionHandler<String> restrictedUserDisplaySelectionHandler) {

        //Clear the list beforehand
        channelList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        channelList.setNextButtonVisible((channels.size() >= channelList.getMaxVisibleRows()));

        channelList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<ChannelForm> channelEntry : channels) {

            if (channelEntry.isReadable()) {
                ChannelForm channel = channelEntry.getKey();

                GWTPagedSimpleLayer programmeWidget = new GWTPagedSimpleLayer();
                for (ActionCollectionEntry<String> programmeEntry : channel.getProgrammes()) {
                    if (programmeEntry.isReadable()) {
                        final String programmeId = programmeEntry.getKey();
                        ClickHandler programmeClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    programmeDisplaySelectionHandler.onItemSelected(programmeId);
                                }
                            };
                        programmeWidget.addRow(new GWTClickableLabel(programmeId, programmeClickHandler));
                    }
                }

                GWTPagedTree rightWidget = new GWTPagedTree();
                for (ActionCollectionEntry<RightForm> rightEntry : channel.getRightForms()) {
                    if (rightEntry.isReadable()) {
                        final RightForm right = rightEntry.getKey();
                        Long newBranchId = rightWidget.getItemByName(right.getRole().getI18nFriendlyString());
                        if (newBranchId==null)
                            newBranchId = rightWidget.addItemWithName(right.getRole().getI18nFriendlyString(), new GWTLabel(right.getRole().getI18nFriendlyString()));
                        ClickHandler rightClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    restrictedUserDisplaySelectionHandler.onItemSelected(right.getRestrictedUserId());
                                }
                            };
                        rightWidget.addItem(new GWTClickableLabel(right.getRestrictedUserId(), rightClickHandler), newBranchId);
                    }
                }

                GWTStreamingStatusBox streamerStatusWidget = new GWTStreamingStatusBox(channel.getLabel(), streamerStatusChangeHandler);

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (channelEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(channel.getLabel()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                if (channelEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                }

                channelList.addRow(channel,
                        channelEntry.isDeletable(),
                        new GWTClickableLabel(channel.getLabel(), channelDisplayClickHandler),
                        new GWTTruncatedLabel(channel.getDescription()),
                        programmeWidget,
                        rightWidget,
                        streamerStatusWidget,
                        operationPanel);
            }
        }

    }
}
