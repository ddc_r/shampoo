/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.STREAMER_ACTION;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.queue.*;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;

/**
 *
 * @author okay_awright
 *
 */
public class RadioPlayerView extends PageView {

    //The channel that is associated to the archives
    protected GWTPagedFormDropDownListBox<ChannelForm> channelComboBox;
    protected GWTStreamingStatusBox streamerStatusWidget;
    protected GWTVerticalPanel displayPanel;
    protected GWTGrid itemPanel;
    protected GWTPlayerSoundManager2 player;

    public GenericDropDownListBoxInterface<ChannelForm> getChannelComboBox() {
        return channelComboBox;
    }

    public GenericStatusBoxInterface<String, STATUS> getStreamerStatusWidget() {
        return streamerStatusWidget;
    }

    public void setStreamerStatusWidget(GWTStreamingStatusBox streamerStatusWidget) {
        this.streamerStatusWidget = streamerStatusWidget;
    }

    public GenericPanelInterface getDisplayPanel() {
        return displayPanel;
    }

    public GenericGridInterface getItemPanel() {
        return itemPanel;
    }

    public GenericFilePlayerInterface getPlayer() {
        return player;
    }

    @Override
    public String getHeaderText() {
        //returns nothing, because it's not needed
        return null;
    }

    @Override
    public String getTitleText() {
        //Cheat a bit and make sure the header will be displayed everytime even if we don't need any caption
        //It can be hidden through CSS trickery
        return "Radio player";
    }

    @Override
    public String getHeaderCSSMarker() {
        return "player";
    }

    @Override
    public GenericPanelInterface buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();
        
        //Top panel section
        GWTGrid gridRadioList = new GWTGrid();
        channelComboBox = new GWTPagedFormDropDownListBox<ChannelForm>();
        streamerStatusWidget = new GWTStreamingStatusBox();
        gridRadioList.addRow(
                new GWTLabel(I18nTranslator.getInstance().channel() + ":"),
                channelComboBox,
                streamerStatusWidget);
        mainPanel.add(gridRadioList);

        //Main panel section, empty by default
        displayPanel = new GWTVerticalPanel();
        mainPanel.add(displayPanel);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, there will never be any link anyway
        return null;
    }

    //Respond to presenter requests
    public void refreshDisplayPanel(ActionCollectionEntry<StreamerModule> streamerModule, EventBusWithLookup eventBus) {

        displayPanel.clean();
        
        if (streamerModule != null && streamerModule.getItem() != null && streamerModule.getItem().getStreamURI() != null && streamerModule.hasAction(STREAMER_ACTION.LISTEN)) {

            String trimmedURI = streamerModule.getItem().getStreamURI().trim();
            if (trimmedURI.length() != 0) {
                GWTFlowPanel extendedItemPanel = new GWTFlowPanel();
                GWTGrid mainItemPanel = new GWTGrid();
                player = new GWTPlayerSoundManager2(trimmedURI, eventBus, null/*JSESSIONID not implemented, yet*/);
                itemPanel = new GWTGrid();
                mainItemPanel.addRow(player, itemPanel);
                extendedItemPanel.add(mainItemPanel);

                displayPanel.add(extendedItemPanel);
            }
        }
    }

    public void refreshStreamerStatus(String streamerId, ActionCollectionEntry<StreamerModule> statusEntry) {
        if (streamerId != null && statusEntry != null && (statusEntry.isReadable() || statusEntry.hasAction(STREAMER_ACTION.LISTEN))) {
            streamerStatusWidget.setID(streamerId);
            if (statusEntry.getItem()!=null) {
                if (statusEntry.getItem().isStreaming()!=null)
                    streamerStatusWidget.setStatus(statusEntry.getItem().isStreaming() ? STATUS.onair : STATUS.offair);
                else
                    streamerStatusWidget.setStatus(STATUS.reserved);
            } else 
                streamerStatusWidget.setStatus(STATUS.free);
        } else {
            streamerStatusWidget.clean();
        }
    }

    public void refreshChannelList(ActionCollection<ChannelForm> channels) {
        //Clear the list beforehand
        channelComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channels) {

            //Don't list the channel if it can't be read or if its streamer does not exist or cannot be listened to
            if (channelEntry.isReadable() && channelEntry.getItem().getSeatNumber() != null && channelEntry.getItem().getSeatNumber().hasAction(STREAMER_ACTION.LISTEN)) {
                ChannelForm channel = channelEntry.getKey();

                channelComboBox.addItem(channel.getLabel(), channel);
            }
        }
    }

    public void addTimetracker(long initialOffset, long maxOffset, int millisecondPerCycle, GenericPanelInterface container) {
        GWTCSSAutoElapsingPercentProgressBar timeWidget = new GWTCSSAutoElapsingPercentProgressBar(
                initialOffset,//
                maxOffset,//
                millisecondPerCycle);
        container.add(timeWidget);
    }

    public void refreshStreamItem(//
            ActionCollectionEntry<? extends QueueForm> streamItemEntry,//
            GenericCallbackInterface<QueueForm, GenericPanelInterface, Void> timeTrackerHandler,//
            GenericMyVoteBoxInterface.VoteChangeHandler<String> myRatingChangeHandler,//
            ClickHandler trackDisplayClickHandler,//
            ClickHandler programmeDisplayClickHandler) {

        itemPanel.clean();

        if (streamItemEntry != null && streamItemEntry.isReadable()) {
            QueueForm streamItem = streamItemEntry.getItem();

            GWTThumbnail coverArtWidget = new GWTThumbnail(streamItem.getCoverArtFile());

            Widget myRatingWidget = null;
            if (streamItem instanceof BroadcastableTrackQueueForm && ((BroadcastableTrackQueueForm) streamItem).isCanBeVoted()) {
                myRatingWidget = new GWTMyVoteBox(((BroadcastableTrackQueueForm) streamItem).getTrackId(), myRatingChangeHandler);
            } else if (streamItem instanceof RequestQueueForm && ((RequestQueueForm) streamItem).isCanBeVoted()) {
                myRatingWidget = new GWTMyVoteBox(((RequestQueueForm) streamItem).getTrackId(), myRatingChangeHandler);
            } else {
                myRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
            }

            Widget contentCaptionWidget2;
            if (streamItem instanceof BroadcastableTrackQueueForm || streamItem instanceof RequestQueueForm) {
                contentCaptionWidget2 = new GWTCSSClickableLabel(streamItem.getContentCaption());
                ((GWTClickableLabel) contentCaptionWidget2).addClickHandler(trackDisplayClickHandler);
                ((GWTCSSClickableLabel) contentCaptionWidget2).setSecondaryCSSMarker("trackLabel");
            } else {
                contentCaptionWidget2 = new GWTCSSLabel(streamItem.getContentCaption());
                ((GWTCSSLabel) contentCaptionWidget2).setSecondaryCSSMarker("trackLabel");
            }

            GWTPagedTree captionWidget2 = new GWTPagedTree();
            Long newBranchId = captionWidget2.addItem(contentCaptionWidget2);
            if (streamItem instanceof RequestQueueForm) {
                RequestQueueForm trackForm = (RequestQueueForm) streamItem;

                if (trackForm.getRequestAuthor() != null && trackForm.getRequestAuthor().length() != 0) {
                    newBranchId = captionWidget2.addItem(new GWTLabel(I18nTranslator.getInstance().requested_by() + ": "
                            + trackForm.getRequestAuthor()), newBranchId);
                } else if (trackForm.getRequestMessage() != null && trackForm.getRequestMessage().length() != 0) {
                    newBranchId = captionWidget2.addItem(new GWTLabel(I18nTranslator.getInstance().requested_by() + ": "
                            + I18nTranslator.getInstance().unknown()), newBranchId);
                }
                if (trackForm.getRequestMessage() != null && trackForm.getRequestMessage().length() != 0) {
                    captionWidget2.addItem(new GWTTruncatedLabel(trackForm.getRequestMessage()), newBranchId);
                }
            }

            //Anonymous function that simulates on-th-fly binding fo links requiring Presenter operations
            GenericPanelInterface timeWidget = new GWTFlowPanel();
            timeTrackerHandler.onCallBack(streamItem, timeWidget, null);

            GWTCSSVerticalPanel mainItemPanel = new GWTCSSVerticalPanel("trackInfo");
            mainItemPanel.add(captionWidget2);
            mainItemPanel.add(timeWidget);

            itemPanel.addRow(//
                    coverArtWidget,//
                    mainItemPanel,//
                    new GWTLabel(I18nTranslator.getInstance().my_vote() + ":"),//
                    myRatingWidget//                    
                    );
        }
    }
}
