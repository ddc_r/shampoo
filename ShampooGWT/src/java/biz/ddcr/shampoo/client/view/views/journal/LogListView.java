/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.journal;

import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.journal.LogForm.LOG_TAGS;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.ColumnHeader;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmation;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmationDelete;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFormSortableTable;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTTruncatedLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class LogListView extends PageView {

    GWTVerticalPanel logPanel;
    GWTFormSortableTable<LogForm, Widget, LOG_TAGS> logList;

    public GenericFormSortableTableInterface<LogForm, Widget, LOG_TAGS> getLogList() {
        return logList;
    }

    public GenericPanelInterface getLogPanel() {
        return logPanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().log_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "journal";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        logPanel = new GWTVerticalPanel();

        return logPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void refreshLogPanel(SortableTableClickHandler listHeaderClickHandler) {
        logList = new GWTFormSortableTable<LogForm, Widget, LOG_TAGS>();
        //Generate list header
        logList.setHeader(
                new ColumnHeader<LOG_TAGS>(I18nTranslator.getInstance().time(), LOG_TAGS.time),
                new ColumnHeader<LOG_TAGS>(I18nTranslator.getInstance().type(), LOG_TAGS.type),
                new ColumnHeader<LOG_TAGS>(I18nTranslator.getInstance().action(), LOG_TAGS.action),
                new ColumnHeader<LOG_TAGS>(I18nTranslator.getInstance().actee(), LOG_TAGS.actee),
                new ColumnHeader<LOG_TAGS>(I18nTranslator.getInstance().actor(), LOG_TAGS.actor),
                new ColumnHeader<LOG_TAGS>(I18nTranslator.getInstance().operation())
                );
        logList.setRefreshClickHandler(listHeaderClickHandler);

        logPanel.add(logList);
    }

    public void refreshLogList(ActionCollection<? extends LogForm> logs, YesNoClickHandler deleteClickHandler, final YesNoClickHandler deleteAllClickHandler) {

        //Clear the list beforehand
        logList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        logList.setNextButtonVisible((logs.size() >= logList.getMaxVisibleRows()));

        logList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<? extends LogForm> logEntry : logs) {

            if (logEntry.isReadable()) {
                LogForm log = logEntry.getKey();

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //Delete
                if (logEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(log.getRefID()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                
                logList.addRow(log,
                        logEntry.isDeletable(),
                        new GWTLabel(log.getTime().getI18nSyntheticFriendlyString()),
                        new GWTLabel(log.getFriendlyTypeCaption()),
                        new GWTTruncatedLabel(log.getAction().getI18nFriendlyString()),
                        new GWTLabel(log.getFriendlyActeeCaption()),
                        new GWTLabel(log.getActor()),
                        operationPanel);
            }
        }

    }

}
