/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.notification;

import biz.ddcr.shampoo.client.form.notification.NotificationForm;
import biz.ddcr.shampoo.client.form.notification.NotificationForm.NOTIFICATION_TAGS;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.ColumnHeader;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmation;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmationDelete;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFormSortableTable;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedTree;
import biz.ddcr.shampoo.client.view.widgets.GWTTruncatedLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class NotificationListView extends PageView {

    GWTVerticalPanel messagePanel;
    GWTFormSortableTable<NotificationForm, Widget, NOTIFICATION_TAGS> messageList;

    public GenericFormSortableTableInterface<NotificationForm, Widget, NOTIFICATION_TAGS> getMessageList() {
        return messageList;
    }

    public GenericPanelInterface getMessagePanel() {
        return messagePanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().notification_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        messagePanel = new GWTVerticalPanel();

        return messagePanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void refreshMessagePanel(SortableTableClickHandler listHeaderClickHandler) {
        messageList = new GWTFormSortableTable<NotificationForm, Widget, NOTIFICATION_TAGS>();
        //Generate list header
        messageList.setHeader(
                new ColumnHeader<NOTIFICATION_TAGS>(I18nTranslator.getInstance().time(), NOTIFICATION_TAGS.time),
                new ColumnHeader<NOTIFICATION_TAGS>(I18nTranslator.getInstance().type(), NOTIFICATION_TAGS.message),
                new ColumnHeader<NOTIFICATION_TAGS>(I18nTranslator.getInstance().operation()));
        messageList.setRefreshClickHandler(listHeaderClickHandler);

        messagePanel.add(messageList);
    }

    public void refreshMessageList(ActionCollection<? extends NotificationForm> messages, YesNoClickHandler deleteClickHandler, final YesNoClickHandler deleteAllClickHandler) {

        //Clear the list beforehand
        messageList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        messageList.setNextButtonVisible((messages.size() >= messageList.getMaxVisibleRows()));

        messageList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<? extends NotificationForm> messageEntry : messages) {

            if (messageEntry.isReadable()) {
                NotificationForm message = messageEntry.getKey();

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //Delete
                if (messageEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(message.getRefID()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }

                //Notification message
                GWTPagedTree messageWidget = new GWTPagedTree();
                Long newBranchId = messageWidget.addItem(new GWTLabel(message.getFriendlyMessage()));
                if (message.getFreeFormText() != null && message.getFreeFormText().length()!=0) {
                    messageWidget.addItem(new GWTTruncatedLabel(message.getFreeFormText()), newBranchId);
                }

                messageList.addRow(message,
                        messageEntry.isDeletable(),
                        new GWTLabel(message.getTime().getI18nSyntheticFriendlyString()),
                        messageWidget,
                        operationPanel);
            }
        }

    }
}
