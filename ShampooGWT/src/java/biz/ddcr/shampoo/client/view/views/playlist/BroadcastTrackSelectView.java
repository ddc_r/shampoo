/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.playlist;

import biz.ddcr.shampoo.client.form.track.BroadcastAdvertForm;
import biz.ddcr.shampoo.client.form.track.BroadcastJingleForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.TRACK_ACTION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackSelectView extends PageView {

    //Tracks that are broadcast only
    protected GWTFormSortableTable<BroadcastTrackForm, Widget, BROADCASTABLE_TRACK_TAGS> broadcastTrackList;
    protected GWTClickableLabel cancel;

    public GenericFormSortableTableInterface<BroadcastTrackForm, Widget, BROADCASTABLE_TRACK_TAGS> getBroadcastTrackList() {
        return broadcastTrackList;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().select_track();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "track";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().track_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        broadcastTrackList = new GWTFormSortableTable<BroadcastTrackForm, Widget, BROADCASTABLE_TRACK_TAGS>();
        //Generate list header
        broadcastTrackList.setHeader(
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().cover_art()),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().enabled(), BROADCASTABLE_TRACK_TAGS.enabled),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().type(), BROADCASTABLE_TRACK_TAGS.type),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().author(), BROADCASTABLE_TRACK_TAGS.author),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().title(), BROADCASTABLE_TRACK_TAGS.title),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().album(), BROADCASTABLE_TRACK_TAGS.album),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().genre(), BROADCASTABLE_TRACK_TAGS.genre),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().description(), BROADCASTABLE_TRACK_TAGS.description),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().duration(), BROADCASTABLE_TRACK_TAGS.duration),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().release_date(), BROADCASTABLE_TRACK_TAGS.yearOfRelease),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().average_vote(), BROADCASTABLE_TRACK_TAGS.averageVote),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().my_vote(), BROADCASTABLE_TRACK_TAGS.myVote),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().programmes(), BROADCASTABLE_TRACK_TAGS.programmes),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().operation()));
        //broadcastTrackList.setRefreshClickHandler(listHeaderClickHandler);

        mainPanel.add(broadcastTrackList);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    public void refreshBroadcastTrackList(
            ActionCollection<? extends BroadcastTrackForm> tracks,
            ClickHandler selectClickHandler,
            VoteChangeHandler<String> myRatingChangeHandler,
            ClickHandler displayTrackClickHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler,
            EventBusWithLookup eventBus,
            String jsessionid) {

        //Clear the list beforehand
        broadcastTrackList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        broadcastTrackList.setNextButtonVisible((tracks.size() >= broadcastTrackList.getMaxVisibleRows()));

        for (ActionCollectionEntry<? extends BroadcastTrackForm> trackEntry : tracks) {

            if (trackEntry.isReadable()) {
                BroadcastTrackForm track = trackEntry.getKey();

                GWTLabel typeLabel = new GWTLabel(null);
                if (track instanceof BroadcastSongForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().song());
                } else if (track instanceof BroadcastJingleForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().jingle());
                } else if (track instanceof BroadcastAdvertForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().advert());
                } else {
                    typeLabel.setCaption(I18nTranslator.getInstance().unknown());
                }

                Widget coverArtWidget = null;
                if (track.isReady()) {
                    coverArtWidget = new GWTThumbnail(track.getCoverArtFile()!=null?track.getCoverArtFile().getItem():null);
                } else {
                    coverArtWidget = new GWTCSSFlowPanel("GWTWorking");
                }

                Widget myRatingWidget = null;
                if (track instanceof BroadcastSongForm) {
                    myRatingWidget = new GWTMyVoteBox(track.getRefID(), myRatingChangeHandler);
                } else {
                    myRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
                }

                GWTLabel averageRatingWidget = null;
                if (track instanceof BroadcastSongForm) {
                    if (((BroadcastSongForm) track).getAverageVote() != null) {
                        averageRatingWidget = new GWTLabel(((BroadcastSongForm) track).getAverageVote().getI18nFriendlyName());
                    } else {
                        averageRatingWidget = new GWTLabel(I18nTranslator.getInstance().none());
                    }
                } else {
                    averageRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
                }

                GWTPagedTree programmeWidget = new GWTPagedTree();
                for (ActionCollectionEntry<BroadcastTrackProgrammeModule> programmeEntry : track.getTrackProgrammes()) {
                    if (programmeEntry.isReadable()) {
                        final BroadcastTrackProgrammeModule trackProgrammeModule = programmeEntry.getKey();
                        ClickHandler programmeClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    programmeDisplaySelectionHandler.onItemSelected(trackProgrammeModule.getProgramme());
                                }
                            };
                        Long newBranchId = programmeWidget.addItem(new GWTClickableLabel(trackProgrammeModule.getProgramme(), programmeClickHandler));

                        programmeWidget.addItem(new GWTLabel(I18nTranslator.getInstance().rotation_count()+": "+trackProgrammeModule.getRotationCount()), newBranchId);
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (track.isReady()
                        && trackEntry.hasAction(TRACK_ACTION.EXTENDED_VIEW)) {
                    if (track.getTrackFile() != null && track.getTrackFile().getItem()!=null) {
                        GWTPlayerSoundManager2 playerApplet = new GWTPlayerSoundManager2(track.getTrackFile().getItem(), eventBus, jsessionid);
                        operationPanel.add(playerApplet);
                    }
                }
                GWTClickableLabel selectLink = new GWTClickableLabelSelect(I18nTranslator.getInstance().select());
                if (selectClickHandler != null) {
                    selectLink.addClickHandler(selectClickHandler);
                }
                operationPanel.add(selectLink);

                broadcastTrackList.addRow(track,
                        //Don't use the multiple selection since only one track can be chosen at a time, so put the flag to false
                        false,
                        coverArtWidget,
                        new GWTLabel(track.isActivated() ? I18nTranslator.getInstance().is_true() : I18nTranslator.getInstance().is_false()),
                        typeLabel,
                        new GWTClickableLabel(track.getAuthor(), displayTrackClickHandler),
                        new GWTClickableLabel(track.getTitle(), displayTrackClickHandler),
                        new GWTLabel(track.getAlbum()),
                        new GWTLabel(track.getGenre()),
                        new GWTTruncatedLabel(track.getDescription()),
                        new GWTLabel(track.getTrackFile() != null && track.getTrackFile().getItem()!=null && track.getTrackFile().getItem().getDuration() != null ? track.getTrackFile().getItem().getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown()),
                        new GWTLabel(track.getYearOfRelease() != null ? Short.toString(track.getYearOfRelease()) : I18nTranslator.getInstance().unknown()),
                        averageRatingWidget,
                        myRatingWidget,
                        programmeWidget,
                        operationPanel);
            }
        }

    }
}
