/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.playlist;

import biz.ddcr.shampoo.client.form.playlist.filter.FILTER_FEATURE;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class FiltersEditView extends PageView {

    //Filters
    private GWTPagedFormDropDownListBox<FILTER_FEATURE> newFilterTypeComboBox;
    private GenericPlaylistFilterInterface newFilterValue;
    private GWTClickableLabel addNewFilterButton;
    private GWTGrid newFilterPanel;
    private GWTPagedFormTable<FilterForm, Widget> filtersListBox;
    //Misc
    private GWTButton okLink;
    private GWTClickableLabel cancel;

    public GenericFormTableInterface<FilterForm, Widget> getFilterList() {
        return filtersListBox;
    }

    public GenericHyperlinkInterface getOkLink() {
        return okLink;
    }

    public GenericHyperlinkInterface getAddFilterLink() {
        return addNewFilterButton;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().filters_list();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "playlist";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().playlist_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        newFilterPanel = new GWTGrid();

        newFilterTypeComboBox = new GWTPagedFormDropDownListBox<FILTER_FEATURE>();
        newFilterTypeComboBox.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent ce) {
                onNewFilterTypeChange();
            }
        });
        resetFilterKeys(FILTER_FEATURE.values());
        addNewFilterButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());
        addNewFilterButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                if (newFilterValue!=null)
                    addNewFilter(newFilterValue.getForm());
            }
        });
        newFilterPanel.addRow(
                newFilterTypeComboBox,
                //Will be programmatically filled in with onNewFilterTypeChange()
                null,
                addNewFilterButton);
        mainPanel.add(newFilterPanel);
        onNewFilterTypeChange();

        filtersListBox = new GWTPagedFormTable<FilterForm, Widget>();
        mainPanel.add(filtersListBox);

        //Form submission
        okLink = new GWTButton(I18nTranslator.getInstance().ok());
        mainPanel.add(okLink);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;

    }

    private void onNewFilterTypeChange() {
        switch(newFilterTypeComboBox.getSelectedValue()) {
            case alphabetical:
                newFilterValue = new GWTEditablePlaylistAlphabeticalFilter();
                break;
            case numerical:
                newFilterValue = new GWTEditablePlaylistNumericalFilter();
                break;
            case category:
                newFilterValue = new GWTEditablePlaylistCategoryFilter();
                break;
            default:
                newFilterValue = null;
        }
        if (newFilterValue!=null) newFilterPanel.setWidgetAt(0, 1, newFilterValue.getWidget());
    }

    private void resetFilterKeys(FILTER_FEATURE[] keys) {
        //Clear the list beforehand
        newFilterTypeComboBox.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        for (FILTER_FEATURE key : keys) {
            newFilterTypeComboBox.addItem(key.getI18nFriendlyString(), key);
        }
    }

    public void refreshExistingFilters(Collection<FilterForm> filters) {
        //clear the list beforehand
        filtersListBox.clean();

        final ClickHandler deletAllClickHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                for (int i = filtersListBox.getRowCount() - 1; i >= 0; i--) {
                    Boolean b = filtersListBox.isRowChecked(i);
                    if (b != null && b == true) {
                        filtersListBox.removeRow(i);
                    }
                }
            }
        };

        filtersListBox.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                bindDeleteHandler(captionContainer, deletAllClickHandler);
            }
        });

        addNewFilters(filters);
    }

    public void addNewFilters(Collection<FilterForm> filters) {
        //generate the list
        if (filters != null) {
            for (FilterForm filter : filters) {
                addNewFilter(filter);
            }
        }
    }

    public void addNewFilter(FilterForm newFilter) {

        //Is it already present in the list?
        if (newFilter != null && !getFilterList().isValueIn(newFilter)) {

            final ClickHandler removeClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    filtersListBox.removeRow(filtersListBox.getCurrentRow(event));
                }
            };
            //Create the channel entry
            GWTFlowPanel operationPanel = new GWTFlowPanel();
            bindDeleteHandler(operationPanel, removeClickHandler);

            //Add the new channel to the list
            filtersListBox.addRow(newFilter, true, new GWTLabel(newFilter.getI18nFriendlyString()), operationPanel);
        }
    }

    private void bindDeleteHandler(GenericPanelInterface container, ClickHandler deleteClickHandler) {
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        container.add(deleteLink);
    }
}
