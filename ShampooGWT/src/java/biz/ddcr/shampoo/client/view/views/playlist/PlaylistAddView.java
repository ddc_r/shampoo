/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.playlist;


import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 *
 * @author okay_awright
 **/
public class PlaylistAddView extends PageView {

    //User widgets
    protected GWTTextBox labelBox;
    protected GWTExtendedTextBox descriptionBox;
    protected GWTLabel programmeLabel;
    protected GWTCheckBox enableLiveCheckBox;
    protected GWTTextBox liveBroadcasterBox;
    protected GWTTextBox liveLoginBox;
    protected GWTPasswordTextBox livePasswordBox;
    protected GWTNullableFormDropDownListBox<PEGI_AGE> liveAdvisoryAgeComboBox;
    protected GWTFlowPanel liveAdvisoryFeaturesPanel;
    protected GWTCheckBox liveAdvisoryViolenceCheckBox;
    protected GWTCheckBox liveAdvisoryProfanityCheckBox;
    protected GWTCheckBox liveAdvisoryFearCheckBox;
    protected GWTCheckBox liveAdvisorySexCheckBox;
    protected GWTCheckBox liveAdvisoryDrugsCheckBox;
    protected GWTCheckBox liveAdvisoryDiscriminationCheckBox;
    //Flyer upload
    protected GWTClickableUncachableImage albumCoverPicture;
    protected GWTPictureUploadMoxie albumCoverUploadPlupload;
    protected GWTClickableLabel resetAlbumCoverArtLink;
    //User requests
    protected GWTPagedFormDropDownListBox<Integer> maxNumberOfUserRequestsSelector;
    protected GWTCheckBox noReplayTimeSelector;
    protected GWTNumericBox noReplayTimeThresholdBox;
    protected GWTCheckBox noReplayPlaylistCheckBox;
    //Timetable slots widgets
    protected GWTPagedFormDropDownListBox<TimetableSlotForm> timetableSlotComboBox;
    protected GWTClickableLabel addNewTimetableSlotButton;
    protected GWTPagedFormTable<TimetableSlotFormID, Widget> timetableSlotListBox;
    //playlist widget
    protected GWTEditablePlaylist playlistContent;
    //overriding tags
    protected GWTCheckBox enableGlobalTagSetCheckBox;
    protected GWTTextBox authorTextBox;
    protected GWTTextBox titleTextBox;
    protected GWTTextBox albumTextBox;
    protected GWTExtendedTextBox descriptionTextBox;
    protected GWTNullableNumericBox yearOfReleaseNumericBox;
    protected GWTTextBox genreTextBox;
    protected GWTNullableFormDropDownListBox<PEGI_AGE> advisoryAgeComboBox;
    protected GWTFlowPanel advisoryFeaturesPanel;
    protected GWTCheckBox advisoryViolenceCheckBox;
    protected GWTCheckBox advisoryProfanityCheckBox;
    protected GWTCheckBox advisoryFearCheckBox;
    protected GWTCheckBox advisorySexCheckBox;
    protected GWTCheckBox advisoryDrugsCheckBox;
    protected GWTCheckBox advisoryDiscriminationCheckBox;

    protected GWTButton submit;
    protected GWTClickableLabel cancel;

    public GenericHyperlinkInterface getAlbumCoverPicture() {
        return albumCoverPicture;
    }

    public GenericHyperlinkInterface getResetAlbumCoverArtLink() {
        return resetAlbumCoverArtLink;
    }

    public GenericFileUploadInterface getAlbumCoverUploadWidget() {
        return albumCoverUploadPlupload;
    }

    public GenericTextBoxInterface getLabelBox() {
        return labelBox;
    }

    public GenericTextBoxInterface getDescriptionBox() {
        return descriptionBox;
    }

    public GenericLabelInterface getProgrammeLabel() {
        return programmeLabel;
    }

    public GenericCheckBoxInterface getEnableLiveCheckBox() {
        return enableLiveCheckBox;
    }

    public GenericTextBoxInterface getLiveBroadcasterBox() {
        return liveBroadcasterBox;
    }

    public GenericTextBoxInterface getLiveLoginBox() {
        return liveLoginBox;
    }

    public GenericTextBoxInterface getLivePasswordBox() {
        return livePasswordBox;
    }

    public GenericDropDownListBoxInterface<PEGI_AGE> getLiveAdvisoryAgeComboBox() {
        return liveAdvisoryAgeComboBox;
    }

    public GenericPanelInterface getLiveAdvisoryFeaturePanel() {
        return liveAdvisoryFeaturesPanel;
    }

    public GenericCheckBoxInterface getLiveAdvisoryDiscriminationCheckBox() {
        return liveAdvisoryDiscriminationCheckBox;
    }

    public GenericCheckBoxInterface getLiveAdvisoryDrugsCheckBox() {
        return liveAdvisoryDrugsCheckBox;
    }

    public GenericCheckBoxInterface getLiveAdvisoryFearCheckBox() {
        return liveAdvisoryFearCheckBox;
    }

    public GenericCheckBoxInterface getLiveAdvisoryProfanityCheckBox() {
        return liveAdvisoryProfanityCheckBox;
    }

    public GenericCheckBoxInterface getLiveAdvisorySexCheckBox() {
        return liveAdvisorySexCheckBox;
    }

    public GenericCheckBoxInterface getLiveAdvisoryViolenceCheckBox() {
        return liveAdvisoryViolenceCheckBox;
    }

    public GenericDropDownListBoxInterface<Integer> getMaxNumberOfUserRequestsSelector() {
        return maxNumberOfUserRequestsSelector;
    }

    public GenericCheckBoxInterface getNoReplayPlaylistCheckBox() {
        return noReplayPlaylistCheckBox;
    }

    public GenericCheckBoxInterface getNoReplayTimeSelector() {
        return noReplayTimeSelector;
    }

    public GenericNumericBoxInterface getNoReplayTimeThresholdBox() {
        return noReplayTimeThresholdBox;
    }

    public GenericHyperlinkInterface getAddNewTimetableSlotButton() {
        return addNewTimetableSlotButton;
    }

    public GenericDropDownListBoxInterface<TimetableSlotForm> getTimetableSlotComboBox() {
        return timetableSlotComboBox;
    }

    public GenericFormTableInterface<TimetableSlotFormID, Widget> getTimetableSlotListBox() {
        return timetableSlotListBox;
    }

    public GenericEditablePlaylistInterface<PlaylistEntryForm, ActionCollectionEntry<String>, Collection<FilterForm>> getPlaylistContent() {
        return playlistContent;
    }

    public GenericCheckBoxInterface getEnableGlobalTagSetCheckBox() {
        return enableGlobalTagSetCheckBox;
    }

    public GenericDropDownListBoxInterface<PEGI_AGE> getAdvisoryAgeComboBox() {
        return advisoryAgeComboBox;
    }

    public GenericPanelInterface getAdvisoryFeaturePanel() {
        return advisoryFeaturesPanel;
    }

    public GenericCheckBoxInterface getAdvisoryDiscriminationCheckBox() {
        return advisoryDiscriminationCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryDrugsCheckBox() {
        return advisoryDrugsCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryFearCheckBox() {
        return advisoryFearCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryProfanityCheckBox() {
        return advisoryProfanityCheckBox;
    }

    public GenericCheckBoxInterface getAdvisorySexCheckBox() {
        return advisorySexCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryViolenceCheckBox() {
        return advisoryViolenceCheckBox;
    }

    public GenericTextBoxInterface getAlbumTextBox() {
        return albumTextBox;
    }

    public GenericTextBoxInterface getAuthorTextBox() {
        return authorTextBox;
    }

    public GenericTextBoxInterface getDescriptionTextBox() {
        return descriptionTextBox;
    }

    public GenericTextBoxInterface getGenreTextBox() {
        return genreTextBox;
    }

    public GenericTextBoxInterface getTitleTextBox() {
        return titleTextBox;
    }

    public GenericNumericBoxInterface<Integer> getYearOfReleaseNumericBox() {
        return yearOfReleaseNumericBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    //Setup minimum view

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().add_playlist();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "playlist";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().playlist_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Playlist widgets
        GWTVerticalPanel playlistPanel = new GWTVerticalPanel();

        GWTGrid tagGrid = new GWTGrid();

        GWTGrid mainGrid = new GWTGrid();

        labelBox = new GWTTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelBox);

        descriptionBox = new GWTExtendedTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionBox);

        programmeLabel = new GWTLabel(null);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeLabel);

        enableLiveCheckBox = new GWTCheckBox(false);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().activate_live() + ":"), enableLiveCheckBox);

        liveBroadcasterBox = new GWTTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().broadcaster_name() + ":"), liveBroadcasterBox);

        liveLoginBox = new GWTTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().live_login() + ":"), liveLoginBox);

        livePasswordBox = new GWTPasswordTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().live_password() + ":"), livePasswordBox);

        bindLiveRatingPanel(mainGrid);

        //Cover art
        GWTFlowPanel newAlbumCoverArtPanel = new GWTFlowPanel();
        albumCoverPicture = new GWTClickableUncachableImage();
        newAlbumCoverArtPanel.add(albumCoverPicture);

        albumCoverUploadPlupload = new GWTPictureUploadMoxie(null);
        resetAlbumCoverArtLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        newAlbumCoverArtPanel.add(albumCoverUploadPlupload);
        newAlbumCoverArtPanel.add(resetAlbumCoverArtLink);

        tagGrid.addRow(mainGrid, newAlbumCoverArtPanel);

        playlistPanel.add(tagGrid);

        //Timetable slot widget
        //Programme widgets
        GWTVerticalPanel timetableSlotPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel timetableSlotLabel = new GWTLabel(I18nTranslator.getInstance().timetables_list());
        timetableSlotPanel.add(timetableSlotLabel);

        GWTVerticalPanel timetableSlotContainerPanel = new GWTVerticalPanel();

        GWTGrid newTimetableSlotEntryPanel = new GWTGrid();

        GWTLabel timetableSlotComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().timetable());
        timetableSlotComboBox = new GWTPagedFormDropDownListBox<TimetableSlotForm>();

        addNewTimetableSlotButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newTimetableSlotEntryPanel.addRow(timetableSlotComboBoxCaption);
        newTimetableSlotEntryPanel.addRow(
                timetableSlotComboBox,
                addNewTimetableSlotButton);
        timetableSlotContainerPanel.add(newTimetableSlotEntryPanel);

        timetableSlotListBox = new GWTPagedFormTable<TimetableSlotFormID, Widget>();
        timetableSlotContainerPanel.add(timetableSlotListBox);
        timetableSlotPanel.add(timetableSlotContainerPanel);

        playlistPanel.add(timetableSlotPanel);

        //Playlist widget
        GWTGrid itemsGrid = new GWTGrid();

        itemsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().playlist_items()));
        playlistContent = new GWTEditablePlaylist();
        itemsGrid.addRow(playlistContent);

        playlistPanel.add(itemsGrid);

        bindMiscOptionsPanel(playlistPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        playlistPanel.add(submit);

        return playlistPanel;
    }

    //Respond to presenter requests
    public void loadCoverArt(HasCoverArtInterface module) {
        albumCoverPicture.setImage(module);
    }
    public void unloadCoverArt() {
        albumCoverPicture.setImage((HasCoverArtInterface)null);
    }

    protected void bindLiveRatingPanel(GenericGridInterface livePanel) {
        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().advisory());

        GWTVerticalPanel _panel = new GWTVerticalPanel();

        GWTGrid advisoryAgeGrid = new GWTGrid();
        liveAdvisoryAgeComboBox = new GWTNullableFormDropDownListBox<PEGI_AGE>();
        advisoryAgeGrid.addRow(new GWTLabel(I18nTranslator.getInstance().rating() + ":"), liveAdvisoryAgeComboBox);
        _panel.add(advisoryAgeGrid);

        liveAdvisoryFeaturesPanel = new GWTFlowPanel();
        liveAdvisoryViolenceCheckBox = new GWTCheckBox(false);
        liveAdvisoryViolenceCheckBox.setCaption(I18nTranslator.getInstance().violence_advisory());
        liveAdvisoryFeaturesPanel.add(liveAdvisoryViolenceCheckBox);
        liveAdvisoryProfanityCheckBox = new GWTCheckBox(false);
        liveAdvisoryProfanityCheckBox.setCaption(I18nTranslator.getInstance().profanity_advisory());
        liveAdvisoryFeaturesPanel.add(liveAdvisoryProfanityCheckBox);
        liveAdvisoryFearCheckBox = new GWTCheckBox(false);
        liveAdvisoryFearCheckBox.setCaption(I18nTranslator.getInstance().fear_advisory());
        liveAdvisoryFeaturesPanel.add(liveAdvisoryFearCheckBox);
        liveAdvisorySexCheckBox = new GWTCheckBox(false);
        liveAdvisorySexCheckBox.setCaption(I18nTranslator.getInstance().sex_advisory());
        liveAdvisoryFeaturesPanel.add(liveAdvisorySexCheckBox);
        liveAdvisoryDrugsCheckBox = new GWTCheckBox(false);
        liveAdvisoryDrugsCheckBox.setCaption(I18nTranslator.getInstance().drugs_advisory());
        liveAdvisoryFeaturesPanel.add(liveAdvisoryDrugsCheckBox);
        liveAdvisoryDiscriminationCheckBox = new GWTCheckBox(false);
        liveAdvisoryDiscriminationCheckBox.setCaption(I18nTranslator.getInstance().discrimination_advisory());
        liveAdvisoryFeaturesPanel.add(liveAdvisoryDiscriminationCheckBox);
        _panel.add(liveAdvisoryFeaturesPanel);

        miscOptionsPanel.add(_panel);

        livePanel.addRow(miscOptionsPanel);
    }

    protected void bindMiscOptionsPanel(GenericPanelInterface playlistPanel) {
        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());

        GWTVerticalPanel _panel = new GWTVerticalPanel();

        GWTGrid userRequestGrid = new GWTGrid();
        maxNumberOfUserRequestsSelector = new GWTPagedFormDropDownListBox<Integer>();
        userRequestGrid.addRow(new GWTLabel(I18nTranslator.getInstance().max_user_requests() + ":"), maxNumberOfUserRequestsSelector);
        _panel.add(userRequestGrid);

        noReplayTimeSelector = new GWTCheckBox(false);
        noReplayTimeSelector.setCaption(I18nTranslator.getInstance().no_replay_until_time());
        noReplayTimeThresholdBox = new GWTNumericBox(60, 1, Integer.MAX_VALUE);
        GWTGrid userRequestGrid2 = new GWTGrid();
        userRequestGrid2.addRow(
                noReplayTimeSelector,
                noReplayTimeThresholdBox,
                new GWTLabel(I18nTranslator.getInstance().minutes()));
        _panel.add(userRequestGrid2);

        GWTGrid userRequestGrid3 = new GWTGrid();
        noReplayPlaylistCheckBox = new GWTCheckBox(true);
        userRequestGrid3.addRow(new GWTLabel(I18nTranslator.getInstance().no_replay_within_playlist()), noReplayPlaylistCheckBox);
        _panel.add(userRequestGrid3);

        GWTGrid globalTagOnGrid = new GWTGrid();
        enableGlobalTagSetCheckBox = new GWTCheckBox(false);
        globalTagOnGrid.addRow(new GWTLabel(I18nTranslator.getInstance().activate_global_tagset() + ":"), enableGlobalTagSetCheckBox);

        _panel.add(globalTagOnGrid);
        
        GWTGrid commonTagOptionsGrid = new GWTGrid();
        authorTextBox = new GWTTextBox();
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().author() + ":"), authorTextBox);
        titleTextBox = new GWTTextBox();
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().title() + ":"), titleTextBox);
        albumTextBox = new GWTTextBox();
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().album() + ":"), albumTextBox);
        descriptionTextBox = new GWTExtendedTextBox();
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().comment() + ":"), descriptionTextBox);
        yearOfReleaseNumericBox = new GWTNullableNumericBox(null, -2000, +3000);
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().year() + ":"), yearOfReleaseNumericBox);
        genreTextBox = new GWTTextBox();
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().genre() + ":"), genreTextBox);

        advisoryAgeComboBox = new GWTNullableFormDropDownListBox<PEGI_AGE>();
        commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().rating() + ":"), advisoryAgeComboBox);

        _panel.add(commonTagOptionsGrid);

        advisoryFeaturesPanel = new GWTFlowPanel();
        advisoryViolenceCheckBox = new GWTCheckBox(false);
        advisoryViolenceCheckBox.setCaption(I18nTranslator.getInstance().violence_advisory());
        advisoryFeaturesPanel.add(advisoryViolenceCheckBox);
        advisoryProfanityCheckBox = new GWTCheckBox(false);
        advisoryProfanityCheckBox.setCaption(I18nTranslator.getInstance().profanity_advisory());
        advisoryFeaturesPanel.add(advisoryProfanityCheckBox);
        advisoryFearCheckBox = new GWTCheckBox(false);
        advisoryFearCheckBox.setCaption(I18nTranslator.getInstance().fear_advisory());
        advisoryFeaturesPanel.add(advisoryFearCheckBox);
        advisorySexCheckBox = new GWTCheckBox(false);
        advisorySexCheckBox.setCaption(I18nTranslator.getInstance().sex_advisory());
        advisoryFeaturesPanel.add(advisorySexCheckBox);
        advisoryDrugsCheckBox = new GWTCheckBox(false);
        advisoryDrugsCheckBox.setCaption(I18nTranslator.getInstance().drugs_advisory());
        advisoryFeaturesPanel.add(advisoryDrugsCheckBox);
        advisoryDiscriminationCheckBox = new GWTCheckBox(false);
        advisoryDiscriminationCheckBox.setCaption(I18nTranslator.getInstance().discrimination_advisory());
        advisoryFeaturesPanel.add(advisoryDiscriminationCheckBox);

        _panel.add(advisoryFeaturesPanel);

        miscOptionsPanel.add(_panel);

        playlistPanel.add(miscOptionsPanel);
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    public void toggleActivateLive() {
        liveBroadcasterBox.setEnabled(enableLiveCheckBox.isChecked());
        liveLoginBox.setEnabled(enableLiveCheckBox.isChecked());
        livePasswordBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisoryAgeComboBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisoryViolenceCheckBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisoryProfanityCheckBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisoryFearCheckBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisorySexCheckBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisoryDrugsCheckBox.setEnabled(enableLiveCheckBox.isChecked());
        liveAdvisoryDiscriminationCheckBox.setEnabled(enableLiveCheckBox.isChecked());
    }

    public void toggleActivateGlobalTagSet() {
        authorTextBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        titleTextBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        albumTextBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        descriptionTextBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        yearOfReleaseNumericBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        genreTextBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisoryAgeComboBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisoryViolenceCheckBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisoryProfanityCheckBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisoryFearCheckBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisorySexCheckBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisoryDrugsCheckBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
        advisoryDiscriminationCheckBox.setEnabled(enableGlobalTagSetCheckBox.isChecked());
    }

    public void onNoReplayTimeSelectorClicker() {
        noReplayTimeThresholdBox.setEnabled(noReplayTimeSelector.isChecked());
    }

    public void refreshSingleTrackContainer(GenericPanelInterface container, ActionCollectionEntry<BroadcastTrackForm> newTrackFormEntry, ClickHandler trackSelectionClickHandler, ClickHandler trackDisplayClickHandler) {
        if (container!=null) {

            container.clean();

            //If we're here then playlist can be added, no need to check if individula entries can be edited or inserted then

            //No track is selected yet
            if (newTrackFormEntry==null || newTrackFormEntry.getItem()==null) {
                GWTLabel label = new GWTLabel(I18nTranslator.getInstance().none());
                container.add(label);
                GWTClickableLabel changeButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add(), trackSelectionClickHandler);
                container.add(changeButton);
            //There's already a track, update the container with its reference accordingly
            } else {
                GenericLabelInterface label;
                //Only if you can see it, otherwise notify the user there's already a track actually, but he cannot see it
                if (newTrackFormEntry.isReadable()) {
                    label = new GWTClickableLabel(newTrackFormEntry.getItem().getFriendlyID(), trackDisplayClickHandler);
                } else {
                    label = new GWTLabel(null);
                }
                container.add(label);
                GWTClickableLabel changeButton = new GWTClickableLabelSelect(I18nTranslator.getInstance().change(), trackSelectionClickHandler);
                container.add(changeButton);
            }

        }
    }

    public void refreshFiltersContainer(GenericPanelInterface container, Collection<FilterForm> filters, ClickHandler filtersSelectionClickHandler) {
        if (container!=null) {

            container.clean();

            //If we're here then playlist can be added, no need to check if individula entries can be edited or inserted then

            //No filter
            if (filters==null || filters.isEmpty()) {
                GWTLabel label = new GWTLabel(I18nTranslator.getInstance().none());
                container.add(label);
                GWTClickableLabel changeButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add(), filtersSelectionClickHandler);
                container.add(changeButton);
            //There's already at least one registered filter, update the container with their references accordingly
            } else {
                GWTLabel label = new GWTLabel(
                        filters.size()>1 ?
                            I18nTranslator.getInstance().x_filters(String.valueOf(filters.size()))
                            :
                            I18nTranslator.getInstance().zero_or_one_filter(String.valueOf(filters.size())));
                container.add(label);
                GWTClickableLabel changeButton = new GWTClickableLabelSelect(I18nTranslator.getInstance().change(), filtersSelectionClickHandler);
                container.add(changeButton);
            }

        }
    }

    public void refreshTimetableSlotList(ActionCollection<TimetableSlotForm> timetableSlots) {
        //Clear the list beforehand
        timetableSlotComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<TimetableSlotForm> timetableSlotEntry : timetableSlots) {
            if (timetableSlotEntry.hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE)) {
                timetableSlotComboBox.addItem(timetableSlotEntry.getItem().getRefId().getSyntheticFriendlyID(),timetableSlotEntry.getItem());
            }
         }
    }

    public void refreshUserRequestsLimitList() {
        //Clear the list beforehand
        maxNumberOfUserRequestsSelector.clean();

        //Generate the list
        maxNumberOfUserRequestsSelector.addItem("0", 0);
        maxNumberOfUserRequestsSelector.addItem("1", 1);
        maxNumberOfUserRequestsSelector.addItem("2", 2);
        maxNumberOfUserRequestsSelector.addItem("3", 3);
        maxNumberOfUserRequestsSelector.addItem("4", 4);
        maxNumberOfUserRequestsSelector.addItem("5", 5);
        maxNumberOfUserRequestsSelector.addItem("10", 10);
        maxNumberOfUserRequestsSelector.addItem("20", 20);
        maxNumberOfUserRequestsSelector.addItem("50", 50);
        maxNumberOfUserRequestsSelector.addItem(I18nTranslator.getInstance().infinite(), null);

    }

    public void addNewTimetableSlot(TimetableSlotForm timetableSlot, ClickHandler deleteClickHandler, ClickHandler displayTimetableSlotClickHandler) {

        //Create the timetableSlot entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new timetableSlot to the list
        timetableSlotListBox.addRow(timetableSlot.getRefId(), false, new GWTClickableLabel(timetableSlot.getRefId().getSyntheticFriendlyID(), displayTimetableSlotClickHandler), operationPanel);
    }

    public void refreshAdvisoryAgeList(PEGI_AGE[] ages) {
        advisoryAgeComboBox.clean();
        for (PEGI_AGE age : ages) {
            String label = age != null ? age.getI18nFriendlyString() : "";
            advisoryAgeComboBox.addItem(label, age);
        }
    }

    public void toggleAdvisoryFeaturesPanel() {
        //No PEGI rating, i.e. null value, disables all the connected features
        advisoryFeaturesPanel.setVisible(advisoryAgeComboBox.getSelectedValue() != null);
    }

    public void refreshLiveAdvisoryAgeList(PEGI_AGE[] ages) {
        liveAdvisoryAgeComboBox.clean();
        for (PEGI_AGE age : ages) {
            String label = age != null ? age.getI18nFriendlyString() : "";
            liveAdvisoryAgeComboBox.addItem(label, age);
        }
    }

    public void toggleLiveAdvisoryFeaturesPanel() {
        //No PEGI rating, i.e. null value, disables all the connected features
        liveAdvisoryFeaturesPanel.setVisible(liveAdvisoryAgeComboBox.getSelectedValue() != null);
    }

}
