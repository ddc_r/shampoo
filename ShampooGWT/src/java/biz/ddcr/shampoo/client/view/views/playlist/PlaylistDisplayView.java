/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.playlist;

import biz.ddcr.shampoo.client.form.playlist.LiveForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistTagSetForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTImage;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTReadOnlyPlaylist;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedTree;
import biz.ddcr.shampoo.client.view.widgets.GWTTruncatedLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericImageInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTreeInterface;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class PlaylistDisplayView extends GenericFormPageView {

    //Cover art
    private GWTImage albumCoverPicture;
    //User widgets
    protected GWTLabel labelLabel;
    protected GWTTruncatedLabel descriptionLabel;
    protected GWTLabel programmeLabel;
    protected GWTDisclosurePanel livePanel;
    protected GWTLabel liveBroadcasterLabel;
    protected GWTLabel liveLoginLabel;
    protected GWTGrid liveRatingGrid;
    //Timetable slots widgets
    protected GWTPagedTree timetableSlotTree;
    //playlist widget
    protected GWTReadOnlyPlaylist playlistContent;
    //Misc options
    protected GWTVerticalPanel globalTagsPanel;
    protected GWTLabel maxNumberOfUserRequestsLabel;

    public GenericImageInterface getAlbumCoverPicture() {
        return albumCoverPicture;
    }

    public GenericLabelInterface getDescriptionLabel() {
        return descriptionLabel;
    }

    public GenericLabelInterface getLabelLabel() {
        return labelLabel;
    }

    public GenericLabelInterface getLiveBroadcasterLabel() {
        return liveBroadcasterLabel;
    }

    public GenericLabelInterface getLiveLoginLabel() {
        return liveLoginLabel;
    }

    public GenericPlaylistInterface getPlaylistContent() {
        return playlistContent;
    }

    public GenericLabelInterface getProgrammeLabel() {
        return programmeLabel;
    }

    public GenericTreeInterface getTimetableSlotTree() {
        return timetableSlotTree;
    }

    public GenericLabelInterface getMaxNumberOfUserRequestsLabel() {
        return maxNumberOfUserRequestsLabel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_playlist();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "playlist";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().playlist_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //Playlist widgets
        GWTVerticalPanel playlistPanel = new GWTVerticalPanel();

        GWTGrid tagGrid = new GWTGrid();

        GWTGrid mainGrid = new GWTGrid();

        labelLabel = new GWTLabel(null);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelLabel);

        descriptionLabel = new GWTTruncatedLabel(null);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionLabel);

        programmeLabel = new GWTLabel(null);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeLabel);

        //Cover art
        GWTFlowPanel newAlbumCoverArtPanel = new GWTFlowPanel();
        albumCoverPicture = new GWTImage();
        newAlbumCoverArtPanel.add(albumCoverPicture);

        tagGrid.addRow(mainGrid, newAlbumCoverArtPanel);

        playlistPanel.add(tagGrid);     

        //Live

        livePanel = new GWTDisclosurePanel(I18nTranslator.getInstance().live());
        GWTVerticalPanel liveSubPanel = new GWTVerticalPanel();
        GWTGrid liveCredentialsGrid = new GWTGrid();
        liveBroadcasterLabel = new GWTLabel(null);
        liveCredentialsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().broadcaster_name() + ":"), liveBroadcasterLabel);

        liveLoginLabel = new GWTLabel(null);
        liveCredentialsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().live_login() + ":"), liveLoginLabel);
        liveSubPanel.add(liveCredentialsGrid);

        //programmatically updated with the proper widget
        liveRatingGrid = new GWTGrid();
        liveSubPanel.add(liveRatingGrid);

        livePanel.add(liveSubPanel);

        playlistPanel.add(livePanel);

        //Timetable slot widget
        GWTDisclosurePanel ttsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().timetables_list());
        timetableSlotTree = new GWTPagedTree();
        ttsPanel.add(timetableSlotTree);

        playlistPanel.add(ttsPanel);

        //Playlist entries
        GWTDisclosurePanel entriesPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().playlist_items());
        playlistContent = new GWTReadOnlyPlaylist();
        entriesPanel.add(playlistContent);

        playlistPanel.add(entriesPanel);

        //misc options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        GWTGrid _tempPanel1 = new GWTGrid();
        maxNumberOfUserRequestsLabel = new GWTLabel(null);
        _tempPanel1.addRow(new GWTLabel(I18nTranslator.getInstance().max_user_requests() + ":"), maxNumberOfUserRequestsLabel);
        miscOptionsGrid.addRow(_tempPanel1);

        //programmatically updated with the proper widget
        globalTagsPanel = new GWTVerticalPanel();
        miscOptionsGrid.addRow(globalTagsPanel);

        miscOptionsPanel.add(miscOptionsGrid);
        playlistPanel.add(miscOptionsPanel);

        return playlistPanel;
    }

    //Respond to presenter requests
    public void loadCoverArt(HasCoverArtInterface module) {
        albumCoverPicture.setImage(module);
    }

    public void refreshExistingTimetableSlots(ActionCollection<TimetableSlotForm> timetableSlots) {
        //clear the list beforehand
        timetableSlotTree.clean();

        //generate the list
        for (ActionCollectionEntry<TimetableSlotForm> timetableSlotEntry : timetableSlots) {
            if (timetableSlotEntry.isReadable()) {
                addNewTimetableSlot(timetableSlotEntry.getItem());
            }
        }
    }

    public void addNewTimetableSlot(TimetableSlotForm ttsForm) {
        //Add the new tts to the list
        Long newBranchId = timetableSlotTree.addItem(
                new GWTLabel(ttsForm.getRefId().getSchedulingTime().getI18nSyntheticFriendlyString()));
        timetableSlotTree.addItem(
                new GWTLabel(I18nTranslator.getInstance().max_end() + ": " + ttsForm.getEndTime().getI18nSyntheticFriendlyString()),
                newBranchId);
    }

    public void refreshExistingLive(LiveForm liveForm) {
        livePanel.setVisible(liveForm != null);
        liveBroadcasterLabel.setCaption(liveForm != null ? liveForm.getBroadcaster() : null);
        liveLoginLabel.setCaption(liveForm != null ? liveForm.getLogin() : null);
    }

    public void refreshSingleTrackContainer(GenericPanelInterface container, ActionCollectionEntry<BroadcastTrackForm> newTrackFormEntry) {
        if (container != null) {

            container.clean();

            //If we're here then playlist can be added, no need to check if individula entries can be edited or inserted then

            //No track is selected
            if (newTrackFormEntry == null || newTrackFormEntry.getItem() == null) {
                GWTLabel label = new GWTLabel(I18nTranslator.getInstance().no_track());
                container.add(label);
                //There's already a track, update the container with its reference accordingly
            } else {
                GenericLabelInterface label;
                //Only if you can see it, otherwise notify the user there's already a track actually, but he cannot see it
                if (newTrackFormEntry.isReadable()) {
                    label = new GWTLabel(newTrackFormEntry.getItem().getFriendlyID());
                } else {
                    label = new GWTLabel(null);
                }
                container.add(label);
            }

        }
    }

    public void refreshFiltersContainer(GenericPanelInterface container, Collection<FilterForm> filters) {
        if (container != null) {

            container.clean();

            //If we're here then playlist can be added, no need to check if individula entries can be edited or inserted then

            if (filters != null) {
                GWTGrid filterGrid = new GWTGrid();

                for (FilterForm filter : filters) {
                    //Add the new channel to the list
                    filterGrid.addRow(new GWTLabel(filter.getI18nFriendlyString()));
                }

                container.add(filterGrid);
            }

        }
    }

    public void refreshGlobalTagsData(PlaylistTagSetForm playlistTagSetForm) {

        globalTagsPanel.setVisible(playlistTagSetForm != null);

        globalTagsPanel.clean();

        if (playlistTagSetForm != null) {

            GWTLabel globalTagsLabel = new GWTLabel(I18nTranslator.getInstance().activate_global_tagset());
            globalTagsPanel.add(globalTagsLabel);

            GWTGrid commonTagOptionsGrid = new GWTGrid();
            GWTTruncatedLabel authorLabel = new GWTTruncatedLabel(playlistTagSetForm.getAuthor());
            commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().author() + ":"), authorLabel);
            GWTTruncatedLabel titleLabel = new GWTTruncatedLabel(playlistTagSetForm.getTitle());
            commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().title() + ":"), titleLabel);
            GWTTruncatedLabel albumLabel = new GWTTruncatedLabel(playlistTagSetForm.getAlbum());
            commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().album() + ":"), albumLabel);
            GWTTruncatedLabel commentLabel = new GWTTruncatedLabel(playlistTagSetForm.getDescription());
            commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().comment() + ":"), commentLabel);
            GWTLabel yearOfReleaseLabel = new GWTLabel(
                    playlistTagSetForm.getYearOfRelease() != null
                    ? playlistTagSetForm.getYearOfRelease().toString()
                    : I18nTranslator.getInstance().unknown());
            commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().year() + ":"), yearOfReleaseLabel);
            GWTTruncatedLabel genreLabel = new GWTTruncatedLabel(playlistTagSetForm.getGenre());
            commonTagOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().genre() + ":"), genreLabel);

            GWTPagedTree advisoryTree = new GWTPagedTree();
            if (playlistTagSetForm.getAdvisory()!=null && playlistTagSetForm.getAdvisory().getRating()!=null) {
                Long newBranchId = advisoryTree.addItem(new GWTLabel(playlistTagSetForm.getAdvisory().getRating().getI18nFriendlyString()));
                if (playlistTagSetForm.getAdvisory().hasDiscrimination())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().discrimination_advisory()), newBranchId);
                if (playlistTagSetForm.getAdvisory().hasDrugs())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().drugs_advisory()), newBranchId);
                if (playlistTagSetForm.getAdvisory().hasFear())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().fear_advisory()), newBranchId);
                if (playlistTagSetForm.getAdvisory().hasProfanity())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().profanity_advisory()), newBranchId);
                if (playlistTagSetForm.getAdvisory().hasSex())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().sex_advisory()), newBranchId);
                if (playlistTagSetForm.getAdvisory().hasViolence())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().violence_advisory()), newBranchId);
            } else {
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().unrated()));
            }
            commonTagOptionsGrid.addRow(
                    new GWTLabel(I18nTranslator.getInstance().rating() + ":"),
                    advisoryTree);

            globalTagsPanel.add(commonTagOptionsGrid);

        }

    }

    public void refreshLiveRatingData(PEGIRatingModule ratingForm) {

        liveRatingGrid.clean();

            GWTPagedTree advisoryTree = new GWTPagedTree();
            if (ratingForm!=null && ratingForm.getRating()!=null) {
                Long newBranchId = advisoryTree.addItem(new GWTLabel(ratingForm.getRating().getI18nFriendlyString()));
                if (ratingForm.hasDiscrimination())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().discrimination_advisory()), newBranchId);
                if (ratingForm.hasDrugs())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().drugs_advisory()), newBranchId);
                if (ratingForm.hasFear())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().fear_advisory()), newBranchId);
                if (ratingForm.hasProfanity())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().profanity_advisory()), newBranchId);
                if (ratingForm.hasSex())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().sex_advisory()), newBranchId);
                if (ratingForm.hasViolence())
                    advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().violence_advisory()), newBranchId);
            } else {
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().unrated()));
            }
            liveRatingGrid.addRow(
                    new GWTLabel(I18nTranslator.getInstance().rating() + ":"),
                    advisoryTree);

    }

}
