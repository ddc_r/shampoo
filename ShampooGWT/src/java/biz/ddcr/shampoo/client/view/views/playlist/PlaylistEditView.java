/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.playlist;


import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;


/**
 *
 * @author okay_awright
 **/
public class PlaylistEditView extends PlaylistAddView {

    //Setup minimum view

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().edit_playlist();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Playlist widgets
        GWTVerticalPanel playlistPanel = new GWTVerticalPanel();

        GWTGrid tagGrid = new GWTGrid();

        GWTGrid mainGrid = new GWTGrid();

        labelBox = new GWTTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelBox);

        descriptionBox = new GWTExtendedTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionBox);

        programmeLabel = new GWTLabel(null);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeLabel);

        enableLiveCheckBox = new GWTCheckBox(false);
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().activate_live() + ":"), enableLiveCheckBox);

        liveBroadcasterBox = new GWTTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().broadcaster_name() + ":"), liveBroadcasterBox);

        liveLoginBox = new GWTTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().live_login() + ":"), liveLoginBox);

        livePasswordBox = new GWTPasswordTextBox();
        mainGrid.addRow(new GWTLabel(I18nTranslator.getInstance().live_password() + ":"), livePasswordBox);

        bindLiveRatingPanel(mainGrid);

        //Cover art
        GWTFlowPanel newAlbumCoverArtPanel = new GWTFlowPanel();
        albumCoverPicture = new GWTClickableUncachableImage();
        newAlbumCoverArtPanel.add(albumCoverPicture);

        albumCoverUploadPlupload = new GWTPictureUploadMoxie(null);
        resetAlbumCoverArtLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        newAlbumCoverArtPanel.add(albumCoverUploadPlupload);
        newAlbumCoverArtPanel.add(resetAlbumCoverArtLink);

        tagGrid.addRow(mainGrid, newAlbumCoverArtPanel);

        playlistPanel.add(tagGrid);

        //Timetable slot widget
        //Programme widgets
        GWTVerticalPanel timetableSlotPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel timetableSlotLabel = new GWTLabel(I18nTranslator.getInstance().timetables_list());
        timetableSlotPanel.add(timetableSlotLabel);

        GWTVerticalPanel timetableSlotContainerPanel = new GWTVerticalPanel();

        GWTGrid newTimetableSlotEntryPanel = new GWTGrid();

        GWTLabel timetableSlotComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().timetable());
        timetableSlotComboBox = new GWTPagedFormDropDownListBox<TimetableSlotForm>();

        addNewTimetableSlotButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newTimetableSlotEntryPanel.addRow(timetableSlotComboBoxCaption);
        newTimetableSlotEntryPanel.addRow(
                timetableSlotComboBox,
                addNewTimetableSlotButton);
        timetableSlotContainerPanel.add(newTimetableSlotEntryPanel);

        timetableSlotListBox = new GWTPagedFormTable<TimetableSlotFormID, Widget>();
        timetableSlotContainerPanel.add(timetableSlotListBox);
        timetableSlotPanel.add(timetableSlotContainerPanel);

        playlistPanel.add(timetableSlotPanel);

        //Playlist widget
        GWTGrid itemsGrid = new GWTGrid();

        itemsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().playlist_items()));
        playlistContent = new GWTEditablePlaylist();
        itemsGrid.addRow(playlistContent);

        playlistPanel.add(itemsGrid);

        bindMiscOptionsPanel(playlistPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        playlistPanel.add(submit);

        return playlistPanel;
    }

    //Respond to presenter requests
    public void refreshExistingTimetableSlots(ActionCollection<TimetableSlotForm> timetableSlots, ClickHandler deleteClickHandler, ClickHandler displayTimetableSlotClickHandler) {
        //clear the list beforehand
        timetableSlotListBox.clean();

        //generate the list
        for (ActionCollectionEntry<TimetableSlotForm> timetableSlotEntry : timetableSlots) {
            if (timetableSlotEntry.hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE)) {
                addNewTimetableSlot(timetableSlotEntry.getItem(), deleteClickHandler, displayTimetableSlotClickHandler);
            }
         }
    }

    public void setDefaultMaxNumberOfUserRequestsValue(Integer value) {
        if (value != null) {
            switch (value) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 10:
                case 20:
                case 50:
                case 0:
                    maxNumberOfUserRequestsSelector.setSelectedValue(value);
                    break;
                default:
                    /** default value **/
                    maxNumberOfUserRequestsSelector.setSelectedValue(1);
            }
        } else {
            maxNumberOfUserRequestsSelector.setSelectedValue(null);
        }
    }

}
