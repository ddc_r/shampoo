/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.playlist;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class PlaylistListView extends PageView {

    //The programme that is associated to the playlists
    GWTPagedFormDropDownListBox<ProgrammeForm> programmeComboBox;
    //List
    GWTVerticalPanel listPanel;
    GWTFormSortableTable<PlaylistForm, Widget, PLAYLIST_TAGS> playlistList;
    GWTClickableLabel addPlaylistLink;

    public GenericFormSortableTableInterface<PlaylistForm, Widget, PLAYLIST_TAGS> getPlaylistList() {
        return playlistList;
    }

    public GenericDropDownListBoxInterface<ProgrammeForm> getProgrammeComboBox() {
        return programmeComboBox;
    }

    public GenericPanelInterface getListPanel() {
        return listPanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().playlist_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "playlist";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid1 = new GWTGrid();
        programmeComboBox = new GWTPagedFormDropDownListBox<ProgrammeForm>();
        grid1.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeComboBox);
        mainPanel.add(grid1);

        listPanel = new GWTVerticalPanel();
        mainPanel.add(listPanel);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests

    public void refreshPlaylistPanel(SortableTableClickHandler listHeaderClickHandler) {

        playlistList = new GWTFormSortableTable<PlaylistForm, Widget, PLAYLIST_TAGS>();
        //Generate list header
        playlistList.setHeader(
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().cover_art()),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().label(), PLAYLIST_TAGS.label),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().description(), PLAYLIST_TAGS.description),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().live(), PLAYLIST_TAGS.live),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().timetables(), PLAYLIST_TAGS.timetableSlots),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().operation())
                );
        playlistList.setRefreshClickHandler(listHeaderClickHandler);

        listPanel.add(playlistList);
        
    }

    public void refreshAddPlaylistLink(ClickHandler clickHandler) {
        //Add a regular user
        addPlaylistLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_playlist());
        if (clickHandler != null) {
            addPlaylistLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addPlaylistLink);
    }

    public void refreshProgrammeList(ActionCollection<ProgrammeForm> programmes) {
        //Clear the list beforehand
        programmeComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {

            if (programmeEntry.isReadable()) {
                ProgrammeForm programme = programmeEntry.getKey();

                programmeComboBox.addItem(programme.getLabel(), programme);
            }
        }

    }

    //List
    public void refreshPlaylistList(ActionCollection<PlaylistForm> playlists,
            ClickHandler editClickHandler,
            YesNoClickHandler deleteClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            ClickHandler playlistDisplayClickHandler,
            final SerializableItemSelectionHandler<TimetableSlotFormID> timetableSlotDisplaySelectionHandler
            ) {

        //Clear the list beforehand
        playlistList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        playlistList.setNextButtonVisible((playlists.size() >= playlistList.getMaxVisibleRows()));

        playlistList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<PlaylistForm> playlistEntry : playlists) {

            if (playlistEntry.isReadable()) {
                PlaylistForm playlist = playlistEntry.getKey();

                Widget coverArtWidget = null;
                if (playlist.isReady()) {
                    coverArtWidget = new GWTThumbnail(playlist.getCoverArtFile());
                } else {
                    coverArtWidget = new GWTCSSFlowPanel("GWTWorking");
                }

                GWTPagedSimpleLayer timetableWidget = new GWTPagedSimpleLayer();
                for (final ActionCollectionEntry<TimetableSlotFormID> timetableEntry : playlist.getTimetableSlotIds()) {
                    if (timetableEntry.isReadable()) {
                        String slotId = timetableEntry.getItem().getSyntheticFriendlyID();
                        ClickHandler timetableSlotClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    timetableSlotDisplaySelectionHandler.onItemSelected(timetableEntry.getItem());
                                }
                            };
                        timetableWidget.addRow(new GWTClickableLabel(slotId, timetableSlotClickHandler));
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (playlist.isReady() &&//
                        playlistEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(playlist.getFriendlyID()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                if (playlist.isReady() &&//
                        playlistEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                }

                playlistList.addRow(playlist,
                        playlist.isReady() && playlistEntry.isDeletable(),
                        coverArtWidget,
                        new GWTClickableLabel(playlist.getFriendlyID(), playlistDisplayClickHandler),
                        new GWTTruncatedLabel(playlist.getDescription()),
                        new GWTLabel(playlist.getLive()!=null ? I18nTranslator.getInstance().is_true() : I18nTranslator.getInstance().is_false()),
                        timetableWidget,
                        operationPanel);
            }
        }

    }

}
