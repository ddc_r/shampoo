/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.programme;

import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.*;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeDisplayView extends GenericFormPageView {

    //Programme widgets
    private GWTLabel labelLabel;
    private GWTTruncatedLabel descriptionLabel;
    //Programmes widgets
    private GWTPagedSimpleLayer channelsGrid;
    //Rights widgets
    private GWTPagedTree rightsTree;

    public GenericLabelInterface getDescriptionLabel() {
        return descriptionLabel;
    }

    public GenericLabelInterface getLabelLabel() {
        return labelLabel;
    }

    public GenericSimpleLayerInterface getChannelsGrid() {
        return channelsGrid;
    }

    public GenericTreeInterface getRightsTree() {
        return rightsTree;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_programme();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "programme";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().programme_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //Programme widgets
        GWTVerticalPanel programmePanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        labelLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelLabel);

        descriptionLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionLabel);

        programmePanel.add(grid);

        //Channel widgets
        GWTDisclosurePanel channelPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().channels_list());
        channelsGrid = new GWTPagedSimpleLayer();
        channelPanel.add(channelsGrid);

        programmePanel.add(channelPanel);

        //Right widgets
        GWTDisclosurePanel rightPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().rights_list());
        rightsTree = new GWTPagedTree();
        rightPanel.add(rightsTree);

        programmePanel.add(rightPanel);

        return programmePanel;
    }

    //Respond to presenter requests

    public void refreshExistingRights(ActionCollection<RightForm> rightForms) {
        //clear the list beforehand
        rightsTree.clean();

        //generate the list
        for (ActionCollectionEntry<RightForm> rightEntry : rightForms) {
            if (rightEntry.isReadable()) {
                addNewRight(rightEntry.getKey());
            }
         }
    }

    public void addNewRight(RightForm rightForm) {

        //Add the new right to the list
        Long newBranchId = rightsTree.getItemByName(rightForm.getRole().getI18nFriendlyString());
        if (newBranchId==null)
            newBranchId = rightsTree.addItemWithName(rightForm.getRole().getI18nFriendlyString(), new GWTLabel(rightForm.getRole().getI18nFriendlyString()));
        rightsTree.addItem(new GWTLabel(rightForm.getRestrictedUserId()), newBranchId);
    }

    public void refreshExistingChannels(ActionCollection<String> channelIds) {
        //clear the list beforehand
        channelsGrid.clean();

        //generate the list
        for (ActionCollectionEntry<String> channelIdEntry : channelIds) {
            if (channelIdEntry.isReadable()) {
                addNewChannel(channelIdEntry.getKey());
            }
         }
    }

    public void addNewChannel(String newChannelId) {

        //Add the new channel to the list
        channelsGrid.addRow(new GWTLabel(newChannelId));
    }

}
