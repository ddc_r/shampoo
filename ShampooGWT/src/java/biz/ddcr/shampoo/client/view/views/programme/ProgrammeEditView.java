/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.programme;

import biz.ddcr.shampoo.client.form.channel.CHANNEL_ACTION;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.form.user.RESTRICTEDUSER_ACTION;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeEditView extends PageView {

    //Programme widgets
    private GWTLabel labelLabel;
    private GWTExtendedTextBox descriptionTextBox;
    //Programmes widgets
    private GWTPagedFormDropDownListBox<ChannelForm> channelComboBox;
    private GWTClickableLabel addNewChannelButton;
    private GWTPagedFormTable<ChannelForm, Widget> channelsListBox;
    //Rights widgets
    private GWTPagedFormDropDownListBox<Role> roleRightComboBox;
    private GWTPagedFormDropDownListBox<RestrictedUserForm> userRightComboBox;
    private GWTClickableLabel addNewRightButton;
    private GWTPagedFormTable<RightForm, Widget> rightsListBox;
    //Misc. options
    private GWTTextBox metadataPatternTextBox;
    private GWTCheckBox overlappingPlaylistCheckBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericTextBoxInterface getMetadataPatternTextBox() {
        return metadataPatternTextBox;
    }

    public GenericCheckBoxInterface getOverlappingPlaylistCheckBox() {
        return overlappingPlaylistCheckBox;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericTextBoxInterface getDescriptionTextBox() {
        return descriptionTextBox;
    }

    public GenericLabelInterface getLabelLabel() {
        return labelLabel;
    }

    public GenericDropDownListBoxInterface<ChannelForm> getChannelComboBox() {
        return channelComboBox;
    }

    public GenericFormTableInterface<ChannelForm, Widget> getChannelsListBox() {
        return channelsListBox;
    }

    public GenericDropDownListBoxInterface<Role> getRoleRightComboBox() {
        return roleRightComboBox;
    }

    public GenericFormTableInterface<RightForm, Widget> getRightsListBox() {
        return rightsListBox;
    }

    public GenericDropDownListBoxInterface<RestrictedUserForm> getUserRightComboBox() {
        return userRightComboBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getAddNewRightButton() {
        return addNewRightButton;
    }

    public GenericHyperlinkInterface getAddNewChannelButton() {
        return addNewChannelButton;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().edit_programme();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "programme";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().programme_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Programme widgets
        GWTVerticalPanel programmePanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        labelLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().label() + ":"), labelLabel);

        descriptionTextBox = new GWTExtendedTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().description() + ":"), descriptionTextBox);

        programmePanel.add(grid);

        //Channel widgets
        GWTVerticalPanel channelPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel channelLabel = new GWTLabel(I18nTranslator.getInstance().channels_list());
        channelPanel.add(channelLabel);

        GWTVerticalPanel channelContainerPanel = new GWTVerticalPanel();

        GWTGrid newChannelEntryPanel = new GWTGrid();

        GWTLabel channelComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().programme());
        channelComboBox = new GWTPagedFormDropDownListBox<ChannelForm>();

        addNewChannelButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newChannelEntryPanel.addRow(channelComboBoxCaption);
        newChannelEntryPanel.addRow(
                channelComboBox,
                addNewChannelButton);
        channelContainerPanel.add(newChannelEntryPanel);

        channelsListBox = new GWTPagedFormTable<ChannelForm, Widget>();
        channelContainerPanel.add(channelsListBox);
        channelPanel.add(channelContainerPanel);

        programmePanel.add(channelPanel);

        //Right widgets
        GWTVerticalPanel rightPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel rightLabel = new GWTLabel(I18nTranslator.getInstance().rights_list());
        rightPanel.add(rightLabel);

        GWTVerticalPanel containerPanel = new GWTVerticalPanel();

        GWTGrid newEntryPanel = new GWTGrid();

        GWTLabel firstComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().role());
        roleRightComboBox = new GWTPagedFormDropDownListBox<Role>();

        GWTLabel secondComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().restricted_user());
        userRightComboBox = new GWTPagedFormDropDownListBox<RestrictedUserForm>();

        addNewRightButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newEntryPanel.addRow(
                firstComboBoxCaption,
                secondComboBoxCaption);
        newEntryPanel.addRow(
                roleRightComboBox,
                userRightComboBox,
                addNewRightButton);
        containerPanel.add(newEntryPanel);

        rightsListBox = new GWTPagedFormTable<RightForm, Widget>();
        rightsListBox.setHeader(firstComboBoxCaption.getCaption(), secondComboBoxCaption.getCaption());
        containerPanel.add(rightsListBox);
        rightPanel.add(containerPanel);

        programmePanel.add(rightPanel);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        metadataPatternTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().metadata_pattern() + ":"), metadataPatternTextBox);
        overlappingPlaylistCheckBox = new GWTCheckBox(false); //false by default
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().overlapping_playlist_allowed() + ":"), overlappingPlaylistCheckBox);

        miscOptionsPanel.add(miscOptionsGrid);

        programmePanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        programmePanel.add(submit);

        return programmePanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests

    public void refreshRestrictedUserRightList(ActionCollection<RestrictedUserForm> users) {
        //Clear the list beforehand
        userRightComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<RestrictedUserForm> userEntry : users) {
            if (userEntry.hasAction(RESTRICTEDUSER_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE)) {
                userRightComboBox.addItem(userEntry.getKey().getUsername(),userEntry.getKey());
            }
         }

    }

    public void refreshRoleRightList(Role[] roles) {
        //Clear the list beforehand
        roleRightComboBox.clean();

        //Generate the list
        for (Role role : roles)
                roleRightComboBox.addItem(role.getI18nFriendlyString(), role);
    }

    public void refreshChannelList(ActionCollection<ChannelForm> channelIds) {
        //Clear the list beforehand
        channelComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channelIds) {
            if (channelEntry.hasAction(CHANNEL_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE)) {
                channelComboBox.addItem(channelEntry.getKey().getLabel(),channelEntry.getKey());
            }
         }
    }

    public void refreshExistingRights(ActionCollection<RightForm> rightForms, ClickHandler deleteClickHandler, ClickHandler displayRestrictedUserClickHandler) {
        //clear the list beforehand
        rightsListBox.clean();

        //generate the list
        for (ActionCollectionEntry<RightForm> rightEntry : rightForms) {
            if (rightEntry.hasAction(RESTRICTEDUSER_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE)) {
                addNewRight(rightEntry.getKey(),deleteClickHandler, displayRestrictedUserClickHandler);
            }
         }
    }

    public void addNewRight(RightForm right, ClickHandler deleteClickHandler, ClickHandler displayRestrictedUserClickHandler) {

        //Create the right entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new right to the list
        rightsListBox.addRow(right, false, new GWTLabel(right.getRole().getI18nFriendlyString()), new GWTClickableLabel(right.getRestrictedUserId(), displayRestrictedUserClickHandler), operationPanel);
    }

    public void refreshExistingChannels(ActionCollection<ChannelForm> channelIds, ClickHandler deleteClickHandler, ClickHandler displayChannelClickHandler) {
        //clear the list beforehand
        channelsListBox.clean();

        //generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channelIds) {
            if (channelEntry.hasAction(CHANNEL_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE)) {
                addNewChannel(channelEntry.getKey(),deleteClickHandler,displayChannelClickHandler);
            }
         }
    }

    public void addNewChannel(ChannelForm newChannel, ClickHandler deleteClickHandler, ClickHandler displayChannelClickHandler) {

        //Create the channel entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new channel to the list
        channelsListBox.addRow(newChannel, false, new GWTClickableLabel(newChannel.getLabel(),displayChannelClickHandler), operationPanel);
    }

}
