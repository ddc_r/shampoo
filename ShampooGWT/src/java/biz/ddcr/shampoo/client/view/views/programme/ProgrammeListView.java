/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.programme;

import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeListView extends PageView {

    GWTVerticalPanel programmePanel;
    GWTClickableLabel addProgrammeLink;
    GWTFormSortableTable<ProgrammeForm, Widget, PROGRAMME_TAGS> programmeList;

    public GenericFormSortableTableInterface<ProgrammeForm, Widget, PROGRAMME_TAGS> getProgrammeList() {
        return programmeList;
    }

    public GenericHyperlinkInterface getAddProgrammeLink() {
        return addProgrammeLink;
    }

    public GenericPanelInterface getProgrammePanel() {
        return programmePanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().programme_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "programme";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        programmePanel = new GWTVerticalPanel();

        return programmePanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests

    public void refreshProgrammePanel(SortableTableClickHandler listHeaderClickHandler) {
        programmeList = new GWTFormSortableTable<ProgrammeForm, Widget, PROGRAMME_TAGS>();
        //Generate list header
        programmeList.setHeader(
                new ColumnHeader<PROGRAMME_TAGS>(I18nTranslator.getInstance().label(), PROGRAMME_TAGS.label),
                new ColumnHeader<PROGRAMME_TAGS>(I18nTranslator.getInstance().description(), PROGRAMME_TAGS.description),
                new ColumnHeader<PROGRAMME_TAGS>(I18nTranslator.getInstance().channels(), PROGRAMME_TAGS.channels),
                new ColumnHeader<PROGRAMME_TAGS>(I18nTranslator.getInstance().rights(), PROGRAMME_TAGS.rights),
                new ColumnHeader<PROGRAMME_TAGS>(I18nTranslator.getInstance().operation())
                );
        programmeList.setRefreshClickHandler(listHeaderClickHandler);

        programmePanel.add(programmeList);
    }

    public void refreshAddProgrammeLink(ClickHandler clickHandler) {
        //Add a regular user
        addProgrammeLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_programme());
        if (clickHandler != null) {
            addProgrammeLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addProgrammeLink);
    }

    public void refreshProgrammeList(ActionCollection<ProgrammeForm> programmes, ClickHandler editClickHandler, YesNoClickHandler deleteClickHandler, final YesNoClickHandler deleteAllClickHandler, ClickHandler programmeDisplayClickHandler, final SerializableItemSelectionHandler<String> channelDisplaySelectionHandler, final SerializableItemSelectionHandler<String> restrictedUserDisplaySelectionHandler) {

        //Clear the list beforehand
        programmeList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        programmeList.setNextButtonVisible((programmes.size()>=programmeList.getMaxVisibleRows()));

        programmeList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {

            if (programmeEntry.isReadable()) {
                ProgrammeForm programme = programmeEntry.getKey();

                GWTPagedSimpleLayer channelWidget = new GWTPagedSimpleLayer();
                for (ActionCollectionEntry<String> channelEntry : programme.getChannels()) {
                    if (channelEntry.isReadable()) {
                        final String channelId = channelEntry.getKey();
                        ClickHandler channelClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    channelDisplaySelectionHandler.onItemSelected(channelId);
                                }
                            };
                        channelWidget.addRow(new GWTClickableLabel(channelId, channelClickHandler));
                    }
                }

                GWTPagedTree rightWidget = new GWTPagedTree();
                for (ActionCollectionEntry<RightForm> rightEntry : programme.getRightForms()) {
                    if (rightEntry.isReadable()) {
                        final RightForm right = rightEntry.getKey();
                        Long newBranchId = rightWidget.getItemByName(right.getRole().getI18nFriendlyString());
                        if (newBranchId==null)
                            newBranchId = rightWidget.addItemWithName(right.getRole().getI18nFriendlyString(), new GWTLabel(right.getRole().getI18nFriendlyString()));
                        ClickHandler rightClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    restrictedUserDisplaySelectionHandler.onItemSelected(right.getRestrictedUserId());
                                }
                            };
                        rightWidget.addItem(new GWTClickableLabel(right.getRestrictedUserId(), rightClickHandler), newBranchId);
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (programmeEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(programme.getLabel()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                if (programmeEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                }

                programmeList.addRow(programme,
                        programmeEntry.isDeletable(),
                        new GWTClickableLabel(programme.getLabel(), programmeDisplayClickHandler),
                        new GWTTruncatedLabel(programme.getDescription()),
                        channelWidget,
                        rightWidget,
                        operationPanel);
            }
        }

    }
}
