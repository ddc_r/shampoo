/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.queue;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.STREAMER_ACTION;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.queue.BlankQueueForm;
import biz.ddcr.shampoo.client.form.queue.BroadcastableTrackQueueForm;
import biz.ddcr.shampoo.client.form.queue.LiveQueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm;
import biz.ddcr.shampoo.client.form.queue.RequestQueueForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class QueueListView extends PageView {

    //The channel that is associated to the archives
    GWTPagedFormDropDownListBox<ChannelForm> channelComboBox;
    GWTGrid listPanel;
    GWTFormTable<QueueForm, Widget> queueList;
    GWTPagedFormDropDownListBox<String> timezoneList;
    GWTStreamingStatusBox streamerStatusWidget;

    public GenericFormTableInterface<QueueForm, Widget> getQueueList() {
        return queueList;
    }

    public GenericGridInterface getListPanel() {
        return listPanel;
    }

    public GenericDropDownListBoxInterface<ChannelForm> getChannelComboBox() {
        return channelComboBox;
    }

    public GenericDropDownListBoxInterface<String> getTimezoneList() {
        return timezoneList;
    }

    public GenericStatusBoxInterface<String, STATUS> getStreamerStatusWidget() {
        return streamerStatusWidget;
    }

    public void setStreamerStatusWidget(GWTStreamingStatusBox streamerStatusWidget) {
        this.streamerStatusWidget = streamerStatusWidget;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().queue_heading();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "queue";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid1 = new GWTGrid();
        channelComboBox = new GWTPagedFormDropDownListBox<ChannelForm>();
        streamerStatusWidget = new GWTStreamingStatusBox();
        grid1.addRow(
                new GWTLabel(I18nTranslator.getInstance().channel() + ":"),
                channelComboBox,
                streamerStatusWidget);
        mainPanel.add(grid1);

        listPanel = new GWTGrid();
        mainPanel.add(listPanel);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void refreshQueuePanel(ChangeHandler listTimezoneChangeHandler) {
        streamerStatusWidget.clean();

        //This table cannot be sorted by users
        queueList = new GWTFormTable<QueueForm, Widget>();
        queueList.setHeader(
                I18nTranslator.getInstance().cover_art(),
                I18nTranslator.getInstance().time(),
                I18nTranslator.getInstance().type(),
                I18nTranslator.getInstance().programme(),
                I18nTranslator.getInstance().playlist(),
                I18nTranslator.getInstance().duration(),
                I18nTranslator.getInstance().label(),
                I18nTranslator.getInstance().average_vote(),
                I18nTranslator.getInstance().my_vote(),
                I18nTranslator.getInstance().operation());
        listPanel.addRow(queueList);

        GWTGrid timezoneGrid = new GWTGrid();
        timezoneList = new GWTPagedFormDropDownListBox<String>();
        timezoneList.addChangeHandler(listTimezoneChangeHandler);
        timezoneGrid.addRow(
                new GWTLabel(I18nTranslator.getInstance().timezone() + ":"),
                timezoneList);
        listPanel.addRow(timezoneGrid);
    }

    public void refreshListTimezoneList(String channelTimezone, String userTimezone) {
        //Clear the list beforehand
        timezoneList.clean();

        //Generate the list
        if (channelTimezone != null) {
            timezoneList.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezone + ")",
                    channelTimezone);
        }
        if (userTimezone != null && !userTimezone.equals(channelTimezone)) {
            timezoneList.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezone + ")",
                    userTimezone);
        }
    }

    public void refreshStreamerStatus(String streamerId, ActionCollectionEntry<StreamerModule> statusEntry) {
        if (streamerId != null && statusEntry != null && (statusEntry.isReadable() || statusEntry.hasAction(STREAMER_ACTION.LISTEN))) {
            streamerStatusWidget.setID(streamerId);
            if (statusEntry.getItem()!=null) {
                if (statusEntry.getItem().isStreaming()!=null)
                    streamerStatusWidget.setStatus(statusEntry.getItem().isStreaming() ? STATUS.onair : STATUS.offair);
                else
                    streamerStatusWidget.setStatus(STATUS.reserved);
            } else 
                streamerStatusWidget.setStatus(STATUS.free);
        } else {
            streamerStatusWidget.clean();
        }
    }

    public void refreshChannelList(ActionCollection<ChannelForm> channels) {
        //Clear the list beforehand
        channelComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channels) {

            if (channelEntry.isReadable()) {
                ChannelForm channel = channelEntry.getKey();

                channelComboBox.addItem(channel.getLabel(), channel);
            }
        }

    }

    public void refreshQueueList(ActionCollectionEntry<? extends QueueForm> nowPlayingItem, ActionCollection<? extends QueueForm> queueItems, VoteChangeHandler<String> myRatingChangeHandler, ClickHandler trackDisplayClickHandler, ClickHandler programmeDisplayClickHandler) {

        //Clear the list beforehand
        queueList.clean();

        QueueForm queue;
        int i = 0;
        //Draw the nw playing item
        if (nowPlayingItem != null && nowPlayingItem.isReadable()) {
            queue = nowPlayingItem.getKey();
            drawRow(
                    i++,
                    "nowPlaying",
                    queue,
                    myRatingChangeHandler,
                    trackDisplayClickHandler,
                    programmeDisplayClickHandler);
        }
        //And now the queue items
        for (ActionCollectionEntry<? extends QueueForm> queueEntry : queueItems) {
            if (queueEntry.isReadable()) {
                queue = queueEntry.getKey();
                drawRow(
                        i++,
                        queue.isPooled() ? "pooled" : null,
                        queue,
                        myRatingChangeHandler,
                        trackDisplayClickHandler,
                        programmeDisplayClickHandler);
            }
        }

    }

    private void drawRow(int i, String cssStyle, QueueForm queue, VoteChangeHandler<String> myRatingChangeHandler, ClickHandler trackDisplayClickHandler, ClickHandler programmeDisplayClickHandler) {

        GWTFlowPanel operationPanel2 = new GWTFlowPanel();

        GWTThumbnail coverArtWidget = new GWTThumbnail(queue.getCoverArtFile());

        Widget myRatingWidget = null;
        if (queue instanceof BroadcastableTrackQueueForm && ((BroadcastableTrackQueueForm) queue).isCanBeVoted()) {
            myRatingWidget = new GWTMyVoteBox(((BroadcastableTrackQueueForm) queue).getTrackId(), myRatingChangeHandler);
        } else {
            myRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
        }

        GWTLabel averageRatingWidget = null;
        if (queue instanceof BroadcastableTrackQueueForm && ((BroadcastableTrackQueueForm) queue).isCanBeVoted()) {
            if (((BroadcastableTrackQueueForm) queue).getAverageVote() != null) {
                averageRatingWidget = new GWTLabel(((BroadcastableTrackQueueForm) queue).getAverageVote().getI18nFriendlyName());
            } else {
                averageRatingWidget = new GWTLabel(I18nTranslator.getInstance().none());
            }
        } else {
            averageRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
        }

        GWTLabel typeLabel2 = new GWTLabel(null);
        if (queue instanceof BroadcastableTrackQueueForm) {
            typeLabel2.setCaption(I18nTranslator.getInstance().track());
        } else if (queue instanceof LiveQueueForm) {
            typeLabel2.setCaption(I18nTranslator.getInstance().live());
        } else if (queue instanceof BlankQueueForm) {
            typeLabel2.setCaption(I18nTranslator.getInstance().blank());
        } else if (queue instanceof RequestQueueForm) {
            typeLabel2.setCaption(I18nTranslator.getInstance().request());
        } else {
            typeLabel2.setCaption(I18nTranslator.getInstance().unknown());
        }

        Widget programmeWidget;
        if (!(queue instanceof BlankQueueForm)) {
            programmeWidget = new GWTClickableLabel(queue.getProgrammeLabel());
            ((GWTClickableLabel)programmeWidget).addClickHandler(programmeDisplayClickHandler);
        } else {
            programmeWidget = new GWTLabel(queue.getProgrammeLabel());
        }

        Widget contentCaptionWidget2;
        if (queue instanceof BroadcastableTrackQueueForm || queue instanceof RequestQueueForm) {
            contentCaptionWidget2 = new GWTClickableLabel(queue.getContentCaption());
            ((GWTClickableLabel)contentCaptionWidget2).addClickHandler(trackDisplayClickHandler);
        } else {
            contentCaptionWidget2 = new GWTLabel(queue.getContentCaption());
        }
        
        GWTPagedTree captionWidget2 = new GWTPagedTree();
        Long newBranchId = captionWidget2.addItemWithName(Long.toString(i), contentCaptionWidget2);
        if (queue instanceof RequestQueueForm) {
            RequestQueueForm trackForm = (RequestQueueForm) queue;

            if (trackForm.getRequestAuthor() != null && trackForm.getRequestAuthor().length()!=0) {
                newBranchId = captionWidget2.addItem(new GWTLabel(I18nTranslator.getInstance().requested_by() + ": "
                        + trackForm.getRequestAuthor()), newBranchId);
            } else if (trackForm.getRequestMessage() != null && trackForm.getRequestMessage().length()!=0) {
                newBranchId = captionWidget2.addItem(new GWTLabel(I18nTranslator.getInstance().requested_by() + ": "
                        + I18nTranslator.getInstance().unknown()), newBranchId);
            }
            if (trackForm.getRequestMessage() != null && trackForm.getRequestMessage().length()!=0) {
                captionWidget2.addItem(new GWTTruncatedLabel(trackForm.getRequestMessage()), newBranchId);
            }
        }

        queueList.addRow(
                cssStyle,
                queue,
                false,
                coverArtWidget,
                new GWTLabel(queue.getRefId().getSchedulingTime().getI18nSyntheticFriendlyString()),
                typeLabel2,
                programmeWidget,
                new GWTLabel(queue.getPlaylistFriendlyId()),
                new GWTLabel(queue.getDuration() != null ? queue.getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown()),
                captionWidget2,
                averageRatingWidget,
                myRatingWidget,
                operationPanel2);

    }
}
