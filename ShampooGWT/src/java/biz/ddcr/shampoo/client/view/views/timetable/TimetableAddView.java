/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.timetable;

import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTCheckBox;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedFormDropDownListBox;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTNullableFormDropDownListBox;
import biz.ddcr.shampoo.client.view.widgets.GWTNumericBox;
import biz.ddcr.shampoo.client.view.widgets.GWTRadioButton;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTYearMonthDayHourMinuteSecondSelector;
import biz.ddcr.shampoo.client.view.widgets.GWTYearMonthDaySelector;
import biz.ddcr.shampoo.client.view.widgets.GenericCheckBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericDropDownListBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericNumericBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericRadioButtonInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDayHourMinuteSecondSelectorInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDaySelectorInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class TimetableAddView extends PageView {

    public enum RecurringEvent {
        none,
        daily,
        weekly,
        monthly,
        yearly
    }

    //Channel, Programme and Playlist widgets
    private GWTClickableLabel channelLabel;
    private GWTPagedFormDropDownListBox<ProgrammeForm> programmeComboBox;
    private GWTNullableFormDropDownListBox<PlaylistForm> playlistComboBox;
    //Misc. view features : the channel and the user timezones (if different)
    private GWTPagedFormDropDownListBox<String> timezoneComboBox;
    //Start date widgets
    private GWTYearMonthDayHourMinuteSecondSelector startTimeSelector;
    //End date widgets
    private GWTRadioButton endLengthRadioButton;
    private GWTRadioButton endDateRadioButton;
    //  By date
    private GWTYearMonthDayHourMinuteSecondSelector endTimeSelector;
    //  By length
    private GWTNumericBox lengthBox;
    //Repeat
    private GWTPagedFormDropDownListBox<RecurringEvent> recurringEventComboBox;
    private GWTLabel recurringEventCaption;
    //Decommissioning date
    private GWTCheckBox decommissioningTimeCheckBox;
    private GWTYearMonthDaySelector decommissioningTimeSelector;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getChannelLabel() {
        return channelLabel;
    }

    public GenericYearMonthDayHourMinuteSecondSelectorInterface getEndTimeSelector() {
        return endTimeSelector;
    }

    public GenericRadioButtonInterface getEndLengthRadioButton() {
        return endLengthRadioButton;
    }

    public GenericRadioButtonInterface getEndDateRadioButton() {
        return endDateRadioButton;
    }

    public GenericNumericBoxInterface<Integer> getLengthBox() {
        return lengthBox;
    }

    public GenericDropDownListBoxInterface<ProgrammeForm> getProgrammeComboBox() {
        return programmeComboBox;
    }

    public GenericDropDownListBoxInterface<PlaylistForm> getPlaylistComboBox() {
        return playlistComboBox;
    }

    public GenericDropDownListBoxInterface<String> getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericDropDownListBoxInterface<RecurringEvent> getRecurringEventComboBox() {
        return recurringEventComboBox;
    }

    public GenericLabelInterface getRecurringEventCaption() {
        return recurringEventCaption;
    }

    public GenericCheckBoxInterface getDecommissioningTimeCheckBox() {
        return decommissioningTimeCheckBox;
    }

    public GenericYearMonthDaySelectorInterface getDecommissioningTimeSelector() {
        return decommissioningTimeSelector;
    }

    public GenericYearMonthDayHourMinuteSecondSelectorInterface getStartTimeSelector() {
        return startTimeSelector;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().add_timetable();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "timetable";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().timetable_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Programme widgets
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        channelLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTClickableLabel(I18nTranslator.getInstance().channel() + ":"), channelLabel);

        programmeComboBox = new GWTPagedFormDropDownListBox<ProgrammeForm>();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeComboBox);
        playlistComboBox = new GWTNullableFormDropDownListBox<PlaylistForm>();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().playlist() + ":"), playlistComboBox);

        mainPanel.add(grid);

        GWTGrid timezoneGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedFormDropDownListBox<String>();
        timezoneGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        mainPanel.add(timezoneGrid);

        mainPanel.add(new GWTLabel(I18nTranslator.getInstance().start() + ":"));       
        startTimeSelector = new GWTYearMonthDayHourMinuteSecondSelector();
        mainPanel.add(startTimeSelector);

        mainPanel.add(new GWTLabel(I18nTranslator.getInstance().max_end() + ":"));

        endLengthRadioButton = new GWTRadioButton("end", true);
        endLengthRadioButton.setCaption(I18nTranslator.getInstance().length());
        mainPanel.add(endLengthRadioButton);

        GWTGrid endLengthGrid = new GWTGrid();
        lengthBox = new GWTNumericBox(60 * 60, 1, Integer.MAX_VALUE);
        endLengthGrid.addRow(lengthBox, new GWTLabel(I18nTranslator.getInstance().seconds()));
        mainPanel.add(endLengthGrid);

        endDateRadioButton = new GWTRadioButton("end", false);
        endDateRadioButton.setCaption(I18nTranslator.getInstance().date());
        mainPanel.add(endDateRadioButton);

        endTimeSelector = new GWTYearMonthDayHourMinuteSecondSelector();
        mainPanel.add(endTimeSelector);

        GWTGrid optionsGrid1 = new GWTGrid();
        recurringEventComboBox = new GWTPagedFormDropDownListBox<RecurringEvent>();
        recurringEventCaption = new GWTLabel(null);
        optionsGrid1.addRow(
                new GWTLabel(I18nTranslator.getInstance().repeat()+ ":"),
                recurringEventComboBox,
                recurringEventCaption);
        mainPanel.add(optionsGrid1);
        GWTGrid optionsGrid2 = new GWTGrid();
        decommissioningTimeCheckBox = new GWTCheckBox(false);
        decommissioningTimeCheckBox.setCaption(I18nTranslator.getInstance().activate_decommissioning_date()+":");
        decommissioningTimeSelector = new GWTYearMonthDaySelector();
        optionsGrid2.addRow(
                decommissioningTimeCheckBox,
                decommissioningTimeSelector);
        mainPanel.add(optionsGrid2);
        

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        mainPanel.add(submit);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests
    public void refreshProgrammeList(ActionCollection<ProgrammeForm> programmes) {
        //Clear the list beforehand
        programmeComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {
            if (programmeEntry.isReadable()) {
                programmeComboBox.addItem(programmeEntry.getKey().getLabel(), programmeEntry.getKey());
            }
        }

    }

    public void refreshPlaylistList(ActionCollection<PlaylistForm> playlists) {
        //Clear the list beforehand
        playlistComboBox.clean();

        //Specifiying a playlist is not mandatory right now, prepend a blank entry to allow this case
        playlistComboBox.addItem("", null);

        //Generate the list
        //Only animators (who are also programme anagers at the same time) can link plyalists to timetable slots
        for (ActionCollectionEntry<PlaylistForm> playlistEntry : playlists) {
            if (playlistEntry.hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE) &&//
                    //It must also be ready
                    playlistEntry.getItem().isReady()) {
                playlistComboBox.addItem(playlistEntry.getItem().getFriendlyID(), playlistEntry.getItem());
            }
        }

    }

    public void refreshRecurringEventKindList(RecurringEvent[] eventKinds) {
        //Clear the list beforehand
        recurringEventComboBox.clean();

        //Generate the list
        for (RecurringEvent event : eventKinds) {
            String i18nCaption = null;
            switch (event) {
                case none:
                    i18nCaption = I18nTranslator.getInstance().none();
                    break;
                case daily:
                    i18nCaption = I18nTranslator.getInstance().daily();
                    break;
                case weekly:
                    i18nCaption = I18nTranslator.getInstance().weekly();
                    break;
                case monthly:
                    i18nCaption = I18nTranslator.getInstance().monthly();
                    break;
                case yearly:
                    i18nCaption = I18nTranslator.getInstance().yearly();
                    break;
            }
            recurringEventComboBox.addItem(
                    i18nCaption,
                    event);
        }

    }

    public void toggleEndFormatChange() {
        if (endDateRadioButton.isSelected()) {
            lengthBox.setEnabled(false);
            endTimeSelector.setEnabled(true);
        } else {
            lengthBox.setEnabled(true);
            endTimeSelector.setEnabled(false);
        }
    }

    public void toggleRecurringEventChange() {
        if (recurringEventComboBox.getSelectedValue()==RecurringEvent.none) {
            decommissioningTimeCheckBox.setVisible(false);
            decommissioningTimeSelector.setVisible(false);
        } else {
            decommissioningTimeCheckBox.setVisible(true);
            decommissioningTimeSelector.setVisible(true);
        }
    }

    public void toggleDecommissioningEnabledChange() {
        if (decommissioningTimeCheckBox.isChecked()) {
            decommissioningTimeSelector.setEnabled(true);
        } else {
            decommissioningTimeSelector.setEnabled(false);
        }
    }

    public void refreshTimezoneList(
            String channelTimezoneString,
            String userTimezoneString) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        if (channelTimezoneString != null) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezoneString + ")",
                    channelTimezoneString);
        }
        if (userTimezoneString != null && !userTimezoneString.equals(channelTimezoneString)) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezoneString + ")",
                    userTimezoneString);
        }

    }
}
