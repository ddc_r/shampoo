/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.timetable;

import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm.RECURRING_EVENT;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class TimetableCloneView extends PageView {

    public static enum CLONING_PATTERN implements I18nFriendlyInterface, Serializable {
        daily,
        weekly,
        monthly,
        yearly,
        freeform;

        private CLONING_PATTERN() {}

        @Override
        public String getI18nFriendlyString() {
            switch(this) {
                case daily: return I18nTranslator.getInstance().daily();
                case weekly: return I18nTranslator.getInstance().weekly();
                case monthly: return I18nTranslator.getInstance().monthly();
                case yearly: return I18nTranslator.getInstance().yearly();
                case freeform: return I18nTranslator.getInstance().freeform();
                default: return I18nTranslator.getInstance().unknown();
            }
        }
        public RECURRING_EVENT toRecurringEvent() {
            switch(this) {
                case daily: return RECURRING_EVENT.daily;
                case weekly: return RECURRING_EVENT.weekly;
                case monthly: return RECURRING_EVENT.monthly;
                case yearly: return RECURRING_EVENT.yearly;
                default: return null;
            }
        }
    }

    //Channel, Programme and Playlist widgets
    private GWTClickableLabel channelLabel;
    private GWTClickableLabel programmeLabel;
    private GWTClickableLabel playlistLabel;
    private GWTLabel startTimeLabel;
    private GWTLabel endTimeLabel;
    //Misc. view features : the channel and the user timezones (if different)
    private GWTPagedFormDropDownListBox<String> timezoneComboBox;
    private GWTPagedFormDropDownListBox<CLONING_PATTERN> cloneEventComboBox;
    private GWTLabel cloneEventCaption;
    //Periodic cloning
    private GWTVerticalPanel recurringClonePanel;
    //Will clone until date
    private GWTYearMonthDaySelector recurringCloneUntilTimeSelector;
    //Free cloning
    private GWTVerticalPanel freeClonePanel;
    private GWTYearMonthDayHourMinuteSecondSelector freeCloneStartTimeSelector;
    //List of dates
    private GWTClickableLabel addNewFreeCloneTimeButton;
    private GWTPagedFormTable<YearMonthDayHourMinuteSecondMillisecondInterface, Widget> freeCloneTimesListBox;
    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getChannelLabel() {
        return channelLabel;
    }

    public GenericHyperlinkInterface getProgrammeLabel() {
        return programmeLabel;
    }

    public GenericHyperlinkInterface getPlaylistLabel() {
        return playlistLabel;
    }

    public GenericLabelInterface getStartTimeLabel() {
        return startTimeLabel;
    }

    public GenericLabelInterface getEndTimeLabel() {
        return endTimeLabel;
    }

    public GenericDropDownListBoxInterface<String> getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericDropDownListBoxInterface<CLONING_PATTERN> getCloneEventComboBox() {
        return cloneEventComboBox;
    }

    public GenericLabelInterface getCloneEventCaption() {
        return cloneEventCaption;
    }

    public GenericYearMonthDaySelectorInterface getRecurringCloneUntilTimeSelector() {
        return recurringCloneUntilTimeSelector;
    }

    public GenericYearMonthDayHourMinuteSecondSelectorInterface getFreeCloneStartTimeSelector() {
        return freeCloneStartTimeSelector;
    }

    public GenericFormTableInterface<YearMonthDayHourMinuteSecondMillisecondInterface, Widget> getFreeCloneTimesListBox() {
        return freeCloneTimesListBox;
    }

    public GenericHyperlinkInterface getAddNewFreeCloneTimeButton() {
        return addNewFreeCloneTimeButton;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().clone();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "timetable";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().timetable_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Programme widgets
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        channelLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelLabel);
        programmeLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeLabel);
        playlistLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().playlist() + ":"), playlistLabel);
        startTimeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().start() + ":"), startTimeLabel);
        endTimeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().end() + ":"), endTimeLabel);
        mainPanel.add(grid);

        GWTGrid timezoneGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedFormDropDownListBox<String>();
        timezoneGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        mainPanel.add(timezoneGrid);

        GWTGrid optionsGrid1 = new GWTGrid();
        cloneEventComboBox = new GWTPagedFormDropDownListBox<CLONING_PATTERN>();
        cloneEventCaption = new GWTLabel(null);
        optionsGrid1.addRow(
                new GWTLabel(I18nTranslator.getInstance().clone() + ":"),
                cloneEventComboBox,
                cloneEventCaption);
        mainPanel.add(optionsGrid1);

        //Recurring clone mode
        recurringClonePanel = new GWTVerticalPanel();
        GWTGrid containerGrid = new GWTGrid();
        recurringCloneUntilTimeSelector = new GWTYearMonthDaySelector();
        containerGrid.addRow(
                new GWTLabel(I18nTranslator.getInstance().until() + ":"),
                recurringCloneUntilTimeSelector);
        recurringClonePanel.add(containerGrid);
        mainPanel.add(recurringClonePanel);

        //Free clone mode
        freeClonePanel = new GWTVerticalPanel();
        GWTGrid newEntryPanel = new GWTGrid();
        freeCloneStartTimeSelector = new GWTYearMonthDayHourMinuteSecondSelector();
        addNewFreeCloneTimeButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newEntryPanel.addRow(
                freeCloneStartTimeSelector,
                addNewFreeCloneTimeButton);
        freeClonePanel.add(newEntryPanel);

        freeCloneTimesListBox = new GWTPagedFormTable<YearMonthDayHourMinuteSecondMillisecondInterface, Widget>();
        freeCloneTimesListBox.setHeader(I18nTranslator.getInstance().date());
        freeClonePanel.add(freeCloneTimesListBox);
        mainPanel.add(freeClonePanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        mainPanel.add(submit);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    public void refreshTimezoneList(
            String channelTimezoneString,
            String userTimezoneString) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        if (channelTimezoneString != null) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezoneString + ")",
                    channelTimezoneString);
        }
        if (userTimezoneString != null && !userTimezoneString.equals(channelTimezoneString)) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezoneString + ")",
                    userTimezoneString);
        }

    }

    public void refreshRecurringCloneEventKindList(CLONING_PATTERN[] eventKinds) {
        //Clear the list beforehand
        cloneEventComboBox.clean();

        //Generate the list
        for (CLONING_PATTERN event : eventKinds) {
            cloneEventComboBox.addItem(
                    event.getI18nFriendlyString(),
                    event);
        }

    }

    public void addNewFreeCloneTime(YearMonthDayHourMinuteSecondMillisecondInterface date, ClickHandler deleteClickHandler) {

        //Create the date entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new date to the list
        freeCloneTimesListBox.addRow(date, false, new GWTLabel(date.getI18nSyntheticFriendlyString()), operationPanel);
    }

    public void toggleCloneModeChange() {
        freeClonePanel.setVisible(cloneEventComboBox.getSelectedValue()!=null && cloneEventComboBox.getSelectedValue()==CLONING_PATTERN.freeform);
        recurringClonePanel.setVisible(cloneEventComboBox.getSelectedValue()!=null && cloneEventComboBox.getSelectedValue()!=CLONING_PATTERN.freeform);
    }
}
