/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.timetable;

import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedFormDropDownListBox;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedTree;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericDropDownListBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTreeInterface;

/**
 *
 * @author okay_awright
 **/
public class TimetableDisplayView extends GenericFormPageView {

    //Channel, Programme and Playlist widgets
    private GWTLabel channelLabel;
    private GWTLabel programmeLabel;
    private GWTLabel playlistLabel;
    private GWTLabel startTimeLabel;
    private GWTPagedTree endTimeTree;
    //Repeat & Decommissioning date
    private GWTPagedTree recurringEventAndDecommissioningTimeTree;
    //Misc. view features : the channel and the user timezones (if different)
    private GWTPagedFormDropDownListBox<String> timezoneComboBox;

    public GenericLabelInterface getChannelLabel() {
        return channelLabel;
    }

    public GenericLabelInterface getProgrammeLabel() {
        return programmeLabel;
    }

    public GenericLabelInterface getPlaylistLabel() {
        return playlistLabel;
    }

    public GenericLabelInterface getStartTimeLabel() {
        return startTimeLabel;
    }

    public GenericTreeInterface getEndTimeTree() {
        return endTimeTree;
    }

    public GenericDropDownListBoxInterface<String> getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericTreeInterface getRecurringEventAndDecommissioningTimeTree() {
        return recurringEventAndDecommissioningTimeTree;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_timetable();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "timetable";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().timetable_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //Programme widgets
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        channelLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelLabel);
        programmeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeLabel);
        playlistLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().playlist() + ":"), playlistLabel);
        startTimeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().start() + ":"), startTimeLabel);
        endTimeTree = new GWTPagedTree();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().end() + ":"), endTimeTree);
        recurringEventAndDecommissioningTimeTree = new GWTPagedTree();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().repeat() + ":"), recurringEventAndDecommissioningTimeTree);
        mainPanel.add(grid);

        GWTGrid timezoneGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedFormDropDownListBox<String>();
        timezoneGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        mainPanel.add(timezoneGrid);

        return mainPanel;
    }

    public void refreshTimezoneList(
            String channelTimezoneString,
            String userTimezoneString) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        if (channelTimezoneString != null) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezoneString + ")",
                    channelTimezoneString);
        }
        if (userTimezoneString != null && !userTimezoneString.equals(channelTimezoneString)) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezoneString + ")",
                    userTimezoneString);
        }

    }

    public void refreshEndTime(YearMonthDayHourMinuteInterface endTime, JSDuration length) {
        endTimeTree.clean();
        Long newBranchId = null;
        if (endTime!=null)
            newBranchId = endTimeTree.addItem(new GWTLabel(endTime.getI18nFullFriendlyString()));
        if (length!=null && newBranchId!=null)
            endTimeTree.addItem(new GWTLabel(I18nTranslator.getInstance().length() + ": " + length.getI18nFriendlyString()), newBranchId);
    }

    public void refreshRecurringEventAndDecommissioningTime(String recurringEvent, YearMonthDayInterface decommissionDate) {
        recurringEventAndDecommissioningTimeTree.clean();
        Long newBranchId = null;
        if (recurringEvent!=null) {
            newBranchId = recurringEventAndDecommissioningTimeTree.addItem(new GWTLabel(recurringEvent));
            if (decommissionDate!=null && newBranchId!=null) {
                recurringEventAndDecommissioningTimeTree.addItem(new GWTLabel(I18nTranslator.getInstance().decommission() + ": " + decommissionDate.getI18nFullFriendlyString()), newBranchId);
            }
        } else {
            recurringEventAndDecommissioningTimeTree.addItem(new GWTLabel(I18nTranslator.getInstance().none()));
        }
    }
}
