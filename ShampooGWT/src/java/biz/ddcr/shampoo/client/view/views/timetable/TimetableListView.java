/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.timetable;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.UniqueTimetableSlotForm;
import biz.ddcr.shampoo.client.helper.date.HourMinuteInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.ColumnHeader;
import biz.ddcr.shampoo.client.view.widgets.GWTCSSClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelAdd;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelClone;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelMove;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelReset;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelResize;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelSelect;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmation;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmationCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmationDelete;
import biz.ddcr.shampoo.client.view.widgets.GWTExtendedFormWeeklyTimetable;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedFormDropDownListBox;
import biz.ddcr.shampoo.client.view.widgets.GWTFormSortableTable;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTTabPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedTree;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericDropDownListBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericExtendedFormTimetableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericExtendedFormTimetableInterface.RefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTabPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class TimetableListView extends PageView {

    //The channel that is associated to the timetable
    GWTPagedFormDropDownListBox<ChannelForm> channelComboBox;
    GWTTabPanel tabPanel;
    //Grid
    GWTVerticalPanel gridPanel;
    GWTExtendedFormWeeklyTimetable<TimetableSlotForm> timetableGrid;
    //List
    GWTVerticalPanel listPanel;
    GWTFormSortableTable<TimetableSlotForm, Widget, TIMETABLE_TAGS> timetableList;
    GWTPagedFormDropDownListBox<String> timezoneList;
    GWTClickableLabel addTimetableLink;

    public GenericTabPanelInterface getTabPanel() {
        return tabPanel;
    }

    public GenericExtendedFormTimetableInterface<TimetableSlotForm, HourMinuteInterface> getTimetableGrid() {
        return timetableGrid;
    }

    public GenericFormSortableTableInterface<TimetableSlotForm, Widget, TIMETABLE_TAGS> getTimetableList() {
        return timetableList;
    }

    public GenericDropDownListBoxInterface<String> getTimezoneList() {
        return timezoneList;
    }

    public GenericDropDownListBoxInterface<ChannelForm> getChannelComboBox() {
        return channelComboBox;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().timetable_heading();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "timetable";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid1 = new GWTGrid();
        channelComboBox = new GWTPagedFormDropDownListBox<ChannelForm>();
        grid1.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelComboBox);
        mainPanel.add(grid1);

        tabPanel = new GWTTabPanel();
        mainPanel.add(tabPanel);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void buildTimetables(ClickHandler gridClickHandler, ClickHandler listClickHandler, final boolean autoSelectListTab) {

        tabPanel.clean();
        gridPanel = new GWTVerticalPanel();
        tabPanel.insertAt(gridPanel, I18nTranslator.getInstance().grid(), 0);
        tabPanel.addClickHandler(gridClickHandler, 0);
        listPanel = new GWTVerticalPanel();
        tabPanel.insertAt(listPanel, I18nTranslator.getInstance().list(), 1);
        tabPanel.addClickHandler(listClickHandler, 1);
        if (autoSelectListTab)
            tabPanel.selectLastTab();
        else
            tabPanel.selectFirstTab();

    }

    public void refreshTimetableGridPanel(RefreshHandler<TimetableSlotForm> globalRefreshHandler, CellRefreshHandler<TimetableSlotForm, HourMinuteInterface> cellRefreshHandler) {

        gridPanel.clean();
        timetableGrid = new GWTExtendedFormWeeklyTimetable();
        timetableGrid.setRefreshHandler(globalRefreshHandler);
        timetableGrid.setCellRefreshHandler(cellRefreshHandler);
        gridPanel.add(timetableGrid);

    }

    public void refreshTimetableListPanel(SortableTableClickHandler listHeaderClickHandler, ChangeHandler listTimezoneChangeHandler) {

        listPanel.clean();

        timetableList = new GWTFormSortableTable<TimetableSlotForm, Widget, TIMETABLE_TAGS>();
        //Generate list header
        timetableList.setHeader(
                new ColumnHeader<TIMETABLE_TAGS>(I18nTranslator.getInstance().time(), TIMETABLE_TAGS.time),
                new ColumnHeader<TIMETABLE_TAGS>(I18nTranslator.getInstance().enabled(), TIMETABLE_TAGS.enabled),
                new ColumnHeader<TIMETABLE_TAGS>(I18nTranslator.getInstance().programme(), TIMETABLE_TAGS.programme),
                new ColumnHeader<TIMETABLE_TAGS>(I18nTranslator.getInstance().playlist(), TIMETABLE_TAGS.playlist),
                new ColumnHeader<TIMETABLE_TAGS>(I18nTranslator.getInstance().type(), TIMETABLE_TAGS.type),
                new ColumnHeader<TIMETABLE_TAGS>(I18nTranslator.getInstance().operation()));
        timetableList.setRefreshClickHandler(listHeaderClickHandler);
        listPanel.add(timetableList);

        GWTGrid timezoneGrid = new GWTGrid();
        timezoneList = new GWTPagedFormDropDownListBox<String>();
        timezoneList.addChangeHandler(listTimezoneChangeHandler);
        timezoneGrid.addRow(
                new GWTLabel(I18nTranslator.getInstance().timezone() + ":"),
                timezoneList);
        listPanel.add(timezoneGrid);

    }

    public void refreshAddTimetableLink(ClickHandler clickHandler) {
        //Add a regular user
        addTimetableLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_timetable());
        if (clickHandler != null) {
            addTimetableLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addTimetableLink);
    }

    public void refreshChannelList(ActionCollection<ChannelForm> channels) {
        //Clear the list beforehand
        channelComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channels) {

            if (channelEntry.isReadable()) {
                ChannelForm channel = channelEntry.getKey();

                channelComboBox.addItem(channel.getLabel(), channel);
            }
        }

    }

    //Grid
    public void refreshVoidGridCell(GenericPanelInterface container) {
        container.add(new GWTLabel(I18nTranslator.getInstance().off()));
    }

    public void refreshGridCellCaption(GenericPanelInterface container,
            ActionCollectionEntry<TimetableSlotForm> currentCellForm,
            HourMinuteInterface startTime,
            HourMinuteInterface endTime,
            ClickHandler timetableSlotDisplayClickHandler) {

        if (currentCellForm != null) {
            GWTCSSClickableLabel slotStartTimeLabel = new GWTCSSClickableLabel(
                    (startTime != null ? startTime.getI18nDayTimeFriendlyString() : "") + " - " + (endTime != null ? endTime.getI18nDayTimeFriendlyString() : ""),
                    timetableSlotDisplayClickHandler);
            //Custom CSS
            slotStartTimeLabel.setSecondaryCSSMarker("start");
            container.add(slotStartTimeLabel);
        }
    }

    public void refreshGridCellContent(GenericPanelInterface container,
            ActionCollectionEntry<TimetableSlotForm> currentCellForm,
            YesNoClickHandler cancelSlotClickHandler,
            ClickHandler reactivateSlotClickHandler,
            ClickHandler selectPlaylistClickHandler,
            ClickHandler moveSlotClickHandler,
            ClickHandler resizeSlotClickHandler,
            ClickHandler resetDecommissioningSlotClickHandler,
            ClickHandler cloneSlotClickHandler,
            YesNoClickHandler deleteSlotClickHandler,
            final SerializableItemSelectionHandler<String> playlistDisplaySelectionHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler) {

        if (currentCellForm != null && currentCellForm.isReadable()) {

            final TimetableSlotForm t = currentCellForm.getItem();

            //Starting time
            //clickable id
            //show both the start time and end time, specify the weekday if different
//            GWTClickableLabel intervalLabel;
            //Check if it ends the same day it starts
            /** BUG? upcast doesn't work with GWT (?)
            if (((JSYearMonthDay)t.getRefId().getSchedulingTime()).equals(
            (JSYearMonthDay)t.getEndTime()
            )) {**/
            //TODO: dropped the day diff until I can find a way to do it gracefully
            /*if (new JSYearMonthDay(t.getRefId().getSchedulingTime()).equals(
            new JSYearMonthDay(t.getEndTime())
            )) {
            }*/
            /*String timeDelimiter = (new JSYearMonthDay(t.getRefId().getSchedulingTime()).equals(new JSYearMonthDay(t.getEndTime()))) ? "-" : "~";
            
            intervalLabel = new GWTClickableLabel(
            t.getRefId().getSchedulingTime().getI18nDayTimeFriendlyString()
            + " " + timeDelimiter + " " +
            t.getEndTime().getI18nDayTimeFriendlyString(),
            timetableSlotDisplayClickHandler
            );*/
            /*} else {
            intervalLabel = new GWTClickableLabel(
            I18nTranslator.getInstance().short_weekday_hour_minute(
            JSYearMonthDay.getI18nDayFriendlyName(t.getRefId().getSchedulingTime().getDayOfWeek()),
            t.getRefId().getSchedulingTime().getI18nDayTimeFriendlyString())
            + " - " +
            I18nTranslator.getInstance().short_weekday_hour_minute(
            JSYearMonthDay.getI18nDayFriendlyName(t.getEndTime().getDayOfWeek()),
            t.getEndTime().getI18nDayTimeFriendlyString()),
            timetableSlotDisplayClickHandler
            );
            }*/
//            container.add(intervalLabel);
            //Titles
            //Programme
            if (t.getProgrammeId().isReadable()) {
                GWTCSSClickableLabel programmeLabel = new GWTCSSClickableLabel(t.getProgrammeId().getItem(), new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent ce) {
                        programmeDisplaySelectionHandler.onItemSelected(t.getProgrammeId().getItem());
                    }
                });
                //Custom CSS
                programmeLabel.setSecondaryCSSMarker("label");
                container.add(programmeLabel);
            }
            //Playlist
            if (t.getPlaylistFriendlyId() != null) {
                if (t.getPlaylistFriendlyId().isReadable()) {
                    GWTCSSClickableLabel playlistLabel = new GWTCSSClickableLabel(t.getPlaylistFriendlyId().getItem(), new ClickHandler() {

                        @Override
                        public void onClick(ClickEvent ce) {
                            playlistDisplaySelectionHandler.onItemSelected(t.getPlaylistId().getItem());
                        }
                    });
                    //Custom CSS
                    playlistLabel.setSecondaryCSSMarker("subLabel");
                    container.add(playlistLabel);
                }
            }

            //Links

            //People (i.e. Animators) who are in charge of the currently bound playlist but who are not allowed to directly edit a timetable slot can still cancel or reactivate a show
            //Do only propose the option if there is an available playlist
            if (t.getPlaylistId() != null) {
                //Withdraw/Re-activate
                if (/*Don't touch it if it's now playing*/!t.isNowPlaying() &&//
                        (currentCellForm.isEditable() || t.getPlaylistId().hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE))) {
                    if (t.getRefId().isEnabled()) {
                        //Cancel
                        GWTClickableLabelWithYesNoConfirmation cancelLink = new GWTClickableLabelWithYesNoConfirmationCancel(I18nTranslator.getInstance().cancel(), cancelSlotClickHandler);
                        cancelLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_cancel_x(t.getRefId().getFullFriendlyID()));
                        container.add(cancelLink);
                    } else //Confirm
                    {
                        container.add(new GWTClickableLabel(
                                I18nTranslator.getInstance().confirm(),
                                reactivateSlotClickHandler));
                    }
                }
            }
            //Change playlist
            if (/*Don't touch it if it's now playing*/!t.isNowPlaying() &&//
                    currentCellForm.hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE)) {
                container.add(new GWTClickableLabelSelect(
                        I18nTranslator.getInstance().select_playlist(),
                        selectPlaylistClickHandler));
            }

            if (/*Don't touch it if it's now playing*/!t.isNowPlaying() &&//
                    currentCellForm.isEditable()) {
                //Move
                container.add(new GWTClickableLabelMove(
                        I18nTranslator.getInstance().move(),
                        moveSlotClickHandler));

                //Resize
                container.add(new GWTClickableLabelResize(
                        I18nTranslator.getInstance().resize(),
                        resizeSlotClickHandler));

                if (t instanceof UniqueTimetableSlotForm) {
                    //Clone
                    container.add(new GWTClickableLabelClone(
                            I18nTranslator.getInstance().clone(),
                            cloneSlotClickHandler));
                } else {
                    //Reset decommissioning date
                    container.add(new GWTClickableLabelReset(
                            I18nTranslator.getInstance().reset_decommissioning_date(),
                            resetDecommissioningSlotClickHandler));
                }
            }

            if (/*Don't touch it if it's now playing*/!t.isNowPlaying() &&//
                    currentCellForm.isDeletable()) {
                //Delete
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(t.getRefId().getFullFriendlyID()));
                deleteLink.addClickHandler(deleteSlotClickHandler);
                container.add(deleteLink);
            }
        }

    }

    public void refreshGridTimescaleList(byte[] minutes) {
        timetableGrid.setTimeScales(minutes);
    }

    public void refreshGridTimezoneList(String channelTimezone, String userTimezone) {
        timetableGrid.setTimeZones(channelTimezone, userTimezone);
    }

    //List
    public void refreshTimetableList(ActionCollection<TimetableSlotForm> timetableSlots,
            YesNoClickHandler cancelSlotClickHandler,
            ClickHandler reactivateSlotClickHandler,
            ClickHandler selectPlaylistClickHandler,
            ClickHandler moveSlotClickHandler,
            ClickHandler resizeSlotClickHandler,
            ClickHandler resetDecommissioningSlotClickHandler,
            ClickHandler cloneSlotClickHandler,
            YesNoClickHandler deleteSlotClickHandler,
            final YesNoClickHandler deleteAllSlotClickHandler,
            ClickHandler timetableSlotDisplayClickHandler,
            final SerializableItemSelectionHandler<String> playlistDisplaySelectionHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler) {

        //Clear the list beforehand
        timetableList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        timetableList.setNextButtonVisible((timetableSlots.size() >= timetableList.getMaxVisibleRows()));

        timetableList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllSlotClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllSlotClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<TimetableSlotForm> timetableSlotEntry : timetableSlots) {

            if (timetableSlotEntry.isReadable()) {
                final TimetableSlotForm timetableSlot = timetableSlotEntry.getKey();

                GWTPagedTree timeWidget = new GWTPagedTree();
                Long newBranchId = timeWidget.addItem(
                        new GWTClickableLabel(timetableSlot.getRefId().getSchedulingTime().getI18nSyntheticFriendlyString(), timetableSlotDisplayClickHandler));
                timeWidget.addItem(
                        new GWTLabel(I18nTranslator.getInstance().max_end() + ": " + timetableSlot.getEndTime().getI18nSyntheticFriendlyString()),
                        newBranchId);

                GWTClickableLabel playlistWidget = new GWTClickableLabel(null);
                if (timetableSlot.getPlaylistFriendlyId() != null) {
                    if (timetableSlot.getPlaylistFriendlyId().isReadable()) {
                        final String playlistId = timetableSlot.getPlaylistId().getItem();
                        ClickHandler playlistClickHandler = new ClickHandler() {

                            @Override
                            public void onClick(ClickEvent ce) {
                                playlistDisplaySelectionHandler.onItemSelected(playlistId);
                            }
                        };
                        playlistWidget.setCaption(timetableSlot.getPlaylistFriendlyId().getItem());
                        playlistWidget.addClickHandler(playlistClickHandler);
                    }
                }

                GWTClickableLabel programmeWidget = new GWTClickableLabel(null);
                if (timetableSlot.getProgrammeId().isReadable()) {
                    final String programmeId = timetableSlot.getProgrammeId().getItem();
                    ClickHandler programmeClickHandler = new ClickHandler() {

                        @Override
                        public void onClick(ClickEvent ce) {
                            programmeDisplaySelectionHandler.onItemSelected(programmeId);
                        }
                    };
                    programmeWidget.setCaption(programmeId);
                    programmeWidget.addClickHandler(programmeClickHandler);
                }

                GWTLabel typeWidget = new GWTLabel(null);
                if (timetableSlot instanceof UniqueTimetableSlotForm) {
                    typeWidget.setCaption(I18nTranslator.getInstance().unique());
                } else {
                    typeWidget.setCaption(((RecurringTimetableSlotForm) timetableSlot).getRecurringEvent().getI18nFriendlyString());
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //People (i.e. Animators) who are in charge of the currently bound playlist but who are not allowed to directly edit a timetable slot can still cancel or reactivate a show
                //Do only propose the option if there is an available playlist
                if (/*Don't touch it if it's now playing*/!timetableSlotEntry.getKey().isNowPlaying() &&//
                        timetableSlotEntry.getKey().getPlaylistId() != null) {
                    //Withdraw/Re-activate
                    if (timetableSlotEntry.isEditable() || timetableSlotEntry.getKey().getPlaylistId().hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE)) {
                        GenericHyperlinkInterface cancelLink;
                        if (timetableSlotEntry.getKey().getRefId().isEnabled()) {
                            //Cancel
                            cancelLink = new GWTClickableLabelWithYesNoConfirmationCancel(I18nTranslator.getInstance().cancel());
                            ((GWTClickableLabelWithYesNoConfirmation) cancelLink).setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_cancel_x(timetableSlot.getRefId().getFullFriendlyID()));
                            if (cancelSlotClickHandler != null) {
                                ((GWTClickableLabelWithYesNoConfirmation) cancelLink).addClickHandler(cancelSlotClickHandler);
                            }
                        } else {
                            //Confirm
                            cancelLink = new GWTClickableLabel(I18nTranslator.getInstance().confirm());
                            if (reactivateSlotClickHandler != null) {
                                cancelLink.addClickHandler(reactivateSlotClickHandler);
                            }
                        }
                        operationPanel.add(cancelLink);
                    }
                }

                //Change playlist
                if (/*Don't touch it if it's now playing*/!timetableSlotEntry.getKey().isNowPlaying() &&//
                        timetableSlotEntry.hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE)) {
                    GenericHyperlinkInterface changePlaylistLink = new GWTClickableLabelSelect(I18nTranslator.getInstance().select_playlist());
                    if (selectPlaylistClickHandler != null) {
                        changePlaylistLink.addClickHandler(selectPlaylistClickHandler);
                    }
                    operationPanel.add(changePlaylistLink);
                }

                if (/*Don't touch it if it's now playing*/!timetableSlotEntry.getKey().isNowPlaying() &&//
                        timetableSlotEntry.isEditable()) {
                    //Move
                    GWTClickableLabel moveLink = new GWTClickableLabelMove(I18nTranslator.getInstance().move());
                    if (moveSlotClickHandler != null) {
                        moveLink.addClickHandler(moveSlotClickHandler);
                    }
                    operationPanel.add(moveLink);

                    //Resize
                    GWTClickableLabel resizeLink = new GWTClickableLabelResize(I18nTranslator.getInstance().resize());
                    if (resizeSlotClickHandler != null) {
                        resizeLink.addClickHandler(resizeSlotClickHandler);
                    }
                    operationPanel.add(resizeLink);

                    if (timetableSlot instanceof UniqueTimetableSlotForm) {
                        //Clone
                        GWTClickableLabel cloneLink = new GWTClickableLabelClone(I18nTranslator.getInstance().clone());
                        if (cloneSlotClickHandler != null) {
                            cloneLink.addClickHandler(cloneSlotClickHandler);
                        }
                        operationPanel.add(cloneLink);
                    } else {
                        //Reset decommissioning date
                        GWTClickableLabel resetDecommissioningLink = new GWTClickableLabelReset(I18nTranslator.getInstance().reset_decommissioning_date());
                        if (resetDecommissioningSlotClickHandler != null) {
                            resetDecommissioningLink.addClickHandler(resetDecommissioningSlotClickHandler);
                        }
                        operationPanel.add(resetDecommissioningLink);
                    }
                }

                if (/*Don't touch it if it's now playing*/!timetableSlotEntry.getKey().isNowPlaying() &&//
                        timetableSlotEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(timetableSlot.getRefId().getFullFriendlyID()));
                    if (deleteSlotClickHandler != null) {
                        deleteLink.addClickHandler(deleteSlotClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }

                timetableList.addRow(timetableSlot,
                        /*Don't touch it if it's now playing*/ !timetableSlotEntry.getKey().isNowPlaying() && timetableSlotEntry.isDeletable(),
                        timeWidget,
                        new GWTLabel(timetableSlot.getRefId().isEnabled() ? I18nTranslator.getInstance().is_true() : I18nTranslator.getInstance().is_false()),
                        programmeWidget,
                        playlistWidget,
                        typeWidget,
                        operationPanel);
            }
        }

    }

    public void refreshPlaylistCaption(PlaylistForm playlist, GenericLabelInterface playlistCaptionContainer, GenericLabelInterface programmeCaptionContainer) {
        if (playlist != null) {
            if (playlistCaptionContainer != null) {
                playlistCaptionContainer.setCaption(
                        playlist.getFriendlyID());
            }
            if (programmeCaptionContainer != null && playlist.getProgrammeId().isReadable()) {
                programmeCaptionContainer.setCaption(
                        playlist.getProgrammeId().getItem());
            }
        }
    }

    public void refreshListTimezoneList(String channelTimezone, String userTimezone) {
        //Clear the list beforehand
        timezoneList.clean();
        //Generate the list
        if (channelTimezone != null) {
            timezoneList.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezone + ")",
                    channelTimezone);
        }
        if (userTimezone != null && !userTimezone.equals(channelTimezone)) {
            timezoneList.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezone + ")",
                    userTimezone);
        }
    }
}
