/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.timetable;

import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.ColumnHeader;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelSelect;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFormSortableTable;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTTruncatedLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class TimetablePlaylistSelectView extends PageView {

    //List
    protected GWTFormSortableTable<PlaylistForm, Widget, PLAYLIST_TAGS> playlistList;
    protected GWTClickableLabel cancel;

    public GenericFormSortableTableInterface<PlaylistForm, Widget, PLAYLIST_TAGS> getPlaylistList() {
        return playlistList;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().select_playlist();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "timetable";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().timetable_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        playlistList = new GWTFormSortableTable<PlaylistForm, Widget, PLAYLIST_TAGS>();
        //Generate list header
        playlistList.setHeader(
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().label(), PLAYLIST_TAGS.label),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().description(), PLAYLIST_TAGS.description),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().live(), PLAYLIST_TAGS.live),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().timetables(), PLAYLIST_TAGS.timetableSlots),
                new ColumnHeader<PLAYLIST_TAGS>(I18nTranslator.getInstance().operation())
                );
        //playlistList.setRefreshClickHandler(listHeaderClickHandler);

        mainPanel.add(playlistList);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests

    //List
    public void refreshPlaylistList(
            ActionCollection<PlaylistForm> playlists,
            ClickHandler selectClickHandler,
            ClickHandler playlistDisplayClickHandler,
            final SerializableItemSelectionHandler<TimetableSlotFormID> timetableSlotDisplaySelectionHandler) {

        //Clear the list beforehand
        playlistList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        playlistList.setNextButtonVisible((playlists.size() >= playlistList.getMaxVisibleRows()));

        for (ActionCollectionEntry<PlaylistForm> playlistEntry : playlists) {

            if (playlistEntry.isReadable()) {
                PlaylistForm playlist = playlistEntry.getKey();

                GWTGrid timetableWidget = new GWTGrid();
                for (final ActionCollectionEntry<TimetableSlotFormID> timetableEntry : playlist.getTimetableSlotIds()) {
                    if (timetableEntry.isReadable()) {
                        String slotId = timetableEntry.getItem().getSyntheticFriendlyID();
                        ClickHandler timetableSlotClickHandler = new ClickHandler() {
                            @Override
                                public void onClick(ClickEvent ce) {
                                    timetableSlotDisplaySelectionHandler.onItemSelected(timetableEntry.getItem());
                                }
                            };
                        timetableWidget.addRow(new GWTClickableLabel(slotId, timetableSlotClickHandler));
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //TODO: check and see if this test is really necessary
                if (playlistEntry.hasAction(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE) && //
                        //It must also be ready
                        playlist.isReady()) {
                    GWTClickableLabel selectLink = new GWTClickableLabelSelect(I18nTranslator.getInstance().select());
                    if (selectClickHandler != null) {
                        selectLink.addClickHandler(selectClickHandler);
                    }
                    operationPanel.add(selectLink);
                }

                playlistList.addRow(playlist,
                        //Don't use the multiple selection since only one playlist can be chosen at a time, so put the flag to false
                        false,
                        new GWTClickableLabel(playlist.getFriendlyID(), playlistDisplayClickHandler),
                        new GWTTruncatedLabel(playlist.getDescription()),
                        new GWTLabel(playlist.getLive()!=null ? I18nTranslator.getInstance().is_true() : I18nTranslator.getInstance().is_false()),
                        timetableWidget,
                        operationPanel);
            }
        }

    }

}
