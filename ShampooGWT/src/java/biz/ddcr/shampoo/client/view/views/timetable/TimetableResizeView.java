/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.timetable;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedFormDropDownListBox;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTNumericBox;
import biz.ddcr.shampoo.client.view.widgets.GWTRadioButton;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTYearMonthDayHourMinuteSecondSelector;
import biz.ddcr.shampoo.client.view.widgets.GenericDropDownListBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericNumericBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericRadioButtonInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericYearMonthDayHourMinuteSecondSelectorInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class TimetableResizeView extends PageView {

    //Channel, Programme and Playlist widgets
    private GWTClickableLabel channelLabel;
    private GWTClickableLabel programmeLabel;
    private GWTClickableLabel playlistLabel;
    private GWTLabel startTimeLabel;
    private GWTLabel originalEndTimeLabel;
    //Resize type widget
    private GWTRadioButton resizeTypeByLengthRadioButton;
    private GWTRadioButton resizeTypeByDateRadioButton;
    //  -Length
    private GWTNumericBox resizeBox;
    //  -Date
    //Misc. view features : the channel and the user timezones (if different)
    private GWTPagedFormDropDownListBox<String> timezoneComboBox;
    //Start date widgets
    private GWTYearMonthDayHourMinuteSecondSelector newEndTimeSelector;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getChannelLabel() {
        return channelLabel;
    }
    public GenericHyperlinkInterface getProgrammeLabel() {
        return programmeLabel;
    }
    public GenericHyperlinkInterface getPlaylistLabel() {
        return playlistLabel;
    }
    public GenericLabelInterface getOriginalEndTimeLabel() {
        return originalEndTimeLabel;
    }
    public GenericLabelInterface getStartTimeLabel() {
        return startTimeLabel;
    }

    public GenericRadioButtonInterface getResizeTypeByLengthRadioButton() {
        return resizeTypeByLengthRadioButton;
    }
    public GenericRadioButtonInterface getResizeTypeByDateRadioButton() {
        return resizeTypeByDateRadioButton;
    }

    public GenericNumericBoxInterface<Integer> getResizeBox() {
        return resizeBox;
    }

    public GenericDropDownListBoxInterface<String> getTimezoneComboBox() {
        return timezoneComboBox;
    }
    public GenericYearMonthDayHourMinuteSecondSelectorInterface getNewEndTimeSelector() {
        return newEndTimeSelector;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().resize_timetable();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "timetable";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().timetable_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Programme widgets
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        channelLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelLabel);
        programmeLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().programme() + ":"), programmeLabel);
        playlistLabel = new GWTClickableLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().playlist() + ":"), playlistLabel);
        startTimeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().start() + ":"), startTimeLabel);
        originalEndTimeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().original_end() + ":"), originalEndTimeLabel);

        mainPanel.add(grid);

        GWTGrid timezoneGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedFormDropDownListBox<String>();
        timezoneGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        mainPanel.add(timezoneGrid);

        resizeTypeByLengthRadioButton = new GWTRadioButton("type", true);
        resizeTypeByLengthRadioButton.setCaption(I18nTranslator.getInstance().length());
        mainPanel.add(resizeTypeByLengthRadioButton);

        GWTGrid resizeGrid = new GWTGrid();
        resizeBox = new GWTNumericBox(60 * 60, 1, Integer.MAX_VALUE);
        resizeGrid.addRow(new GWTLabel(I18nTranslator.getInstance().new_size()+":"), resizeBox, new GWTLabel(I18nTranslator.getInstance().seconds()));
        mainPanel.add(resizeGrid);

        resizeTypeByDateRadioButton = new GWTRadioButton("type", false);
        resizeTypeByDateRadioButton.setCaption(I18nTranslator.getInstance().date());
        mainPanel.add(resizeTypeByDateRadioButton);

        mainPanel.add(new GWTLabel(I18nTranslator.getInstance().new_end() + ":"));
        newEndTimeSelector = new GWTYearMonthDayHourMinuteSecondSelector();
        mainPanel.add(newEndTimeSelector);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        mainPanel.add(submit);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests

    public void toggleResizeTypeChange() {
        if (resizeTypeByDateRadioButton.isSelected()) {
            resizeBox.setEnabled(false);
            newEndTimeSelector.setEnabled(true);
        } else {
            resizeBox.setEnabled(true);
            newEndTimeSelector.setEnabled(false);
        }
    }

    public void refreshTimezoneList(
            String channelTimezoneString,
            String userTimezoneString) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        if (channelTimezoneString != null) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezoneString + ")",
                    channelTimezoneString);
        }
        if (userTimezoneString != null && !userTimezoneString.equals(channelTimezoneString)) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezoneString + ")",
                    userTimezoneString);
        }

    }

}
