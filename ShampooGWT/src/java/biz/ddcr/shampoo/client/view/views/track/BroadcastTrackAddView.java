/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.track;

import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackAddView extends PageView {

    //Track
    private GWTClickableUncachableImage albumCoverPicture;
    private GWTPictureUploadMoxie albumCoverUploadPlupload;
    private GWTClickableLabel resetAlbumCoverArtLink;
    private GWTAudioUploadMoxie trackUploadPlupload;
    private GWTClickableLabel resetTrackLink;
    private GWTClickableLabel fillTagsFromTrackLink;
    private GWTDisclosurePanel trackInfoPanel;
    //Track properties widgets
    private GWTCheckBox enabledCheckBox;
    private GWTPagedFormDropDownListBox<TYPE> typeComboBox;
    private GWTTextBox authorTextBox;
    private GWTTextBox titleTextBox;
    private GWTTextBox albumTextBox;
    private GWTExtendedTextBox descriptionTextBox;
    private GWTNullableNumericBox yearOfReleaseNumericBox;
    private GWTTextBox genreTextBox;
    //Misc items
    private GWTNullableFormDropDownListBox<PEGI_AGE> advisoryAgeComboBox;
    private GWTFlowPanel advisoryFeaturesPanel;
    private GWTCheckBox advisoryViolenceCheckBox;
    private GWTCheckBox advisoryProfanityCheckBox;
    private GWTCheckBox advisoryFearCheckBox;
    private GWTCheckBox advisorySexCheckBox;
    private GWTCheckBox advisoryDrugsCheckBox;
    private GWTCheckBox advisoryDiscriminationCheckBox;
    private GWTTextBox publisherTextBox;
    private GWTTextBox copyrightTextBox;
    private GWTTextBox tagTextBox;
    //Programmes widgets
    private GWTPagedFormCommunicatingDualSelectBoxes<ProgrammeForm> programmeComboBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericHyperlinkInterface getAlbumCoverPicture() {
        return albumCoverPicture;
    }

    public GenericHyperlinkInterface getResetAlbumCoverArtLink() {
        return resetAlbumCoverArtLink;
    }

    public GenericHyperlinkInterface getResetTrackLink() {
        return resetTrackLink;
    }

    public GenericNamedPanelInterface getTrackInfoPanel() {
        return trackInfoPanel;
    }

    public GenericHyperlinkInterface getFillTagsFromTrackLink() {
        return fillTagsFromTrackLink;
    }

    public GenericDropDownListBoxInterface<PEGI_AGE> getAdvisoryAgeComboBox() {
        return advisoryAgeComboBox;
    }

    public GenericCheckBoxInterface getAdvisoryDiscriminationCheckBox() {
        return advisoryDiscriminationCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryDrugsCheckBox() {
        return advisoryDrugsCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryFearCheckBox() {
        return advisoryFearCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryProfanityCheckBox() {
        return advisoryProfanityCheckBox;
    }

    public GenericCheckBoxInterface getAdvisorySexCheckBox() {
        return advisorySexCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryViolenceCheckBox() {
        return advisoryViolenceCheckBox;
    }

    public GenericTextBoxInterface getAlbumTextBox() {
        return albumTextBox;
    }

    public GenericTextBoxInterface getAuthorTextBox() {
        return authorTextBox;
    }

    public GenericTextBoxInterface getPublisherTextBox() {
        return publisherTextBox;
    }
    
    public GenericTextBoxInterface getCopyrightTextBox() {
        return copyrightTextBox;
    }

    public GenericTextBoxInterface getDescriptionTextBox() {
        return descriptionTextBox;
    }

    public GenericCheckBoxInterface getEnabledCheckBox() {
        return enabledCheckBox;
    }

    public GenericTextBoxInterface getGenreTextBox() {
        return genreTextBox;
    }

    public GenericTextBoxInterface getTagTextBox() {
        return tagTextBox;
    }

    public GenericTextBoxInterface getTitleTextBox() {
        return titleTextBox;
    }

    public GenericDropDownListBoxInterface<TYPE> getTypeComboBox() {
        return typeComboBox;
    }

    public GenericNumericBoxInterface<Integer> getYearOfReleaseNumericBox() {
        return yearOfReleaseNumericBox;
    }

    public GenericFormCommunicatingDualSelectBoxesInterface<ProgrammeForm> getProgrammeComboBox() {
        return programmeComboBox;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericFileUploadInterface getAlbumCoverUploadWidget() {
        return albumCoverUploadPlupload;
    }

    public GenericFileUploadInterface getTrackUploadWidget() {
        return trackUploadPlupload;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().add_track();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "track";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().track_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Channel widgets
        GWTVerticalPanel trackPanel = new GWTVerticalPanel();

        //track
        trackUploadPlupload = new GWTAudioUploadMoxie(null);
        resetTrackLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        fillTagsFromTrackLink = new GWTClickableLabelReset(I18nTranslator.getInstance().reset_empty_tags_from_track());
        trackInfoPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().technical_info());
        GWTFlowPanel newTrackPanel = new GWTFlowPanel();
        newTrackPanel.add(trackUploadPlupload);
        newTrackPanel.add(trackInfoPanel);
        newTrackPanel.add(resetTrackLink);
        newTrackPanel.add(fillTagsFromTrackLink);
        trackPanel.add(newTrackPanel);

        GWTGrid tagGrid = new GWTGrid();

        //Common tags
        GWTGrid grid = new GWTGrid();

        enabledCheckBox = new GWTCheckBox(false);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().enabled() + ":"), enabledCheckBox);

        typeComboBox = new GWTPagedFormDropDownListBox<TYPE>();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().type() + ":"), typeComboBox);

        authorTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().author() + ":"), authorTextBox);

        titleTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().title() + ":"), titleTextBox);

        albumTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().album() + ":"), albumTextBox);

        descriptionTextBox = new GWTExtendedTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().comment() + ":"), descriptionTextBox);

        yearOfReleaseNumericBox = new GWTNullableNumericBox(null, -2000, +3000);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().year() + ":"), yearOfReleaseNumericBox);

        genreTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().genre() + ":"), genreTextBox);

        //Cover art
        GWTFlowPanel newAlbumCoverArtPanel = new GWTFlowPanel();
        albumCoverPicture = new GWTClickableUncachableImage();
        newAlbumCoverArtPanel.add(albumCoverPicture);

        albumCoverUploadPlupload = new GWTPictureUploadMoxie(null);
        resetAlbumCoverArtLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        newAlbumCoverArtPanel.add(albumCoverUploadPlupload);
        newAlbumCoverArtPanel.add(resetAlbumCoverArtLink);

        tagGrid.addRow(grid, newAlbumCoverArtPanel);

        trackPanel.add(tagGrid);

        //Programme widgets
        GWTVerticalPanel programmePanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel programmeLabel = new GWTLabel(I18nTranslator.getInstance().programmes_list());
        programmePanel.add(programmeLabel);
        programmeComboBox = new GWTPagedFormCommunicatingDualSelectBoxes<ProgrammeForm>();
        programmeComboBox.setHeader(I18nTranslator.getInstance().available(), I18nTranslator.getInstance().selection());
        programmePanel.add(programmeComboBox);

        trackPanel.add(programmePanel);

        //Misc options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        GWTFlowPanel advisoryPanel = new GWTFlowPanel();

        GWTGrid advisoryAgeGrid = new GWTGrid();

        advisoryAgeComboBox = new GWTNullableFormDropDownListBox<PEGI_AGE>();
        advisoryAgeGrid.addRow(new GWTLabel(I18nTranslator.getInstance().rating() + ":"), advisoryAgeComboBox);
        advisoryPanel.add(advisoryAgeGrid);
        advisoryFeaturesPanel = new GWTFlowPanel();
        advisoryViolenceCheckBox = new GWTCheckBox(false);
        advisoryViolenceCheckBox.setCaption(I18nTranslator.getInstance().violence_advisory());
        advisoryFeaturesPanel.add(advisoryViolenceCheckBox);
        advisoryProfanityCheckBox = new GWTCheckBox(false);
        advisoryProfanityCheckBox.setCaption(I18nTranslator.getInstance().profanity_advisory());
        advisoryFeaturesPanel.add(advisoryProfanityCheckBox);
        advisoryFearCheckBox = new GWTCheckBox(false);
        advisoryFearCheckBox.setCaption(I18nTranslator.getInstance().fear_advisory());
        advisoryFeaturesPanel.add(advisoryFearCheckBox);
        advisorySexCheckBox = new GWTCheckBox(false);
        advisorySexCheckBox.setCaption(I18nTranslator.getInstance().sex_advisory());
        advisoryFeaturesPanel.add(advisorySexCheckBox);
        advisoryDrugsCheckBox = new GWTCheckBox(false);
        advisoryDrugsCheckBox.setCaption(I18nTranslator.getInstance().drugs_advisory());
        advisoryFeaturesPanel.add(advisoryDrugsCheckBox);
        advisoryDiscriminationCheckBox = new GWTCheckBox(false);
        advisoryDiscriminationCheckBox.setCaption(I18nTranslator.getInstance().discrimination_advisory());
        advisoryFeaturesPanel.add(advisoryDiscriminationCheckBox);
        advisoryPanel.add(advisoryFeaturesPanel);
        miscOptionsGrid.addRow(advisoryPanel);

        publisherTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().publisher() + ":"), publisherTextBox);
        
        copyrightTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().copyright() + ":"), copyrightTextBox);

        tagTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().tag() + ":"), tagTextBox);

        miscOptionsPanel.add(miscOptionsGrid);
        trackPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        trackPanel.add(submit);

        return trackPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests
    public void loadCoverArt(HasCoverArtInterface module) {
        albumCoverPicture.setImage(module);
    }
    public void unloadCoverArt() {
        albumCoverPicture.setImage((HasCoverArtInterface)null);
    }

    public void refreshTypeList(TYPE[] types) {
        typeComboBox.clean();
        for (TYPE type : types) {
            typeComboBox.addItem(type.getI18nFriendlyString(), type);
        }
    }

    public void refreshAdvisoryAgeList(PEGI_AGE[] ages) {
        advisoryAgeComboBox.clean();
        for (PEGI_AGE age : ages) {
            String label = age != null ? age.getI18nFriendlyString() : "";
            advisoryAgeComboBox.addItem(label, age);
        }
    }

    public void toggleAdvisoryFeaturesPanel() {
        //No PEGI rating, i.e. null value, disables all the connected features
        advisoryFeaturesPanel.setVisible(advisoryAgeComboBox.getSelectedValue() != null);
    }

    public void refreshProgrammeList(ActionCollection<ProgrammeForm> programmes) {
        //Clear the list beforehand
        programmeComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {
            //readableExtended programmes are reserved to Curators and Contributors
            if (programmeEntry.isEditable() || programmeEntry.hasAction(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE)) {
                programmeComboBox.appendUnselected(programmeEntry.getKey().getLabel(), programmeEntry.getKey());
            }
         }
    }

}
