/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.track;

import biz.ddcr.shampoo.client.form.track.AverageVoteModule;
import biz.ddcr.shampoo.client.form.track.HasAudioInterface;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTImage;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTMyVoteBox;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedTree;
import biz.ddcr.shampoo.client.view.widgets.GWTTruncatedLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericGridInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericImageInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTreeInterface;

/**
 *
 * @author okay_awright
 **/
public class BroadcastTrackDisplayView extends GenericFormPageView {
    //Track
    private GWTImage albumCoverPicture;
    private GWTDisclosurePanel trackInfoPanel;
    //Track properties widgets
    private GWTLabel enabledLabel;
    private GWTLabel typeLabel;
    private GWTTruncatedLabel authorLabel;
    private GWTTruncatedLabel titleLabel;
    private GWTTruncatedLabel albumLabel;
    private GWTTruncatedLabel descriptionLabel;
    private GWTLabel durationLabel;
    private GWTLabel yearOfReleaseLabel;
    private GWTTruncatedLabel genreLabel;
    //Votes
    private GWTDisclosurePanel votePanel;
    //Misc items
    private GWTPagedTree advisoryTree;
    private GWTLabel publisherLabel;
    private GWTLabel copyrightLabel;
    private GWTLabel tagLabel;
    //Programmes widgets
    private GWTGrid programmesGrid;

    public GenericTreeInterface getAdvisoryTree() {
        return advisoryTree;
    }

    public GenericImageInterface getAlbumCoverPicture() {
        return albumCoverPicture;
    }

    public GenericLabelInterface getAlbumLabel() {
        return albumLabel;
    }

    public GenericLabelInterface getAuthorLabel() {
        return authorLabel;
    }

    public GenericLabelInterface getPublisherLabel() {
        return publisherLabel;
    }
    
    public GenericLabelInterface getCopyrightLabel() {
        return copyrightLabel;
    }

    public GenericLabelInterface getDescriptionLabel() {
        return descriptionLabel;
    }

    public GenericLabelInterface getDurationLabel() {
        return durationLabel;
    }
    
    public GenericLabelInterface getEnabledLabel() {
        return enabledLabel;
    }

    public GenericLabelInterface getGenreLabel() {
        return genreLabel;
    }

    public GenericGridInterface getProgrammesGrid() {
        return programmesGrid;
    }

    public GenericLabelInterface getTagLabel() {
        return tagLabel;
    }

    public GenericLabelInterface getTitleLabel() {
        return titleLabel;
    }

    public GenericPanelInterface getTrackInfoPanel() {
        return trackInfoPanel;
    }

    public GenericLabelInterface getTypeLabel() {
        return typeLabel;
    }

    public GenericLabelInterface getYearOfReleaseLabel() {
        return yearOfReleaseLabel;
    }


    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_track();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "track";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().track_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //Channel widgets
        GWTVerticalPanel trackPanel = new GWTVerticalPanel();

        trackInfoPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().technical_info());
        trackPanel.add(trackInfoPanel);

        GWTGrid tagGrid = new GWTGrid();

        //Common tags
        GWTGrid grid = new GWTGrid();

        enabledLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().enabled() + ":"), enabledLabel);

        typeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().type() + ":"), typeLabel);

        authorLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().author() + ":"), authorLabel);

        titleLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().title() + ":"), titleLabel);

        albumLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().album() + ":"), albumLabel);

        descriptionLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().comment() + ":"), descriptionLabel);

        durationLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().duration() + ":"), durationLabel);
        
        yearOfReleaseLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().year() + ":"), yearOfReleaseLabel);

        genreLabel = new GWTTruncatedLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().genre() + ":"), genreLabel);

        //Cover art
        GWTFlowPanel newAlbumCoverArtPanel = new GWTFlowPanel();
        albumCoverPicture = new GWTImage();
        newAlbumCoverArtPanel.add(albumCoverPicture);

        tagGrid.addRow(grid, newAlbumCoverArtPanel);

        trackPanel.add(tagGrid);

        //Vote widget
        votePanel = new GWTDisclosurePanel(I18nTranslator.getInstance().votes());
        //programmatically updated with the proper widget
        trackPanel.add(votePanel);

        //Programme widgets
        GWTDisclosurePanel programmePanel = new GWTDisclosurePanel(I18nTranslator.getInstance().programmes_list());
        programmesGrid = new GWTGrid();
        programmePanel.add(programmesGrid);

        trackPanel.add(programmePanel);

        //Misc options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        advisoryTree = new GWTPagedTree();
        miscOptionsGrid.addRow(advisoryTree);

        publisherLabel = new GWTTruncatedLabel(null);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().publisher() + ":"), publisherLabel);
        
        copyrightLabel = new GWTTruncatedLabel(null);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().copyright() + ":"), copyrightLabel);

        tagLabel = new GWTTruncatedLabel(null);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().tag() + ":"), tagLabel);

        miscOptionsPanel.add(miscOptionsGrid);
        trackPanel.add(miscOptionsPanel);

        return trackPanel;
    }

    //Respond to presenter requests
    public void loadCoverArt(ActionCollectionEntry<? extends HasCoverArtInterface> module) {
        if (module!=null && module.getItem()!=null && module.isReadable()) {
            albumCoverPicture.setImage(module.getItem());
        } else {
            albumCoverPicture.clean();
        }
    }

    public void refreshExistingProgrammes(ActionCollection<String> programmes) {
        //clear the list beforehand
        programmesGrid.clean();

        //generate the list
        for (ActionCollectionEntry<String> programmeEntry : programmes) {
            if (programmeEntry.isReadable()) {
                addNewProgramme(programmeEntry.getKey());
            }
         }
    }

    public void addNewProgramme(String programmeId) {
        //Add the new programme to the list
        programmesGrid.addRow(new GWTLabel(programmeId));
    }

    public void refreshAdvisoryData(PEGIRatingModule advisoryModule) {
        advisoryTree.clean();

        if (advisoryModule!=null && advisoryModule.getRating()!=null) {
            Long newBranchId = advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().rating() + ":" + advisoryModule.getRating().getI18nFriendlyString()));
            if (advisoryModule.hasDiscrimination())
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().discrimination_advisory()), newBranchId);
            if (advisoryModule.hasDrugs())
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().drugs_advisory()), newBranchId);
            if (advisoryModule.hasFear())
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().fear_advisory()), newBranchId);
            if (advisoryModule.hasProfanity())
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().profanity_advisory()), newBranchId);
            if (advisoryModule.hasSex())
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().sex_advisory()), newBranchId);
            if (advisoryModule.hasViolence())
                advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().violence_advisory()), newBranchId);
        } else {
            advisoryTree.addItem(new GWTLabel(I18nTranslator.getInstance().rating() + ": " + I18nTranslator.getInstance().unrated()));
        }
    }

    public void refreshVoteData(String trackId, AverageVoteModule averageVoteModule, VoteChangeHandler<String> myRatingChangeHandler) {

        votePanel.setVisible(trackId!=null);

        votePanel.clean();

        GWTGrid voteGrid = new GWTGrid();

        GWTLabel averageRatingWidget = new GWTLabel(
                averageVoteModule!=null ?
                averageVoteModule.getI18nFriendlyName() :
                I18nTranslator.getInstance().none()
            );
        voteGrid.addRow(new GWTLabel(I18nTranslator.getInstance().average_vote()+":"), averageRatingWidget);

        GWTMyVoteBox voteBox = new GWTMyVoteBox(trackId, myRatingChangeHandler);
        voteGrid.addRow(new GWTLabel(I18nTranslator.getInstance().my_vote()+":"), voteBox);

        votePanel.add(voteGrid);
    }

    public void refreshTrackInfo(ActionCollectionEntry<ContainerModule> container) {
        //The eventBus will be bound using specificFormBind()
        trackInfoPanel.setVisible(container!=null && container.getItem()!=null && container.isEditable());
        trackInfoPanel.clean();
        if (container!=null && container.getItem()!=null && container.isEditable()) {
            final ContainerModule trackModule = container.getItem();
            trackInfoPanel.add(
                    new GWTLabel(
                        I18nTranslator.getInstance().audio_track_technical_info(
                            trackModule.getTrackFormat().getI18nFriendlyString(),
                            Long.toString(trackModule.getSize()),
                            Long.toString(trackModule.getBitrate()),
                            Integer.toString(trackModule.getSamplerate()),
                            Byte.toString(trackModule.getChannels()),
                            trackModule.isVbr() ? "VBR" : "A/CBR")));
        }
    }

}
