/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.views.track;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTExtendedTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTextBoxInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class DelegatedReviewView extends PageView {

    //Review properties widgets
    private GWTExtendedTextBox commentTextBox;
    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericTextBoxInterface getCommentTextBox() {
        return commentTextBox;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().reject_track();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "track";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().track_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        GWTVerticalPanel trackPanel = new GWTVerticalPanel();

        //Common tags
        GWTGrid grid = new GWTGrid();

        commentTextBox = new GWTExtendedTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().comment()+":"), commentTextBox);

        trackPanel.add(grid);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().ok());
        trackPanel.add(submit);

        return trackPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests

}
