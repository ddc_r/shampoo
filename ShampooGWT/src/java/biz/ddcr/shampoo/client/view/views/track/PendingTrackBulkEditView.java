/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.track;

import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class PendingTrackBulkEditView extends PageView {
    //Tracks

    private GWTPagedFormTable<PendingTrackForm, Widget> tracksList;
    //Track Picture
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableCoverPictureEditionCheckBox;
    private GWTClickableImage albumCoverPicture;
    private GWTPictureUploadMoxie albumCoverUploadPlupload;
    private GWTClickableLabel resetAlbumCoverArtLink;
    //Track properties widgets
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableTypeEditionCheckBox;
    private GWTPagedFormDropDownListBox<TYPE> typeComboBox;;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableAlbumEditionCheckBox;
    private GWTTextBox albumTextBox;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableDescriptionEditionCheckBox;
    private GWTExtendedTextBox descriptionTextBox;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableYearOfReleaseEditionCheckBox;
    private GWTNullableNumericBox yearOfReleaseNumericBox;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableGenreEditionCheckBox;
    private GWTTextBox genreTextBox;
    //Misc items
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enablePEGIEditionCheckBox;
    private GWTNullableFormDropDownListBox<PEGI_AGE> advisoryAgeComboBox;
    private GWTFlowPanel advisoryFeaturesPanel;
    private GWTCheckBox advisoryViolenceCheckBox;
    private GWTCheckBox advisoryProfanityCheckBox;
    private GWTCheckBox advisoryFearCheckBox;
    private GWTCheckBox advisorySexCheckBox;
    private GWTCheckBox advisoryDrugsCheckBox;
    private GWTCheckBox advisoryDiscriminationCheckBox;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enablePublisherEditionCheckBox;
    private GWTTextBox publisherTextBox;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableCopyrightEditionCheckBox;
    private GWTTextBox copyrightTextBox;
    //Enable or disable the bulk edition of a specific field
    private GWTCheckBox enableTagEditionCheckBox;
    private GWTTextBox tagTextBox;
    //Programmes widgets
    private GWTCheckBox enableProgrammeEditionCheckBox;
    private GWTPagedFormCommunicatingDualSelectBoxes<ProgrammeForm> programmeComboBox;
    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericDropDownListBoxInterface<TYPE> getTypeComboBox() {
        return typeComboBox;
    }

    public GenericCheckBoxInterface getEnableCoverPictureEditionCheckBox() {
        return enableCoverPictureEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableTypeEditionCheckBox() {
        return enableTypeEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableAlbumEditionCheckBox() {
        return enableAlbumEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableDescriptionEditionCheckBox() {
        return enableDescriptionEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableYearOfReleaseEditionCheckBox() {
        return enableYearOfReleaseEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableGenreEditionCheckBox() {
        return enableGenreEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnablePEGIEditionCheckBox() {
        return enablePEGIEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnablePublisherEditionCheckBox() {
        return enablePublisherEditionCheckBox;
    }
    
    public GenericCheckBoxInterface getEnableCopyrightEditionCheckBox() {
        return enableCopyrightEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableTagEditionCheckBox() {
        return enableTagEditionCheckBox;
    }

    public GenericCheckBoxInterface getEnableProgrammeEditionCheckBox() {
        return enableProgrammeEditionCheckBox;
    }

    public GenericHyperlinkInterface getAlbumCoverPicture() {
        return albumCoverPicture;
    }

    public GenericHyperlinkInterface getResetAlbumCoverArtLink() {
        return resetAlbumCoverArtLink;
    }

    public GenericDropDownListBoxInterface<PEGI_AGE> getAdvisoryAgeComboBox() {
        return advisoryAgeComboBox;
    }

    public GenericCheckBoxInterface getAdvisoryDiscriminationCheckBox() {
        return advisoryDiscriminationCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryDrugsCheckBox() {
        return advisoryDrugsCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryFearCheckBox() {
        return advisoryFearCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryProfanityCheckBox() {
        return advisoryProfanityCheckBox;
    }

    public GenericCheckBoxInterface getAdvisorySexCheckBox() {
        return advisorySexCheckBox;
    }

    public GenericCheckBoxInterface getAdvisoryViolenceCheckBox() {
        return advisoryViolenceCheckBox;
    }

    public GenericTextBoxInterface getAlbumTextBox() {
        return albumTextBox;
    }

    public GenericTextBoxInterface getPublisherTextBox() {
        return publisherTextBox;
    }
    
    public GenericTextBoxInterface getCopyrightTextBox() {
        return copyrightTextBox;
    }

    public GenericTextBoxInterface getDescriptionTextBox() {
        return descriptionTextBox;
    }

    public GenericTextBoxInterface getGenreTextBox() {
        return genreTextBox;
    }

    public GenericTextBoxInterface getTagTextBox() {
        return tagTextBox;
    }

    public GenericNumericBoxInterface<Integer> getYearOfReleaseNumericBox() {
        return yearOfReleaseNumericBox;
    }

    public GenericFormCommunicatingDualSelectBoxesInterface<ProgrammeForm> getProgrammeComboBox() {
        return programmeComboBox;
    }

    public GenericFormTableInterface<PendingTrackForm, Widget> getTracksListBox() {
        return tracksList;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericFileUploadInterface getAlbumCoverUploadWidget() {
        return albumCoverUploadPlupload;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().bulk_edit_tracks();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "track";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().track_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //Channel widgets
        GWTVerticalPanel trackPanel = new GWTVerticalPanel();

        GWTVerticalPanel trackListPanel = new GWTVerticalPanel();
        tracksList = new GWTPagedFormTable<PendingTrackForm, Widget>();
        trackListPanel.add(new GWTLabel(I18nTranslator.getInstance().tracks() + ":"));
        trackListPanel.add(tracksList);
        trackPanel.add(trackListPanel);

        GWTGrid tagGrid = new GWTGrid();

        //Common tags
        GWTGrid grid = new GWTGrid();

        enableTypeEditionCheckBox = new GWTCheckBox(false);
        typeComboBox = new GWTPagedFormDropDownListBox<TYPE>();
        grid.addRow(enableTypeEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().type() + ":"), typeComboBox);

        enableAlbumEditionCheckBox = new GWTCheckBox(false);
        albumTextBox = new GWTTextBox();
        grid.addRow(enableAlbumEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().album() + ":"), albumTextBox);

        enableDescriptionEditionCheckBox = new GWTCheckBox(false);
        descriptionTextBox = new GWTExtendedTextBox();
        grid.addRow(enableDescriptionEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().comment() + ":"), descriptionTextBox);

        enableYearOfReleaseEditionCheckBox = new GWTCheckBox(false);
        yearOfReleaseNumericBox = new GWTNullableNumericBox(null, -2000, +3000);
        grid.addRow(enableYearOfReleaseEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().year() + ":"), yearOfReleaseNumericBox);

        enableGenreEditionCheckBox = new GWTCheckBox(false);
        genreTextBox = new GWTTextBox();
        grid.addRow(enableGenreEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().genre() + ":"), genreTextBox);

        //Cover art
        GWTGrid grid2 = new GWTGrid();

        enableCoverPictureEditionCheckBox = new GWTCheckBox(false);

        GWTFlowPanel newAlbumCoverArtPanel = new GWTFlowPanel();
        albumCoverPicture = new GWTClickableImage();
        newAlbumCoverArtPanel.add(albumCoverPicture);

        albumCoverUploadPlupload = new GWTPictureUploadMoxie(null);
        resetAlbumCoverArtLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        newAlbumCoverArtPanel.add(albumCoverUploadPlupload);
        newAlbumCoverArtPanel.add(resetAlbumCoverArtLink);

        grid2.addRow(enableCoverPictureEditionCheckBox, newAlbumCoverArtPanel);

        tagGrid.addRow(grid, grid2);

        trackPanel.add(tagGrid);

        //Programme widgets
        GWTGrid grid3 = new GWTGrid();

        enableProgrammeEditionCheckBox = new GWTCheckBox(false);

        GWTVerticalPanel programmePanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel programmeLabel = new GWTLabel(I18nTranslator.getInstance().programmes_list());
        programmePanel.add(programmeLabel);
        programmeComboBox = new GWTPagedFormCommunicatingDualSelectBoxes<ProgrammeForm>();
        programmeComboBox.setHeader(I18nTranslator.getInstance().available(), I18nTranslator.getInstance().selection());
        programmePanel.add(programmeComboBox);

        grid3.addRow(enableProgrammeEditionCheckBox, programmePanel);

        trackPanel.add(grid3);

        //Misc options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        enablePEGIEditionCheckBox = new GWTCheckBox(false);

        GWTFlowPanel advisoryPanel = new GWTFlowPanel();

        GWTGrid advisoryAgeGrid = new GWTGrid();

        advisoryAgeComboBox = new GWTNullableFormDropDownListBox<PEGI_AGE>();
        advisoryAgeGrid.addRow(new GWTLabel(I18nTranslator.getInstance().rating() + ":"), advisoryAgeComboBox);
        advisoryPanel.add(advisoryAgeGrid);
        advisoryFeaturesPanel = new GWTFlowPanel();
        advisoryViolenceCheckBox = new GWTCheckBox(false);
        advisoryViolenceCheckBox.setCaption(I18nTranslator.getInstance().violence_advisory());
        advisoryFeaturesPanel.add(advisoryViolenceCheckBox);
        advisoryProfanityCheckBox = new GWTCheckBox(false);
        advisoryProfanityCheckBox.setCaption(I18nTranslator.getInstance().profanity_advisory());
        advisoryFeaturesPanel.add(advisoryProfanityCheckBox);
        advisoryFearCheckBox = new GWTCheckBox(false);
        advisoryFearCheckBox.setCaption(I18nTranslator.getInstance().fear_advisory());
        advisoryFeaturesPanel.add(advisoryFearCheckBox);
        advisorySexCheckBox = new GWTCheckBox(false);
        advisorySexCheckBox.setCaption(I18nTranslator.getInstance().sex_advisory());
        advisoryFeaturesPanel.add(advisorySexCheckBox);
        advisoryDrugsCheckBox = new GWTCheckBox(false);
        advisoryDrugsCheckBox.setCaption(I18nTranslator.getInstance().drugs_advisory());
        advisoryFeaturesPanel.add(advisoryDrugsCheckBox);
        advisoryDiscriminationCheckBox = new GWTCheckBox(false);
        advisoryDiscriminationCheckBox.setCaption(I18nTranslator.getInstance().discrimination_advisory());
        advisoryFeaturesPanel.add(advisoryDiscriminationCheckBox);
        advisoryPanel.add(advisoryFeaturesPanel);

        miscOptionsGrid.addRow(enablePEGIEditionCheckBox, advisoryPanel);

        enablePublisherEditionCheckBox = new GWTCheckBox(false);
        publisherTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(enablePublisherEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().publisher() + ":"), publisherTextBox);        
        
        enableCopyrightEditionCheckBox = new GWTCheckBox(false);
        copyrightTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(enableCopyrightEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().copyright() + ":"), copyrightTextBox);

        enableTagEditionCheckBox = new GWTCheckBox(false);
        tagTextBox = new GWTTextBox();
        miscOptionsGrid.addRow(enableTagEditionCheckBox, new GWTLabel(I18nTranslator.getInstance().tag() + ":"), tagTextBox);

        miscOptionsPanel.add(miscOptionsGrid);
        trackPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        trackPanel.add(submit);

        return trackPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests
    public void loadCoverArt(HasCoverArtInterface module) {
        albumCoverPicture.setImage(module);
    }

    public void unloadCoverArt() {
        albumCoverPicture.setImage((HasCoverArtInterface) null);
    }

    public void refreshTypeList(TYPE[] types) {
        typeComboBox.clean();
        for (TYPE type : types) {
            typeComboBox.addItem(type.getI18nFriendlyString(), type);
        }
    }

    public void refreshAdvisoryAgeList(PEGI_AGE[] ages) {
        advisoryAgeComboBox.clean();
        for (PEGI_AGE age : ages) {
            String label = age != null ? age.getI18nFriendlyString() : "";
            advisoryAgeComboBox.addItem(label, age);
        }
    }

    public void toggleAdvisoryFeaturesPanel() {
        //No PEGI rating, i.e. null value, disables all the connected features
        advisoryFeaturesPanel.setVisible(advisoryAgeComboBox.getSelectedValue() != null);
    }

    public void refreshProgrammeList(ActionCollection<ProgrammeForm> programmes) {
        //Clear the list beforehand
        programmeComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ProgrammeForm> programmeEntry : programmes) {
            //readableExtended programmes are reserved to Curators and Contributors
            if (programmeEntry.isEditable() || programmeEntry.hasAction(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE)) {
                programmeComboBox.appendUnselected(programmeEntry.getKey().getLabel(), programmeEntry.getKey());
            }
        }
    }

    public void toggleTypeEdition() {
        typeComboBox.setEnabled(enableTypeEditionCheckBox.isChecked());
    }

    public void toggleCoverPictureEdition() {
        albumCoverPicture.setEnabled(enableCoverPictureEditionCheckBox.isChecked());
        albumCoverUploadPlupload.setEnabled(enableCoverPictureEditionCheckBox.isChecked());
        resetAlbumCoverArtLink.setEnabled(enableCoverPictureEditionCheckBox.isChecked());
    }

    public void toggleAlbumEdition() {
        albumTextBox.setEnabled(enableAlbumEditionCheckBox.isChecked());
    }

    public void toggleDescriptionEdition() {
        descriptionTextBox.setEnabled(enableDescriptionEditionCheckBox.isChecked());
    }

    public void toggleYearOfReleaseEdition() {
        yearOfReleaseNumericBox.setEnabled(enableYearOfReleaseEditionCheckBox.isChecked());
    }

    public void toggleGenreEdition() {
        genreTextBox.setEnabled(enableGenreEditionCheckBox.isChecked());
    }

    public void togglePEGIEdition() {
        advisoryAgeComboBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
        advisoryViolenceCheckBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
        advisoryProfanityCheckBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
        advisoryFearCheckBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
        advisorySexCheckBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
        advisoryDrugsCheckBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
        advisoryDiscriminationCheckBox.setEnabled(enablePEGIEditionCheckBox.isChecked());
    }

    public void togglePublisherEdition() {
        publisherTextBox.setEnabled(enablePublisherEditionCheckBox.isChecked());
    }
    
    public void toggleCopyrightEdition() {
        copyrightTextBox.setEnabled(enableCopyrightEditionCheckBox.isChecked());
    }

    public void toggleTagEdition() {
        tagTextBox.setEnabled(enableTagEditionCheckBox.isChecked());
    }

    public void toggleProgrammesEdition() {
        programmeComboBox.setEnabled(enableProgrammeEditionCheckBox.isChecked());
    }

    public void refreshTracksList(Collection<PendingTrackForm> forms, ClickHandler trackDisplayClickHandler) {
        tracksList.clean();
        if (forms!=null)
            for (PendingTrackForm form : forms) {
                tracksList.addRow(form, false, new GWTClickableLabel(form.getFriendlyCaption(), trackDisplayClickHandler));
            }

    }
}
