/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.track;

import biz.ddcr.shampoo.client.form.track.BroadcastAdvertForm;
import biz.ddcr.shampoo.client.form.track.BroadcastJingleForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.PendingTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.TRACK_ACTION;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;

/**
 *
 * @author okay_awright
 *
 */
public class TrackListView extends PageView {

    GWTTabPanel tabPanel;
    //Tracks that are broadcast
    GWTVerticalPanel broadcastPanel;
    GWTFormSortableTable<BroadcastTrackForm, Widget, BROADCASTABLE_TRACK_TAGS> broadcastTrackList;
    //Tracks in the curation queue
    GWTVerticalPanel pendingPanel;
    GWTFormSortableTable<PendingTrackForm, Widget, PENDING_TRACK_TAGS> pendingTrackList;
    //Adding new tracks
    GWTClickableLabel addBroadcastTrackLink;
    GWTClickableLabel addPendingTrackLink;

    public GenericFormSortableTableInterface<BroadcastTrackForm, Widget, BROADCASTABLE_TRACK_TAGS> getBroadcastTrackList() {
        return broadcastTrackList;
    }

    public GenericFormSortableTableInterface<PendingTrackForm, Widget, PENDING_TRACK_TAGS> getPendingTrackList() {
        return pendingTrackList;
    }

    public GenericHyperlinkInterface getAddBroadcastTrackLink() {
        return addBroadcastTrackLink;
    }

    public GenericHyperlinkInterface getAddPendingTrackLink() {
        return addPendingTrackLink;
    }

    public GenericPanelInterface getBroadcastPanel() {
        return broadcastPanel;
    }

    public GenericPanelInterface getPendingPanel() {
        return pendingPanel;
    }

    public GenericTabPanelInterface getTabPanel() {
        return tabPanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().track_heading();
    }

    @Override
    public String getHeaderCSSMarker() {
        return "track";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        tabPanel = new GWTTabPanel();
        mainPanel.add(tabPanel);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void buildTabPanel(boolean canBuildBroadcastList, ClickHandler broadcastClickHandler, boolean canBuildPendingList, ClickHandler pendingClickHandler, final boolean autoSelectPendingTab) {

        tabPanel.clean();
        if (canBuildBroadcastList) {
            int broadcastPanelIndex = 0;
            broadcastPanel = new GWTVerticalPanel();
            tabPanel.insertAt(broadcastPanel, I18nTranslator.getInstance().broadcastable_list(), broadcastPanelIndex);
            tabPanel.addClickHandler(broadcastClickHandler, broadcastPanelIndex);
        }
        if (canBuildPendingList) {
            int pendingPanelIndex = canBuildBroadcastList ? 1 : 0;
            pendingPanel = new GWTVerticalPanel();
            tabPanel.insertAt(pendingPanel, I18nTranslator.getInstance().submission_pending_queue(), pendingPanelIndex);
            tabPanel.addClickHandler(pendingClickHandler, pendingPanelIndex);
        }
        //Refresh the first available one immediately
        //selectFirstTab() will transparently call the click action associated with the said tab
        if (canBuildPendingList && autoSelectPendingTab)
            tabPanel.selectLastTab();
        else
            tabPanel.selectFirstTab();

    }

    public void refreshBroadcastTrackPanel(SortableTableClickHandler listHeaderClickHandler) {

        broadcastPanel.clean();

        broadcastTrackList = new GWTFormSortableTable<BroadcastTrackForm, Widget, BROADCASTABLE_TRACK_TAGS>();
        //Generate list header
        broadcastTrackList.setHeader(
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().cover_art()),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().enabled(), BROADCASTABLE_TRACK_TAGS.enabled),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().type(), BROADCASTABLE_TRACK_TAGS.type),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().author(), BROADCASTABLE_TRACK_TAGS.author),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().title(), BROADCASTABLE_TRACK_TAGS.title),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().album(), BROADCASTABLE_TRACK_TAGS.album),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().genre(), BROADCASTABLE_TRACK_TAGS.genre),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().description(), BROADCASTABLE_TRACK_TAGS.description),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().duration(), BROADCASTABLE_TRACK_TAGS.duration),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().release_date(), BROADCASTABLE_TRACK_TAGS.yearOfRelease),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().average_vote(), BROADCASTABLE_TRACK_TAGS.averageVote),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().my_vote(), BROADCASTABLE_TRACK_TAGS.myVote),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().my_request(), BROADCASTABLE_TRACK_TAGS.myRequest),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().programmes(), BROADCASTABLE_TRACK_TAGS.programmes),
                new ColumnHeader<BROADCASTABLE_TRACK_TAGS>(I18nTranslator.getInstance().operation()));
        broadcastTrackList.setRefreshClickHandler(listHeaderClickHandler);

        broadcastPanel.add(broadcastTrackList);
    }

    public void refreshPendingTrackPanel(SortableTableClickHandler listHeaderClickHandler) {

        pendingPanel.clean();

        pendingTrackList = new GWTFormSortableTable<PendingTrackForm, Widget, PENDING_TRACK_TAGS>();
        //Generate list header
        pendingTrackList.setHeader(
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().cover_art()),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().type(), PENDING_TRACK_TAGS.type),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().author(), PENDING_TRACK_TAGS.author),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().title(), PENDING_TRACK_TAGS.title),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().album(), PENDING_TRACK_TAGS.album),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().genre(), PENDING_TRACK_TAGS.genre),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().description(), PENDING_TRACK_TAGS.description),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().duration(), PENDING_TRACK_TAGS.duration),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().release_date(), PENDING_TRACK_TAGS.yearOfRelease),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().programmes(), PENDING_TRACK_TAGS.programmes),
                new ColumnHeader<PENDING_TRACK_TAGS>(I18nTranslator.getInstance().operation()));
        pendingTrackList.setRefreshClickHandler(listHeaderClickHandler);

        pendingPanel.add(pendingTrackList);
    }

    public void refreshAddBroadcastTrackLink(ClickHandler clickHandler) {
        //Add a regular track
        addBroadcastTrackLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_track());
        if (clickHandler != null) {
            addBroadcastTrackLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addBroadcastTrackLink);
    }

    public void refreshAddPendingTrackLink(ClickHandler clickHandler) {
        //Add a regular track
        addPendingTrackLink = new GWTClickableLabelSuggest(I18nTranslator.getInstance().suggest_track());
        if (clickHandler != null) {
            addPendingTrackLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addPendingTrackLink);
    }

    public void refreshAddNewRequestLink(GenericPanelInterface operationWidget, ClickHandler addRequestClickHandler) {
        GWTClickableLabel addLink = new GWTClickableLabelRequest(I18nTranslator.getInstance().request());
        if (addRequestClickHandler != null) {
            addLink.addClickHandler(addRequestClickHandler);
        }
        operationWidget.add(addLink);
    }

    public void refreshAddDeleteRequestLink(GenericPanelInterface operationWidget, ClickHandler deleteRequestClickHandler) {
        GWTClickableLabel deleteLink = new GWTClickableLabelUnrequest(I18nTranslator.getInstance().withdraw_request());
        if (deleteRequestClickHandler != null) {
            deleteLink.addClickHandler(deleteRequestClickHandler);
        }
        operationWidget.add(deleteLink);
    }

    public void refreshAddRequestInfo(RequestModule request, GenericPanelInterface requestBox) {
        if (requestBox != null) {
            requestBox.clean();

            if (request != null) {
                GWTPagedTree requestTree = new GWTPagedTree();
                Long newBranchId = null;
                if (request.isStreaming()) {
                    newBranchId = requestTree.addItem(new GWTLabel(I18nTranslator.getInstance().on_air()));
                } else if (request.isQueued()) {
                    newBranchId = requestTree.addItem(new GWTLabel(I18nTranslator.getInstance().queued_up()));
                } else {
                    newBranchId = requestTree.addItem(new GWTLabel(I18nTranslator.getInstance().pending()));
                }
                newBranchId = requestTree.addItem(new GWTLabel(request.getChannel()), newBranchId);
                if (request.getMessage() != null && request.getMessage().length() != 0) {
                    requestTree.addItem(new GWTTruncatedLabel(request.getMessage()), newBranchId);
                }

                requestBox.add(requestTree);
            }
        }
    }

    public void refreshBroadcastTrackList(
            ActionCollection<? extends BroadcastTrackForm> tracks,
            ClickHandler editClickHandler,
            final ClickHandler editAllClickHandler,
            YesNoClickHandler deleteClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            VoteChangeHandler<String> myRatingChangeHandler,
            ClickHandler trackDisplayClickHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler,
            GenericCallbackInterface<String, GenericPanelInterface, GenericPanelInterface> requestHandler,
            EventBusWithLookup eventBus,
            String jsessionid) {

        //Clear the list beforehand
        broadcastTrackList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        broadcastTrackList.setNextButtonVisible((tracks.size() >= broadcastTrackList.getMaxVisibleRows()));

        //Default, uninitialized, values are false by default
        boolean canAtLeastDeleteOneItem = false;
        boolean canAtLeastEditOneItem = false;

        for (ActionCollectionEntry<? extends BroadcastTrackForm> trackEntry : tracks) {

            if (trackEntry.isReadable()) {
                BroadcastTrackForm track = trackEntry.getKey();

                GWTLabel typeLabel = new GWTLabel(null);
                if (track instanceof BroadcastSongForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().song());
                } else if (track instanceof BroadcastJingleForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().jingle());
                } else if (track instanceof BroadcastAdvertForm) {
                    typeLabel.setCaption(I18nTranslator.getInstance().advert());
                } else {
                    typeLabel.setCaption(I18nTranslator.getInstance().unknown());
                }

                Widget coverArtWidget = null;
                if (track.isReady()) {
                    coverArtWidget = new GWTThumbnail(track.getCoverArtFile() != null ? track.getCoverArtFile().getItem() : null);
                } else {
                    coverArtWidget = new GWTCSSFlowPanel("GWTWorking");
                }

                GWTLabel averageRatingWidget = null;
                if (track instanceof BroadcastSongForm) {
                    if (((BroadcastSongForm) track).getAverageVote() != null) {
                        averageRatingWidget = new GWTLabel(((BroadcastSongForm) track).getAverageVote().getI18nFriendlyName());
                    } else {
                        averageRatingWidget = new GWTLabel(I18nTranslator.getInstance().none());
                    }
                } else {
                    averageRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
                }

                GWTPagedTree programmeWidget = new GWTPagedTree();
                for (ActionCollectionEntry<BroadcastTrackProgrammeModule> programmeEntry : track.getTrackProgrammes()) {
                    if (programmeEntry.isReadable()) {
                        final BroadcastTrackProgrammeModule trackProgrammeModule = programmeEntry.getKey();
                        ClickHandler programmeClickHandler = new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent ce) {
                                programmeDisplaySelectionHandler.onItemSelected(trackProgrammeModule.getProgramme());
                            }
                        };
                        Long newBranchId = programmeWidget.getItemByName(trackProgrammeModule.getProgramme());
                        if (newBranchId == null) {
                            newBranchId = programmeWidget.addItemWithName(trackProgrammeModule.getProgramme(), new GWTClickableLabel(trackProgrammeModule.getProgramme(), programmeClickHandler));
                        }

                        programmeWidget.addItem(new GWTLabel(I18nTranslator.getInstance().rotation_count() + ": " + trackProgrammeModule.getRotationCount()), newBranchId);
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (track.isReady()
                        && trackEntry.hasAction(TRACK_ACTION.EXTENDED_VIEW)) {
                    if (track.getTrackFile() != null && track.getTrackFile().getItem() != null) {
                        GWTPlayerSoundManager2 playerApplet = new GWTPlayerSoundManager2(track.getTrackFile().getItem(), eventBus, jsessionid);
                        operationPanel.add(playerApplet);
                    }
                }
                if (!track.isNowPlaying() &&//
                        track.isReady() &&//
                        trackEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                    canAtLeastEditOneItem = true;
                }
                if (!track.isNowPlaying() &&//
                        track.isReady() &&//
                        trackEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(track.getFriendlyID()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                    canAtLeastDeleteOneItem = true;
                }

                Widget myRatingWidget = null;
                Widget myRequestWidget = null;
                if (track instanceof BroadcastSongForm) {
                    myRatingWidget = new GWTMyVoteBox(track.getRefID(), myRatingChangeHandler);
                    myRequestWidget = new GWTFlowPanel();
                    //Callback to the request for display, if any, not an independent widget like MyVote
                    //It will both update the operation panel (with a new link) and the request panel (with info)
                    requestHandler.onCallBack(track.getRefID(), (GenericPanelInterface) myRequestWidget, operationPanel);
                } else {
                    myRatingWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
                    myRequestWidget = new GWTLabel(I18nTranslator.getInstance().not_applicable());
                }

                //the header must be built before adding a row since this latter method check its presence
                final boolean _canAtLeastDeleteOneItem = canAtLeastDeleteOneItem;
                final boolean _canAtLeastEditOneItem = canAtLeastEditOneItem;
                broadcastTrackList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {
                    @Override
                    public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {

                        //Delete checked
                        if (_canAtLeastDeleteOneItem) {
                            GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                            deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                            if (deleteAllClickHandler != null) {
                                deleteLink.addClickHandler(deleteAllClickHandler);
                            }
                            captionContainer.add(deleteLink);
                        }
                        if (_canAtLeastEditOneItem) {
                            //Bulk edit checked
                            GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().bulk_edit());
                            if (editAllClickHandler != null) {
                                editLink.addClickHandler(editAllClickHandler);
                            }
                            captionContainer.add(editLink);
                        }
                    }
                });

                broadcastTrackList.addRow(track,
                        !track.isNowPlaying() && track.isReady() && (trackEntry.isDeletable() || trackEntry.isEditable()),
                        coverArtWidget,
                        new GWTLabel(track.isActivated() ? I18nTranslator.getInstance().is_true() : I18nTranslator.getInstance().is_false()),
                        typeLabel,
                        new GWTClickableLabel(track.getAuthor(), trackDisplayClickHandler),
                        new GWTClickableLabel(track.getTitle(), trackDisplayClickHandler),
                        new GWTLabel(track.getAlbum()),
                        new GWTLabel(track.getGenre()),
                        new GWTTruncatedLabel(track.getDescription()),
                        new GWTLabel(track.getTrackFile() != null && track.getTrackFile().getItem() != null && track.getTrackFile().getItem().getDuration() != null ? track.getTrackFile().getItem().getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown()),
                        new GWTLabel(track.getYearOfRelease() != null ? Short.toString(track.getYearOfRelease()) : I18nTranslator.getInstance().unknown()),
                        averageRatingWidget,
                        myRatingWidget,
                        myRequestWidget,
                        programmeWidget,
                        operationPanel);
            }
        }

    }

    public void refreshPendingTrackList(
            ActionCollection<? extends PendingTrackForm> tracks,
            ClickHandler editClickHandler,
            final ClickHandler editAllClickHandler,
            YesNoClickHandler deleteClickHandler,
            YesNoClickHandler validateClickHandler,
            ClickHandler rejectClickHandler,
            final YesNoClickHandler deleteAllClickHandler,
            final YesNoClickHandler validateAllClickHandler,
            final ClickHandler rejectAllClickHandler,
            ClickHandler trackDisplayClickHandler,
            final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler,
            EventBusWithLookup eventBus,
            String jsessionid) {

        //Clear the list beforehand
        pendingTrackList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        pendingTrackList.setNextButtonVisible((tracks.size() >= pendingTrackList.getMaxVisibleRows()));

        //Default, uninitialized, values are false by default
        boolean canAtLeastDeleteOneItem = false;
        boolean canAtLeastValidateOneItem = false;
        boolean canAtLeastRejectOneItem = false;
        boolean canAtLeastEditOneItem = false;

        for (ActionCollectionEntry<? extends PendingTrackForm> trackEntry : tracks) {

            if (trackEntry.isReadable()) {
                PendingTrackForm track = trackEntry.getKey();

                Widget coverArtWidget = null;
                if (track.isReady()) {
                    coverArtWidget = new GWTThumbnail(track.getCoverArtFile() != null ? track.getCoverArtFile().getItem() : null);
                } else {
                    coverArtWidget = new GWTCSSFlowPanel("GWTWorking");
                }

                boolean atLeastOneProgrammeLinked = false;

                GWTGrid programmeWidget = new GWTGrid();
                for (ActionCollectionEntry<PendingTrackProgrammeModule> programmeEntry : track.getTrackProgrammes()) {
                    if (programmeEntry.isReadable()) {
                        final PendingTrackProgrammeModule trackProgrammeModule = programmeEntry.getKey();
                        ClickHandler programmeClickHandler = new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent ce) {
                                programmeDisplaySelectionHandler.onItemSelected(trackProgrammeModule.getProgramme());
                            }
                        };
                        programmeWidget.addRow(new GWTClickableLabel(trackProgrammeModule.getProgramme(), programmeClickHandler));
                        atLeastOneProgrammeLinked = true;
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (track.isReady()
                        && trackEntry.hasAction(TRACK_ACTION.EXTENDED_VIEW)) {
                    if (track.getTrackFile() != null && track.getTrackFile().getItem() != null) {
                        GWTPlayerSoundManager2 playerApplet = new GWTPlayerSoundManager2(track.getTrackFile().getItem(), eventBus, jsessionid);
                        operationPanel.add(playerApplet);
                    }
                }
                if (track.isReady()
                        && trackEntry.hasAction(TRACK_ACTION.REVIEW)/**
                         * i.e. can validate a track
                         */
                        ) {

                    //A track can only be rejected or validated if it's not already been reviewed
                    if (atLeastOneProgrammeLinked) {
                        GWTClickableLabelWithYesNoConfirmation validateLink = new GWTClickableLabelWithYesNoConfirmationValidate(I18nTranslator.getInstance().validate());
                        validateLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_validate_x(track.getFriendlyID()));
                        if (validateClickHandler != null) {
                            validateLink.addClickHandler(validateClickHandler);
                        }
                        operationPanel.add(validateLink);
                        canAtLeastValidateOneItem = true;

                        GWTClickableLabel rejectLink = new GWTClickableLabelReject(I18nTranslator.getInstance().reject());
                        if (rejectClickHandler != null) {
                            rejectLink.addClickHandler(rejectClickHandler);
                        }
                        operationPanel.add(rejectLink);
                        canAtLeastRejectOneItem = true;
                    }
                }
                if (track.isReady()
                        && trackEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                    canAtLeastEditOneItem = true;
                }
                if (track.isReady()
                        && trackEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(track.getFriendlyID()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                    canAtLeastDeleteOneItem = true;
                }

                //the header must be built before adding a row since this latter method check its presence
                final boolean _canAtLeastDeleteOneItem = canAtLeastDeleteOneItem;
                final boolean _canAtLeastValidateOneItem = canAtLeastValidateOneItem;
                final boolean _canAtLeastRejectOneItem = canAtLeastRejectOneItem;
                final boolean _canAtLeastEditOneItem = canAtLeastEditOneItem;
                pendingTrackList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {
                    @Override
                    public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                        //validate all
                        if (_canAtLeastValidateOneItem) {
                            GWTClickableLabelWithYesNoConfirmation validateLink = new GWTClickableLabelWithYesNoConfirmationValidate(I18nTranslator.getInstance().validate());
                            validateLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_validate_selected_items());
                            if (validateAllClickHandler != null) {
                                validateLink.addClickHandler(validateAllClickHandler);
                            }
                            captionContainer.add(validateLink);
                        }
                        //reject all
                        if (_canAtLeastRejectOneItem) {
                            GWTClickableLabel rejectLink = new GWTClickableLabelReject(I18nTranslator.getInstance().reject());
                            if (rejectAllClickHandler != null) {
                                rejectLink.addClickHandler(rejectAllClickHandler);
                            }
                            captionContainer.add(rejectLink);
                        }
                        //delete all
                        if (_canAtLeastDeleteOneItem) {
                            GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                            deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                            if (deleteAllClickHandler != null) {
                                deleteLink.addClickHandler(deleteAllClickHandler);
                            }
                            captionContainer.add(deleteLink);
                        }
                        //Bulk edit checked
                        if (_canAtLeastEditOneItem) {
                            GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().bulk_edit());
                            if (editAllClickHandler != null) {
                                editLink.addClickHandler(editAllClickHandler);
                            }
                            captionContainer.add(editLink);
                        }
                    }
                });

                pendingTrackList.addRow(track,
                        track.isReady() && (trackEntry.isDeletable() || trackEntry.isEditable() || trackEntry.hasAction(TRACK_ACTION.REVIEW)/**
                         * i.e. can be validated *
                         */
                        ),
                        coverArtWidget,
                        new GWTLabel(track.getType().getI18nFriendlyString()),
                        new GWTClickableLabel(track.getAuthor(), trackDisplayClickHandler),
                        new GWTClickableLabel(track.getTitle(), trackDisplayClickHandler),
                        new GWTLabel(track.getAlbum()),
                        new GWTLabel(track.getGenre()),
                        new GWTLabel(track.getDescription()),
                        new GWTLabel(track.getTrackFile() != null && track.getTrackFile().getItem() != null && track.getTrackFile().getItem().getDuration() != null ? track.getTrackFile().getItem().getDuration().getI18nFriendlyString() : I18nTranslator.getInstance().unknown()),
                        new GWTLabel(track.getYearOfRelease() != null ? track.getYearOfRelease().toString() : I18nTranslator.getInstance().unknown()),
                        programmeWidget,
                        operationPanel);
            }
        }
    }
}
