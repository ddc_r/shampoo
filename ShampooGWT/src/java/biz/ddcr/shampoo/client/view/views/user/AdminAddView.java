/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.user;


import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTCheckBox;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedComboBox;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTPasswordTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericCheckBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericComboBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTextBoxInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 *
 * @author okay_awright
 **/
public class AdminAddView extends PageView {

    //User widgets
    private GWTCheckBox enabledCheckBox;
    private GWTTextBox usernameTextBox;
    private GWTPasswordTextBox passwordTextBox;
    private GWTPasswordTextBox confirmPasswordTextBox;
    private GWTTextBox firstAndLastNameTextBox;
    private GWTTextBox emailTextBox;
    private GWTPagedComboBox timezoneComboBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericTextBoxInterface getConfirmPasswordTextBox() {
        return confirmPasswordTextBox;
    }

    public GenericTextBoxInterface getEmailTextBox() {
        return emailTextBox;
    }

    public GenericCheckBoxInterface getEnabledCheckBox() {
        return enabledCheckBox;
    }

    public GenericTextBoxInterface getFirstAndLastNameTextBox() {
        return firstAndLastNameTextBox;
    }

    public GenericTextBoxInterface getPasswordTextBox() {
        return passwordTextBox;
    }

    public GenericTextBoxInterface getUsernameTextBox() {
        return usernameTextBox;
    }

    public GenericComboBoxInterface getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    //Setup minimum view

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().add_admin();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().user_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //User widgets
        GWTVerticalPanel userPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        enabledCheckBox = new GWTCheckBox(false);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().enabled() + ":"), enabledCheckBox);

        usernameTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().username() + ":"), usernameTextBox);

        passwordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password() + ":"), passwordTextBox);

        confirmPasswordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password_bis() + ":"), confirmPasswordTextBox);

        firstAndLastNameTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().first_and_last_name() + ":"), firstAndLastNameTextBox);

        emailTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().email() + ":"), emailTextBox);

        userPanel.add(grid);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedComboBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        miscOptionsPanel.add(miscOptionsGrid);

        userPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        userPanel.add(submit);

        return userPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    public void refreshTimezoneList(Collection<String> timezones) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        for (String timezone : timezones)
            timezoneComboBox.addItem(timezone,timezone);
        //Select a default value
        timezoneComboBox.setSelectedValue("UTC");

    }

}
