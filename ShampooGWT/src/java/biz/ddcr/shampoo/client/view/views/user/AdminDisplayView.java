/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.user;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;


/**
 *
 * @author okay_awright
 **/
public class AdminDisplayView extends GenericFormPageView {

    //User widgets
    private GWTLabel enabledLabel;
    private GWTLabel usernameLabel;
    private GWTLabel firstAndLastNameLabel;
    private GWTLabel emailLabel;
    private GWTLabel timezoneLabel;

    public GenericLabelInterface getEmailLabel() {
        return emailLabel;
    }

    public GenericLabelInterface getEnabledLabel() {
        return enabledLabel;
    }

    public GenericLabelInterface getFirstAndLastNameLabel() {
        return firstAndLastNameLabel;
    }

    public GenericLabelInterface getUsernameLabel() {
        return usernameLabel;
    }

    public GenericLabelInterface getTimezoneLabel() {
        return timezoneLabel;
    }

    //Setup minimum view

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_admin();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().user_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //User widgets
        GWTVerticalPanel userPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        grid.addRow(new GWTLabel(I18nTranslator.getInstance().account() + ":"), new GWTLabel(I18nTranslator.getInstance().admin()));

        enabledLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().enabled() + ":"), enabledLabel);

        usernameLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().username() + ":"), usernameLabel);

        firstAndLastNameLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().first_and_last_name() + ":"), firstAndLastNameLabel);

        emailLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().email() + ":"), emailLabel);

        userPanel.add(grid);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();
        timezoneLabel = new GWTLabel(null);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneLabel);
        miscOptionsPanel.add(miscOptionsGrid);

        userPanel.add(miscOptionsPanel);

        return userPanel;
    }

    //Respond to presenter requests

}
