/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.user;

import biz.ddcr.shampoo.client.form.right.ProgrammeRole;
import biz.ddcr.shampoo.client.form.right.ChannelRole;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.form.user.ProfileRightForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class ChangeMyProfileView extends PageView {

    //This widget is only published in order to bind the hourglass
    private GWTVerticalPanel userPanel;
    //User widgets
    private GWTLabel usernameLabel;
    private GWTCheckBox changePasswordCheckBox;
    private GWTPasswordTextBox oldPasswordTextBox;
    private GWTPasswordTextBox newPasswordTextBox;
    private GWTPasswordTextBox confirmNewPasswordTextBox;
    private GWTTextBox firstAndLastNameTextBox;
    private GWTTextBox emailTextBox;
    private GWTPagedComboBox timezoneComboBox;
    private GWTCheckBox emailNotificationCheckBox;
    //Rights
    private GWTVerticalPanel rightPanel;
    private GWTLabel accountTypeLabel;
    private GWTPagedTree rightTree;
    //Modifiable rights
    private GWTVerticalPanel editableRightPanel;
    private GWTPagedFormDropDownListBox<Role> editableRoleRightComboBox;
    private GWTPagedFormDropDownListBox<String> editableDomainRightComboBox;
    private GWTClickableLabel addNewRightButton;
    private GWTPagedFormTable<RightForm, Widget> editableRightsListBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericPanelInterface getUserPanel() {
        return userPanel;
    }

    public GenericCheckBoxInterface getEmailNotificationCheckBox() {
        return emailNotificationCheckBox;
    }

    public GenericCheckBoxInterface getChangePasswordCheckBox() {
        return changePasswordCheckBox;
    }

    public GenericTextBoxInterface getOldPasswordTextBox() {
        return oldPasswordTextBox;
    }

    public GenericTextBoxInterface getConfirmNewPasswordTextBox() {
        return confirmNewPasswordTextBox;
    }

    public GenericLabelInterface getUsernameLabel() {
        return usernameLabel;
    }

    public GenericTextBoxInterface getEmailTextBox() {
        return emailTextBox;
    }

    public GenericTextBoxInterface getFirstAndLastNameTextBox() {
        return firstAndLastNameTextBox;
    }

    public GenericTextBoxInterface getNewPasswordTextBox() {
        return newPasswordTextBox;
    }

    public GenericLabelInterface getAccountTypeLabel() {
        return accountTypeLabel;
    }

    public GenericComboBoxInterface getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericPanelInterface getRightPanel() {
        return rightPanel;
    }

    public GenericPanelInterface getEditableRightPanel() {
        return editableRightPanel;
    }

    public GenericHyperlinkInterface getAddNewRightButton() {
        return addNewRightButton;
    }

    public GenericDropDownListBoxInterface<String> getEditableDomainRightComboBox() {
        return editableDomainRightComboBox;
    }

    public GenericFormTableInterface<RightForm, Widget> getEditableRightsListBox() {
        return editableRightsListBox;
    }

    public GenericDropDownListBoxInterface<Role> getEditableRoleRightComboBox() {
        return editableRoleRightComboBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().change_my_profile();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().user_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //User widgets
        userPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        accountTypeLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().account() + ":"), accountTypeLabel);

        usernameLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().username() + ":"), usernameLabel);

        changePasswordCheckBox = new GWTCheckBox(false);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().change_my_password() + ":"), changePasswordCheckBox);

        oldPasswordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().old_password() + ":"), oldPasswordTextBox);

        newPasswordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().new_password() + ":"), newPasswordTextBox);

        confirmNewPasswordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().new_password_bis() + ":"), confirmNewPasswordTextBox);

        firstAndLastNameTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().first_and_last_name() + ":"), firstAndLastNameTextBox);

        emailTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().email() + ":"), emailTextBox);

        userPanel.add(grid);

        rightPanel = new GWTVerticalPanel();
        GWTLabel rightLabel = new GWTLabel(I18nTranslator.getInstance().rights_list());
        rightPanel.add(rightLabel);

        rightTree = new GWTPagedTree();
        rightPanel.add(rightTree);
        userPanel.add(rightPanel);

        //Rights widgets
        editableRightPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");

        GWTVerticalPanel containerPanel = new GWTVerticalPanel();

        GWTGrid newEntryPanel = new GWTGrid();

        GWTLabel firstComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().role());
        editableRoleRightComboBox = new GWTPagedFormDropDownListBox<Role>();

        GWTLabel secondComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().domain());
        editableDomainRightComboBox = new GWTPagedFormDropDownListBox<String>();

        addNewRightButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newEntryPanel.addRow(
                firstComboBoxCaption,
                secondComboBoxCaption);
        newEntryPanel.addRow(
                editableRoleRightComboBox,
                editableDomainRightComboBox,
                addNewRightButton);
        containerPanel.add(newEntryPanel);

        editableRightsListBox = new GWTPagedFormTable<RightForm, Widget>();
        editableRightsListBox.setHeader(firstComboBoxCaption.getCaption(), secondComboBoxCaption.getCaption());
        containerPanel.add(editableRightsListBox);
        editableRightPanel.add(containerPanel);

        userPanel.add(editableRightPanel);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedComboBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        /** add a sensible default value **/
        emailNotificationCheckBox = new GWTCheckBox(true);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().email_notification() + ":"), emailNotificationCheckBox);
        miscOptionsPanel.add(miscOptionsGrid);

        userPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        userPanel.add(submit);

        return userPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests
    public void togglePasswordChange() {
        if (changePasswordCheckBox.isChecked()) {
            oldPasswordTextBox.setEnabled(true);
            newPasswordTextBox.setEnabled(true);
            confirmNewPasswordTextBox.setEnabled(true);
        } else {
            oldPasswordTextBox.setEnabled(false);
            newPasswordTextBox.setEnabled(false);
            confirmNewPasswordTextBox.setEnabled(false);
        }
    }

    public void refreshTimezoneList(Collection<String> timezones) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        for (String timezone : timezones) {
            timezoneComboBox.addItem(timezone, timezone);
        }

    }

    public void refreshEditableRoleRightList(Collection<Role> roles) {
        //Clear the list beforehand
        editableRoleRightComboBox.clean();

        //Generate the list
        for (Role role : roles)
            editableRoleRightComboBox.addItem(role.getI18nFriendlyString(), role);
    }

    public void refreshEditableDomainRightList(Collection<String> domains) {
        //Clear the list beforehand
        editableDomainRightComboBox.clean();

        //Generate the list
        for (String domainEntry : domains) {
            editableDomainRightComboBox.addItem(domainEntry,domainEntry);
         }
    }

    public void addNewRight(RightForm right, ClickHandler deleteClickHandler) {

        //Create the right entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new right to the list
        editableRightsListBox.addRow(right, false, new GWTLabel(right.getRole().getI18nFriendlyString()), new GWTLabel(right.getDomainId()), operationPanel);
    }

    public void showRightList(boolean isVisible) {
        rightPanel.setVisible(isVisible);
    }

    public void refreshReadOnlyRightList(ProfileRightForm rightForms, final SerializableItemSelectionHandler<String> programmeDisplaySelectionHandler, SerializableItemSelectionHandler<String> channelDisplaySelectionHandler) {
        //Read-only list
        rightTree.clean();
        if (rightForms != null) {
            addNewRightItems(ProgrammeRole.animator, rightForms.getAnimatorRights(), programmeDisplaySelectionHandler);
            addNewRightItems(ProgrammeRole.curator, rightForms.getCuratorRights(), programmeDisplaySelectionHandler);
            addNewRightItems(ProgrammeRole.contributor, rightForms.getContributorRights(), programmeDisplaySelectionHandler);
            addNewRightItems(ChannelRole.listener, rightForms.getListenerRights(), channelDisplaySelectionHandler);
            addNewRightItems(ChannelRole.programmeManager, rightForms.getProgrammeManagerRights(), channelDisplaySelectionHandler);
            addNewRightItems(ChannelRole.channelAdministrator, rightForms.getChannelAdministratorRights(), channelDisplaySelectionHandler);
        }
    }

    public void refreshEditableRightList(ActionCollection<RightForm> rightForms, ClickHandler deleteClickHandler) {
        //Editable list
        editableRightsListBox.clean();
        //generate the list
        for (ActionCollectionEntry<RightForm> rightEntry : rightForms) {
            addNewRight(rightEntry.getKey(), deleteClickHandler);
         }
    }

    private void addNewRightItems(Role role, Collection<String> rights, final SerializableItemSelectionHandler<String> displaySelectionHandler) {
        for (final String s : rights) {
            Long newBranchId = rightTree.getItemByName(role.getI18nFriendlyString());
            if (newBranchId==null)
                newBranchId = rightTree.addItemWithName(role.getI18nFriendlyString(), new GWTLabel(role.getI18nFriendlyString()));
            ClickHandler domainClickHandler = new ClickHandler() {

                @Override
                public void onClick(ClickEvent ce) {
                    displaySelectionHandler.onItemSelected(s);
                }
            };
            rightTree.addItem(new GWTClickableLabel(s, domainClickHandler), newBranchId);
        }
    }
}
