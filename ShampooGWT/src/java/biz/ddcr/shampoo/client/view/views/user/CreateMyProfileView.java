/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.user;

import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class CreateMyProfileView extends PageView {

    //This widget is only published in order to bind the hourglass
    private GWTVerticalPanel userPanel;
    //User widgets
    private GWTTextBox usernameTextBox;
    private GWTPasswordTextBox passwordTextBox;
    private GWTPasswordTextBox confirmPasswordTextBox;
    private GWTTextBox firstAndLastNameTextBox;
    private GWTTextBox emailTextBox;
    private GWTTextBox confirmEmailTextBox;
    private GWTPagedComboBox timezoneComboBox;
    private GWTCheckBox emailNotificationCheckBox;
    //Rights
    private GWTPagedFormDropDownListBox<Role> roleRightComboBox;
    private GWTPagedFormDropDownListBox<String> domainRightComboBox;
    private GWTClickableLabel addNewRightButton;
    private GWTPagedFormTable<RightForm, Widget> rightsListBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericPanelInterface getUserPanel() {
        return userPanel;
    }

    public GenericCheckBoxInterface getEmailNotificationCheckBox() {
        return emailNotificationCheckBox;
    }

    public GenericTextBoxInterface getPasswordTextBox() {
        return passwordTextBox;
    }

    public GenericTextBoxInterface getConfirmPasswordTextBox() {
        return confirmPasswordTextBox;
    }

    public GenericTextBoxInterface getUsernameTextBox() {
        return usernameTextBox;
    }

    public GenericTextBoxInterface getEmailTextBox() {
        return emailTextBox;
    }

    public GenericTextBoxInterface getConfirmEmailTextBox() {
        return confirmEmailTextBox;
    }

    public GenericTextBoxInterface getFirstAndLastNameTextBox() {
        return firstAndLastNameTextBox;
    }

    public GenericComboBoxInterface getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericHyperlinkInterface getAddNewRightButton() {
        return addNewRightButton;
    }

    public GenericDropDownListBoxInterface<String> getDomainRightComboBox() {
        return domainRightComboBox;
    }

    public GenericFormTableInterface<RightForm, Widget> getRightsListBox() {
        return rightsListBox;
    }

    public GenericDropDownListBoxInterface<Role> getRoleRightComboBox() {
        return roleRightComboBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().create_my_profile();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().user_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //User widgets
        userPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        usernameTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().username() + ":"), usernameTextBox);

        passwordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password() + ":"), passwordTextBox);

        confirmPasswordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password_bis() + ":"), confirmPasswordTextBox);

        firstAndLastNameTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().first_and_last_name() + ":"), firstAndLastNameTextBox);

        emailTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().email() + ":"), emailTextBox);

        confirmEmailTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().email_bis() + ":"), confirmEmailTextBox);

        userPanel.add(grid);

        //Rights widgets
        GWTVerticalPanel rightPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel rightLabel = new GWTLabel(I18nTranslator.getInstance().rights_list());
        rightPanel.add(rightLabel);

        GWTVerticalPanel containerPanel = new GWTVerticalPanel();

        GWTGrid newEntryPanel = new GWTGrid();

        GWTLabel firstComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().role());
        roleRightComboBox = new GWTPagedFormDropDownListBox<Role>();

        GWTLabel secondComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().domain());
        domainRightComboBox = new GWTPagedFormDropDownListBox<String>();

        addNewRightButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newEntryPanel.addRow(
                firstComboBoxCaption,
                secondComboBoxCaption);
        newEntryPanel.addRow(
                roleRightComboBox,
                domainRightComboBox,
                addNewRightButton);
        containerPanel.add(newEntryPanel);

        rightsListBox = new GWTPagedFormTable<RightForm, Widget>();
        rightsListBox.setHeader(firstComboBoxCaption.getCaption(), secondComboBoxCaption.getCaption());
        containerPanel.add(rightsListBox);
        rightPanel.add(containerPanel);

        userPanel.add(rightPanel);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedComboBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        /** add a sensible default value **/
        emailNotificationCheckBox = new GWTCheckBox(true);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().email_notification() + ":"), emailNotificationCheckBox);
        miscOptionsPanel.add(miscOptionsGrid);

        userPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        userPanel.add(submit);

        return userPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests
    public void refreshRoleRightList(Collection<Role> roles) {
        //Clear the list beforehand
        roleRightComboBox.clean();

        //Generate the list
        for (Role role : roles)
            roleRightComboBox.addItem(role.getI18nFriendlyString(), role);
    }

    public void refreshDomainRightList(Collection<String> domains) {
        //Clear the list beforehand
        domainRightComboBox.clean();

        //Generate the list
        for (String domainEntry : domains) {
            domainRightComboBox.addItem(domainEntry,domainEntry);
         }
    }

    public void refreshTimezoneList(Collection<String> timezones) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        for (String timezone : timezones) {
            timezoneComboBox.addItem(timezone, timezone);
        }
        //Select a default value
        timezoneComboBox.setSelectedValue("UTC");

    }

    public void addNewRight(RightForm right, ClickHandler deleteClickHandler) {

        //Create the right entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new right to the list
        rightsListBox.addRow(right, false, new GWTLabel(right.getRole().getI18nFriendlyString()), new GWTLabel(right.getDomainId()), operationPanel);
    }

}
