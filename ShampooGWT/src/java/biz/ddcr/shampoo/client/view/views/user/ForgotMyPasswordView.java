/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.views.user;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTRadioButton;
import biz.ddcr.shampoo.client.view.widgets.GWTTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericRadioButtonInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTextBoxInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class ForgotMyPasswordView extends PageView {

    //This widget is only published in order to bind the hourglass
    private GWTVerticalPanel passwordPanel;
    //User widgets
    private GWTRadioButton usernameButton;
    private GWTRadioButton emailButton;
    private GWTTextBox textBox;
    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericPanelInterface getPasswordPanel() {
        return passwordPanel;
    }

    public GenericRadioButtonInterface getUsernameRadioButton() {
        return usernameButton;
    }

    public GenericRadioButtonInterface getEmailRadioButton() {
        return emailButton;
    }

    public GenericTextBoxInterface getTextBox() {
        return textBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().forgot_my_password();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().user_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //User widgets
        passwordPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        usernameButton = new GWTRadioButton("iDoRemember", true);
        usernameButton.setCaption(I18nTranslator.getInstance().i_remember_my_username());
        grid.addRow(usernameButton);

        emailButton = new GWTRadioButton("iDoRemember", false);
        emailButton.setCaption(I18nTranslator.getInstance().i_remember_my_email_address());
        grid.addRow(emailButton);

        textBox = new GWTTextBox();
        grid.addRow(textBox);

        passwordPanel.add(grid);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().ok());
        passwordPanel.add(submit);

        return passwordPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

}
