/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.user;

import biz.ddcr.shampoo.client.form.DomainForm;
import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.right.Role;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.*;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


/**
 *
 * @author okay_awright
 **/
public class RestrictedUserEditView extends PageView {

    //User widgets
    private GWTCheckBox enabledCheckBox;
    private GWTLabel usernameLabel;
    private GWTCheckBox changePasswordCheckBox;
    private GWTPasswordTextBox passwordTextBox;
    private GWTPasswordTextBox confirmPasswordTextBox;
    private GWTTextBox firstAndLastNameTextBox;
    private GWTTextBox emailTextBox;
    private GWTCheckBox emailNotificationCheckBox;
    private GWTPagedComboBox timezoneComboBox;
    //Rights widgets
    private GWTPagedFormDropDownListBox<Role> roleRightComboBox;
    private GWTPagedFormDropDownListBox<DomainForm> domainRightComboBox;
    private GWTClickableLabel addNewRightButton;
    private GWTPagedFormTable<RightForm, Widget> rightsListBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericCheckBoxInterface getEmailNotificationCheckBox() {
        return emailNotificationCheckBox;
    }

    public GenericCheckBoxInterface getChangePasswordCheckBox() {
        return changePasswordCheckBox;
    }

    public GenericTextBoxInterface getConfirmPasswordTextBox() {
        return confirmPasswordTextBox;
    }

    public GenericTextBoxInterface getEmailTextBox() {
        return emailTextBox;
    }

    public GenericCheckBoxInterface getEnabledCheckBox() {
        return enabledCheckBox;
    }

    public GenericTextBoxInterface getFirstAndLastNameTextBox() {
        return firstAndLastNameTextBox;
    }

    public GenericTextBoxInterface getPasswordTextBox() {
        return passwordTextBox;
    }

    public GenericLabelInterface getUsernameLabel() {
        return usernameLabel;
    }

    public GenericComboBoxInterface getTimezoneComboBox() {
        return timezoneComboBox;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericDropDownListBoxInterface<DomainForm> getDomainRightComboBox() {
        return domainRightComboBox;
    }

    public GenericFormTableInterface<RightForm, Widget> getRightsListBox() {
        return rightsListBox;
    }

    public GenericHyperlinkInterface getAddNewRightButton() {
        return addNewRightButton;
    }
    
    public GenericDropDownListBoxInterface<Role> getRoleRightComboBox() {
        return roleRightComboBox;
    }


    //Setup minimum view

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().edit_user();
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().user_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        //User widgets
        GWTVerticalPanel userPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        enabledCheckBox = new GWTCheckBox(false);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().enabled() + ":"), enabledCheckBox);

        usernameLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().username() + ":"), usernameLabel);

        changePasswordCheckBox = new GWTCheckBox(false);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().change_my_password() + ":"), changePasswordCheckBox);
        
        passwordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password() + ":"), passwordTextBox);

        confirmPasswordTextBox = new GWTPasswordTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().password_bis() + ":"), confirmPasswordTextBox);

        firstAndLastNameTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().first_and_last_name() + ":"), firstAndLastNameTextBox);

        emailTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().email() + ":"), emailTextBox);

        userPanel.add(grid);

        //Rights widgets
        GWTVerticalPanel rightPanel = new GWTCSSVerticalPanel("GWTHighlightVerticalPanel");
        GWTLabel rightLabel = new GWTLabel(I18nTranslator.getInstance().rights_list());
        rightPanel.add(rightLabel);

        GWTVerticalPanel containerPanel = new GWTVerticalPanel();

        GWTGrid newEntryPanel = new GWTGrid();

        GWTLabel firstComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().role());
        roleRightComboBox = new GWTPagedFormDropDownListBox<Role>();

        GWTLabel secondComboBoxCaption = new GWTLabel(I18nTranslator.getInstance().domain());
        domainRightComboBox = new GWTPagedFormDropDownListBox<DomainForm>();

        addNewRightButton = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());

        newEntryPanel.addRow(
                firstComboBoxCaption,
                secondComboBoxCaption);
        newEntryPanel.addRow(
                roleRightComboBox,
                domainRightComboBox,
                addNewRightButton);
        containerPanel.add(newEntryPanel);

        rightsListBox = new GWTPagedFormTable<RightForm, Widget>();
        rightsListBox.setHeader(firstComboBoxCaption.getCaption(), secondComboBoxCaption.getCaption());
        containerPanel.add(rightsListBox);
        rightPanel.add(containerPanel);

        userPanel.add(rightPanel);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();
        timezoneComboBox = new GWTPagedComboBox();
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox);
        /** add a sensible default value **/
        emailNotificationCheckBox = new GWTCheckBox(true);
        miscOptionsGrid.addRow(new GWTLabel(I18nTranslator.getInstance().email_notification() + ":"), emailNotificationCheckBox);
        miscOptionsPanel.add(miscOptionsGrid);

        userPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().edit());
        userPanel.add(submit);

        return userPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests

    public void togglePasswordChange() {
        if (changePasswordCheckBox.isChecked()) {
            passwordTextBox.setEnabled(true);
            confirmPasswordTextBox.setEnabled(true);
        } else {
            passwordTextBox.setEnabled(false);
            confirmPasswordTextBox.setEnabled(false);
        }
    }

    public void refreshTimezoneList(Collection<String> timezones) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        for (String timezone : timezones)
            timezoneComboBox.addItem(timezone,timezone);

    }

    public void refreshRoleRightList(Collection<Role> roles) {
        //Clear the list beforehand
        roleRightComboBox.clean();

        //Generate the list
        for (Role role : roles)
            roleRightComboBox.addItem(role.getI18nFriendlyString(), role);
    }

    public void refreshDomainRightList(ActionCollection<? extends DomainForm> domains) {
        //Clear the list beforehand
        domainRightComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<? extends DomainForm> domainEntry : domains) {
            if (domainEntry.isEditable()) {
                domainRightComboBox.addItem(domainEntry.getKey().getLabel(),domainEntry.getKey());
            }
         }
    }

    public void refreshExistingRights(ActionCollection<RightForm> rightForms, ClickHandler deleteClickHandler, ClickHandler displayClickHandler) {
        //clear the list beforehand
        rightsListBox.clean();

        //generate the list
        for (ActionCollectionEntry<RightForm> rightEntry : rightForms) {
            if (rightEntry.isEditable()) {
                addNewRight(rightEntry.getKey(), deleteClickHandler, displayClickHandler);
            }
         }
    }

    public void addNewRight(RightForm rightForm, ClickHandler deleteClickHandler, ClickHandler displayClickHandler) {

        //Create the right entry
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTClickableLabel deleteLink = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
        deleteLink.addClickHandler(deleteClickHandler);
        operationPanel.add(deleteLink);

        //Add the new right to the list
        rightsListBox.addRow(rightForm, false, new GWTLabel(rightForm.getRole().getI18nFriendlyString()), new GWTClickableLabel(rightForm.getDomainId(), displayClickHandler), operationPanel);
    }

}
