/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.user;

import biz.ddcr.shampoo.client.form.right.RightForm;
import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.ColumnHeader;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelAdd;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelEdit;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmation;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmationDelete;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFormSortableTable;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedTree;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.SerializableItemSelectionHandler;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class UserListView extends PageView {

    GWTVerticalPanel userPanel;
    GWTFormSortableTable<UserForm, Widget, USER_TAGS> userList;
    GWTClickableLabel addUserLink;
    GWTClickableLabel addAdminLink;

    public GenericFormSortableTableInterface<UserForm, Widget, USER_TAGS> getUserList() {
        return userList;
    }

    public GenericHyperlinkInterface getAddAdminLink() {
        return addAdminLink;
    }

    public GenericHyperlinkInterface getAddUserLink() {
        return addUserLink;
    }

    public GenericPanelInterface getUserPanel() {
        return userPanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().user_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "user";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        userPanel = new GWTVerticalPanel();

        return userPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests
    public void refreshUserPanel(SortableTableClickHandler listHeaderClickHandler) {
        userList = new GWTFormSortableTable<UserForm, Widget, USER_TAGS>();
        //Generate list header
        userList.setHeader(
                new ColumnHeader<USER_TAGS>(I18nTranslator.getInstance().username(), USER_TAGS.username),
                new ColumnHeader<USER_TAGS>(I18nTranslator.getInstance().enabled(), USER_TAGS.enabled),
                new ColumnHeader<USER_TAGS>(I18nTranslator.getInstance().account(), USER_TAGS.type),
                new ColumnHeader<USER_TAGS>(I18nTranslator.getInstance().first_and_last_name(), USER_TAGS.firstAndLastName),
                new ColumnHeader<USER_TAGS>(I18nTranslator.getInstance().rights(), USER_TAGS.rights),
                new ColumnHeader<USER_TAGS>(I18nTranslator.getInstance().operation())
                );
        userList.setRefreshClickHandler(listHeaderClickHandler);

        userPanel.add(userList);
    }

    public void refreshAddUserLink(ClickHandler clickHandler) {
        //Add a regular user
        addUserLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_user());
        if (clickHandler != null) {
            addUserLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addUserLink);
    }

    public void refreshAddAdminLink(ClickHandler clickHandler) {
        //Add an administrator
        addAdminLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_admin());
        if (clickHandler != null) {
            addAdminLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addAdminLink);
    }

    public void refreshUserList(ActionCollection<? extends UserForm> users, ClickHandler editClickHandler, YesNoClickHandler deleteClickHandler, final YesNoClickHandler deleteAllClickHandler, ClickHandler userDisplayClickHandler, final SerializableItemSelectionHandler<RightForm> domainDisplaySelectionHandler) {

        //Clear the list beforehand
        userList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        userList.setNextButtonVisible((users.size() >= userList.getMaxVisibleRows()));

        userList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<? extends UserForm> userEntry : users) {

            if (userEntry.isReadable()) {
                UserForm user = userEntry.getKey();

                GWTPagedTree rightWidget = null;
                if (user instanceof RestrictedUserForm) {
                    rightWidget = new GWTPagedTree();
                    for (ActionCollectionEntry<RightForm> rightEntry : ((RestrictedUserForm)user).getRightForms()) {
                        if (rightEntry.isReadable()) {
                            final RightForm right = rightEntry.getItem();
                            Long newBranchId = rightWidget.getItemByName(right.getRole().getI18nFriendlyString());
                            if (newBranchId==null)
                                newBranchId = rightWidget.addItemWithName(right.getRole().getI18nFriendlyString(), new GWTClickableLabel(right.getRole().getI18nFriendlyString()));
                            ClickHandler domainClickHandler = new ClickHandler() {
                                @Override
                                public void onClick(ClickEvent ce) {
                                    domainDisplaySelectionHandler.onItemSelected(right);
                                }
                            };
                            rightWidget.addItem(new GWTClickableLabel(right.getDomainId(), domainClickHandler), newBranchId);
                        }
                    }
                }

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                //Edit
                if (userEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                }
                //Delete
                if (userEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(user.getUsername()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                
                userList.addRow(user,
                        userEntry.isDeletable(),
                        new GWTClickableLabel(user.getUsername(), userDisplayClickHandler),
                        new GWTLabel(user.isEnabled() ? I18nTranslator.getInstance().is_true() : I18nTranslator.getInstance().is_false()),
                        new GWTLabel((user instanceof AdministratorForm) ? I18nTranslator.getInstance().admin() : I18nTranslator.getInstance().restricted_user()),
                        new GWTLabel(user.getFirstAndLastName()),
                        rightWidget,
                        operationPanel);
            }
        }

    }

}
