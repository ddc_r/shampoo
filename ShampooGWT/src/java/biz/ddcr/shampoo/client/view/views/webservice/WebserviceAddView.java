/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.webservice;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.GWTButton;
import biz.ddcr.shampoo.client.view.widgets.GWTCheckBox;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelCancel;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTPagedFormDropDownListBox;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTNumericBox;
import biz.ddcr.shampoo.client.view.widgets.GWTTextBox;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericCheckBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericDropDownListBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericNumericBoxInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericTextBoxInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class WebserviceAddView extends PageView {

    //Main widgets
    private GWTTextBox apiKeyTextBox;
    private GWTTextBox privateKeyTextBox;
    //Channels widget
    private GWTPagedFormDropDownListBox<String> channelComboBox;
    //Option widgets
    private GWTCheckBox maxFireRateCheckBox;
    private GWTNumericBox maxFireRateNumericBox;
    private GWTCheckBox maxDailyQuotaCheckBox;
    private GWTNumericBox maxDailyQuotaNumericBox;
    private GWTTextBox whitelistRegexpTextBox;
    //Misc. options
    private GWTCheckBox moduleNowPlayingCheckBox;
    private GWTCheckBox moduleComingNextCheckBox;
    private GWTCheckBox moduleArchiveCheckBox;
    private GWTCheckBox moduleTimetableCheckBox;
    private GWTCheckBox moduleVoteCheckBox;
    private GWTCheckBox moduleRequestCheckBox;

    private GWTButton submit;
    private GWTClickableLabel cancel;

    public GenericTextBoxInterface getApiKeyTextBox() {
        return apiKeyTextBox;
    }

    public GenericDropDownListBoxInterface<String> getChannelComboBox() {
        return channelComboBox;
    }

    public GenericTextBoxInterface getWhitelistRegexpTextBox() {
        return whitelistRegexpTextBox;
    }

    public GenericCheckBoxInterface getMaxDailyQuotaCheckBox() {
        return maxDailyQuotaCheckBox;
    }

    public GenericNumericBoxInterface getMaxDailyQuotaNumericBox() {
        return maxDailyQuotaNumericBox;
    }

    public GenericCheckBoxInterface getMaxFireRateCheckBox() {
        return maxFireRateCheckBox;
    }

    public GenericNumericBoxInterface getMaxFireRateNumericBox() {
        return maxFireRateNumericBox;
    }

    public GenericCheckBoxInterface getModuleArchiveCheckBox() {
        return moduleArchiveCheckBox;
    }

    public GenericCheckBoxInterface getModuleComingNextCheckBox() {
        return moduleComingNextCheckBox;
    }

    public GenericCheckBoxInterface getModuleNowPlayingCheckBox() {
        return moduleNowPlayingCheckBox;
    }

    public GenericCheckBoxInterface getModuleRequestCheckBox() {
        return moduleRequestCheckBox;
    }

    public GenericCheckBoxInterface getModuleTimetableCheckBox() {
        return moduleTimetableCheckBox;
    }

    public GenericCheckBoxInterface getModuleVoteCheckBox() {
        return moduleVoteCheckBox;
    }

    public GenericTextBoxInterface getPrivateKeyTextBox() {
        return privateKeyTextBox;
    }

    public GenericHyperlinkInterface getCancel() {
        return cancel;
    }

    public GenericHyperlinkInterface getSubmit() {
        return submit;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().add_webservice();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "webservice";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().webservice_heading();
    }

    @Override
    public GWTVerticalPanel buildPageContent() {

        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        GWTGrid grid = new GWTGrid();

        apiKeyTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().public_key() + ":"), apiKeyTextBox);

        privateKeyTextBox = new GWTTextBox();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().private_key() + ":"), privateKeyTextBox);

        channelComboBox = new GWTPagedFormDropDownListBox<String>();
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelComboBox);

        mainPanel.add(grid);

        //Modules widgets
        GWTVerticalPanel auxPanel = new GWTVerticalPanel();
        GWTLabel modulesLabel = new GWTLabel(I18nTranslator.getInstance().modules()+":");
        auxPanel.add(modulesLabel);

        GWTGrid modulesPanel = new GWTGrid();

        moduleNowPlayingCheckBox = new GWTCheckBox(true);//Fill in some sensible default value
        moduleNowPlayingCheckBox.setCaption(I18nTranslator.getInstance().now_playing_module());
        modulesPanel.addRow(moduleNowPlayingCheckBox);
        moduleComingNextCheckBox = new GWTCheckBox(true);//Fill in some sensible default value
        moduleComingNextCheckBox.setCaption(I18nTranslator.getInstance().coming_next_module());
        modulesPanel.addRow(moduleComingNextCheckBox);
        moduleArchiveCheckBox = new GWTCheckBox(true);//Fill in some sensible default value
        moduleArchiveCheckBox.setCaption(I18nTranslator.getInstance().archive_module());
        modulesPanel.addRow(moduleArchiveCheckBox);
        moduleTimetableCheckBox = new GWTCheckBox(false);//Fill in some sensible default value
        moduleTimetableCheckBox.setCaption(I18nTranslator.getInstance().timetable_module());
        //Not yet implemented
        moduleTimetableCheckBox.setEnabled(false);
        modulesPanel.addRow(moduleTimetableCheckBox);
        moduleVoteCheckBox = new GWTCheckBox(false);//Fill in some sensible default value
        moduleVoteCheckBox.setCaption(I18nTranslator.getInstance().vote_module());
        //Not yet implemented
        moduleVoteCheckBox.setEnabled(false);
        modulesPanel.addRow(moduleVoteCheckBox);
        moduleRequestCheckBox = new GWTCheckBox(false);//Fill in some sensible default value        
        moduleRequestCheckBox.setCaption(I18nTranslator.getInstance().request_module());
        //Not yet implemented
        moduleRequestCheckBox.setEnabled(false);
        modulesPanel.addRow(moduleRequestCheckBox);

        auxPanel.add(modulesPanel);
        mainPanel.add(auxPanel);

        //Advanced options
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        GWTGrid miscOptionsGrid = new GWTGrid();

        GWTGrid option1Panel = new GWTGrid();
        maxFireRateCheckBox = new GWTCheckBox(false); //Fill in some sensible values
        maxFireRateCheckBox.setCaption(I18nTranslator.getInstance().maximum_fire_rate()+":");
        maxFireRateNumericBox = new GWTNumericBox(100, 10, 1000);
        option1Panel.addRow(maxFireRateCheckBox, maxFireRateNumericBox, new GWTLabel(I18nTranslator.getInstance().milliseconds()));
        miscOptionsGrid.addRow(option1Panel);

        GWTGrid option2Panel = new GWTGrid();
        maxDailyQuotaCheckBox = new GWTCheckBox(true); //Fill in some sensible values
        maxDailyQuotaCheckBox.setCaption(I18nTranslator.getInstance().maximum_daily_quota()+":");
        maxDailyQuotaNumericBox = new GWTNumericBox(10000, 1, 100000);
        option2Panel.addRow(maxDailyQuotaCheckBox, maxDailyQuotaNumericBox, new GWTLabel(I18nTranslator.getInstance().hits()));
        miscOptionsGrid.addRow(option2Panel);

        GWTGrid option3Panel = new GWTGrid();
        GWTLabel whitelistRegexpLabel = new GWTLabel(I18nTranslator.getInstance().ip_whitelist_regexp()+":");
        whitelistRegexpTextBox = new GWTTextBox();
        option3Panel.addRow(whitelistRegexpLabel, whitelistRegexpTextBox);
        miscOptionsGrid.addRow(option3Panel);

        miscOptionsPanel.add(miscOptionsGrid);

        mainPanel.add(miscOptionsPanel);

        //Form submission
        submit = new GWTButton(I18nTranslator.getInstance().add());
        mainPanel.add(submit);

        return mainPanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        Set<GenericHyperlinkInterface> links = new HashSet<GenericHyperlinkInterface>();

        //Add a cancel button
        cancel = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        links.add(cancel);

        return links;
    }

    //Respond to presenter requests

    public void refreshChannelList(ActionCollection<ChannelForm> channels) {
        //Clear the list beforehand
        channelComboBox.clean();

        //Generate the list
        for (ActionCollectionEntry<ChannelForm> channelEntry : channels) {
            if (channelEntry.isEditable()) {
                channelComboBox.addItem(channelEntry.getKey().getLabel(),channelEntry.getKey().getLabel());
            }
         }

    }

    public void toggleMaxFireRateChange() {
        if (maxFireRateCheckBox.isChecked()) {
            maxFireRateNumericBox.setEnabled(true);
        } else {
            maxFireRateNumericBox.setEnabled(false);
        }
    }
    public void toggleMaxDailyQuotaChange() {
        if (maxDailyQuotaCheckBox.isChecked()) {
            maxDailyQuotaNumericBox.setEnabled(true);
        } else {
            maxDailyQuotaNumericBox.setEnabled(false);
        }
    }


}
