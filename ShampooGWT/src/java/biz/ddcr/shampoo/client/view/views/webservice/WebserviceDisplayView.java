/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.webservice;

import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.GenericFormPageView;
import biz.ddcr.shampoo.client.view.widgets.GWTDisclosurePanel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericGridInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericLabelInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import com.google.gwt.i18n.client.NumberFormat;

/**
 *
 * @author okay_awright
 **/
public class WebserviceDisplayView extends GenericFormPageView {

    private GWTLabel apiKeyLabel;
    private GWTLabel privateKeyLabel;
    private GWTLabel channelLabel;
    //Streamer metadata
    private GWTDisclosurePanel metadataPanel;
    //Modules widgets
    private GWTGrid modulesPanel;
    //Options widgets
    private GWTLabel maxFireRateLabel;
    private GWTLabel maxDailyQuotaLabel;
    private GWTLabel whitelistRegexpLabel;

    public GenericLabelInterface getApiKeyLabel() {
        return apiKeyLabel;
    }

    public GenericLabelInterface getWhitelistRegexpLabel() {
        return apiKeyLabel;
    }

    public GenericLabelInterface getPrivateKeyLabel() {
        return privateKeyLabel;
    }

    public GenericLabelInterface getChannelLabel() {
        return channelLabel;
    }

    public GenericPanelInterface getMetadataPanel() {
        return metadataPanel;
    }
    
    public GenericGridInterface getModulesPanel() {
        return modulesPanel;
    }

    public GenericLabelInterface getMaxFireRateLabel() {
        return maxFireRateLabel;
    }

    public GenericLabelInterface getMaxDailyQuotaLabel() {
        return maxDailyQuotaLabel;
    }

    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().view_webservice();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "webservice";
    }

    @Override
    public String getTitleText() {
        return I18nTranslator.getInstance().webservice_heading();
    }

    @Override
    public GWTVerticalPanel getFormSpecificPageContent() {

        //Programme widgets
        GWTVerticalPanel mainPanel = new GWTVerticalPanel();

        //Webservice info
        metadataPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().technical_info());
        mainPanel.add(metadataPanel);
        
        GWTGrid grid = new GWTGrid();

        apiKeyLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().public_key() + ":"), apiKeyLabel);

        privateKeyLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().private_key() + ":"), privateKeyLabel);

        channelLabel = new GWTLabel(null);
        grid.addRow(new GWTLabel(I18nTranslator.getInstance().channel() + ":"), channelLabel);

        mainPanel.add(grid);

        GWTVerticalPanel auxPanel = new GWTVerticalPanel();
        GWTLabel modulesLabel = new GWTLabel(I18nTranslator.getInstance().modules()+":");
        auxPanel.add(modulesLabel);
        modulesPanel = new GWTGrid();
        auxPanel.add(modulesPanel);

        mainPanel.add(auxPanel);

        //misc. widgets
        GWTGrid grid2 = new GWTGrid();

        maxFireRateLabel = new GWTLabel(null);
        grid2.addRow(new GWTLabel(I18nTranslator.getInstance().maximum_fire_rate() + ":"), maxFireRateLabel);

        maxDailyQuotaLabel = new GWTLabel(null);
        grid2.addRow(new GWTLabel(I18nTranslator.getInstance().maximum_daily_quota() + ":"), maxDailyQuotaLabel);

        whitelistRegexpLabel = new GWTLabel(null);
        grid2.addRow(new GWTLabel(I18nTranslator.getInstance().ip_whitelist_regexp() + ":"), whitelistRegexpLabel);

        mainPanel.add(grid2);

        return mainPanel;
    }

    //Respond to presenter requests
    public void refreshModulesList(//
                boolean isArchiveModule,//
                boolean isComingNextModule,//
                boolean isNowPlayingModule,//
                boolean isRequestModule,//
                boolean isTimetableModule,//
                boolean isVoteModule) {
        modulesPanel.clean();
        if (isArchiveModule) modulesPanel.addRow(new GWTLabel(I18nTranslator.getInstance().archive_module()));
        if (isComingNextModule) modulesPanel.addRow(new GWTLabel(I18nTranslator.getInstance().coming_next_module()));
        if (isNowPlayingModule) modulesPanel.addRow(new GWTLabel(I18nTranslator.getInstance().now_playing_module()));
        if (isRequestModule) modulesPanel.addRow(new GWTLabel(I18nTranslator.getInstance().request_module()));
        if (isTimetableModule) modulesPanel.addRow(new GWTLabel(I18nTranslator.getInstance().timetable_module()));
        if (isVoteModule) modulesPanel.addRow(new GWTLabel(I18nTranslator.getInstance().vote_module()));
    }

    public void refreshMaxDailyQuotaLabel(Long maxDailyQuota) {
        maxDailyQuotaLabel.clean();
        if (maxDailyQuota==null)
            maxDailyQuotaLabel.setCaption(I18nTranslator.getInstance().infinite());
        else
            maxDailyQuotaLabel.setCaption(Long.toString(maxDailyQuota)+" "+I18nTranslator.getInstance().hits());
    }
    public void refreshMaxFireRateLabel(Long maxFireRate) {
        maxFireRateLabel.clean();
        if (maxFireRate==null)
            maxFireRateLabel.setCaption(I18nTranslator.getInstance().infinite());
        else
            maxFireRateLabel.setCaption(NumberFormat.getDecimalFormat().format(1000.0/maxFireRate)+" "+I18nTranslator.getInstance().hits_per_second());
    }
    public void refreshWhitelistRegexpLabel(String whitelistRegexp) {
        whitelistRegexpLabel.clean();
        if (whitelistRegexp==null || whitelistRegexp.length()==0)
            whitelistRegexpLabel.setCaption(I18nTranslator.getInstance().none());
        else
            whitelistRegexpLabel.setCaption(whitelistRegexp);
    }
    
    public void refreshMetadata(ActionCollectionEntry<? extends WebserviceModule> webserviceEntry) {
        //The eventBus will be bound using specificFormBind()
        final WebserviceModule container = webserviceEntry != null ? webserviceEntry.getItem() : null;
        metadataPanel.setVisible(container != null && webserviceEntry.isReadable());
        metadataPanel.clean();
        if (container != null && webserviceEntry.isReadable()) {
            metadataPanel.add(new GWTLabel(container.getI18nFriendlyName()));
        }
    }
}
