/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.views.webservice;

import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.views.PageView;
import biz.ddcr.shampoo.client.view.widgets.ColumnHeader;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelAdd;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelEdit;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmation;
import biz.ddcr.shampoo.client.view.widgets.GWTClickableLabelWithYesNoConfirmationDelete;
import biz.ddcr.shampoo.client.view.widgets.GWTFlowPanel;
import biz.ddcr.shampoo.client.view.widgets.GWTFormSortableTable;
import biz.ddcr.shampoo.client.view.widgets.GWTLabel;
import biz.ddcr.shampoo.client.view.widgets.GWTGrid;
import biz.ddcr.shampoo.client.view.widgets.GWTVerticalPanel;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericHyperlinkInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericPanelInterface;
import biz.ddcr.shampoo.client.view.widgets.YesNoClickHandler;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class WebserviceListView extends PageView {

    GWTVerticalPanel webservicePanel;
    GWTClickableLabel addWebserviceLink;
    GWTFormSortableTable<WebserviceForm, Widget, WEBSERVICE_TAGS> webserviceList;

    public GenericFormSortableTableInterface<WebserviceForm, Widget, WEBSERVICE_TAGS> getWebserviceList() {
        return webserviceList;
    }

    public GenericHyperlinkInterface getAddWebserviceLink() {
        return addWebserviceLink;
    }

    public GenericPanelInterface getWebservicePanel() {
        return webservicePanel;
    }

    //Setup minimum view
    @Override
    public String getHeaderText() {
        return I18nTranslator.getInstance().webservice_heading();
    }
    @Override
    public String getHeaderCSSMarker() {
        return "webservice";
    }

    @Override
    public GWTVerticalPanel buildPageContent() {
        webservicePanel = new GWTVerticalPanel();

        return webservicePanel;
    }

    @Override
    public Collection<GenericHyperlinkInterface> getNavigationLinks() {
        //Do nothing, start with no links;
        return null;
    }

    //Respond to presenter requests

    public void refreshWebservicePanel(SortableTableClickHandler listHeaderClickHandler) {
        webserviceList = new GWTFormSortableTable<WebserviceForm, Widget, WEBSERVICE_TAGS>();
        //Generate list header
        webserviceList.setHeader(
                new ColumnHeader<WEBSERVICE_TAGS>(I18nTranslator.getInstance().public_key(), WEBSERVICE_TAGS.apiKey),
                new ColumnHeader<WEBSERVICE_TAGS>(I18nTranslator.getInstance().channel(), WEBSERVICE_TAGS.channel),
                new ColumnHeader<WEBSERVICE_TAGS>(I18nTranslator.getInstance().modules()),
                new ColumnHeader<WEBSERVICE_TAGS>(I18nTranslator.getInstance().maximum_fire_rate(), WEBSERVICE_TAGS.maxFireRate),
                new ColumnHeader<WEBSERVICE_TAGS>(I18nTranslator.getInstance().maximum_daily_quota(), WEBSERVICE_TAGS.maxDailyQuota),
                new ColumnHeader<WEBSERVICE_TAGS>(I18nTranslator.getInstance().operation())
                );
        webserviceList.setRefreshClickHandler(listHeaderClickHandler);

        webservicePanel.add(webserviceList);
    }

    public void refreshAddWebserviceLink(ClickHandler clickHandler) {
        //Add a regular user
        addWebserviceLink = new GWTClickableLabelAdd(I18nTranslator.getInstance().add_webservice());
        if (clickHandler != null) {
            addWebserviceLink.addClickHandler(clickHandler);
        }
        addNavigationLink(addWebserviceLink);
    }

    public void refreshWebserviceList(ActionCollection<WebserviceForm> webservices, ClickHandler editClickHandler, YesNoClickHandler deleteClickHandler, final YesNoClickHandler deleteAllClickHandler, ClickHandler webserviceDisplayClickHandler, ClickHandler channelDisplayClickHandler) {

        //Clear the list beforehand
        webserviceList.clean();

        //Decide if we need to show the "Next" button
        //This is only a gross approximate: the condition specified below does not mean that there is +1 row after
        webserviceList.setNextButtonVisible((webservices.size()>=webserviceList.getMaxVisibleRows()));

        webserviceList.setCheckedItemHeaderRefreshHandler(new CheckedItemHeaderRefreshHandler() {

            @Override
            public void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer) {
                GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_selected_items());
                if (deleteAllClickHandler != null) {
                    deleteLink.addClickHandler(deleteAllClickHandler);
                }
                captionContainer.add(deleteLink);
            }
        });

        for (ActionCollectionEntry<WebserviceForm> webserviceEntry : webservices) {

            if (webserviceEntry.isReadable()) {
                WebserviceForm webservice = webserviceEntry.getKey();

                GWTClickableLabel channelWidget = new GWTClickableLabel(null);
                channelWidget.setCaption(webservice.getChannel());
                if (webservice.getChannel()!=null) {
                    channelWidget.addClickHandler(channelDisplayClickHandler);
                }

                GWTGrid<GWTLabel> modulesWidget = new GWTGrid<GWTLabel>();
                if (webservice.isArchiveModule()) modulesWidget.addRow(new GWTLabel(I18nTranslator.getInstance().archive_module()));
                if (webservice.isComingNextModule()) modulesWidget.addRow(new GWTLabel(I18nTranslator.getInstance().coming_next_module()));
                if (webservice.isNowPlayingModule()) modulesWidget.addRow(new GWTLabel(I18nTranslator.getInstance().now_playing_module()));
                if (webservice.isRequestModule()) modulesWidget.addRow(new GWTLabel(I18nTranslator.getInstance().request_module()));
                if (webservice.isTimetableModule()) modulesWidget.addRow(new GWTLabel(I18nTranslator.getInstance().timetable_module()));
                if (webservice.isVoteModule()) modulesWidget.addRow(new GWTLabel(I18nTranslator.getInstance().vote_module()));

                GWTFlowPanel operationPanel = new GWTFlowPanel();
                if (webserviceEntry.isDeletable()) {
                    GWTClickableLabelWithYesNoConfirmation deleteLink = new GWTClickableLabelWithYesNoConfirmationDelete(I18nTranslator.getInstance().delete());
                    deleteLink.setConfirmationMessage(I18nTranslator.getInstance().do_you_want_to_delete_x(webservice.getApiKey()));
                    if (deleteClickHandler != null) {
                        deleteLink.addClickHandler(deleteClickHandler);
                    }
                    operationPanel.add(deleteLink);
                }
                if (webserviceEntry.isEditable()) {
                    GWTClickableLabel editLink = new GWTClickableLabelEdit(I18nTranslator.getInstance().edit());
                    if (editClickHandler != null) {
                        editLink.addClickHandler(editClickHandler);
                    }
                    operationPanel.add(editLink);
                }

                webserviceList.addRow(webservice,
                        webserviceEntry.isDeletable(),
                        new GWTClickableLabel(webservice.getApiKey(), webserviceDisplayClickHandler),
                        channelWidget,
                        modulesWidget,
                        new GWTLabel(//
                            webservice.getMaxFireRate()!=null ?//
                                NumberFormat.getDecimalFormat().format(1000.0/webservice.getMaxFireRate())+" "+I18nTranslator.getInstance().hits_per_second()//
                                : I18nTranslator.getInstance().infinite()),
                        new GWTLabel(//
                            webservice.getMaxDailyQuota()!=null ?//
                                Long.toString(webservice.getMaxDailyQuota())+" "+I18nTranslator.getInstance().hits()//
                                : I18nTranslator.getInstance().infinite()),
                        operationPanel);
            }
        }

    }
}
