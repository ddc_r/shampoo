/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableHeaderEntryInterface;

/**
 *
 * @author okay_awright
 **/
public class ColumnHeader<F> implements SortableHeaderEntryInterface<F> {

    private String caption;
    private F flag;

    public ColumnHeader(String caption, F flag) {
        this.caption = caption;
        this.flag = flag;
    }

    public ColumnHeader(String caption) {
        this.caption = caption;
    }

    @Override
    public String getCaption() {
        return caption;
    }

    @Override
    public F getFlag() {
        return flag;
    }
}
