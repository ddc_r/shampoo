/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * Component version of the generic GWT handler that allows withdrawal or not of the event method when called, at runtime
 *
 * @author okay_awright
 **/
public abstract class ExtendedClickHandler implements ClickHandler, ExtendedHandlerInterface {

    final private ClickHandler embeddedHandler;

    public ExtendedClickHandler(ClickHandler embeddedHandler) {
        this.embeddedHandler = embeddedHandler;
    }

    @Override
    public abstract boolean isCallGranted();

    @Override
    public void onClick(ClickEvent event) {
        if (isCallGranted())
            embeddedHandler.onClick(event);
    }
}
