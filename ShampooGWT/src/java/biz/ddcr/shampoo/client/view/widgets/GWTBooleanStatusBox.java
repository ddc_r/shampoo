/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;

/**
 *
 * @author okay_awright
 **/
public class GWTBooleanStatusBox extends GWTExtendedWidget implements GenericStatusBoxInterface<String,Boolean> {

    private String id;
    final private GWTLabel statusLabel;

    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private GenericStatusChangeHandler<String,Boolean> delegatedStatusChange = null;

    public GWTBooleanStatusBox(String id, GenericStatusChangeHandler<String,Boolean> statusChangeHandler) {
        this(id);
        setStatusChangeHandler(statusChangeHandler);
    }
    public GWTBooleanStatusBox(String id, Boolean status) {
        this(id);
        setStatus(status);
    }
    public GWTBooleanStatusBox(String id) {
        this();
        setID(id);
    }
    public GWTBooleanStatusBox() {
        super();
        statusLabel = new GWTLabel(null);
        //Custom CSS
        statusLabel.setStyleName("GWTStatusBox");

        attachMainWidget(statusLabel);
    }

    @Override
    public void setEnabled(boolean enabled) {
        //Do nothing
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public void setStatus(Boolean status) {
        if (status==null) {
            clean();
        } else {
            final String statusText = status?I18nTranslator.getInstance().on():I18nTranslator.getInstance().off();
            statusLabel.setStyleName("GWTStatusBox-"+(status?"on":"off"));
            statusLabel.setCaption(statusText);
        }
    }

    @Override
    public void clean() {
        super.clean();
        statusLabel.setStyleName("GWTStatusBox");
        statusLabel.setCaption(null);
    }

    @Override
    public void setStatusChangeHandler(GenericStatusChangeHandler<String,Boolean> handler) {
        delegatedStatusChange = handler;
        //refresh the status ASAP
        refreshStatus();
    }

    @Override
    public void refreshStatus() {
        if (delegatedStatusChange!=null)
                delegatedStatusChange.onGet(this);
    }

}
