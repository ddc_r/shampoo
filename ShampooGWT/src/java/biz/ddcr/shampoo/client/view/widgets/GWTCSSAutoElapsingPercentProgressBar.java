/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.Timer;
import java.util.Date;

/**
 *
 * A progress bar that will progress by itself frequently (in ms)
 *
 * @author okay_awright
 *
 */
public class GWTCSSAutoElapsingPercentProgressBar extends GWTExtendedWidget implements GenericProgressBarInterface, GenericCSSInterface {

    final private IncubatorProgressBar widget;
    private String currentCSSSecondaryStyle = null;

    public GWTCSSAutoElapsingPercentProgressBar(long initialOffset, long maxOffset, int updateFrequency) {
        this(false, initialOffset, maxOffset, updateFrequency, null);
    }
    public GWTCSSAutoElapsingPercentProgressBar(boolean showText, long maxOffset, int updateFrequency) {
        this(showText, 0L,  maxOffset, updateFrequency, null);
    }
    public GWTCSSAutoElapsingPercentProgressBar(long maxOffset, int updateFrequency, String defaultCSSStyle) {
        this(false, 0L,  maxOffset, updateFrequency, defaultCSSStyle);
    }
    public GWTCSSAutoElapsingPercentProgressBar(boolean showText, long initialOffset, long maxOffset, int updateFrequency) {
        this(showText, initialOffset,  maxOffset, updateFrequency, null);
    }
    public GWTCSSAutoElapsingPercentProgressBar(long initialOffset, long maxOffset, int updateFrequency, String defaultCSSStyle) {
        this(false, initialOffset,  maxOffset, updateFrequency, defaultCSSStyle);
    }
    public GWTCSSAutoElapsingPercentProgressBar(boolean showText, long initialOffset, long maxOffset, int updateFrequency, String defaultCSSStyle) {
        super();
        //FIX
        widget = new IncubatorProgressBar(0.0, (double)maxOffset, (double)Math.min(maxOffset, Math.max(0, initialOffset))); //Min, max and inital value for percents
        widget.setPercentTextVisible(showText);
        //Custom CSS
        widget.setStyleName("GWTProgressBar");
        if (defaultCSSStyle!=null) widget.setStyleDependentName(defaultCSSStyle, true);
        start(updateFrequency);
        attachMainWidget(widget);
    }
   
    protected void start(final int updateFrequency) {
        //Reinit the progress bar
        Timer t = new Timer() {            
            private long previousTime = (new Date()).getTime();            
            @Override
            public void run() {
                //The JavaScript timer (on the same thread as all other GUI events) is not accurate enough, we need to figure how much time has actually elapsed since the last call
                long now = (new Date()).getTime();
                double newProgress = widget.getProgress() + (now - previousTime);
                previousTime = now;
                widget.setProgress(newProgress);
                if (widget.getProgress() >= widget.getMaxProgress()) {
                    cancel();
                }
            }
        };
        t.scheduleRepeating(updateFrequency);
    }

    @Override
    public void clean() {
        super.clean();
        widget.setProgress(0.0);
        unsetSecondaryCSSMarker();
    }

    @Override
    public void setProgress(long progress) {
        widget.setProgress((double)progress);
    }

    @Override
    public long getProgress() {
        return (long)widget.getProgress();
    }

    @Override
    public long getMaxProgress() {
        return (long)widget.getMaxProgress();
    }

    @Override
    public void setPercentTextVisible(boolean isVisible) {
        widget.setPercentTextVisible(isVisible);
    }

    @Override
    public boolean isPercentTextVisible() {
        return widget.isPercentTextVisible();
    }
    
    @Override
    public boolean setSecondaryCSSMarker(String cssName) {
        if (currentCSSSecondaryStyle==null) {
            widget.addStyleDependentName(cssName);
            currentCSSSecondaryStyle=cssName;
            return true;
        } else
            return false;
    }
    
    @Override
    public void unsetSecondaryCSSMarker() {
        if (currentCSSSecondaryStyle!=null) {
            widget.removeStyleDependentName(currentCSSSecondaryStyle);
            currentCSSSecondaryStyle = null;
        }
    }
    
    @Override
    public String getSecondaryCSSMarker() {
        return currentCSSSecondaryStyle;
    }
}
