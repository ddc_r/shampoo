/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * Composite widget made of a main widget and an accessory one
 * The accessory one is mainly used to display an hourglass when data is not yet ready or show notifications to user in relation to the main widget activity
 *
 * @author okay_awright
 **/
public class GWTCSSClickableContainer extends GWTExtendedWidget implements GenericClickableContainerWithImageInterface {

    final private FlexTable box;
    private boolean isEnabled = true;

    public GWTCSSClickableContainer(Widget widget) {
        super();
        box = new FlexTable();
        setBoxedObject(widget);
        attachMainWidget(box);
    }

    @Override
    public Widget getWidget() {
        return this;
    }

    public void setBoxedObject(Widget widget) {
        if (widget!=null) {
            box.setWidget(0, 0, widget);
        }
    }

    public void setImage(String url) {
        if (url!=null) {
            box.setWidget(0, 1, new GWTImage(url));
        }
    }

    public void unsetImage() {
        if (box.isCellPresent(0, 1)) box.removeCell(0, 1);
    }

    @Override
    public void clean() {
        super.clean();
        unsetImage();
    }

    @Override
    public void setCaption(String text) {
        setBoxedObject(new GWTLabel(text));
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return box.addClickHandler( new ExtendedClickHandler(handler) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
    }

    @Override
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

}
