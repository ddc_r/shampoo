/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * Single simple clickable CSS-stylable
 *
 * @author okay_awright
 **/
public class GWTCSSClickableLabel extends GWTClickableLabel implements GenericCSSInterface {

    private String currentCSSSecondaryStyle = null;

    public GWTCSSClickableLabel(String text) {
        super(text);
    }
    public GWTCSSClickableLabel(String text, String defaultCSSStyle) {
        this(text);
        widget.setStyleName(defaultCSSStyle);
    }
    public GWTCSSClickableLabel(String text, ClickHandler handler) {
        this(text);
        addClickHandler(handler);
    }
    public GWTCSSClickableLabel(String text, ClickHandler handler, String defaultCSSStyle) {
        this(text, defaultCSSStyle);
        addClickHandler(handler);
    }  

    @Override
    public boolean setSecondaryCSSMarker(String cssName) {
        if (currentCSSSecondaryStyle==null) {
            widget.addStyleDependentName(cssName);
            currentCSSSecondaryStyle=cssName;
            return true;
        } else
            return false;
    }
    
    @Override
    public void unsetSecondaryCSSMarker() {
        if (currentCSSSecondaryStyle!=null) {
            widget.removeStyleDependentName(currentCSSSecondaryStyle);
            currentCSSSecondaryStyle = null;
        }
    }
    
    @Override
    public String getSecondaryCSSMarker() {
        return currentCSSSecondaryStyle;
    }
    
    @Override
    public void clean() {
        super.clean();
        unsetSecondaryCSSMarker();
    }

}
