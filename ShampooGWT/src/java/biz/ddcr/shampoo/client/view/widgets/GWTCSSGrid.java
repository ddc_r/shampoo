/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class GWTCSSGrid<E extends Widget> extends GWTGrid<E> implements GenericCSSGridInterface<E> {

    public GWTCSSGrid(String defaultCSSStyle) {
        super();
        widget = new FlexTable();
        //Custom CSS
        widget.setStyleName(defaultCSSStyle);
        attachMainWidget(widget);
    }

    @Override
    public void insertRowAt(String CSSStyle, int beforeRow, E... widgets) {
        if (beforeRow >= 0 && beforeRow <= widget.getRowCount()) {
            int colIndex = 0;
            widget.insertRow(beforeRow);
            for (E localWidget : widgets) {
                this.widget.setWidget(beforeRow, colIndex++, localWidget);
            }
            //Custom CSS
            widget.getRowFormatter().setStyleName(beforeRow, CSSStyle);
        }
    }
    @Override
    public void insertRowAt(String CSSStyle, int beforeRow, E localWidget) {
        if (beforeRow >= 0 && beforeRow <= widget.getRowCount()) {
            int colIndex = 1;
            widget.insertRow(beforeRow);
            this.widget.setWidget(beforeRow, colIndex, localWidget);
            //Custom CSS
            widget.getRowFormatter().setStyleName(beforeRow, CSSStyle);
        }
    }

    @Override
    public void addRow(String CSSStyle, E... widgets) {
        int extraRow = widget.getRowCount();
        int colIndex = 0;
        for (E localWidget : widgets) {
            this.widget.setWidget(extraRow, colIndex++, localWidget);
        }
        //Custom CSS
        widget.getRowFormatter().setStyleName(extraRow, CSSStyle);
    }
    @Override
    public void addRow(String CSSStyle, E localWidget) {
        int extraRow = widget.getRowCount();
        int colIndex = 1;
        this.widget.setWidget(extraRow, colIndex, localWidget);
        //Custom CSS
        widget.getRowFormatter().setStyleName(extraRow, CSSStyle);
    }

    @Override
    public void setWidgetAt(String CSSStyle, int row, int column, E localWidget) {
        if (row < widget.getRowCount() && column < widget.getCellCount(row)) {
            //Make sure that if widget is null the cell will be properly left void
            //This method should be throw an Exception since it should only be used on pre-existing cells
            if (localWidget != null) {
                widget.setWidget(row, column, localWidget);
            } else {
                widget.clearCell(row, column);
            }
            //Custom CSS
            widget.getRowFormatter().setStyleName(row, CSSStyle);
        }
    }

}
