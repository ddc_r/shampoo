/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class GWTCSSTwoLevelsVerticalMenu extends GWTExtendedWidget implements GenericCSSMenuInterface {

    private Grid widget = null;

    public GWTCSSTwoLevelsVerticalMenu(int menuSize) {
        super();
        widget = new Grid();
        //Custom CSS
        widget.setStyleName("GWTMenu");
        widget.resizeColumns(2);
        resizeMenu(menuSize);
        attachMainWidget(widget);
    }

    public GWTCSSTwoLevelsVerticalMenu() {
        this(0);
    }

    @Override
    public void clean() {
        super.clean();
        widget.clear();
        resizeMenu(0);
    }

    @Override
    public void resizeMenu(int newSize) {
        if (newSize<widget.getRowCount())
            for (int i = widget.getRowCount()-1; i >= newSize; i--)
                widget.removeRow(i);
        else if (newSize>widget.getRowCount())
            for (int i = widget.getRowCount(); i < newSize; i++) {
                widget.insertRow(i);
                widget.setWidget(i, 0, null);
            }
    }

    @Override
    public boolean setMenuEntryAt(int index, String caption, String cssName) {
        return setMenuEntryAt(index, caption, null, 0, cssName);
    }
    
    @Override
    public boolean setMenuEntryAt(int index, String caption) {
        return setMenuEntryAt(index, caption, null, 0);
    }

    @Override
    public boolean setMenuEntryAt(int index, String caption, int subMenuSize, String cssName) {
        return setMenuEntryAt(index, caption, null, subMenuSize, cssName);
    }
    
    @Override
    public boolean setMenuEntryAt(int index, String caption, int subMenuSize) {
        return setMenuEntryAt(index, caption, null, subMenuSize);
    }

    @Override
    public boolean setMenuEntryAt(int index, String caption, ClickHandler clickHandler, String cssName) {
        return setMenuEntryAt(index, caption, clickHandler, 0, cssName);
    }
    
    @Override
    public boolean setMenuEntryAt(int index, String caption, ClickHandler clickHandler) {
        return setMenuEntryAt(index, caption, clickHandler, 0);
    }

    @Override
    public boolean setMenuEntryAt(int index, String caption, ClickHandler clickHandler, int subMenuSize) {
        return setMenuEntryAt(index, caption, clickHandler, 0, null);
    }
    
    @Override
    public boolean setMenuEntryAt(int index, String caption, ClickHandler clickHandler, int subMenuSize, String cssName) {
        if (caption != null && caption.length()!=0 && index > -1 && index < widget.getRowCount()) {

            VerticalPanel element = new VerticalPanel();
            if (cssName!=null)
                element.setStyleName(cssName);
            PluggableWidgetInterface item;
            if (clickHandler == null) {
                item = new GWTLabel(caption);
            } else {
                item = new GWTClickableLabel(caption);
                ((GWTClickableLabel) item).addClickHandler(clickHandler);
            }
            element.add(item.getWidget());
            Grid subElement = new Grid();
            for (int i = 0; i < subMenuSize; i++) {
                subElement.insertRow(0);
            }
            element.add(subElement);
            widget.setWidget(index, 0, element);

            return true;
        }
        return false;
    }

    @Override
    public boolean unsetMenuEntry(int index) {
        if (index > -1 && index < widget.getRowCount()) {
            Widget w = widget.getWidget(index, 0);
            if (w != null) {
                w = null;
                return true;
            }
        }
        return false;
    }

    protected boolean setSubElementWidgetAt(int menuIndex, int subMenuIndex, PluggableWidgetInterface widget) {
        if (menuIndex > -1 && menuIndex < this.widget.getRowCount()) {
            VerticalPanel element = (VerticalPanel) this.widget.getWidget(menuIndex, 0);
            if (element != null && element.getWidgetCount() == 2) {
                Grid subElement = (Grid) element.getWidget(1);
                if (subElement != null && subMenuIndex > -1 && subMenuIndex < subElement.getRowCount()) {
                    subElement.setWidget(subMenuIndex, 0, widget.getWidget());
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean unsetSubMenuEntry(int menuIndex, int subMenuIndex) {
        return setSubElementWidgetAt(menuIndex, subMenuIndex, null);
    }

    @Override
    public boolean setSubMenuEntryAt(int menuIndex, int subMenuIndex, String caption, ClickHandler clickHandler) {
        return setSubMenuEntryAt(menuIndex, subMenuIndex, caption, clickHandler, null);
    }
    
    @Override
    public boolean setSubMenuEntryAt(int menuIndex, int subMenuIndex, String caption, ClickHandler clickHandler, String cssName) {
        PluggableWidgetInterface item;
        if (clickHandler == null) {
            if (cssName==null)
                item = new GWTLabel(caption);
            else
                item = new GWTCSSLabel(caption, cssName);
        } else {
            if (cssName==null)
                item = new GWTClickableLabel(caption);
            else
                item = new GWTCSSClickableLabel(caption, cssName);
            ((GWTClickableLabel) item).addClickHandler(clickHandler);
        }
        return setSubElementWidgetAt(menuIndex, subMenuIndex, item);
    }
    
}
