/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.CheckBox;

/**
 *
 * @author okay_awright
 **/
public class GWTCheckBox extends GWTExtendedWidget implements GenericCheckBoxInterface {

    final private CheckBox widget;

    public GWTCheckBox(boolean checked) {
        super();
        widget = new CheckBox();
        //Custom CSS
        widget.setStyleName("GWTCheckBox");
        setChecked(checked);
        attachMainWidget(widget);
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public void setChecked(boolean checked) {
        widget.setValue(checked);
    }

    @Override
    public boolean isChecked() {
        return widget.getValue();
    }

    @Override
    public void setCaption(String text) {
        widget.setText(text);
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return widget.addClickHandler(handler);
    }

}
