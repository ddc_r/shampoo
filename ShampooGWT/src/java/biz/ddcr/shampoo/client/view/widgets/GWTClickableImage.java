/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;

/**
 *
 * @author okay_awright
 **/
public class GWTClickableImage extends GWTImage implements GenericHyperlinkInterface {

    private boolean isEnabled = true;

    public GWTClickableImage() {
        this((HasCoverArtInterface)null);
    }

    public GWTClickableImage(HasCoverArtInterface form) {
        super(form);
        //Custom CSS
        widget.setStyleName("GWTClickableImage");
    }

    public GWTClickableImage(String url) {
        super(url);
        //Custom CSS
        widget.setStyleName("GWTClickableImage");
    }
    
    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return widget.addClickHandler( new ExtendedClickHandler(handler) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
    }

    @Override
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    @Override
    public void setCaption(String text) {
        //Do nothing
    }

}
