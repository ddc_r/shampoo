/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Label;

/**
 *
 * @author okay_awright
 **/
public class GWTClickableLabelWithYesNoConfirmation extends GWTExtendedWidget implements GenericHyperlinkWithYesNoConfirmationInterface, GenericLabelInterface {

    final private GWTConfirmationDialogBox confirmationDialogBox;
    final protected Label widget;
    private boolean isEnabled = true;

    public GWTClickableLabelWithYesNoConfirmation(String text) {
        super();
        widget = new Label();
        //Custom CSS
        widget.setStyleName("GWTClickableLabel");
        setCaption(text);
        attachMainWidget(widget);

        confirmationDialogBox = new GWTConfirmationDialogBox();
    }

    public GWTClickableLabelWithYesNoConfirmation(String text, YesNoClickHandler handler) {
        this(text);
        addClickHandler(handler);
    }

    @Override
    public void setConfirmationMessage(String message) {
        confirmationDialogBox.setMessage(message);
    }

    @Override
    public void setCaption(String text) {
        widget.setText(text);
    }

    @Override
    public String getCaption() {
        return widget.getText();
    }

    @Override
    public HandlerRegistration addClickHandler(final YesNoClickHandler yesNoClickHandler) {

        ClickHandler yesButtonClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                yesNoClickHandler.onYesClick();
            }
        };
        confirmationDialogBox.addButtonClickHandler(yesButtonClickHandler);
        ClickHandler noButtonClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                yesNoClickHandler.onNoClick();
            }
        };
        confirmationDialogBox.addOtherButtonClickHandler(noButtonClickHandler);
        ClickHandler linkClickHandler = new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                yesNoClickHandler.onPreClick(event);
                confirmationDialogBox.show();
            }
        };
        return widget.addClickHandler( new ExtendedClickHandler(linkClickHandler) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

}
