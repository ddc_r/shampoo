/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.DialogBox;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTConfirmationDialogBox extends GWTExtendedWidget implements GenericTwoButtonMessageBoxInterface {

    final private DialogBox dialogBox;
    final private GWTVerticalPanel messagePanel;
    final private GWTClickableLabel yesButton;
    final private GWTClickableLabel noButton;
    
    public GWTConfirmationDialogBox() {
        super();

        ClickHandler defaultCloseBoxHandler = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                close();
            }
        };

        yesButton = new GWTClickableLabel(I18nTranslator.getInstance().yes());
        yesButton.addClickHandler(defaultCloseBoxHandler);
        noButton = new GWTClickableLabel(I18nTranslator.getInstance().no());
        noButton.addClickHandler(defaultCloseBoxHandler);

        GWTFlowPanel buttonPanel = new GWTFlowPanel();
        buttonPanel.add(yesButton);
        buttonPanel.add(noButton);

        GWTVerticalPanel panel = new GWTVerticalPanel();

        messagePanel = new GWTVerticalPanel();

        panel.add(messagePanel);
        panel.add(buttonPanel);

        dialogBox = new DialogBox(false);
        //Custom CSS
        dialogBox.setStyleName("GWTDialogBox");

        // Enable animation.
        dialogBox.setAnimationEnabled(true);
        // Enable glass background.
        dialogBox.setGlassEnabled(true);        
        dialogBox.setGlassStyleName("GWTModalBackground");
        
        dialogBox.setText(I18nTranslator.getInstance().confirmation());
        dialogBox.setWidget(panel);

        attachMainWidget(dialogBox);

    }

    @Override
    public void show() {
        dialogBox.center();
    }

    @Override
    public void close() {
        dialogBox.hide();
    }

    @Override
    public void setMessage(String message) {
        messagePanel.clean();
        addMessage(message);
    }

    @Override
    public void addMessage(String message) {
        messagePanel.add(new GWTLabel(message));
    }

    @Override
    public void setMessages(Collection<String> messages) {
        messagePanel.clean();
        if (messages!=null)
            for (String message : messages)
                addMessage(message);
    }

    @Override
    public HandlerRegistration addButtonClickHandler(ClickHandler clickHandler) {
        return yesButton.addClickHandler(clickHandler);
    }

    @Override
    public HandlerRegistration addOtherButtonClickHandler(ClickHandler clickHandler) {
        return noButton.addClickHandler(clickHandler);
    }

}

