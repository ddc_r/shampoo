/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.DialogBox;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTCustomizableConfirmationDialogBox extends GWTExtendedWidget implements GenericMultipleButtonMessageBoxInterface {

    final private DialogBox dialogBox;
    final private GWTVerticalPanel messagePanel;
    final private GWTFlowPanel buttonPanel;
    
    /** Extended version of a ClickHandler that automatically closes the dialog box when the click is processed **/
    protected class ExtendedClickHandler implements ClickHandler {

        private ClickHandler delegatedClickHandler;

        ExtendedClickHandler(ClickHandler delegatedClickHandler) {
            this();
            this.delegatedClickHandler = delegatedClickHandler;
        }

        ExtendedClickHandler() {
        }

        @Override
        public void onClick(ClickEvent ce) {
            if (delegatedClickHandler!=null)
                delegatedClickHandler.onClick(ce);
            close();
        }

    }

    public GWTCustomizableConfirmationDialogBox() {
        super();

        buttonPanel = new GWTFlowPanel();
        GWTClickableLabel defaultButton = new GWTClickableLabelCancel(I18nTranslator.getInstance().cancel());
        defaultButton.addClickHandler(new ExtendedClickHandler());
        buttonPanel.add(defaultButton);

        GWTVerticalPanel panel = new GWTVerticalPanel();
        messagePanel = new GWTVerticalPanel();
        panel.add(messagePanel);
        panel.add(buttonPanel);

        dialogBox = new DialogBox(false);
        //Custom CSS
        dialogBox.setStyleName("GWTDialogBox");

        // Enable animation.
        dialogBox.setAnimationEnabled(true);
        // Enable glass background.
        dialogBox.setGlassEnabled(true);
        dialogBox.setGlassStyleName("GWTModalBackground");
        
        dialogBox.setText(I18nTranslator.getInstance().confirmation());
        dialogBox.setWidget(panel);

        attachMainWidget(dialogBox);

    }

    @Override
    public void show() {
        dialogBox.center();
    }

    @Override
    public void close() {
        dialogBox.hide();
    }

    @Override
    public void setMessage(String message) {
        messagePanel.clean();
        addMessage(message);
    }

    @Override
    public void addMessage(String message) {
        messagePanel.add(new GWTLabel(message));
    }

    @Override
    public void setMessages(Collection<String> messages) {
        messagePanel.clean();
        if (messages!=null)
            for (String message : messages)
                addMessage(message);
    }

    @Override
    public HandlerRegistration addButton(String buttonCaption, ClickHandler clickHandler) {
        if (buttonCaption!=null /*&& clickHandler!=null*/) {
            GWTClickableLabel newButton = new GWTClickableLabel(buttonCaption);
            HandlerRegistration handler =  newButton.addClickHandler(new ExtendedClickHandler(clickHandler));
            buttonPanel.insertAt(buttonPanel.getNumberOfBoundFrameWidget()-1, newButton);
            return handler;
        }
        return null;
    }

    @Override
    public void clean() {
        super.clean();
        buttonPanel.clean();
    }

    @Override
    public HandlerRegistration addButtonClickHandler(ClickHandler cickHandler) {
        throw new UnsupportedOperationException("Use addAnotherButtonClick() instead");
    }

}

