/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.DynamicPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePSlot<T extends PlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericEditablePlaylistSlotComponentInterface<T, U, V> {

    private GWTNullableFormDropDownListBox<Boolean> featureKeySelector; //True: Single track, False: Bag of tracks, Null: unselected
    private GWTPagedFormDropDownListBox<Integer> featureFadeInSelector;
    private GenericEditablePlaylistSlotComponentInterface<? extends Object, U, V> featureValueContainer; //The widget that will contain the item related to the selected value by featureKeySelector
    private GWTPagedFormDropDownListBox<Integer> featureLoopSelector;
    private GWTCheckBox featureRequestCheckBox;
    private GWTGrid widgetContainer;
    private final ChangeHandler featureKeySelectorChanger = new ChangeHandler() {

        @Override
        public void onChange(ChangeEvent event) {
            onFeatureKeySelectorChanger();
        }
    };
    //Callback to the underlying presentation layer in order to notify it a user edit tracks
    private SlotRefreshHandler<U, V> delegatedSlotRefreshHandler = null;

    public GWTEditablePSlot(T form) {
        this(form, null);
    }
    public GWTEditablePSlot(T form, SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super();
        widgetContainer = new GWTGrid();
        //Custom CSS
        widgetContainer.setStyleName("GWTPSlot");

        featureKeySelector = new GWTNullableFormDropDownListBox<Boolean>();
        featureKeySelector.addChangeHandler(featureKeySelectorChanger);
        resetFeatureKeys();

        featureFadeInSelector = new GWTPagedFormDropDownListBox<Integer>();
        resetFeatureFadeIn();
        
        featureLoopSelector = new GWTPagedFormDropDownListBox<Integer>();
        resetFeatureLoop();

        /** add a sensible default value **/
        featureRequestCheckBox = new GWTCheckBox(false);
        resetFeatureRequest();

        widgetContainer.addRow(
                featureKeySelector,
                null,
                new GWTLabel(I18nTranslator.getInstance().fade_in()+":"),
                featureFadeInSelector,
                new GWTLabel(I18nTranslator.getInstance().repeat()+":"),
                featureLoopSelector,
                new GWTLabel(I18nTranslator.getInstance().request_allowed()+":"),
                featureRequestCheckBox);

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        setPropertiesFromComponent(form);

        attachMainWidget(widgetContainer);
    }

    public void attachNextEmbeddedComponent(GenericEditablePlaylistSlotComponentInterface<T, U, V> newComponent) {
        featureValueContainer = newComponent;
        widgetContainer.setWidgetAt(0, 1, featureValueContainer.getWidget());
    }
    public void detachNextEmbeddedComponent() {
        featureValueContainer = null;
        widgetContainer.setWidgetAt(0, 1, null);
    }

    private void resetFeatureKeys() {
        //Clear the list beforehand
        featureKeySelector.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        featureKeySelector.addItem(I18nTranslator.getInstance().static_item(), true);
        featureKeySelector.addItem(I18nTranslator.getInstance().dynamic_item(), false);
    }

    private void resetFeatureLoop() {
        //Clear the list beforehand
        featureLoopSelector.clean();

        //Generate the list
        featureLoopSelector.addItem("1", 1);
        featureLoopSelector.addItem("2", 2);
        featureLoopSelector.addItem("3", 3);
        featureLoopSelector.addItem("4", 4);
        featureLoopSelector.addItem("5", 5);
        featureLoopSelector.addItem("10", 10);
        featureLoopSelector.addItem("20", 20);
        featureLoopSelector.addItem("50", 50);
        featureLoopSelector.addItem(I18nTranslator.getInstance().infinite(), 0);
    }

    private void resetFeatureFadeIn() {
        //Clear the list beforehand
        featureFadeInSelector.clean();
        
        //Generate the list
        featureFadeInSelector.addItem(I18nTranslator.getInstance().default_value(), null);        
        featureFadeInSelector.addItem(I18nTranslator.getInstance().none(), 0);
        featureFadeInSelector.addItem("1 "+I18nTranslator.getInstance().second(), 1);
        featureFadeInSelector.addItem("2 "+I18nTranslator.getInstance().seconds(), 2);
        featureFadeInSelector.addItem("3 "+I18nTranslator.getInstance().seconds(), 3);
        featureFadeInSelector.addItem("4 "+I18nTranslator.getInstance().seconds(), 4);
        featureFadeInSelector.addItem("5 "+I18nTranslator.getInstance().seconds(), 5);
        featureFadeInSelector.addItem("10 "+I18nTranslator.getInstance().seconds(), 10);
        featureFadeInSelector.addItem("20 "+I18nTranslator.getInstance().seconds(), 20);
    }
    
    private void resetFeatureRequest() {
        //Not much to do
        featureRequestCheckBox.clean();
    }
    
    @Override
    public void setEnabled(boolean enabled) {
        featureKeySelector.setEnabled(enabled);
        if (featureValueContainer != null) {
            featureValueContainer.setEnabled(enabled);
        }
        featureFadeInSelector.setEnabled(enabled);
        featureLoopSelector.setEnabled(enabled);
        featureRequestCheckBox.setEnabled(enabled);
    }

    @Override
    public boolean setPropertiesFromComponent(T component) {
        detachNextEmbeddedComponent();
        if (component != null) {
            //Type
            if (component instanceof StaticPlaylistEntryForm) {

                StaticPlaylistEntryForm staticComponent = (StaticPlaylistEntryForm)component;
                featureKeySelector.setSelectedValue(true);
                attachNextEmbeddedComponent(new GWTEditablePSlotStaticEntry(staticComponent, delegatedSlotRefreshHandler));

            } else if (component instanceof DynamicPlaylistEntryForm) {

                DynamicPlaylistEntryForm dynamicComponent = (DynamicPlaylistEntryForm)component;
                featureKeySelector.setSelectedValue(false);
                attachNextEmbeddedComponent(new GWTEditablePSlotDynamicEntry(dynamicComponent, delegatedSlotRefreshHandler));

            } else {

                featureKeySelector.setSelectedValue(null);

            }
            //Fade in
            if (component.getFadeIn()!=null) {
                int fadeInSeconds = (int)Math.round(component.getFadeIn().intValue() / 1000.0);
                switch (fadeInSeconds) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 10:
                    case 20:
                        featureFadeInSelector.setSelectedValue(fadeInSeconds);
                        break;
                    default:
                        /** default value **/
                        featureFadeInSelector.setSelectedValue(null);
                }
            } else {
                //Add a sensible default value
                featureLoopSelector.setSelectedValue(null);
            }
            //Loop
            if (component.getLoop()!=null) {
                switch (component.getLoop().intValue()) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 10:
                    case 20:
                    case 50:
                    case 0:
                        featureLoopSelector.setSelectedValue(component.getLoop().intValue());
                        break;
                    default:
                        /** default value **/
                        featureLoopSelector.setSelectedValue(1);
                }
            } else {
                //Add a sensible default value
                featureLoopSelector.setSelectedValue(1);
            }
            //Request
            featureRequestCheckBox.setChecked(component.isRequestAllowed());
            return true;
        } else
            featureKeySelector.setSelectedValue(null);
        return false;
    }

    private StaticPlaylistEntryForm getDefaultNewStaticForm() {
        return new StaticPlaylistEntryForm();
    }

    private DynamicPlaylistEntryForm getDefaultNewDynamicForm() {
        return new DynamicPlaylistEntryForm();
    }

    private void onFeatureKeySelectorChanger() {
        detachNextEmbeddedComponent();
        if (featureKeySelector.getSelectedValue()==null) {
            //Do nothing; nothing is selected so nothing to re-attach
        } else if (featureKeySelector.getSelectedValue()==true) {
            attachNextEmbeddedComponent(new GWTEditablePSlotStaticEntry(getDefaultNewStaticForm(), delegatedSlotRefreshHandler));
        } else if (featureKeySelector.getSelectedValue()==false) {
            attachNextEmbeddedComponent(new GWTEditablePSlotDynamicEntry(getDefaultNewDynamicForm(), delegatedSlotRefreshHandler));
        }
    }

    @Override
    public T getComponentFromProperties() {
        T f = null;
        if (_arePropertiesFilledInNotRecursive()) {
            if (featureValueContainer!=null) {

                if (featureKeySelector!=null) {
                    f = (T) featureValueContainer.getComponentFromProperties();
                    if (f!=null) {
                        //Convert milliseonds to seconds
                        Integer fadeInMilliseconds = featureFadeInSelector.getSelectedValue()!=null ? (int)Math.round(featureFadeInSelector.getSelectedValue() * 1000.0) : null;
                        f.setFadeIn(fadeInMilliseconds);
                        f.setLoop(Long.valueOf(featureLoopSelector.getSelectedValue()));
                        f.setRequestAllowed(featureRequestCheckBox.isChecked());
                    }
                    //sequenceindex is directly managed by the playlist itself
                }
            }
        }
        return f;
    }

    @Override
    public boolean arePropertiesFilledIn() {
        if (_arePropertiesFilledInNotRecursive()) {
            if (featureValueContainer!=null) return featureValueContainer.arePropertiesFilledIn();
        }
        return false;
    }

    private boolean _arePropertiesFilledInNotRecursive() {
        return (featureValueContainer!=null);
    }

    @Override
    public void clean() {
        super.clean();
        resetFeatureKeys();
        detachNextEmbeddedComponent();
        resetFeatureFadeIn();
        resetFeatureLoop();
        resetFeatureRequest();
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        delegatedSlotRefreshHandler = handler;
        //No use here, pass along and notify sub-components
        if (featureValueContainer!=null)
            featureValueContainer.setSlotRefreshHandler(handler);
    }
}
