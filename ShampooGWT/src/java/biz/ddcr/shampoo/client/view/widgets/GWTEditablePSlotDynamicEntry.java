/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.SubSelectionModule;
import biz.ddcr.shampoo.client.form.playlist.DynamicPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.track.SELECTION;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePSlotDynamicEntry<T extends DynamicPlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericEditablePlaylistSlotComponentInterface<T,U,V> {

    private GWTPagedFormDropDownListBox<SELECTION> ruleKeySelector;
    private GenericEditablePlaylistSlotComponentInterface<? extends Object, U, V> ruleValueContainer;  //Will embed the widget corresponding to the featureKeySelector selected value
    private GWTGrid ruleWidgetContainer;
    private GWTFlowPanel filterWidgetContainer;
    //General Options
    private GWTCheckBox noReplayTimeSelector;
    private GWTNumericBox noReplayTimeThresholdBox;
    private GWTCheckBox noReplayPlaylistCheckBox;
    //local copy of the inside form
    private Collection<FilterForm> filters;
    //Callback to the underlying presentation layer in order to notify it a user edit tracks
    private SlotRefreshHandler<U, V> delegatedSlotRefreshHandler = null;
    private final ChangeHandler ruleKeySelectorChanger = new ChangeHandler() {

        @Override
        public void onChange(ChangeEvent event) {
            onRuleKeySelectorChanger();
        }
    };
    private final ClickHandler noReplayTimeSelectorClicker = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            onNoReplayTimeSelectorClicker();
        }
    };

    public GWTEditablePSlotDynamicEntry(T form) {
        this(form, null);
    }

    public GWTEditablePSlotDynamicEntry(T form, SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super();
        GWTFlowPanel widget = new GWTFlowPanel();
        //Custom CSS
        widget.setStyleName("GWTPSlotEntry");

        /* Rule section */
        ruleKeySelector = new GWTPagedFormDropDownListBox<SELECTION>();
        ruleKeySelector.addChangeHandler(ruleKeySelectorChanger);
        resetRuleKeys(SELECTION.values());

        ruleWidgetContainer = new GWTGrid();
        ruleWidgetContainer.addRow(
                new GWTLabel(I18nTranslator.getInstance().selection() + ":"),
                ruleKeySelector,
                null);
        widget.add(ruleWidgetContainer);

        /* Filters section */
        GWTDisclosurePanel filterPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().filters());
        //DisclosurePanels can only contain two widgets at most
        filterWidgetContainer = new GWTFlowPanel();

        filterPanel.add(filterWidgetContainer);
        widget.add(filterPanel);

        /* Misc. options section */
        GWTDisclosurePanel miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        //DisclosurePanels can only contain two widgets at most
        GWTVerticalPanel dummyPanel1 = new GWTVerticalPanel();

        noReplayTimeSelector = new GWTCheckBox(false);
        noReplayTimeSelector.setCaption(I18nTranslator.getInstance().no_replay_until_time());
        noReplayTimeSelector.addClickHandler(noReplayTimeSelectorClicker);
        noReplayTimeThresholdBox = new GWTNumericBox(60, 1, Integer.MAX_VALUE);
        GWTGrid miscOptionsGrid = new GWTGrid();
        miscOptionsGrid.addRow(
                noReplayTimeSelector,
                noReplayTimeThresholdBox,
                new GWTLabel(I18nTranslator.getInstance().minutes()));
        dummyPanel1.add(miscOptionsGrid);

        noReplayPlaylistCheckBox = new GWTCheckBox(true);
        noReplayPlaylistCheckBox.setCaption(I18nTranslator.getInstance().no_replay_within_playlist());
        dummyPanel1.add(noReplayPlaylistCheckBox);

        miscOptionsPanel.add(dummyPanel1);
        widget.add(miscOptionsPanel);

        onNoReplayTimeSelectorClicker();

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        setPropertiesFromComponent(form);

        attachMainWidget(widget);
    }

    public void attachNextEmbeddedComponent(GenericEditablePlaylistSlotComponentInterface<SubSelectionModule, U, V> newComponent) {
        ruleValueContainer = newComponent;
        ruleWidgetContainer.setWidgetAt(0, 2, ruleValueContainer.getWidget());
    }

    public void detachNextEmbeddedComponent() {
        ruleValueContainer = null;
        ruleWidgetContainer.setWidgetAt(0, 2, null);
    }

    private void resetRuleKeys(SELECTION[] rules) {
        //Clear the list beforehand
        ruleKeySelector.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        for (SELECTION rule : rules) {
            ruleKeySelector.addItem(rule.getI18nFriendlyString(), rule);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        ruleKeySelector.setEnabled(enabled);
        if (ruleValueContainer != null) {
            ruleValueContainer.setEnabled(enabled);
        }
        //Enabling/Disabling the filter widget is currently unsupported
        /*if (filterWidgetContainer != null) {
        filterWidgetContainer.setEnabled(enabled);
        }*/
        noReplayTimeSelector.setEnabled(enabled);
        noReplayTimeThresholdBox.setEnabled(enabled);
        noReplayPlaylistCheckBox.setEnabled(enabled);

    }

    @Override
    public boolean setPropertiesFromComponent(DynamicPlaylistEntryForm component) {
        if (component != null) {

            /* Rule */
            //the rule is one of the few options that cannot be left null so specify a sensible default one when none is already set
            if (component.getSelection()==null)
                component.setSelection(SELECTION.random);
            
            detachNextEmbeddedComponent();
            ruleKeySelector.setSelectedValue(component.getSelection());
            switch (component.getSelection()) {
                case random:
                    attachNextEmbeddedComponent(new GWTEditablePSlotOptionalCriterion(I18nTranslator.getInstance().sort_first(), component.getSubSelection()));
                    break;
                case weightedRandom:
                    attachNextEmbeddedComponent(new GWTEditablePSlotMandatoryCriterion(I18nTranslator.getInstance().likeliness(), component.getSubSelection()));
                    break;
                case orderedSequence:
                    attachNextEmbeddedComponent(new GWTEditablePSlotMandatoryCriterion(I18nTranslator.getInstance().sort_first(), component.getSubSelection()));
                    break;
            }

            /* Filters */
            filters = component.getFilters();
            if (delegatedSlotRefreshHandler != null) {
                delegatedSlotRefreshHandler.onFiltersCaptionRefresh((V) filters,filterWidgetContainer);
            }

            /* Misc. options */
            noReplayTimeSelector.setChecked(component.getNoReplayDelay() != null && component.getNoReplayDelay() != 0);
            onNoReplayTimeSelectorClicker();
            //yet another object that cannot be left unspecified
            if (component.getNoReplayDelay() == null) {
                component.setNoReplayDelay(null);
            } else {
                noReplayTimeThresholdBox.setValue((Long.valueOf(component.getNoReplayDelay())).intValue());
            }
            noReplayPlaylistCheckBox.setChecked(component.isNoReplayInPlaylist());

            return true;
        }
        return false;
    }

    private void onNoReplayTimeSelectorClicker() {
        noReplayTimeThresholdBox.setEnabled(noReplayTimeSelector.isChecked());
    }

    private SubSelectionModule getDefaultNewRandomRuleCriterionForm() {
        return null;
    }

    private SubSelectionModule getDefaultNewWeightedRandomRuleCriterionForm() {
        return new SubSelectionModule();
    }

    private SubSelectionModule getDefaultNewOrderedRuleCriterionForm() {
        return new SubSelectionModule();
    }

    private void onRuleKeySelectorChanger() {
        detachNextEmbeddedComponent();
        if (ruleKeySelector.getSelectedValue() != null) {
            switch (ruleKeySelector.getSelectedValue()) {
                case random:
                    attachNextEmbeddedComponent(new GWTEditablePSlotOptionalCriterion(I18nTranslator.getInstance().sort_first(), getDefaultNewRandomRuleCriterionForm()));
                    break;
                case weightedRandom:
                    attachNextEmbeddedComponent(new GWTEditablePSlotMandatoryCriterion(I18nTranslator.getInstance().likeliness(), getDefaultNewWeightedRandomRuleCriterionForm()));
                    break;
                case orderedSequence:
                    attachNextEmbeddedComponent(new GWTEditablePSlotMandatoryCriterion(I18nTranslator.getInstance().sort_first(), getDefaultNewOrderedRuleCriterionForm()));
                    break;
            }
        }
    }

    @Override
    public T getComponentFromProperties() {
        DynamicPlaylistEntryForm dpef = null;
        if (_arePropertiesFilledInNotRecursive()) {
            if (ruleValueContainer != null) {

                SubSelectionModule cm = (SubSelectionModule) ruleValueContainer.getComponentFromProperties();
                //A SubSelectionModule is mandatory, except for RandomRules
                if (cm != null || ruleKeySelector.getSelectedValue()==SELECTION.random) {

                    dpef = new DynamicPlaylistEntryForm();

                    /* Rule */
                    dpef.setSelection(ruleKeySelector.getSelectedValue());
                    dpef.setSubSelection(cm);

                    /* Filters */
                    dpef.setFilters(filters);

                    /* Misc. Options */
                    dpef.setNoReplayDelay(noReplayTimeSelector.isChecked() ? Long.valueOf(noReplayTimeThresholdBox.getValue()) : null);
                    dpef.setNoReplayInPlaylist(noReplayPlaylistCheckBox.isChecked());

                }

            }
        }
        return (T) dpef;
    }

    @Override
    public boolean arePropertiesFilledIn() {
        if (_arePropertiesFilledInNotRecursive()) {
            if (ruleValueContainer != null) {
                return ruleValueContainer.arePropertiesFilledIn();
            }
        }
        return false;
    }

    private boolean _arePropertiesFilledInNotRecursive() {
        return (ruleValueContainer != null);
    }

    @Override
    public void clean() {
        super.clean();
        resetRuleKeys(SELECTION.values());
        detachNextEmbeddedComponent();
        noReplayTimeSelector.setChecked(false);
        onNoReplayTimeSelectorClicker();
        noReplayPlaylistCheckBox.setChecked(true);
        filterWidgetContainer.clean();
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        delegatedSlotRefreshHandler = handler;
        //No use here, pass along and notify sub-components
        if (ruleValueContainer != null) {
            ruleValueContainer.setSlotRefreshHandler(handler);
        }
    }
}
