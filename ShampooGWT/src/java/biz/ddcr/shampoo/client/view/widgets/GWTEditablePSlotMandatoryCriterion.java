/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.SORT;
import biz.ddcr.shampoo.client.form.playlist.SubSelectionModule;
import biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePSlotMandatoryCriterion<T extends SubSelectionModule, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericEditablePlaylistSlotComponentInterface<T, U, V> {

    //Options for weight random selection
    private GWTPagedFormDropDownListBox<SORT> likelihoodSelector;
    private GWTCheckBox fromSelector;
    private GWTNumericBox fromValueBox;
    private GWTCheckBox toSelector;
    private GWTNumericBox toValueBox;

    private ClickHandler fromSelectorClicker = new ClickHandler() {

        @Override
        public void onClick(ClickEvent ce) {
            onFromSelectorClicker();
        }
    };
    private ClickHandler toSelectorClicker = new ClickHandler() {

        @Override
        public void onClick(ClickEvent ce) {
            onToSelectorClicker();
        }
    };

    public GWTEditablePSlotMandatoryCriterion(String caption, T form) {
        super();

        GWTVerticalPanel widget = new GWTVerticalPanel();
        //Custom CSS
        widget.setStyleName("GWTPSlotCriterion");

        GWTGrid likelihoodPanel = new GWTGrid();
        likelihoodSelector = new GWTPagedFormDropDownListBox<SORT>();
        resetLikelihoodKeys(SORT.values());

        likelihoodPanel.addRow(
                new GWTLabel(caption + ":"),
                likelihoodSelector);
        widget.add(likelihoodPanel);

        GWTDisclosurePanel subSelectionPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().limit());

        GWTGrid limitGrid = new GWTGrid();
        fromSelector = new GWTCheckBox(false);
        fromSelector.setCaption(I18nTranslator.getInstance().from());
        fromSelector.addClickHandler(fromSelectorClicker);
        fromValueBox = new GWTNumericBox(0, 0, Integer.MAX_VALUE);
        toSelector = new GWTCheckBox(false);
        toSelector.setCaption(I18nTranslator.getInstance().to());
        toSelector.addClickHandler(toSelectorClicker);
        toValueBox = new GWTNumericBox(0, 0, Integer.MAX_VALUE);

        limitGrid.addRow(
                fromSelector,
                fromValueBox,
                toSelector,
                toValueBox);
        subSelectionPanel.add(limitGrid);
        widget.add(subSelectionPanel);

        onFromSelectorClicker();
        onToSelectorClicker();

        setPropertiesFromComponent(form);

        attachMainWidget(widget);
    }

    private void resetLikelihoodKeys(SORT[] keys) {
        //Clear the list beforehand
        likelihoodSelector.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        for (SORT key : keys) {
            likelihoodSelector.addItem(key.getI18nFriendlyString(), key);
        }
    }

    private void onFromSelectorClicker() {
        fromValueBox.setEnabled(fromSelector.isChecked());
    }

    private void onToSelectorClicker() {
        toValueBox.setEnabled(toSelector.isChecked());
    }

    @Override
    public void setEnabled(boolean enabled) {       
        fromSelector.setEnabled(enabled);
        toSelector.setEnabled(enabled);
        if (enabled) {
            onFromSelectorClicker();
            onToSelectorClicker();
        } else {
            likelihoodSelector.setEnabled(false);
            fromValueBox.setEnabled(false);
            toValueBox.setEnabled(false);
        }
    }

    @Override
    public boolean arePropertiesFilledIn() {
        return _arePropertiesFilledInNotRecursive();
    }

    private boolean _arePropertiesFilledInNotRecursive() {
        return (likelihoodSelector.getSelectedValue() != null);

    }

    @Override
    public boolean setPropertiesFromComponent(SubSelectionModule component) {
        if (component != null) {
            likelihoodSelector.setSelectedValue(component.getSort());
            fromSelector.setChecked(component.getFrom()!=null);
            fromValueBox.setValue(component.getFrom()!=null?component.getFrom().intValue():0);
            toSelector.setChecked(component.getTo()!=null);
            toValueBox.setValue(component.getTo()!=null?component.getTo().intValue():0);
            onFromSelectorClicker();
            onToSelectorClicker();
            return true;
        }
        return false;
    }

    @Override
    public T getComponentFromProperties() {
        if (_arePropertiesFilledInNotRecursive()) {
            SubSelectionModule newForm = new SubSelectionModule();
            newForm.setSort(likelihoodSelector.getSelectedValue());
            newForm.setFrom(fromSelector.isChecked()?fromValueBox.getValue().longValue():null);
            newForm.setTo(toSelector.isChecked()?toValueBox.getValue().longValue():null);
            return (T) newForm;
        }
        return null;
    }

    @Override
    public void clean() {
        super.clean();
        resetLikelihoodKeys(SORT.values());
        fromSelector.setChecked(false);
        fromValueBox.setValue(0);
        toSelector.setChecked(false);
        toValueBox.setValue(0);
    }

    public void attachNextEmbeddedComponent(GenericEditablePlaylistSlotComponentInterface<AlphabeticalFilterForm, U, V> newComponent) {
        //Do nothing
    }

    public void detachNextEmbeddedComponent() {
        //Do nothing
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
