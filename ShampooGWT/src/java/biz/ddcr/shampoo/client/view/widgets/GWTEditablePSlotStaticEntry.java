/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePSlotStaticEntry<T extends StaticPlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTReadOnlyPSlotStaticEntry<T, U, V> implements GenericEditablePlaylistSlotComponentInterface<T, U, V> {

    public GWTEditablePSlotStaticEntry(T form) {
        this(form, null);
    }

    public GWTEditablePSlotStaticEntry(T form, SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super(form, playlistSlotRefreshHandler);
    }

    @Override
    public boolean arePropertiesFilledIn() {
        return _arePropertiesFilledInNotRecursive();
    }

    private boolean _arePropertiesFilledInNotRecursive() {
        return (trackFormId != null);
    }

    public void attachNextEmbeddedComponent(GenericPlaylistSlotComponentInterface<ActionCollectionEntry<String>, U, V> newComponent) {
        //Do nothing
    }

    public void detachNextEmbeddedComponent() {
        //Do nothing
    }

    @Override
    public T getComponentFromProperties() {
        if (_arePropertiesFilledInNotRecursive()) {
            StaticPlaylistEntryForm entry = new StaticPlaylistEntryForm();
            entry.setBroadcastTrackId(trackFormId);
            return (T) entry;
        }
        return null;
    }

}
