/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePlaylist extends GWTExtendedWidget implements GenericEditablePlaylistInterface<PlaylistEntryForm, ActionCollectionEntry<String>, Collection<FilterForm>> {

    protected GWTCSSGrid widget;
    //A container that will embed misc. global links when grid cells can be checked
    protected GWTFlowPanel checkedItemsLinkPanel;
    protected boolean isCheckedItemsActivated = false;
    protected ClickHandler moveUpClickHandler = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            moveSlot(getCurrentSlot(event), -1);
        }
    };
    protected ClickHandler moveDownClickHandler = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            moveSlot(getCurrentSlot(event), +1);
        }
    };
    protected ClickHandler moveTopClickHandler = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            int index = getCurrentSlot(event);
            moveSlot(index, 0 - index);
        }
    };
    protected ClickHandler moveBottomClickHandler = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            int index = getCurrentSlot(event);
            moveSlot(index, getSlotCount() - 1 - index);
        }
    };
    protected ClickHandler removeClickHandler = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            removeSlot(getCurrentSlot(event));
        }
    };
    protected ClickHandler addClickHandler = new ClickHandler() {

        @Override
        public void onClick(ClickEvent event) {
            appendNewSlotFromTemplate();
        }
    };
    //Callback to the underlying presentation layer in order to notify it a user edit tracks
    private SlotRefreshHandler<ActionCollectionEntry<String>, Collection<FilterForm>> delegatedSlotRefreshHandler = null;

    public GWTEditablePlaylist() {
        this(null);
    }

    public GWTEditablePlaylist(SlotRefreshHandler<ActionCollectionEntry<String>, Collection<FilterForm>> playlistSlotRefreshHandler) {
        super();
        GWTScrollPanel scrollPanel = new GWTScrollPanel();
        scrollPanel.add(buildUI());

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        attachMainWidget(scrollPanel);
    }

    protected Widget buildUI() {
        GWTGrid gridContainer = new GWTGrid();
        checkedItemsLinkPanel = new GWTFlowPanel();
        gridContainer.addRow(checkedItemsLinkPanel);
        widget = new GWTCSSGrid("GWTPlaylist");
        widget.setHeight("100%");
        gridContainer.addRow(widget);
        //Header
        addHeader(
                I18nTranslator.getInstance().index(),
                I18nTranslator.getInstance().slot(),
                I18nTranslator.getInstance().operation());
        //Footer
        buildTemplateSlot();
        return gridContainer;
    }

    @Override
    public void clean() {
        super.clean();
        cleanItemsLinkPanel();
        for (int i = widget.getRowCount() - 2; i > 0; i--) {
            widget.removeRow(i);
        }
        cleanTemplateSlot();
    }

    @Override
    public int getSlotCount() {
        return widget.getRowCount() - 2;
    }

    protected void buildItemsLinkPanel() {
        if (!isCheckedItemsActivated) {
            checkedItemsLinkPanel.clean();

            //Add a "select all/unselect all" widget to the container
            GWTClickableLabel checkedItemsSelectAllButton = new GWTClickableLabelInvert(I18nTranslator.getInstance().invert_selection());
            checkedItemsSelectAllButton.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    invertSelection();
                }
            });
            checkedItemsLinkPanel.add(checkedItemsSelectAllButton);

            //Add a delete all button
            GWTClickableLabel checkedItemsDeleteAllButton = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
            checkedItemsDeleteAllButton.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    deleteSelection();
                }
            });
            checkedItemsLinkPanel.add(checkedItemsDeleteAllButton);

            isCheckedItemsActivated = true;
        }
    }

    protected void cleanItemsLinkPanel() {
        if (isCheckedItemsActivated) {
            checkedItemsLinkPanel.clean();
            isCheckedItemsActivated = false;
        }
    }

    protected void invertSelection() {
        for (int i = 0; i < getSlotCount(); i++) {
            GenericCheckBoxInterface w = getCheckBox(i);
            if (w != null) {
                w.setChecked(!w.isChecked());
            }
        }
    }

    protected void deleteSelection() {
        for (int i = getSlotCount() - 1; i >= 0; i--) {
            Boolean b = isSlotChecked(i);
            if (b != null && b == true) {
                removeSlot(i);
            }
        }
    }

    protected GenericCheckBoxInterface getCheckBox(int index) {
        if (index < getSlotCount() && index >= 0) {
            Widget w = widget.getWidgetAt(index + 1, 0);//at column 0 it is either the checkBox or a blank, always;
            return (w instanceof GenericCheckBoxInterface) ? (GenericCheckBoxInterface) w : null;
        }
        return null;
    }

    @Override
    public Boolean isSlotChecked(int index) {
        GenericCheckBoxInterface w = getCheckBox(index);
        return w != null ? w.isChecked() : null;
    }

    private PlaylistEntryForm getDefaultNewForm() {
        return new StaticPlaylistEntryForm();
    }

    protected void buildTemplateSlot() {
        //checkbox

        //sequence index

        //slot

        //operations
        GWTFlowPanel operationPanel = new GWTFlowPanel();
        GWTGrid operationGrid = new GWTGrid();
        operationPanel.add(operationGrid);
        GWTClickableLabel validate = new GWTClickableLabelAdd(I18nTranslator.getInstance().add());
        validate.addClickHandler(addClickHandler);
        operationGrid.addRow(validate);

        widget.addRow(
                "template",
                null,
                null,
                //Add a sensible default component
                new GWTEditablePSlot(getDefaultNewForm(), delegatedSlotRefreshHandler),
                operationPanel);

    }

    protected void cleanTemplateSlot() {
        widget.setWidgetAt(widget.getRowCount() - 1, 2, new GWTEditablePSlot(getDefaultNewForm(), delegatedSlotRefreshHandler));
    }

    protected void appendNewSlotFromTemplate() {
        GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(widget.getRowCount() - 1, 2);
        if (s != null) {
            addFinalizedSlot(s);
            cleanTemplateSlot();
        }
    }

    protected void moveSlot(int slotToMoveIndex, int shift) {
        if (shift != 0) {
            GWTEditablePSlot s = getSlotAt(slotToMoveIndex);
            int newPosition = slotToMoveIndex + shift;
            if (s != null && newPosition >= 0 && newPosition < getSlotCount()) {
                //Keep a track of its check state
                boolean b = isSlotChecked(slotToMoveIndex);
                //Shift it
                removeSlot(slotToMoveIndex);
                insertFinalizedSlotAt(s, newPosition);
                //Reapply its original check state
                getCheckBox(newPosition).setChecked(b);
            }
        }
    }

    protected boolean isOddRow(int index) {
        return ((index % 2) != 0);
    }
    
    protected boolean addFinalizedSlot(GWTEditablePSlot slot) {
        return insertFinalizedSlotAt(slot, getSlotCount());
    }

    protected boolean insertFinalizedSlotAt(GWTEditablePSlot slot, int beforeIndex) {
        if (slot != null && beforeIndex >= 0 && beforeIndex <= getSlotCount()) {

            //checkbox
            Widget checkBox = new GWTCheckBox(false);
            buildItemsLinkPanel();

            //sequence index
            GWTLabel sequenceIndex = new GWTLabel(Integer.toString(beforeIndex + 1));

            //slot

            //operations
            GWTFlowPanel operationPanel = new GWTFlowPanel();
            GWTGrid operationGrid = new GWTGrid();
            operationPanel.add(operationGrid);
            GWTClickableLabel moveDown = null;
            GWTClickableLabel moveLast = null;
            GWTClickableLabel moveUp = null;
            GWTClickableLabel moveTop = null;
            GWTClickableLabel del = null;
            if (beforeIndex == getSlotCount()) {
                fixSlotNotLastAnymore(beforeIndex - 1);
            } else {
                moveDown = new GWTClickableLabelDown(I18nTranslator.getInstance().shift_down());
                moveDown.addClickHandler(moveDownClickHandler);
                moveLast = new GWTClickableLabelBottom(I18nTranslator.getInstance().shift_bottom());
                moveLast.addClickHandler(moveBottomClickHandler);
            }
            if (beforeIndex == 0) {
                fixSlotNotFirstAnymore(beforeIndex);
            } else {
                moveUp = new GWTClickableLabelUp(I18nTranslator.getInstance().shift_up());
                moveUp.addClickHandler(moveUpClickHandler);
                moveTop = new GWTClickableLabelTop(I18nTranslator.getInstance().shift_top());
                moveTop.addClickHandler(moveTopClickHandler);
            }
            del = new GWTClickableLabelDelete(I18nTranslator.getInstance().delete());
            del.addClickHandler(removeClickHandler);
            operationGrid.addRow(moveDown, moveLast, moveUp, moveTop, del);

            widget.insertRowAt(
                    isOddRow(beforeIndex + 1)?"odd":"even",
                    beforeIndex + 1,
                    checkBox,
                    sequenceIndex,
                    slot,
                    operationPanel);

            renumberNextSlots(beforeIndex);

            return true;
        }
        return false;
    }

    private void renumberNextSlots(int startingIndex) {
        for (int i = (startingIndex + 1); i < widget.getRowCount() - 1; i++) {
            widget.setWidgetAt(
                    isOddRow(i)?"odd":"even",
                    i,
                    1,
                    new GWTLabel(Integer.toString(i)));
        }
    }

    private void fixSlotNotLastAnymore(int index) {
        if (index < getSlotCount() && index >= 0) {
            GWTFlowPanel operationPanel = (GWTFlowPanel) widget.getWidgetAt(index + 1, 3);
            GWTGrid operationGrid = (GWTGrid) operationPanel.getFirstBoundFrameWidget();
            GWTClickableLabel moveDown = new GWTClickableLabelDown(I18nTranslator.getInstance().shift_down());
            moveDown.addClickHandler(moveDownClickHandler);
            operationGrid.setWidgetAt(0, 0, moveDown);
            GWTClickableLabel moveLast = new GWTClickableLabelBottom(I18nTranslator.getInstance().shift_bottom());
            moveLast.addClickHandler(moveBottomClickHandler);
            operationGrid.setWidgetAt(0, 1, moveLast);
        }
    }

    private void fixSlotNotFirstAnymore(int index) {
        if (index < getSlotCount() && index >= 0) {
            GWTFlowPanel operationPanel = (GWTFlowPanel) widget.getWidgetAt(index + 1, 3);
            GWTGrid operationGrid = (GWTGrid) operationPanel.getFirstBoundFrameWidget();
            GWTClickableLabel moveUp = new GWTClickableLabelUp(I18nTranslator.getInstance().shift_up());
            moveUp.addClickHandler(moveUpClickHandler);
            operationGrid.setWidgetAt(0, 2, moveUp);
            GWTClickableLabel moveTop = new GWTClickableLabelTop(I18nTranslator.getInstance().shift_top());
            moveTop.addClickHandler(moveTopClickHandler);
            operationGrid.setWidgetAt(0, 3, moveTop);

        }
    }

    private void fixSlotNowFirst(int index) {
        if (index < getSlotCount() && index >= 0) {
            GWTFlowPanel operationPanel = (GWTFlowPanel) widget.getWidgetAt(index + 1, 3);
            GWTGrid operationGrid = (GWTGrid) operationPanel.getFirstBoundFrameWidget();
            operationGrid.setWidgetAt(0, 2, new GWTLabel(null));
            operationGrid.setWidgetAt(0, 3, new GWTLabel(null));
        }
    }

    private void fixSlotNowLast(int index) {
        if (index < getSlotCount() && index >= 0) {
            GWTFlowPanel operationPanel = (GWTFlowPanel) widget.getWidgetAt(index + 1, 3);
            GWTGrid operationGrid = (GWTGrid) operationPanel.getFirstBoundFrameWidget();
            operationGrid.setWidgetAt(0, 0, new GWTLabel(null));
            operationGrid.setWidgetAt(0, 1, new GWTLabel(null));
        }
    }

    protected int getCurrentSlot(ClickEvent event) {
        return widget.getCurrentRow(event) - 1;
    }

    @Override
    public boolean removeSlot(int index) {
        if (index < getSlotCount() && index >= 0) {

            if (index == 0) {
                fixSlotNowFirst(1);
            }
            if (index == getSlotCount() - 1) {
                fixSlotNowLast(getSlotCount() - 2);
            }

            widget.removeRow(index + 1);
            renumberNextSlots(index);

            //remove the header if applicable
            if (getSlotCount() == 0) {
                cleanItemsLinkPanel();
            }

            return true;
        }
        return false;
    }

    @Override
    public boolean validateSlotAt(int index) {
        if (index >= 0 && index < getSlotCount()) {
            GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(index + 1, 2);
            if (s != null) {
                return s.arePropertiesFilledIn();
            }
        }
        return false;
    }

    @Override
    public PlaylistEntryForm getTemplateForm() {
        GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(widget.getRowCount() - 1, 2);
        if (s != null) {
            return s.getComponentFromProperties();
        }
        return null;
    }

    @Override
    public PlaylistEntryForm getFormAt(int index) {
        if (index >= 0 && index < getSlotCount()) {
            GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(index + 1, 2);
            if (s != null) {
                PlaylistEntryForm p = s.getComponentFromProperties();
                if (p != null) {
                    //Update its sort order and keep it in sync
                    p.setSequenceIndex(index);
                    return p;
                }
            }
        }
        return null;
    }

    @Override
    public boolean setTemplateForm(PlaylistEntryForm form) {
        GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(widget.getRowCount() - 1, 2);
        if (s != null) {
            return s.setPropertiesFromComponent(form);
        }
        return false;
    }

    @Override
    public boolean setFormAt(int index, PlaylistEntryForm form) {
        if (index >= 0 && index < getSlotCount()) {
            GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(index + 1, 2);
            if (s != null) {
                return s.setPropertiesFromComponent(form);
            }
        }
        return false;
    }

    @Override
    public GWTEditablePSlot getSlotAt(int index) {
        if (index >= 0 && index < getSlotCount()) {
            return (GWTEditablePSlot) widget.getWidgetAt(index + 1, 2);
        }
        return null;
    }

    protected void addHeader(String... captions) {
        if (captions != null) {

            Widget[] ws = new Widget[captions.length + 1];
            int colIndex = 0;
            //Add extra space before so as to reflect the lcoation of potential check boxes
            ws[colIndex++] = new GWTLabel(null);
            //Now add an header for each programmatically bound widget
            for (String caption : captions) {
                ws[colIndex++] = new GWTLabel(caption);
            }
            widget.insertRowAt("heading", 0, ws);
        }
    }

    @Override
    public Collection<PlaylistEntryForm> getAllForms() {
        Collection<PlaylistEntryForm> results = new ArrayList<PlaylistEntryForm>();
        for (int i = 0; i < getSlotCount(); i++) {
            results.add(getFormAt(i));
        }
        return results;
    }

    @Override
    public Collection<PlaylistEntryForm> getAllCheckedValues() {
        Collection<PlaylistEntryForm> results = new ArrayList<PlaylistEntryForm>();
        for (int i = 0; i < getSlotCount(); i++) {
            Boolean b = isSlotChecked(i);
            if (b != null && b == true) {
                results.add(getFormAt(i));
            }
        }
        return results;
    }

    @Override
    public boolean appendForms(Collection<PlaylistEntryForm> forms) {
        if (forms != null) {

            for (PlaylistEntryForm form : forms) {

                //Convert forms into widgets
                addFinalizedSlot(new GWTEditablePSlot(form, delegatedSlotRefreshHandler));

            }

            return true;
        }
        return false;
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<ActionCollectionEntry<String>, Collection<FilterForm>> handler) {
        delegatedSlotRefreshHandler = handler;
        //No use here, pass along and notify sub-components INCLUDING the template slot
        for (int index = getSlotCount() /*- 1 template slot must be in*/; index >= 0; index--) {
            GWTEditablePSlot s = (GWTEditablePSlot) widget.getWidgetAt(index + 1, 2);
            if (s != null) {
                s.setSlotRefreshHandler(delegatedSlotRefreshHandler);
            }
        }
    }

    @Override
    public int getCurrentRow(ClickEvent event) {
        return widget.getCurrentRow(event)-1;
    }

    @Override
    public boolean isTemplateSlotAt(int index) {
        return (index == getSlotCount());
    }
}
