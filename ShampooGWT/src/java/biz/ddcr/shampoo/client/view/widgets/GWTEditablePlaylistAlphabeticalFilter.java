/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePlaylistAlphabeticalFilter extends GWTExtendedWidget implements GenericPlaylistFilterInterface<AlphabeticalFilterForm> {

    private GWTPagedFormDropDownListBox<ALPHABETICAL_FEATURE> featureSelector;
    private GWTPagedFormDropDownListBox<Boolean> valueSelector;
    private GWTTextBox valueBox;

    public GWTEditablePlaylistAlphabeticalFilter() {
        super();

        GWTGrid widget = new GWTGrid();
        //Custom CSS
        widget.setStyleName("GWTPlaylistFilter");

        featureSelector = new GWTPagedFormDropDownListBox<ALPHABETICAL_FEATURE>();
        resetFeatureKeys(ALPHABETICAL_FEATURE.values());

        valueSelector = new GWTPagedFormDropDownListBox<Boolean>();
        resetValueKeys();

        valueBox = new GWTTextBox();

        widget.addRow(
                featureSelector,
                valueSelector,
                valueBox);

        attachMainWidget(widget);
    }

    private void resetFeatureKeys(ALPHABETICAL_FEATURE[] keys) {
        //Clear the list beforehand
        featureSelector.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        for (ALPHABETICAL_FEATURE key : keys) {
            featureSelector.addItem(key.getI18nFriendlyString(), key);
        }
    }

    private void resetValueKeys() {
        valueSelector.clean();
        valueSelector.addItem(I18nTranslator.getInstance().contains(), true);
        valueSelector.addItem(I18nTranslator.getInstance().does_not_contain(), false);
    }

    @Override
    public void setEnabled(boolean enabled) {       
        featureSelector.setEnabled(enabled);
        valueSelector.setEnabled(enabled);
        valueBox.setEnabled(enabled);
    }

    @Override
    public void setForm(AlphabeticalFilterForm form) {
        if (form != null) {
            featureSelector.setSelectedValue(form.getFeature());
            valueSelector.setSelectedValue(form.isInclude());
            valueBox.setText(form.getContains());
        }
    }

    @Override
    public AlphabeticalFilterForm getForm() {
        valueBox.setText(valueBox.getText().trim());
        if (featureSelector.getSelectedValue()!=null && valueSelector.getSelectedValue()!=null && valueBox.getText().length()!=0) {
            AlphabeticalFilterForm newForm = new AlphabeticalFilterForm();
            newForm.setFeature(featureSelector.getSelectedValue());
            newForm.setContains(valueBox.getText());
            newForm.setInclude(valueSelector.getSelectedValue());
            return newForm;
        }
        return null;
    }

    @Override
    public void clean() {
        super.clean();
        resetFeatureKeys(ALPHABETICAL_FEATURE.values());
        resetValueKeys();
        valueBox.clean();
    }

}
