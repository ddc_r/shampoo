/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.filter.CategoryFilterForm;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePlaylistCategoryFilter extends GWTExtendedWidget implements GenericPlaylistFilterInterface<CategoryFilterForm> {

    private GWTPagedFormDropDownListBox<TYPE> featureSelector;
    private GWTPagedFormDropDownListBox<Boolean> valueSelector;

    public GWTEditablePlaylistCategoryFilter() {
        super();

        GWTGrid widget = new GWTGrid();
        //Custom CSS
        widget.setStyleName("GWTPlaylistFilter");

        featureSelector = new GWTPagedFormDropDownListBox<TYPE>();
        resetFeatureKeys(TYPE.values());
        valueSelector = new GWTPagedFormDropDownListBox<Boolean>();
        resetValueKeys();

        widget.addRow(
                new GWTLabel(I18nTranslator.getInstance().is()),
                featureSelector);

        attachMainWidget(widget);
    }

    private void resetFeatureKeys(TYPE[] keys) {
        //Clear the list beforehand
        featureSelector.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        for (TYPE key : keys) {
            featureSelector.addItem(key.getI18nFriendlyString(), key);
        }
    }

    private void resetValueKeys() {
        valueSelector.clean();
        valueSelector.addItem(I18nTranslator.getInstance().is(), true);
        valueSelector.addItem(I18nTranslator.getInstance().is_not(), false);
    }

    @Override
    public void setEnabled(boolean enabled) {       
        featureSelector.setEnabled(enabled);
        valueSelector.setEnabled(enabled);
    }

    @Override
    public void setForm(CategoryFilterForm form) {
        if (form != null) {
            featureSelector.setSelectedValue(form.getFeature());
            valueSelector.setSelectedValue(form.isInclude());
        }
    }

    @Override
    public CategoryFilterForm getForm() {
        if (featureSelector.getSelectedValue()!=null && valueSelector.getSelectedValue()!=null) {
            CategoryFilterForm newForm = new CategoryFilterForm();
            newForm.setFeature(featureSelector.getSelectedValue());
            newForm.setInclude(valueSelector.getSelectedValue());
            return newForm;
        }
        return null;
    }

    @Override
    public void clean() {
        super.clean();
        resetFeatureKeys(TYPE.values());
        resetValueKeys();
    }

}
