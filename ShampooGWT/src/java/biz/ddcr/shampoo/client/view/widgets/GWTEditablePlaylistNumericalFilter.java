/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class GWTEditablePlaylistNumericalFilter extends GWTExtendedWidget implements GenericPlaylistFilterInterface<NumericalFilterForm> {

    private GWTPagedFormDropDownListBox<NUMERICAL_FEATURE> featureSelector;
    private GWTPagedFormDropDownListBox<Boolean> valueSelector;
    private GWTNumericBox fromValueBox;
    private GWTNumericBox toValueBox;

    public GWTEditablePlaylistNumericalFilter() {
        super();

        GWTGrid widget = new GWTGrid();
        //Custom CSS
        widget.setStyleName("GWTPlaylistFilter");

        featureSelector = new GWTPagedFormDropDownListBox<NUMERICAL_FEATURE>();
        resetFeatureKeys(NUMERICAL_FEATURE.values());
        valueSelector = new GWTPagedFormDropDownListBox<Boolean>();
        resetValueKeys();

        fromValueBox = new GWTNumericBox(0, Short.MIN_VALUE, Short.MAX_VALUE);
        toValueBox = new GWTNumericBox(0, Short.MIN_VALUE, Short.MAX_VALUE);

        widget.addRow(
                featureSelector,
                valueSelector,
                fromValueBox,
                new GWTLabel("-"),
                toValueBox);

        attachMainWidget(widget);
    }

    private void resetFeatureKeys(NUMERICAL_FEATURE[] keys) {
        //Clear the list beforehand
        featureSelector.clean();

        //Always prepend with a void item
        //featureKeySelector.addItem("", null);
        //Generate the list
        for (NUMERICAL_FEATURE key : keys) {
            featureSelector.addItem(key.getI18nFriendlyString(), key);
        }
    }

    private void resetValueKeys() {
        valueSelector.clean();
        valueSelector.addItem(I18nTranslator.getInstance().inside_interval(), true);
        valueSelector.addItem(I18nTranslator.getInstance().outside_interval(), false);
    }

    @Override
    public void setEnabled(boolean enabled) {       
        featureSelector.setEnabled(enabled);
        valueSelector.setEnabled(enabled);
        fromValueBox.setEnabled(enabled);
        toValueBox.setEnabled(enabled);
    }

    @Override
    public void setForm(NumericalFilterForm form) {
        if (form != null) {
            featureSelector.setSelectedValue(form.getFeature());
            valueSelector.setSelectedValue(form.isInclude());
            fromValueBox.setValue((int)form.getMin());
            toValueBox.setValue((int)form.getMin());
        }
    }

    @Override
    public NumericalFilterForm getForm() {
        if (featureSelector.getSelectedValue()!=null && valueSelector.getSelectedValue()!=null) {
            NumericalFilterForm newForm = new NumericalFilterForm();
            newForm.setFeature(featureSelector.getSelectedValue());
            newForm.setMax((short) Math.max(fromValueBox.getValue(), toValueBox.getValue()));
            newForm.setMin((short) Math.min(fromValueBox.getValue(), toValueBox.getValue()));
            newForm.setInclude(valueSelector.getSelectedValue());
            return newForm;
        }
        return null;
    }

    @Override
    public void clean() {
        super.clean();
        resetFeatureKeys(NUMERICAL_FEATURE.values());
        resetValueKeys();
        fromValueBox.clean();
        toValueBox.clean();
    }

}
