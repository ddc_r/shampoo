/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.helper.date.HourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.JSHourMinute;
import biz.ddcr.shampoo.client.helper.date.JSSecondMillisecond;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericCalendarInterface.DateChangeHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericExtendedFormTimetableInterface.RefreshHandler;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellCoordinates;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellRefreshHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import java.util.Collection;
import java.util.Date;

/**
 *
 * "Glue" widget that encompasses a Popup Calendar, a timezone and a timescale selector and a Weekly Timetable
 *
 * @author okay_awright
 **/
public class GWTExtendedFormWeeklyTimetable<T extends TimetableSlotForm> extends GWTExtendedWidget implements GenericExtendedFormTimetableInterface<T,HourMinuteInterface> {

    private static enum Event {

        all,
        unique,
        recurring,
        daily,
        weekly,
        monthly,
        yearly
    }
    //THE timetable grid
    final private GWTFormWeeklyTimetable timetable;
    //The date picker
    final private GWTPopupCalendar calendar;
    //The calendar navigation buttons
    final private GWTClickableLabel nextWeekButton;
    final private GWTClickableLabel previousWeekButton;
    //The channel and the user timezones (if different)
    final private GWTPagedFormDropDownListBox<String> timezoneComboBox;
    //The timetable time scale in minutes
    final private GWTPagedFormDropDownListBox<Byte> timescaleComboBox;
    //A filtering option to ony show specific kinds of timetable slot (recurring, unique, etc)
    final private GWTPagedFormDropDownListBox<Event> eventKindComboBox;
    //Callback to the underlying presentation layer in order to notify the timetable needs data in order to update the grid
    private RefreshHandler<T> delegatedRefreshHandler = null;

    public GWTExtendedFormWeeklyTimetable() {
        super();
        GWTVerticalPanel container = new GWTVerticalPanel();

        //Header controls
        GWTGrid grid1 = new GWTGrid();
        calendar = new GWTPopupCalendar(I18nTranslator.getInstance().pick_date());
        final DateChangeHandler calendarChangeHandler = new DateChangeHandler() {

            @Override
            public void onDateChange(YearMonthDayInterface newDate) {
                refreshTimetableData();
            }
        };
        calendar.setDateChangeHandler(calendarChangeHandler);
        nextWeekButton = new GWTClickableLabelNext(I18nTranslator.getInstance().next_week());
        nextWeekButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Add seven days to the current day
                YearMonthDayInterface previousCalendarDate = calendar.getCurrentDate();
                setCurrentDate(addMinutes(previousCalendarDate, 7 * 24 * 60));
            }
        });
        previousWeekButton = new GWTClickableLabelPrevious(I18nTranslator.getInstance().previous_week());
        previousWeekButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Remove seven days from the current day
                YearMonthDayInterface previousCalendarDate = calendar.getCurrentDate();
                setCurrentDate(addMinutes(previousCalendarDate, -(7 * 24 * 60)));
            }
        });
        grid1.addRow(
                previousWeekButton,
                calendar,
                nextWeekButton);

        //The main grid
        timetable = new GWTFormWeeklyTimetable((byte) 0, (byte) 60);

        //Footer controls
        GWTGrid grid2 = new GWTGrid();
        timezoneComboBox = new GWTPagedFormDropDownListBox<String>();
        final ChangeHandler timetableChangeHandler1 = new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                //Switch the time zone within the calendar current date
                YearMonthDayInterface previousCalendarDate = calendar.getCurrentDate();
                setCurrentDate(
                        new JSYearMonthDay(
                        timezoneComboBox.getSelectedValue(),
                        previousCalendarDate.getYear(),
                        previousCalendarDate.getMonth(),
                        previousCalendarDate.getDayOfMonth()));
            }
        };
        timezoneComboBox.addChangeHandler(timetableChangeHandler1);
        timescaleComboBox = new GWTPagedFormDropDownListBox<Byte>();
        final ChangeHandler timetableChangeHandler2 = new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                refreshTimetableUI();
            }
        };
        timescaleComboBox.addChangeHandler(timetableChangeHandler2);
        eventKindComboBox = new GWTPagedFormDropDownListBox<Event>();
        eventKindComboBox.addChangeHandler(timetableChangeHandler2);
        refreshEventKindList();
        grid2.addRow(
                new GWTLabel(I18nTranslator.getInstance().timezone() + ":"), timezoneComboBox,
                new GWTLabel(I18nTranslator.getInstance().event_kind() + ":"), eventKindComboBox,
                new GWTLabel(I18nTranslator.getInstance().timescale() + ":"), timescaleComboBox);

        container.add(grid1);
        container.add(timetable);
        container.add(grid2);

        attachMainWidget(container);
    }

    public void setTimeZones(
            String channelTimezoneString,
            String userTimezoneString) {
        refreshTimezoneList(channelTimezoneString, userTimezoneString);
        //Update the timetable
        refreshTimetableData();
    }

    protected JSYearMonthDay addMinutes(YearMonthDayInterface oldYearMonthDay, long shiftMinutes) {
        //Beware that the date is set up from for the current client locale, try to minimize the shift by using UTC! Hopefully it shouldn't be a real problem
        long oldUnixTimeStamp = Date.UTC(
                oldYearMonthDay.getYear() - 1900,
                oldYearMonthDay.getMonth() - 1,
                oldYearMonthDay.getDayOfMonth(),
                //Put the time at noon and ry to avoid and DST discrepancy
                12,
                0,
                0);
        //Shift it
        oldUnixTimeStamp += (shiftMinutes * 60 * 1000);
        Date newDate = new Date(oldUnixTimeStamp);
        return new JSYearMonthDay(
                oldYearMonthDay.getTimeZone(),
                (short) (newDate.getYear() + 1900),
                (byte) (newDate.getMonth() + 1),
                (byte) newDate.getDate());
    }

    @Override
    public void onRefreshData(ActionCollection<T> newForms) {
        if (newForms != null) {

            timetable.removeAllCells();

            //The calendar should display the same user-dependent week days boundaries as the grid so we can directly check dates and inject those results into the grid
            //there's no need afterward to call _getLocaleDayOfWeek()

            for (ActionCollectionEntry<T> timetableEntry : newForms) {
                if (timetableEntry.isReadable()) {

                    timetable.addSpanningCell(
                                calendar.getCurrentWeekDays(),
                                calendar.getNextWeekStartDay(),
                                timetableEntry);
                }
            }

            //Now refresh UI
            refreshTimetableUI();
        }
    }

    @Override
    public void forceRefresh() {
        refreshTimetableData();
    }
    
    protected void refreshTimetableData() {
        //First gather the current selected date
        //Do not compute a new date if the calendar is not yet initialized (e.g. a timezone is set but no time yet)
        YearMonthDayInterface currentDate = calendar.getCurrentDate();
        if (currentDate != null) {

            //Now ask the underlying presentation layer to feed the widget some slots
            if (delegatedRefreshHandler != null) {
                delegatedRefreshHandler.onRefresh(
                        new JSYearMonthDayHourMinuteSecondMillisecond(
                        calendar.getDayInCurrentWeek((byte) 0),
                        //Put the hour at noon because some DSTs may happen at midnight
                        //UPDATE: old portion of the code, I don't remember exactly the real motivation behind this so I reverted the old "new day at midnight" behaviour
                        //new JSHourMinute((byte) 12, (byte) 0),
                        new JSHourMinute((byte) 0, (byte) 0),
                        new JSSecondMillisecond((byte) 0, (short) 0)));
            }

        }
    }

    protected void refreshTimetableUI() {

        //There's always at least one default header with a weekly timetable widget
        //Add or edit another one, as appropriate
        if (calendar.getCurrentWeekDays() != null) {
            if (timetable.getHeaderRowNumber() > 1) {
                timetable.setHeaderRow(0,
                        calendar.getDayInCurrentWeek((byte) 0).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 1).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 2).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 3).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 4).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 5).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 6).getI18nSyntheticFriendlyString());
            } else {
                timetable.prependHeaderRow(
                        calendar.getDayInCurrentWeek((byte) 0).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 1).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 2).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 3).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 4).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 5).getI18nSyntheticFriendlyString(),
                        calendar.getDayInCurrentWeek((byte) 6).getI18nSyntheticFriendlyString());
            }
        }

        //Update the timescale
        timetable.setTimescaleResolution(timescaleComboBox.getSelectedValue());
        //Update the filters
        switch (eventKindComboBox.getSelectedValue()) {
            case all:
                timetable.setShowDailySlots(true);
                timetable.setShowMonthlySlots(true);
                timetable.setShowUniqueSlots(true);
                timetable.setShowWeeklySlots(true);
                timetable.setShowYearlySlots(true);
                break;
            case unique:
                timetable.setShowDailySlots(false);
                timetable.setShowMonthlySlots(false);
                timetable.setShowUniqueSlots(true);
                timetable.setShowWeeklySlots(false);
                timetable.setShowYearlySlots(false);
                break;
            case recurring:
                timetable.setShowDailySlots(true);
                timetable.setShowMonthlySlots(true);
                timetable.setShowUniqueSlots(false);
                timetable.setShowWeeklySlots(true);
                timetable.setShowYearlySlots(true);
                break;
            case daily:
                timetable.setShowDailySlots(true);
                timetable.setShowMonthlySlots(false);
                timetable.setShowUniqueSlots(false);
                timetable.setShowWeeklySlots(false);
                timetable.setShowYearlySlots(false);
                break;
            case weekly:
                timetable.setShowDailySlots(false);
                timetable.setShowMonthlySlots(false);
                timetable.setShowUniqueSlots(false);
                timetable.setShowWeeklySlots(true);
                timetable.setShowYearlySlots(false);
                break;
            case monthly:
                timetable.setShowDailySlots(false);
                timetable.setShowMonthlySlots(true);
                timetable.setShowUniqueSlots(false);
                timetable.setShowWeeklySlots(false);
                timetable.setShowYearlySlots(false);
                break;
            case yearly:
                timetable.setShowDailySlots(false);
                timetable.setShowMonthlySlots(false);
                timetable.setShowUniqueSlots(false);
                timetable.setShowWeeklySlots(false);
                timetable.setShowYearlySlots(true);
                break;
        }

        //Finally update the grid
        timetable.refresh();

    }

    protected void refreshTimezoneList(
            String channelTimezoneString,
            String userTimezoneString) {
        //Clear the list beforehand
        timezoneComboBox.clean();

        //Generate the list
        if (channelTimezoneString != null) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().channel() + " (" + channelTimezoneString + ")",
                    channelTimezoneString);
        }
        if (userTimezoneString != null && !userTimezoneString.equals(channelTimezoneString)) {
            timezoneComboBox.addItem(
                    I18nTranslator.getInstance().yours() + " (" + userTimezoneString + ")",
                    userTimezoneString);
        }

    }

    public void setTimeScales(byte[] minutes) {
        refreshTimescaleList(minutes);
        //Update the timetable
        refreshTimetableUI();
    }

    protected void refreshTimescaleList(byte[] minutes) {
        //Clear the list beforehand
        timescaleComboBox.clean();

        //Generate the list
        if (minutes != null) {
            for (byte minute : minutes) {
                timescaleComboBox.addItem(
                        minute + " " + (minute < 2 ? I18nTranslator.getInstance().minute() : I18nTranslator.getInstance().minutes()),
                        minute);
            }
        }

    }

    protected void refreshEventKindList() {
        //Clear the list beforehand
        eventKindComboBox.clean();

        //Generate the list
        for (Event event : Event.values()) {
            String i18nCaption = null;
            switch (event) {
                case all:
                    i18nCaption = I18nTranslator.getInstance().all();
                    break;
                case unique:
                    i18nCaption = I18nTranslator.getInstance().unique();
                    break;
                case recurring:
                    i18nCaption = I18nTranslator.getInstance().recurring();
                    break;
                case daily:
                    i18nCaption = I18nTranslator.getInstance().daily();
                    break;
                case weekly:
                    i18nCaption = I18nTranslator.getInstance().weekly();
                    break;
                case monthly:
                    i18nCaption = I18nTranslator.getInstance().monthly();
                    break;
                case yearly:
                    i18nCaption = I18nTranslator.getInstance().yearly();
                    break;
            }
            eventKindComboBox.addItem(
                    i18nCaption,
                    event);
        }

    }

    @Override
    public short getTimeShift() {
        return timetable.getTimeShift();
    }

    @Override
    public int getTimescaleResolution() {
        return timetable.getTimescaleResolution();
    }

    @Override
    public String getTimeZone() {
        return timezoneComboBox.getSelectedValue();
    }

    @Override
    public CellCoordinates getCurrentCellCoordinates(ClickEvent event) {
        return timetable.getCurrentCellCoordinates(event);
    }

    @Override
    public ActionCollectionEntry<T> getFormAt(CellCoordinates coordinates) {
        return timetable.getFormAt(coordinates);
    }

    @Override
    public void setDefaultForm(ActionCollectionEntry<T> form) {
        timetable.setDefaultForm(form);
    }

    @Override
    public void setCurrentDate(YearMonthDayInterface currentDate) {
        calendar.setCurrentDate(currentDate);
        refreshTimetableData();
    }

    @Override
    public YearMonthDayInterface getCurrentDate() {
        return calendar.getCurrentDate();
    }

    @Override
    public void setRefreshHandler(RefreshHandler<T> handler) {
        delegatedRefreshHandler = handler;
    }

    @Override
    public void setCellRefreshHandler(CellRefreshHandler<T,HourMinuteInterface> handler) {
        timetable.setCellRefreshHandler(handler);
    }

    @Override
    public Collection<T> getAllForms() {
        return timetable.getAllForms();
    }
    
}
