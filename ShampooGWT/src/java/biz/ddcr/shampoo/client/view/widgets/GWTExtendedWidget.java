/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * Composite widget made of a main widget and an accessory one
 * The accessory one is mainly used to display an hourglass when data is not yet ready or show notifications to user in relation to the main widget activity
 * It does not handle multiple concurrent bindings of objects to the accessory widget
 *
 * @author okay_awright
 **/
public abstract class GWTExtendedWidget extends Composite implements PluggableWidgetInterface {

    private FlexTable container = null;
    protected GWTCSSFlowPanel hourglassWidget = null;

    public GWTExtendedWidget() {
        this(null);
    }

    /**
     *
     * Supports partial and crude cloning:
     * Events are not cloned!
     *
     * @param cloneableWidget
     **/
    public GWTExtendedWidget(GWTExtendedWidget cloneableWidget) {
        super();
        container = new FlexTable();
        if (cloneableWidget!=null)
            container.getElement().setInnerHTML(cloneableWidget.getContainerElement().getInnerHTML());
        initWidget(container);
    }

    @Override
    public Widget getWidget() {
        return this;
    }

    public void attachMainWidget(Widget widget) {
        if (widget!=null) {
            container.setWidget(0, 0, widget);
        }
    }

    public void detachMainWidget() {
        if (container.isCellPresent(0, 0)) container.removeCell(0, 0);
    }

    @Override
    public void attachContextualWidget(Widget widget) {
        if (widget!=null) {
            container.setWidget(0, 1, widget);
            //Custom CSS
            container.getCellFormatter().setStyleName(0, 0, "GWTContextualWidget");
        }
    }

    @Override
    public void detachContextualWidget() {
        if (container.isCellPresent(0, 1)) container.removeCell(0, 1);
    }

    @Override
    public void clean() {
        detachContextualWidget();
    }

    @Override
    public void setVisible(boolean visible) {
        //Never hide the contextual widget
        container.setVisible(visible);
    }

    /**
     *
     * Internal use for cloning
     *
     * @return
     **/
    protected Element getContainerElement() {
        return container.getElement();
    }

    /**
     * Overrides the default GWT setEnabled() for classes that support it
     * @param enabled
     */
    @Override
    public void setEnabled(boolean enabled) {
        DOM.setElementPropertyBoolean(getContainerElement(), "disabled", !enabled);
    }

    @Override
    public void busy() {
        //Don't reinstantiate the hourglass if unecessary
        if (hourglassWidget == null) {
            hourglassWidget = new GWTCSSFlowPanel("GWTHourglass");
            this.attachContextualWidget(hourglassWidget);
        }
    }

    @Override
    public void unbusy() {
        //Don't detach the hourglass if the widget is null
        if (hourglassWidget != null) {
            this.detachContextualWidget();
            hourglassWidget = null;
        }
    }

}
