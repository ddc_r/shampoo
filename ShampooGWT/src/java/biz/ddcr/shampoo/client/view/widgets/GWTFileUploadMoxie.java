/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.helper.text.NaiveCSV;
import biz.ddcr.shampoo.client.helper.errors.UploadingGenericException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;
import java.util.Date;
import org.moxieapps.gwt.uploader.client.Uploader;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteEvent;
import org.moxieapps.gwt.uploader.client.events.FileDialogCompleteHandler;
import org.moxieapps.gwt.uploader.client.events.FileQueueErrorEvent;
import org.moxieapps.gwt.uploader.client.events.FileQueueErrorHandler;
import org.moxieapps.gwt.uploader.client.events.UploadErrorEvent;
import org.moxieapps.gwt.uploader.client.events.UploadErrorEvent.ErrorCode;
import org.moxieapps.gwt.uploader.client.events.UploadErrorHandler;
import org.moxieapps.gwt.uploader.client.events.UploadProgressEvent;
import org.moxieapps.gwt.uploader.client.events.UploadProgressHandler;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessEvent;
import org.moxieapps.gwt.uploader.client.events.UploadSuccessHandler;

/**
 *
 * @author okay_awright
 *
 */
public abstract class GWTFileUploadMoxie extends GWTExtendedWidget implements GenericFileUploadInterface {

    private class GWTClickableMoxieWrappedUpLabel extends GWTExtendedWidget implements GenericHyperlinkInterface, GenericLabelInterface {

        final private Uploader uploader;
        private String caption;

        public GWTClickableMoxieWrappedUpLabel(final Uploader uploader, final String text) {
            super();
            this.uploader = uploader;
            setCaption(text);
            attachMainWidget(this.uploader);
        }

        @Override
        public void setEnabled(boolean enabled) {
            super.setEnabled(enabled);
            uploader.setButtonDisabled(!enabled);
        }

        @Override
        public HandlerRegistration addClickHandler(ClickHandler handler) {
            //Adding click handler is prohibited, it prevents the hack Moxie uses to work
            return null;
        }

        @Override
        public void setCaption(String caption) {
            this.caption = new SafeHtmlBuilder().appendHtmlConstant("<span class=\"GWTClickableLabel GWTClickableLabel-upload\">").appendEscaped(caption).appendHtmlConstant("</span>").toSafeHtml().asString();
            uploader.setButtonText(this.caption);
        }

        @Override
        public String getCaption() {
            return caption;
        }

        public Uploader getUploader() {
            return uploader;
        }

    }

    private final static String UPLOADID_TRACKING_COOKIE = "SHAMPOO.UPLOAD.ID";
    private final static String SESSIONID_TRACKING_COOKIE = "JSESSIONID";

    //GUI
    final private VerticalPanel container;
    final private GWTFlowPanel basePanel;
    final private GWTFlowPanel statusPanel;
    final private GWTPercentProgressBar progressBar;
    final private GWTClickableLabelCancel abort;
    //objects used in the presenter or view that created this widget
    private EventBusWithLookup eventBus;
    protected String postURLPath; //upload endpoint on the server
    protected Collection<String> allowedFileExtensions;
    protected String jsessionid;
    //Internal flag to know whether the applet is fully loaded or not
    private boolean isPluploadJSReady = false;
    //The wrapped up Button/JS widget combo
    final private GWTClickableMoxieWrappedUpLabel upload;
    //Upload identifier
    private String plplldId;
    //Recycle it for each upload? May help defeat browser cache
    private boolean doRecyclePlplldId;

    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private UploadHandler delegatedUploadHandler = null;

    final FileDialogCompleteHandler fileDialogCompleteHandler = new FileDialogCompleteHandler() {
        @Override
        public boolean onFileDialogComplete(FileDialogCompleteEvent fileDialogCompleteEvent) {
            if (fileDialogCompleteEvent.getTotalFilesInQueue() > 0 && upload.getUploader().getStats().getUploadsInProgress() <= 0) {
                //We can now update with the progressbar, since a file has been chosen the upload process is now deterministic: the upload cannot be withdrawn without a notification being displayed to the user (via onSucess() or onError())
                //plugHourglassWidgetToContainer() avoids reinitializing the hourglass if it's already instantiated
                GWTFileUploadMoxie.this.busy();
                GWTFileUploadMoxie.this.refreshGUI(true);
                if (isPluploadJSReady) {
                    if (!doRecyclePlplldId) resetPlplldId();
                    upload.getUploader().startUpload();
                }
            }
            return true;
        }
    };

    final UploadProgressHandler uploadProgressHandler = new UploadProgressHandler() {
        @Override
        public boolean onUploadProgress(UploadProgressEvent uploadProgressEvent) {
            GWTFileUploadMoxie.this.onProgress(
                    (byte) Math.floor((double) uploadProgressEvent.getBytesComplete() / (double) uploadProgressEvent.getBytesTotal() * 100.)
            );
            return true;
        }
    };
    final UploadSuccessHandler uploadSuccessHandler = new UploadSuccessHandler() {
        @Override
        public boolean onUploadSuccess(UploadSuccessEvent uploadSuccessEvent) {
            GWTFileUploadMoxie.this.unbusy();
            GWTFileUploadMoxie.this.onSuccess(getCurrentUploadId());
            return true;
        }
    };

    final FileQueueErrorHandler fileQueueErrorHandler = new FileQueueErrorHandler() {
        @Override
        public boolean onFileQueueError(FileQueueErrorEvent fileQueueErrorEvent) {
            GWTFileUploadMoxie.this.unbusy();
            GWTFileUploadMoxie.this.onError(I18nTranslator.translateException(new UploadingGenericException(fileQueueErrorEvent.getMessage())));
            return true;
        }
    };

    final UploadErrorHandler uploadErrorHandler = new UploadErrorHandler() {
        @Override
        public boolean onUploadError(UploadErrorEvent uploadErrorEvent) {
            GWTFileUploadMoxie.this.unbusy();               
            if (uploadErrorEvent.getErrorCode()==ErrorCode.FILE_CANCELLED)
                GWTFileUploadMoxie.this.onCancel();
            else if (uploadErrorEvent.getErrorCodeAsInt()>299)
                GWTFileUploadMoxie.this.onError(uploadErrorEvent.getErrorCodeAsInt());
            else 
                GWTFileUploadMoxie.this.onError(I18nTranslator.translateException(new UploadingGenericException(uploadErrorEvent.getMessage())));
            return true;
        }
    };

    public GenericHyperlinkInterface getAbortLink() {
        return abort;
    }

    public GenericHyperlinkInterface getUploadLink() {
        return upload;
    }

    @Deprecated
    private void resetCookie() {
        StringBuilder cookie = new StringBuilder();
        cookie.append(UPLOADID_TRACKING_COOKIE).append("=").append(getCurrentUploadId());
        if (jsessionid != null) {
            cookie.append("; ").append(SESSIONID_TRACKING_COOKIE).append("=").append(jsessionid);
        }
        upload.getUploader().setOption("/post_params/Cookie", cookie.toString());
    }

    private void resetAllowedExtensions() {
        if (allowedFileExtensions != null && allowedFileExtensions.size() > 0) {
            upload.getUploader().setFileTypesDescription(NaiveCSV.toString(allowedFileExtensions, false).toUpperCase());
            final StringBuilder allowedFileExtensionFilter = new StringBuilder();
            boolean isFirstItem = true;
            for (String allowedFileExtension : allowedFileExtensions) {
                if (!isFirstItem) {
                    allowedFileExtensionFilter.append(";");
                }
                allowedFileExtensionFilter.append("*.").append(allowedFileExtension.toLowerCase());
                isFirstItem = false;
            }
            upload.getUploader().setFileTypes(allowedFileExtensionFilter.toString());
        }
    }

    private void resetPlplldId() {
        plplldId = "U" + Math.abs(Random.nextInt()) + (new Date()).getTime();
        upload.getUploader().setFilePostName(plplldId);
    }
    
    private void resetUploadURL() {
        if (postURLPath != null && postURLPath.length() > 0) {
            upload.getUploader().setUploadURL(postURLPath);
        }
    }

    public GWTFileUploadMoxie(String jsessionid) {
        this(true, jsessionid);
    }
    public GWTFileUploadMoxie(boolean doRecycleFileId, String jsessionid) {

        container = new VerticalPanel();
        container.setStyleName("GWTFileUploader");

        busy();

        //Panel displayed when no upload is currently in progress        
        basePanel = new GWTCSSFlowPanel("GWTFileUploader-notransfer");
        upload = new GWTClickableMoxieWrappedUpLabel(new Uploader(), I18nTranslator.getInstance().upload());
        upload.getUploader()
                .setHttpSuccess(200, 201)
                .setButtonAction(Uploader.ButtonAction.SELECT_FILE)
                .setUploadProgressHandler(uploadProgressHandler)
                .setUploadSuccessHandler(uploadSuccessHandler)                
                .setFileDialogCompleteHandler(fileDialogCompleteHandler)
                .setFileQueueErrorHandler(fileQueueErrorHandler)
                .setUploadErrorHandler(uploadErrorHandler);
        doRecyclePlplldId = doRecycleFileId;
        if (doRecyclePlplldId) resetPlplldId();

        //Don't eanble it until the applet is ready
        //UPDATE this Moxie widget has no onInit() anonymous function like the others, pretend it's already ready at this stage
        upload.setEnabled(true);
        basePanel.add(upload);

        container.add(basePanel);

        //Panel displayed when an upload is in progress
        statusPanel = new GWTCSSFlowPanel("GWTFileUploader-transfer");
        progressBar = new GWTPercentProgressBar();
        abort = new GWTClickableLabelCancel(I18nTranslator.getInstance().abort());
        //Don't eanble it until the applet is ready
        abort.setEnabled(true);
        abort.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (isPluploadJSReady) {
                    upload.getUploader().cancelUpload();
                }
                //Avoid clicking twice; it has no use
                abort.setEnabled(false);
            }
        });
        statusPanel.add(progressBar);
        statusPanel.add(abort);
        container.add(statusPanel);

        this.jsessionid = jsessionid;

        refreshGUI(false);

        isPluploadJSReady = true;

        unbusy();

        attachMainWidget(container);

    }

    protected void refreshGUI(boolean isUploadingInProgress) {
        statusPanel.setVisible(isUploadingInProgress);
        abort.setEnabled(isUploadingInProgress);
        basePanel.setVisible(!isUploadingInProgress);
        upload.setEnabled(!isUploadingInProgress);
    }

    @Override
    public void setEnabled(boolean enabled) {
        abort.setEnabled(enabled);
        upload.setEnabled(enabled);
    }

    @Override
    public void setAbsolutePostURL(String postURLPath) {
        if (postURLPath != null && postURLPath.length() != 0 && !postURLPath.equals(this.postURLPath)) {
            this.postURLPath = postURLPath;
            resetUploadURL();
        }
    }

    @Override
    public void setEventBus(EventBusWithLookup eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public String getCurrentUploadId() {
        return isPluploadJSReady ? plplldId : null;
    }

    @Override
    public void setCurrentSessionId(String jsessionidFragment) {
        if (jsessionidFragment != null && jsessionidFragment.length() != 0 && !jsessionidFragment.equals(jsessionid)) {
            jsessionid = jsessionidFragment;
        }
    }

    /**
     * Directly initiate the upload mechanism from outside this widget
     *
     */
    @Override
    public void triggerUpload() {
        //LIMITATION Not working anymore with the Moxie implementation
    }

    @Override
    public void setAllowedFileExtensions(Collection<String> extensions) {
        if (extensions != null && !extensions.isEmpty() && /*identity check for quick equality*/ extensions != allowedFileExtensions) {
            allowedFileExtensions = extensions;
            resetAllowedExtensions();
        }
    }

    private void onSuccess(String uploadId) {
        if (eventBus != null) {
            eventBus.dispatch(GlobalEventsEnum.DISPLAY_INFO, I18nTranslator.getInstance().file_successfully_uploaded());
        }
        refreshGUI(false);
        onUploadSucceeded(uploadId);
    }

    public void onUploadSucceeded(String uploadId) {
        if (delegatedUploadHandler != null) {
            delegatedUploadHandler.onSuccess(uploadId);
        }
    }

    private void onCancel() {
        refreshGUI(false);
    }
    
    private void onError(int errorCode) {
        //Convert errorCodes into Exceptions
        if (eventBus != null) {
            eventBus.dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(translateErrorCodeIntoException(errorCode)));
        }
        refreshGUI(false);
        onUploadFailed();
    }

    private void onError(String errorText) {
        //Convert errorCodes into Exceptions
        if (eventBus != null) {
            eventBus.dispatch(GlobalEventsEnum.DISPLAY_ERROR, errorText);
        }
        refreshGUI(false);
        onUploadFailed();
    }

    public void onUploadFailed() {
        if (delegatedUploadHandler != null) {
            delegatedUploadHandler.onFailure();
        }
    }

    public abstract RuntimeException translateErrorCodeIntoException(int errorCode);

    private void onProgress(byte percentDone) {
        if (percentDone >= 0) {
            progressBar.setPercent(percentDone);
        }
    }

    @Override
    public void clean() {
        progressBar.clean();
        abort.clean();
        upload.clean();
        unbusy();
        refreshGUI(false);
    }

    @Override
    public void setUploadHandler(UploadHandler handler) {
        delegatedUploadHandler = handler;
    }
}
