/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.FlowPanel;

/**
 *
 * @author okay_awright
 **/
public class GWTFlowPanel extends GWTExtendedWidget implements GenericPanelInterface {

    final protected FlowPanel widget;

    public GWTFlowPanel() {
        super();
        widget = new FlowPanel();
        //Custom CSS
        widget.setStyleName("GWTFlowPanel");
        attachMainWidget(widget);
    }

    @Override
    public void clean() {
        super.clean();
        widget.clear();
    }

    @Override
    public void add(FrameWidgetInterface widget) {
        this.widget.add(widget.getWidget());
    }

    @Override
    public void insertAt(int index, FrameWidgetInterface widget) {
        if (index>-1 && index<getNumberOfBoundFrameWidget())
            this.widget.insert(widget.getWidget(), index);
    }

    @Override
    public void remove(FrameWidgetInterface widget) {
        this.widget.remove(widget.getWidget());
    }

    @Override
    public FrameWidgetInterface getFirstBoundFrameWidget() {
        return getBoundFrameWidget(0);
    }

    @Override
    public int getNumberOfBoundFrameWidget() {
        return widget.getWidgetCount();
    }

    @Override
    public FrameWidgetInterface getBoundFrameWidget(int index) {
        if (index>=0 && index<getNumberOfBoundFrameWidget()) {
            return (FrameWidgetInterface) this.widget.getWidget(index);
        } else {
            return null;
        }
    }

}
