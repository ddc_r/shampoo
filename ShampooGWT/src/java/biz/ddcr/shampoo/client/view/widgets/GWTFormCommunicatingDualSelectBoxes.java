/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author okay_awright
 **/
public class GWTFormCommunicatingDualSelectBoxes<T> extends GWTExtendedWidget implements GenericFormCommunicatingDualSelectBoxesInterface<T> {

    final private GWTGrid containerPanel;
    final private GWTCSSClickableLabel fromLabel;
    final private Map<String, T> fromMap;
    final private ListBox fromWidget;
    final private GWTCSSClickableLabel toLabel;
    final private Map<String, T> toMap;
    final private ListBox toWidget;
    final private GWTClickableLabel selectButton;
    final private GWTClickableLabel unselectButton;
    private boolean isEnabled;
    //The current sorting parameter used
    private boolean isCurrentSortOrderDescendingFrom;
    private boolean isCurrentSortOrderDescendingTo;
    final private static byte NUMBER_OF_VISIBLE_ITEMS = 6;

    public GWTFormCommunicatingDualSelectBoxes() {
        super();

        containerPanel = new GWTGrid();

        fromLabel = new GWTCSSClickableLabel(null, "GWTTableHeader");
        fromLabel.addClickHandler(new ExtendedClickHandler(new ClickHandler() {

            @Override
                public void onClick(ClickEvent event) {
                    //toggle the descending/ascending switch
                    isCurrentSortOrderDescendingFrom = !isCurrentSortOrderDescendingFrom;
                    onHeaderEntryClicked(isCurrentSortOrderDescendingFrom, fromLabel, fromWidget, event);
                }
            }) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
        fromMap = new HashMap<String, T>();
        fromWidget = new ListBox(true);
        //Make enough room to display that many items
        fromWidget.setVisibleItemCount(NUMBER_OF_VISIBLE_ITEMS);

        toLabel = new GWTCSSClickableLabel(null, "GWTTableHeader");
        toLabel.addClickHandler(new ExtendedClickHandler(new ClickHandler() {

            @Override
                public void onClick(ClickEvent event) {
                    //toggle the descending/ascending switch
                    isCurrentSortOrderDescendingTo = !isCurrentSortOrderDescendingTo;
                    onHeaderEntryClicked(isCurrentSortOrderDescendingTo, toLabel, toWidget, event);
                }
            }) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
        toMap = new HashMap<String, T>();
        toWidget = new ListBox(true);
        //Make enough room to display that many items
        toWidget.setVisibleItemCount(NUMBER_OF_VISIBLE_ITEMS);

        VerticalPanel buttonPanel = new VerticalPanel();
        selectButton = new GWTClickableLabelRight(I18nTranslator.getInstance().add());
        selectButton.addClickHandler(new ExtendedClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                move(fromWidget, fromMap, toWidget, toMap);
            }
        }) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
        buttonPanel.add(selectButton);
        unselectButton = new GWTClickableLabelLeft(I18nTranslator.getInstance().delete());
        unselectButton.addClickHandler(new ExtendedClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent ce) {
                move(toWidget, toMap, fromWidget, fromMap);
            }
        }) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        });
        buttonPanel.add(unselectButton);

        //Custom CSS
        containerPanel.setStyleName("GWTFormCommunicatingDualSelectBoxes");
        fromWidget.setStyleName("GWTFormCommunicatingDualSelectBoxes-Left");
        toWidget.setStyleName("GWTFormCommunicatingDualSelectBoxes-Right");

        containerPanel.addRow(fromLabel, null, toLabel);
        containerPanel.addRow(fromWidget, buttonPanel, toWidget);

        resetSortingMode();
        isEnabled = true;

        attachMainWidget(containerPanel);
    }

    protected void onHeaderEntryClicked(boolean sortingMode, GWTCSSClickableLabel _label, ListBox _widget, ClickEvent event) {
        if (_label != null && _widget != null && event != null) {
            //Clean the UI first
            //Remove any sorting image attached to the entry
            _label.clean();
            //refresh the currentSelectedHeaderCellIndex
            sort(sortingMode, _widget);
            //Add the appropriate sorting image to the header cell UI
            _label.setSecondaryCSSMarker(sortingMode ? "sortDesc" : "sortAsc");
        }
    }

    protected void sort(final boolean sortingMode, ListBox _widget) {
        if (_widget!=null){

            busy();
            
            Comparator<String> c = new Comparator<String>() {

                @Override
                public int compare(String o1, String o2) {
                    return sortingMode ? o2.compareTo(o1) : o1.compareTo(o2);
                }
            };

            Map<String, String> _sortedMap = new TreeMap<String, String>(c);
            for (int i = _widget.getItemCount() - 1; i > -1; i--)
                _sortedMap.put(_widget.getItemText(i), _widget.getValue(i));

            _widget.clear();
            for (Entry<String, String> _entry : _sortedMap.entrySet()) {
                _widget.addItem(_entry.getKey(), _entry.getValue());
            }

            unbusy();
        }
    }

    @Override
    public void clean() {
        super.clean();
        fromMap.clear();
        fromWidget.clear();
        toMap.clear();
        toWidget.clear();
        resetSortingMode();
    }

    private void resetSortingMode() {
        isCurrentSortOrderDescendingFrom = true;
        fromLabel.setSecondaryCSSMarker("sortDesc");
        isCurrentSortOrderDescendingTo = true;
        toLabel.setSecondaryCSSMarker("sortDesc");
    }

    @Override
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        fromWidget.setEnabled(enabled);
        fromLabel.setEnabled(enabled);
        toWidget.setEnabled(enabled);
        toLabel.setEnabled(enabled);
        selectButton.setEnabled(enabled);
        unselectButton.setEnabled(enabled);
    }

    /** Returns false if no data has been moved over **/
    protected boolean move(ListBox _fromWidget, Map<String, T> _fromMap, ListBox _toWidget, Map<String, T> _toMap) {
        boolean result = false;
        for (int i = _fromWidget.getItemCount() - 1; i > -1; i--) {

            if (_fromWidget.isItemSelected(i)) {
                String text = _fromWidget.getItemText(i);
                String key = _fromWidget.getValue(i);
                T item = _fromMap.get(key);

                _toWidget.addItem(text, key);
                _toMap.put(key, item);

                _fromWidget.removeItem(i);
                _fromMap.remove(key);
                result = true;
            }

        }
        return result;
    }

    /** Returns false if no data has been moved over **/
    protected boolean move(String key, ListBox _fromWidget, Map<String, T> _fromMap, ListBox _toWidget, Map<String, T> _toMap) {
        for (int i = _fromWidget.getItemCount() - 1; i > -1; i--) {

            if (_fromWidget.getValue(i).equals(key)) {
                String text = _fromWidget.getItemText(i);
                T item = _fromMap.get(key);

                _toWidget.addItem(text, key);
                _toMap.put(key, item);

                _fromWidget.removeItem(i);
                _fromMap.remove(key);
                return true;
            }

        }
        return false;
    }

    /** Retunrs false if data cannot be shifted **/
    public boolean select(String key) {
        return move(key, fromWidget, fromMap, toWidget, toMap);
    }

    /** Retunrs false if data cannot be shifted **/
    public boolean unselect(String key) {
        return move(key, toWidget, toMap, fromWidget, fromMap);
    }

    @Override
    public void setHeader(String leftCaption, String rightCaption) {
        fromLabel.setCaption(leftCaption);
        toLabel.setCaption(rightCaption);
    }

    /** Retunrs false if data cannot be added **/
    public boolean appendUnselected(String caption, String key, T value) {
        return append(caption, key, value, fromWidget, fromMap, toWidget, toMap);
    }

    /** Retunrs false if data cannot be added **/
    public boolean appendSelected(String caption, String key, T value) {
        return append(caption, key, value, toWidget, toMap, fromWidget, fromMap);
    }

    protected boolean append(String caption, String key, T value, ListBox _fromWidget, Map<String, T> _fromMap, ListBox _toWidget, Map<String, T> _toMap) {
        if (caption != null && caption.length() > 0 && key != null && key.length() > 0 && value != null) {
            //Check whether the data doesn't already exist
            if (!_fromMap.containsKey(key) && !_toMap.containsKey(key)) {
                _fromWidget.addItem(caption, key);
                _fromMap.put(key, value);
                return true;
            }
        }
        return false;
    }

    @Override
    public Collection<T> getSelectedValues() {
        return toMap.values();
    }

    @Override
    public Collection<String> getSelectedKeys() {
        return toMap.keySet();
    }

    @Override
    public boolean select(String key, T value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean unselect(String key, T value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean appendUnselected(String key, T value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean appendSelected(String key, T value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
