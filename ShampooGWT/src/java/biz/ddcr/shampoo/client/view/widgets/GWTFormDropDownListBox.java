/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author okay_awright
 **/
public class GWTFormDropDownListBox<T> extends GWTExtendedWidget implements GenericDropDownListBoxInterface<T> {

    final protected ListBox widget;
    final protected Map<String,T> list;

    public GWTFormDropDownListBox() {
        super();
        widget = new ListBox(false);
        //Custom CSS
        widget.setStyleName("GWTFormDropDownListBox");
        list = new HashMap<String,T>();
        attachMainWidget(widget);
    }

    @Override
    public boolean addItem(String key, T value) {
        if (value!=null && !list.containsKey(key)) {
            list.put(key, value);
            widget.addItem(key);
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return widget.getItemCount();
    }

    @Override
    public T getSelectedValue() {
        T result = null;
        int selectedIndex = widget.getSelectedIndex();
        if (selectedIndex>=0) {
            result = list.get(widget.getValue(selectedIndex));
        }
        return result;
    }

    @Override
    public boolean setSelectedValue(T selectedValue) {
        if (selectedValue!=null)
            for (Entry<String, T> entry : list.entrySet())
                if (entry.getValue().equals(selectedValue))
                    for (int i = 0; i<widget.getItemCount();i++)
                        if (widget.getValue(i).equals(entry.getKey())) {
                            widget.setSelectedIndex(i);
                            return true;
                        }
        return false;
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return widget.addChangeHandler(handler);
    }

    @Override
    public void clean() {
        super.clean();
        widget.clear();
        list.clear();
    }

    @Override
    public boolean isValueIn(T value) {
        return list.containsValue(value);
    }

}
