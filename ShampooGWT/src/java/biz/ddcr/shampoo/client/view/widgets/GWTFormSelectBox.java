/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author okay_awright
 **/
public class GWTFormSelectBox<T> extends GWTExtendedWidget implements GenericFormSelectBoxInterface<T> {

    final private ListBox widget;
    final private Map<String,T> list;

    /**
     * Courtesy struct for GWTFormCommunicatingDualSelectBoxes, notably
     **/
    public class Entry {
        public String key;
        public T value;
        public Entry(String key, T value) {
            this.key = key;
            this.value = value;
        }
    }

    public GWTFormSelectBox() {
        super();
        GWTScrollPanel scrollPanel = new GWTScrollPanel();
        widget = new ListBox(true);
        //Custom CSS
        widget.setStyleName("GWTFormSelectBox");
        list = new HashMap<String,T>();
        scrollPanel.add(widget);
        attachMainWidget(scrollPanel);
    }

    @Override
    public boolean addItem(String key, T value) {
        if (value!=null && !list.containsKey(key)) {
            list.put(key, value);
            widget.addItem(key);
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return widget.getItemCount();
    }

    public T getValueAt(int row) {
        if (row<widget.getItemCount())
            return list.get(widget.getValue(row));
        return null;
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return widget.addChangeHandler(handler);
    }

    @Override
    public Collection<T> getSelectedValues() {
        Collection<T> results = null;
        for (int i = 0; i < widget.getItemCount(); i++) {
            if (widget.isItemSelected(i)) {
                if (results==null) results = new ArrayList<T>();
                results.add(list.get(widget.getValue(i)));
            }
        }
        return results;
    }

    public Collection<Entry> getSelectedEntries() {
        Collection<Entry> results = null;
        for (int i = 0; i < widget.getItemCount(); i++) {
            if (widget.isItemSelected(i)) {
                if (results==null) results = new ArrayList<Entry>();
                String selectKey = widget.getValue(i);
                Entry newEntry = new Entry(selectKey, list.get(selectKey));
                results.add(newEntry);
            }
        }
        return results;
    }

    public boolean removeItem(String key) {
        for (int i = 0; i < widget.getItemCount(); i++)
            if (widget.getValue(i).equals(key)) {
                if (list.remove(key)!=null) {
                    widget.removeItem(i);
                    return true;
                }
                return false;
            }
        return false;
    }

    @Override
    public void clean() {
        super.clean();
        widget.clear();
        list.clear();
    }

    @Override
    public boolean isValueIn(T value) {
        return list.containsValue(value);
    }

}
