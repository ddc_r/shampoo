/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.GenericFormInterface;
import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.view.widgets.GenericFormSortableTableInterface.SortableTableClickHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;

/**
 *
 * @author okay_awright
 **/
public class GWTFormSortableTable<T extends GenericFormInterface, E extends Widget, F extends TAGS> extends GWTFormTable<T, E> implements GenericFormSortableTableInterface<T, E, F> {

    ///The maximum number of rows to display
    private int maxRows;
    //The current starting page index
    private int currentOffset;
    //The current sorting parameter used
    private int currentSelectedHeaderCellIndex;
    private boolean isCurrentSortOrderDescending;
    //The current filtering options
    private F currentSelectedFilter;
    private String currentFilterOperator;
    private String currentFilterCriterion;
    //all the sorting tags associated with the header cells
    final private HashMap<Integer,F> tags;
    //Header click handlers
    final private ClickHandler headerEntryClicked;
    private ClickHandler filterButtonClicked;
    private ClickHandler previousButtonClicked;
    private ClickHandler nextButtonClicked;
    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private SortableTableClickHandler delegatedHeaderClick;
    private SortableTableClickHandler delegatedFilterClick;
    private SortableTableClickHandler delegatedNextButtonClick;
    private SortableTableClickHandler delegatedPreviousButtonClick;
    private GWTClickableLabel previousButton;
    private GWTClickableLabel nextButton;
    //Special composite widget that allows the user to filter out results from the grid
    private GWTPagedFormDropDownListBox<F> filterComboBox;
    private GWTTextBox filterTextBox;
    private GWTPagedFormDropDownListBox<String> filterOperator;
    private GWTClickableLabel filterOkButton;

    public GWTFormSortableTable() {
        super();
        headerEntryClicked = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onHeaderEntryClicked(event);
            }
        };

        //Initialize default values
        maxRows = 20;
        currentOffset = 0;
        currentSelectedHeaderCellIndex = -1;
        isCurrentSortOrderDescending = true;
        tags = new HashMap<Integer,F>();
    }

    @Override
    protected Widget buildUI() {
        //Add a filter box
        GWTGrid filterPanel = new GWTGrid();
        //Custom CSS
        filterPanel.setStyleName("GWTFilterBox");
        ChangeHandler filterComboBoxChanged = new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent event) {
                refreshFilterOptions();
            }
        };
        filterComboBox = new GWTPagedFormDropDownListBox<F>();
        filterComboBox.addChangeHandler(filterComboBoxChanged);
        filterOperator = new GWTPagedFormDropDownListBox<String>();
        filterTextBox = new GWTTextBox();
        filterButtonClicked = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onFilterButtonClicked(event);
            }
        };
        filterOkButton = new GWTClickableLabelFilter(I18nTranslator.getInstance().filter());
        filterOkButton.addClickHandler(filterButtonClicked);
        filterPanel.addRow(filterComboBox, filterOperator, filterTextBox, filterOkButton);

        //Add a "Previous" and a "Next" button to the grid but do not activate them until needed
        previousButtonClicked = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onPreviousButtonClicked(event);
            }
        };
        previousButton = new GWTClickableLabelPrevious(I18nTranslator.getInstance().previous());
        previousButton.setVisible(false);
        previousButton.addClickHandler(previousButtonClicked);
        nextButtonClicked = new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                onNextButtonClicked(event);
            }
        };
        nextButton = new GWTClickableLabelNext(I18nTranslator.getInstance().next());
        nextButton.setVisible(false);
        nextButton.addClickHandler(nextButtonClicked);

        GWTGrid gridContainer = new GWTGrid();
        gridContainer.addRow(filterPanel);
        gridContainer.addRow(super.buildUI());
        //Add those buttons below the actual grid
        GWTFlowPanel buttonPanel = new GWTFlowPanel();
        buttonPanel.add(previousButton);
        buttonPanel.add(nextButton);
        gridContainer.addRow(buttonPanel);

        return gridContainer;
    }
    
    @Override
    public void setHeader(SortableHeaderEntryInterface<F>... columnEntries) {
        if (columnEntries != null) {
            if (!header) {
                widget.insertRow(0);
                //Custom CSS
                widget.getRowFormatter().setStyleName(0, "heading");
                updateRowCSS(1);
                header = true;
            }
            int colIndex = 0;
            //Add extra space before so as to reflect the lcoation of potential check boxes
            widget.setWidget(0, colIndex++, new GWTLabel(null));
            tags.clear();
            filterComboBox.clean();
            //Now add an header for each programmatically bound widget
            for (SortableHeaderEntryInterface<F> columnEntry : columnEntries) {
                //Embed each header cell widget in a GenericClickableContainerWithImage
                //And add a click handler to each one
                GWTCSSClickableLabel boxedLabel = new GWTCSSClickableLabel(columnEntry.getCaption(), "GWTTableHeader");
                boxedLabel.addClickHandler(headerEntryClicked);
                widget.setWidget(0, colIndex, boxedLabel);
                
                if (columnEntry.getFlag()!=null) {
                    tags.put(colIndex, columnEntry.getFlag());
                    //Do only add it as a filter option if its type is supported, i.e. is not null
                    if (columnEntry.getFlag().getLooseType()!=null)
                        filterComboBox.addItem(columnEntry.getCaption(), columnEntry.getFlag());
                }

                colIndex++;
            }
            //Select a default currently selected header cell if there is at least one available
            for (int i = 0; i < widget.getCellCount(0); i++) {
                if (tags.containsKey(i)) {
                    updateSortingParametersAndUIForHeader(i);
                    break;
                }
            }
            //Update the filter options if necessary
            refreshFilterOptions();
        }
    }

    @Override
    public void setHeader(String... captions) {
        throw new UnsupportedOperationException("Use setHeader(SortableHeaderEntryInterface<F>...) instead");
    }

    @Override
    public void removeHeader() {
        super.removeHeader();
        currentSelectedHeaderCellIndex = -1;
        tags.clear();
        filterComboBox.clean();
        refreshFilterOptions();
    }
   
    @Override
    public void setRefreshClickHandler(SortableTableClickHandler handler) {
        delegatedHeaderClick = handler;
        delegatedFilterClick = handler;
        delegatedPreviousButtonClick = handler;
        delegatedNextButtonClick = handler;
    }

    @Override
    public GroupAndSort<F> getCurrentGroupAndSortParameters() {
        return generateGroupAndSortObject();
    }

    protected void onPreviousButtonClicked(ClickEvent event) {
        //First make sure that currentOffset is greater than 0
        //There is a clickHandler bound to the event
        if (delegatedPreviousButtonClick != null && currentOffset > 0) {
            //Update both the sorting parameters and the UI with the values dependent of the now selected header entry
            updateSortingParametersAndUIForButton(currentOffset - 1);

            //Fire an event that must be handled by the underlying layer that use this object
            //Add to this event a specific GroupAndSort object that contains all the info needed to trim and sort out valued based on the header clicked
            delegatedPreviousButtonClick.onClick(event, generateGroupAndSortObject());
        }
    }

    protected void onNextButtonClicked(ClickEvent event) {
        //First make sure that currentOffset is greater than or equals 0
        //There is a clickHandler bound to the event
        if (delegatedNextButtonClick != null && currentOffset >= 0) {
            //Update both the sorting parameters and the UI with the values dependent of the now selected header entry
            updateSortingParametersAndUIForButton(currentOffset + 1);

            //Fire an event that must be handled by the underlying layer that use this object
            //Add to this event a specific GroupAndSort object that contains all the info needed to trim and sort out valued based on the header clicked
            delegatedNextButtonClick.onClick(event, generateGroupAndSortObject());
        }
    }

    protected void onHeaderEntryClicked(ClickEvent event) {
        //First make sure that tags are present
        //There is a clickHandler bound to the event
        //And that we can track which cell has been clicked
        if (delegatedHeaderClick != null && tags != null && event != null) {
            //Now find which header cell has been clicked
            HTMLTable.Cell clickedHeaderCell = widget.getCellForEvent(event);
            if (clickedHeaderCell != null) {
                //OK, a cell in the header has been clicked
                int i = clickedHeaderCell.getCellIndex();
                //See if there's a tag for this cell: the cell index must match one in the tag list
                if (tags.containsKey(i)) {

                    //Update both the sorting parameters and the UI with the values dependent of the now selected header entry
                    updateSortingParametersAndUIForHeader(i);

                    //Fire an event that must be handled by the underlying layer that use this object
                    //Add to this event a specific GroupAndSort object that contains all the info needed to trim and sort out valued based on the header clicked
                    delegatedHeaderClick.onClick(event, generateGroupAndSortObject());
                }
            }
        }
    }

    protected void onFilterButtonClicked(ClickEvent event) {
        //First make sure that tags are present
        //There is a clickHandler bound to the event
        //And that we can track which cell has been clicked
        if (delegatedFilterClick != null && filterComboBox != null && filterTextBox != null) {
            F flag = filterComboBox.getSelectedValue();
            String operator = filterOperator.getSelectedValue();
            //See if there's a tag for this entry: the selected value must match one in the tag list
            if (flag!=null && operator!=null) {

                //Update both the filtering parameters and the UI with the values dependent of the now selected combox entry
                updateSortingParametersAndUIForFilter(flag, operator, filterTextBox.getText());

                //BUG fix: must reset the current starting row
                updateSortingParametersAndUIForButton(0);

                //Fire an event that must be handled by the underlying layer that use this object
                //Add to this event a specific GroupAndSort object that contains all the info needed to trim and sort out valued based on the header clicked
                delegatedFilterClick.onClick(event, generateGroupAndSortObject());
            }
        }
    }

    private void refreshFilterOptions() {
        filterOperator.clean();
        filterTextBox.clean();
        F currentTag = filterComboBox.getSelectedValue();
        if (currentTag!=null) {
            switch (currentTag.getLooseType()) {
                case alphanumeric:
                    filterOperator.addItem(I18nTranslator.getInstance().is(), "=");
                    filterOperator.addItem(I18nTranslator.getInstance().is_not(), "!=");
                    filterOperator.addItem(I18nTranslator.getInstance().contains(), "LIKE");
                    filterOperator.addItem(I18nTranslator.getInstance().does_not_contain(), "NOT LIKE");
                    break;
                case numeric:
                    filterOperator.addItem(I18nTranslator.getInstance().is_equal_to(), "=");
                    filterOperator.addItem(I18nTranslator.getInstance().is_not_equal_to(), "<>");
                    filterOperator.addItem(I18nTranslator.getInstance().is_equal_or_greater_than(), ">=");
                    filterOperator.addItem(I18nTranslator.getInstance().is_equal_or_lower_than(), "<=");
                    break;
            }
        }
    }

    private void updateSortingParametersAndUIForButton(int newOffset) {
        //Update the current sorting parameters
        currentOffset = newOffset;
        //if the currentOffset is greater than zero then the "Previous" can be displayed
        previousButton.setVisible(currentOffset > 0);
        //displaying the "Next" button is the sole responsibility of the underlying presentation layer
    }

    private void updateSortingParametersAndUIForHeader(int currentlySelectedHeaderCellIndex) {
        if (header) {
            //Clean the UI first
            //Starts at 1, and not 0, since the first widget in the deader is reserved for mass selection
            for (int i = 1; i < widget.getCellCount(0); i++) {
                GWTCSSClickableLabel currentWidget = (GWTCSSClickableLabel) widget.getWidget(0, i);
                //Remove any sorting image attached to the entry
                currentWidget.clean();
            }
            if (header && currentlySelectedHeaderCellIndex > -1) {
                //Update the current sorting parameters
                //If the previous cell clicked is the same as now: toggle the descending/ascending switch
                if (currentSelectedHeaderCellIndex == currentlySelectedHeaderCellIndex) {
                    isCurrentSortOrderDescending = !isCurrentSortOrderDescending;
                }
                //refresh the currentSelectedHeaderCellIndex
                currentSelectedHeaderCellIndex = currentlySelectedHeaderCellIndex;
                //Add the appropriate sorting image to the header cell UI
                GWTCSSClickableLabel currentWidget = (GWTCSSClickableLabel) widget.getWidget(0, currentSelectedHeaderCellIndex);
                currentWidget.setSecondaryCSSMarker(isCurrentSortOrderDescending ? "sortDesc" : "sortAsc");
            }
        }
    }

    private void updateSortingParametersAndUIForFilter(F filter, String operator, String criterion) {
        currentSelectedFilter = filter;
        currentFilterOperator = operator;
        currentFilterCriterion = criterion;
        //Nothing to redraw
    }

    private GroupAndSort<F> generateGroupAndSortObject() {
        GroupAndSort<F> currentGroupAndSort = new GroupAndSort<F>();
        currentGroupAndSort.setMaxRows(maxRows);
        currentGroupAndSort.setSortByDescendingDirection(isCurrentSortOrderDescending);
        currentGroupAndSort.setSortByFeature((currentSelectedHeaderCellIndex > -1 ? tags.get(currentSelectedHeaderCellIndex) : null));
        currentGroupAndSort.setStartRow(currentOffset * maxRows);
        currentGroupAndSort.setFilterByFeature(currentSelectedFilter, currentFilterOperator, currentFilterCriterion);
        return currentGroupAndSort;
    }

    @Override
    public void setMaxVisibleRows(int maxRows) {
        if (maxRows > 0) {
            this.maxRows = maxRows;
        }
    }

    @Override
    public int getMaxVisibleRows() {
        return maxRows;
    }

    @Override
    public void setNextButtonVisible(boolean isVisible) {
        nextButton.setVisible(isVisible);
    }
}
