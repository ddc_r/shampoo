/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTFormTable<T, E extends Widget> extends GWTExtendedWidget implements GenericFormTableInterface<T, E> {

    protected FlexTable widget;
    protected final ArrayList<T> list = new ArrayList<T>();
    protected boolean header = false;
    //A container that will embed misc. global links when grid cells can be checked
    protected GWTFlowPanel checkedItemsLinkPanel;
    protected GWTClickableLabel checkedItemsSelectAllButton;
    protected boolean isCheckedItemsActivated = false;
    //Callback to the underlying presentation layer so as to bind misc. captions in a special header when at least one item can be clicked on
    private CheckedItemHeaderRefreshHandler delegatedRefreshCheckedItemLinkPanelHandler = null;
    //Keep an eye on the current number of checked items; when an item is removed it helps to decide if the special header needs to be disabled
    private int currentNumberOfCheckedItems;

    public GWTFormTable() {
        super();
        attachMainWidget(buildUI());
    }

    protected Widget buildUI() {
        GWTGrid gridContainer = new GWTGrid();
        checkedItemsLinkPanel = new GWTFlowPanel();
        gridContainer.addRow(checkedItemsLinkPanel);
        widget = new FlexTable();
        //Custom CSS
        widget.setStyleName("GWTFormTable");
        widget.setHeight("100%");
        gridContainer.addRow(widget);
        return gridContainer;
    }

    protected boolean hasHeader() {
        return header;
    }

    @Override
    public void clean() {
        super.clean();
        cleanItemsLinkPanel();
        if (!header) {
            widget.removeAllRows();
        } else {
            for (int i = widget.getRowCount() - 1; i > 0; i--) {
                widget.removeRow(i);
            }
        }
        list.clear();
    }

    @Override
    public int getRowCount() {
        return header ? widget.getRowCount() - 1 : widget.getRowCount();
    }

    @Override
    public int getCellCount(int row) {
        return widget.getCellCount(row);
    }

    protected void updateRowCSS(int fromIndex) {
        updateRowCSS(fromIndex, null);
    }
    protected void updateRowCSS(int fromIndex, String secondaryCssStyle) {
        HTMLTable.RowFormatter rf = widget.getRowFormatter();

        if (fromIndex < widget.getRowCount()) {
            if (secondaryCssStyle==null || secondaryCssStyle.length()==0)
                _updateRowCSS(rf, fromIndex, getSecondaryRowCSS(rf, fromIndex));
            else
                _updateRowCSS(rf, fromIndex, "-"+secondaryCssStyle);
            for (int row = fromIndex+1; row < widget.getRowCount(); row++)
                _updateRowCSS(rf, row, getSecondaryRowCSS(rf, row));
        }
    }
    protected String getSecondaryRowCSS(HTMLTable.RowFormatter rf, int row) {
        String currentFullStyle = rf.getStyleName(row);
        return (currentFullStyle.length()>4) ? currentFullStyle.substring(3) : null;
    }
    protected void _updateRowCSS(HTMLTable.RowFormatter rf, int row, String secondaryCssStyle) {
        if ((row % 2) != 0) {
            rf.setStyleName(row, "odd"+(secondaryCssStyle!=null?secondaryCssStyle:""));
        } else {
            rf.setStyleName(row, "evn"+(secondaryCssStyle!=null?secondaryCssStyle:""));
        }
    }

    protected void buildItemsLinkPanel() {
        if (!isCheckedItemsActivated) {
            currentNumberOfCheckedItems = 0;
            checkedItemsLinkPanel.clean();

            //Add a "select all/unselect all" widget to the container
            checkedItemsSelectAllButton = new GWTClickableLabelInvert(I18nTranslator.getInstance().invert_selection());
            checkedItemsSelectAllButton.addClickHandler(new ClickHandler() {

                @Override
                public void onClick(ClickEvent event) {
                    invertSelection();
                }
            });
            checkedItemsLinkPanel.add(checkedItemsSelectAllButton);

            //Now ask the underlying presentation layer to feed the widget some misc. links or captions
            if (delegatedRefreshCheckedItemLinkPanelHandler != null) {
                delegatedRefreshCheckedItemLinkPanelHandler.onCheckedItemHeaderRefresh(checkedItemsLinkPanel);
            }

            isCheckedItemsActivated = true;
        }
    }
    protected void cleanItemsLinkPanel() {
        if (isCheckedItemsActivated) {
            currentNumberOfCheckedItems = 0;
            checkedItemsLinkPanel.clean();
            isCheckedItemsActivated = false;
        }
    }
    protected void invertSelection() {
        for (int i = 0; i < getRowCount(); i++) {
            GenericCheckBoxInterface w = getCheckBox(i);
            if (w!=null)
                w.setChecked(!w.isChecked());
        }
    }
    protected GenericCheckBoxInterface getCheckBox(int row) {
        int currentRow = header ? row + 1 : row;
        if (currentRow < widget.getRowCount()) {
            Widget w = widget.getWidget(currentRow, 0);//at column 0 is either the checkBox or a blank, always;
            return (w instanceof GenericCheckBoxInterface) ? (GenericCheckBoxInterface)w : null;
        }
        return null;
    }
    @Override
    public Boolean isRowChecked(int row) {
        GenericCheckBoxInterface w = getCheckBox(row);
        return w!=null ? w.isChecked() : null;
    }

    @Override
    public boolean addRow(T form, boolean canBeChecked, E... widgets) {
        return addRow(null, form, canBeChecked, widgets);
    }
    @Override
    public boolean addRow(String cssStyle, T form, boolean canBeChecked, E... widgets) {
        if (widgets != null) {
            int colIndex = 0;
            int rowIndex = widget.getRowCount();
            if (list.add(form)) {
                //checkbox
                Widget checkBox = null;
                if (canBeChecked) {
                    checkBox = new GWTCheckBox(false);
                    buildItemsLinkPanel();
                    //Increment the number of checkable items
                    if (isCheckedItemsActivated)
                        currentNumberOfCheckedItems++;
                } else {
                    //dummy widget
                    checkBox = new GWTLabel(null);
                }
                widget.setWidget(rowIndex, colIndex, checkBox);
                //Custom CSS
                widget.getCellFormatter().setStyleName(rowIndex, colIndex++, "cell");
                //All other items
                for (E localWidget : widgets) {
                    widget.setWidget(rowIndex, colIndex, localWidget);
                    //Custom CSS
                    widget.getCellFormatter().setStyleName(rowIndex, colIndex++, "cell");
                }
                updateRowCSS(rowIndex, cssStyle);
                return true;
            }
        }
        return false;
    }

    @Override
    public int getCurrentRow(ClickEvent event) {
        HTMLTable.Cell clickedCell= null;
        if (event!=null && (clickedCell = widget.getCellForEvent(event))!=null) {
            return header ? clickedCell.getRowIndex() - 1 : clickedCell.getRowIndex();
        }
        return -1;
    }

    @Override
    public boolean removeRow(int row) {
        int currentRow = header ? row + 1 : row;
        if (currentRow < widget.getRowCount() && currentRow > -1) {
            if (list.remove(row)!=null) {
                //Decrement the number of checkable items iff the item can be checked (i.e. isRowChecked() is not null)
                if (isCheckedItemsActivated && isRowChecked(currentRow)!=null) {
                    currentNumberOfCheckedItems--;
                    //check if the special header needs to be disabled
                    if (currentNumberOfCheckedItems<1)
                        cleanItemsLinkPanel();
                }
                //And finally drop the row
                widget.removeRow(currentRow);
                updateRowCSS(currentRow);
                return true;
            }
        }
        return false;
    }

    @Override
    public E getComponentAt(int row, int column) {
        int currentRow = header ? row + 1 : row;
        int currentColumn = column + 1;
        if (currentRow < widget.getRowCount() && currentColumn < widget.getCellCount(currentRow)) {
            return (E) widget.getWidget(currentRow, currentColumn);
        } else {
            return null;
        }
    }

    @Override
   public void setHeader(String... captions) {
        if (captions != null) {
            if (!header) {
                widget.insertRow(0);
                //Custom CSS
                widget.getRowFormatter().setStyleName(0, "heading");
                updateRowCSS(1);
                header = true;
            }
            int colIndex = 0;
            //Add extra space before so as to reflect the lcoation of potential check boxes
            widget.setWidget(0, colIndex++, new GWTLabel(null));
            //Now add an header for each programmatically bound widget
            for (String caption : captions) {
                widget.setWidget(0, colIndex++, new GWTLabel(caption));
            }
        }
    }

    @Override
    public void removeHeader() {
        if (header) {
            header = false;
            widget.removeRow(0);
            updateRowCSS(0);
        }
    }

    @Override
    public T getValueAt(int row) {
        if (row < list.size()) {
            return list.get(row);
        } else {
            return null;
        }
    }

    @Override
    public boolean isValueIn(T value) {
        boolean result = false;
        for (T oldValue : list) {
            if (value.equals(oldValue)) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Override
    public Collection<T> getAllValues() {
        Collection<T> results = new ArrayList<T>();
        for (int i = 0; i < getRowCount(); i++) {
            T value = list.get(i);
            if (value!=null) {
                results.add(value);
            }
        }
        return results;
    }
    @Override
    public Collection<T> getAllCheckedValues() {
        Collection<T> results = new ArrayList<T>();
        for (int i = 0; i < getRowCount(); i++) {
            Boolean b = isRowChecked(i);
            if (b!=null && b==true) {
                T value = list.get(i);
                if (value!=null) {
                    results.add(value);
                }
            }
        }
        return results;
    }

    @Override
    public void setCheckedItemHeaderRefreshHandler(CheckedItemHeaderRefreshHandler handler) {
        delegatedRefreshCheckedItemLinkPanelHandler = handler;
    }
}
