/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class GWTFormTimetableTimeSlotBoundaries {

    private short from;
    private short max;

    public GWTFormTimetableTimeSlotBoundaries(short from, short max) {
        this.from = from;
        this.max = max;
    }

    public short getFrom() {
        return from;
    }

    public short getMax() {
        return max;
    }

}
