/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.UniqueTimetableSlotForm;
import biz.ddcr.shampoo.client.helper.date.HourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.JSHourMinute;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.client.helper.date.SecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellCoordinates;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellRefreshHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.i18n.client.constants.DateTimeConstants;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

/**
 *
 * @author okay_awright
 *
 */
public class GWTFormWeeklyTimetable<T extends TimetableSlotForm> extends GWTExtendedWidget implements GenericFormTimetableInterface<T, HourMinuteInterface> {

    static DateTimeConstants intlConstants;
    private static byte startingDayOfWeek;

    static {
        if (GWT.isClient()) {
            intlConstants = LocaleInfo.getCurrentLocale().getDateTimeConstants();
            //According to GWT days: Sunday is 1 and Monday is 2; make it start on Monday
            startingDayOfWeek = (byte) (Byte.parseByte(GWTFormWeeklyTimetable.intlConstants.firstDayOfTheWeek()) - 1);
            if (startingDayOfWeek < 1) {
                startingDayOfWeek += 7;
            }
        } else {
            //If we got no clue, we assume a week starts on Monday (compliant with ISO and European standards)
            startingDayOfWeek = 1;
        }
    }
    //Flextables support span
    final private FlexTable widget;
    //Additional headers; week day name header is the basic one
    ArrayList<String[]> additionalHeaders;
    //Additional left legends; time intervals is the basic one
    ArrayList<String[]> additionalLegends;
    //Headers on top of the grid; there is always at least one
    //private int headerRowNumber = 0;
    //Legends at the left side of the grid; there is always at least one
    //private int legendColNumber = 0;
    //timescale resolution in minutes
    private byte timescaleResolution = 1;
    //shifting number for timeslots
    private short timeShift = 0;
    //Each time slot is allocated one or more indexes to specific widgets; null == no widget
    //Multiple widgets of different kinds (unique, weekly, etc) can share a same time slot
    private GWTFormTimetableTimeSlot[] timeSlots;
    private long timeSlotNum;
    private TreeMap<Long, GWTFormTimetableTimeSlotBoundaries> timeSlotBoundaries;
    private TreeMap<Long, ActionCollectionEntry<T>> slotForms;
    //Default entries if none specified
    private ActionCollectionEntry<T> defaultSlotForm = null;
    //filtering flags
    private boolean showDailySlots = true;
    private boolean showWeeklySlots = true;
    private boolean showMonthlySlots = true;
    private boolean showYearlySlots = true;
    private boolean showUniqueSlots = true;
    //Flag to ensure an empty grid is on
    boolean isGridOn = false;
    //Callback to the underlying presentation layer so as to bind misc. captions in a freshly drawn cell
    private CellRefreshHandler<T, HourMinuteInterface> delegatedCellRefreshHandler = null;

    @Override
    public Collection<T> getAllForms() {
        Collection<T> results = new ArrayList<T>();
        if (slotForms != null) {
            for (ActionCollectionEntry<T> slotForm : slotForms.values()) {
                results.add(slotForm.getItem());
            }
        }
        return results;
    }

    private void reinitGridDataParameters() {
        int numberOfSlotsWorthAWeekLong = 7 * 24 * 60;
        timeSlots = new GWTFormTimetableTimeSlot[numberOfSlotsWorthAWeekLong];
        for (int i = 0; i < numberOfSlotsWorthAWeekLong; i++) {
            timeSlots[i] = new GWTFormTimetableTimeSlot(); //initialize each individually
        }
        slotForms = new TreeMap<Long, ActionCollectionEntry<T>>();
        timeSlotBoundaries = new TreeMap<Long, GWTFormTimetableTimeSlotBoundaries>();
        timeSlotNum = 0;
    }

    private void reinitAdditionalGridDataParameters() {
        additionalHeaders = new ArrayList<String[]>();
        additionalLegends = new ArrayList<String[]>();
    }

    public GWTFormWeeklyTimetable(byte timezoneDifference, byte timescaleResolution) {
        super();

        setTimescaleResolution(timescaleResolution);
        setTimeShift(timezoneDifference);

        reinitGridDataParameters();
        reinitAdditionalGridDataParameters();

        widget = new FlexTable();
        //Custom CSS
        widget.setStyleName("GWTFormWeeklyTimetable");

        attachMainWidget(widget);
    }

    @Override
    public void setDefaultForm(ActionCollectionEntry<T> defaultForm) {
        this.defaultSlotForm = defaultForm;
    }

    @Override
    public void clearGrid() {
        widget.removeAllRows();
        isGridOn = false;
    }

    @Override
    public void removeAllCells() {
        reinitGridDataParameters();
    }

    private GWTVerticalPanelWithTag createCellWidget(ActionCollectionEntry<T> currentForm, short tag) {

        GWTVerticalPanelWithTag slotPanel = new GWTVerticalPanelWithTag(tag);

        //Now ask the underlying presentation layer to feed the widget some slots
        if (delegatedCellRefreshHandler != null) {
            delegatedCellRefreshHandler.onContentRefresh(
                    currentForm,
                    slotPanel.getContent());
        }

        return slotPanel;

    }

    private void updateCellWidgetCaption(GWTVerticalPanelWithTag cell, ActionCollectionEntry<T> currentForm, short startTimeInMinutePerDay, short endTimeInMinutePerDay) {
        //Now ask the underlying presentation layer to feed the widget some slots
        if (delegatedCellRefreshHandler != null) {
            delegatedCellRefreshHandler.onCaptionRefresh(
                    new JSHourMinute((short) (startTimeInMinutePerDay % (24 * 60))),
                    new JSHourMinute((short) (endTimeInMinutePerDay % (24 * 60))),
                    currentForm,
                    cell.getCaption());
        }
    }

    public static byte getStartingDayOfWeek() {
        return startingDayOfWeek;
    }

    protected String[] buildWeekDayNameHeader() {

        String[] days = new String[7];

        //Use the GWT internal locale library to know which day actually starts a week
        for (byte i = 0; i < 7; i++) {
            byte dayIdx = (byte) (i + getStartingDayOfWeek() < 8 ? i + getStartingDayOfWeek() : i + getStartingDayOfWeek() - 7);
            String dayLabel = null;
            switch (dayIdx) {
                case 1:
                    dayLabel = I18nTranslator.getInstance().monday();
                    break;
                case 2:
                    dayLabel = I18nTranslator.getInstance().tuesday();
                    break;
                case 3:
                    dayLabel = I18nTranslator.getInstance().wednesday();
                    break;
                case 4:
                    dayLabel = I18nTranslator.getInstance().thursday();
                    break;
                case 5:
                    dayLabel = I18nTranslator.getInstance().friday();
                    break;
                case 6:
                    dayLabel = I18nTranslator.getInstance().saturday();
                    break;
                case 7:
                    dayLabel = I18nTranslator.getInstance().sunday();
                    break;
            }
            days[i] = dayLabel;
        }

        return days;

    }

    protected void buildEmptyGrid() {
        //Build a void grid of 7 x (1440/timescaleResolution) elements

        clearGrid();

        int colOffset = additionalLegends.size() + 1;
        int rowOffset = 0;

        //Add additional headers first
        for (; rowOffset < additionalHeaders.size(); rowOffset++) {
            //Left padding
            for (int j = 0; j < colOffset; j++) {
                widget.setText(rowOffset, j, null);
            }
            //Add additional headers
            String[] currentHeader = additionalHeaders.get(rowOffset);
            for (int k = 0; k < 7 && k < currentHeader.length; k++) {
                widget.setText(rowOffset, k + colOffset, currentHeader[k]);
            }
            //Right padding
            for (int l = currentHeader.length; l < 7; l++) {
                widget.setText(rowOffset, l + colOffset, null);
            }

            //Custom CSS
            widget.getRowFormatter().setStyleName(rowOffset, "heading");
        }

        //Add basic header
        //Left padding
        for (int i = 0; i < colOffset; i++) {
            widget.setText(rowOffset, i, null);
            //Custom CSS
            widget.getColumnFormatter().setStyleName(i, "legend");

        }
        String[] basicHeader = buildWeekDayNameHeader();
        for (int j = 0; j < basicHeader.length; j++) {
            widget.setText(rowOffset, j + colOffset, basicHeader[j]);
        }
        //Custom CSS
        widget.getRowFormatter().setStyleName(rowOffset, "heading");


        ++rowOffset;
        //Make up the whole grid and prepend additional legends and the time
        int numberOfTimeSlots = (24 * 60) / timescaleResolution;
        for (int i = 0; i < numberOfTimeSlots; i++) {

            //Add additional legends
            int j = 0;
            for (; j < additionalLegends.size(); j++) {
                String[] currentLegend = additionalLegends.get(j);
                if (currentLegend.length > i) {
                    widget.setText(i + rowOffset, j, currentLegend[i]);
                }
            }

            //Add basic legend
            //Compute the time interval for this row
            String fromTime = (new JSHourMinute((short) (i * timescaleResolution))).getI18nFriendlyString();
            String toTime = (new JSHourMinute((short) ((i + 1) * timescaleResolution))).getI18nFriendlyString();
            widget.setText(i + rowOffset, j, fromTime + " - " + toTime);

        }

        isGridOn = true;

    }

    public boolean isShowDailySlots() {
        return showDailySlots;
    }

    public void setShowDailySlots(boolean showDailySlots) {
        this.showDailySlots = showDailySlots;
    }

    public boolean isShowMonthlySlots() {
        return showMonthlySlots;
    }

    public void setShowMonthlySlots(boolean showMonthlySlots) {
        this.showMonthlySlots = showMonthlySlots;
    }

    public boolean isShowUniqueSlots() {
        return showUniqueSlots;
    }

    public void setShowUniqueSlots(boolean showUniqueSlots) {
        this.showUniqueSlots = showUniqueSlots;
    }

    public boolean isShowWeeklySlots() {
        return showWeeklySlots;
    }

    public void setShowWeeklySlots(boolean showWeeklySlots) {
        this.showWeeklySlots = showWeeklySlots;
    }

    public boolean isShowYearlySlots() {
        return showYearlySlots;
    }

    public void setShowYearlySlots(boolean showYearlySlots) {
        this.showYearlySlots = showYearlySlots;
    }

    @Override
    public short getTimeShift() {
        return timeShift;
    }

    /**
     *
     * Shift the time slots by the specified number of hours It cannot be more
     * than 24*60 minutes 24*60 must also be a multiple of timeShift
     *
     * @param timeShift
     *
     */
    @Override
    public void setTimeShift(short timeShiftInMinutes) {
        if (timeShiftInMinutes <= (24 * 60) && (timeShiftInMinutes == 0 || (24 * 60) % timeShiftInMinutes == 0) && this.timeShift != timeShiftInMinutes) {
            this.timeShift = timeShiftInMinutes;
        }
    }

    @Override
    public int getTimescaleResolution() {
        return timescaleResolution;
    }

    /**
     *
     * Set the number of minutes spanning between each row of the timetable
     * 24*60 must be a multiple of this number
     *
     * @param timescaleResolution
     *
     */
    @Override
    public void setTimescaleResolution(byte timescaleResolutionInMinutes) {
        if ((timescaleResolutionInMinutes == 0 || (24 * 60) % timescaleResolutionInMinutes == 0) && this.timescaleResolution != timescaleResolutionInMinutes) {
            this.timescaleResolution = timescaleResolutionInMinutes;
        }
    }

    /**
     *
     * Retrieve the identifier of the time slot that has been clicked on Return
     * null if none is selected
     *
     * @param event
     * @return
     *
     */
    @Override
    public CellCoordinates getCurrentCellCoordinates(ClickEvent event) {
        HTMLTable.Cell clickedCell = null;
        if (event != null && (clickedCell = widget.getCellForEvent(event)) != null) {
            //TODO: The API doc. is not clear about cellIndex; I assume it's the cell index in a given row: must be thorougly tested
            //Warning: spanning cells do not count for columns! rows are ok though

            return new CellCoordinates(
                    clickedCell.getCellIndex() - getLegendColNumber(),
                    clickedCell.getRowIndex() - getHeaderRowNumber());
        }
        return null;
    }

    /**
     *
     * Get the form associated with the specified coordinates on the timetable
     * Return null if there is none
     *
     *
     */
    @Override
    public ActionCollectionEntry<T> getFormAt(CellCoordinates coordinates) {
        if (coordinates != null) {
            int currentRow = coordinates.getRowIndex() + getHeaderRowNumber();
            int currentCol = coordinates.getColIndex() + getLegendColNumber();
            if (coordinates.getRowIndex() > -1
                    && coordinates.getRowIndex() < widget.getRowCount()
                    && coordinates.getColIndex() > -1
                    && coordinates.getColIndex() < widget.getCellCount(currentRow)) {

                PluggableWidgetWithTagInterface w = ((PluggableWidgetWithTagInterface) widget.getWidget(
                        currentRow,
                        currentCol));
                if (w != null) {
                    short cellIndex = w.getTag();
                    //Convert the coordinates into a timeslot index
                    //int cellIndex = (coordinates.getColIndex() * 60 * 24) + (coordinates.getRowIndex() * timescaleResolution);

                    //Find out which slot has been be drawn as a new cell
                    GWTFormTimetableTimeSlot candidateSlots = timeSlots[cellIndex];
                    Long slotToDraw = _getSlotToDraw(
                            candidateSlots,
                            !isShowUniqueSlots(),
                            !isShowDailySlots(),
                            !isShowWeeklySlots(),
                            !isShowMonthlySlots(),
                            !isShowYearlySlots());

                    return slotForms.get(slotToDraw);
                }
            }
        }

        return null;
    }

    private Short getConflictingOldEndingTimeWithPrecedence(T oldForm, Short oldToBoundary, T newForm, Short newFromBoundary) {
        if (oldForm == null) {
            return newForm != null ? newFromBoundary : null;
        }
        if (newForm == null) {
            return oldForm != null ? oldToBoundary : null;
        }
        byte oldTypeSalientFactor = 0;
        byte newTypeSalientFactor = 0;
        if (oldForm instanceof RecurringTimetableSlotForm) {
            switch (((RecurringTimetableSlotForm) oldForm).getRecurringEvent()) {
                case yearly:
                    oldTypeSalientFactor = 1;
                    break;
                case monthly:
                    oldTypeSalientFactor = 2;
                    break;
                case weekly:
                    oldTypeSalientFactor = 3;
                    break;
                case daily:
                    oldTypeSalientFactor = 4;
            }
        }
        if (newForm instanceof RecurringTimetableSlotForm) {
            switch (((RecurringTimetableSlotForm) newForm).getRecurringEvent()) {
                case yearly:
                    newTypeSalientFactor = 1;
                    break;
                case monthly:
                    newTypeSalientFactor = 2;
                    break;
                case weekly:
                    newTypeSalientFactor = 3;
                    break;
                case daily:
                    newTypeSalientFactor = 4;
            }
        }
        if (newTypeSalientFactor == oldTypeSalientFactor) {
            if (oldToBoundary > newFromBoundary) {
                return newFromBoundary;
            } else {
                return oldToBoundary;
            }
        } else {
            if (newTypeSalientFactor < oldTypeSalientFactor) {
                return newFromBoundary;
            } else {
                return oldToBoundary;
            }
        }
    }

    private void _assignTimeSlot(T form, Long id, short i) {
        if (form instanceof UniqueTimetableSlotForm) {
            timeSlots[i].uniqueSlot = getSlotWithPrecedence(timeSlots[i].uniqueSlot, id);
        } else {
            switch (((RecurringTimetableSlotForm) form).getRecurringEvent()) {
                case daily:
                    timeSlots[i].dailySlot = getSlotWithPrecedence(timeSlots[i].dailySlot, id);
                    break;
                case weekly:
                    timeSlots[i].weeklySlot = getSlotWithPrecedence(timeSlots[i].weeklySlot, id);
                    break;
                case monthly:
                    timeSlots[i].monthlySlot = getSlotWithPrecedence(timeSlots[i].monthlySlot, id);
                    break;
                case yearly:
                    timeSlots[i].yearlySlot = getSlotWithPrecedence(timeSlots[i].yearlySlot, id);
                    break;
            }
        }
    }

    /**
     * Check whether a time slot can overwrite any existing one that share a
     * whole or part of the same time interval
     */
    private Long getSlotWithPrecedence(Long oldId, Long newId) {
        //If there is no previous slot then automatically fill in the new one; this newest one may be null, it is allowed
        if (oldId == null) {
            return newId;
        }
        ActionCollectionEntry<T> oldForm = slotForms.get(oldId);
        if (oldForm == null) {
            return newId;
        }
        //If the new one is null but not the previous one then return the old one
        if (newId == null) {
            return oldId;
        }
        ActionCollectionEntry<T> newForm = slotForms.get(newId);
        if (newForm == null) {
            return oldId;
        }
        //Otherwise, the slot that starts last has precedence; there could be no ex-aequo since it would violate database constraints
        if (oldForm.getItem().getRefId().getSchedulingTime().compareTo(newForm.getItem().getRefId().getSchedulingTime()) < 0) {
            return newId;
        } else {
            return oldId;
        }
    }

    /**
     *
     * Shift a day based on the standard Monday-to-Sunday week to a day based on
     * the locale preference for this grid The result is normed from 1 to 7
     *
     * @param dayOfWeek
     * @return
     *
     */
    private byte _getLocaleDayOfWeek(byte standardDayOfWeek) {
        if (standardDayOfWeek < 8 && standardDayOfWeek > 0) {
            int d = standardDayOfWeek - getStartingDayOfWeek();
            return (byte) ((d < 0) ? d + 8 : d + 1);
        } else {
            return 1;
        }
    }

    /**
     *
     * Add an item to the grid to display it, one must call refreshTimetable()
     * afterward: this is not an automatic event
     *
     * This method does not permit spanning cells from one end to the grid to
     * the other: fromDayOfWeek and toDayOfWeek may be out of bounds (i.e. lower
     * than 1 or greater than 7) but the start time cannot be greater than stop
     * time
     *
     *
     */
    /*public boolean addStaticCell(byte fromDayOfWeek, short fromMinutes, byte toDayOfWeek, short toMinutes, ActionCollectionEntry<T> form) {
     //Validation of parameters
     // minutes € [0..(24*60-1)]
     //If start time is greater than end time then we do not allow it to span over the end to the week to the next one
     if ((form == null)
     || (fromMinutes < 0 && fromMinutes > (24 * 60 - 1))
     || (toMinutes < 0 && toMinutes > (24 * 60 - 1))
     || (fromDayOfWeek > toDayOfWeek)
     || (fromDayOfWeek == toDayOfWeek && fromMinutes > toMinutes)) {
     return false;
     }

     //Shift the given days of the week according to the displayed hedaer grid
     fromDayOfWeek = _getLocaleDayOfWeek(fromDayOfWeek);
     toDayOfWeek = _getLocaleDayOfWeek(toDayOfWeek);

     //Store this timeSlot at the right location in the grid
     short startingSlotIndex =
     fromDayOfWeek < 1 ? 0 : (short) ((fromDayOfWeek - 1) * (24 * 60) + fromMinutes);
     short endingSlotIndex =
     toDayOfWeek > 7 ? ((7 * 24 * 60) - 1) : (short) ((toDayOfWeek - 1) * (24 * 60) + toMinutes);

     //the endingSlot must not be included in the interval: it's the maximum boundary
     endingSlotIndex--;

     //The refId of a timeSlot must not be null; void is reserved for "absence of slot"
     slotForms.put(form.getItem().getRefId(), form);

     for (short i = startingSlotIndex; i <= endingSlotIndex; i++) {
     _assignTimeSlot(form.getItem(), form.getItem().getRefId(), i);
     }

     return true;
     }*/
    public boolean addSpanningCell(byte dayOfWeek, short atMinute, ActionCollectionEntry<T> form) {
        return addSpanningCell(dayOfWeek, atMinute, dayOfWeek, atMinute, form);
    }

    /**
     *
     * Add an item to the grid to display it, one must call refreshTimetable()
     * afterward: this is not an automatic event
     *
     * This method permits spanning cells from one end to the grid to the other:
     * fromDayOfWeek and toDayOfWeek cannot be out of bounds (i.e. lower than 1
     * or greater than 7) but the start time can be greater than stop time
     * Beware, the end time is NON inclusive.
     *
     *
     */
    @Override
    public boolean addSpanningCell(byte fromDayOfWeek, short fromMinutes, byte toDayOfWeek, short toMinutes, ActionCollectionEntry<T> form) {

        //Shift the given days of the week according to the displayed hedaer grid
        fromDayOfWeek = _getLocaleDayOfWeek(fromDayOfWeek);
        toDayOfWeek = _getLocaleDayOfWeek(toDayOfWeek);

        return addSpanningCellNoLocale(fromDayOfWeek, fromMinutes, toDayOfWeek, toMinutes, form);

    }

    protected boolean addSpanningCellNoLocale(byte fromDayOfWeek, short fromMinutes, byte toDayOfWeek, short toMinutes, ActionCollectionEntry<T> form) {
        //Validation of parameters
        // week € [1..7]
        // minutes € [0..(24*60-1)]
        //If start time is greater than end time then we assume it spans over the end to the week to the next one
        if ((form == null)
                || (fromDayOfWeek == toDayOfWeek && fromMinutes == toMinutes)
                || (fromDayOfWeek < 1 && fromDayOfWeek > 7)
                || (fromMinutes < 0 && fromMinutes > (24 * 60 - 1))
                || (toDayOfWeek < 1 && toDayOfWeek > 8)
                || (toMinutes < 0 && toMinutes > (24 * 60 - 1))
                //Failsafe for the first day of the next week
                || (toDayOfWeek == 8 && toMinutes > 0) //Failsafe if length==0
                /*|| (fromDayOfWeek == toDayOfWeek && fromMinutes == toMinutes)*/) {
            return false;
        }

        //Store this timeSlot at the right location in the grid
        short startingSlotIndex = (short) ((fromDayOfWeek - 1) * (24 * 60) + fromMinutes);
        short endingSlotIndex = (short) ((toDayOfWeek - 1) * (24 * 60) + toMinutes);

        //The refId of a timeSlot must not be null; void is reserved for "absence of slot"
        timeSlotNum++;
        slotForms.put(timeSlotNum, form);
        timeSlotBoundaries.put(timeSlotNum, new GWTFormTimetableTimeSlotBoundaries(startingSlotIndex, endingSlotIndex));

        //the endingSlot must not be included in the interval: it's the maximum boundary
        endingSlotIndex--;

        if (endingSlotIndex < startingSlotIndex) {
            for (short i = 0; i <= endingSlotIndex; i++) {
                _assignTimeSlot(form.getItem(), timeSlotNum, i);
            }
            for (short j = startingSlotIndex; j < timeSlots.length; j++) {
                _assignTimeSlot(form.getItem(), timeSlotNum, j);
            }
        } else {
            for (short i = startingSlotIndex; i <= endingSlotIndex; i++) {
                _assignTimeSlot(form.getItem(), timeSlotNum, i);
            }
        }

        return true;
    }

    @Override
    public boolean addSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<T> timetableEntry) {

        if (timetableEntry == null) {
            return false;
        }

        if (timetableEntry.getItem() instanceof UniqueTimetableSlotForm) {
            return _addUniqueSpanningCell(localeDaysOfCurrentWeek, localeStartDayOfNextWeek, (ActionCollectionEntry<UniqueTimetableSlotForm>) timetableEntry);
        } else {
            return _addRecurringSpanningCell(localeDaysOfCurrentWeek, localeStartDayOfNextWeek, (ActionCollectionEntry<? extends RecurringTimetableSlotForm>) timetableEntry);
        }

    }

    private boolean _addUniqueSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<UniqueTimetableSlotForm> form) {
        JSYearMonthDayHourMinuteSecondMillisecond localeStartOfCurrentWeek = new JSYearMonthDayHourMinuteSecondMillisecond(localeDaysOfCurrentWeek[0]);
        JSYearMonthDayHourMinuteSecondMillisecond localeStartOfNextWeek = new JSYearMonthDayHourMinuteSecondMillisecond(localeStartDayOfNextWeek);
        byte fromDayIdx, toDayIdx;
        short fromMinuteIdx, toMinuteIdx;
        if (localeStartOfCurrentWeek.compareTo(form.getItem().getRefId().getSchedulingTime()) > 0) {
            fromDayIdx = localeStartOfCurrentWeek.getDayOfWeek();
            fromMinuteIdx = 0;
        } else {
            fromDayIdx = _getLocaleDayOfWeek(form.getItem().getRefId().getSchedulingTime().getDayOfWeek());
            fromMinuteIdx = form.getItem().getRefId().getSchedulingTime().getMinuteFromStartOfDay();
        }
        if (localeStartOfNextWeek.compareTo(form.getItem().getEndTime()) < 0) {
            toDayIdx = localeStartOfNextWeek.getDayOfWeek();
            toMinuteIdx = 0;
        } else {
            toDayIdx = _getLocaleDayOfWeek(form.getItem().getEndTime().getDayOfWeek());
            toMinuteIdx = form.getItem().getEndTime().getMinuteFromStartOfDay();
        }
        return addSpanningCellNoLocale(fromDayIdx, fromMinuteIdx, toDayIdx, toMinuteIdx, (ActionCollectionEntry<T>) form);
    }

    private boolean _addRecurringSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<? extends RecurringTimetableSlotForm> form) {
        //Failsafe; check if the timeslot is not meant to be played on any later week only
        if (localeStartDayOfNextWeek.compareTo(form.getItem().getRefId().getSchedulingTime()) <= 0) {
            return false;
        }

        switch (form.getItem().getRecurringEvent()) {
            case daily:
                return _addDailyRecurringSpanningCell(
                        localeDaysOfCurrentWeek,
                        localeStartDayOfNextWeek,
                        form);
            case weekly:
                return _addWeeklyRecurringSpanningCell(
                        localeDaysOfCurrentWeek,
                        localeStartDayOfNextWeek,
                        form);
            case monthly:
                return _addMonthlyRecurringSpanningCell(
                        localeDaysOfCurrentWeek,
                        localeStartDayOfNextWeek,
                        form);
            case yearly:
                return _addYearlyRecurringSpanningCell(
                        localeDaysOfCurrentWeek,
                        localeStartDayOfNextWeek,
                        form);
        }
        return false;
    }

    private boolean _addYearlyRecurringSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<? extends RecurringTimetableSlotForm> form) {
        JSYearMonthDayHourMinuteSecondMillisecond localeStartOfCurrentWeek = new JSYearMonthDayHourMinuteSecondMillisecond(localeDaysOfCurrentWeek[0]);
        JSYearMonthDayHourMinuteSecondMillisecond localeStartOfNextWeek = new JSYearMonthDayHourMinuteSecondMillisecond(localeStartDayOfNextWeek);
        byte fromDayIdx, toDayIdx;
        short fromMinuteIdx, toMinuteIdx;

        short currentFormDayOfYear = form.getItem().getRefId().getSchedulingTime().getDayOfYear();
        if (localeStartOfCurrentWeek.getDayOfYear() < localeStartOfNextWeek.getDayOfYear() && currentFormDayOfYear < localeStartOfCurrentWeek.getDayOfYear()) {
            fromDayIdx = localeStartOfCurrentWeek.getDayOfWeek();
            fromMinuteIdx = 0;
        } else {
            byte i;
            for (i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                if (localeDaysOfCurrentWeek[i].getDayOfYear() == currentFormDayOfYear) {
                    break;
                }
            }

            fromDayIdx = (byte) (i + 1);
            fromMinuteIdx = form.getItem().getRefId().getSchedulingTime().getMinuteFromStartOfDay();
        }

        currentFormDayOfYear = form.getItem().getEndTime().getDayOfYear();
        if (localeStartOfCurrentWeek.getDayOfYear() < localeStartOfNextWeek.getDayOfYear() && currentFormDayOfYear >= localeStartDayOfNextWeek.getDayOfYear()) {
            toDayIdx = localeStartOfNextWeek.getDayOfWeek();
            toMinuteIdx = 0;
        } else {
            byte i;
            for (i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                if (localeDaysOfCurrentWeek[i].getDayOfYear() == currentFormDayOfYear) {
                    break;
                }
            }

            toDayIdx = (byte) (i + 1);
            toMinuteIdx = form.getItem().getEndTime().getMinuteFromStartOfDay();
        }

        return addSpanningCellNoLocale(fromDayIdx, fromMinuteIdx, toDayIdx, toMinuteIdx, (ActionCollectionEntry<T>) form);
    }

    private boolean _addMonthlyRecurringSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<? extends RecurringTimetableSlotForm> form) {
        JSYearMonthDayHourMinuteSecondMillisecond localeStartOfCurrentWeek = new JSYearMonthDayHourMinuteSecondMillisecond(localeDaysOfCurrentWeek[0]);
        JSYearMonthDayHourMinuteSecondMillisecond localeStartOfNextWeek = new JSYearMonthDayHourMinuteSecondMillisecond(localeStartDayOfNextWeek);
        byte fromDayIdx, toDayIdx;
        short fromMinuteIdx, toMinuteIdx;

        byte currentFormDayOfMonth = form.getItem().getRefId().getSchedulingTime().getDayOfMonth();
        if (currentFormDayOfMonth < localeStartOfCurrentWeek.getDayOfMonth()) {
            fromDayIdx = localeStartOfCurrentWeek.getDayOfWeek();
            fromMinuteIdx = 0;
        } else {
            byte i;
            for (i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                if (localeDaysOfCurrentWeek[i].getDayOfMonth() == currentFormDayOfMonth) {
                    break;
                }
            }

            fromDayIdx = (byte) (i + 1);
            fromMinuteIdx = form.getItem().getRefId().getSchedulingTime().getMinuteFromStartOfDay();
        }

        currentFormDayOfMonth = form.getItem().getEndTime().getDayOfMonth();
        if (localeStartOfCurrentWeek.getDayOfMonth() < localeStartOfNextWeek.getDayOfMonth() && currentFormDayOfMonth >= localeStartDayOfNextWeek.getDayOfMonth()) {
            toDayIdx = localeStartOfNextWeek.getDayOfWeek();
            toMinuteIdx = 0;
        } else {
            byte i;
            for (i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                if (localeDaysOfCurrentWeek[i].getDayOfMonth() == currentFormDayOfMonth) {
                    break;
                }
            }

            toDayIdx = (byte) (i + 1);
            toMinuteIdx = form.getItem().getEndTime().getMinuteFromStartOfDay();
        }

        return addSpanningCellNoLocale(fromDayIdx, fromMinuteIdx, toDayIdx, toMinuteIdx, (ActionCollectionEntry<T>) form);
    }

    private boolean _addDailyRecurringSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<? extends RecurringTimetableSlotForm> form) {
        byte gridFromDayOfWeek = _getLocaleDayOfWeek(form.getItem().getRefId().getSchedulingTime().getDayOfWeek());
        byte gridToDayOfWeek = _getLocaleDayOfWeek(form.getItem().getEndTime().getDayOfWeek());

        boolean result = true;

        if (gridFromDayOfWeek == gridToDayOfWeek) {
            //this slot starts and ends within the same day

            //Clone the slot
            for (int i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                if (localeDaysOfCurrentWeek[i].compareTo(form.getItem().getRefId().getSchedulingTime()) >= 0
                        && (form.getItem().getDecommissioningTime() == null
                        || localeDaysOfCurrentWeek[i].compareTo(form.getItem().getDecommissioningTime()) < 0)) {

                    result = addSpanningCellNoLocale(
                            (byte) (i + 1),
                            form.getItem().getRefId().getSchedulingTime().getMinuteFromStartOfDay(),
                            (byte) (i + 1),
                            form.getItem().getEndTime().getMinuteFromStartOfDay(),
                            (ActionCollectionEntry<T>) form);
                    if (result == false) {
                        break;
                    }
                }
            }

        } else {
            //This slot starts and ends on two different days

            //First fill in slots from start to midnight
            //Clone the slot
            for (int i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                if (localeDaysOfCurrentWeek[i].compareTo(form.getItem().getRefId().getSchedulingTime()) >= 0
                        && (form.getItem().getDecommissioningTime() == null
                        || localeDaysOfCurrentWeek[i].compareTo(form.getItem().getDecommissioningTime()) < 0)) {

                    result = addSpanningCellNoLocale(
                            (byte) (i + 1),
                            form.getItem().getRefId().getSchedulingTime().getMinuteFromStartOfDay(),
                            i == (localeDaysOfCurrentWeek.length - 1) ? 1 : (byte) (i + 2),
                            (short) 0,
                            (ActionCollectionEntry<T>) form);
                    if (result == false) {
                        break;
                    }
                }
            }

            //then the remaining part of the slot, from midnight to the end
            //Clone the slot
            if (result) //Clone the slot
            {
                for (int i = 0; i < localeDaysOfCurrentWeek.length; i++) {
                    if (localeDaysOfCurrentWeek[i].compareTo(form.getItem().getRefId().getSchedulingTime()) > 0
                            && (form.getItem().getDecommissioningTime() == null
                            || localeDaysOfCurrentWeek[i].compareTo(form.getItem().getDecommissioningTime()) <= 0)) {

                        result = addSpanningCellNoLocale(
                                (byte) (i + 1),
                                (short) 0,
                                (byte) (i + 1),
                                form.getItem().getEndTime().getMinuteFromStartOfDay(),
                                (ActionCollectionEntry<T>) form);
                        if (result == false) {
                            break;
                        }
                    }
                }
            }

        }
        return result;
    }

    private boolean _addWeeklyRecurringSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<? extends RecurringTimetableSlotForm> form) {

        //Convert the current boundaries of the recurring slot into fully fledged, translated dates
        //Days of current week go from 0 to 6 (either idxed from Sun or Mon, or maybe others)
        //Week days from YearMonthDay children are indexed 1 to 7 (always indxed from Mon)
        byte gridFromDayOfWeek = _getLocaleDayOfWeek(form.getItem().getRefId().getSchedulingTime().getDayOfWeek());
        byte gridToDayOfWeek = _getLocaleDayOfWeek(form.getItem().getEndTime().getDayOfWeek());
        JSYearMonthDayHourMinuteSecondMillisecond gridFrom = new JSYearMonthDayHourMinuteSecondMillisecond(
                localeDaysOfCurrentWeek[gridFromDayOfWeek - 1],
                (HourMinuteInterface) form.getItem().getRefId().getSchedulingTime(),
                (SecondMillisecondInterface) form.getItem().getRefId().getSchedulingTime());
        JSYearMonthDayHourMinuteSecondMillisecond gridTo = new JSYearMonthDayHourMinuteSecondMillisecond(
                localeDaysOfCurrentWeek[gridToDayOfWeek - 1],
                (HourMinuteInterface) form.getItem().getEndTime(),
                (SecondMillisecondInterface) form.getItem().getEndTime());

        if (gridFrom.compareTo(gridTo) < 0) {
            //this slot starts and ends within the current week

            //Create the slot for this week
            return addSpanningCellNoLocale(
                    gridFromDayOfWeek,
                    gridFrom.getMinuteFromStartOfDay(),
                    gridToDayOfWeek,
                    gridTo.getMinuteFromStartOfDay(),
                    (ActionCollectionEntry<T>) form);

        } else {
            //This slot spans over the grid
            //Two slots can be drawn at most
            boolean result = true;

            //First determine if the slot starting this week has to be displayed
            //This slot might have been retrieved only because the part that started last week was still not decommisioned while the part that starts this week is not valid though
            if (form.getItem().getDecommissioningTime() == null || gridFrom.compareTo(form.getItem().getDecommissioningTime()) < 0) {
                if (gridFromDayOfWeek != 8 && gridFrom.getMinuteFromStartOfDay() >= 0) {
                    result = addSpanningCellNoLocale(
                            gridFromDayOfWeek,
                            gridFrom.getMinuteFromStartOfDay(),
                            (byte) 8, //First day of next week
                            (short) 0,
                            (ActionCollectionEntry<T>) form);
                }
            }
            //Then the previous one
            if (result && localeDaysOfCurrentWeek[0].compareTo(form.getItem().getRefId().getSchedulingTime()) > 0) {
                if (gridToDayOfWeek != 1 && gridTo.getMinuteFromStartOfDay() >= 0) {
                    result = addSpanningCellNoLocale(
                            (byte) 1, //First day of current week
                            (short) 0,
                            gridToDayOfWeek,
                            gridTo.getMinuteFromStartOfDay(),
                            (ActionCollectionEntry<T>) form);
                }
            }

            return result;
        }

    }

    /**
     *
     * Get the number of rows located at the top and that contains legends (i.e.
     * no timetable data)
     *
     * @return
     *
     */
    @Override
    public int getHeaderRowNumber() {
        return isGridOn ? additionalHeaders.size() + 1 : 0;
    }

    /**
     *
     * Get the number of columns located on the far left side of the grid and
     * that contains legends (i.e. no timetable data)
     *
     * @return
     *
     */
    @Override
    public int getLegendColNumber() {
        return isGridOn ? additionalLegends.size() + 1 : 0;
    }

    @Override
    public void clean() {
        super.clean();
        clearGrid();
        reinitGridDataParameters();
        reinitAdditionalGridDataParameters();
    }

    public boolean prependHeaderRow(
            String mondayCaption,
            String tuesdayCaption,
            String wednesdayCaption,
            String thursdayCaption,
            String fridayCaption,
            String saturdayCaption,
            String sundayCaption) {

        String[] header = new String[7];
        header[0] = mondayCaption;
        header[1] = tuesdayCaption;
        header[2] = wednesdayCaption;
        header[3] = thursdayCaption;
        header[4] = fridayCaption;
        header[5] = saturdayCaption;
        header[6] = sundayCaption;
        additionalHeaders.add(header);

        return true;
    }

    public boolean setHeaderRow(
            int headerIndex,
            String mondayCaption,
            String tuesdayCaption,
            String wednesdayCaption,
            String thursdayCaption,
            String fridayCaption,
            String saturdayCaption,
            String sundayCaption) {
        if (headerIndex >= 0 && headerIndex < additionalHeaders.size()) {
            String[] newHeader = new String[7];
            newHeader[0] = mondayCaption;
            newHeader[1] = tuesdayCaption;
            newHeader[2] = wednesdayCaption;
            newHeader[3] = thursdayCaption;
            newHeader[4] = fridayCaption;
            newHeader[5] = saturdayCaption;
            newHeader[6] = sundayCaption;
            additionalHeaders.set(headerIndex, newHeader);
            return true;
        }
        return false;
    }

    public boolean removeHeaderRow(int headerIndex) {
        if (headerIndex >= 0 && headerIndex < additionalHeaders.size()) {
            additionalHeaders.remove(headerIndex);
            return true;
        }
        return false;
    }

    @Override
    public int getRowCount() {
        return widget.getRowCount() - getHeaderRowNumber();
    }

    protected int getCellCount(int row) {
        return widget.getCellCount(row) - getLegendColNumber();
    }

    protected void refreshTimetable() {
        buildEmptyGrid();
        if (timeSlots != null) {
            _refreshTimetable(
                    timeSlots,
                    !showUniqueSlots,
                    !showDailySlots,
                    !showWeeklySlots,
                    !showMonthlySlots,
                    !showYearlySlots);
        }
    }

    private Long _getSlotToDraw(
            GWTFormTimetableTimeSlot candidateSlots,
            boolean filterOutUniqueSlots,
            boolean filterOutDailySlots,
            boolean filterOutWeeklySlots,
            boolean filterOutMonthlySlots,
            boolean filterOutYearlySlots) {
        //Find which slot should be drawn as a new cell
        Long slotToDraw = null;
        if (candidateSlots != null) {
            //There is at least one condidate to draw
            //Find the only one to draw among all candidates according to the following rule:
            // Unique > Yearly > Monthly > Weekly > Daily
            //Apply filtering out flags if available too
            if (!filterOutUniqueSlots && candidateSlots.uniqueSlot != null) {
                slotToDraw = candidateSlots.uniqueSlot;
            } else if (!filterOutYearlySlots && candidateSlots.yearlySlot != null) {
                slotToDraw = candidateSlots.yearlySlot;
            } else if (!filterOutMonthlySlots && candidateSlots.monthlySlot != null) {
                slotToDraw = candidateSlots.monthlySlot;
            } else if (!filterOutWeeklySlots && candidateSlots.weeklySlot != null) {
                slotToDraw = candidateSlots.weeklySlot;
            } else if (!filterOutDailySlots && candidateSlots.dailySlot != null) {
                slotToDraw = candidateSlots.dailySlot;
            }

        }
        return slotToDraw;
    }

    private void _refreshTimetable(
            GWTFormTimetableTimeSlot[] timeSlots,
            boolean filterOutUniqueSlots,
            boolean filterOutDailySlots,
            boolean filterOutWeeklySlots,
            boolean filterOutMonthlySlots,
            boolean filterOutYearlySlots) {

        //Variables for updting the previous cell
        GWTVerticalPanelWithTag previousCell = null;
        Short previousStartTime = null;
        Short previousEndTime = null;

        //CSS colours scheme for cells, remember programmes already drawn and assign a new colour if seen for the frst time, otherwise use the one specifed by the map
        TreeMap<String, Byte> cellColourIndexesForProgrammes = new TreeMap<String, Byte>();
        byte currentNewCellColourIndexForProgrammes = 1; //Goes from 1 to 10;

        //A timetable contains exactly 7 * (24 * 60) time slots, i.e 10080 slots
        //24*60 must be a multiple of timescaleResolution and timeShift

        short numOfSpannedRows = 0;
        //TimetableSlotFormID previousId = null;
        ActionCollectionEntry<T> previousSlot = null;
        int spannedRowIndex = -1;
        short spannedColumnIndex = -1;
        //Logical flag to force the reset of spanning cells (it happens every new day)
        boolean forceSpanningReset = true;

        //Array that keeps track of the number of cells physical created for each row
        //Values are used to compute the actual column index of a new widget to create
        //Day numbers cannot be just used as column indexes because of potential spanning rows
        byte[] numOfWidgetsPerRow = new byte[(24 * 60) / timescaleResolution];

        //Timeshifting
        short shift = (short) (timeShift >= 0 ? timeShift : (7 * 24 * 60) + timeShift);
        short i_shifted;

        for (short i = 0; i < (7 * 24 * 60); i += timescaleResolution) {

            //Apply timeshift
            i_shifted = (short) ((i + shift) % (7 * 24 * 60));

            //A new day happens every 1440 slots
            //Reset spanning parameters
            if (i % (24 * 60) == 0) {

                numOfSpannedRows = 1;
                spannedColumnIndex = -1;
                spannedRowIndex = -1;
                //Force previousId to null and reset possible spanning between different days
                forceSpanningReset = true;
            }

            //Find which slot should be drawn as a new cell
            GWTFormTimetableTimeSlot candidateSlots = timeSlots[i_shifted];
            Long slotToDraw = _getSlotToDraw(
                    candidateSlots,
                    filterOutUniqueSlots,
                    filterOutDailySlots,
                    filterOutWeeklySlots,
                    filterOutMonthlySlots,
                    filterOutYearlySlots);
            ActionCollectionEntry<T> currentSlot = null;
            if (slotToDraw != null) {
                currentSlot = slotForms.get(slotToDraw);
            }

            //Draw the appropriate cell
            if (forceSpanningReset || currentSlot != previousSlot) {
                //This slot contains different data than the previous visited one

                //Reset the forceSpanning flag if needed
                if (forceSpanningReset) {
                    forceSpanningReset = false;
                    previousStartTime = null;
                }

                //Create a new slot at this location
                //byte dayIndex = (byte) (i / (24 * 60));
                short minutesIndex = (short) (i % (24 * 60));
                int rowIndex = (minutesIndex / timescaleResolution);

                if (slotToDraw == null) {
                    GWTVerticalPanelWithTag currentCell = createCellWidget(
                            defaultSlotForm,
                            i);
                    widget.setWidget(
                            rowIndex + getHeaderRowNumber(),
                            numOfWidgetsPerRow[rowIndex] + getLegendColNumber(),
                            currentCell);

                    //Update previous cell
                    if (previousCell != null && previousSlot != null) {
                        updateCellWidgetCaption(
                                previousCell,
                                previousSlot,
                                previousStartTime,
                                previousEndTime);
                    }
                    previousCell = null;
                    previousStartTime = null;

                    //Update ID
                    previousSlot = null;
                    previousEndTime = null;
                    //Custom CSS
                    widget.getCellFormatter().setStyleName(rowIndex + getHeaderRowNumber(), numOfWidgetsPerRow[rowIndex] + getLegendColNumber(), "cell");
                } else {
                    GWTVerticalPanelWithTag currentCell = createCellWidget(
                            currentSlot,
                            i);
                    widget.setWidget(
                            rowIndex + getHeaderRowNumber(),
                            numOfWidgetsPerRow[rowIndex] + getLegendColNumber(),
                            currentCell);

                    //Update previous cell
                    GWTFormTimetableTimeSlotBoundaries currentSlotBoundaries = timeSlotBoundaries.get(slotToDraw);
                    if (previousSlot == null) {
                        if (rowIndex == 0) {
                            //previousStartTime = (currentSlotBoundaries.getFrom() < i) ? 0 : currentSlotBoundaries.getFrom();
                            previousStartTime = 0;
                        } else {
                            previousStartTime = currentSlotBoundaries.getFrom();
                        }
                    } else {
                        Short _previousEndTime = getConflictingOldEndingTimeWithPrecedence(previousSlot.getItem(), previousEndTime, currentSlot.getItem(), currentSlotBoundaries.getFrom());
                        if (previousCell != null && previousSlot != null) {
                            updateCellWidgetCaption(
                                    previousCell,
                                    previousSlot,
                                    previousStartTime,
                                    _previousEndTime);
                        }
                        previousStartTime = (short) Math.max(_previousEndTime, currentSlotBoundaries.getFrom());
                    }
                    previousCell = currentCell;

                    //Update ID
                    previousSlot = currentSlot;
                    previousEndTime = currentSlotBoundaries.getMax();
                    //Custom CSS
                    widget.getCellFormatter().setStyleName(rowIndex + getHeaderRowNumber(), numOfWidgetsPerRow[rowIndex] + getLegendColNumber(), "cell");
                    //Custom CSS if enabled of disabled
                    //Both check if a playlist is bound to the slot and if it has not been explictly disabled
                    widget.getCellFormatter().addStyleName(
                            rowIndex + getHeaderRowNumber(),//
                            numOfWidgetsPerRow[rowIndex] + getLegendColNumber(),//
                            currentSlot.getItem().getRefId().isEnabled()//
                            && currentSlot.getItem().getPlaylistId() != null//
                            ? "cellEnabled"//
                            : "cellDisabled");

                    //Add a specific style if the slot is now playing
                    if (currentSlot.getItem().isNowPlaying()) {
                        widget.getCellFormatter().addStyleName(
                                rowIndex + getHeaderRowNumber(),//
                                numOfWidgetsPerRow[rowIndex] + getLegendColNumber(),//
                                "cellOnAir");
                    }

                    //Add a style specific to its programme
                    String currentProgrammeId = currentSlot.getItem().getProgrammeId().getItem();
                    Byte existingIndex = cellColourIndexesForProgrammes.get(currentProgrammeId);
                    if (existingIndex != null) {
                        widget.getCellFormatter().addStyleName(rowIndex + getHeaderRowNumber(), numOfWidgetsPerRow[rowIndex] + getLegendColNumber(), "cell" + existingIndex);
                    } else {
                        cellColourIndexesForProgrammes.put(currentProgrammeId, currentNewCellColourIndexForProgrammes);
                        widget.getCellFormatter().addStyleName(rowIndex + getHeaderRowNumber(), numOfWidgetsPerRow[rowIndex] + getLegendColNumber(), "cell" + currentNewCellColourIndexForProgrammes);
                        //update the new available colour index
                        if (currentNewCellColourIndexForProgrammes == 10) {
                            currentNewCellColourIndexForProgrammes = 1;
                        } else {
                            currentNewCellColourIndexForProgrammes++;
                        }
                    }

                }

                //And spanning rows for the previous slot with a different ID
                if (numOfSpannedRows > 1
                        && spannedColumnIndex > -1 && spannedRowIndex > -1) {
                    widget.getFlexCellFormatter().setRowSpan(
                            spannedRowIndex + getHeaderRowNumber(),
                            spannedColumnIndex + getLegendColNumber(),
                            numOfSpannedRows);
                }

                numOfSpannedRows = 1;
                spannedColumnIndex = numOfWidgetsPerRow[rowIndex];
                spannedRowIndex = rowIndex;
                numOfWidgetsPerRow[rowIndex]++;
            } else //Same ID, different slot: update the number of rows to span
            {
                numOfSpannedRows++;
            }


            //Force the drawing of the remaining spanning cells if required because the flag will be reset on the next day
            if (((i + timescaleResolution) % (24 * 60) == 0)) {
                if (previousCell != null && previousSlot != null) {
                    //Short currentEndTime = (short) Math.min(i + timescaleResolution, previousEndTime);
                    updateCellWidgetCaption(
                            previousCell,
                            previousSlot,
                            previousStartTime,
                            //currentEndTime);
                            (short)0);
                }
                previousCell = null;
                previousStartTime = null;
                previousSlot = null;
                previousEndTime = null;
                if (numOfSpannedRows > 1 && spannedColumnIndex > -1 && spannedRowIndex > -1) {
                    //Make the cell located numOfSpannedRows rows above a spanning cell
                    widget.getFlexCellFormatter().setRowSpan(
                            spannedRowIndex + getHeaderRowNumber(),
                            spannedColumnIndex + getLegendColNumber(),
                            numOfSpannedRows);
                }
            }

        }

    }

    @Override
    public void refresh() {
        refreshTimetable();
    }

    @Override
    public void setCellRefreshHandler(CellRefreshHandler<T, HourMinuteInterface> handler) {
        delegatedCellRefreshHandler = handler;
    }
}
