/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class GWTGrid<E extends Widget> extends GWTExtendedWidget implements GenericGridInterface<E> {

    protected FlexTable widget = null;

    public GWTGrid() {
        super();
        widget = new FlexTable();
        //Custom CSS
        widget.setStyleName("GWTGrid");
        attachMainWidget(widget);
    }

    @Override
    public void clean() {
        super.clean();
        widget.removeAllRows();
    }

    @Override
    public int getRowCount() {
        return widget.getRowCount();
    }

    @Override
    public int getCellCount(int row) {
        return widget.getCellCount(row);
    }

    @Override
    public void insertRowAt(int beforeRow, E... widgets) {
        if (beforeRow >= 0 && beforeRow <= widget.getRowCount()) {
            int colIndex = 0;
            widget.insertRow(beforeRow);
            for (E localWidget : widgets) {
                this.widget.setWidget(beforeRow, colIndex++, localWidget);
            }
        }
    }
    @Override
    public void insertRowAt(int beforeRow, E localWidget) {
        if (beforeRow >= 0 && beforeRow <= widget.getRowCount()) {
            int colIndex = 1;
            widget.insertRow(beforeRow);
            this.widget.setWidget(beforeRow, colIndex, localWidget);
        }
    }

    @Override
    public void addRow(E... widgets) {
        int extraRow = widget.getRowCount();
        int colIndex = 0;
        for (E localWidget : widgets) {
            this.widget.setWidget(extraRow, colIndex++, localWidget);
        }
    }
    @Override
    public void addRow(E localWidget) {
        int extraRow = widget.getRowCount();
        int colIndex = 1;
        this.widget.setWidget(extraRow, colIndex, localWidget);
    }

    @Override
    public E getWidgetAt(int row, int column) {
        if (row < widget.getRowCount() && column < widget.getCellCount(row)) {
            return (E) widget.getWidget(row, column);
        } else {
            return null;
        }
    }

    @Override
    public void setWidgetAt(int row, int column, E widget) {
        if (row < this.widget.getRowCount() && column < this.widget.getCellCount(row)) {
            //Make sure that if widget is null the cell will be properly left void
            //This method should be throw an Exception since it should only be used on pre-existing cells
            if (widget != null) {
                this.widget.setWidget(row, column, widget);
            } else {
                this.widget.clearCell(row, column);
            }
        }
    }

    @Override
    public int getCurrentRow(ClickEvent event) {
        HTMLTable.Cell clickedCell;
        if (event != null && (clickedCell = widget.getCellForEvent(event)) != null) {
            return clickedCell.getRowIndex();
        }
        return -1;
    }

    @Override
    public boolean removeRow(int row) {
        if (row < widget.getRowCount() && row > -1) {
            widget.removeRow(row);
            return true;
        }
        return false;
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler ch) {
        return widget.addClickHandler(ch);
    }
}
