/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.HTML;

/**
 *
 * @author okay_awright
 **/
public class GWTHTML extends GWTExtendedWidget implements GenericLabelInterface {

    protected HTML widget;

    public GWTHTML(String html) {
        super();
        widget = new HTML();
        //Custom CSS
        widget.setStyleName("GWTHTML");
        setCaption(html);
        attachMainWidget(widget);
    }

    @Override
    public void setCaption(String text) {
        widget.setHTML(text);
    }

    @Override
    public String getCaption() {
        return widget.getHTML();
    }

}
