/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import biz.ddcr.shampoo.client.helper.resource.ResourceResolver;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;

/**
 *
 * @author okay_awright
 **/
public class GWTImage extends GWTExtendedWidget implements GenericImageInterface {

    protected final Image widget;

    protected void setDefaultCSSStyle() {
        //Custom CSS
        widget.setStyleName("GWTImage");
    }
    protected void setNullCSSStyle() {
        //Custom CSS
        widget.setStyleName("GWTUnknown");
    }

    public GWTImage() {
        this((HasCoverArtInterface)null);
    }

    public GWTImage(String url) {
        super();
        widget = new Image();
        //Custom CSS
        setDefaultCSSStyle();
        setImage(url);
        attachMainWidget(widget);
    }

    public GWTImage(HasCoverArtInterface form) {
        super();
        widget = new Image();
        //Custom CSS
        setDefaultCSSStyle();
        setImage(form);
        attachMainWidget(widget);
    }

    @Override
    public void setImage(String url) {
        widget.setUrl(url);
    }

    @Override
    public void setImage(HasCoverArtInterface form) {
        if (form !=null) {
            setImage(form.getCoverArtDownloadURL());
            setDefaultCSSStyle();
        } else {
            clean();
        }
    }

    @Override
    public int getHeight() {
        return widget.getHeight();
    }

    @Override
    public int getWidth() {
        return widget.getWidth();
    }

    @Override
    public void clean() {
        super.clean();
        setImage(GWT.getModuleBaseURL() + ResourceResolver.getInstance().InvisibleGetRelativeEndpoint());
        setNullCSSStyle();
    }
    
}
