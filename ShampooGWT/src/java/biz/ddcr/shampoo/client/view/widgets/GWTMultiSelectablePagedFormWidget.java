/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author okay_awright
 *
 */
public abstract class GWTMultiSelectablePagedFormWidget<T, U, V extends Widget> extends GWTPagedFormWidget<T, U, V> {

    public GWTMultiSelectablePagedFormWidget(V widget) {
        super(widget);
    }

    protected Collection<U> getVisibleSelectedValues() {
        Collection<U> results = null;
        for (int i = 0; i < getVisibleItemCount(); i++) {
            if (isVisibleItemSelected(i)) {
                if (results==null) results = new ArrayList<U>();
                PagedItem<T,U> item = items.get(i + firstVisibleItemOffset);
                if (item!=null) results.add(item.getValue());
            }
        }
        return results;
    }
    protected Collection<PagedItem<T,U>> getVisibleSelectedItems() {
        Collection<PagedItem<T,U>> results = null;
        for (int i = 0; i < getVisibleItemCount(); i++) {
            if (isVisibleItemSelected(i)) {
                if (results==null) results = new ArrayList<PagedItem<T,U>>();
                PagedItem<T,U> item = items.get(i + firstVisibleItemOffset);
                if (item!=null) results.add(item);
            }
        }
        return results;
    }
    protected abstract boolean isVisibleItemSelected(int index);
    
}
