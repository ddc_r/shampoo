/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.view.widgets.GenericMyVoteBoxInterface.VoteChangeHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;

/**
 *
 * @author okay_awright
 **/
public class GWTMyVoteBox extends GWTExtendedWidget implements GenericMyVoteBoxInterface<String> {

    final private String songID;
    final private GWTNullableFormDropDownListBox<VOTE> myRatingListBox;

    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private VoteChangeHandler<String> delegatedVoteChange = null;

    final private ChangeHandler changeHandler = new ChangeHandler() {

        @Override
        public void onChange(ChangeEvent event) {
            if (delegatedVoteChange!=null)
                delegatedVoteChange.onSet(GWTMyVoteBox.this);
        }
    };

    public GWTMyVoteBox(String songID, VoteChangeHandler<String> voteChangeHandler) {
        this(songID);
        setVoteChangeHandler(voteChangeHandler);
        if (delegatedVoteChange!=null)
                delegatedVoteChange.onGet(GWTMyVoteBox.this);
    }
    public GWTMyVoteBox(String songID, VOTE myVote) {
        this(songID);
        setMyVote(myVote);
    }
    protected GWTMyVoteBox(String songID) {
        super();
        this.songID = songID;
        myRatingListBox = new GWTNullableFormDropDownListBox<VOTE>();
        myRatingListBox.addChangeHandler(changeHandler);
        buildRatingListBox();

        attachMainWidget(myRatingListBox);
    }

    private void buildRatingListBox() {
        myRatingListBox.addItem("", null);
        for (VOTE rating : VOTE.values())
            myRatingListBox.addItem(rating.getI18nFriendlyString(), rating);
    }

    @Override
    public String getSongID() {
        return songID;
    }

    @Override
    public void setMyVote(VOTE myVote) {
        myRatingListBox.setSelectedValue(myVote);
    }

    @Override
    public VOTE getMyVote() {
        return myRatingListBox.getSelectedValue();
    }

    @Override
    public void setEnabled(boolean enabled) {
        myRatingListBox.setEnabled(enabled);
    }

    @Override
    public void clean() {
        super.clean();
        setMyVote(null);
    }

    @Override
    public void setVoteChangeHandler(VoteChangeHandler<String> handler) {
        delegatedVoteChange = handler;
    }

}
