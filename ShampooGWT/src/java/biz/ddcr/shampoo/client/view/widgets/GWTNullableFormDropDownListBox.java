/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import java.util.Collection;

/**
 *
 * @author okay_awright
 *
 */
public class GWTNullableFormDropDownListBox<T> extends GWTPagedFormDropDownListBox<T> {

    public GWTNullableFormDropDownListBox() {
        super();
    }
    
    @Override
    protected boolean addPagedItem(String key, T value) {
        return super.addPagedItem(true, key, value);
    }

    @Override
    protected boolean addPagedItems(Collection<PagedItem<String, T>> keyValues) {
        return super.addPagedItems(true, keyValues);
    }

    @Override
    protected boolean removePagedItem(String key, T value) {
        return super.removePagedItem(true, key, value);
    }

    @Override
    protected boolean removePagedItems(Collection<PagedItem<String, T>> keyValues) {
        return super.removePagedItems(true, keyValues);
    }
}
