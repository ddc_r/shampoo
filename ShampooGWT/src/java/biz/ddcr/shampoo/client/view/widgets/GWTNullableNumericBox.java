/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

/**
 *
 * @author okay_awright
 **/
public class GWTNullableNumericBox extends GWTNumericBox {

    public GWTNullableNumericBox(Integer defaultValue, int minValue, int maxValue) {
        super(defaultValue, minValue, maxValue);
    }

    @Override
    public void setDefaultValue(Integer defaultValue) {
        if (defaultValue == null || (defaultValue <= this.maxValue && defaultValue >= this.minValue)) {
            this.defaultValue = defaultValue;
        }
    }

    @Override
    public Integer getValue() {
        try {
            if (widget.getText()!=null && widget.getText().length()!=0)
                return Integer.decode(widget.getText());
            else
                return null;
        } catch (NumberFormatException e) {
            //Do nothing
        }
        return this.currentValue;
    }

    @Override
    protected boolean setValue(String value) {
        try {
            Integer intFromText = (value == null || value.length()==0) ? null : Integer.decode(value);
            return setValue(intFromText);
        } catch (NumberFormatException e) {
            //Do nothing
        }
        return false;
    }

    @Override
    public boolean setValue(Integer value) {
        if (value == null) {

            widget.setText("");
            this.currentValue = null;
            return true;

        } else if (value <= this.maxValue && value >= this.minValue) {

            try {

                widget.setText(Integer.toString(value));
                this.currentValue = value;
                return true;

            } catch (NumberFormatException e) {
                //Do nothing
            }
        }
        return false;
    }
    
}
