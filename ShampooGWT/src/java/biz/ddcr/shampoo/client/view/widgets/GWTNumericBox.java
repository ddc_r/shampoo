/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.TextBox;

/**
 *
 * @author okay_awright
 **/
public class GWTNumericBox extends GWTExtendedWidget implements GenericNumericBoxInterface<Integer> {

    /**
     * Flag whether a non-printing keyboard key during an onDown() event:
     * deadNonPrinting is a key that has no influence on a text,
     * liveNonPrinting is a special case for delete and backspace,
     * printing is a key that is visible in a text
     **/
    private static enum KEY_TYPE {

        deadNonPrinting,
        deleteForwardNonPrinting,
        deleteBackwardNonPrinting,
        printing
    };
    final protected TextBox widget;
    protected Integer currentValue;
    protected Integer defaultValue;
    protected int maxValue;
    protected int minValue;
    private KEY_TYPE pressedKeyType = null;
    final private KeyDownHandler keyDownHandler = new KeyDownHandler() {

        @Override
        public void onKeyDown(KeyDownEvent event) {
            switch (event.getNativeKeyCode()) {
                case KeyCodes.KEY_ALT:
                case KeyCodes.KEY_CTRL:
                case KeyCodes.KEY_DOWN:
                case KeyCodes.KEY_END:
                case KeyCodes.KEY_ENTER:
                case KeyCodes.KEY_ESCAPE:
                case KeyCodes.KEY_HOME:
                case KeyCodes.KEY_LEFT:
                case KeyCodes.KEY_PAGEDOWN:
                case KeyCodes.KEY_PAGEUP:
                case KeyCodes.KEY_RIGHT:
                case KeyCodes.KEY_SHIFT:
                case KeyCodes.KEY_TAB:
                case KeyCodes.KEY_UP:
                    pressedKeyType = KEY_TYPE.deadNonPrinting;
                    break;
                case KeyCodes.KEY_BACKSPACE:
                    pressedKeyType = KEY_TYPE.deleteBackwardNonPrinting;
                    break;
                case KeyCodes.KEY_DELETE:
                    pressedKeyType = KEY_TYPE.deleteForwardNonPrinting;
                    break;
                default:
                    pressedKeyType = KEY_TYPE.printing;
                    break;
            }
        }
    };
    private KeyPressHandler keyPressHandler = new KeyPressHandler() {

        @Override
        public void onKeyPress(KeyPressEvent event) {

            if (pressedKeyType != KEY_TYPE.deadNonPrinting) {

                int index = widget.getCursorPos();
                int newIndex = index;
                String previousText = widget.getText();
                String newText = null;

                if (pressedKeyType == KEY_TYPE.printing) {
                    if (widget.getSelectionLength() > 0) {
                        newText = previousText.substring(0, index)
                                + event.getCharCode()
                                + previousText.substring(index + widget.getSelectionLength(), previousText.length());
                    } else {
                        newText = previousText.substring(0, index) + event.getCharCode()
                                + previousText.substring(index, previousText.length());
                    }
                    newIndex++;
                } else if (pressedKeyType == KEY_TYPE.deleteForwardNonPrinting) {
                    if (widget.getSelectionLength() > 0) {
                        newText = previousText.substring(0, index)
                                + previousText.substring(index + widget.getSelectionLength(), previousText.length());
                    } else {
                        newText = previousText.substring(0, index);
                        if (index < previousText.length()) {
                            newText += previousText.substring(index + 1, previousText.length());
                        }
                    }
                } else if (pressedKeyType == KEY_TYPE.deleteBackwardNonPrinting) {
                    if (widget.getSelectionLength() > 0) {
                        newText = previousText.substring(0, index)
                                + previousText.substring(index + widget.getSelectionLength(), previousText.length());
                    } else {
                        newText = previousText.substring(index, previousText.length());
                        if (index > 0) {
                            newText = previousText.substring(0, index - 1) + newText;
                            newIndex--;
                        }
                    }
                }

                //whatever happens drop the key that has been pressed
                widget.cancelKey();

                if (setValue(newText)) //Put the cursor at the previous index + the difference between the previous and the new text
                {
                    widget.setCursorPos(newIndex);
                }

                //Because canceKey() forbids the propagation of the event any further, at least manually trigger the onValueChange() event
                NativeEvent newEvent = Document.get().createChangeEvent();
                DomEvent.fireNativeEvent(newEvent, widget);

            }
        }
    };

    public GWTNumericBox(Integer defaultValue, int minValue, int maxValue) {
        super();
        widget = new TextBox();
        //Custom CSS
        widget.setStyleName("GWTNumericBox");

        this.minValue = Math.min(minValue, maxValue);
        this.maxValue = Math.max(minValue, maxValue);
        setBoxLength();
        setDefaultValue(defaultValue);
        setValue(getDefaultValue());

        widget.addKeyDownHandler(keyDownHandler);
        widget.addKeyPressHandler(keyPressHandler);

        attachMainWidget(widget);
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public Integer getDefaultValue() {
        return defaultValue;
    }

    @Override
    public void setDefaultValue(Integer defaultValue) {
        if (defaultValue <= this.maxValue && defaultValue >= this.minValue) {
            this.defaultValue = defaultValue;
        }
    }

    @Override
    public Integer getMaxValue() {
        return maxValue;
    }

    private void setBoxLength() {
        widget.setMaxLength(Integer.toString(maxValue).length());
    }

    @Override
    public void setMaxValue(Integer maxValue) {
        if (maxValue > this.minValue) {
            this.maxValue = maxValue;
            setBoxLength();
        }
    }

    @Override
    public Integer getMinValue() {
        return minValue;
    }

    @Override
    public void setMinValue(Integer minValue) {
        if (minValue < this.maxValue) {
            this.minValue = minValue;
        }
    }

    @Override
    public Integer getValue() {
        Integer intFromDom = null;
        try {

            intFromDom = Integer.decode(widget.getText());
        } catch (NumberFormatException e) {
            //Do nothing
        }
        return (intFromDom == null) ? this.currentValue : intFromDom;
    }

    protected boolean setValue(String value) {
        try {

            Integer intFromText = (value == null || value.length()==0) ? defaultValue : Integer.decode(value);
            if (intFromText != null) {
                return setValue(intFromText);
            }
        } catch (NumberFormatException e) {
            //Do nothing
        }
        return false;
    }

    @Override
    public boolean setValue(Integer value) {
        if (value <= this.maxValue && value >= this.minValue) {

            try {

                widget.setText(Integer.toString(value));
                this.currentValue = value;
                return true;

            } catch (NumberFormatException e) {
                //Do nothing
            }
        }
        return false;
    }

    @Override
    public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
        return widget.addKeyPressHandler(handler);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return widget.addChangeHandler(handler);
    }
    
    @Override
    public HandlerRegistration addBlurHandler(BlurHandler handler) {
        return widget.addBlurHandler(handler);
    }

    @Override
    public void clean() {
        super.clean();
        setValue(getDefaultValue());
    }
}
