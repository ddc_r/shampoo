/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;

/**
 *
 * @author okay_awright
 **/
public class GWTPagedFormDropDownListBox<U> extends GWTSelectablePagedFormWidget<String, U, ListBox> implements GenericDropDownListBoxInterface<U> {

    private ChangeHandler delegatedChangeHandler;
    final private boolean respondToSetSelection;
    
    public GWTPagedFormDropDownListBox() {
        this(false);
    }
    public GWTPagedFormDropDownListBox(boolean respondToSetSelection) {
        super(new ListBox(false));
        //Custom CSS
        getVisibleWidget().setStyleName("GWTFormDropDownListBox");
        getVisibleWidget().setStyleDependentName("Paged", true);
        this.respondToSetSelection = respondToSetSelection;
    }    

    @Override
    public U getSelectedValue() {
        return getVisibleSelectedValue();
    }    
    @Override
    protected int getVisibleSelectedIndex() {
        return getVisibleWidget().getSelectedIndex();
    }
    @Override
    public boolean setSelectedValue(U selectedValue) {
        return setPagedSelectedValue(selectedValue);
    }
    @Override
    protected void setVisibleSelectedIndex(int index) {
        if (getVisibleWidget().getSelectedIndex()!=index) {
            getVisibleWidget().setSelectedIndex(index);
            if (respondToSetSelection) onVisibleSelectionChanged();
        }
    }

    @Override
    protected boolean onPreviousButtonClicked(ClickEvent event) {
        if (super.onPreviousButtonClicked(event)) {
            onVisibleSelectionChanged();
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean onNextButtonClicked(ClickEvent event) {
        if (super.onNextButtonClicked(event)) {
            onVisibleSelectionChanged();
            return true;
        }
        return false;
    }
    
    protected void onVisibleSelectionChanged() {
        if (delegatedChangeHandler!=null)
            delegatedChangeHandler.onChange(null);
    }

    @Override
    public boolean addItem(String key, U value) {
        return addPagedItem(key, value);
    }    
    @Override
    protected boolean addVisibleItem(String key) {
        getVisibleWidget().addItem(key);
        return true;
    }

    @Override
    protected void resetVisibleItems() {
        getVisibleWidget().clear();
    }

    @Override
    public int getItemCount() {
        return getPagedItemCount();
    }
    @Override
    public int getVisibleItemCount() {
        return getVisibleWidget().getItemCount();
    }

    @Override
    public void clean() {
        super.clean();
        getVisibleWidget().clear();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        getVisibleWidget().setEnabled(enabled);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        getVisibleWidget().setVisible(visible);
    }    

    @Override
    public boolean isValueIn(U value) {
        return isPagedValueIn(value);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        delegatedChangeHandler = handler;
        return getVisibleWidget().addChangeHandler(handler);
    }
    
}
