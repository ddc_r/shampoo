/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTPagedFormSelectBox<T> extends GWTMultiSelectablePagedFormWidget<String, T, ListBox> implements GenericFormSelectBoxInterface<T> {

    public GWTPagedFormSelectBox() {
        super(new ListBox(true));
        //Custom CSS
        getVisibleWidget().setStyleName("GWTFormSelectBox");
        getVisibleWidget().setStyleDependentName("Paged", true);
        //Make enough room to display that many items
        getVisibleWidget().setVisibleItemCount(MAX_VISIBLE_ITEMS);
    }

    @Override
    protected boolean isVisibleItemSelected(int index) {
        return getVisibleWidget().isItemSelected(index);
    }
    
    @Override
    public boolean addItem(String key, T value) {
        return addPagedItem(key, value);
    }    
    @Override
    protected boolean addVisibleItem(String key) {
        getVisibleWidget().addItem(key);
        return true;
    }

    @Override
    protected void resetVisibleItems() {
        getVisibleWidget().clear();
    }

    @Override
    public int getItemCount() {
        return getPagedItemCount();
    }
    @Override
    public int getVisibleItemCount() {
        return getVisibleWidget().getItemCount();
    }

    @Override
    public void clean() {
        super.clean();
        getVisibleWidget().clear();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        getVisibleWidget().setEnabled(enabled);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        getVisibleWidget().setVisible(visible);
    }    

    @Override
    public boolean isValueIn(T value) {
        return isPagedValueIn(value);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return getVisibleWidget().addChangeHandler(handler);
    }

    @Override
    public Collection<T> getSelectedValues() {
        return getVisibleSelectedValues();
    }
    
}
