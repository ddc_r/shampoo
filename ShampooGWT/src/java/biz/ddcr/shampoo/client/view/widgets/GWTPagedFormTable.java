/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.view.widgets.GenericFormTableInterface.CheckedItemHeaderRefreshHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Widget;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 *
 */
public class GWTPagedFormTable<T, E extends Widget> extends GWTPagedFormWidget<PagedRow<T, E>, Void, GWTFormTable<T, E>> implements GenericFormTableInterface<T, E> {

    public GWTPagedFormTable() {
        super(new GWTFormTable<T, E>());
        //Custom CSS
        getVisibleWidget().setStyleDependentName("Paged", true);
    }

    @Override
    public int getVisibleItemCount() {
        return getVisibleWidget().getRowCount();
    }

    @Override
    protected void resetVisibleItems() {
        getVisibleWidget().clean();
    }

    @Override
    public boolean addRow(T form, boolean canBeChecked, E... components) {
        PagedRow<T, E> pagedRow = new PagedRow<T, E>(null, form, canBeChecked, components);
        return addPagedItem(pagedRow, null);
    }

    @Override
    public boolean addRow(String cssStyle, T form, boolean canBeChecked, E... components) {
        PagedRow<T, E> pagedRow = new PagedRow<T, E>(cssStyle, form, canBeChecked, components);
        return addPagedItem(pagedRow, null);
    }

    @Override
    protected boolean addVisibleItem(PagedRow<T, E> key) {
        if (key != null) {
            return getVisibleWidget().addRow(//
                    key.getCssStyle(),//
                    key.getForm(),//
                    key.isCanBeChecked(),//
                    key.getComponents());
        }
        return false;
    }

    /**
     * Can only retrieve *visible* items that are checked, no checked
     * buffered/paged items
     */
    @Override
    public Collection<T> getAllCheckedValues() {
        return getVisibleWidget().getAllCheckedValues();
    }

    @Override
    public Collection<T> getAllValues() {
        Collection<T> result = new HashSet<T>();
        for (PagedRow<T, E> pagedRow : getAllPagedKeys()) {
            result.add(pagedRow.getForm());
        }
        return result;
    }

    @Override
    public int getCellCount(int row) {
        return getVisibleWidget().getCellCount(row);
    }

    @Override
    public E getComponentAt(int row, int column) {
        return getVisibleWidget().getComponentAt(row, column);
    }

    @Override
    public int getCurrentRow(ClickEvent event) {
        return getVisibleWidget().getCurrentRow(event);
    }

    @Override
    public int getRowCount() {
        return getVisibleWidget().getRowCount();
    }

    @Override
    public T getValueAt(int row) {
        return getVisibleWidget().getValueAt(row);
    }

    @Override
    public Boolean isRowChecked(int row) {
        return getVisibleWidget().isRowChecked(row);
    }

    @Override
    public boolean isValueIn(T value) {
        for (PagedRow<T, E> pagedRow : getAllPagedKeys()) {
            if (pagedRow.getForm().equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void removeHeader() {
        getVisibleWidget().removeHeader();
    }

    @Override
    public boolean removeRow(int row) {
        PagedItem<PagedRow<T, E>, Void> pagedItem = getVisibleItem(row);
        return (pagedItem != null) ?//
                removePagedItem(pagedItem.getKey(), null)//
                ://
                false;
    }

    @Override
    public void setCheckedItemHeaderRefreshHandler(CheckedItemHeaderRefreshHandler handler) {
        getVisibleWidget().setCheckedItemHeaderRefreshHandler(handler);
    }

    @Override
    public void setHeader(String... captions) {
        getVisibleWidget().setHeader(captions);
    }

    @Override
    public void clean() {
        super.clean();
        getVisibleWidget().clean();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        getVisibleWidget().setEnabled(enabled);
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        getVisibleWidget().setVisible(visible);
    }
}
