/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import java.util.*;

/**
 *
 * @author okay_awright
 *
 */
public abstract class GWTPagedFormWidget<T, U, V extends Widget> extends GWTExtendedWidget {

    public class PagedItem<T, U> implements Comparable<PagedItem<T, U>> {

        private T key;
        private U value;

        public PagedItem(T key, U value) {
            this.key = key;
            this.value = value;
        }

        public PagedItem(T key) {
            this.key = key;
        }

        public T getKey() {
            return key;
        }

        public void setValue(U value) {
            this.value = value;
        }

        public U getValue() {
            return value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PagedItem<T, U> other = (PagedItem<T, U>) obj;
            if (this.key != other.key && (this.key == null || !this.key.equals(other.key))) {
                return false;
            }
            if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 23 * hash + (this.key != null ? this.key.hashCode() : 0);
            hash = 23 * hash + (this.value != null ? this.value.hashCode() : 0);
            return hash;
        }

        @Override
        public int compareTo(PagedItem<T, U> o) {
            int result = 0;
            if (getKey() != null && o.getKey() != null) {
                result = getKey().toString().compareTo(o.getKey().toString());
                if (result == 0) {
                    if (getValue() != null && o.getValue() != null) {
                        return getKey().toString().compareTo(o.getKey().toString());
                    } else {
                        return getValue() == null ? (o.getValue() == null ? 0 : 1) : -1;
                    }
                } else {
                    return result;
                }
            } else {
                return getKey() == null ? (o.getKey() == null ? 0 : 1) : -1;
            }
        }
    }
    final protected static int MAX_VISIBLE_ITEMS = 20;
    //The current starting visible item index
    protected int firstVisibleItemOffset;
    final protected List<PagedItem<T, U>> items;
    final private ClickHandler previousButtonClicked;
    final private ClickHandler nextButtonClicked;
    private GWTClickableLabel previousButton;
    private GWTClickableLabel nextButton;
    private boolean isEnabled;
    private boolean showNavigationBar;
    
    final private V widget;
    
    public GWTPagedFormWidget(V widget) {
        super();

        this.widget = widget;
        items = new ArrayList<PagedItem<T, U>>();

        //Add a "Previous" and a "Next" button to the grid but do not activate them until needed
        previousButtonClicked = new ExtendedClickHandler(
                new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        onPreviousButtonClicked(event);
                    }
                }) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        };
        previousButton = new GWTClickableLabelPrevious(/*
                 * no label
                 */null);
        previousButton.setVisible(false);
        previousButton.addClickHandler(previousButtonClicked);
        nextButtonClicked = new ExtendedClickHandler(
                new ClickHandler() {

                    @Override
                    public void onClick(ClickEvent event) {
                        onNextButtonClicked(event);
                    }
                }) {

            @Override
            public boolean isCallGranted() {
                return isEnabled;
            }
        };
        nextButton = new GWTClickableLabelNext(/*
                 * no label
                 */null);
        nextButton.setVisible(false);
        nextButton.addClickHandler(nextButtonClicked);

        isEnabled = true;
        showNavigationBar = true;

        HorizontalPanel container = new HorizontalPanel();
        container.add(previousButton);
        container.add(widget);
        container.add(nextButton);
        //Custom CSS
        container.setStyleName("GWTPagingContainer");
        attachMainWidget(container);
    }

    public V getVisibleWidget() {
        return widget;
    }

    /**
     * total number of visible + paged items
     */
    protected int getPagedItemCount() {
        return items.size();
    }

    /**
     * Only counts the number of items visible on the GUI
     */
    public abstract int getVisibleItemCount();

    @Override
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        previousButton.setEnabled(enabled);
        nextButton.setEnabled(enabled);
    }

    public PagedItem<T, U> getVisibleItem(int index) {
        return (index > -1 && index < getVisibleItemCount()) ? items.get(index) : null;
    }

    public void hideNavigationBar(boolean hide) {
        showNavigationBar = !hide;
        refreshButtonStates();
    }
    
    @Override
    public void setVisible(boolean visible) {
        getVisibleWidget().setVisible(visible);
        refreshButtonStates();
    }

    @Override
    public void clean() {
        super.clean();
        items.clear();
        firstVisibleItemOffset = 0;
        refreshButtonStates();
    }

    protected boolean hasPreviousPage() {
        return firstVisibleItemOffset >= MAX_VISIBLE_ITEMS;
    }

    protected boolean hasNextPage() {
        return getVisibleItemCount() == MAX_VISIBLE_ITEMS && getPagedItemCount() > (firstVisibleItemOffset + MAX_VISIBLE_ITEMS);
    }

    protected void refreshButtonStates() {
        nextButton.setVisible(showNavigationBar & hasNextPage());
        previousButton.setVisible(showNavigationBar & hasPreviousPage());
    }

    protected boolean addPagedItem(T key, U value) {
        return addPagedItem(false, key, value);
    }
    protected boolean addPagedItem(boolean allowNullKey, T key, U value) {
        if ((allowNullKey || key != null) && items.add(new PagedItem<T, U>(key, value))) {
            /**
             * bind the current item to the widget ASAP if it's possible
             */
            if (getVisibleItemCount() < MAX_VISIBLE_ITEMS) {
                addVisibleItem(key);
            }
            refreshButtonStates();
            return true;
        }
        return false;
    }

    protected boolean addPagedItems(Collection<PagedItem<T, U>> keyValues) {
        return addPagedItems(false, keyValues);
    }
    protected boolean addPagedItems(boolean allowNullKey, Collection<PagedItem<T, U>> keyValues) {
        boolean result = false;
        if (keyValues != null && !keyValues.isEmpty()) {
            for (PagedItem<T, U> keyValue : keyValues) {
                if (allowNullKey || keyValue.getKey() != null) {
                    result = items.add(keyValue) & result;
                    /**
                     * bind the current item to the widget ASAP if it's possible
                     */
                    if (getVisibleItemCount() < MAX_VISIBLE_ITEMS) {
                        addVisibleItem(keyValue.getKey());
                    }
                }
            }
            refreshButtonStates();
            return true;
        }
        return result;
    }

    protected abstract boolean addVisibleItem(T key);

    protected boolean removePagedItem(T key, U value) {
        return removePagedItem(false, key, value);
    }
    protected boolean removePagedItem(boolean allowNullKey, T key, U value) {
        if (allowNullKey || key != null) {
            int itemPosition = items.indexOf(new PagedItem<T, U>(key, value));
            if (itemPosition > -1) {
                items.remove(itemPosition);
                if (itemPosition < firstVisibleItemOffset) {
                    refreshVisibleItems(firstVisibleItemOffset - 1);
                } else if (itemPosition < (firstVisibleItemOffset + MAX_VISIBLE_ITEMS)) {
                    refreshVisibleItems(firstVisibleItemOffset, true);
                } else {
                    refreshButtonStates();
                }
                return true;
            }
        }
        return false;
    }

    protected boolean removePagedItems(Collection<PagedItem<T, U>> keyValues) {
        return removePagedItems(false, keyValues);
    }
    protected boolean removePagedItems(boolean allowNullKey, Collection<PagedItem<T, U>> keyValues) {
        boolean result = false;
        int newFirstVisibleItemOffset = firstVisibleItemOffset;
        boolean refreshCurrentPage = false;
        if (keyValues != null && !keyValues.isEmpty()) {
            for (PagedItem<T, U> keyValue : keyValues) {
                if (allowNullKey || keyValue.getKey() != null) {
                    int itemPosition = items.indexOf(keyValue);
                    if (itemPosition > -1) {
                        result = items.remove(itemPosition)!=null & result;
                        if (itemPosition < firstVisibleItemOffset) {
                            newFirstVisibleItemOffset--;
                            refreshCurrentPage = true;
                        } else if (itemPosition < (firstVisibleItemOffset + MAX_VISIBLE_ITEMS)) {
                            refreshCurrentPage = true;
                        }
                    }
                }
            }
        }
        if (refreshCurrentPage) {
            refreshVisibleItems(newFirstVisibleItemOffset, true);
        } else refreshButtonStates();
            
        return result;
    }

    protected boolean onPreviousButtonClicked(ClickEvent event) {
        if (hasPreviousPage()) {
            return refreshVisibleItems(firstVisibleItemOffset - MAX_VISIBLE_ITEMS);
        }
        return false;
    }

    protected boolean onNextButtonClicked(ClickEvent event) {
        if (hasNextPage()) {
            return refreshVisibleItems(firstVisibleItemOffset + MAX_VISIBLE_ITEMS);
        }
        return false;
    }

    protected boolean refreshVisibleItems(int startOffset) {
        return refreshVisibleItems(startOffset, false);
    }

    protected boolean refreshVisibleItems(int startOffset, boolean forceRefresh) {
        int newFirstVisibleItemOffset = Math.max(0, Math.min(getPagedItemCount() - 1, startOffset));
        //Do only perform if refresh if necessary
        if (forceRefresh || newFirstVisibleItemOffset != firstVisibleItemOffset) {
            busy();
            firstVisibleItemOffset = newFirstVisibleItemOffset;
            resetVisibleItems();
            int iStart = firstVisibleItemOffset;
            int iStop = Math.min(firstVisibleItemOffset + MAX_VISIBLE_ITEMS, getPagedItemCount());
            for (int i = iStart; i < iStop; i++) {
                addVisibleItem(items.get(i).getKey());
            }
            unbusy();
            refreshButtonStates();
            return true;
        }
        return false;
    }

    protected abstract void resetVisibleItems();

    public boolean isPagedValueIn(U value) {
        busy();
        if (value != null) {
            for (PagedItem<T, U> item : items) {
                if (value.equals(item.getValue())) {
                    unbusy();
                    return true;
                }
            }
        }
        unbusy();
        return false;
    }
    
    public boolean isPagedKeyIn(T key) {
        busy();
        if (key != null) {
            for (PagedItem<T, U> item : items) {
                if (key.equals(item.getKey())) {
                    unbusy();
                    return true;
                }
            }
        }
        unbusy();
        return false;
    }

    public boolean isPagedItemIn(T key, U value) {
        return items.contains(new PagedItem<T, U>(key, value));
    }

    public void sort(final boolean sortingMode /*
             * true==descending, false==ascending
             */) {
        busy();
        Comparator<PagedItem<T, U>> c = new Comparator<PagedItem<T, U>>() {

            @Override
            public int compare(PagedItem<T, U> o1, PagedItem<T, U> o2) {
                return sortingMode ? o2.compareTo(o1) : o1.compareTo(o2);
            }
        };

        Set<PagedItem<T, U>> _sortedSet = new TreeSet<PagedItem<T, U>>(c);
        for (int i = getPagedItemCount() - 1; i > -1; i--) {
            _sortedSet.add(items.get(i));
        }

        clean();
        for (PagedItem<T, U> _entry : _sortedSet) {
            addPagedItem(_entry.getKey(), _entry.getValue());
        }
        unbusy();
    }

    public Collection<U> getAllPagedValues() {
        Collection<U> result = new HashSet<U>();/*
         * no duplicate
         */
        for (PagedItem<T, U> item : items) {
            result.add(item.getValue());
        }
        return result;
    }

    public Collection<T> getAllPagedKeys() {
        Collection<T> result = new HashSet<T>();/*
         * no duplicate
         */
        for (PagedItem<T, U> item : items) {
            result.add(item.getKey());
        }
        return result;
    }
}
