/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 **/
public class GWTPagedSimpleLayer<E extends Widget> extends GWTPagedFormWidget<E, Void, FlexTable> implements GenericSimpleLayerInterface<E> {
   
    public GWTPagedSimpleLayer() {
        super(new FlexTable());
        //Custom CSS
        getVisibleWidget().setStyleName("GWTGrid");
        getVisibleWidget().setStyleDependentName("Paged", true);        
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        getVisibleWidget().setVisible(visible);
    }

    @Override
    public void clean() {
        super.clean();
        getVisibleWidget().removeAllRows();
    }
    
    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return getVisibleWidget().addClickHandler(handler);
    }

    @Override
    public int getVisibleItemCount() {
        return getVisibleWidget().getRowCount();
    }

    @Override
    protected void resetVisibleItems() {
        getVisibleWidget().removeAllRows();
    }

    @Override
    public int getCurrentRow(ClickEvent event) {
        HTMLTable.Cell clickedCell;
        if (event != null && (clickedCell = getVisibleWidget().getCellForEvent(event)) != null) {
            return clickedCell.getRowIndex();
        }
        return -1;
    }

    @Override
    public int getRowCount() {
        return getVisibleWidget().getRowCount();
    }

    @Override
    protected boolean addVisibleItem(E key) {
        int extraRow = getVisibleWidget().getRowCount();
        int colIndex = 1;
        getVisibleWidget().setWidget(extraRow, colIndex, key);
        return true;
    }

    @Override
    public void addRow(E widget) {
        addPagedItem(widget, null);
    }
    
}
