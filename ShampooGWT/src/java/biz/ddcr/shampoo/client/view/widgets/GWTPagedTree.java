/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import java.util.TreeMap;

/**
 *
 * @author okay_awright
 **/
public class GWTPagedTree extends GWTPagedFormWidget<GWTPagedTreeItem, Void, Tree> implements GenericTreeInterface<Long, Widget> {

   protected static final Tree.Resources DUMMY_RESOURCES = new GWTTree.UnStylableTreeResources();
    
    final private TreeMap<Long, GWTPagedTreeItem> nodes;
    final private TreeMap<String, Long> nodesByName;
    /** autoincremented long handle of the latest created branch **/
    private long latestNodeId;

    public GWTPagedTree() {
        super(new Tree(DUMMY_RESOURCES));
        //Custom CSS
        getVisibleWidget().setStyleName("GWTTree");
        getVisibleWidget().setStyleDependentName("Paged", true);
        
        //Hide the navigation bar (if any) when the tree is not expanded
        getVisibleWidget().addOpenHandler(new OpenHandler<TreeItem>() {

            @Override
            public void onOpen(OpenEvent<TreeItem> event) {
                if (event.getTarget()!=null && event.getTarget().getWidget()!=null) {
                    ((GWTPagedTreeItem)event.getTarget().getWidget()).hideNavigationBar(false);
                }
            }
        });
        getVisibleWidget().addCloseHandler(new CloseHandler<TreeItem>() {

            @Override
            public void onClose(CloseEvent<TreeItem> event) {
                if (event.getTarget()!=null && event.getTarget().getWidget()!=null) {
                    ((GWTPagedTreeItem)event.getTarget().getWidget()).hideNavigationBar(true);
                }
            }
        });
        
        nodes = new TreeMap<Long, GWTPagedTreeItem>();
        nodesByName = new TreeMap<String, Long>();
        latestNodeId = 0;
        //Hide the nivagtion bar by default since its leaf is close after instantiation
        hideNavigationBar(true);
    }

    private long getNewItemId() {
        return ++latestNodeId;
    }

    @Override
    protected boolean addVisibleItem(GWTPagedTreeItem key) {
        //Widget must be a GWTTreeItem
        getVisibleWidget().addItem(key.getNode());
        return true;
    }

    @Override
    public Long addItem(Widget localWidget) {
        if (localWidget != null) {
            GWTPagedTreeItem newTreeItem = new GWTPagedTreeItem(localWidget);
            //Hide the nivagtion bar by default since its leaf is close after instantiation
            newTreeItem.hideNavigationBar(true);
            if (addPagedItem(newTreeItem, null)) {
                //update the item referency list
                long newId = getNewItemId();
                nodes.put(newId, newTreeItem);
                return newId;
            }
        }
        return null;
    }

    @Override
    public Long getItemByName(String name) {
        if (name != null) {
            return nodesByName.get(name);
        }
        return null;
    }
    
    @Override
    public Long addItemWithName(String name, Widget localWidget) {
        if (name != null && localWidget != null) {
            Long newId = addItem(localWidget);
            if (newId!=null)
                nodesByName.put(name, newId);
            return newId;
        }
        return null;
    }
    
    @Override
    public Long addItem(Widget localWidget, Long itemIdContainer) {
        if (localWidget != null && itemIdContainer != null) {
            GWTPagedTreeItem containerTreeItem = null;
            if ((containerTreeItem = nodes.get(itemIdContainer)) == null) {
                return addItem(localWidget);
            }
            //Add this entry to the corresponding branch
            GWTPagedTreeItem newTreeItem = containerTreeItem.addItem(localWidget);
            if (newTreeItem!=null) {
                //update the item referency list
                long newId = getNewItemId();
                nodes.put(newId, newTreeItem);                
                return newId;
            }
        }
        return null;
    }

    @Override
    public int getVisibleItemCount() {
        return getVisibleWidget().getItemCount();        
    }

    @Override
    protected void resetVisibleItems() {
        getVisibleWidget().clear();
    }

    @Override
    public void clean() {
        super.clean();
        getVisibleWidget().clear();
        nodes.clear();
        nodesByName.clear();
        latestNodeId = 0;
    }
    
    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        getVisibleWidget().setVisible(visible);
    }    

}
