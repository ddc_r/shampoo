/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class GWTPagedTreeItem extends GWTPagedFormWidget<GWTPagedTreeItem, Void, Widget> {

    final private TreeItem treeItem;
    
    public GWTPagedTreeItem(Widget widget) {
        super(widget);
        treeItem = new TreeItem(this);
        //Custom CSS
        treeItem.setStyleName("GWTTreeItem");
        treeItem.setStyleDependentName("Paged", true);
    }

    public TreeItem getNode() {
        return treeItem;
    }
    
    public GWTPagedTreeItem addItem(Widget widget) {
        if (widget!=null) {
            GWTPagedTreeItem newPagedTreeItem = new GWTPagedTreeItem(widget);
            //Hide the nivagtion bar by default since its leaf is close after instantiation
            newPagedTreeItem.hideNavigationBar(false);
            if (addPagedItem(newPagedTreeItem, null))
                return newPagedTreeItem;
        }
        return null;
    }

    @Override
    protected boolean addVisibleItem(GWTPagedTreeItem key) {
        treeItem.addItem(key.getNode());
        return true;
    }

    @Override
    public int getVisibleItemCount() {
        return treeItem.getChildCount();
    }

    @Override
    protected void resetVisibleItems() {
        treeItem.removeItems();
    }

    @Override
    public void clean() {
        super.clean();
        treeItem.removeItems();
    }
    
    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        getVisibleWidget().setVisible(visible);
    }    

}
