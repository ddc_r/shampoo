/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

/**
 *
 * @author okay_awright
 **/
public class GWTPercentProgressBar extends GWTExtendedWidget implements GenericProgressBarInterface {

    final private IncubatorProgressBar widget;

    public GWTPercentProgressBar() {
        super();
        widget = new IncubatorProgressBar(0.0, 100.0, 0.0); //Min, max and inital value for percents
        //Custom CSS
        widget.setStyleName("GWTProgressBar");
        attachMainWidget(widget);
    }

    @Override
    public void clean() {
        super.clean();
        widget.setProgress(0.0);
    }

    @Override
    public void setProgress(long progress) {
        widget.setProgress((double)progress);
    }
    public void setPercent(byte percent) {
        widget.setProgress((double)percent);
    }

    @Override
    public long getMaxProgress() {
        return 100L;
    }
    
    @Override
    public long getProgress() {
        return (long)widget.getProgress();
    }
    public byte getPercent() {
        return (byte)widget.getProgress();
    }
    
    @Override
    public void setPercentTextVisible(boolean isVisible) {
        widget.setPercentTextVisible(isVisible);
    }

    @Override
    public boolean isPercentTextVisible() {
        return widget.isPercentTextVisible();
    }

}
