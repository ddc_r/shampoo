/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;

/**
 *
 * @author okay_awright
 **/
public class GWTPictureUploadMoxie extends GWTFileUploadMoxie {

    public GWTPictureUploadMoxie(String jsessionid) {
        super(jsessionid);
        //Change the upload button caption
        getUploadLink().setCaption(I18nTranslator.getInstance().upload_new_coverart());
    }
    public GWTPictureUploadMoxie(String postURLEndpoint, String jsessionid) {
        this(jsessionid);
        setAbsolutePostURL(postURLEndpoint);
    }

    @Override
    public RuntimeException translateErrorCodeIntoException(int errorCode) {
        return HTTPCustomErrorCodes.toException(errorCode);
    }

}
