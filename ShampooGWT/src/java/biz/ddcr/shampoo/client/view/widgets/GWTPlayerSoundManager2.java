/*
 *  Copyright (C) 2014 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.GlobalEventsEnum;
import biz.ddcr.shampoo.client.form.track.HasAudioInterface;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.errors.AudioPlayerPlayingException;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.playlist.Playlist;
import com.chj.gwt.client.soundmanager2.Callback;
import com.chj.gwt.client.soundmanager2.Option;
import com.chj.gwt.client.soundmanager2.SMSound;
import com.chj.gwt.client.soundmanager2.SoundManager;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Date;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import java.util.Collections;

/**
 * GWT audio player, based on SoundManager 2. Applets, even if technically
 * superior, have been dropped because of those silly security popups and their
 * slow startup times
 *
 * @author okay_awright
 *
 */
public class GWTPlayerSoundManager2 extends GWTExtendedWidget implements GenericFilePlayerInterface {

    //Internal flag to know whether the applet is fully loaded or not
    private static boolean isSM2JSReady = false;
    //The wrapped up JS player
    private static final SoundManager soundManager = SoundManager.getInstance();

    //Singleton semantic untranslated in Javascript
    static {
        //SM2 general options
        soundManager.setUseHtml5Audio(true);
        //HTML5 Audio support for streaming Shoutcast and Icecast is horrible on most platforms, including Chrome and IE, except Firefox which is fantastic, prefer Flash for now
        soundManager.setPreferFlash(true);
        //soundManager.setFlashVersion(8);
        soundManager.setSoundManagerURL(GWT.getModuleBaseURL());
        //Callbacks
        soundManager.onTimeout(new Callback() {
            @Override
            public void execute() {
                isSM2JSReady = false;
            }
        });
        soundManager.onReady(new Callback() {
            @Override
            public void execute() {
                isSM2JSReady = true;
            }
        });
        //Initializing SM2
        soundManager.beginDelayedInit();
    }

    //GUI
    final private VerticalPanel container;
    final private GWTFlowPanel basePanel;
    final private GWTClickableLabel play;
    final private GWTLabel state;
    //objects used in the presenter or view that created this widget
    private EventBusWithLookup eventBus;
    protected boolean isPlaylistReady;
    protected Playlist playlist; //upload endpoint on the server; only used at initialization
    protected String currentPlaylistItem;
    protected String jsessionid;
    private boolean isEnabled = true;
    //the sound id currently being played
    private final String smId;

    final private Playlist.Callback playlistConstructionCallback = new Playlist.Callback() {

        @Override
        public void onBuilt(Playlist builtPlaylist) {
            playlist = builtPlaylist;
            isPlaylistReady = true;
        }

        @Override
        public void onError(Throwable error) {
            if (error!=null) GWTPlayerSoundManager2.this.onError(error.getMessage());
        }

    };

    /** Make sure the player stops playing if the widget is detached from teh DOM tree*/
    @Override
    protected void onUnload() {
        super.onUnload();
        if (isSM2JSReady) {
            soundManager.destroySound(smId);
        }
    }

    private void onCurrentTimeMarker(long seconds) {
        state.setCaption(new JSDuration(seconds * 1000).getI18nFriendlyString());
    }

    private void onError(String message) {
        //Convert errorCodes into Exceptions
        if (eventBus != null) {
            eventBus.dispatch(GlobalEventsEnum.DISPLAY_ERROR, I18nTranslator.translateException(new AudioPlayerPlayingException(message)));
        }
        //Reset its playing state as well
        onStop();
        refreshGUI(isEnabled);
    }

    private void onPlay() {
        state.setCaption(I18nTranslator.getInstance().connecting());
        play.setCaption(I18nTranslator.getInstance().stop());
    }

    private void onStop() {
        state.setCaption(null);
        play.setCaption(I18nTranslator.getInstance().play());
    }

    public GenericHyperlinkInterface getPlayLink() {
        return play;
    }

    public GWTPlayerSoundManager2(HasAudioInterface form, EventBusWithLookup eventBus, String jsessionid) {
        this(
                form != null
                ? form.getTrackDownloadURL()
                : null,
                true,
                eventBus,
                jsessionid);
    }

    public GWTPlayerSoundManager2(String mediaURLPath, EventBusWithLookup eventBus, String jsessionid) {
        this(mediaURLPath, false, eventBus, jsessionid);
    }

    public GWTPlayerSoundManager2(String mediaURLPath, boolean disablePlaylistMode, EventBusWithLookup eventBus, String jsessionid) {
        container = new VerticalPanel();

        smId = "S" + Math.abs(Random.nextInt()) + (new Date()).getTime();

        //Panel displayed when no upload is currently in progress
        basePanel = new GWTCSSFlowPanel("GWTMediaPlayer");
        play = new GWTClickableLabelPlay(I18nTranslator.getInstance().play());
        basePanel.add(play);
        state = new GWTLabel(null);
        basePanel.add(state);
        container.add(basePanel);

        this.eventBus = eventBus;
        if (disablePlaylistMode) {
            this.playlist = new Playlist(Collections.singletonList(mediaURLPath));
        } else {
            Playlist.build(mediaURLPath, playlistConstructionCallback);
        };
        this.currentPlaylistItem = null;
        this.isPlaylistReady = disablePlaylistMode;

        this.jsessionid = jsessionid;

        refreshGUI(true);
        attachMainWidget(container);

        play.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (isSM2JSReady) {
                    playStop();
                }
            }
        });

    }

    private void refreshGUI(boolean isReady) {
        basePanel.setVisible(isReady);
        play.setEnabled(isReady);
        state.setCaption(null);
    }

    @Override
    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        refreshGUI(isEnabled);
    }

    protected void setRelativeMediaURL(String mediaURLPath, boolean disablePlaylistMode) {
        //Open the media right away
        if (mediaURLPath != null && mediaURLPath.length() != 0) {
            if (disablePlaylistMode) {
                this.playlist = new Playlist(Collections.singletonList(mediaURLPath));
            } else {
                Playlist.build(mediaURLPath, playlistConstructionCallback);
            };
            this.currentPlaylistItem = null;
            this.isPlaylistReady = disablePlaylistMode;
        }
    }

    @Override
    public void setTrack(HasAudioInterface form, String absoluteEndpoint) {
        setRelativeMediaURL(form != null
                ? form.getTrackDownloadURL()
                : null,
                true);
    }

    @Override
    public void setEventBus(EventBusWithLookup eventBus) {
        this.eventBus = eventBus;
    }

    private static final RegExp JSESSIONID_PATTERN = RegExp.compile("[Jj][Ss][Ee][Ss]{2}[Ii][Oo][Nn][Ii][Dd]=([^;&]+)");
    private static final String JSESSIONID_MARKER = "jsessionid";

    protected String makeJESSIONIDURI(String url) {
        if (url != null) {
            url = url.trim();
            if (jsessionid != null && url.matches("[Hh][Tt][Tt][Pp][Ss]?://.*")) {
                MatchResult matcher = JSESSIONID_PATTERN.exec(url);
                if (matcher == null) {
                    int i = url.indexOf('?');
                    if (i < 0) {
                        return url + ";" + JSESSIONID_MARKER + "=" + jsessionid;
                    } else {
                        /**
                         * Insert it before any URL parameter, otherwise the
                         * Java HttpRequest's getParameter() will fail
                         */
                        String leftPart = url.substring(0, i);
                        String rightPart = url.substring(i + 1);
                        return leftPart + ";" + JSESSIONID_MARKER + "=" + jsessionid + "?" + rightPart;
                    }
                } else {
                    String leftPart = url.substring(0, matcher.getIndex());
                    String rightPart = url.substring(matcher.getIndex() + matcher.getGroup(1).length());
                    return leftPart + JSESSIONID_MARKER + "=" + jsessionid + rightPart;
                }
            }
        }
        return url;
    }

    @Override
    public void setCurrentSessionId(String jsessionidFragment) {
        if (jsessionidFragment != null && jsessionidFragment.length() != 0) {
            jsessionid = jsessionidFragment;
        }
    }

    private void resetNewCurrentPlaylistItem() {
        if (!playlist.hasNext()) {
            playlist.rewind();
        }
        if (playlist.hasNext()) {
            currentPlaylistItem = playlist.next();
        } else {
            onError("No more available item to play");
        }
    }

    private String getCurrentPlaylistItem() {
        if (currentPlaylistItem == null) {
            resetNewCurrentPlaylistItem();
        }
        return currentPlaylistItem;
    }

    private static final RegExp SHOUTCAST_PATTERN = RegExp.compile("^[Hh][Tt][Tt][Pp]://(.*:[0-9]+)/?$");

    /**
     * Shoutcast streams need to be handled in special ways to be directly
     * playable using the pure HTML5 implementation of SoundManager2
     *
     * @param url
     * @return
     */
    protected String filterShoutcastStream(String url) {
        if (url != null) {
            MatchResult matcher = SHOUTCAST_PATTERN.exec(url);
            //Override "browser mode" in Shoutcast protocol, so as to be streamable
            return matcher != null && matcher.getGroupCount() > 1 ? "http://" + matcher.getGroup(1) + "/;" : url;
        }
        return url;
    }

    /**
     * Directly initiate playing or stopping the stream from outside this widget
     *
     */
    @Override
    public void playStop() {
        if (isSM2JSReady && isPlaylistReady) {
            try {
                final SMSound sound = soundManager.getSoundById(smId);
                if (sound == null || sound.getPlayState() == 0) {
                    if (sound != null) {
                        soundManager.destroySound(smId);
                    }
                    final Option[] options = {
                        new Option("debugMode", false),
                        new Option("id", smId),
                        new Option("url", makeJESSIONIDURI(filterShoutcastStream(getCurrentPlaylistItem()))),
                        new Option("autoLoad", false),
                        new Option("autoPlay", false),
                        new Option("multiShot", false),
                        new Option("stream", canReadStreams()),
                        //Callbacks
                        new Option("onload", new Callback() {
                            @Override
                            public void execute() {
                                unbusy();
                                final SMSound s = soundManager.getSoundById(smId);
                                if (s!=null && !s.getLoaded()) {
                                    resetNewCurrentPlaylistItem();
                                    onError("That stream cannot be loaded");
                                }
                            }
                        }),
                        new Option("onplay", new Callback() {
                            @Override
                            public void execute() {
                                busy();
                                onPlay();
                            }
                        }),
                        new Option("onstop", new Callback() {
                            @Override
                            public void execute() {
                                unbusy();
                                soundManager.destroySound(smId);
                                onStop();
                            }
                        }),
                        new Option("onfinish", new Callback() {
                            @Override
                            public void execute() {
                                resetNewCurrentPlaylistItem();
                                unbusy();
                                soundManager.destroySound(smId);
                                onStop();
                            }
                        }),
                        new Option("onbufferchange", new Callback() {
                            @Override
                            public void execute() {
                                final SMSound s = soundManager.getSoundById(smId);
                                if (s != null) {
                                    if (s.getIsBuffering()) {
                                        busy();
                                    } else {
                                        unbusy();
                                    }
                                }
                            }
                        }),
                        new Option("whileplaying", new Callback() {
                            @Override
                            public void execute() {                                
                                final SMSound s = soundManager.getSoundById(smId);
                                if (s != null) {
                                    unbusy();
                                    onCurrentTimeMarker(s.getPosition());
                                }
                            }
                        })};
                    soundManager.createSound(options);
                    //FIX : SHAMPOO-43
                    soundManager.stopAll();
                    soundManager.play(smId);
                } else {
                    soundManager.stop(smId);
                }
            } catch (Exception ex) {
                unbusy();
                soundManager.destroySound(smId);
                onError(ex.getMessage());
            }
        }
    }

    @Override
    public boolean isPlaying() {
        if (isSM2JSReady) {
            final SMSound s = soundManager.getSoundById(smId);
            return (s != null && s.getPlayState() != 0);
        }
        return false;
    }

    @Override
    public void clean() {
        play.clean();
        unbusy();
        if (isSM2JSReady) {
            soundManager.destroySound(smId);
        }
        refreshGUI(isEnabled);
    }

    @Override
    public boolean canReadStreams() {
        //Default
        return true;
    }
}
