/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.datepicker.client.DatePicker;
import java.util.Date;

/**
 *
 * @author okay_awright
 **/
public class GWTPopupCalendar extends GWTExtendedWidget implements FocusHandler, GenericCalendarInterface {

    private DatePicker widget = null;
    private GWTClickableLabel label = null;
    private PopupPanel popup = null;
    private byte currentDayIndex;
    private YearMonthDayInterface[] currentWeekDays;
    private YearMonthDayInterface nextWeekStartDay;
    //Whether the clickable label displays the selected date (when null) or a generic label (when not null)
    private String defaultText;
    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private DateChangeHandler delegatedDateChange = null;

    public GWTPopupCalendar(String defaultText) {
        this(null, defaultText);
    }

    public GWTPopupCalendar(YearMonthDayInterface currentDate) {
        this(currentDate, null);
    }

    public GWTPopupCalendar(YearMonthDayInterface currentDate, String defaultText) {
        super();

        widget = new DatePicker();
        //Custom CSS
        widget.setStyleName("GWTDatePicker");
        label = new GWTClickableLabelDate(null);

        popup = new PopupPanel();
        popup.setAutoHideEnabled(true);
        popup.addAutoHidePartner(label.getElement());
        popup.setWidget(widget);

        widget.addValueChangeHandler(new ValueChangeHandler<Date>() {

            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                setCurrentDate(event.getValue());
                updateLabelText();
                onAfterValueSet();
            }
        });
        label.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                toggleCalendar();
            }
        });

        this.defaultText = defaultText;
        setCurrentDate(currentDate);

        attachMainWidget(label);
    }

    @Override
    public void setEnabled(boolean enabled) {
        label.setEnabled(enabled);
    }


    @Override
    public void setCaption(String defaultText) {
        if ((defaultText!=null && !defaultText.equals(this.defaultText)) || (defaultText==null && this.defaultText!=null)) {
            this.defaultText = defaultText;
            updateLabelText();
        }
    }

    @Override
    public void unsetCaption() {
        setCaption(null);
    }

    protected void _setValue(Date newDate) {
        widget.setCurrentMonth(newDate);
        widget.setValue(newDate);
    }

    protected void onAfterValueSet() {
        if (delegatedDateChange != null) {
            delegatedDateChange.onDateChange(getCurrentDate());
        }
    }

    private void _setValues(String localTimeZone, Date currentDate) {
        //Are we currently monday, tuesday, etc?
        //0=SUN, 1=MON, etc
        //Shift from American standard (SUN==0) to client startDayOfWeek
        int currentDayOfWeek = (currentDate.getDay() - GWTFormWeeklyTimetable.getStartingDayOfWeek());
        if (currentDayOfWeek < 0) {
            currentDayOfWeek += 7;
        }
        
        //Translate every day of the week that contains the currentDate into complete date
        //The same routines for computing week boundaries are used within GWTWeeklyTimetable and Calendar so no need to adjust them
        currentWeekDays = new JSYearMonthDay[7];
        for (byte i = 0; i < 7; i++) {

            /*
             *** drifts may be too important using timstamps modifications, day increments through the date object is the only solution ***
            //This is computed using the current client locale!
            long diffMilliseconds = (i - currentDayOfWeek) * 24 * 60 * 60 * 1000;

            //set current day index while we're here
            if (diffMilliseconds == 0) currentDayIndex = i;
            Date iDate = new Date(currentDate.getTime() + diffMilliseconds);
             */
            int shift = i - currentDayOfWeek;
            if (shift == 0) {
                currentDayIndex = i;
            }
            Date iDate = new Date(
                    currentDate.getYear(),
                    currentDate.getMonth(),
                    currentDate.getDate() + shift);

            currentWeekDays[i] = new JSYearMonthDay(
                    localTimeZone,
                    (short) (iDate.getYear() + 1900),
                    (byte) (iDate.getMonth() + 1),
                    (byte) iDate.getDate());
        }
        
        //Compute the first day of the next upcoming week
        int shift = 7 - currentDayOfWeek;
        Date iDate = new Date(
                    currentDate.getYear(),
                    currentDate.getMonth(),
                    currentDate.getDate() + shift);
        nextWeekStartDay = new JSYearMonthDay(
                    localTimeZone,
                    (short) (iDate.getYear() + 1900),
                    (byte) (iDate.getMonth() + 1),
                    (byte) iDate.getDate());
        
    }

    protected void setCurrentDate(Date currentDate) {
        if (currentDate != null) {
            //Since we don't manipulate hours and minutes we will assume that the given date is the one specified with the calendar (which is not of course)
            String previousTimeZone = null;
            if (getCurrentDate() == null) {
                previousTimeZone = "UTC";
            } else {
                previousTimeZone = getCurrentDate().getTimeZone();
            }

            _setValues(previousTimeZone, currentDate);
            _setValue(currentDate);

            hideCalendar();

            updateLabelText();
        }
    }

    @Override
    public void setCurrentDate(YearMonthDayInterface currentDate) {
        if (currentDate != null && (getCurrentDate() == null || getCurrentDate() != currentDate)) {
            //Since we don't manipulate hours and minutes we will assume that the given date is the one specified with the calendar (which is not of course)
            Date newDate = new Date(
                    currentDate.getYear() - 1900,
                    currentDate.getMonth() - 1,
                    currentDate.getDayOfMonth());

            _setValues(currentDate.getTimeZone(), newDate);
            _setValue(newDate);

            hideCalendar();

            updateLabelText();

        }
    }

    @Override
    public YearMonthDayInterface getCurrentDate() {
        return getDayInCurrentWeek(currentDayIndex);
    }

    @Override
    public YearMonthDayInterface getNextWeekStartDay() {
        return nextWeekStartDay;
    }

    @Override
    public YearMonthDayInterface[] getCurrentWeekDays() {
        //TODO: Make it mutable, clone() not supported by GWT
        return currentWeekDays;
    }

    @Override
    public YearMonthDayInterface getDayInCurrentWeek(byte dayIndex) {
        if (currentWeekDays != null) {
            return currentWeekDays[dayIndex];
        } else {
            return null;
        }
    }

    protected void updateLabelText() {
        if (defaultText == null) {
            if (getCurrentDate() != null) {
                label.setCaption(getCurrentDate().getI18nSyntheticFriendlyString());
            }
        } else {
            label.setCaption(defaultText);
        }
    }

    protected void toggleCalendar() {
        if (popup.isShowing()) {
            hideCalendar();
        } else {
            showCalendar();
        }
    }

    protected void hideCalendar() {
        if (popup.isShowing()) {
            popup.hide();
        }
    }

    protected void showCalendar() {
        popup.showRelativeTo(this);
    }

    @Override
    public void onFocus(FocusEvent event) {
        if (!popup.isShowing()) {
            showCalendar();
        }
    }

    @Override
    public void setDateChangeHandler(DateChangeHandler handler) {
        delegatedDateChange = handler;
    }

}
