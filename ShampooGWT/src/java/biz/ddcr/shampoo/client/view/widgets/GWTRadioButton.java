/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RadioButton;

/**
 *
 * @author okay_awright
 **/
public class GWTRadioButton extends GWTExtendedWidget implements GenericRadioButtonInterface {

    final private RadioButton widget;

    public GWTRadioButton(String groupName, boolean selected) {
        super();
        widget = new RadioButton(groupName);
        //Custom CSS
        widget.setStyleName("GWTRadioButton");
        setSelected(selected);
        attachMainWidget(widget);
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public void setSelected(boolean selected) {
        widget.setValue(selected);
    }

    @Override
    public boolean isSelected() {
        return widget.getValue();
    }

    @Override
    public void setCaption(String text) {
        widget.setText(text);
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return widget.addClickHandler(handler);
    }

    @Override
    public void setGroupName(String groupName) {
        widget.setName(groupName);
    }

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler handler) {
        return widget.addValueChangeHandler(handler);
    }

}
