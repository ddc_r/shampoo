/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.DynamicPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTReadOnlyPSlot<T extends PlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericPlaylistSlotComponentInterface<T, U, V> {

    private GenericPlaylistSlotComponentInterface<? extends Object, U, V> featureValueContainer; //The widget that will contain the item related to the selected value by featureKeySelector
    private GWTLabel featureFadeInLabel;
    private GWTLabel featureLoopLabel;
    private GWTLabel featureRequestLabel;
    private GWTGrid widgetContainer;
    //Callback to the underlying presentation layer in order to notify it a user edit tracks
    private SlotRefreshHandler<U, V> delegatedSlotRefreshHandler = null;
    //local copy of the form passed to the widget
    private T component;

    public GWTReadOnlyPSlot(T form) {
        this(form, null);
    }
    public GWTReadOnlyPSlot(T form, SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super();
        widgetContainer = new GWTGrid();
        //Custom CSS
        widgetContainer.setStyleName("GWTPSlot");

        featureFadeInLabel = new GWTLabel(null);
        featureLoopLabel = new GWTLabel(null);
        featureRequestLabel = new GWTLabel(null);

        widgetContainer.addRow(
                null,
                new GWTLabel(I18nTranslator.getInstance().fade_in()+":"),
                featureFadeInLabel,
                new GWTLabel(I18nTranslator.getInstance().repeat()+":"),
                featureLoopLabel,
                new GWTLabel(I18nTranslator.getInstance().request_allowed()+":"),
                featureRequestLabel);

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        setPropertiesFromComponent(form);

        attachMainWidget(widgetContainer);
    }

    public void attachNextEmbeddedComponent(GenericPlaylistSlotComponentInterface<T, U, V> newComponent) {
        featureValueContainer = newComponent;
        widgetContainer.setWidgetAt(0, /*index 0 is the next widget index*/0, featureValueContainer.getWidget());
    }
    public void detachNextEmbeddedComponent() {
        featureValueContainer = null;
        widgetContainer.setWidgetAt(0, /*index 0 is the next widget index*/0, null);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (featureValueContainer != null) {
            featureValueContainer.setEnabled(enabled);
        }
    }

    @Override
    public boolean setPropertiesFromComponent(T component) {

        this.component = component;

        detachNextEmbeddedComponent();
        if (component != null) {

            //Type
            if (component instanceof StaticPlaylistEntryForm) {

                StaticPlaylistEntryForm staticComponent = (StaticPlaylistEntryForm)component;
                attachNextEmbeddedComponent(new GWTReadOnlyPSlotStaticEntry(staticComponent, delegatedSlotRefreshHandler));

            } else if (component instanceof DynamicPlaylistEntryForm) {

                DynamicPlaylistEntryForm dynamicComponent = (DynamicPlaylistEntryForm)component;
                attachNextEmbeddedComponent(new GWTReadOnlyPSlotDynamicEntry(dynamicComponent, delegatedSlotRefreshHandler));

            }
            //Loop
            if (component.getFadeIn()!=null) {
                if (component.getFadeIn()==0)
                    featureFadeInLabel.setCaption(I18nTranslator.getInstance().none());
                else {
                    //Convert milliseonds to seconds
                    Integer fadeInSeconds = (int)Math.round(component.getFadeIn() / 1000.0);
                    featureFadeInLabel.setCaption(fadeInSeconds.toString()+(fadeInSeconds==1?I18nTranslator.getInstance().second():I18nTranslator.getInstance().seconds()));
                }
            } else {
                //Add a sensible default value
                featureFadeInLabel.setCaption(I18nTranslator.getInstance().default_value());
            }
            //Loop
            if (component.getLoop()!=null) {
                featureLoopLabel.setCaption(component.getLoop().toString());
            } else {
                //Add a sensible default value
                featureLoopLabel.setCaption(I18nTranslator.getInstance().not_applicable());
            }
            //Request
            featureRequestLabel.setCaption(
                    component.isRequestAllowed() ?
                        I18nTranslator.getInstance().is_true() :
                        I18nTranslator.getInstance().is_false()
                    );
            return true;
        }
        return false;
    }

    @Override
    public T getComponentFromProperties() {
        return component;
    }

    @Override
    public void clean() {
        super.clean();
        detachNextEmbeddedComponent();
        featureLoopLabel.clean();
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        delegatedSlotRefreshHandler = handler;
        //No use here, pass along and notify sub-components
        if (featureValueContainer!=null)
            featureValueContainer.setSlotRefreshHandler(handler);
    }
}
