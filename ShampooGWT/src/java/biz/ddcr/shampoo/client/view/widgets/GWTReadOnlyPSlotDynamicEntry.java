/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.DynamicPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTReadOnlyPSlotDynamicEntry<T extends DynamicPlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericPlaylistSlotComponentInterface<T,U,V> {

    private GWTLabel ruleKeyLabel;
    private GenericPlaylistSlotComponentInterface<? extends Object, U, V> ruleValueContainer;  //Will embed the widget corresponding to the featureKeySelector selected value
    private GWTGrid ruleWidgetContainer;
    private GWTDisclosurePanel filterPanel;
    private GWTFlowPanel filterWidgetContainer;
    //General Options
    private GWTDisclosurePanel miscOptionsPanel;
    private GWTLabel noReplayTimeLabel;
    private GWTLabel noReplayPlaylistLabel;
    //Callback to the underlying presentation layer in order to notify it a user edit tracks
    private SlotRefreshHandler<U, V> delegatedSlotRefreshHandler = null;

    public GWTReadOnlyPSlotDynamicEntry(T form) {
        this(form, null);
    }

    public GWTReadOnlyPSlotDynamicEntry(T form, SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super();
        GWTFlowPanel widget = new GWTFlowPanel();
        //Custom CSS
        widget.setStyleName("GWTPSlotEntry");

        /* Rule section */
        ruleKeyLabel = new GWTLabel(null);

        ruleWidgetContainer = new GWTGrid();
        ruleWidgetContainer.addRow(
                ruleKeyLabel,
                /** the next widget will be specified only when required **/
                null);
        widget.add(ruleWidgetContainer);

        /* Filters section */
        filterPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().filters());
        //DisclosurePanels can only contain two widgets at most
        filterWidgetContainer = new GWTFlowPanel();

        filterPanel.add(filterWidgetContainer);
        widget.add(filterPanel);

        /* Misc. options section */
        miscOptionsPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().misc_options());
        //DisclosurePanels can only contain two widgets at most
        GWTVerticalPanel dummyPanel1 = new GWTVerticalPanel();

        noReplayTimeLabel = new GWTLabel(null);
        dummyPanel1.add(noReplayTimeLabel);

        noReplayPlaylistLabel = new GWTLabel(null);
        dummyPanel1.add(noReplayPlaylistLabel);

        miscOptionsPanel.add(dummyPanel1);
        widget.add(miscOptionsPanel);

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        setPropertiesFromComponent(form);

        attachMainWidget(widget);
    }

    public void attachNextEmbeddedComponent(GenericPlaylistSlotComponentInterface<T, U, V> newComponent) {
        ruleValueContainer = newComponent;
        ruleWidgetContainer.setWidgetAt(0, /* index 1 is the location of the next embedded widget*/1, ruleValueContainer.getWidget());
    }

    public void detachNextEmbeddedComponent() {
        ruleValueContainer = null;
        ruleWidgetContainer.setWidgetAt(0, /* index 1 is the location of the next embedded widget*/1, null);
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (ruleValueContainer != null) {
            ruleValueContainer.setEnabled(enabled);
        }
        //Enabling/Disabling the filter widget is currently unsupported
        /*if (filterWidgetContainer != null) {
        filterWidgetContainer.setEnabled(enabled);
        }*/
    }

    @Override
    public boolean setPropertiesFromComponent(T component) {
        if (component != null) {

            /* Rule */
            //the rule is one of the few options that cannot be left null so specify a sensible default one when none is already set
            if (component.getSelection()==null)
                ruleKeyLabel.setCaption(I18nTranslator.getInstance().unknown());
            
            detachNextEmbeddedComponent();
            if (component.getSelection()!=null)
                switch (component.getSelection()) {
                    case random:
                        ruleKeyLabel.setCaption(I18nTranslator.getInstance().random_selection());
                        attachNextEmbeddedComponent(new GWTReadOnlyPSlotOptionalCriterion(I18nTranslator.getInstance().sort_first(), component.getSubSelection()));
                        break;
                    case weightedRandom:
                        ruleKeyLabel.setCaption(I18nTranslator.getInstance().weighted_random_selection());
                        attachNextEmbeddedComponent(new GWTReadOnlyPSlotMandatoryCriterion(I18nTranslator.getInstance().likeliness(), component.getSubSelection()));
                        break;
                    case orderedSequence:
                        ruleKeyLabel.setCaption(I18nTranslator.getInstance().ordered_selection());
                        attachNextEmbeddedComponent(new GWTReadOnlyPSlotMandatoryCriterion(I18nTranslator.getInstance().sort_first(), component.getSubSelection()));
                        break;
                }

            /* Filters */
            //Do only delegate its display if there's at least one to take care of
            filterPanel.setVisible(!component.getFilters().isEmpty());
            if (!component.getFilters().isEmpty()) {
                if (delegatedSlotRefreshHandler != null) {
                    delegatedSlotRefreshHandler.onFiltersCaptionRefresh((V) component.getFilters(),filterWidgetContainer);
                }
            }

            /* Misc. options */
            boolean isUnrestrictedReplayAllowed = true;
            if (component.getNoReplayDelay() != null && component.getNoReplayDelay() != 0) {
                noReplayTimeLabel.setCaption(I18nTranslator.getInstance().no_replay_until_x_minutes(component.getNoReplayDelay().toString()));
                isUnrestrictedReplayAllowed = false;
            }
            if (component.isNoReplayInPlaylist()) {
                noReplayPlaylistLabel.setCaption(I18nTranslator.getInstance().no_replay_within_playlist());
                isUnrestrictedReplayAllowed = false;
            }
            miscOptionsPanel.setVisible(!isUnrestrictedReplayAllowed);

            return true;
        }
        return false;
    }

    @Override
    public T getComponentFromProperties() {
        //handled elsewhere
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clean() {
        super.clean();
        ruleKeyLabel.clean();
        detachNextEmbeddedComponent();
        noReplayTimeLabel.clean();
        noReplayPlaylistLabel.clean();
        filterWidgetContainer.clean();
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        delegatedSlotRefreshHandler = handler;
        //No use here, pass along and notify sub-components
        if (ruleValueContainer != null) {
            ruleValueContainer.setSlotRefreshHandler(handler);
        }
    }
}
