/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.SubSelectionModule;
import biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTReadOnlyPSlotOptionalCriterion<T extends SubSelectionModule, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericPlaylistSlotComponentInterface<T, U, V> {

    //Options for weight random selection
    private GWTDisclosurePanel subSelectionPanel;
    private GWTLabel sortLabel;
    private GWTLabel limitLabel;

    public GWTReadOnlyPSlotOptionalCriterion(String caption, T form) {
        super();

        subSelectionPanel = new GWTDisclosurePanel(I18nTranslator.getInstance().limit());
        //Custom CSS
        subSelectionPanel.setStyleName("GWTPSlotCriterion");
        //DisclosurePanels can only contain two widgets at most
        GWTVerticalPanel dummyPanel1 = new GWTVerticalPanel();

        GWTGrid sortGrid = new GWTGrid();
        sortLabel = new GWTLabel(null);
        sortGrid.addRow(
                new GWTLabel(caption + ":"),
                sortLabel);
       
        limitLabel = new GWTLabel(null);
        sortGrid.addRow(
                limitLabel);
        dummyPanel1.add(sortGrid);

        setPropertiesFromComponent(form);

        subSelectionPanel.add(dummyPanel1);

        attachMainWidget(subSelectionPanel);
    }

    @Override
    public void setEnabled(boolean enabled) {       
        //not much to do, really
    }

    @Override
    public boolean setPropertiesFromComponent(T component) {
        subSelectionPanel.setVisible(component != null);
        if (component != null) {
            sortLabel.setCaption(
                    component.getSort()!=null ?
                    component.getSort().getI18nFriendlyString() :
                    I18nTranslator.getInstance().unknown());
            if (component.getFrom()!=null && component.getTo()!=null) {
                limitLabel.setVisible(true);
                limitLabel.setCaption(I18nTranslator.getInstance().limit_selection_from_index_x_to_index_y(
                        component.getFrom().toString(),
                        component.getTo().toString()));
            } else if (component.getFrom()!=null) {
                limitLabel.setVisible(true);
                limitLabel.setCaption(I18nTranslator.getInstance().limit_selection_from_index_x(
                        component.getFrom().toString()));
            } else if (component.getTo()!=null) {
                limitLabel.setVisible(true);
                limitLabel.setCaption(I18nTranslator.getInstance().limit_selection_to_index_x(
                        component.getTo().toString()));
            } else {
                limitLabel.setVisible(false);
                limitLabel.setCaption(null);
            }
        }
        return true;
    }

    @Override
    public T getComponentFromProperties() {
        //handled elsewhere
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clean() {
        super.clean();
        sortLabel.clean();
        limitLabel.clean();
    }

    public void attachNextEmbeddedComponent(GenericEditablePlaylistSlotComponentInterface<AlphabeticalFilterForm, U, V> newComponent) {
        //Do nothing
    }

    public void detachNextEmbeddedComponent() {
        //Do nothing
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
