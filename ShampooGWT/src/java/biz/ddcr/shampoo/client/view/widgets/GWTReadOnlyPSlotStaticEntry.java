/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTReadOnlyPSlotStaticEntry<T extends StaticPlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericPlaylistSlotComponentInterface<T, U, V> {

    final protected GWTFlowPanel container;
    //local copy of the inside form
    protected ActionCollectionEntry<String> trackFormId;
    //Callback to the underlying presentation layer in order to notify it a user edit tracks
    protected SlotRefreshHandler<U, V> delegatedSlotRefreshHandler = null;

    public GWTReadOnlyPSlotStaticEntry(T form) {
        this(form, null);
    }

    public GWTReadOnlyPSlotStaticEntry(T form, SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super();
        container = new GWTFlowPanel();
        //Custom CSS
        container.setStyleName("GWTPSlotEntry");

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        setPropertiesFromComponent(form);

        attachMainWidget(container);
    }

    @Override
    public void setEnabled(boolean enabled) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean setPropertiesFromComponent(T component) {
        trackFormId = component != null ? component.getBroadcastTrackId() : null;
        if (delegatedSlotRefreshHandler != null) {
            delegatedSlotRefreshHandler.onTrackCaptionRefresh((U) trackFormId, container);
        }
        return (trackFormId != null);
    }

    @Override
    public T getComponentFromProperties() {
        //handled elsewhere
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clean() {
        super.clean();
        container.clean();
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        delegatedSlotRefreshHandler = handler;
    }
}
