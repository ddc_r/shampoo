/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTReadOnlyPlaylist<T extends PlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GWTExtendedWidget implements GenericPlaylistInterface<T, U, V> {

    protected GWTGrid widget;
    //Callback to the underlying presentation layer in order to update relevant widgets with external data
    private SlotRefreshHandler<U, V> delegatedSlotRefreshHandler = null;

    public GWTReadOnlyPlaylist() {
        this(null);
    }

    public GWTReadOnlyPlaylist(SlotRefreshHandler<U, V> playlistSlotRefreshHandler) {
        super();
        GWTScrollPanel scrollPanel = new GWTScrollPanel();
        scrollPanel.add(buildUI());

        setSlotRefreshHandler(playlistSlotRefreshHandler);

        attachMainWidget(scrollPanel);
    }

    protected Widget buildUI() {
        GWTGrid gridContainer = new GWTGrid();
        //Custom CSS
        gridContainer.setStyleName("GWTPlaylist");
        widget = new GWTGrid();
        widget.setHeight("100%");
        gridContainer.addRow(widget);
        //Header
        /*addHeader(
                I18nTranslator.getInstance().index(),
                I18nTranslator.getInstance().slot());*/
        return gridContainer;
    }

    @Override
    public void clean() {
        super.clean();
        for (int i = widget.getRowCount() - 1; i >= 0; i--) {
            widget.removeRow(i);
        }
    }

    @Override
    public int getSlotCount() {
        return widget.getRowCount();
    }

    protected boolean addFinalizedSlot(GWTReadOnlyPSlot slot) {
        return insertFinalizedSlotAt(slot, getSlotCount());
    }

    protected boolean insertFinalizedSlotAt(GWTReadOnlyPSlot slot, int beforeIndex) {
        if (slot != null && beforeIndex >= 0 && beforeIndex <= getSlotCount()) {

            //sequence index
            GWTLabel sequenceIndex = new GWTLabel(Integer.toString(beforeIndex + 1));

            widget.insertRowAt(beforeIndex,
                    sequenceIndex,
                    slot);

            renumberNextSlots(beforeIndex);

            return true;
        }
        return false;
    }

    private void renumberNextSlots(int startingIndex) {
        for (int i = (startingIndex + 1); i < widget.getRowCount(); i++) {

            //new sequence index
            GWTLabel newSequenceIndex = new GWTLabel(Integer.toString(i + 1));

            widget.setWidgetAt(i, /** index 0 is the seq. index*/ 0, newSequenceIndex);
        }
    }

    protected int getCurrentSlot(ClickEvent event) {
        return widget.getCurrentRow(event);
    }

    @Override
    public boolean removeSlot(int index) {
        if (index < getSlotCount() && index >= 0) {

            widget.removeRow(index);
            renumberNextSlots(index - 1);

            return true;
        }
        return false;
    }

    @Override
    public T getFormAt(int index) {
        if (index >= 0 && index < getSlotCount()) {
            GWTReadOnlyPSlot s = (GWTReadOnlyPSlot) widget.getWidgetAt(index, /* index 1 is the slot widget*/1);
            if (s != null) {
                PlaylistEntryForm p = s.getComponentFromProperties();
                if (p != null) {
                    //Update its sort order and keep it in sync
                    p.setSequenceIndex(index);
                    return (T) p;
                }
            }
        }
        return null;
    }

    @Override
    public boolean setFormAt(int index, T form) {
        if (index >= 0 && index < getSlotCount()) {
            GWTReadOnlyPSlot s = (GWTReadOnlyPSlot) widget.getWidgetAt(index, /* index 1 is the slot widget*/1);
            if (s != null) {
                return s.setPropertiesFromComponent(form);
            }
        }
        return false;
    }

    @Override
    public GWTReadOnlyPSlot getSlotAt(int index) {
        if (index >= 0 && index < getSlotCount()) {
            return (GWTReadOnlyPSlot) widget.getWidgetAt(index, /* index 1 is the slot widget*/1);
        }
        return null;
    }

    /*protected void addHeader(String... captions) {
        if (captions != null) {

            Widget[] ws = new Widget[captions.length];
            int colIndex = 0;
            //Now add an header for each programmatically bound widget
            for (String caption : captions) {
                ws[colIndex++] = new GWTLabel(caption);
            }
            widget.insertRowAt(0, ws);
        }
    }*/

    @Override
    public Collection<T> getAllForms() {
        Collection<PlaylistEntryForm> results = new ArrayList<PlaylistEntryForm>();
        for (int i = 0; i < getSlotCount(); i++) {
            results.add(getFormAt(i));
        }
        return (Collection<T>) results;
    }

    @Override
    public boolean appendForms(Collection<T> forms) {
        if (forms != null) {

            for (PlaylistEntryForm form : forms) {

                //Convert forms into widgets
                addFinalizedSlot(new GWTReadOnlyPSlot(form, delegatedSlotRefreshHandler));

            }

            return true;
        }
        return false;
    }

    @Override
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler) {
        delegatedSlotRefreshHandler = handler;
        //No use here, pass along and notify sub-components
        for (int index = getSlotCount()-1; index >= 0; index--) {
            GWTReadOnlyPSlot s = (GWTReadOnlyPSlot) widget.getWidgetAt(index, /* index 1 is the slot widget*/1);
            if (s != null) {
                s.setSlotRefreshHandler(delegatedSlotRefreshHandler);
            }
        }
    }

    @Override
    public int getCurrentRow(ClickEvent event) {
        return widget.getCurrentRow(event);
    }

}
