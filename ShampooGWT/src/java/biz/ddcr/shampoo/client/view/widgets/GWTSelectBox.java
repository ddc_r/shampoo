/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.ListBox;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class GWTSelectBox extends GWTExtendedWidget implements GenericSelectBoxInterface {

    final private ListBox widget;

    public GWTSelectBox() {
        super();
        GWTScrollPanel scrollPanel = new GWTScrollPanel();
        widget = new ListBox(true);
        //Custom CSS
        widget.setStyleName("GWTSelectBox");
        scrollPanel.add(widget);
        attachMainWidget(scrollPanel);
    }

    @Override
    public void addItem(String key, String value) {
        widget.addItem(key, value);
    }

    @Override
    public int getItemCount() {
        return widget.getItemCount();
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return widget.addChangeHandler(handler);
    }

    @Override
    public Collection<String> getSelectedValues() {
        Collection<String> results = null;
        for (int i = 0; i < widget.getItemCount(); i++) {
            if (widget.isItemSelected(i)) {
                if (results==null) results = new ArrayList<String>();
                results.add(widget.getValue(i));
            }
        }
        return results;
    }

    @Override
    public void clean() {
        super.clean();
        widget.clear();
    }

}
