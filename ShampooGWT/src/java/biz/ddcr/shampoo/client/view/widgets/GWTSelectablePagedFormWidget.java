/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright
 *
 */
public abstract class GWTSelectablePagedFormWidget<T, U, V extends Widget> extends GWTPagedFormWidget<T, U, V> {

    public GWTSelectablePagedFormWidget(V widget) {
        super(widget);
    }
    
    protected U getVisibleSelectedValue() {
        int selectedIndex = getVisibleSelectedIndex();
        if (selectedIndex>=0) {
            PagedItem<T,U> item = items.get(selectedIndex + firstVisibleItemOffset);
            if (item!=null) return item.getValue();
        }
        return null;
    }
    protected PagedItem<T,U> getVisibleSelectedItem() {
        int selectedIndex = getVisibleSelectedIndex();
        if (selectedIndex>=0) {
            PagedItem<T,U> item = items.get(selectedIndex + firstVisibleItemOffset);
            if (item!=null) return item;
        }
        return null;
    }
    protected abstract int getVisibleSelectedIndex();

    /*@Override
    protected boolean refreshVisibleItems(int startOffset, boolean forceRefresh) {
        if (super.refreshVisibleItems(startOffset, forceRefresh)) {
            onVisibleSelectionChanged(0);
            return true;
        }
        return false;
    }

    @Override
    protected boolean removePagedItem(boolean allowNullKey, T key, U value) {
        int previousSelection = getVisibleSelectedIndex();
        if (super.removePagedItem(allowNullKey, key, value)) {
            int currentSelection = getVisibleSelectedIndex();
            if (previousSelection!=currentSelection)
                onVisibleSelectionChanged(currentSelection);
            return true;
        }
        return false;
    }

    @Override
    protected boolean removePagedItems(boolean allowNullKey, Collection<PagedItem<T, U>> keyValues) {
        int previousSelection = getVisibleSelectedIndex();
        if (super.removePagedItems(allowNullKey, keyValues)) {
            int currentSelection = getVisibleSelectedIndex();
            if (previousSelection!=currentSelection)
                onVisibleSelectionChanged(currentSelection);
            return true;
        }
        return false;
    }

    @Override
    protected boolean addPagedItem(boolean allowNullKey, T key, U value) {
        int previousSelection = getVisibleSelectedIndex();
        if (super.addPagedItem(allowNullKey, key, value)) {
            int currentSelection = getVisibleSelectedIndex();
            if (previousSelection!=currentSelection)
                onVisibleSelectionChanged(currentSelection);
            return true;
        }
        return false;
    }

    @Override
    protected boolean addPagedItems(boolean allowNullKey, Collection<PagedItem<T, U>> keyValues) {
        int previousSelection = getVisibleSelectedIndex();
        if (super.addPagedItems(allowNullKey, keyValues)) {
            int currentSelection = getVisibleSelectedIndex();
            if (previousSelection!=currentSelection)
                onVisibleSelectionChanged(currentSelection);
            return true;
        }
        return false;
    }*/

    protected boolean setPagedSelectedValue(U selectedValue) {
        if (selectedValue!=null) {
            int position = 0;
            for (PagedItem<T,U> item : items) {
                if (selectedValue.equals(item.getValue())) {
                    //get the corresponding page index
                    refreshVisibleItems((position / MAX_VISIBLE_ITEMS) * MAX_VISIBLE_ITEMS);
                    setVisibleSelectedIndex(position % MAX_VISIBLE_ITEMS);
                    return true;
                }
                position++;
            }
        }
        return false;
    }
    protected abstract void setVisibleSelectedIndex(int index);
    
}
