/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;

/**
 *
 * @author okay_awright
 **/
public class GWTStatusBox<U extends I18nFriendlyInterface> extends GWTExtendedWidget implements GenericStatusBoxInterface<String,U> {

    private String id;
    final private GWTLabel statusLabel;

    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private GenericStatusChangeHandler<String,U> delegatedStatusChange = null;

    public GWTStatusBox(String id, GenericStatusChangeHandler<String,U> statusChangeHandler) {
        this(id);
        setStatusChangeHandler(statusChangeHandler);
    }
    public GWTStatusBox(String id, U status) {
        this(id);
        setStatus(status);
    }
    public GWTStatusBox(String id) {
        this();
        setID(id);
    }
    public GWTStatusBox() {
        super();
        statusLabel = new GWTLabel(null);
        //Custom CSS
        statusLabel.setStyleName("GWTStatusBox");

        attachMainWidget(statusLabel);
    }

    @Override
    public void setEnabled(boolean enabled) {
        //Do nothing
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public void setStatus(U status) {
        if (status==null) {
            clean();
        } else {
            statusLabel.setStyleName("GWTStatusBox-"+status.toString());
            statusLabel.setCaption(status.getI18nFriendlyString());
        }
    }

    @Override
    public void clean() {
        super.clean();
        statusLabel.setStyleName("GWTStatusBox");
        statusLabel.setCaption(null);
    }

    @Override
    public void setStatusChangeHandler(GenericStatusChangeHandler<String,U> handler) {
        delegatedStatusChange = handler;
        //refresh the status ASAP
        refreshStatus();
    }

    @Override
    public void refreshStatus() {
        if (delegatedStatusChange!=null)
                delegatedStatusChange.onGet(this);
    }

}
