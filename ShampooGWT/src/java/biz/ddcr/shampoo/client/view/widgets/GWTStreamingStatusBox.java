/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.i18n.I18nFriendlyInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GWTStreamingStatusBox.STATUS;
import biz.ddcr.shampoo.client.view.widgets.GenericStatusBoxInterface.GenericStatusChangeHandler;

/**
 *
 * @author okay_awright
 **/
public class GWTStreamingStatusBox extends GWTExtendedWidget implements GenericStatusBoxInterface<String,STATUS> {

    public static enum STATUS implements I18nFriendlyInterface {
        reserved,
        free,
        onair,
        offair;
        
        private STATUS() {
        }

        @Override
        public String getI18nFriendlyString() {
            switch (this) {
                case reserved:
                    return I18nTranslator.getInstance().connecting();
                case free:
                    return I18nTranslator.getInstance().disconnected();
                case onair:
                    return I18nTranslator.getInstance().on_air();
                case offair:
                    return I18nTranslator.getInstance().off_air();
                default:
                    return I18nTranslator.getInstance().unknown();
            }
        }
        
    }
    
    private String id;
    final private GWTLabel statusLabel;

    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private GenericStatusChangeHandler<String,STATUS> delegatedStatusChange = null;

    public GWTStreamingStatusBox(String id, GenericStatusChangeHandler<String,STATUS> statusChangeHandler) {
        this(id);
        setStatusChangeHandler(statusChangeHandler);
    }
    public GWTStreamingStatusBox(String id, STATUS status) {
        this(id);
        setStatus(status);
    }
    public GWTStreamingStatusBox(String id) {
        this();
        setID(id);
    }
    public GWTStreamingStatusBox() {
        super();
        statusLabel = new GWTLabel(null);
        //Custom CSS
        statusLabel.setStyleName("GWTStatusBox");

        attachMainWidget(statusLabel);
    }

    @Override
    public void setEnabled(boolean enabled) {
        //Do nothing
    }

    @Override
    public String getID() {
        return id;
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public void setStatus(STATUS status) {
        if (status==null) {
            clean();
        } else {
            statusLabel.setStyleName("GWTStatusBox-"+status.name());
            statusLabel.setCaption(status.getI18nFriendlyString());
        }
    }

    @Override
    public void clean() {
        super.clean();
        statusLabel.setStyleName("GWTStatusBox");
        statusLabel.setCaption(null);
    }

    @Override
    public void setStatusChangeHandler(GenericStatusChangeHandler<String,STATUS> handler) {
        delegatedStatusChange = handler;
        //refresh the status ASAP
        refreshStatus();
    }

    @Override
    public void refreshStatus() {
        if (delegatedStatusChange!=null)
                delegatedStatusChange.onGet(this);
    }

}
