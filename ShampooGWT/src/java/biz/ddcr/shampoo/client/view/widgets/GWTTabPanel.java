/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.TabPanel;
import java.util.ArrayList;
import java.util.TreeMap;

/**
 *
 * @author okay_awright
 **/
public class GWTTabPanel extends GWTExtendedWidget implements GenericTabPanelInterface {

    final private TabPanel widget;
    final private TreeMap<Integer, ClickHandler> clickHandlers;
    //A list that enforce the respective order of a tab when inserted
    //such as FIXED_INTERNAL_INDEX = tabOrders.get( VISIBLE_ORDER )
    final private ArrayList<Integer> tabOrders;

    public GWTTabPanel() {
        super();
        widget = new TabPanel();
        //Custom CSS
        widget.setStyleName("GWTTabPanel");
        clickHandlers = new TreeMap<Integer, ClickHandler>();
        tabOrders = new ArrayList<Integer>();
        attachMainWidget(widget);
    }

    @Override
    public void clean() {
        super.clean();
        clickHandlers.clear();
        tabOrders.clear();
        widget.clear();
    }

    @Override
    public void add(PluggableWidgetInterface widget, String title) {
        this.widget.add(widget.getWidget(), title);
        //Add a default order because it is not enforced
        tabOrders.add(getTabCount()-1);
    }

    @Override
    public void insertAt(PluggableWidgetInterface widget, String title, int requiredIndex) {
        if (requiredIndex>-1) {
            //Check if a tab with a highest order is not already displayed, then create the new one just before
            for (int realOrder = 0; realOrder<tabOrders.size(); realOrder++) {
                int currentOrder = tabOrders.get(realOrder).intValue();
                if (currentOrder==requiredIndex) {
                    //there's a tab that shares the same order: put the new one here and shift the next orders by 1
                    for (int shiftOrder=currentOrder; shiftOrder<tabOrders.size(); shiftOrder++)
                        tabOrders.set(shiftOrder, tabOrders.get(shiftOrder)+1);
                }
                if (currentOrder>=requiredIndex) {
                    //There's a highest order tab placed before the required position: create the new tab just before
                    this.widget.insert(widget.getWidget(), title, realOrder);
                    tabOrders.add(realOrder, requiredIndex);
                    return;
                }
            }
        }
        //if we get there then just append the tab at the end
        this.widget.add(widget.getWidget(), title);
        tabOrders.add(requiredIndex);
    }

    public int findVisibleTabIndex(int internalIndex) {
        int i = 0;
        for (int j : tabOrders)
            if (j==internalIndex)
                return i;
            else
                i++;
        return -1;
    }

    /**
     *
     * Register a click handler for the given tab
     *
     * @param handler
     * @param index
     * @return
     **/
    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler, int index) {
        int visibleIndex = -1;
        if (index > -1 && (visibleIndex = findVisibleTabIndex(index))>-1) {
            clickHandlers.put(visibleIndex, handler);
            return widget.getTabBar().getTab(visibleIndex).addClickHandler(handler);
        } else
            return null;
    }

    /**
     * Return -1 if there's no available tab
     *
     * @return
     **/
    @Override
    public int getSelectedTabIndex() {
        int visibleSelectedTab = widget.getTabBar().getSelectedTab();
        if (tabOrders.size()>0 && visibleSelectedTab>-1)
            return tabOrders.get(visibleSelectedTab);
        else
            return -1;
    }

    @Override
    public PluggableWidgetInterface getTabWidgetAt(int index) {
        int visibleIndex = -1;
        if (index > -1 && (visibleIndex = findVisibleTabIndex(index))>-1)
            return (PluggableWidgetInterface) widget.getWidget(visibleIndex);
        else
            return null;
    }

    @Override
    public boolean selectTabAt(int index) {
        int visibleIndex = -1;
        //Do only select a tab if it's not already selected
        if (index > -1 && getSelectedTabIndex()!=index && (visibleIndex = findVisibleTabIndex(index))>-1) {
            widget.selectTab(visibleIndex);
            //Force a virtual click event to occur (but that should not really click on the tab) since selectTab doesn't trigger this kind of event
            ClickHandler handler = clickHandlers.get(visibleIndex);
            if (handler!=null)
               handler.onClick(null);
            return true;
        }
        return false;
    }

    @Override
    public boolean selectFirstTab() {
        for (int i=0; i<tabOrders.size(); i++)
            if (selectTabAt(tabOrders.get(i)))
                return true;
        return false;
    }

    @Override
    public boolean selectLastTab() {
        for (int i=tabOrders.size()-1; i>-1; i--)
            if (selectTabAt(tabOrders.get(i)))
                return true;
        return false;
    }

    @Override
    public int getTabCount() {
        return tabOrders.size();
    }


}
