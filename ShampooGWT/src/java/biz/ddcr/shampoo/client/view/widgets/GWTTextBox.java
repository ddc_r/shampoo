/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.TextBox;

/**
 *
 * @author okay_awright
 **/
public class GWTTextBox extends GWTExtendedWidget implements GenericTextBoxInterface {

    final private TextBox widget;
    final private ChangeHandler fixTextHandler;

    public GWTTextBox() {
        super();
        widget = new TextBox();
        //Custom CSS
        widget.setStyleName("GWTTextBox");
        fixTextHandler = new ChangeHandler() {

            @Override
            public void onChange(ChangeEvent ce) {
                //send the current text through the setter, that in turn will properly reformat it
                setText(getText());
            }
        };
        widget.addChangeHandler(fixTextHandler);
        attachMainWidget(widget);
    }

    @Override
    public void setEnabled(boolean enabled) {
        widget.setEnabled(enabled);
    }

    @Override
    public void setText(String text) {
        //Fix text if necessary
        widget.setText(text!=null ? text.trim() : null);
    }

    @Override
    public String getText() {
        return widget.getText();
    }

    @Override
    public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
        return widget.addKeyPressHandler(handler);
    }

    @Override
    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return widget.addChangeHandler(handler);
    }

    @Override
    public void clean() {
        super.clean();
        widget.setText(null);
    }
}
