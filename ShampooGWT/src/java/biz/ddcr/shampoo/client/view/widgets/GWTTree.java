/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.resource.ResourceResolver;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import java.util.TreeMap;

/**
 *
 * @author okay_awright
 **/
public class GWTTree extends GWTExtendedWidget implements GenericTreeInterface<Long, Widget> {

    /** Don't let GWT trees load their default pictures, CSS is the way.
     * As usual, since you can't override most of the GWT widget functions without duplicating lots of code, the easiest soultion is to make those icons blank
     * DON'T make it too small because it's internally used as a button to expnad/collate branches
     * It's really a dirty hack, just like the way the original GWT tree is made
     * UPDATE: Too many artifacts, reuse the Resources the proper way the tree so as it handles them. No CSS anymore, unfortunately.
     */
    public static class UnStylableTreeResources implements Tree.Resources {

        protected static final ImageResource dummyImageResource = new ImageResource() {

            @Override
            public int getHeight() {
                return 0;
            }

            @Override
            public int getLeft() {
                return 0;
            }

            @Override
            public int getTop() {
                return 0;
            }

            @Override
            public String getURL() {
                return "";
            }

            @Override
            public int getWidth() {
                return 0;
            }

            @Override
            public boolean isAnimated() {
                return false;
            }

            @Override
            public String getName() {
                return "";
            }
            
            @Override
            public SafeUri getSafeUri() {
                return UriUtils.fromSafeConstant(getURL());
            }
        };
        
        protected static final ImageResource collapseImageResource = new ImageResource() {

            @Override
            public int getHeight() {
                return 8;
            }

            @Override
            public int getLeft() {
                return 0;
            }

            @Override
            public int getTop() {
                return 0;
            }

            @Override
            public String getURL() {
                return GWT.getModuleBaseURL() + ResourceResolver.getInstance().CollapseNodeGetRelativeEndpoint();
            }

            @Override
            public int getWidth() {
                return 8;
            }

            @Override
            public boolean isAnimated() {
                return false;
            }

            @Override
            public String getName() {
                return "";
            }
            
            @Override
            public SafeUri getSafeUri() {
                return UriUtils.fromSafeConstant(getURL());
            }
        };
        
        protected static final ImageResource expandImageResource = new ImageResource() {

            @Override
            public int getHeight() {
                return 8;
            }

            @Override
            public int getLeft() {
                return 0;
            }

            @Override
            public int getTop() {
                return 0;
            }

            @Override
            public String getURL() {
                return GWT.getModuleBaseURL() + ResourceResolver.getInstance().ExpandNodeGetRelativeEndpoint();
            }

            @Override
            public int getWidth() {
                return 8;
            }

            @Override
            public boolean isAnimated() {
                return false;
            }

            @Override
            public String getName() {
                return "";
            }

            @Override
            public SafeUri getSafeUri() {
                return UriUtils.fromSafeConstant(getURL());
            }
        };
        
        @Override
        public ImageResource treeClosed() {
            return expandImageResource;
        }

        @Override
        public ImageResource treeLeaf() {
            return dummyImageResource;
        }

        @Override
        public ImageResource treeOpen() {
            return collapseImageResource;
        }
    }    
    protected static final Tree.Resources DUMMY_RESOURCES = new UnStylableTreeResources();
    
    final private Tree widget;
    final private TreeMap<Long, TreeItem> items;
    final private TreeMap<String, Long> itemsByName;
    /** autoincremented long handle of the latest created barnch **/
    private long latestItemId;

    public GWTTree() {
        super();
        GWTScrollPanel scrollPanel = new GWTScrollPanel();
        widget = new Tree(DUMMY_RESOURCES);
        //Custom CSS
        widget.setStyleName("GWTTree");
        items = new TreeMap<Long, TreeItem>();
        itemsByName = new TreeMap<String, Long>();
        latestItemId = 0;
        scrollPanel.add(widget);
        attachMainWidget(scrollPanel);
    }

    private long getNewItemId() {
        return ++latestItemId;
    }

    @Override
    public Long addItem(Widget localWidget) {
        if (localWidget != null) {
            TreeItem newTreeItem = widget.addItem(localWidget);
            //Custom CSS
            newTreeItem.setStyleName("GWTTreeItem");
            //update the item referency list
            long newId = getNewItemId();
            items.put(newId, newTreeItem);
            return newId;
        }
        return null;
    }

    @Override
    public Long getItemByName(String name) {
        if (name != null) {
            return itemsByName.get(name);
        }
        return null;
    }
    
    @Override
    public Long addItemWithName(String name, Widget localWidget) {
        if (name != null && localWidget != null) {
            Long newId = addItem(localWidget);
            if (newId!=null)
                itemsByName.put(name, newId);
            return newId;
        }
        return null;
    }
    
    @Override
    public Long addItem(Widget localWidget, Long itemIdContainer) {
        if (localWidget != null && itemIdContainer != null) {
            TreeItem containerTreeItem = null;
            if ((containerTreeItem = items.get(itemIdContainer)) == null) {
                return addItem(localWidget);
            }
            //Add this entry to the corresponding branch
            TreeItem newTreeItem = containerTreeItem.addItem(localWidget);
            //Custom CSS
            newTreeItem.setStyleName("GWTTreeItem");
            //update the item referency list
            long newId = getNewItemId();
            items.put(newId, newTreeItem);
            return newId;
        }
        return null;
    }

    @Override
    public void clean() {
        super.clean();
        widget.clear();
        items.clear();
        itemsByName.clear();
        latestItemId = 0;
    }
}
