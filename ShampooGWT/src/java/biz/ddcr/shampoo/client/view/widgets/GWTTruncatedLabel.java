/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

/**
 *
 * A label which text is truncated beyond a certian number of characters
 * Can be folded and unfolded when clicked on
 *
 * @author okay_awright
 **/
public class GWTTruncatedLabel extends GWTLabel {

    protected static final int MAX_N_OF_CHARS = 64;
    protected String pristineText;
    protected boolean folded = true; //Folded by default
    protected final ClickHandler folder = new ClickHandler() {

        @Override
        public void onClick(ClickEvent ce) {
            folded = !folded;
            setCaption(pristineText);
        }
    };

    public GWTTruncatedLabel(String text) {
        super(text);
        widget.addClickHandler(folder);
    }

    public boolean isFolded() {
        return folded;
    }

    @Override
    public void setCaption(String text) {
        super.setCaption(cutText(text));
    }

    private String cutText(String text) {
        pristineText = text;
        if (text != null && text.length()!=0 && folded && text.length() > MAX_N_OF_CHARS) {
            return text.substring(0, MAX_N_OF_CHARS - 3) + "...";
        } else {
            return text;
        }
    }
}
