/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;
import java.util.Date;

/**
 *
 * @author okay_awright
 **/
public class GWTUncachableImage extends GWTImage {

    public GWTUncachableImage() {
        super();
    }

    public GWTUncachableImage(String url) {
        super(url);
    }

    public GWTUncachableImage(HasCoverArtInterface form) {
        super(form);
    }

    @Override
    public void setImage(String url) {
        /** defeat browser caching by not hitting hit it when updating the image content through this method*/
        String uncachableUrl = url!=null&&!url.isEmpty()?(url + (url.lastIndexOf("?")>-1?"&":"?") + "nocache=" + (new Date()).getTime()):"";
        super.setImage(uncachableUrl);
    }

    
}
