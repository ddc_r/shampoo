/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package biz.ddcr.shampoo.client.view.widgets;

/**
 *
 * @author okay_awright
 **/
public class GWTVerticalPanelWithTag extends GWTVerticalPanel implements PluggableWidgetWithTagInterface {

    private final GWTFlowPanel caption;
    private final GWTVerticalPanel content;
    private short tag = 0;

    public GWTVerticalPanelWithTag(short tag) {
        super();
        caption = new GWTFlowPanel();
        //Custom CSS
        caption.setStyleName("caption");
        content = new GWTVerticalPanel();
        //Custom CSS
        content.setStyleName("content");
        setTag(tag);
        add(caption);
        add(content);
    }

    @Override
    public void setTag(short tag) {
        this.tag = tag;
    }

    @Override
    public short getTag() {
        return tag;
    }

    public GenericPanelInterface getCaption() {
        return caption;
    }

    public GenericPanelInterface getContent() {
        return content;
    }

}
