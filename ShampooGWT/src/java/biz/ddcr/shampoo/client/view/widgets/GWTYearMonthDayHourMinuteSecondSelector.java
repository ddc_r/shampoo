/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.date.HourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.JSHourMinute;
import biz.ddcr.shampoo.client.helper.date.JSSecondMillisecond;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.client.helper.date.SecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.i18n.I18nTranslator;
import biz.ddcr.shampoo.client.view.widgets.GenericCalendarInterface.DateChangeHandler;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;

/**
 *
 * @author okay_awright
 *
 */
public class GWTYearMonthDayHourMinuteSecondSelector extends GWTExtendedWidget implements GenericYearMonthDayHourMinuteSecondSelectorInterface {

    private GWTPopupCalendar dateCalendar = null;
    private GWTNumericBox yearNumericBox = null;
    private GWTPagedFormDropDownListBox<Byte> monthComboBox = null;
    private GWTPagedFormDropDownListBox<Byte> dayComboBox = null;
    private GWTPagedFormDropDownListBox<Byte> hourComboBox = null;
    private GWTPagedFormDropDownListBox<Byte> minuteComboBox = null;
    private GWTPagedFormDropDownListBox<Byte> secondComboBox = null;
    private YearMonthDayHourMinuteSecondMillisecondInterface currentTime = null;
    //Callback to the underlying presentation layer in order to notify it a user clicked on the header or a button
    private YearMonthDayHourMinuteSecondTimeChangeHandler delegatedTimeChange = null;
    final private ChangeHandler onChangeHourMinute = new ChangeHandler() {
        @Override
        public void onChange(ChangeEvent event) {
            setValue(computeValue());
            onAfterValueSet();
        }
    };
    final private DateChangeHandler onChangeCalendar = new DateChangeHandler() {
        @Override
        public void onDateChange(YearMonthDayInterface newDate) {
            setValue(computeValue());
            //Update the alternative date inputs with this date
            updateYearMonthDay(currentTime);
            onAfterValueSet();
        }
    };

    private void onChangeData() {
        //First pass this new date to the calendar that will check for any possible incorrect date
        JSYearMonthDay d = new JSYearMonthDay(
                getCurrentTime().getTimeZone(),
                yearNumericBox.getValue().shortValue(),
                monthComboBox.getSelectedValue(),
                dayComboBox.getSelectedValue());
        dateCalendar.setCurrentDate(d);

        setValue(computeValue());
        //And then modify back the input in case something changed
        updateYearMonthDay(getCurrentTime());

        onAfterValueSet();
    }
    private BlurHandler onChangeYear = new BlurHandler() {
        @Override
        public void onBlur(BlurEvent event) {
            onChangeData();
        }
    };
    private ChangeHandler onChangeMonthDay = new ChangeHandler() {
        @Override
        public void onChange(ChangeEvent event) {
            onChangeData();
        }
    };

    public GWTYearMonthDayHourMinuteSecondSelector() {
        this(null);
    }

    public GWTYearMonthDayHourMinuteSecondSelector(YearMonthDayHourMinuteSecondMillisecondInterface date) {
        super();

        GWTGrid grid = new GWTGrid();
        //Custom CSS
        grid.setStyleName("GWTDateSelector");
        dateCalendar = new GWTPopupCalendar(I18nTranslator.getInstance().pick_date());
        dateCalendar.setDateChangeHandler(onChangeCalendar);
        yearNumericBox = new GWTNumericBox(1970, 1, 2100);
        yearNumericBox.addBlurHandler(onChangeYear);
        monthComboBox = new GWTPagedFormDropDownListBox<Byte>();
        monthComboBox.addChangeHandler(onChangeMonthDay);
        for (byte i = 1; i < 13; i++) {
            monthComboBox.addItem(JSHourMinute.toDoubleDigitIfRequired(i), i);
        }
        dayComboBox = new GWTPagedFormDropDownListBox<Byte>();
        dayComboBox.addChangeHandler(onChangeMonthDay);
        for (byte i = 1; i < 32; i++) {
            dayComboBox.addItem(JSHourMinute.toDoubleDigitIfRequired(i), i);
        }
        hourComboBox = new GWTPagedFormDropDownListBox<Byte>();
        hourComboBox.addChangeHandler(onChangeHourMinute);
        for (byte i = 0; i < 24; i++) {
            hourComboBox.addItem(JSHourMinute.toDoubleDigitIfRequired(i), i);
        }
        minuteComboBox = new GWTPagedFormDropDownListBox<Byte>();
        minuteComboBox.addChangeHandler(onChangeHourMinute);
        for (byte i = 0; i < 60; i++) {
            minuteComboBox.addItem(JSHourMinute.toDoubleDigitIfRequired(i), i);
        }
        secondComboBox = new GWTPagedFormDropDownListBox<Byte>();
        secondComboBox.addChangeHandler(onChangeHourMinute);
        for (byte i = 0; i < 60; i++) {
            secondComboBox.addItem(JSHourMinute.toDoubleDigitIfRequired(i), i);
        }

        setCurrentTime(date);

        grid.addRow(
                new GWTLabel(null),
                new GWTLabel(I18nTranslator.getInstance().year()),
                new GWTLabel(I18nTranslator.getInstance().month()),
                new GWTLabel(I18nTranslator.getInstance().day()),
                new GWTLabel(I18nTranslator.getInstance().hour()),
                new GWTLabel(I18nTranslator.getInstance().minute()),
                new GWTLabel(I18nTranslator.getInstance().second()));
        grid.addRow(
                dateCalendar,
                yearNumericBox,
                monthComboBox,
                dayComboBox,
                hourComboBox,
                minuteComboBox,
                secondComboBox);
        attachMainWidget(grid);
    }

    private void onAfterValueSet() {
        if (delegatedTimeChange != null) {
            delegatedTimeChange.onTimeChange(getCurrentTime());
        }
    }

    private void setValue(YearMonthDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            date = new JSYearMonthDayHourMinuteSecondMillisecond(
                    (YearMonthDayInterface) date,
                    (HourMinuteInterface) date,
                    new JSSecondMillisecond(
                    date.getSecondOfMinute(),
                    //Drop millisecond precision
                    (short) 0));
        }
        currentTime = date;
    }

    private YearMonthDayHourMinuteSecondMillisecondInterface computeValue() {
        YearMonthDayInterface date = dateCalendar.getCurrentDate();
        if (date != null) {

            return new JSYearMonthDayHourMinuteSecondMillisecond(
                    date,
                    new JSHourMinute(
                    hourComboBox.getSelectedValue(),
                    minuteComboBox.getSelectedValue()),
                    new JSSecondMillisecond(
                    secondComboBox.getSelectedValue(),
                    //Drop millisecond precision
                    (short) 0));

        } else {
            return null;
        }
    }

    @Override
    public void setCurrentTime(YearMonthDayHourMinuteSecondMillisecondInterface date) {
        setValue(date);
        if (date != null) {
            updateCalendar(date);
            updateYearMonthDay(date);
            updateTime(
                    (HourMinuteInterface) date,
                    //Drop millisecond precision
                    new JSSecondMillisecond(date.getSecondOfMinute(), (short) 0));
        }
    }

    @Override
    public void setCurrentTime(YearMonthDayInterface date) {
        setCurrentTime(new JSYearMonthDayHourMinuteSecondMillisecond(
                date,
                new JSHourMinute((byte) 0, (byte) 0),
                new JSSecondMillisecond((byte) 0, (short) 0)));
    }

    private void updateCalendar(YearMonthDayInterface date) {
        dateCalendar.setCurrentDate(date);
    }

    private void updateYearMonthDay(YearMonthDayInterface date) {
        yearNumericBox.setValue(Integer.valueOf(date.getYear()));
        monthComboBox.setSelectedValue(date.getMonth());
        dayComboBox.setSelectedValue(date.getDayOfMonth());
    }

    private void updateTime(HourMinuteInterface hourMinute, SecondMillisecondInterface secondMillisecond) {
        hourComboBox.setSelectedValue(hourMinute.getHourOfDay());
        minuteComboBox.setSelectedValue(hourMinute.getMinuteOfHour());
        secondComboBox.setSelectedValue(secondMillisecond.getSecondOfMinute());
    }

    @Override
    public YearMonthDayHourMinuteSecondMillisecondInterface getCurrentTime() {
        return currentTime;
    }

    @Override
    public void setTimeChangeHandler(YearMonthDayHourMinuteSecondTimeChangeHandler handler) {
        delegatedTimeChange = handler;
    }

    @Override
    public void setTimeChangeHandler(TimeChangeHandler handler) {
        if (handler instanceof YearMonthDayHourMinuteSecondTimeChangeHandler) {
            setTimeChangeHandler((YearMonthDayHourMinuteSecondTimeChangeHandler) handler);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        dateCalendar.setEnabled(enabled);
        yearNumericBox.setEnabled(enabled);
        monthComboBox.setEnabled(enabled);
        dayComboBox.setEnabled(enabled);
        hourComboBox.setEnabled(enabled);
        minuteComboBox.setEnabled(enabled);
        secondComboBox.setEnabled(enabled);
    }
}
