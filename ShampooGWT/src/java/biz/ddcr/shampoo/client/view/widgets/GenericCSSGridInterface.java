package biz.ddcr.shampoo.client.view.widgets;

public interface GenericCSSGridInterface<E> extends GenericGridInterface<E> {

    public void addRow(String CSSName, E... widgets);
    public void addRow(String CSSName, E widget);
    public void insertRowAt(String CSSName, int beforeRow, E... widgets);
    public void insertRowAt(String CSSName, int beforeRow, E widget);
    public void setWidgetAt(String CSSStyle, int row, int column, E widget);
    
}
