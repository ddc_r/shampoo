/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author okay_awright
 **/
public interface GenericCalendarInterface extends PluggableWidgetInterface {

    public void setVisible(boolean isVisible);

    @Override
    public void setEnabled(boolean enabled);

    public interface DateChangeHandler extends EventHandler {
        void onDateChange(YearMonthDayInterface newDate);
    }
    public void setDateChangeHandler(DateChangeHandler handler);

    public void setCaption(String defaultText);

    public void unsetCaption();

    public void setCurrentDate(YearMonthDayInterface currentDate);

    public YearMonthDayInterface getCurrentDate();

    public YearMonthDayInterface[] getCurrentWeekDays();

    public YearMonthDayInterface getDayInCurrentWeek(byte dayIndex);
    
    public YearMonthDayInterface getNextWeekStartDay();
}
