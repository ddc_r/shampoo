package biz.ddcr.shampoo.client.view.widgets;

public interface GenericCallbackInterface<T,U,V> {

    public void onCallBack(T s1, U s2, V s3);

}
