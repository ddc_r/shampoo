package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasClickHandlers;

public interface GenericCheckBoxInterface extends HasClickHandlers, PluggableWidgetInterface {

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

	public void setChecked(boolean checked);
        public boolean isChecked();

        public void setCaption(String caption);
}
