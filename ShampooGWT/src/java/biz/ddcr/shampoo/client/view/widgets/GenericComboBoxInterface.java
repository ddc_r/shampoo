package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasChangeHandlers;

public interface GenericComboBoxInterface extends HasChangeHandlers, PluggableWidgetInterface {

    @Override
        public void clean();

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

        public boolean addItem(String key);

        public int getItemCount();

        public String getSelectedValue();

        public boolean setSelectedValue(String selectedValue);

}
