package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.Widget;

public interface GenericContainerWithImageInterface extends GenericImageInterface {

    public void setBoxedObject(Widget widget);

}
