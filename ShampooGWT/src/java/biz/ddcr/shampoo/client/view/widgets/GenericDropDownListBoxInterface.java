package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasChangeHandlers;

public interface GenericDropDownListBoxInterface<T> extends HasChangeHandlers, PluggableWidgetInterface {

    @Override
        public void clean();

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

        public boolean addItem(String key, T value);

        public int getItemCount();

        public T getSelectedValue();

        public boolean setSelectedValue(T selectedValue);

        public boolean isValueIn(T value);
}
