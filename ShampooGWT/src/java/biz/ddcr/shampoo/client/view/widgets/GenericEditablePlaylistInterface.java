package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import java.util.Collection;

public interface GenericEditablePlaylistInterface<T extends PlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GenericPlaylistInterface<T, U, V> {

    public Boolean isSlotChecked(int index);
    public boolean validateSlotAt(int index);

    @Override
    public GWTEditablePSlot getSlotAt(int index);

    public T getTemplateForm();

    public Collection<T> getAllCheckedValues();

    /**
     * The template form, that is unnumbered by default
     * @param form
     * @return
     */
    public boolean setTemplateForm(T form);

    /**
     * Beware that contrary to every other methods the returned index takes into account the template slot.
     * @param event
     * @return
     */

    /** Check whether the value returned by getCurrentRow() locates a regular slot or the template slot
     */
    public boolean isTemplateSlotAt(int index);

}
