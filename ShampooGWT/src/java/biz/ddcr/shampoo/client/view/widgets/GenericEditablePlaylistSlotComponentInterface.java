package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import java.util.Collection;

public interface GenericEditablePlaylistSlotComponentInterface<T, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends GenericPlaylistSlotComponentInterface<T, U, V> {

        /*public void attachNextEmbeddedComponent(GenericEditablePlaylistSlotComponentInterface<T,U,V> newComponent);
        public void detachNextEmbeddedComponent();*/

        public boolean arePropertiesFilledIn();

}
