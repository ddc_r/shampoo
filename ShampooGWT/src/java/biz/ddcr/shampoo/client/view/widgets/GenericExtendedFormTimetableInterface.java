package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellCoordinates;
import biz.ddcr.shampoo.client.view.widgets.GenericFormTimetableInterface.CellRefreshHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventHandler;
import java.util.Collection;

public interface GenericExtendedFormTimetableInterface<T,U> extends PluggableWidgetInterface {

    public short getTimeShift();

    public int getTimescaleResolution();

    public CellCoordinates getCurrentCellCoordinates(ClickEvent event);

    public ActionCollectionEntry<T> getFormAt(CellCoordinates coordinates);
    
    public void setDefaultForm(ActionCollectionEntry<T> form);

    public void setCurrentDate(YearMonthDayInterface currentDate);

    public YearMonthDayInterface getCurrentDate();

    //Asking for refresh
    public interface RefreshHandler<T> extends EventHandler {
        void onRefresh(YearMonthDayHourMinuteSecondMillisecondInterface startOfWeek);
    }
    //Respond to refresh call
    public void onRefreshData(ActionCollection<T> newForms);

    public void setRefreshHandler(RefreshHandler<T> handler);

    public void setCellRefreshHandler(CellRefreshHandler<T,U> handler);

    public String getTimeZone();

    public Collection<T> getAllForms();
    
    public void forceRefresh();
    
}
