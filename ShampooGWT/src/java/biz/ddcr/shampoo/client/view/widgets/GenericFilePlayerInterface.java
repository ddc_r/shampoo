package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.HasAudioInterface;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionMalformedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionTimedOutException;
import biz.ddcr.shampoo.client.helper.errors.CustomErrorCodesInterface;
import com.mvp4g.client.event.EventBusWithLookup;

public interface GenericFilePlayerInterface extends GenericExternalJSWidgetInterface {

    public enum HTTPCustomErrorCodes implements CustomErrorCodesInterface {

        //CORS/Firewall
        BLOCKED(0),
        //HTTP error code
        TIME_OUT(408),
        MALFORMED(409),
        FORBIDDEN(403);

        private final int value;

        HTTPCustomErrorCodes(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static RuntimeException toException(int value) {
            switch (value) {
                case 0:
                    return new AccessDeniedException("CORS likely not enabled or request blocked by a firewall; cannot access the remote resource");
                //Known HTTP error codes, and correctly translated by the endpoint servlet
                case 408:
                    return new ConnectionTimedOutException();
                case 409:
                    return new ConnectionMalformedException();
                case 403:
                    return new AccessDeniedException();
                default:
                    return new ConnectionException(value + "");
            }
        }
    }
    
        public void setTrack(HasAudioInterface form, String absoluteEndpoint);

        public void setEventBus(EventBusWithLookup eventBus);

        /** Set the JSESSIONID, cannot be automatically be appended by GWT because of the "HttpOnly" flag, it must be manuallt specified during each transaction**/
        public void setCurrentSessionId(String jsessionidFragment);
        
        /**
         * Manually initiate the play/stop mechanism from outside the widget
         **/
        public void playStop();
        
        /**
         * Is it currently playing something?
         * @return 
         */
        public boolean isPlaying();
        
        public boolean canReadStreams();

}
