package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionMalformedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionTimedOutException;
import biz.ddcr.shampoo.client.helper.errors.CustomErrorCodesInterface;
import biz.ddcr.shampoo.client.helper.errors.LocalResourceAccessFailure;
import biz.ddcr.shampoo.client.helper.errors.TrackBitrateTooHighException;
import biz.ddcr.shampoo.client.helper.errors.TrackBitrateTooLowException;
import biz.ddcr.shampoo.client.helper.errors.TrackBrokenException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotCBRException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotStereoException;
import biz.ddcr.shampoo.client.helper.errors.TrackSamplerateTooHighException;
import biz.ddcr.shampoo.client.helper.errors.TrackSamplerateTooLowException;
import biz.ddcr.shampoo.client.helper.errors.TrackTooLongException;
import biz.ddcr.shampoo.client.helper.errors.TrackTooShortException;
import biz.ddcr.shampoo.client.helper.errors.UploadingCanceledException;
import biz.ddcr.shampoo.client.helper.errors.UploadingOversizedException;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import com.google.gwt.event.shared.EventHandler;
import com.mvp4g.client.event.EventBusWithLookup;
import java.util.Collection;

public interface GenericFileUploadInterface extends GenericExternalJSWidgetInterface {

    public enum HTTPCustomErrorCodes implements CustomErrorCodesInterface {

        //Custom error code
        GENERIC_ERROR(0),
        UNSECURED_APPLET_CALL(1),
        CANCELED(2),
        //HTTP error code
        TIME_OUT(408),
        CONTENT_SIZE_TOO_BIG(413),
        MALFORMED(409),
        WRONG_FORMAT(415),
        FORBIDDEN(403),
        //Customized HTTP error codes (see FrontendUpMediaController)
        TRACK_BROKEN(450),
        TRACK_BITRATE_TOO_LOW(451),
        TRACK_BITRATE_TOO_HIGH(452),
        TRACK_NOT_STEREO(453),
        TRACK_SAMPLERATE_TOO_LOW(454),
        TRACK_SAMLERATE_TOO_HIGH(455),
        TRACK_NOT_CBR(456),
        TRACK_TOO_SHORT(457),
        TRACK_TOO_LONG(458);

        private final int value;

        HTTPCustomErrorCodes(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static RuntimeException toException(int value) {
            switch (value) {
                //Custom error codes (should be <100)
                case 1:
                    return new LocalResourceAccessFailure();
                case 2:
                    return new UploadingCanceledException();
                //Known HTTP error codes, and correctly translated by the endpoint servlet
                case 408:
                    return new ConnectionTimedOutException();
                case 413:
                    return new UploadingOversizedException();
                case 409:
                    return new ConnectionMalformedException();
                case 415:
                    return new UploadingWrongFormatException();
                case 403:
                    return new AccessDeniedException();
                //Custom HTTP error codes
                case 450:
                    return new TrackBrokenException();
                case 451:
                    return new TrackBitrateTooLowException();
                case 452:
                    return new TrackBitrateTooHighException();
                case 453:
                    return new TrackNotStereoException();
                case 454:
                    return new TrackSamplerateTooLowException();
                case 455:
                    return new TrackSamplerateTooHighException();
                case 456:
                    return new TrackNotCBRException();
                case 457:
                    return new TrackTooShortException();
                case 458:
                    return new TrackTooLongException();
                default:
                    return new ConnectionException(value + "");
            }
        }
    }

    public void setAbsolutePostURL(String postURLPath);

    public void setEventBus(EventBusWithLookup eventBus);

    public String getCurrentUploadId();

    /**
     * Set the JSESSIONID, cannot be automatically be appended by GWT because of
     * the "HttpOnly" flag, it must be manuallt specified during each transaction*
     */
    public void setCurrentSessionId(String jsessionidFragment);

    public void setAllowedFileExtensions(Collection<String> extensions);

    public interface UploadHandler extends EventHandler {

        public void onSuccess(String uploadId);

        public void onFailure();
    }

    public void setUploadHandler(UploadHandler handler);

    /**
     * Manually initiate the upload mechanism from outside the widget
         *
     */
    public void triggerUpload();

}
