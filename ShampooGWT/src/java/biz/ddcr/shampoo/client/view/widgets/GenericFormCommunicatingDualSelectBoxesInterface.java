package biz.ddcr.shampoo.client.view.widgets;

import java.util.Collection;

public interface GenericFormCommunicatingDualSelectBoxesInterface<T> extends PluggableWidgetInterface {

    @Override
        public void clean();

	public void setVisible(boolean isVisible);

    @Override
        public void setEnabled(boolean enabled);

        public boolean select(String key, T value);
        public boolean unselect(String key, T value);

        public void setHeader(String leftCaption, String rightCaption);

        public boolean appendUnselected(String key, T value);
        public boolean appendSelected(String key, T value);

        public Collection<T> getSelectedValues();
        public Collection<String> getSelectedKeys();
}
