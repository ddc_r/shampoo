package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import java.util.Collection;

public interface GenericFormSelectBoxInterface<T> extends HasChangeHandlers, PluggableWidgetInterface {

    @Override
        public void clean();

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

        public boolean addItem(String key, T value);

        public int getItemCount();

        public Collection<T> getSelectedValues();

        public boolean isValueIn(T value);
}
