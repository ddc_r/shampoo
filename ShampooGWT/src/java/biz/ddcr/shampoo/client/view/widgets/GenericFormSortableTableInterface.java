package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventHandler;

public interface GenericFormSortableTableInterface<T, E, F> extends GenericFormTableInterface<T, E> {

    public interface SortableHeaderEntryInterface<F> {
        public String getCaption();
        public F getFlag();
    }
    public void setHeader(SortableHeaderEntryInterface<F>... columnHeaders);
    
    public interface SortableTableClickHandler extends EventHandler {
        void onClick(ClickEvent event, GroupAndSort groupAndSortObject);
    }
    public void setRefreshClickHandler(SortableTableClickHandler handler);

    public GroupAndSort getCurrentGroupAndSortParameters();

    public void setNextButtonVisible(boolean isVisible);

    public void setMaxVisibleRows(int maxRows);
    public int getMaxVisibleRows();

}
