package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventHandler;
import java.util.Collection;

public interface GenericFormTableInterface<T, E> extends PluggableWidgetInterface {

    @Override
    public void clean();
    public void setVisible(boolean isVisible);
    public int getRowCount();
    public int getCellCount(int row);
    public void setHeader(String... captions);
    public void removeHeader();
    public boolean addRow(T form, boolean canBeChecked, E... components);
    public boolean addRow(String cssStyle, T form, boolean canBeChecked, E... widgets);
    public boolean removeRow(int row);
    public E getComponentAt(int row, int column);
    public T getValueAt(int row);
    public boolean isValueIn(T value);
    public int getCurrentRow(ClickEvent event);
    public Collection<T> getAllValues();
    public Collection<T> getAllCheckedValues();
    public Boolean isRowChecked(int row);

    //Asking for cell captions refresh
    public interface CheckedItemHeaderRefreshHandler extends EventHandler {
        void onCheckedItemHeaderRefresh(GenericPanelInterface captionContainer);
    }
    public void setCheckedItemHeaderRefreshHandler(CheckedItemHeaderRefreshHandler handler);
}
