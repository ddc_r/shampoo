package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventHandler;
import java.util.Collection;

public interface GenericFormTimetableInterface<T,U> extends PluggableWidgetInterface {

    public class CellCoordinates {
        private int colIndex;
        private int rowIndex;

        public CellCoordinates(int colIndex, int rowIndex) {
            this.colIndex = colIndex;
            this.rowIndex = rowIndex;
        }

        public int getColIndex() {
            return colIndex;
        }

        public void setColIndex(int colIndex) {
            this.colIndex = colIndex;
        }

        public int getRowIndex() {
            return rowIndex;
        }

        public void setRowIndex(int rowIndex) {
            this.rowIndex = rowIndex;
        }

    }

    public short getTimeShift();

    public void setTimeShift(short timeShiftInMinutes);

    public int getTimescaleResolution();

    public void setTimescaleResolution(byte timescaleResolutionInMinutes);

    //public boolean addStaticCell(byte fromDayOfWeek, short fromMinutes, byte toDayOfWeek, short toMinutes, ActionCollectionEntry<T> form);
    public boolean addSpanningCell(byte fromDayOfWeek, short fromMinutes, byte toDayOfWeek, short toMinutes, ActionCollectionEntry<T> form);
    public boolean addSpanningCell(YearMonthDayInterface[] localeDaysOfCurrentWeek, YearMonthDayInterface localeStartDayOfNextWeek, ActionCollectionEntry<T> timetableEntry);
    /**
     * Remove all cell data
     **/
    public void removeAllCells();

    /**
     * Clear the whole grid but do not remove cell data
     **/
    public void clearGrid();

    public int getHeaderRowNumber();

    public int getLegendColNumber();

    public int getRowCount();

    public void refresh();

    public CellCoordinates getCurrentCellCoordinates(ClickEvent event);

    public ActionCollectionEntry<T> getFormAt(CellCoordinates coordinates);
    
    public void setDefaultForm(ActionCollectionEntry<T> form);

    //Asking for cell captions refresh
    public interface CellRefreshHandler<T,U> extends EventHandler {
        void onContentRefresh(ActionCollectionEntry<T> cellForm, GenericPanelInterface contentContainer);
        void onCaptionRefresh(U startTime, U endTime, ActionCollectionEntry<T> cellForm, GenericPanelInterface captionContainer);
    }

    public void setCellRefreshHandler(CellRefreshHandler<T,U> handler);

    public Collection<T> getAllForms();
}
