package biz.ddcr.shampoo.client.view.widgets;

public interface GenericGridInterface<E> extends GenericSimpleLayerInterface<E> {

    public int getCellCount(int row);
    public void addRow(E... widgets);
    public void insertRowAt(int beforeRow, E... widgets);
    public void insertRowAt(int beforeRow, E widget);
    public E getWidgetAt(int row, int column);
    public void setWidgetAt(int row, int column, E widget);
    public boolean removeRow(int row);
    
}
