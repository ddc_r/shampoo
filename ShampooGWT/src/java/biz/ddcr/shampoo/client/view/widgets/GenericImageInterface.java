package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.HasCoverArtInterface;

public interface GenericImageInterface extends PluggableWidgetInterface {

	public void setVisible( boolean isVisible );
        public boolean isVisible();

	public void setImage(String url);
        public void setImage(HasCoverArtInterface form);

        public int getWidth();
        public int getHeight();

}
