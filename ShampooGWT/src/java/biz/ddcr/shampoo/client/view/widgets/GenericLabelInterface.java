package biz.ddcr.shampoo.client.view.widgets;

public interface GenericLabelInterface extends PluggableWidgetInterface {

	public void setVisible( boolean isVisible );
        public boolean isVisible();

	public void setCaption(String caption);
        public String getCaption();

}
