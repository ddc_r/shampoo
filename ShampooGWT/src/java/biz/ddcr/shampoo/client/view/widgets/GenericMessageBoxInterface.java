package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import java.util.Collection;

public interface GenericMessageBoxInterface extends PluggableWidgetInterface {

    public void show();
    public void close();

    public void setMessage(String message);
    public void addMessage(String message);
    public void setMessages(Collection<String> messages);

    public HandlerRegistration addButtonClickHandler(ClickHandler cickHandler);

}
