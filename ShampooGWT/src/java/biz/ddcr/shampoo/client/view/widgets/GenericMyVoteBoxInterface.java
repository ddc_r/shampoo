package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;

public interface GenericMyVoteBoxInterface<T> extends PluggableWidgetInterface {

    @Override
    public void setEnabled(boolean enabled);

    public void setVisible(boolean isVisible);

    public void setMyVote(VOTE myVote);

    public VOTE getMyVote();

    public T getSongID();

    public interface VoteChangeHandler<T> {
        public void onSet(GenericMyVoteBoxInterface<T> source);
        public void onGet(GenericMyVoteBoxInterface<T> source);
    };
    public void setVoteChangeHandler(VoteChangeHandler<T> handler);

}
