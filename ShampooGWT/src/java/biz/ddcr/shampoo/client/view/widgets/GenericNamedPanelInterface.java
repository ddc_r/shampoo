package biz.ddcr.shampoo.client.view.widgets;

public interface GenericNamedPanelInterface extends GenericPanelInterface {

    public void setHeader(String text);

}
