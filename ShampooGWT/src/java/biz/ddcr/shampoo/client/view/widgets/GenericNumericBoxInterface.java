package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasBlurHandlers;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.dom.client.HasKeyPressHandlers;

public interface GenericNumericBoxInterface<T extends Number> extends HasKeyPressHandlers, HasChangeHandlers, HasBlurHandlers, PluggableWidgetInterface {

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

	public boolean setValue(T value);
        public T getValue();

        public void setDefaultValue(T value);
        public T getDefaultValue();
        public void setMinValue(T value);
        public T getMinValue();
        public void setMaxValue(T value);
        public T getMaxValue();
}
