package biz.ddcr.shampoo.client.view.widgets;

public interface GenericPanelInterface extends PluggableWidgetInterface {

    public void add(FrameWidgetInterface widget);
    public void insertAt(int index, FrameWidgetInterface widget);
    public void remove(FrameWidgetInterface widget);

    public FrameWidgetInterface getFirstBoundFrameWidget();
    public int getNumberOfBoundFrameWidget();
    public FrameWidgetInterface getBoundFrameWidget(int index);

    public void setVisible(boolean isVisible);
    public void setWidth(String width);

    public void setStyleName(String styleName);
    
}
