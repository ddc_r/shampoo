package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;

public interface GenericPlaylistFilterInterface<T extends FilterForm> extends PluggableWidgetInterface {

    @Override
        public void clean();

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

        public void setForm(T form);
        public T getForm();

}
