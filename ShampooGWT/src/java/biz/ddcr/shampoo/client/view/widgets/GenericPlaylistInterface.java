package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.EventHandler;
import java.util.Collection;

public interface GenericPlaylistInterface<T extends PlaylistEntryForm, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends PluggableWidgetInterface {

    public int getSlotCount();

    public boolean removeSlot(int index);

    public GenericPlaylistSlotComponentInterface<T,U,V> getSlotAt(int index);

    public T getFormAt(int index);

    public Collection<T> getAllForms();

    public boolean setFormAt(int index, T form);

    public boolean appendForms(Collection<T> forms);

    /**
     * Beware that contrary to every other methods the returned index takes into account the template slot.
     * @param event
     * @return
     */
    public int getCurrentRow(ClickEvent event);

    /**
     * Called each time a slot associated with either a Static or DynamicPlaylistEntryForm needs to refresh its GUI for the tracks it contains
     * Can be useful to bind a custom button and a callback mechanism so as to directly modify the form itself too
     * @param <T>
     */
    public interface SlotRefreshHandler<U, V> extends EventHandler {
        void onTrackCaptionRefresh(U form, GenericPanelInterface container);
        void onFiltersCaptionRefresh(V form, GenericPanelInterface container);
    }
    public void setSlotRefreshHandler(SlotRefreshHandler<U, V> handler);

}
