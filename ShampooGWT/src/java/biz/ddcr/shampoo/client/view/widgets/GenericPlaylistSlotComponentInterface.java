package biz.ddcr.shampoo.client.view.widgets;

import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.view.widgets.GenericPlaylistInterface.SlotRefreshHandler;
import java.util.Collection;

public interface GenericPlaylistSlotComponentInterface<T, U extends ActionCollectionEntry<String>, V extends Collection<FilterForm>> extends PluggableWidgetInterface {

    @Override
        public void clean();

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

        public boolean setPropertiesFromComponent(T component);
        public T getComponentFromProperties();

        public void setSlotRefreshHandler(SlotRefreshHandler<U,V> handler);

}
