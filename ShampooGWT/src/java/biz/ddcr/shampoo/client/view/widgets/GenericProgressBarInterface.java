package biz.ddcr.shampoo.client.view.widgets;

public interface GenericProgressBarInterface extends PluggableWidgetInterface {
  
	public void setVisible(boolean isVisible);

	public void setProgress(long progress);
        public long getProgress();
        public long getMaxProgress();
        
        public void setPercentTextVisible(boolean isVisible);
        public boolean isPercentTextVisible();
}
