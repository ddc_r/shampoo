package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;

public interface GenericRadioButtonInterface extends HasValueChangeHandlers, HasClickHandlers, PluggableWidgetInterface {

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

	public void setSelected(boolean checked);
        public boolean isSelected();

        public void setGroupName(String groupName);

        public void setCaption(String caption);
}
