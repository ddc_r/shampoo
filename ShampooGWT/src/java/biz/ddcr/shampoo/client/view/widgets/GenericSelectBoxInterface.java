package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import java.util.Collection;

public interface GenericSelectBoxInterface extends HasChangeHandlers, PluggableWidgetInterface {

    @Override
        public void clean();

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

        public void addItem(String key, String value);

        public int getItemCount();

        public Collection<String> getSelectedValues();
}
