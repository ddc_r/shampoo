package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;

public interface GenericSimpleLayerInterface<E> extends HasClickHandlers, PluggableWidgetInterface {

    @Override
    public void clean();
    public void setVisible(boolean isVisible);
    public int getRowCount();
    public void addRow(E widget);
    public int getCurrentRow(ClickEvent event);
    
}
