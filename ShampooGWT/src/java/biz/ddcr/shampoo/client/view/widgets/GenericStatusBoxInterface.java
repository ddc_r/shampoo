package biz.ddcr.shampoo.client.view.widgets;

public interface GenericStatusBoxInterface<T,U> extends PluggableWidgetInterface {

    @Override
    public void setEnabled(boolean enabled);

    public void setVisible(boolean isVisible);

    public void setStatus(U status);

    public T getID();

    public void setID(T id);

    public interface GenericStatusChangeHandler<T,U> {
        public void onGet(GenericStatusBoxInterface<T,U> source);
        //public void onSet(GenericStatusBoxInterface<T,U> source);
    };
    public void setStatusChangeHandler(GenericStatusChangeHandler<T,U> handler);

    /*Automatically reset the status thanks the statusChangeHandler*/
    public void refreshStatus();
    
}
