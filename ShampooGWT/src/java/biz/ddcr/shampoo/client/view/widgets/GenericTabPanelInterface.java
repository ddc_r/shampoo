package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;

/**
 * Beware that all indexes used by this widget do not necessarily follow the visible tab sequence from the GUI.
 * Indexes are internal pointer explicitly defined with insertAt()
 *
 * @author okay_awright
 **/
public interface GenericTabPanelInterface extends PluggableWidgetInterface {

    public void add(PluggableWidgetInterface widget, String title);
    public void insertAt(PluggableWidgetInterface widget, String title, int internalIndex);

    public HandlerRegistration addClickHandler(ClickHandler handler, int index);

    public PluggableWidgetInterface getTabWidgetAt(int visibleIndex);

    public int getSelectedTabIndex();
    public int getTabCount();

    public boolean selectTabAt(int visibleIndex);
    public boolean selectFirstTab();
    public boolean selectLastTab();

}
