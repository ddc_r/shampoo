package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.dom.client.HasKeyPressHandlers;

public interface GenericTextBoxInterface extends HasEditableText, HasChangeHandlers, HasKeyPressHandlers, PluggableWidgetInterface {

    @Override
        public void setEnabled( boolean enabled );

	public void setVisible( boolean isVisible );

}
