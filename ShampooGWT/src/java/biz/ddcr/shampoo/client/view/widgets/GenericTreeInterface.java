package biz.ddcr.shampoo.client.view.widgets;

public interface GenericTreeInterface<D, E> extends PluggableWidgetInterface {

    @Override
    public void clean();
    public void setVisible(boolean isVisible);
    /**
     * Add an item to the branch defined by the handle itemIdContainer
     * returns a unique handle to this new branch, null if the item has not been added **/
    public D addItem(E widget, D itemIdContainer);
    /**
     * Add an item to the root tree
     * returns a unique handle to this new branch, null if the item has not been added **/
    public D addItem(E widget);    
    /**
     * Add an item to the branch defined by the name
     * returns a unique handle to this new branch, null if the item has not been added **/  
    public D addItemWithName(String name, E localWidget);
    public D getItemByName(String name);
    
}
