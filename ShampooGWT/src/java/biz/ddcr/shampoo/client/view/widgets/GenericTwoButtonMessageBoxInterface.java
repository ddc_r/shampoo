package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;

public interface GenericTwoButtonMessageBoxInterface extends GenericMessageBoxInterface {

    public HandlerRegistration addOtherButtonClickHandler(ClickHandler clickHandler);

}
