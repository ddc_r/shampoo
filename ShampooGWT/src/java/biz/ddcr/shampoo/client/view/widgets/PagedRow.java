/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.client.view.widgets;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PagedRow<T, E extends Widget> {

    private E[] components;
    private boolean canBeChecked;
    private T form;
    private String cssStyle;

    public PagedRow(String cssStyle, T form, boolean canBeChecked, E... components) {
        this.components = components;
        this.canBeChecked = canBeChecked;
        this.form = form;
        this.cssStyle = cssStyle;
    }

    public boolean isCanBeChecked() {
        return canBeChecked;
    }

    public E[] getComponents() {
        //TODO: make it mutable, clone() not supported by GWT
        return components;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public T getForm() {
        return form;
    }
}