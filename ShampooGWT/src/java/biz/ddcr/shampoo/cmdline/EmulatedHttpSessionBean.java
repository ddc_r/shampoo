/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.cmdline;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.helper.ServletUtils2;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

/**
 *
 * Emulate the use of HttpSession for beans that are not dependant on Servlets
 * Log in Administrators for the whole lifecycle of the fake HttpSession
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class EmulatedHttpSessionBean {

    private static HttpSession makeFakeSession() {
        //Instantiate fake HTTPSession for storing user credentials
        return new HttpSession() {

            private String id = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());;
            private Hashtable<String, Object> sessionData = new Hashtable<String, Object>();

            @Override
            public long getCreationTime() {
                return 0;
            }

            @Override
            public String getId() {
                return id;
            }

            @Override
            public long getLastAccessedTime() {
                return System.currentTimeMillis();
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public void setMaxInactiveInterval(int i) {
                //Do nothing
            }

            @Override
            public int getMaxInactiveInterval() {
                return 0;
            }

            @Override
            public HttpSessionContext getSessionContext() {
                return null;
            }

            @Override
            public Object getAttribute(String string) {
                return sessionData.get(string);
            }

            @Override
            public Object getValue(String string) {
                return getAttribute(string);
            }

            @Override
            public Enumeration getAttributeNames() {
                return sessionData.keys();
            }

            @Override
            public String[] getValueNames() {
                final Collection<String> c = sessionData.keySet();
                return c.toArray(new String[c.size()]);
            }

            @Override
            public void setAttribute(String string, Object o) {
                sessionData.put(string, o);
            }

            @Override
            public void putValue(String string, Object o) {
                setAttribute(string, o);
            }

            @Override
            public void removeAttribute(String string) {
                sessionData.remove(string);
            }

            @Override
            public void removeValue(String string) {
                removeAttribute(string);
            }

            @Override
            public void invalidate() {
                //Do nothing
            }

            @Override
            public boolean isNew() {
                return false;
            }
        };
    }

    private SecurityManagerInterface securityManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public EmulatedHttpSessionBean(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /** Open a fake HttpSession and register it via ServletUtils2 **/
    public void openSession(String username, String password) throws Exception {
        if (ServletUtils2.getLocalSession() == null) {
            ServletUtils2.setLocalSession(makeFakeSession());
        }
        bindUser(username, password);
    }

    /** Close the session opened with openSession() **/
    protected void bindUser(String username, String password) throws Exception {
        //Automatically log in the given user
        User user = getSecurityManager().fetchRegisteredUser(username, true, password, false);
        UserVisitor visitor = new UserVisitor() {

            @Override
            public void visit(RestrictedUser user) throws Exception {
                throw new AccessDeniedException("You must log in as an Administrator");
            }

            @Override
            public void visit(Administrator user) throws Exception {
                getSecurityManager().setCurrentlyAuthenticatedUserFromSession(user);
            }
        };
        if (user != null) {
            user.acceptVisit(visitor);
        } else {
            throw new AccessDeniedException("The specified user doesn't exist or is not allowed to log in");
        }
    }

    public void closeSession() {
        ServletUtils2.resetLocalSession();
    }
}
