/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.cmdline;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import biz.ddcr.shampoo.client.helper.text.NaiveCSV;
import java.util.Collection;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.helper.TrackConfigurationHelper;
import biz.ddcr.shampoo.server.io.parser.FileHeaderParser;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.io.util.UntypedLocalFileStream;
import biz.ddcr.shampoo.server.io.util.UntypedStream;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 * Simple local batch importer for tracks from legacy systems
 * Transaction-less, and synchronous
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ImportTracks extends EmulatedHttpSessionBean {

    private DefaultTransactionDefinition transactionDefinition;
    private PlatformTransactionManager transactionManager;
    private DatastoreService datastoreService;
    private TrackManagerInterface trackManager;
    private ProgrammeManagerInterface programmeManager;
    private TrackConfigurationHelper trackConfigurationHelper;
    private CoverArtConfigurationHelper coverArtConfigurationHelper;

    public ImportTracks(PlatformTransactionManager transactionManager, DatastoreService datastoreService, SecurityManagerInterface securityManager, TrackManagerInterface trackManager, ProgrammeManagerInterface programmeManager, TrackConfigurationHelper trackConfigurationHelper, CoverArtConfigurationHelper coverArtConfigurationHelper) {
        super(securityManager);
        this.datastoreService = datastoreService;
        this.trackManager = trackManager;
        this.programmeManager = programmeManager;
        this.trackConfigurationHelper = trackConfigurationHelper;
        this.coverArtConfigurationHelper = coverArtConfigurationHelper;
        this.transactionManager = transactionManager;
        transactionDefinition = new DefaultTransactionDefinition();
        //Read-Write
        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
    }

    public TransactionStatus startTransaction() {
        return transactionManager.getTransaction(transactionDefinition);
    }

    public void commitTransaction(TransactionStatus status) {
        transactionManager.commit(status);
    }

    public void rollbackTransaction(TransactionStatus status) {
        transactionManager.rollback(status);
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public CoverArtConfigurationHelper getCoverArtConfigurationHelper() {
        return coverArtConfigurationHelper;
    }

    public TrackConfigurationHelper getTrackConfigurationHelper() {
        return trackConfigurationHelper;
    }

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    /**
     * Check if a File can be used by Shampoo and make it a proper Track Stream
     * @param file
     * @return
     * @throws Exception
     */
    protected TrackStream toTrackStream(File file) throws Exception {
        UntypedStream stream = new UntypedLocalFileStream(file.getAbsolutePath(), file);

        //First check if the file is valid
        //Extensions alone are nothing short than useless except if they can help filtering out false positives that will be more thoroughly examined later on
        //predefined MIME types are automatically filled in if extensions appear to be valid
        AUDIO_FORMAT guessedFormat = getTrackConfigurationHelper().filterOutUnwantedExtensions(file.getName());
        if (guessedFormat == null) {
            throw new UploadingWrongFormatException();
        }
        //Now sniff the header for fingerprints, and even if it's oh-so slow

        AudioFileInfo fileHeader = null;
        fileHeader = FileHeaderParser.getHeader(guessedFormat, stream);

        if (fileHeader == null || !getTrackConfigurationHelper().isAcceptFormat(fileHeader.getFormat())) {
            throw new UploadingWrongFormatException();
        }

        //Check if the appropriate restrictions regarding the allowed features are fullfilled
        if (getTrackConfigurationHelper().checkFeatures(fileHeader)) {

            //fetch the embedded tags too
            Tag fileTag = FileHeaderParser.getTag(getCoverArtConfigurationHelper(), guessedFormat, stream);

            return new TrackStream(fileTag, fileHeader, stream);

        }
        throw new UploadingWrongFormatException();
    }

    /**
     * Insert an entry from the CSV Import File into the database
     * No check is performed whether it's a duplicate
     * @param file
     * @param type
     * @param programmeIds
     * @throws Exception
     */
    public void persistBroadcastableTrack(File file, TYPE type, final Collection<String> programmeIds) throws Exception {

        Collection<Integer> transactions;

        //Make the File a Track Stream and check its format
        TrackStream stream = toTrackStream(file);
        Tag tags = stream.getTag();
        //no need to go any further if there are no tags
        if (tags == null) {
            throw new IllegalStateException("No tags");
        }

        //Make the track
        BroadcastableTrack newTrack = null;
        switch (type) {
            case jingle:
                newTrack = new BroadcastableJingle();
                break;
            case song:
                newTrack = new BroadcastableSong();
                break;
            case advert:
                newTrack = new BroadcastableAdvert();
                break;
            default:
                throw new IllegalArgumentException("Unknown type");
        }
        //Fill in all attributes
        newTrack.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
        newTrack.setEnabled(true); //activated by default
        //it's brand new: it could not have been played yet, no rotation
        newTrack.setRating(tags.getRating());
        newTrack.setAlbum(tags.getAlbum());
        newTrack.setAuthor(tags.getAuthor());
        newTrack.setPublisher(tags.getPublisher());
        newTrack.setCopyright(tags.getCopyright());
        newTrack.setDescription(tags.getDescription());
        newTrack.setGenre(tags.getGenre());
        newTrack.setTag(tags.getTag());
        newTrack.setTitle(tags.getTitle());
        newTrack.setDateOfRelease(tags.getDateOfRelease());
        newTrack.setReady(true); //false has no use since the instantiation is synchronous here

        //Open a new DB session with a transaction
        TransactionStatus status = startTransaction();

        //Containers info that need to be imported from external data are handled elsewhere
        //Don't instantiate empty containers if the form has no container attached to it
        transactions = getDatastoreService().copyTrackToDataStore(newTrack, stream, tags.getCoverArt());
        try {
            //Programmes
            //it's a brand new association: the track could not have been played yet, its rotation is zero
            Collection<Programme> programmes = getProgrammeManager().loadProgrammes(programmeIds);
            newTrack.addProgrammes(programmes);

            //Persist in database
            getTrackManager().addTrack(newTrack);

        } catch (Exception e) {
            //Rollback from datastore
            getDatastoreService().rollbackChangesInDataStore(transactions);
            //And DB
            rollbackTransaction(status);
            //and re-throw
            throw e;
        }
        //Commit in datastore
        getDatastoreService().commitChangesInDataStore(transactions);
        //And commit in DB
        commitTransaction(status);
    }

    public static void main(String[] args) throws Exception {

        //ERRORLEVEL for shells
        boolean errorcode = false;
        BufferedReader br = null;
        String username, password;
        try {

            //First check if parameters are ok, otherwise initiate early termination
            if (args == null || args.length!=3) {
                throw new IllegalArgumentException("Wrong number of parameters: <username> <password> <CSV>");
            }
            username = args[0];
            password = args[1];

            System.out.println("Opening CSV import file");
            br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(args[2]))));

            System.out.println("Loading full context (please be patient)");
            //Bind the current application context; it's the full Web Applicaion Context, so it's über heavy
            ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContextCore.xml");
            //And create a Spring-unmanaged bean tha will actualy performs the conversion
            SecurityManagerInterface securityManager = (SecurityManagerInterface) context.getBean("securityManager");
            TrackManagerInterface trackManager = (TrackManagerInterface) context.getBean("trackManager");
            ProgrammeManagerInterface programmeManager = (ProgrammeManagerInterface) context.getBean("programmeManager");
            DatastoreService datastoreService = (DatastoreService) context.getBean("datastoreService");
            TrackConfigurationHelper trackConfigurationHelper = (TrackConfigurationHelper) context.getBean("trackConfigurationHelper");
            CoverArtConfigurationHelper coverArtConfigurationHelper = (CoverArtConfigurationHelper) context.getBean("coverArtConfigurationHelper");
            PlatformTransactionManager transactionManager = (PlatformTransactionManager) context.getBean("transactionManager");
            ImportTracks importer = new ImportTracks(transactionManager, datastoreService, securityManager, trackManager, programmeManager, trackConfigurationHelper, coverArtConfigurationHelper);

            importer.openSession(username, password);

            //parse the CSV file one line after another
            System.out.println("Parsing CSV import file");
            String strLine;
            Collection<String> tokens;
            long i = 0;
            File file = null;
            TYPE type;
            Collection<String> programmeIds;
            while ((strLine = br.readLine()) != null) {
                i++;
                try {
                    tokens = NaiveCSV.toCollection(strLine, false); //quoted items are not mandatory

                    Iterator<String> iter = tokens.iterator();
                    //Get the file to import
                    if (iter.hasNext()) {
                        String _file = iter.next().trim();
                        file = new File(_file);
                    } else {
                        throw new IllegalArgumentException("Invalid tokens on line " + i + ": no file to import");
                    }

                    //Then its type
                    if (iter.hasNext()) {
                        String _type = iter.next().trim().toLowerCase();
                        type = TYPE.valueOf(_type);
                    } else {
                        throw new IllegalArgumentException("Invalid tokens on line " + i + ": no type");
                    }

                    //And finally, everything else are bound programme ids
                    //it can be unspecified
                    programmeIds = new HashSet<String>();
                    while (iter.hasNext()) {
                        programmeIds.add(iter.next().trim());
                    }

                    System.out.println("Persisting entry " + i + ": " + file.getPath() + " as " + type.toString() + " for " + programmeIds.size() + " programmes");

                    //now let's go, import this entry
                    importer.persistBroadcastableTrack(file, type, programmeIds);
                    System.out.println("ok");

                } catch (Exception e) {
                    //Exceptions thrown within the loop should not prevent other lines to be read
                    //But update the errorcode though, since it ddn't go on plan nonetheless
                    errorcode = true;
                    System.err.println("Error on entry " + i + ": " + (file!=null ? file.getPath() + ": " : "") + e);
                }
            }

            //Explicitly drop current session (optional but nicer)
            importer.closeSession();

        } catch (Exception e) {
            errorcode = true;
            System.err.println("Global error: " + e);
        } finally {
            //Bye
            System.out.println("Terminating");
            if (br != null) {
                br.close();
            }
            System.exit(errorcode ? 1 : 0);
        }
    }
}
