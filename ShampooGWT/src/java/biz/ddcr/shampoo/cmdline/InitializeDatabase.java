/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.cmdline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.util.ConfigHelper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class InitializeDatabase {

    private final static String USER_SCRIPT = "/import.sql";
    
    private static List<String> parseScript(Reader importFileReader) throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(importFileReader);
            //long lineNo = 0;
            for (String sql = reader.readLine(); sql != null; sql = reader.readLine()) {
                //lineNo++;
                String trimmedSql = sql.trim();
                if (trimmedSql.length() == 0
                        || trimmedSql.startsWith("--")
                        || trimmedSql.startsWith("//")
                        || trimmedSql.startsWith("/*")) {
                    continue;
                } else {
                    if (trimmedSql.endsWith(";")) {
                        trimmedSql = trimmedSql.substring(0, trimmedSql.length() - 1);
                    }
                    result.add(trimmedSql);
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return result;
    }

    public static void importScript(LocalSessionFactoryBean sessionFactory, String importFile) throws IOException {

        Reader importFileReader = null;
        try {
            InputStream stream = ConfigHelper.getResourceAsStream(importFile);
            importFileReader = new InputStreamReader(stream);

            HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory.getObject());
            for (final String statement : parseScript(importFileReader)) {
                hibernateTemplate.execute(new HibernateCallback() {

                    @Override
                    public Object doInHibernate(Session session) throws HibernateException, SQLException {
                        Query query = session.createSQLQuery(statement);
                        return query.executeUpdate();
                    }
                });
            }

        } finally {
            if (importFileReader != null) {
                importFileReader.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        //ERRORLEVEL for shells
        boolean errorcode = false;
        try {
            System.out.println("Loading full context (please be patient)");
            //Bind the current application context; it's the full Web Applicaion Context, so it's über heavy
            ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContextCore.xml");
            LocalSessionFactoryBean sessionFactory = (LocalSessionFactoryBean) context.getBean("&sessionFactory");
            System.out.println("Creating database schema");
            sessionFactory.createDatabaseSchema();
            System.out.println("Populating the default Administrator account");
            importScript(sessionFactory, USER_SCRIPT);

        } catch (Exception e) {
            errorcode = true;
            System.err.println(e);
        } finally {
            //Bye
            System.out.println("Terminating");
            System.exit(errorcode ? 1 : 0);
        }
    }
}
