/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.cmdline;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class UpdateDatabase {

    public static void main(String[] args) throws Exception {
        //ERRORLEVEL for shells
        boolean errorcode = false;
        try {
            System.out.println("Loading full context (please be patient)");
            //Bind the current application context; it's the full Web Applicaion Context, so it's über heavy
            ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContextCore.xml");
            LocalSessionFactoryBean sessionFactory = (LocalSessionFactoryBean) context.getBean("&sessionFactory");
            System.out.println("Updating database schema");
            sessionFactory.updateDatabaseSchema();
        } catch (Exception e) {
            errorcode = true;
            System.err.println(e);
        } finally {
            //Bye
            System.out.println("Terminating");
            System.exit(errorcode ? 1 : 0);
        }
    }
}
