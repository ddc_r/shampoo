/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.cmdline;

import biz.ddcr.shampoo.client.helper.text.NaiveCSV;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import biz.ddcr.shampoo.server.helper.FinalBoolean;
import org.discogs.model.ReleaseArtist;
import org.discogs.ws.Discogs;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import biz.ddcr.shampoo.server.io.parser.TagFormatter;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.UntypedMemoryStream;
import biz.ddcr.shampoo.server.io.util.UntypedStream;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.benow.xml.sax.ObjectProducer;
import org.benow.xml.sax.ObjectProducer.ProductionHandler;
import org.discogs.model.Release;
import org.discogs.model.Track;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Simple local batch importer for tracks from legacy systems
 * Transaction-less, and synchronous
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class UpdateDiscogsTracks extends EmulatedHttpSessionBean {

    protected class DoubleString {

        public String string1;
        public String string2;

        public DoubleString(String string1, String string2) {
            this.string1 = string1;
            this.string2 = string2;
        }
    }

    public class ReleaseExtendedProducer extends ObjectProducer<ExtendedRelease> {

        protected Discogs client;

        public ReleaseExtendedProducer(Discogs client) {
            this();
            this.client = client;
        }

        public ReleaseExtendedProducer() {
            super("release");
        }

        @Override
        protected ExtendedRelease createObject(Element constructed) {
            return new ExtendedRelease(constructed, client);
        }

        @Override
        protected InputStream createStream(File src) throws IOException {
            return new BufferedInputStream(new GZIPInputStream(new FileInputStream(src)));
        }
    }

    /** Discogs releae with extended tracks **/
    public class ExtendedRelease extends Release {

        public ExtendedRelease(Element releaseElem, Discogs client) {
            super(releaseElem, client);
        }

        public List<ExtendedTrack> getExtendedTracks() {
            List<ExtendedTrack> tracks = new ArrayList<ExtendedTrack>();
            Element gE = (Element) getNodeByPath("tracklist");
            if (gE != null) {
                NodeList ges = gE.getElementsByTagName("track");
                for (int i = 0; i < ges.getLength(); i++) {
                    Element curr = (Element) ges.item(i);
                    tracks.add(new ExtendedTrack(curr, client));
                }
            }
            return tracks;
        }

        public List<String> getImageURLs() {
            List<String> results = new ArrayList<String>();
            Element imagesE = (Element) getNodeByPath("images");
            if (imagesE != null) {
                NodeList cn = imagesE.getElementsByTagName("image");
                for (int i = 0; i < cn.getLength(); i++) {
                    results.add(((Element) cn.item(i)).getAttribute("uri"));
                }
            }
            return results;
        }
    }

    /** Discogs track with artists **/
    public class ExtendedTrack extends Track {

        protected transient Discogs client;

        public ExtendedTrack(Element item, Discogs client) {
            super(item);
            this.client = client;
        }

        public List<ReleaseArtist> getArtists() {
            List<ReleaseArtist> results = new ArrayList<ReleaseArtist>();
            Element aE = (Element) getNodeByPath("artists");
            if (aE != null) {
                NodeList cn = aE.getElementsByTagName("artist");
                for (int i = 0; i < cn.getLength(); i++) {
                    results.add(new ReleaseArtist((Element) cn.item(i),
                            client));
                }
            }
            return results;
        }
    }
    private File gzDiscogsReleasesXMLFile;
    private ReleaseExtendedProducer releaseProducer;
    private DefaultTransactionDefinition transactionDefinition;
    private PlatformTransactionManager transactionManager;
    private DatastoreService datastoreService;
    private TrackManagerInterface trackManager;
    private ProgrammeManagerInterface programmeManager;
    private CoverArtConfigurationHelper coverArtConfigurationHelper;
    private boolean strictSearch = true;
    private boolean releaseOnlySearch = false;
    private static final Pattern discogsPatternThe = Pattern.compile("(.+), The");
    private static final Pattern discogsPatternAlias = Pattern.compile("(.+) \\(\\d+\\)");
    private static final Pattern discogsPatternNoRemix = Pattern.compile("(.+) \\(\\.+\\)");

    public UpdateDiscogsTracks(File gzDiscogsReleasesXMLFile, PlatformTransactionManager transactionManager, DatastoreService datastoreService, SecurityManagerInterface securityManager, TrackManagerInterface trackManager, ProgrammeManagerInterface programmeManager, CoverArtConfigurationHelper coverArtConfigurationHelper) {
        super(securityManager);
        this.datastoreService = datastoreService;
        this.trackManager = trackManager;
        this.programmeManager = programmeManager;
        this.coverArtConfigurationHelper = coverArtConfigurationHelper;
        this.transactionManager = transactionManager;
        this.gzDiscogsReleasesXMLFile = gzDiscogsReleasesXMLFile;
        transactionDefinition = new DefaultTransactionDefinition();
        //Read-Write
        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        releaseProducer = new ReleaseExtendedProducer();
        //Look-ahead off
        releaseProducer.setPreCalculate(false);
    }

    public boolean isReleaseOnlySearch() {
        return releaseOnlySearch;
    }

    public void setReleaseOnlySearch(boolean releaseOnlySearch) {
        this.releaseOnlySearch = releaseOnlySearch;
    }

    public boolean isStrictSearch() {
        return strictSearch;
    }

    public void setStrictSearch(boolean strictSearch) {
        this.strictSearch = strictSearch;
    }

    public ReleaseExtendedProducer getReleaseProducer() {
        return releaseProducer;
    }

    public TransactionStatus startTransaction() {
        return transactionManager.getTransaction(transactionDefinition);
    }

    public void commitTransaction(TransactionStatus status) {
        transactionManager.commit(status);
    }

    public void rollbackTransaction(TransactionStatus status) {
        transactionManager.rollback(status);
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public CoverArtConfigurationHelper getCoverArtConfigurationHelper() {
        return coverArtConfigurationHelper;
    }

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    /**
     * Check if a File can be used by Shampoo and make it a proper Track Stream
     * @param file
     * @return
     * @throws Exception
     */
    protected PictureStream toPictureStream(URL file) throws Exception {
        UntypedStream stream = new UntypedMemoryStream(file.toString(), IOUtil.readToEndOnePass(file.openStream()));

        //First check if the file is valid
        //Extensions alone are nothing short than useless except if they can help filtering out false positives that will be more thoroughly examined later on
        //predefined MIME types are automatically filled in if extensions appear to be valid
        PICTURE_FORMAT guessedFormat = getCoverArtConfigurationHelper().filterOutUnwantedExtensions(file.getPath());
        if (guessedFormat == null) {
            throw new UploadingWrongFormatException();
        }
        //Now sniff the header for fingerprints, and even if it's oh-so slow

        //Resize the picture if applicable
        try {
            if (getCoverArtConfigurationHelper().mustResizeIfApplicable()) {
                PICTURE_FORMAT newFormat = TagFormatter.rescalePicture(getCoverArtConfigurationHelper(), stream);
                if (newFormat!=null) guessedFormat = newFormat;
            }
        } catch (IOException e) {
            throw new UploadingWrongFormatException();
        }

        //Then wrap the stream to be stored
        PictureFileInfo container = new PictureFileInfo();
        container.setFormat(guessedFormat);
        container.setSize(stream.getSize());

        if (container == null || !getCoverArtConfigurationHelper().isAcceptFormat(container.getFormat())) {
            throw new UploadingWrongFormatException();
        }

        return new PictureStream(container, stream);
    }

    protected String normalizeArtistName(String text) {
        if (text!=null && text.length()!=0) {
            Matcher m1 = discogsPatternThe.matcher(text);
            if (m1.matches()) {
                text = "The " + m1.group(1);
            }

            Matcher m2 = discogsPatternAlias.matcher(text);
            if (m2.matches()) {
                text = m2.group(1);
            }
            return text;
        }
        return null;
    }
    protected String normalizeTrackName(String text) {
        if (text!=null && text.length()!=0) {
            if (!isStrictSearch()) {
                Matcher m1 = discogsPatternNoRemix.matcher(text);
                if (m1.matches()) {
                    text = m1.group(1);
                }
            }
            return text;
        }
        return null;
    }

    protected List<DoubleString> parseArtistNames(List<ReleaseArtist> releaseArtists) throws Exception {
        List<DoubleString> modReleaseArtists = new ArrayList<DoubleString>();
        //Find artist match

        if (releaseArtists != null) {
            for (ReleaseArtist releaseArtist : releaseArtists) {
                String artistName = releaseArtist.getName();
                String anv = releaseArtist.getANV();
                DoubleString names = new DoubleString(normalizeArtistName(artistName), normalizeArtistName(anv));
                modReleaseArtists.add(names);
            }
        }
        return modReleaseArtists;

    }

    protected boolean processTrack(BroadcastableSong matchedSong, ExtendedTrack discogsTrack, ExtendedRelease discogsRelease) throws Exception {
        boolean result = false;
        boolean needsUpdate = false;
        boolean needsPictureUpdate = false;

        Collection<Integer> transactions = null;
        //Open a transaction for each entry: it's a resource hog but failed entries should not block valid ones
        //Open a new DB session with a transaction
        TransactionStatus status = startTransaction();

        try {

            System.out.println("New entry for: "+matchedSong.getAuthor()+" - "+matchedSong.getTitle());

            if ((matchedSong.getAlbum() == null || matchedSong.getAlbum().length()==0)
                    && (discogsRelease.getTitle() != null && discogsRelease.getTitle().length()!=0)) {
                System.out.println("Updating album to: " + discogsRelease.getTitle());
                needsUpdate = true;
                matchedSong.setAlbum(discogsRelease.getTitle());
            }
            if ((matchedSong.getGenre() == null || matchedSong.getGenre().length()==0)
                    && (discogsRelease.getGenres() != null && !discogsRelease.getGenres().isEmpty())) {
                String csvGenres = NaiveCSV.toString(discogsRelease.getGenres(), false);
                System.out.println("Updating genres to: " + csvGenres);
                needsUpdate = true;
                matchedSong.setGenre(
                        csvGenres);
            }
            if ((matchedSong.getTag() == null || matchedSong.getTag().length()==0)
                    && (discogsRelease.getStyles() != null && !discogsRelease.getStyles().isEmpty())) {
                String csvStyles = NaiveCSV.toString(discogsRelease.getStyles(), false);
                System.out.println("Updating tag to: " + csvStyles);
                needsUpdate = true;
                matchedSong.setTag(
                        csvStyles);
            }
            if ((matchedSong.getDateOfRelease() == null)
                    && (discogsRelease.getReleaseDate() != null)) {
                needsUpdate = true;
                short releaseDate = (short) discogsRelease.getReleaseDate().getYear();
                if (releaseDate < 100) {
                    //Java Dates are a bitch!
                    releaseDate += 1900;
                }
                System.out.println("Updating release date to: " + releaseDate);
                matchedSong.setDateOfRelease(
                        releaseDate);
            }
            if (discogsRelease.getLabelReleases() != null && !discogsRelease.getLabelReleases().isEmpty()) {
                String labelName = discogsRelease.getLabelReleases().get(0).getLabelName();
                if ((matchedSong.getPublisher() == null || matchedSong.getPublisher().length()==0)
                        && (labelName!=null && labelName.length()!=0)) {
                    System.out.println("Updating publisher to: " + labelName);
                    needsUpdate = true;
                    matchedSong.setPublisher(labelName);
                }
            }
            PictureStream pictureStream = null;
            if ((matchedSong.getCoverArtContainer() == null)
                    && (discogsRelease.getImageURLs() != null && !discogsRelease.getImageURLs().isEmpty())) {
                int i = 0;
                while (pictureStream == null && i < discogsRelease.getImageURLs().size()) {
                    System.out.println("Updating picture to: " + discogsRelease.getImageURLs().get(i));
                    URL url = new URL(discogsRelease.getImageURLs().get(i));
                    try {
                        pictureStream = toPictureStream(url);
                    } catch (UploadingWrongFormatException e) {
                        //Not a good file, try with the next one
                        pictureStream = null;
                    }
                    i++;
                }
                if (pictureStream!=null) {
                    needsUpdate = true;
                    needsPictureUpdate = true;
                }
            }

            //Containers info that need to be imported from external data are handled elsewhere
            //Don't instantiate empty containers if the form has no container attached to it
            if (needsPictureUpdate) {
                transactions = getDatastoreService().copyTrackToDataStore(matchedSong, null, pictureStream);
            }

            //Persist in database
            if (needsUpdate) {
                getTrackManager().updateTrack(matchedSong);
            } else {
                System.out.println("Nothing updated");
            }

            result = needsUpdate;
        } catch (Exception e) {
            //Rollback from datastore
            if (transactions != null) {
                getDatastoreService().rollbackChangesInDataStore(transactions);
            }
            //And DB
            rollbackTransaction(status);
            //and re-throw
            throw e;
        }
        //Commit in datastore
        if (transactions != null) {
            getDatastoreService().commitChangesInDataStore(transactions);
        }
        //And commit in DB
        commitTransaction(status);

        return result;
    }

    protected boolean checkTitles(String title1, String title2) {
        if (title1!=null && title2!=null) {
            return normalizeTrackName(title1).equalsIgnoreCase(normalizeTrackName(title2));
        }
        return false;
    }

    protected boolean checkTrack(Collection<BroadcastableSong> artistsSongs, ExtendedTrack track, ExtendedRelease release) throws Exception {
        for (BroadcastableSong artistsSong : artistsSongs) {
            if (checkTitles(artistsSong.getTitle(),track.getTitle())) {
                if (processTrack(artistsSong, track, release)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected List<String> generateDoubleStringCombinations(List<DoubleString> list) {
        //Generating all combinations is too expensive, just combine names with ANVs IFF there's only one artist, do NOT shift the name positions, nor include ANVs in case of multiple artists
        List<String> combos = new ArrayList<String>();
        if (list.size()==1) {
            DoubleString doubleString = list.get(0);
            if (doubleString.string1!=null) combos.add(doubleString.string1);
            if (doubleString.string2!=null) combos.add(doubleString.string2);
        } else if (list.size()>1) {
            for (DoubleString doubleString : list) {
                //prefer ANVs over plain names
                if (doubleString.string2!=null)
                    combos.add(doubleString.string2);
                else if (doubleString.string1!=null)
                    combos.add(doubleString.string1);
            }
        }

        return combos;
    }

    protected void browseXMLEntry(ObjectProducer<ExtendedRelease> producer, ExtendedRelease release) throws Exception {
        //Find artist match
        List<DoubleString> artistNames = parseArtistNames(release.getArtists());
        if (artistNames.isEmpty()) {
            throw new IllegalArgumentException("No artists for this release");
        }
        //Compilation?
        if (artistNames.get(0).string1 != null && artistNames.get(0).string1.equals("Various")) {
            if (!isReleaseOnlySearch())
                for (ExtendedTrack track : release.getExtendedTracks()) {
                    List<DoubleString> trackArtistNames = parseArtistNames(track.getArtists());
                    if (trackArtistNames.isEmpty()) {
                        throw new IllegalArgumentException("No artists for this compilation release");
                    }
                    Collection<BroadcastableSong> artistsSongs = getTrackManager().getFuzzyBroadcastSongsForAuthorNames(generateDoubleStringCombinations(trackArtistNames), trackArtistNames.size()>1);
                    if (artistsSongs.size() > 0) {
                        checkTrack(artistsSongs, track, release);
                    }
                }
        } else {
            Collection<BroadcastableSong> artistsSongs = getTrackManager().getFuzzyBroadcastSongsForAuthorNames(generateDoubleStringCombinations(artistNames), artistNames.size()>1);
            if (artistsSongs.size() > 0) {
                for (ExtendedTrack track : release.getExtendedTracks()) {
                    checkTrack(artistsSongs, track, release);
                }
            }
        }

    }

    public boolean browseXML() throws Exception {
        final FinalBoolean error = new FinalBoolean();
        error.value = false;
        getReleaseProducer().produce(gzDiscogsReleasesXMLFile, new ProductionHandler<ExtendedRelease>() {

            @Override
            public void onProduce(ObjectProducer<ExtendedRelease> producer, ExtendedRelease obj) {
                try {
                    browseXMLEntry(producer, obj);
                } catch (Exception ex) {
                    System.err.println("Error on entry " + producer.getCurrentObjectNumber() + ": " + ex);
                    error.value = true;
                }
            }
        });
        return error.value;
    }

    public static void main(String[] args) throws Exception {

        //ERRORLEVEL for shells
        boolean errorcode = false;
        File gzDiscogsReleasesXMLFile = null;
        String username, password;

        try {

            //First check if parameters are ok, otherwise initiate early termination
            if (args == null || args.length < 3 || args.length > 4) {
                throw new IllegalArgumentException("Wrong number of parameters: <username> <password> <gzDiscogsReleasesXMLDump> [options...]");
            }
            username = args[0];
            password = args[1];

            System.out.println("Opening Discogs Releases dump file");
            gzDiscogsReleasesXMLFile = new File(args[2]);

            System.out.println("Loading full context (please be patient)");
            //Bind the current application context; it's the full Web Applicaion Context, so it's über heavy
            ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContextCore.xml");
            //And create a Spring-unmanaged bean tha will actualy performs the conversion
            SecurityManagerInterface securityManager = (SecurityManagerInterface) context.getBean("securityManager");
            TrackManagerInterface trackManager = (TrackManagerInterface) context.getBean("trackManager");
            ProgrammeManagerInterface programmeManager = (ProgrammeManagerInterface) context.getBean("programmeManager");
            DatastoreService datastoreService = (DatastoreService) context.getBean("datastoreService");
            CoverArtConfigurationHelper coverArtConfigurationHelper = (CoverArtConfigurationHelper) context.getBean("coverArtConfigurationHelper");
            PlatformTransactionManager transactionManager = (PlatformTransactionManager) context.getBean("transactionManager");
            UpdateDiscogsTracks importer = new UpdateDiscogsTracks(gzDiscogsReleasesXMLFile, transactionManager, datastoreService, securityManager, trackManager, programmeManager, coverArtConfigurationHelper);
            importer.setStrictSearch(!(args.length>3 && args[3].contains("l")));
            importer.setReleaseOnlySearch(args.length>3 && args[3].contains("r"));

            importer.openSession(username, password);

            //parse the CSV file one line after another
            System.out.println("Parsing Discogs Releases dump file");
            errorcode = importer.browseXML();

            //Explicitly drop current session (optional but nicer)
            importer.closeSession();

        } catch (Exception e) {
            errorcode = true;
            System.err.println("Global error: " + e);
        } finally {
            //Bye
            System.out.println("Terminating");
            System.exit(errorcode ? 1 : 0);
        }
    }
}
