/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.cmdline;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public final class XMLUnicode2ASCII {

    private static boolean isGzippedFile(String filename) {
        int dot = filename.lastIndexOf(".");
        return "gz".equalsIgnoreCase(filename.substring(dot+1,filename.length()));
    }
    
    private static InputStream input(String filename, boolean isGzippedFile) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(new File(filename));
        return isGzippedFile ? new GZIPInputStream(is) : is;
    }
    private static OutputStream output(String filename, boolean isGzippedFile) throws FileNotFoundException, IOException {
        OutputStream os = new FileOutputStream(new File(filename));
        return isGzippedFile ? new GZIPOutputStream(os) : os;
    }
    
    public static void main(String[] args) {

        try {

            boolean isGzippedFile = isGzippedFile(args[0]);
            BufferedReader in = new BufferedReader(new InputStreamReader(input(args[0], isGzippedFile), "UTF8"));
            Writer out = new BufferedWriter(new OutputStreamWriter(output(args[0] + "-CLEANED", isGzippedFile), "UTF8"));

            int c;
            String s;
            while ((c = in.read()) != -1) {
                if ((c >= 0x0020 && c <= 0xD7FF)
                        || (c >= 0xE000 && c <= 0xFFFD)
                        || c == 0x0009
                        || c == 0x000A
                        || c == 0x000D) {
                    if (c > 127) {
                        out.write('&');
                        out.write('#');
                        s = Long.toString(c);
                        for (int i=0;i<s.length();i++)
                            out.write(s.charAt(i));
                        out.write(';');
                    } else {
                        out.write(c);
                    }
                }
            }

            in.close();
            out.close();
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    private XMLUnicode2ASCII() {
        //No instantiation
    }
}
