/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.dao;

import biz.ddcr.shampoo.client.form.GenericFormInterface.TAGS;
import biz.ddcr.shampoo.client.helper.errors.MalformedFilterExpressionException;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 *
 * @author okay_awright
 **/
public abstract class GenericHibernateDao extends HibernateDaoSupport {

    protected final static Pattern whereKeywordPattern;
    static {
        whereKeywordPattern = Pattern.compile("[\\)\\s]([Ww][Hh][Ee][Rr][Ee])[\\s\\(]");
    }
    
    /** Set it to True if your RDBMS fully supports Cursor*/
    private boolean cursorSupported;
    public boolean isCursorSupported() {
        return cursorSupported;
    }
    public void setCursorSupported(boolean cursorSupported) {
        this.cursorSupported = cursorSupported;
    }    
    
    protected interface RowHandler<K> {

        /**
         * Store SQL row in result set.
         *
         * Each time a row is fetched, this callback is used.
         * The actual row is inputRow and the object to return will be added to the final output.
         * Return null if there's nothing to add to the output.
         *
         * Keep the code simple and fast inside this method.
         *
         * @param inputRow
         * @param output
         * @return
         **/
        public K onRow(ScrollableResults inputRow);
    }

    protected interface XQLObjectTranslatorHandler<K extends TAGS> {

        /**
         * Generate a HQL-compatible string from a TAGS
         *
         * Each time a TAGS needs to be converted into its HQL equivalent, this callback is used.
         * This is the responsability of this method to generate a properly valid HQL command used for sorting out results. Only the objects and components are required, do not prepend ORDER BY or any DESC/ASC modifiers.
         * Return null if there's nothing to add to the output.
         *
         * @param inputRow
         * @param output
         * @return
         **/
        public String[] onTag(K currentTag);
    }

    /**
     * Placeholder
     * @return
     */
    protected HibernateTemplate getTemplate() {
        return getHibernateTemplate();
    }

    public void forceFlush() {
        getTemplate().getSessionFactory().getCurrentSession().flush();
    }

    /**
     * Automatically gather a Projection for an Hibernate Criteria with *all* the properties of a class
     * Handy when you need to use a Projection as a workaround for the absence of server-side DISTINCT in Criteria
     *
     * @param properties
     * @return
     **/
    protected ProjectionList fullPropertiesProjection(Class hibernateClass) {
        String[] properties = getTemplate().getSessionFactory().getClassMetadata(hibernateClass).getPropertyNames();
        ProjectionList list = Projections.projectionList();
        for (int i = 0; i < properties.length; ++i) {
            list.add(Projections.property(properties[i]), properties[i]);
        }
        return list;
    }

    protected static StringBuffer appendSQLItem(StringBuffer s, String objectPrefix, String i) {
        return (objectPrefix != null ? (s.append(objectPrefix).append('.').append(i)) : s.append(i));
    }

    protected static String generateSQLChannelIdList(String sqlOperator, String objectPrefix, Collection<Channel> channels) {
        HashSet<String> ids = new HashSet<String>();
        for (Channel channel : channels) {
            ids.add(channel.getLabel());
        }
        return GenericHibernateDao.generateSQLList(true, sqlOperator, objectPrefix, ids);
    }

    protected static String generateSQLList(boolean addOpeningAndClosingBrackets, String sqlOperator, String objectPrefix, Collection<String> list) {
        if (list == null || list.isEmpty()) {
            return "";
        }

        //I'm no fond of the Criteria API in its current shape, let's do some dynamic String building
        StringBuffer concat = new StringBuffer();

        Iterator i = list.iterator();
        if (addOpeningAndClosingBrackets) {
            concat.append('(');
        }
        if (i.hasNext()) {
            appendSQLItem(concat, objectPrefix, (String) i.next());
        }
        while (i.hasNext()) {
            concat.append(' ').append(sqlOperator).append(' ');
            appendSQLItem(concat, objectPrefix, (String) i.next());
        }
        if (addOpeningAndClosingBrackets) {
            concat.append(')');
        }

        return concat.toString();
    }

    protected void batchSave(Session currentHibernateSession, Collection<? extends GenericEntityInterface> entitiesToSave) {
        //Using stateless sessions instead of the default session throws UnsuportedOperationException with Hibernate 3.2
        int absI = 0;
        for (GenericEntityInterface entityToSave : entitiesToSave) {

            currentHibernateSession.save(entityToSave);

            //Flush periodically in order not to saturate Hibernate's first-level cache (a bit of trial and error here to set the correct threshold)
            if (++absI % 100 == 0) {
                currentHibernateSession.flush();
                currentHibernateSession.clear();
            }
        }

    }

    protected void batchUpdate(Session currentHibernateSession, Collection<? extends GenericEntityInterface> entitiesToUpdate) {
        //Using stateless sessions instead of the default session throws UnsuportedOperationException with Hibernate 3.2
        int absI = 0;
        for (GenericEntityInterface entityToUpdate : entitiesToUpdate) {

            currentHibernateSession.update(entityToUpdate);

            //Flush periodically in order not to saturate Hibernate's first-level cache (a bit of trial and error here to set the correct threshold)
            if (++absI % 100 == 0) {
                currentHibernateSession.flush();
                currentHibernateSession.clear();
            }
        }

    }

    /**
     * Use the ScrollableResults of an Hibernate query to fill in a GenericActionMap.
     * The group and sort object is used to point the cursor at a specific location and limit the number of rows returned.
     * The RowHandler callback method onRow is called each time a row is fetched from the set and is used whether to store or not values of this set and process them if necessary on a case by case basis
     *
     * @return
     **/
    protected Collection<? extends GenericEntityInterface> scrollResults(Session currentHibernateSession, ScrollableResults rawData, GroupAndSort constraint, RowHandler<? extends GenericEntityInterface> rowHandler) {
        //Aggregate and sort out the output
        LinkedHashSet<GenericEntityInterface> output = new LinkedHashSet<GenericEntityInterface>();

        //Check first if there is any row to process, otherwise prematurely exits
        boolean prematureExit = true;

        if (rawData != null) {
            try {
                //Point the cursor at the right starting position
                //Hibernate 3 BUG: moving through records when the original query used joins leads to an Exception
                //At the moment there's no workaround, except refactoring the HQL
                if (isCursorSupported() && constraint != null && (constraint.getStartRow() != null && constraint.getStartRow() != 0)) {
                    if (rawData.setRowNumber(constraint.getStartRow())) {
                        prematureExit = false;
                    }
                } else {
                    if (rawData.first()) {
                        prematureExit = false;
                    }
                }
                //Hibernate 3 bug workaround: cannot use scroll on an empty set
            } catch (GenericJDBCException e) {
                //Silently drop the "cannot scroll when no data" exception; JIRA#1804
                //Quick FIX for Hibernate < 3.5
                //if (!e.getMessage().equalsIgnoreCase("could not perform sequential read of results (forward)")) {
                //Quick FIX for Hibernate >= 3.5
                if (!e.getMessage().equalsIgnoreCase("could not doAfterTransactionCompletion sequential read of results (forward)")) {
                    throw e;
                }
            }
        }

        if (!prematureExit) {
            int i = 0;
            //int absI = 0;
            //Compute the maximum number of records to fetch according to constraint
            //The limit is topped at the maximum capacity of Integers
            int iMax = (isCursorSupported() && constraint != null && constraint.getMaxRows() != null ? constraint.getMaxRows() : Integer.MAX_VALUE);
            while (iMax > i) {
                //Fetch the Entity to store in the GenericActionMap
                GenericEntityInterface e = (GenericEntityInterface) rowHandler.onRow(rawData);
                //Add the entity to the output
                //And increment the cursor if merging is successful

                if (e != null && output.add(e)) {
                    i++;
                }
                //Flush periodically in order not to saturate Hibernate's first-level cache (a bit of trial and error here to set the correct threshold)
                //BUG FIX: misplaced call, should not be set during SELECT but after INSERT, DELETE and UPDATE
                /*if (++absI % 100 == 0) {
                    currentHibernateSession.flush();
                    currentHibernateSession.clear();
                }*/
                //Goto next row if available
                if (!rawData.next()) {
                    break;
                }
            }
        }

        return output;
    }

    protected StringBuilder insertAfterWhereStatement(StringBuilder hqlQuery, StringBuilder statementToInsert) {
        if (statementToInsert != null && statementToInsert.length() > 0) {
            //Insert it just after the WHERE keyword from the request, otherwise append it to the request
            Matcher matcher = whereKeywordPattern.matcher(hqlQuery);
            boolean matchFound = matcher.find();
            if (matchFound) {
                //Matcher works with Strings only
                hqlQuery = new StringBuilder(matcher.replaceFirst(" $1 ( " + statementToInsert.toString() + " ) AND ( ")).append(" )");
            } else {
                hqlQuery.append(" WHERE ").append(statementToInsert);
            }
        }
        return hqlQuery;
    }

    protected Query makeQuery(Session session, StringBuilder hqlQuery, GroupAndSort<? extends TAGS> constraint, XQLObjectTranslatorHandler<? extends TAGS> tagTranslator) {
        Query query = null;
        if (session != null && hqlQuery != null) {

            //First try to add the filter commands, if applicable
            StringBuilder filterCommand = generateHQLFilterCommand(constraint, tagTranslator);
            hqlQuery = insertAfterWhereStatement(hqlQuery, filterCommand);

            //Then try to add the sort commands, if applicable
            StringBuilder sortCommand = generateHQLSortCommand(constraint, tagTranslator);
            if (sortCommand != null && sortCommand.length() > 0) {
                hqlQuery.append(" ORDER BY ").append(sortCommand);
            }

            //Now generate the query
            query = session.createQuery(hqlQuery.toString());
            //And add the filter wildcard, if applicable
            if (filterCommand != null && filterCommand.length() > 0) {
                switch (constraint.getFilterByFeature().getLooseType()) {
                    case alphanumeric:
                        //Dirty
                        if (constraint.getFilterOperator().contains("LIKE")) {
                            query.setString("filterCriterion", "%" + constraint.getFilterCriterion() + "%");
                        } else {
                            query.setString("filterCriterion", constraint.getFilterCriterion());
                        }
                        break;
                    case numeric:
                        try {
                            query.setInteger("filterCriterion", Integer.parseInt(constraint.getFilterCriterion()));
                        } catch (NumberFormatException e) {
                            throw new MalformedFilterExpressionException();
                        }
                        break;
                }
            }
            //Pagination through cursor? then don't use Hibernate SQL-supported setFirst/setMax
            if (!isCursorSupported()) {
                if (constraint != null && (constraint.getStartRow() != null && constraint.getStartRow() != 0))
                    query.setFirstResult(constraint.getStartRow());
                if (constraint != null && constraint.getMaxRows() != null && constraint.getMaxRows()>0)
                    query.setMaxResults(constraint.getMaxRows());
            }
            //add some misc. info
            if (constraint != null && constraint.getMaxRows() != null && constraint.getMaxRows()>0)
                query.setFetchSize(constraint.getMaxRows());
            query.setReadOnly(true);

        }
        return query;
    }

    public StringBuilder generateHQLSortCommand(GroupAndSort<? extends TAGS> constraint, XQLObjectTranslatorHandler sortCommandTranslatorCallback) {
        return generateXQLSortCommand(constraint, sortCommandTranslatorCallback);
    }

    private StringBuilder generateXQLSortCommand(GroupAndSort<? extends TAGS> constraint, XQLObjectTranslatorHandler sortCommandTranslatorCallback) {
        String[] sortCommand = null;
        if (constraint == null
                || constraint.getSortByFeature() == null
                || sortCommandTranslatorCallback == null
                || (sortCommand = sortCommandTranslatorCallback.onTag(constraint.getSortByFeature())) == null
                || sortCommand.length < 1) {
            return null;
        } else {
            StringBuilder translatedCommand = new StringBuilder();
            for (int i = 0; i < sortCommand.length; i++) {
                if (i != 0) {
                    translatedCommand.append(", ");
                }
                translatedCommand.append(sortCommand[i]).append(" ").append(constraint.getSortByDescendingDirection() ? "DESC" : "ASC");
            }
            return translatedCommand;
        }
    }

    public StringBuilder generateHQLFilterCommand(GroupAndSort<? extends TAGS> constraint, XQLObjectTranslatorHandler filterCommandTranslatorCallback) {
        return generateXQLFilterCommand(constraint, filterCommandTranslatorCallback);
    }

    private StringBuilder generateXQLFilterCommand(GroupAndSort<? extends TAGS> constraint, XQLObjectTranslatorHandler filterCommandTranslatorCallback) {
        String[] filterCommand = null;
        if (constraint == null
                || constraint.getFilterByFeature() == null
                || constraint.getFilterOperator() == null
                || constraint.getFilterCriterion() == null
                || filterCommandTranslatorCallback == null
                || (filterCommand = filterCommandTranslatorCallback.onTag(constraint.getFilterByFeature())) == null
                || filterCommand.length < 1) {
            return null;
        } else {
            StringBuilder translatedCommand = new StringBuilder();
            for (int i = 0; i < filterCommand.length; i++) {
                if (i != 0) {
                    translatedCommand.append(" OR ");
                } else {
                    translatedCommand.append(" (");
                }
                translatedCommand.append(filterCommand[i]).append(" ") //TODO Untranslated, make it SQL aware
                        .append(constraint.getFilterOperator()).append(" ").append(":filterCriterion");
                if (i == filterCommand.length - 1) {
                    translatedCommand.append(") ");
                }
            }
            //Fill in the parameter when the Query is created
            return translatedCommand;
        }
    }

    /**
     *@deprecated Huge performance drop when using DTO and sessions are only very short
     *
     * Workaround for Hibernate lazy-loading with objects instantiated outside a DAO
     * Should only be used within LoginManager for objects pulled from an HTTP session
     *
     * Call upon this method before any hydration of an entity properties or the entty itself
     *
     * @param user
     **/
    public GenericEntityInterface reattachEntityToSessionIfRequired(GenericEntityInterface entity) {
        if (entity == null) {
            return null;
        }
        Session currentSession = getTemplate().getSessionFactory().getCurrentSession();
        if (!alreadyInSession(entity)) {
            return (GenericEntityInterface) currentSession.merge(entity);
        } else {
            return entity;
        }
    }

    /**
     * @deprecated
     * @param entity
     * @return
     **/
    public boolean alreadyInSession(GenericEntityInterface entity) {
        if (entity == null) {
            return false;
        }
        Session currentSession = getTemplate().getSessionFactory().getCurrentSession();
        return (currentSession.contains(entity));
    }
}
