/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.dao.archive;

import biz.ddcr.shampoo.client.form.archive.ArchiveForm.ARCHIVE_TAGS;
import biz.ddcr.shampoo.client.form.archive.ReportForm.REPORT_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.archive.RequestArchive;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ArchiveDao extends GenericHibernateDao {

    public void addArchive(Archive archive) throws DataAccessException {
        if (archive != null) {
            getTemplate().save(archive);
        }
    }
    public void addReport(Report report) throws DataAccessException {
        if (report != null) {
            getTemplate().save(report);
        }
    }

    /**
     * * TODO: getTemplate().getSessionFactory().getCurrentSession() hurls that no session is bound when used within an UnmanagedTransactionUnit, WTF!!! The transactionManager is correctly bound and a transaction is meant to be open in the FacadeService via annotation
     * @param archives
     * @throws DataAccessException
     */
    public void addBatchArchives(final Collection<? extends Archive> archives) throws DataAccessException {
        if (archives != null) {

            /*try {
            SessionFactory sf = getTemplate().getSessionFactory();
            if (sf.isClosed())
            logger.warn("Session is close");
            } catch (Exception e) {
            logger.error(e);
            }

            getTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {

            try {
            Session s = SessionFactoryUtils.getSession(getSessionFactory(), false);
            logger.warn("Is there a session ready? : "+s+"and current session is "+session);
            } catch (Exception e) {
            logger.error(e);
            }
            try {
            logger.warn("Does factory has transactional sessions ready? : "+SessionFactoryUtils.hasTransactionalSession(getSessionFactory()));
            } catch (Exception e) {
            logger.error(e);
            }
            try {
            logger.warn("Session transactional? : "+SessionFactoryUtils.isSessionTransactional(session, getSessionFactory()));
            } catch (Exception e) {
            logger.error(e);
            }

            batchSave(
            session,
            archives);
            return null;
            }
            });*/

            batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    archives);
        }
    }

    public void addBatchReports(final Collection<Report> reports) throws DataAccessException {
        if (reports != null) {
            batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    reports);
        }
    }
    
    public void updateArchive(Archive archive) throws DataAccessException {
        if (archive != null) {
            getTemplate().update(archive);
        }
    }
    
    public void updateReport(Report report) throws DataAccessException {
        if (report != null) {
            getTemplate().update(report);
        }
    }

    /**
     * * TODO: getTemplate().getSessionFactory().getCurrentSession() hurls that no session is bound when used within an UnmanagedTransactionUnit, WTF!!! The transactionManager is correctly bound and a transaction is meant to be open in the FacadeService via annotation
     * @param archives
     * @throws DataAccessException
     */
    public void updateBatchArchives(final Collection<? extends Archive> archives) throws DataAccessException {
        if (archives != null) {

            /*getTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {
            batchUpdate(
            session,
            archives);
            return null;
            }
            });*/

            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    archives);
        }
    }
    public void updateBatchReports(final Collection<Report> reports) throws DataAccessException {
        if (reports != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    reports);
        }
    }

    public void deleteArchive(Archive archive) throws DataAccessException {
        if (archive != null) {
            getTemplate().delete(archive);
        }
    }
    
    public void deleteReport(Report report) throws DataAccessException {
        if (report != null) {
            getTemplate().delete(report);
        }
    }

    public void deleteBatchArchives(Collection<? extends Archive> archives) throws DataAccessException {
        if (archives != null) {
            getTemplate().deleteAll(archives);
        }
    }
    
    public void deleteBatchReports(Collection<Report> reports) throws DataAccessException {
        if (reports != null) {
            getTemplate().deleteAll(reports);
        }
    }

    public Archive getArchive(final String id) throws DataAccessException {
        return getTemplate().get(Archive.class, id);
    }
    
    public Report getReport(final String id) throws DataAccessException {
        return getTemplate().get(Report.class, id);
    }

    public Collection<Archive> getArchives(Collection<String> ids) throws DataAccessException {
        Collection<Archive> archives = null;
        if (ids != null) {
            archives = new HashSet<Archive>();
            for (String id : ids) {
                Archive archive = getTemplate().get(Archive.class, id);
                if (archive!=null)
                    archives.add(archive);
            }
        }
        return archives;
    }
    
    public Collection<Report> getReports(Collection<String> ids) throws DataAccessException {
        Collection<Report> reports = null;
        if (ids != null) {
            reports = new HashSet<Report>();
            for (String id : ids) {
                Report report = getTemplate().get(Report.class, id);
                if (report!=null)
                    reports.add(report);
            }
        }
        return reports;
    }

    public Archive loadArchive(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Archive) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT ar "
                            + "FROM " + Archive.class.getCanonicalName() + " ar "
                            + "     LEFT JOIN FETCH ar.location.channel "
                            + "WHERE "
                            + "     ar.refID = :id ";

                    Query query = session.createQuery(hqlCommand);
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }
    
    public Report loadReport(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Report) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT r "
                            + "FROM " + Report.class.getCanonicalName() + " r "
                            + "     LEFT JOIN FETCH a.location.channel "
                            + "WHERE "
                            + "     r.refID = :id ";

                    Query query = session.createQuery(hqlCommand);
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }
    
    public Collection<Report> loadReports(final Collection<String> ids) throws DataAccessException {
        if (ids == null || ids.isEmpty()) {
            return new HashSet<Report>();
        } else {
            Collection<Report> list = (Collection<Report>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT r ")//
                            .append("FROM ").append(Report.class.getCanonicalName()).append(" r ")//
                            .append("     LEFT JOIN FETCH r.location.channel ")//
                            .append("WHERE ")//
                            .append("     r.refID IN (:refids) ");
                    Query query = session.createQuery(hqlCommand.toString());
                    query.setParameterList("refids", ids);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<Report>() : list;
        }
    }
    
    public Archive getArchiveFromQueue(final String queueId) throws DataAccessException {
        if (queueId == null) {
            return null;
        } else {
            return (Archive) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT ar "
                            + "FROM " + Archive.class.getCanonicalName() + " ar "
                            + "WHERE "
                            + "     ar.queueID = :id ";

                    Query query = session.createQuery(hqlCommand);
                    query.setString("id", queueId);
                    return query.uniqueResult();
                }
            });
        }
    }
    
    /**
     *
     * The selection boundary applies on a descending list, i.e. most recently played archives are sorted first
     *
     * @param clazz
     * @param constraint
     * @param programmeIds
     * @param currentlyAuthenticatedUserName
     * @return
     * @throws DataAccessException
     */
    public Collection<? extends Archive> getSelectedArchives(final String channelId, final long start, final long end) throws DataAccessException {
        return getSelectedArchives(Archive.class, channelId, start, end);
    }

    protected Collection<? extends Archive> getSelectedArchives(final Class clazz, final String channelId, final long start, final long end) throws DataAccessException {

        if (channelId == null || channelId.length()==0 || start>end) {
            return new TreeSet<Archive>();
        }

        final long startIndex = start<0 ? 0 : start;
        final long size = end<0 ? 1 : end-start+1;

        return (Collection<? extends Archive>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT ar ")//
                        .append(" FROM ").append(clazz.getCanonicalName()).append(" ar ")//
                        /*DB paging-only FIX*/
                        //.append("       LEFT JOIN FETCH ar.location.channel ")//
                        .append(" WHERE ")//
                        //.append("       ar.location.channel.label = :channelId ")//
                        /*DB paging-only FIX*/
                        .append("       ar.location.channel.label = :channelId ")//
                        .append(" ORDER BY ")//
                        .append("       ar.location.startTimestamp DESC ");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("channelId", channelId);
                query.setFirstResult(NumberUtil.safeLongToInt(startIndex));
                query.setMaxResults(NumberUtil.safeLongToInt(size));
                return query.list();
            }
        });

    }
        
    public Collection<Archive> getArchivesForChannel(final GroupAndSort<ARCHIVE_TAGS> constraint, final String channelId) throws DataAccessException {

        if (channelId == null) {
            return new HashSet<Archive>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ar ")
                        .append("FROM ").append(Archive.class.getCanonicalName()).append(" ar ")
                        //.append("     LEFT JOIN ar.location.channel ")
                        .append("WHERE ")
                        .append("     ar.location.channel.label = :channelId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<ARCHIVE_TAGS>() {

                    @Override
                    public String[] onTag(ARCHIVE_TAGS currentTag) {
                        switch (currentTag) {
                            case playlist:
                                return new String[]{"ar.playlist"};
                            case programme:
                                return new String[]{"ar.programmeCaption"};
                            case time:
                                return new String[]{"ar.location.startTimestamp"};
                            case duration:
                                return new String[]{"ar.duration"};
                            case type:
                                return new String[]{"ar.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("channelId", channelId);
                    return query.scroll();
                } else {
                    return new HashSet<Archive>();
                }
            }
        });

        return (Collection<Archive>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }
    
    public Collection<Report> getReportsForChannel(final GroupAndSort<REPORT_TAGS> constraint, final String channelId) throws DataAccessException {

        if (channelId == null) {
            return new HashSet<Report>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT r ")
                        .append("FROM ").append(Report.class.getCanonicalName()).append(" r ")
                        //.append("     LEFT JOIN ar.location.channel ")
                        .append("WHERE ")
                        .append("     r.location.channel.label = :channelId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<REPORT_TAGS>() {
                    @Override
                    public String[] onTag(REPORT_TAGS currentTag) {
                        switch (currentTag) {
                            case state:
                                return new String[]{"r.ready"};
                            case from:
                                return new String[]{"r.location.startTimestamp"};
                            case to:
                                return new String[]{"r.location.endTimestamp"};
                            case channel:
                                return new String[]{"r.location.channel.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("channelId", channelId);
                    return query.scroll();
                } else {
                    return new HashSet<Report>();
                }
            }
        });

        return (Collection<Report>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }
    
    public Collection<Archive> getArchivesWithRightsByChannelsForChannel(
            final Collection<String> channelIds,
            final GroupAndSort<ARCHIVE_TAGS> constraint,
            final String channelId) throws DataAccessException {
        if (channelId == null || channelIds == null || channelIds.isEmpty()) {
            return new HashSet<Archive>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!channelIds.contains(channelId)) {
            return new HashSet<Archive>();
        }

        return getArchivesForChannel(constraint, channelId);
    }
    
    public Collection<Report> getReportsWithRightsByChannelsForChannel(
            final Collection<String> channelIds,
            final GroupAndSort<REPORT_TAGS> constraint,
            final String channelId) throws DataAccessException {
        if (channelId == null || channelIds == null || channelIds.isEmpty()) {
            return new HashSet<Report>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!channelIds.contains(channelId)) {
            return new HashSet<Report>();
        }

        return getReportsForChannel(constraint, channelId);
    }
    
    public Collection<Archive> getArchivesWithRightsByChannels(
            final Collection<String> channelIds,
            final GroupAndSort<ARCHIVE_TAGS> constraint) throws DataAccessException {
        if (channelIds != null && channelIds.isEmpty()) {
            return new HashSet<Archive>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT ar ")
                        .append("FROM ").append(Archive.class.getCanonicalName()).append(" ar ")
                        ;//.append("     LEFT JOIN ar.location.channel ");
                if (channelIds != null) {
                    hqlCommand.append("WHERE ")//
                            .append("     ar.location.channel.label IN (:channelIds) ");//
                }
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<ARCHIVE_TAGS>() {

                    @Override
                    public String[] onTag(ARCHIVE_TAGS currentTag) {
                        switch (currentTag) {
                            case playlist:
                                return new String[]{"ar.playlist"};
                            case programme:
                                return new String[]{"ar.programmeCaption"};
                            case time:
                                return new String[]{"ar.location.startTimestamp"};
                            case duration:
                                return new String[]{"ar.duration"};
                            case type:
                                return new String[]{"ar.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (channelIds != null) {
                        query.setParameterList("channelIds", channelIds);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Archive>();
                }
            }
        });

        return (Collection<Archive>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Report> getReportsWithRightsByChannels(
            final Collection<String> channelIds,
            final GroupAndSort<REPORT_TAGS> constraint) throws DataAccessException {
        if (channelIds != null && channelIds.isEmpty()) {
            return new HashSet<Report>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT r ")
                        .append("FROM ").append(Report.class.getCanonicalName()).append(" r ")
                        ;//.append("     LEFT JOIN ar.location.channel ");
                if (channelIds != null) {
                    hqlCommand.append("WHERE ")//
                            .append("     r.location.channel.label IN (:channelIds) ");//
                }
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<REPORT_TAGS>() {

                    @Override
                    public String[] onTag(REPORT_TAGS currentTag) {
                        switch (currentTag) {
                            case state:
                                return new String[]{"r.ready"};
                            case from:
                                return new String[]{"r.location.startTimestamp"};
                            case to:
                                return new String[]{"r.location.endTimestamp"};
                            case channel:
                                return new String[]{"r.location.channel.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (channelIds != null) {
                        query.setParameterList("channelIds", channelIds);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Report>();
                }
            }
        });

        return (Collection<Report>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }    
    
    public Collection<Archive> getAllArchives(final GroupAndSort<ARCHIVE_TAGS> constraint) throws DataAccessException {
        return getArchivesWithRightsByChannels(null, constraint);
    }
    
    public Collection<Report> getAllReports(final GroupAndSort<REPORT_TAGS> constraint) throws DataAccessException {
        return getReportsWithRightsByChannels(null, constraint);
    }
    
    public Collection<Archive> getAllArchivesBeforeTimeThreshold(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        Collection<Archive> list = null;
        if (expirationDate != null) {
            list = (Collection<Archive>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT ar ")//
                            .append("FROM ").append(Archive.class.getCanonicalName()).append(" ar ")//
                            .append("WHERE ")//
                            .append("     ar.creationDate < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    //query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    query.setString("expiryTimestamp", expirationDate.toSQLDate());
                    return query.list();
                }
            });
        }
        return (list == null) ? new HashSet<Archive>() : list;
    }

    public Collection<Report> getAllReportsBeforeTimeThreshold(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        Collection<Report> list = null;
        if (expirationDate != null) {
            list = (Collection<Report>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT r ")//
                            .append("FROM ").append(Report.class.getCanonicalName()).append(" r ")//
                            .append("WHERE ")//
                            .append("     r.creationDate < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("expiryTimestamp", expirationDate.toSQLDate());
                    return query.list();
                }
            });
        }
        return (list == null) ? new HashSet<Report>() : list;
    }
    
    public Collection<Archive> getArchivesForChannel(final String channelId, final YearMonthWeekDayHourMinuteSecondMillisecondInterface fromDate, final YearMonthWeekDayHourMinuteSecondMillisecondInterface toDate) throws Exception {
        Collection<Archive> list = null;
        if (fromDate != null && toDate!=null) {
            list = (Collection<Archive>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT ar ")//
                            .append("FROM ").append(Archive.class.getCanonicalName()).append(" ar ")//
                            .append("WHERE ")//
                            .append("     ar.location.channel = :channelId ")//
                            .append("   AND ")//
                            .append("     ar.location.startTimestamp BETWEEN :fromTimestamp AND :toTimestamp ")//
                            .append("ORDER BY ar.location.startTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("channelId", channelId);
                    query.setLong("fromTimestamp", fromDate.getUnixTime());
                    query.setLong("toTimestamp", toDate.getUnixTime());
                    return query.list();
                }
            });
        }
        return (list == null) ? new HashSet<Archive>() : list;
    }    
    
    public Long getNumberOfRequestsForUserAfterTimeThreshold(final String channelId, final String restrictedUserId, final YearMonthWeekDayHourMinuteSecondMillisecondInterface timeThreshold) {
        if (channelId==null || restrictedUserId==null || timeThreshold==null) {
            return null;
        }

        Long numOfRequests = (Long) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        //DO NOT use "fetch" if the joins are not developed in the SELECT clause, like it is the case with COUNT
                        .append("SELECT COUNT(ar) ")//
                        .append("FROM ").append(RequestArchive.class.getCanonicalName()).append(" ar ")//
                        .append("WHERE ")//
                        .append("       ar.requester = :requesterId ")//
                        .append("   AND ")//
                        .append("       ar.location.channel.label = :channelId ")//
                        .append("   AND ")//
                        .append("       ar.creationDate > :pastDate ");//

                return session.createQuery(hqlCommand.toString())//
                    .setString("requesterId", restrictedUserId)//
                    .setString("channelId", channelId)//
                    .setString("pastDate", timeThreshold.toSQLDate())
                    .uniqueResult();

            }

        });

        return numOfRequests;
    }

    public void purgeArchives(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        if (expirationDate != null) {
            getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("DELETE ").append(Archive.class.getCanonicalName()).append(" ar ")//
                            .append("WHERE ")//
                            //.append("     ar.location.startTimestamp < :expiryTimestamp ");
                            .append("     ar.creationDate < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    //query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    query.setString("expiryTimestamp", expirationDate.toSQLDate());
                    return query.executeUpdate();
                }
            });
        }
    }

    public void purgeReports(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        if (expirationDate != null) {
            getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("DELETE ").append(Report.class.getCanonicalName()).append(" r ")//
                            .append("WHERE ")//
                            .append("     r.creationDate < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("expiryTimestamp", expirationDate.toSQLDate());
                    return query.executeUpdate();
                }
            });
        }
    }
    
}
