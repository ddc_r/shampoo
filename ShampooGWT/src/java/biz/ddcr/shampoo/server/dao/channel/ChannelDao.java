package biz.ddcr.shampoo.server.dao.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.queue.QueueForm.QUEUE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.timetable.*;
import biz.ddcr.shampoo.server.domain.webservice.Hit;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.helper.*;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;
/**
 *
 * @author okay_awright
 **/
public class ChannelDao extends GenericHibernateDao {
    
    /** dummy class for passing final objects**/
    private class IsSpanning {
        public boolean week = false;
        public boolean month = false;
        public boolean year = false;
    }

    public void addChannel(Channel channel) throws DataAccessException {
        if (channel != null) {
            getTemplate().save(channel);
        }
    }

    public void addTimetableSlot(TimetableSlot timetableSlot) throws DataAccessException {
        if (timetableSlot != null) {
            getTemplate().save(timetableSlot);
        }
    }

    public void addQueueItem(QueueItem queueItem) throws DataAccessException {
        if (queueItem != null) {
            getTemplate().save(queueItem);
        }
    }

    public void addStreamItem(StreamItem streamItem) throws DataAccessException {
        if (streamItem != null) {
            getTemplate().save(streamItem);
        }
    }

    public void addWebservice(Webservice webservice) throws DataAccessException {
        if (webservice != null) {
            getTemplate().save(webservice);
        }
    }

    public void addHit(Hit hit) throws DataAccessException {
        if (hit != null) {
            getTemplate().save(hit);
        }
    }

    public void addBatchTimetableSlots(Collection<TimetableSlot> timetableSlots) throws DataAccessException {
        if (timetableSlots != null) {
            this.batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    timetableSlots);
        }
    }

    public void addBatchQueueItems(Collection<QueueItem> queueItems) throws DataAccessException {
        if (queueItems != null) {
            this.batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    queueItems);
        }
    }

    public void addBatchStreamItems(Collection<StreamItem> streamItems) throws DataAccessException {
        if (streamItems != null) {
            this.batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    streamItems);
        }
    }

    public void addBatchWebservices(Collection<Webservice> webservices) throws DataAccessException {
        if (webservices != null) {
            this.batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    webservices);
        }
    }

    public void updateChannel(Channel channel) throws DataAccessException {
        if (channel != null) {
            getTemplate().update(channel);
        }
    }

    public void updateBatchChannels(Collection<Channel> channels) throws DataAccessException {
        if (channels != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    channels);
        }
    }

    public void updateTimetableSlot(TimetableSlot timetableSlot) throws DataAccessException {
        if (timetableSlot != null) {
            getTemplate().update(timetableSlot);
        }
    }

    public void updateQueueItem(QueueItem queueItem) throws DataAccessException {
        if (queueItem != null) {
            getTemplate().update(queueItem);
        }
    }

    public void updateStreamItem(StreamItem streamItem) throws DataAccessException {
        if (streamItem != null) {
            getTemplate().update(streamItem);
        }
    }

    public void updateBatchTimetableSlots(Collection<TimetableSlot> timetableSlots) throws DataAccessException {
        if (timetableSlots != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    timetableSlots);
        }
    }

    public void updateBatchQueueItems(Collection<QueueItem> queueItems) throws DataAccessException {
        if (queueItems != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    queueItems);
        }
    }

    public void updateBatchStreamItems(Collection<StreamItem> streamItems) throws DataAccessException {
        if (streamItems != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    streamItems);
        }
    }

    public void updateWebservice(Webservice webservice) throws DataAccessException {
        if (webservice != null) {
            getTemplate().update(webservice);
        }
    }

    public void updateBatchWebservices(Collection<Webservice> webservices) throws DataAccessException {
        if (webservices != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    webservices);
        }
    }

    public Channel getChannel(final String id) throws DataAccessException {
        return getTemplate().get(Channel.class, id);
    }

    public Collection<Channel> getChannels(Collection<String> ids) throws DataAccessException {
        Collection<Channel> channels = null;
        if (ids != null) {
            channels = new HashSet<Channel>();
            for (String id : ids) {
                Channel channel = getTemplate().get(Channel.class, id);
                if (channel!=null)
                    channels.add(channel);
            }
        }
        return channels;
    }

    public Channel loadChannel(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Channel) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()
                            .append("SELECT ra ")//
                            .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")//
                            .append("     LEFT JOIN FETCH ra.programmes ")//
                            .append("     LEFT JOIN FETCH ra.channelAdministratorRights ")//
                            .append("     LEFT JOIN FETCH ra.programmeManagerRights ")//
                            .append("     LEFT JOIN FETCH ra.listenerRights ")//
                            .append("WHERE ")//
                            .append("     ra.label = :id ");//

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    public Collection<Channel> loadChannels(final Collection<String> ids) throws DataAccessException {
        if (ids == null || ids.isEmpty()) {
            return new HashSet<Channel>();
        } else {
            Collection<Channel> list = (Collection<Channel>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT ra ")//
                            .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")//
                            .append("     LEFT JOIN FETCH ra.programmes ")//
                            .append("     LEFT JOIN FETCH ra.channelAdministratorRights ")//
                            .append("     LEFT JOIN FETCH ra.programmeManagerRights ")//
                            .append("     LEFT JOIN FETCH ra.listenerRights ")//
                            .append("WHERE ")//
                            .append("     ra.label IN (:ids) ");
                    Query query = session.createQuery(hqlCommand.toString());
                    query.setParameterList("ids", ids);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<Channel>() : list;
        }
    }
    
    public String getChannelTimezone(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (String) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()
                            .append("SELECT ra.timezone ")//
                            .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")//
                            .append("WHERE ")//
                            .append("     ra.label = :id ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    public TimetableSlot getTimetableSlot(String id) throws DataAccessException {
        return getTemplate().get(
    TimetableSlot.class,
    id);
    }

    public Collection<TimetableSlot> getTimetableSlots(Collection<String> ids) throws DataAccessException {
        Collection<TimetableSlot> timetableSlots = null;
        if (ids != null) {
            timetableSlots = new HashSet<TimetableSlot>();
            for (String id : ids) {
                TimetableSlot timetableSlot = getTemplate().get(TimetableSlot.class, id);
                if (timetableSlot!=null)
                    timetableSlots.add(timetableSlot);
            }
        }
        return timetableSlots;
    }

    public QueueItem getQueueItem(String id) throws DataAccessException {
        return getTemplate().get(
        QueueItem.class,
        id);
    }

    public StreamItem getStreamItem(String id) throws DataAccessException {
        return getTemplate().get(
       StreamItem.class,
       id);
    }

    /*public TimetableSlot getTimetableSlot(TimetableSlotLocation location) throws DataAccessException {
        return (TimetableSlot) getTemplate().get(
                    TimetableSlot.class,
                    location);
    }*/
    
    public TimetableSlot getTimetableSlot(final String channelId, final YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws DataAccessException {

        return (TimetableSlot) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                String hqlCommand =
                        "SELECT DISTINCT tt "
                        + "FROM " + TimetableSlot.class.getCanonicalName() + " tt "
                        + "WHERE "
                        + "     tt.location.channel.label = :channelId "
                        + "     AND tt.location.startTimestamp = :startDate ";
                Query query = session.createQuery(hqlCommand);
                query.setString("channelId", channelId);
                query.setLong("startDate", startTime.getUnixTime());
                //query.setMaxResults(1);
                return query.uniqueResult();
            }
        });

    }

    public TimetableSlot loadTimetableSlot(final String timetableSlotId) throws DataAccessException {

        return (TimetableSlot) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                String hqlCommand =
                        "SELECT DISTINCT tt "
                        + "FROM " + TimetableSlot.class.getCanonicalName() + " tt "
                        + "     LEFT JOIN FETCH tt.location.channel "
                        + "     LEFT JOIN FETCH tt.playlist "
                        + "     LEFT JOIN FETCH tt.programme "
                        + "WHERE "
                        + "     tt.refID = :timetableSlotId ";
                Query query = session.createQuery(hqlCommand);
                query.setString("timetableSlotId", timetableSlotId);
                return query.uniqueResult();
            }
        });

    }
    
    public Webservice getWebservice(final String id) throws DataAccessException {
        return getTemplate().get(Webservice.class, id);
    }

    public Collection<Webservice> getWebservices(Collection<String> ids) throws DataAccessException {
        Collection<Webservice> webservices = null;
        if (ids != null) {
            webservices = new HashSet<Webservice>();
            for (String id : ids) {
                Webservice webservice = getTemplate().get(Webservice.class, id);
                if (webservice!=null)
                    webservices.add(webservice);
            }
        }
        return webservices;
    }

    public Webservice loadWebservice(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Webservice) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()
                            .append("SELECT ws ")//
                            .append("FROM ").append(Webservice.class.getCanonicalName()).append(" ws ")//
                            .append("     LEFT JOIN FETCH ws.channel ")//
                            .append("WHERE ")//
                            .append("     ws.apiKey = :id ");//

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    /** Intervals are inclusive **/
    public Collection<TimetableSlot> getEnabledTimetableSlotsForChannelStartingInInterval(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface from, final YearMonthWeekDayHourMinuteSecondMillisecondInterface to) throws DataAccessException {

        if (channelId == null || from == null || to == null || from.compareTo(to)>0) {
            return new HashSet<TimetableSlot>();
        }

        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedFrom = from.switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTo = to.switchTimeZone(tz);

        /*final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyCurrentDayStart = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonthWeekDay(from))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyPreviousDayStart = new YearMonthWeekDayHourMinuteSecondMillisecond(fuzzyCurrentDayStart);
        fuzzyPreviousDayStart.addDays(-1);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyCurrentWeekStart = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonthWeek(from))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyPreviousWeekStart = new YearMonthWeekDayHourMinuteSecondMillisecond(fuzzyCurrentWeekStart);
        fuzzyPreviousWeekStart.addWeeks(-1);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyCurrentMonthStart = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonth(from))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyPreviousMonthStart = new YearMonthWeekDayHourMinuteSecondMillisecond(fuzzyCurrentMonthStart);
        fuzzyPreviousMonthStart.addMonths(-1);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyCurrentYearStart = (new YearMonthWeekDayHourMinuteSecondMillisecond(new Year(from))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fuzzyPreviousYearStart = new YearMonthWeekDayHourMinuteSecondMillisecond(fuzzyCurrentYearStart);
        fuzzyPreviousYearStart.addYears(-1);*/
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface t = adjustedFrom;
        final YearMonthWeekDay fuzzyCurrentDayStart = new YearMonthWeekDay(t);
        final YearMonthWeekDay fuzzyPreviousDayStart = (YearMonthWeekDay)new YearMonthWeekDayHourMinuteSecondMillisecond(fuzzyCurrentDayStart);
        fuzzyPreviousDayStart.addDays(-1);
        final YearMonthWeek fuzzyCurrentWeekStart = new YearMonthWeek(t);
        final YearMonthWeek fuzzyPreviousWeekStart = new YearMonthWeek(fuzzyCurrentWeekStart);
        fuzzyPreviousWeekStart.addWeeks(-1);
        final YearMonth fuzzyCurrentMonthStart =  new YearMonth(t);
        final YearMonth fuzzyPreviousMonthStart = new YearMonth(fuzzyCurrentMonthStart);
        fuzzyPreviousMonthStart.addMonths(-1);
        final Year fuzzyCurrentYearStart = new Year(t);
        final Year fuzzyPreviousYearStart = new Year(fuzzyCurrentYearStart);
        fuzzyPreviousYearStart.addYears(-1);

        final long fromMillisecondInDay = adjustedFrom.getMillisecondInDayPeriod();
        final long fromMillisecondInWeek = adjustedFrom.getMillisecondInWeekPeriod();
        final long fromMillisecondInMonth = adjustedFrom.getMillisecondInMonthPeriod();
        final long fromMillisecondInYear = adjustedFrom.getMillisecondInYearPeriod();
        final long toMillisecondInDay = adjustedTo.getMillisecondInDayPeriod();
        final long toMillisecondInWeek = adjustedTo.getMillisecondInWeekPeriod();
        final long toMillisecondInMonth = adjustedTo.getMillisecondInMonthPeriod();
        final long toMillisecondInYear = adjustedTo.getMillisecondInYearPeriod();

        Collection<TimetableSlot> list = (Collection<TimetableSlot>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(TimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*.append("     LEFT JOIN tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ")
                        .append("     tt.location.channel.label = :channelId ")
                        //Do only bother retrieving enabled slots
                        .append("     AND tt.enabled = TRUE ")
                        .append("     AND tt.playlist IS NOT NULL ")
                        .append("     AND ( ")
                        .append("             ( ") //Unique timetable slot
                        .append("               tt.class = ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("               AND ( tt.location.startTimestamp >= :fromTime AND tt.location.startTimestamp < :toTime ) ")
                        .append("             ) OR ( ") //Recurring time slot)
                        .append("               tt.class != ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" ")//
                        //      Commissioning date
                        .append("               AND tt.location.startTimestamp < :toTime ")//
                        .append("               AND (")//
                        .append("                   ( ") //          Daily timetable slot
                        .append("                      tt.class = ").append(DailyTimetableSlot.class.getCanonicalName()).append(" ")//

                        //FIX: This bit "? < ?" is not allowed with Derby, hardcode it then
                        /*.append("                      AND (")
                        .append("                               (") //If the interval doesn't span on two days
                        .append("                                   :fromMillisecondInDay < :toMillisecondInDay ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :toMillisecondInDay ")
                        .append("                                   AND tt.startMillisecondFromPeriod >= :fromMillisecondInDay ")
                        .append("                               ) OR (") //If the interval spans on more than one day (always less than 24h in duration, it's mandatory)
                        .append("                                   :fromMillisecondInDay > :toMillisecondInDay ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :toMillisecondInDay ")
                        .append("                                         OR tt.startMillisecondFromPeriod >= :fromMillisecondInDay ) ")
                        .append("                               )")
                        .append("                      )")*/                        
                        ;
                        if (fromMillisecondInDay<toMillisecondInDay || fromMillisecondInDay>toMillisecondInDay) {
                            hqlCommand.append("          AND (");
                            
                            if (fromMillisecondInDay<toMillisecondInDay) {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod < :toMillisecondInDay ")//
                                .append("                            AND tt.startMillisecondFromPeriod >= :fromMillisecondInDay ")//
                                ;
                            } else {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod < :toMillisecondInDay ")//
                                .append("                            OR tt.startMillisecondFromPeriod >= :fromMillisecondInDay ")//
                                ;
                            }                            
                            
                            hqlCommand.append("          )");
                        }
                        hqlCommand

                        .append("                      AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                           OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentDayStart ) ) ")//
                        .append("                           OR ( tt.startMillisecondFromPeriod > tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousDayStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousDayStart + tt.startMillisecondFromPeriod ) >= :fromTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousDayStart ) ) ")//
                        .append("                      ) ")//
                        .append("                   ) ")
                        .append("                   OR ( ") //          Weekly timetable slot
                        .append("                      tt.class = ").append(WeeklyTimetableSlot.class.getCanonicalName()).append(" ")

                        //FIX: This bit "? < ?" is not allowed with Derby, hardcode it then
                        /*.append("                      AND (")
                        .append("                               (") //If the interval doesn't span on two weeks
                        .append("                                   :fromMillisecondInWeek < :toMillisecondInWeek ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :toMillisecondInWeek ")
                        .append("                                   AND tt.startMillisecondFromPeriod >= :fromMillisecondInWeek ")
                        .append("                               ) OR (") //If the interval spans on more than one week (always less than 24h*7 in duration, it's mandatory)
                        .append("                                   :fromMillisecondInWeek > :toMillisecondInWeek ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :toMillisecondInWeek ")
                        .append("                                         OR tt.startMillisecondFromPeriod >= :fromMillisecondInWeek ) ")
                        .append("                               )")                        
                        .append("                      )")*/
                        ;
                        if (fromMillisecondInWeek<toMillisecondInWeek || fromMillisecondInWeek>toMillisecondInWeek) {
                            hqlCommand.append("          AND (");

                            if (fromMillisecondInWeek<toMillisecondInWeek) {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod <= :toMillisecondInWeek ")//
                                .append("                            AND tt.startMillisecondFromPeriod >= :fromMillisecondInWeek ")//
                                ;
                            } else {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod <= :toMillisecondInWeek ")//
                                .append("                            OR tt.startMillisecondFromPeriod >= :fromMillisecondInWeek ")//
                                ;
                            }

                            hqlCommand.append("          )");
                        }
                        hqlCommand

                        .append("                      AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                           OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentWeekStart ) ) ")//
                        .append("                           OR ( tt.startMillisecondFromPeriod > tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousWeekStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousWeekStart + tt.startMillisecondFromPeriod ) >= :fromTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousWeekStart ) ) ")//
                        .append("                      ) ")//                        
                        .append("                   ) ")
                        .append("                   OR ( ") //          Monthly timetable slot
                        .append("                      tt.class = ").append(MonthlyTimetableSlot.class.getCanonicalName()).append(" ")

                        //FIX: This bit "? < ?" is not allowed with Derby, hardcode it then
                        /*.append("                      AND (")
                        .append("                               (") //If the interval doesn't span on two months
                        .append("                                   :fromMillisecondInMonth < :toMillisecondInMonth ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :toMillisecondInMonth ")
                        .append("                                   AND tt.startMillisecondFromPeriod >= :fromMillisecondInMonth ")
                        .append("                               ) OR (") //If the interval spans on more than one month (always less than 24h*7*(28/29/30/31) in duration, it's mandatory)
                        .append("                                   :fromMillisecondInWeek > :toMillisecondInWeek ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :toMillisecondInWeek ")
                        .append("                                         OR tt.startMillisecondFromPeriod >= :fromMillisecondInWeek ) ")
                        .append("                               )")                         
                        .append("                      )")*/
                        ;
                        if (fromMillisecondInMonth<toMillisecondInMonth || fromMillisecondInMonth>toMillisecondInMonth) {
                            hqlCommand.append("          AND (");

                            if (fromMillisecondInMonth<toMillisecondInMonth) {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod <= :toMillisecondInMonth ")//
                                .append("                            AND tt.startMillisecondFromPeriod >= :fromMillisecondInMonth ")//
                                ;
                            } else {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod <= :toMillisecondInMonth ")//
                                .append("                            OR tt.startMillisecondFromPeriod >= :fromMillisecondInMonth ")//
                                ;
                            }

                            hqlCommand.append("          )");
                        }
                        hqlCommand

                        .append("                      AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                           OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentMonthStart ) ) ")//
                        .append("                           OR ( tt.startMillisecondFromPeriod > tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousMonthStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousMonthStart + tt.startMillisecondFromPeriod ) >= :fromTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousMonthStart ) ) ")//
                        .append("                      ) ")//                        
                        .append("                   ) ") //          Yearly timetable slot
                        .append("                   OR ( ")
                        .append("                      tt.class = ").append(YearlyTimetableSlot.class.getCanonicalName()).append(" ")

                        //FIX: This bit "? < ?" is not allowed with Derby, hardcode it then
                        /*.append("                      AND (")
                        .append("                               (") //If the interval doesn't span on two years
                        .append("                                   :fromMillisecondInYear < :toMillisecondInYear ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :toMillisecondInYear ")
                        .append("                                   AND tt.startMillisecondFromPeriod >= :fromMillisecondInYear ")
                        .append("                               ) OR (") //If the interval spans on more than one year (always less than 24h*7*(28/29/30/31)*365 in duration, it's mandatory)
                        .append("                                   :fromMillisecondInWeek > :toMillisecondInYear ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :toMillisecondInYear ")
                        .append("                                         OR tt.startMillisecondFromPeriod >= :fromMillisecondInYear ) ")
                        .append("                               )")                                                 
                        .append("                      )")*/
                        ;
                        if (fromMillisecondInYear<toMillisecondInYear || fromMillisecondInWeek>toMillisecondInYear) {
                            hqlCommand.append("          AND (");

                            if (fromMillisecondInYear<toMillisecondInYear) {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod <= :toMillisecondInYear ")//
                                .append("                            AND tt.startMillisecondFromPeriod >= :fromMillisecondInYear ")//
                                ;
                            } else {
                                hqlCommand//
                                .append("                            tt.startMillisecondFromPeriod <= :toMillisecondInYear ")//
                                .append("                            OR tt.startMillisecondFromPeriod >= :fromMillisecondInYear ")//
                                ;
                            }

                            hqlCommand.append("          )");
                        }
                        hqlCommand

                        .append("                      AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                           OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentYearStart ) ) ")//
                        .append("                           OR ( tt.startMillisecondFromPeriod > tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousYearStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousYearStart + tt.startMillisecondFromPeriod ) >= :fromTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousYearStart ) ) ")//
                        .append("                      ) ")//                        
                        .append("                   ) ")
                        .append("               ) ")
                        .append("             ) ")
                        .append("     ) ");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("channelId", channelId);
                query.setLong("fromMillisecondInDay", fromMillisecondInDay);
                query.setLong("fromMillisecondInWeek", fromMillisecondInWeek);
                query.setLong("fromMillisecondInMonth", fromMillisecondInMonth);
                query.setLong("fromMillisecondInYear", fromMillisecondInYear);
                query.setLong("toMillisecondInDay", toMillisecondInDay);
                query.setLong("toMillisecondInWeek", toMillisecondInWeek);
                query.setLong("toMillisecondInMonth", toMillisecondInMonth);
                query.setLong("toMillisecondInYear", toMillisecondInYear);
                query.setLong("fromTime", adjustedFrom.getUnixTime());
                query.setLong("toTime", adjustedTo.getUnixTime());
                query.setLong("fuzzyCurrentDayStart", fuzzyCurrentDayStart.getUnixTime());
                query.setLong("fuzzyPreviousDayStart", fuzzyPreviousDayStart.getUnixTime());
                query.setLong("fuzzyCurrentWeekStart", fuzzyCurrentWeekStart.getUnixTime());
                query.setLong("fuzzyPreviousWeekStart", fuzzyPreviousWeekStart.getUnixTime());
                query.setLong("fuzzyCurrentMonthStart", fuzzyCurrentMonthStart.getUnixTime());
                query.setLong("fuzzyPreviousMonthStart", fuzzyPreviousMonthStart.getUnixTime());
                query.setLong("fuzzyCurrentYearStart", fuzzyCurrentYearStart.getUnixTime());
                query.setLong("fuzzyPreviousYearStart", fuzzyPreviousYearStart.getUnixTime());                                

                return query.list();
            }
        });
        return (list == null) ? new HashSet<TimetableSlot>() : list;

    }
    
    public Collection<TimetableSlot> getEnabledTimetableSlotsForChannelFuzzyAt(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws DataAccessException {

        if (channelId == null || time == null) {
            return new HashSet<TimetableSlot>();
        }

        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTime = time.switchTimeZone(tz);

        final YearMonthWeekDayHourMinuteSecondMillisecondInterface t = adjustedTime;
        final YearMonthWeekDay fuzzyCurrentDayStart = new YearMonthWeekDay(t);
        final YearMonthWeekDay fuzzyPreviousDayStart = (YearMonthWeekDay)new YearMonthWeekDayHourMinuteSecondMillisecond(fuzzyCurrentDayStart);
        fuzzyPreviousDayStart.addDays(-1);
        final YearMonthWeek fuzzyCurrentWeekStart = new YearMonthWeek(t);
        final YearMonthWeek fuzzyPreviousWeekStart = new YearMonthWeek(fuzzyCurrentWeekStart);
        fuzzyPreviousWeekStart.addWeeks(-1);
        final YearMonth fuzzyCurrentMonthStart =  new YearMonth(t);
        final YearMonth fuzzyPreviousMonthStart = new YearMonth(fuzzyCurrentMonthStart);
        fuzzyPreviousMonthStart.addMonths(-1);
        final Year fuzzyCurrentYearStart = new Year(t);
        final Year fuzzyPreviousYearStart = new Year(fuzzyCurrentYearStart);
        fuzzyPreviousYearStart.addYears(-1);        
        
        Collection<TimetableSlot> list = (Collection<TimetableSlot>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(TimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*.append("     LEFT JOIN tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ")
                        .append("     tt.location.channel.label = :channelId ")
                        //Do only bother retrieving enabled slots
                        .append("     AND tt.enabled = TRUE ")
                        .append("     AND tt.playlist IS NOT NULL ")
                        .append("     AND ( ")
                        .append("             ( ") //Unique timetable slot
                        .append("               tt.class = ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("               AND tt.location.startTimestamp <= :fuzzyTime ")
                        .append("               AND tt.endTimestamp > :fuzzyTime ")
                        .append("             ) OR ( ") //Recurring time slot)
                        .append("               tt.class != ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" ") //      Commissioning date
                        .append("               AND tt.location.startTimestamp <= :fuzzyTime ") //      Decommissioning date
                        .append("               AND (")
                        .append("                   ( ") //          Daily timetable slot
                        .append("                      tt.class = ").append(DailyTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("                      AND (")
                        .append("                               (") //If the slot doesn't span on two days
                        .append("                                   tt.startMillisecondFromPeriod < tt.endMillisecondFromPeriod ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :fuzzyMillisecondInDay ")
                        .append("                                   AND tt.endMillisecondFromPeriod > :fuzzyMillisecondInDay ")
                        .append("                               ) OR (") //If the slot spans on more than one day (always less than 24h in duration, it's mandatory)
                        .append("                                   tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :fuzzyMillisecondInDay ")
                        .append("                                         OR tt.endMillisecondFromPeriod > :fuzzyMillisecondInDay ) ")
                        .append("                               )")
                        .append("                               AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                                   OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentDayStart ) ) ")//
                        .append("                                   OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousDayStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousDayStart + tt.startMillisecondFromPeriod ) >= :fuzzyTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousDayStart ) ) ")//
                        .append("                               ) ")//                        
                        .append("                      )")
                        .append("                   ) ")
                        .append("                   OR ( ") //          Weekly timetable slot
                        .append("                      tt.class = ").append(WeeklyTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("                      AND (")
                        .append("                               (") //If the slot doesn't span on two days
                        .append("                                   tt.startMillisecondFromPeriod < tt.endMillisecondFromPeriod ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :fuzzyMillisecondInWeek ")
                        .append("                                   AND tt.endMillisecondFromPeriod > :fuzzyMillisecondInWeek ")
                        .append("                               ) OR (") //If the slot spans on more than one day (always less than a week in duration, it's mandatory)
                        .append("                                   tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :fuzzyMillisecondInWeek ")
                        .append("                                         OR tt.endMillisecondFromPeriod > :fuzzyMillisecondInWeek ) ")
                        .append("                               )")
                        .append("                               AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                                   OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentWeekStart ) ) ")//
                        .append("                                   OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousWeekStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousWeekStart + tt.startMillisecondFromPeriod ) >= :fuzzyTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousWeekStart ) ) ")//
                        .append("                               ) ")//                        
                        .append("                      )")
                        .append("                   ) ")
                        .append("                   OR ( ") //          Monthly timetable slot
                        .append("                      tt.class = ").append(MonthlyTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("                      AND (")
                        .append("                               (") //If the slot doesn't span on two days
                        .append("                                   tt.startMillisecondFromPeriod < tt.endMillisecondFromPeriod ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :fuzzyMillisecondInMonth ")
                        .append("                                   AND tt.endMillisecondFromPeriod > :fuzzyMillisecondInMonth ")
                        .append("                               ) OR (") //If the slot spans on more than one day (always less than a month in duration, it's mandatory)
                        .append("                                   tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :fuzzyMillisecondInMonth ")
                        .append("                                         OR tt.endMillisecondFromPeriod > :fuzzyMillisecondInMonth ) ")
                        .append("                               )")
                        .append("                               AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                                   OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentMonthStart ) ) ")//
                        .append("                                   OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousMonthStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousMonthStart + tt.startMillisecondFromPeriod ) >= :fuzzyTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousMonthStart ) ) ")//
                        .append("                               ) ")//                        
                        .append("                      )")
                        .append("                   ) ") //          Yearly timetable slot
                        .append("                   OR ( ")
                        .append("                      tt.class = ").append(YearlyTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("                      AND (")
                        .append("                               (") //If the slot doesn't span on two days
                        .append("                                   tt.startMillisecondFromPeriod < tt.endMillisecondFromPeriod ")
                        .append("                                   AND tt.startMillisecondFromPeriod <= :fuzzyMillisecondInYear ")
                        .append("                                   AND tt.endMillisecondFromPeriod > :fuzzyMillisecondInYear ")
                        .append("                               ) OR (") //If the slot spans on more than one day (always less than a year in duration, it's mandatory)
                        .append("                                   tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod ")
                        .append("                                   AND ( tt.startMillisecondFromPeriod <= :fuzzyMillisecondInYear ")
                        .append("                                         OR tt.endMillisecondFromPeriod > :fuzzyMillisecondInYear ) ")
                        .append("                               )")
                        .append("                               AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                                   OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentYearStart ) ) ")//
                        .append("                                   OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousYearStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousYearStart + tt.startMillisecondFromPeriod ) >= :fuzzyTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousYearStart ) ) ")//
                        .append("                               ) ")//                        
                        .append("                      )")
                        .append("                   ) ")
                        .append("               ) ")
                        .append("             ) ")
                        .append("     ) ");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("channelId", channelId);
                query.setLong("fuzzyMillisecondInDay", adjustedTime.getMillisecondInDayPeriod());
                query.setLong("fuzzyMillisecondInWeek", adjustedTime.getMillisecondInWeekPeriod());
                query.setLong("fuzzyMillisecondInMonth", adjustedTime.getMillisecondInMonthPeriod());
                query.setLong("fuzzyMillisecondInYear", adjustedTime.getMillisecondInYearPeriod());
                query.setLong("fuzzyTime", adjustedTime.getUnixTime());
                query.setLong("fuzzyCurrentDayStart", fuzzyCurrentDayStart.getUnixTime());
                query.setLong("fuzzyPreviousDayStart", fuzzyPreviousDayStart.getUnixTime());
                query.setLong("fuzzyCurrentWeekStart", fuzzyCurrentWeekStart.getUnixTime());
                query.setLong("fuzzyPreviousWeekStart", fuzzyPreviousWeekStart.getUnixTime());
                query.setLong("fuzzyCurrentMonthStart", fuzzyCurrentMonthStart.getUnixTime());
                query.setLong("fuzzyPreviousMonthStart", fuzzyPreviousMonthStart.getUnixTime());
                query.setLong("fuzzyCurrentYearStart", fuzzyCurrentYearStart.getUnixTime());
                query.setLong("fuzzyPreviousYearStart", fuzzyPreviousYearStart.getUnixTime());                

                return query.list();
            }
        });
        return (list == null) ? new HashSet<TimetableSlot>() : list;

    }

    public UniqueTimetableSlot getNextEnabledUniqueTimetableSlotForChannelFuzzyFrom(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws DataAccessException {

        if (channelId == null || time == null) {
            return null;
        }

        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTime = time.switchTimeZone(tz);

        Collection<UniqueTimetableSlot> slots = (Collection<UniqueTimetableSlot>)getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*DB paging-only FIX*//*.append("     LEFT JOIN FETCH tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ")
                        //.append("     tt.location.channel.label = :channelId ")
                        /*DB paging-only FIX*/.append("     tt.location.channel.label = :channelId ")
                        //Do only bother retrieving enabled slots
                        .append("     AND tt.enabled = TRUE ")
                        .append("     AND tt.playlist IS NOT NULL ")
                        .append("     AND tt.location.startTimestamp > :fuzzyTime ")
                        .append("ORDER BY tt.location.startTimestamp ASC ");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("channelId", channelId);
                query.setLong("fuzzyTime", adjustedTime.getUnixTime());
                //Unique result
                query.setFirstResult(0);
                query.setMaxResults(1);
                /*DB paging-only FIX*///Hibernate is sometimes insane; it cannot add the proper LIMIT SQL statemennt in such a query: it will first get everything in MEMORY(!) and then filter out, madness!
                return query.list();
            }
        });
        if (slots!=null && !slots.isEmpty()) {
            Iterator<UniqueTimetableSlot> i = slots.iterator();
            //It doesn't matter if the set is not sorted, setFirstResult() and setMaxResults() will do the job
            if (i.hasNext()) return i.next();
        }
        return null;

    }

    protected TimetableSlot getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(final Class<? extends TimetableSlot> clazz, final String channelId, final long absoluteStartTime, final long currentPeriodStart, final long previousPeriodStart) throws DataAccessException {
        return getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(clazz, channelId, absoluteStartTime, null, currentPeriodStart, previousPeriodStart);
    }
    protected TimetableSlot getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(final Class<? extends TimetableSlot> clazz, final String channelId, final long absoluteStartTime, final Long startMillisecondInPeriod, final long currentPeriodStart, final long previousPeriodStart) throws DataAccessException {

        if (channelId == null) {
            return null;
        }

        Collection<TimetableSlot> slots = (Collection<TimetableSlot>)getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")//
                        .append("FROM ").append(clazz.getCanonicalName()).append(" tt ")//
                        /*DB paging-only FIX*//*.append("     LEFT JOIN FETCH tt.location.channel ")//
                        .append("     LEFT JOIN tt.playlist ")//
                        .append("     LEFT JOIN tt.programme ")//*/
                        .append("WHERE ")//
                        //.append("     tt.location.channel.label = :channelId ")//
                        /*DB paging-only FIX*/.append("     tt.location.channel.label = :channelId ")//
                        //Do only bother retrieving enabled slots
                        .append("     AND tt.enabled = TRUE ")//
                        .append("     AND tt.playlist IS NOT NULL ");
                        if (startMillisecondInPeriod!=null) hqlCommand
                            .append(" AND tt.startMillisecondFromPeriod >= :fuzzyMillisecondInPeriod ");
                        hqlCommand
                        .append("     AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("       OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyCurrentPeriodStart ) ) ")//
                        .append("       OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fuzzyPreviousPeriodStart >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fuzzyPreviousPeriodStart + tt.startMillisecondFromPeriod ) >= :fuzzyTime AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fuzzyPreviousPeriodStart ) ) ")//
                        .append("     ) ")//
                        .append("ORDER BY ")//
                        .append("   tt.startMillisecondFromPeriod ASC, ")//
                        //Slots that were programmed last have precedence
                        .append("   tt.location.startTimestamp DESC ");
                Query query = session.createQuery(hqlCommand.toString());
                query.setString("channelId", channelId);
                query.setLong("fuzzyTime", absoluteStartTime);
                if (startMillisecondInPeriod!=null)
                    query.setLong("fuzzyMillisecondInPeriod", startMillisecondInPeriod);
                query.setLong("fuzzyPreviousPeriodStart", previousPeriodStart);
                query.setLong("fuzzyCurrentPeriodStart", currentPeriodStart);
                //Unique result
                //TODO: remove the joins to issue a real LIMIT in SQL and make it a lot faster
                query.setFirstResult(0);
                query.setMaxResults(1);
                /*DB paging-only FIX*///Hibernate is sometimes insane; it cannot add the proper LIMIT SQL statemennt in such a query: it will first get everything in MEMORY(!) and then filter out, madness!
                return query.list();
            }
        });
        if (slots!=null && !slots.isEmpty()) {
            Iterator<TimetableSlot> i = slots.iterator();
            //It doesn't matter if the set is not sorted, setFirstResult() and setMaxResults() will do the job
            if (i.hasNext()) return i.next();
        }
        return null;

    }
    
    public Collection<DailyTimetableSlot> getNextEnabledDailyTimetableSlotForChannelFuzzyFrom(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws DataAccessException {
        if (channelId == null || time == null) {
            return new HashSet<DailyTimetableSlot>();
        }
        
        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTime = time.switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromStartDay = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonthWeekDay(time))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromDayBefore = new YearMonthWeekDayHourMinuteSecondMillisecond(fromStartDay);
        fromDayBefore.addDays(-1);

        HashSet<DailyTimetableSlot> ds = new HashSet<DailyTimetableSlot>();
        //I'm strugging with SQL
        //Try first to select a slot whose own period starts after the specified getPeriodInSecond()
        DailyTimetableSlot d1 = (DailyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(DailyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), adjustedTime.getMillisecondInDayPeriod(), fromStartDay.getUnixTime(), fromDayBefore.getUnixTime());
        if (d1!=null) ds.add(d1);
        //Then pick up the first one available, without any constraint, the best match between will be sorted out later on
        DailyTimetableSlot d2 = (DailyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(DailyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), fromStartDay.getUnixTime(), fromDayBefore.getUnixTime());
        if (d2!=null) ds.add(d2);
        return ds;

    }

    public Collection<WeeklyTimetableSlot> getNextEnabledWeeklyTimetableSlotForChannelFuzzyFrom(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws DataAccessException {
        if (channelId == null || time == null) {
            return new HashSet<WeeklyTimetableSlot>();
        }
        
        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTime = time.switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromStartWeek = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonthWeek(time))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromWeekBefore = new YearMonthWeekDayHourMinuteSecondMillisecond(fromStartWeek);
        fromWeekBefore.addWeeks(-1);

        HashSet<WeeklyTimetableSlot> ws = new HashSet<WeeklyTimetableSlot>();
        //I'm strugging with SQL
        //Try first to select a slot whose own period starts after the specified getPeriodInSecond()
        WeeklyTimetableSlot w1 = (WeeklyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(WeeklyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), adjustedTime.getMillisecondInWeekPeriod(), fromStartWeek.getUnixTime(), fromWeekBefore.getUnixTime());
        if (w1!=null) ws.add(w1);
        //Then pick up the first one available, without any constraint, the best match between will be sorted out later on
        WeeklyTimetableSlot w2 = (WeeklyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(WeeklyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), fromStartWeek.getUnixTime(), fromWeekBefore.getUnixTime());
        if (w2!=null) ws.add(w2);
        return ws;
    }

    public Collection<MonthlyTimetableSlot> getNextEnabledMonthlyTimetableSlotForChannelFuzzyFrom(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws DataAccessException {
        if (channelId == null || time == null) {
            return new HashSet<MonthlyTimetableSlot>();
        }
        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTime = time.switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromStartMonth = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonth(time))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromMonthBefore = new YearMonthWeekDayHourMinuteSecondMillisecond(fromStartMonth);
        fromMonthBefore.addMonths(-1);

        HashSet<MonthlyTimetableSlot> ms = new HashSet<MonthlyTimetableSlot>();
        //I'm strugging with SQL
        //Try first to select a slot whose own period starts after the specified getPeriodInSecond()
        MonthlyTimetableSlot m1 = (MonthlyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(MonthlyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), adjustedTime.getMillisecondInMonthPeriod(), fromStartMonth.getUnixTime(), fromMonthBefore.getUnixTime());
        if (m1!=null) ms.add(m1);
        //Then pick up the first one available, without any constraint, the best match between will be sorted out later on
        MonthlyTimetableSlot m2 = (MonthlyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(MonthlyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), fromStartMonth.getUnixTime(), fromMonthBefore.getUnixTime());
        if (m2!=null) ms.add(m2);
        return ms;
    }

    public Collection<YearlyTimetableSlot> getNextEnabledYearlyTimetableSlotForChannelFuzzyFrom(final String channelId, final String tz, final YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws DataAccessException {
        if (channelId == null || time == null) {
            return new HashSet<YearlyTimetableSlot>();
        }
        //Get the time within the channel time zone
        final YearMonthWeekDayHourMinuteSecondMillisecondInterface adjustedTime = time.switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromStartYear = (new YearMonthWeekDayHourMinuteSecondMillisecond(new Year(time))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinuteSecondMillisecond fromYearBefore = new YearMonthWeekDayHourMinuteSecondMillisecond(fromStartYear);
        fromYearBefore.addYears(-1);

        HashSet<YearlyTimetableSlot> ys = new HashSet<YearlyTimetableSlot>();
        //I'm strugging with SQL
        //Try first to select a slot whose own period starts after the specified getPeriodInSecond()
        YearlyTimetableSlot y1 = (YearlyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(YearlyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), adjustedTime.getMillisecondInYearPeriod(), fromStartYear.getUnixTime(), fromYearBefore.getUnixTime());
        if (y1!=null) ys.add(y1);
        //Then pick up the first one available, without any constraint, the best match between will be sorted out later on
        YearlyTimetableSlot y2 = (YearlyTimetableSlot)getNextEnabledRecurringTimetableSlotForChannelFuzzyFrom(YearlyTimetableSlot.class, channelId, adjustedTime.getUnixTime(), fromStartYear.getUnixTime(), fromYearBefore.getUnixTime());
        if (y2!=null) ys.add(y2);
        return ys;
    }

    public void deleteChannel(Channel channel) throws DataAccessException {
        if (channel != null) {
            getTemplate().delete(channel);
        }
    }

    public void deleteBatchChannels(Collection<Channel> channels) throws DataAccessException {
        if (channels != null) {
            getTemplate().deleteAll(channels);
        }
    }

    public void deleteTimetableSlot(TimetableSlot timetableSlot) throws DataAccessException {
        if (timetableSlot != null) {
            getTemplate().delete(timetableSlot);
        }
    }

    public void deleteQueueItem(QueueItem queueItem) throws DataAccessException {
        if (queueItem != null) {
            getTemplate().delete(queueItem);
        }
    }
    public void deleteStreamItem(StreamItem streamItem) throws DataAccessException {
        if (streamItem != null) {
            getTemplate().delete(streamItem);
        }
    }

    public void deleteWebservice(Webservice webservice) throws DataAccessException {
        if (webservice != null) {
            getTemplate().delete(webservice);
        }
    }

    public void deleteBatchTimetableSlots(Collection<TimetableSlot> timetableSlots) throws DataAccessException {
        if (timetableSlots != null) {
            getTemplate().deleteAll(timetableSlots);
        }
    }

    public void deleteBatchQueueItems(Collection<QueueItem> queueItems) throws DataAccessException {
        if (queueItems != null) {
            getTemplate().deleteAll(queueItems);
        }
    }

    public void deleteBatchWebservices(Collection<Webservice> webservices) throws DataAccessException {
        if (webservices != null) {
            getTemplate().deleteAll(webservices);
        }
    }

    /*public void deleteTimetableSlotsForChannel(final String channelId, final JSYearMonthDayHourMinute fromSchedulingTime, final JSYearMonthDayHourMinute toSchedulingTime) throws DataAccessException {
    if (channelId != null && fromSchedulingTime != null && toSchedulingTime != null) {
    getTemplate().execute(new HibernateCallback() {

    public Object doInHibernate(Session session) throws HibernateException, SQLException {
    String hqlCommand =
    "DELETE "
    + "FROM " + TimetableSlot.class.getCanonicalName()).append(" tt "
    + "WHERE "
    + "     tt.refID.channel = :channelId "
    + "     AND tt.refID.startYear >= :fromYear "
    + "     AND tt.refID.startMonth >= :fromMonth "
    + "     AND tt.refID.startDayOfMonth >= :fromDayOfMonth "
    + "     AND tt.refID.startHourOfDay >= :fromHourOfDay "
    + "     AND tt.refID.startMinuteOfHour >= :fromMinuteOfHour "
    + "     AND tt.refID.startYear <= :toYear "
    + "     AND tt.refID.startMonth <= :toMonth "
    + "     AND tt.refID.startDayOfMonth <= :toDayOfMonth "
    + "     AND tt.refID.startHourOfDay <= :toHourOfDay "
    + "     AND tt.refID.startMinuteOfHour <= :toMinuteOfHour ";

    Query query = session.createQuery(hqlCommand);
    query.setString("channelId", channelId);
    query.setShort("fromYear", fromSchedulingTime.getYearMonthDay().getYearMonth().getYear());
    query.setByte("fromMonth", fromSchedulingTime.getYearMonthDay().getYearMonth().getMonth());
    query.setByte("fromDayOfMonth", fromSchedulingTime.getYearMonthDay().getDayOfMonth());
    query.setByte("fromHourOfDay", fromSchedulingTime.getHourMinute().getHour());
    query.setByte("fromMinuteOfHour", fromSchedulingTime.getHourMinute().getHour());
    query.setShort("toYear", toSchedulingTime.getYearMonthDay().getYearMonth().getYear());
    query.setByte("toMonth", toSchedulingTime.getYearMonthDay().getYearMonth().getMonth());
    query.setByte("toDayOfMonth", toSchedulingTime.getYearMonthDay().getDayOfMonth());
    query.setByte("toHourOfDay", toSchedulingTime.getHourMinute().getHour());
    query.setByte("toMinuteOfHour", toSchedulingTime.getHourMinute().getHour());
    return query.executeUpdate();
    }
    });
    }
    }**/
    /**
     * Find all Timetable slots contained within a week, for one or more channels
     * Start query should be cached since it is complex and ubiquitous
     **/
    public Collection<TimetableSlot> getWeekTimetableSlotsWithRightsByChannelsForChannel(
            final Collection<String> channelIds,
            final String channelId, final String tz,
            YearMonthWeekDayHourMinuteInterface weekStart) throws DataAccessException {
        if (channelId == null || channelIds == null || channelIds.isEmpty() || weekStart == null) {
            return new HashSet<TimetableSlot>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!channelIds.contains(channelId)) {
            return new HashSet<TimetableSlot>();
        }

        return getWeekTimetableSlotsForChannel(channelId, tz, weekStart);

    }

    /**
     * weekStart may not be Monday-based so make sure to include all combinations of spanning weeks, months, years in the request; the request contains lots of branching
     * @param channelId
     * @param weekStart
     * @return
     * @throws DataAccessException 
     */
    public Collection<TimetableSlot> getWeekTimetableSlotsForChannel(
            final String channelId, final String tz,
            YearMonthWeekDayHourMinuteInterface weekStart) throws DataAccessException {

        if (channelId == null || weekStart == null) {
            return new HashSet<TimetableSlot>();
        }

        final IsSpanning isSpanning = new IsSpanning();
        
        //Get the weekStart within the channel time zone
        final YearMonthWeekDayHourMinuteInterface adjustedWeekStart = weekStart.switchTimeZone(tz);
        //Compute the end of week from the given start of week
        final YearMonthWeekDayHourMinute weekEnd = new YearMonthWeekDayHourMinute(adjustedWeekStart);
        //because those results are meant to be displayed within a widget with fixed time slots, the length of the interval must always be of the same length in minutes (no DST or whatever), just 7 full days of 24 hours each
        weekEnd.addWeeks(1);
        
        final YearMonthWeekDayHourMinute fromStartDay = (new YearMonthWeekDayHourMinuteSecondMillisecond(new YearMonthWeekDay(weekStart))).switchTimeZone(tz);
        final YearMonthWeekDayHourMinute fromDayBefore = new YearMonthWeekDayHourMinute(fromStartDay);
        fromDayBefore.addDays(-1);
        
        //The actual starting week day is manually defined in weekStart, don't try to readjust it to monday or it will screw up the user-locale defined grid
        final YearMonthWeekDayHourMinute fromStartWeekLeft = (new YearMonthWeekDayHourMinute(new YearMonthWeek(weekStart))).switchTimeZone(tz); 
        final YearMonthWeekDayHourMinute _w = new YearMonthWeekDayHourMinute(weekStart);
        _w.addMinutes(6 * 24 * 60);
        final YearMonthWeekDayHourMinute fromStartWeekRight = (new YearMonthWeekDayHourMinute(new YearMonthWeek(_w))).switchTimeZone(tz); 
        final YearMonthWeekDayHourMinute fromWeekBefore = new YearMonthWeekDayHourMinute(fromStartWeekLeft);
        fromWeekBefore.addWeeks(-1);
        if ((new YearMonthWeek(fromStartWeekLeft)).compareTo(new YearMonthWeek(fromStartWeekRight)) != 0) {
            isSpanning.week = true;
        }
        
        final YearMonthWeekDayHourMinute fromStartMonthLeft = (new YearMonthWeekDayHourMinute(new YearMonth(weekStart))).switchTimeZone(tz);        
        final YearMonthWeekDayHourMinute fromStartMonthRight = (new YearMonthWeekDayHourMinute(new YearMonth(_w))).switchTimeZone(tz);         
        final YearMonthWeekDayHourMinute fromMonthBefore = new YearMonthWeekDayHourMinute(fromStartMonthLeft);
        fromMonthBefore.addMonths(-1);
        if ((new YearMonth(fromStartMonthLeft)).compareTo(new YearMonth(fromStartMonthRight)) != 0) {        
            isSpanning.month = true;
        }
        
        final YearMonthWeekDayHourMinute fromStartYearLeft = (new YearMonthWeekDayHourMinute(new Year(weekStart))).switchTimeZone(tz);        
        final YearMonthWeekDayHourMinute fromStartYearRight = (new YearMonthWeekDayHourMinute(new Year(_w))).switchTimeZone(tz);         
        final YearMonthWeekDayHourMinute fromYearBefore = new YearMonthWeekDayHourMinute(fromStartYearLeft);
        fromYearBefore.addYears(-1);
        if ((new Year(fromStartYearLeft)).compareTo(new Year(fromStartYearRight)) != 0) {        
            isSpanning.year = true;
        }

        Collection<TimetableSlot> list = (Collection<TimetableSlot>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(TimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*.append("     LEFT JOIN tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ")
                        .append("     tt.location.channel.label = :channelId ")
                        .append("     AND ( ")
                        .append("             ( ") //Unique timetable slot
                        .append("               tt.class = ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" ")
                        .append("               AND tt.location.startTimestamp < :toDate ")
                        .append("               AND tt.endTimestamp > :fromDate ")
                        .append("             ) OR ( ") //Recurring time slot)
                        .append("               tt.class != ").append(UniqueTimetableSlot.class.getCanonicalName()).append(" ") //      Commissioning date
                        .append("               AND tt.location.startTimestamp < :toDate ") //      Decommissioning date
                        .append("               AND (") //          Daily timetable slot
                        .append("                   ( tt.class = ").append(DailyTimetableSlot.class.getCanonicalName()).append(" ") //          Weekly timetable slot
                        .append("                       AND ( tt.decommissioningTimestamp IS NULL ")//
                        .append("                               OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartDay ) ) ")//
                        .append("                               OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fromDayBefore >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fromStartDay ) >= :fromDate AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromDayBefore ) ) ")//
                        .append("                       ) ")//
                        .append("                   ) ")//
                        .append("                   OR ( tt.class = ").append(WeeklyTimetableSlot.class.getCanonicalName()).append(" ") //          Monthly timetable slot
                        .append("                       AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                               OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartWeekLeft ) ) ")/*;//
                        if (isSpanning.week)
                        hqlCommand
                        .append("                               OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartWeekRight ) ) ");//
                        hqlCommand*/
                        .append("                               OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fromWeekBefore >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fromWeekBefore + tt.startMillisecondFromPeriod ) >= :fromDate AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromWeekBefore ) ) ")//
                        .append("                       ) ")//
                        .append("                   ) ")//
                        .append("                   OR ( ")
                        .append("                      tt.class = ").append(MonthlyTimetableSlot.class.getCanonicalName()).append(" ")
                        /*.append("                      AND ( tt.startWeekOfMonth < :toWeek ")
                        .append("                         OR ( ( tt.startWeekOfMonth = :toWeek AND tt.startDayOfWeek < :toDay ) ")
                        .append("                             OR ( tt.startDayOfWeek = :toDay AND tt.location.startMillisecond < :toTime ) ) ) ")
                        .append("                      AND ( tt.endWeekOfMonth > :fromWeek ")
                        .append("                         OR ( ( tt.endWeekOfMonth = :fromWeek AND tt.endDayOfWeek > :fromDay ) ")
                        .append("                             OR ( tt.endDayOfWeek = :fromDay AND tt.endMillisecond > :fromTime ) ) ) ")
                        */                      
                        .append("                               AND (")
                        .append("                                 (")
                        .append("                                   (")
                        .append("                                       tt.startMillisecondFromPeriod <= :fuzzyStartMillisecondInMonthLeft ")
                        .append("                                       AND (tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod OR tt.endMillisecondFromPeriod > :fuzzyStartMillisecondInMonthLeft) ")
                        .append("                                   ) OR (")
                        .append("                                       tt.startMillisecondFromPeriod >= :fuzzyStartMillisecondInMonthLeft ");
                        if (!isSpanning.month)                                
                        hqlCommand
                        .append("                                       AND tt.startMillisecondFromPeriod < :fuzzyEndMillisecondInMonthLeft ");
                        hqlCommand
                        .append("                                   ) OR (")//
                        .append("                                       tt.startMillisecondFromPeriod > tt.endMillisecondFromPeriod ")//
                        .append("                                       AND tt.endMillisecondFromPeriod > :fuzzyStartMillisecondInMonthLeft ")//
                        .append("                                   ) ")//
                        .append("                                 )")
                        .append("                                 AND ( tt.decommissioningTimestamp IS NULL ")//
                        .append("                                   OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartMonthLeft ) ) ")//
                        .append("                                   OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fromMonthBefore >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fromMonthBefore + tt.startMillisecondFromPeriod ) >= :fromDate AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromMonthBefore ) ) ")//
                        .append("                                 ) ");//
                        if (isSpanning.month)
                        hqlCommand
                        .append("                                 OR (")
                        .append("                                   tt.startMillisecondFromPeriod <= :fuzzyEndMillisecondInMonthRight ")//
                        .append("                                   AND ( tt.decommissioningTimestamp IS NULL ")//        
                        .append("                                     OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartMonthRight ) ) ")//
                        .append("                                   ) ")
                        .append("                                 ) ");//
                        hqlCommand        
                        .append("                   ) ") //          Yearly timetable slot
                        .append("                   OR ( ")
                        .append("                      tt.class = ").append(YearlyTimetableSlot.class.getCanonicalName()).append(" ")
                        /*.append("                      AND ( tt.startMonth < :toMonth ")
                        .append("                         OR ( ( tt.startMonth = :toMonth AND tt.startWeekOfMonth < :toWeek ) ")
                        .append("                             OR ( ( tt.startWeekOfMonth = :toWeek AND tt.startDayOfWeek < :toDay ) ")
                        .append("                                 OR ( tt.startDayOfWeek = :toDay AND tt.location.startMillisecond < :toTime ) ) ) ) ")
                        .append("                      AND ( tt.endMonth > :fromMonth ")
                        .append("                         OR ( ( tt.endMonth = :fromMonth AND tt.endWeekOfMonth > :fromWeek ) ")
                        .append("                             OR ( ( tt.endWeekOfMonth = :fromWeek AND tt.endDayOfWeek > :fromDay ) ")
                        .append("                                 OR ( tt.endDayOfWeek = :fromDay AND tt.endMillisecond > :fromTime ) ) ) ) ")
                        */
                        .append("                               AND (")
                        .append("                                 ( ")
                        .append("                                       tt.startMillisecondFromPeriod <= :fuzzyStartMillisecondInYearLeft ")
                        .append("                                       AND (tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod OR tt.endMillisecondFromPeriod > :fuzzyStartMillisecondInYearLeft) ")
                        .append("                                   ) OR (")
                        .append("                                       tt.startMillisecondFromPeriod >= :fuzzyStartMillisecondInYearLeft ");
                        if (!isSpanning.year)
                        hqlCommand                                
                        .append("                                       AND tt.startMillisecondFromPeriod < :fuzzyEndMillisecondInYearLeft ");
                        hqlCommand
                        .append("                                   ) OR (")//
                        .append("                                       tt.startMillisecondFromPeriod > tt.endMillisecondFromPeriod ")//
                        .append("                                       AND tt.endMillisecondFromPeriod > :fuzzyStartMillisecondInYearLeft ")//
                        .append("                                   ) ")//                                
                        .append("                                   )")
                        .append("                                 )")                                
                        .append("                                 AND ( tt.decommissioningTimestamp IS NULL ")//                        
                        .append("                                   OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartYearLeft ) ) ")//                     
                        .append("                                   OR ( tt.startMillisecondFromPeriod >= tt.endMillisecondFromPeriod AND tt.startMillisecondFromPeriod + :fromYearBefore >= tt.location.startTimestamp AND ( tt.endMillisecondFromPeriod + :fromYearBefore + tt.startMillisecondFromPeriod ) >= :fromDate AND tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromYearBefore ) ) ")//
                        .append("                                 ) ")//                                               
                        .append("                               ) ");
                        if (isSpanning.year)
                        hqlCommand
                        .append("                                 OR (")
                        .append("                                   tt.startMillisecondFromPeriod <= :fuzzyEndMillisecondInYearRight ")//
                        .append("                                   AND ( tt.decommissioningTimestamp IS NULL ")//        
                        .append("                                     OR ( tt.decommissioningTimestamp > ( tt.startMillisecondFromPeriod + :fromStartYearRight ) ) ")//
                        .append("                                   ) ")
                        .append("                                 ) ");// 
                        hqlCommand
                        .append("                   ) ")
                        .append("               ) ")
                        .append("             ) ")
                        .append("     ) ");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("channelId", channelId);
                query.setLong("fromDate", adjustedWeekStart.getUnixTime());
                query.setLong("fuzzyStartMillisecondInMonthLeft", adjustedWeekStart.getMillisecondInMonthPeriod());
                query.setLong("fuzzyStartMillisecondInYearLeft", adjustedWeekStart.getMillisecondInYearPeriod());
                query.setLong("toDate", weekEnd.getUnixTime());
                if (isSpanning.month) {
                    query.setLong("fromStartMonthRight", fromStartMonthRight.getUnixTime());
                    query.setLong("fuzzyEndMillisecondInMonthRight", weekEnd.getMillisecondInMonthPeriod());
                } else {
                    query.setLong("fuzzyEndMillisecondInMonthLeft", weekEnd.getMillisecondInMonthPeriod());
                }
                query.setLong("fromStartDay", fromStartDay.getUnixTime());
                query.setLong("fromDayBefore", fromDayBefore.getUnixTime());
                query.setLong("fromStartWeekLeft", fromStartWeekLeft.getUnixTime());
                //if (isSpanning.week)
                //    query.setLong("fromStartWeekRight", fromStartWeekRight.getUnixTime());
                query.setLong("fromWeekBefore", fromWeekBefore.getUnixTime());
                query.setLong("fromStartMonthLeft", fromStartMonthLeft.getUnixTime());
                query.setLong("fromMonthBefore", fromMonthBefore.getUnixTime());
                query.setLong("fromStartYearLeft", fromStartYearLeft.getUnixTime());
                if (isSpanning.year) {
                    query.setLong("fromStartYearRight", fromStartYearRight.getUnixTime());
                    query.setLong("fuzzyEndMillisecondInYearRight", weekEnd.getMillisecondInYearPeriod());
                } else {
                    query.setLong("fuzzyEndMillisecondInYearLeft", weekEnd.getMillisecondInYearPeriod());
                }                    
                query.setLong("fromYearBefore", fromYearBefore.getUnixTime());
                return query.list();
            }
        });
        return (list == null) ? new HashSet<TimetableSlot>() : list;

    }

    /**
     * Find all Channels that have the specified rights
     * Start query should be cached since it is complex and ubiquitous
     * @param channels
     * @return
     * @throws DataAccessException
     **/
    public Collection<Channel> getChannelsWithRightsByChannelsForProgramme(final Collection<String> channelIds, final GroupAndSort<CHANNEL_TAGS> constraint, final String programmeId) throws DataAccessException {
        if (channelIds == null || channelIds.isEmpty()) {
            return new HashSet<Channel>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr, rar, pmr, lr ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("     LEFT JOIN ra.programmes pr ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ")
                        .append("WHERE ")
                        .append((programmeId != null
                        ? "     pr.label = :programmeId "
                        + "     AND " : ""))
                        .append("     ra.label IN (:channelIds) ");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<CHANNEL_TAGS>() {

                    @Override
                    public String[] onTag(CHANNEL_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"ra.description"};
                            case label:
                                return new String[]{"ra.label"};
                            case programmes:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"rar.username", "pmr.username", "lr.username"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setParameterList("channelIds", channelIds);
                    if (programmeId != null) {
                        query.setString("programmeId", programmeId);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Channel>();
                }
            }
        });

        return (Collection<Channel>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Channel> getChannelsWithRightsByChannelsForProgrammes(final Collection<String> channelIds, final GroupAndSort<CHANNEL_TAGS> constraint, final Collection<String> programmeIds) throws DataAccessException {
        if (channelIds == null || channelIds.isEmpty()) {
            return new HashSet<Channel>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr, rar, pmr, lr ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("     LEFT JOIN ra.programmes pr ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ")
                        .append("WHERE ")
                        .append((programmeIds != null && !programmeIds.isEmpty()
                        ? "     pr.label IN (:programmeIds) "
                        + "     AND " : ""))
                        .append("     ra.label IN (:channelIds) ");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<CHANNEL_TAGS>() {

                    @Override
                    public String[] onTag(CHANNEL_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"ra.description"};
                            case label:
                                return new String[]{"ra.label"};
                            case programmes:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"rar.username", "pmr.username", "lr.username"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setParameterList("channelIds", channelIds);
                    if (programmeIds != null && !programmeIds.isEmpty()) {
                        query.setParameterList("programmeIds", programmeIds);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Channel>();
                }
            }
        });

        return (Collection<Channel>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Channel> getAllChannelsWithNoRightForCreator(final String restrictedUserId, final GroupAndSort<CHANNEL_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ")
                        .append("     LEFT JOIN ra.programmes pr ")
                        .append("WHERE (")
                        .append("   rar IS NULL AND ")
                        .append("   pmr IS NULL AND ")
                        .append("   lr IS NULL ")
                        .append(") ");
                        if (restrictedUserId != null)
                            hqlCommand.append(" AND ra.creator.username = :restrictedUserId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<CHANNEL_TAGS>() {

                    @Override
                    public String[] onTag(CHANNEL_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"ra.description"};
                            case label:
                                return new String[]{"ra.label"};
                            case programmes:
                                return new String[]{"pr.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (restrictedUserId != null) {
                        query.setString("restrictedUserId", restrictedUserId);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Channel>();
                }
            }
        });

        return (Collection<Channel>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Channel> getChannelsForProgramme(final GroupAndSort<CHANNEL_TAGS> constraint, final String programmeId) throws DataAccessException {

        if (programmeId == null) {
            return new HashSet<Channel>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr, rar, pmr, lr ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("     JOIN ra.programmes pr ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ")
                        .append("WHERE ")
                        .append("     pr.label = :programmeId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<CHANNEL_TAGS>() {

                    @Override
                    public String[] onTag(CHANNEL_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"ra.description"};
                            case label:
                                return new String[]{"ra.label"};
                            case programmes:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"rar.username", "pmr.username", "lr.username"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("programmeId", programmeId);
                    return query.scroll();
                } else {
                    return new HashSet<Channel>();
                }
            }
        });

        return (Collection<Channel>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    /**
     * Find all Webservices that have the specified rights
     * @param channels
     * @return
     * @throws DataAccessException
     **/
    public Collection<Webservice> getWebservicesWithRightsByChannels(final Collection<String> channelIds, final GroupAndSort<WEBSERVICE_TAGS> constraint) throws DataAccessException {
        if (channelIds == null || channelIds.isEmpty()) {
            return new HashSet<Webservice>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ws ")
                        .append("FROM ").append(Webservice.class.getCanonicalName()).append(" ws ")
                        //.append("     LEFT JOIN ws.channel wsc ")
                        //.append("     JOIN ws.channel wsc ")//
                        .append("WHERE ")
                        .append("     ws.channel.label IN (:channelIds) ");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<WEBSERVICE_TAGS>() {

                    @Override
                    public String[] onTag(WEBSERVICE_TAGS currentTag) {
                        switch (currentTag) {
                            case apiKey:
                                return new String[]{"ws.apiKey"};
                            case channel:
                                return new String[]{"ws.channel.label"};
                            case maxDailyQuota:
                                return new String[]{"ws.maxDailyLimit"};
                            case maxFireRate:
                                return new String[]{"ws.maxFireRate"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setParameterList("channelIds", channelIds);
                    return query.scroll();
                } else {
                    return new HashSet<Webservice>();
                }
            }
        });

        return (Collection<Webservice>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Webservice> getAllWebservices(final GroupAndSort<WEBSERVICE_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ws ")
                        .append("FROM ").append(Webservice.class.getCanonicalName()).append(" ws ")
                        ;//.append("     LEFT JOIN FETCH ws.channel wsc ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<WEBSERVICE_TAGS>() {

                    @Override
                    public String[] onTag(WEBSERVICE_TAGS currentTag) {
                        switch (currentTag) {
                            case apiKey:
                                return new String[]{"ws.apiKey"};
                            case channel:
                                return new String[]{"ws.channel.label"};
                            case maxDailyQuota:
                                return new String[]{"ws.maxDailyLimit"};
                            case maxFireRate:
                                return new String[]{"ws.maxFireRate"};
                            default:
                                return null;
                        }
                    }
                });

                return query != null ? query.scroll() : new HashSet<Webservice>();
            }
        });

        return (Collection<Webservice>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Hit getLatestWebserviceHit(final String apiKey) {
        if (apiKey == null) {
            return null;
        }
        return (Hit) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT ht ")//
                            .append("FROM ").append(Hit.class.getCanonicalName()).append(" ht ")//
                            .append("WHERE ")//
                            .append("     ht.refID.webservice = :id ")// 
                            .append(" ORDER BY ")//
                            .append("     ht.refID.timestamp DESC ");
                return session.createQuery(hqlCommand.toString())//
                        .setString("id", apiKey)//
                        .setMaxResults(1)//
                        .setFirstResult(0)//
                        .uniqueResult();
            }
        });
    }
    
    public Collection<Channel> getChannelsForProgrammes(final GroupAndSort<CHANNEL_TAGS> constraint, final Collection<String> programmeIds) throws DataAccessException {

        if (programmeIds == null || programmeIds.isEmpty()) {
            return new HashSet<Channel>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr, rar, pmr, lr ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        //.append("     LEFT JOIN ra.programmes pr ")
                        .append("     JOIN ra.programmes pr ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ")
                        .append("WHERE ")
                        .append("     pr.label IN (:programmeIds) ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<CHANNEL_TAGS>() {

                    @Override
                    public String[] onTag(CHANNEL_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"ra.description"};
                            case label:
                                return new String[]{"ra.label"};
                            case programmes:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"rar.username", "pmr.username", "lr.username"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setParameterList("programmeIds", programmeIds);
                    return query.scroll();
                } else {
                    return new HashSet<Channel>();
                }
            }
        });

        return (Collection<Channel>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<TimetableSlot> getTimetableSlotsWithRightsByChannelsForChannel(
            final Collection<String> channelIds,
            final GroupAndSort<TIMETABLE_TAGS> constraint,
            final String channelId) throws DataAccessException {
        if (channelId == null || channelIds == null || channelIds.isEmpty()) {
            return new HashSet<TimetableSlot>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!channelIds.contains(channelId)) {
            return new HashSet<TimetableSlot>();
        }

        return getTimetableSlotsForChannel(constraint, channelId);
    }

    public Collection<TimetableSlot> getTimetableSlotsForChannel(final GroupAndSort<TIMETABLE_TAGS> constraint, final String channelId) throws DataAccessException {

        if (channelId == null) {
            return new HashSet<TimetableSlot>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(TimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*.append("     LEFT JOIN tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ")
                        .append("     tt.location.channel.label = :channelId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<TIMETABLE_TAGS>() {

                    @Override
                    public String[] onTag(TIMETABLE_TAGS currentTag) {
                        switch (currentTag) {
                            case channel:
                                return new String[]{"tt.location.channel.label"};
                            case playlist:
                                return new String[]{"tt.playlist.refID"};
                            case programme:
                                return new String[]{"tt.programme.label"};
                            case time:
                                return new String[]{"tt.location.startTimestamp"};
                            case enabled:
                                return new String[]{"tt.enabled"};
                            case type:
                                return new String[]{"tt.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("channelId", channelId);
                    return query.scroll();
                } else {
                    return new HashSet<TimetableSlot>();
                }
            }
        });

        return (Collection<TimetableSlot>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<TimetableSlot> getTimetableSlotsWithRightsByChannelsForProgramme(
            final Collection<String> channelIds,
            final GroupAndSort<TIMETABLE_TAGS> constraint,
            final String programmeId) throws DataAccessException {
        if (programmeId == null || (channelIds != null && channelIds.isEmpty())) {
            return new HashSet<TimetableSlot>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(TimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*.append("     LEFT JOIN tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ");
                        if (channelIds!=null)
                            hqlCommand.append("     tt.location.channel.label IN (:channelIds) ")
                            .append(" AND ");
                        hqlCommand.append("     tt.programme.label = :programmeId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<TIMETABLE_TAGS>() {

                    @Override
                    public String[] onTag(TIMETABLE_TAGS currentTag) {
                        switch (currentTag) {
                            case channel:
                                return new String[]{"tt.location.channel.label"};
                            case playlist:
                                return new String[]{"tt.playlist.refID"};
                            case programme:
                                return new String[]{"tt.programme.label"};
                            case time:
                                return new String[]{"tt.location.startTimestamp"};
                            case enabled:
                                return new String[]{"tt.enabled"};
                            case type:
                                return new String[]{"tt.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("programmeId", programmeId);
                    if (channelIds!=null)
                        query.setParameterList("channelIds", channelIds);
                    return query.scroll();
                } else {
                    return new HashSet<TimetableSlot>();
                }
            }
        });

        return (Collection<TimetableSlot>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<TimetableSlot> getTimetableSlotsForProgramme(final GroupAndSort<TIMETABLE_TAGS> constraint, final String programmeId) throws DataAccessException {
        return getTimetableSlotsWithRightsByChannelsForProgramme(null, constraint, programmeId);
    }

    public Collection<TimetableSlot> getTimetableSlotsWithRightsByChannelsForPlaylist(
            final Collection<String> channelIds,
            final GroupAndSort<TIMETABLE_TAGS> constraint,
            final String playlistId) throws DataAccessException {
        if (playlistId == null || (channelIds != null && channelIds.isEmpty())) {
            return new HashSet<TimetableSlot>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT tt ")
                        .append("FROM ").append(TimetableSlot.class.getCanonicalName()).append(" tt ")
                        /*.append("     LEFT JOIN tt.location.channel ")
                        .append("     LEFT JOIN tt.playlist ")
                        .append("     LEFT JOIN tt.programme ")*/
                        .append("WHERE ");
                        if (channelIds!=null)
                            hqlCommand.append("     tt.location.channel.label IN (:channelIds) ")
                            .append(" AND ");
                        hqlCommand.append("     tt.playlist.refID = :playlistId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<TIMETABLE_TAGS>() {

                    @Override
                    public String[] onTag(TIMETABLE_TAGS currentTag) {
                        switch (currentTag) {
                            case channel:
                                return new String[]{"tt.location.channel.label"};
                            case playlist:
                                return new String[]{"tt.playlist.refID"};
                            case programme:
                                return new String[]{"tt.programme.label"};
                            case time:
                                return new String[]{"tt.location.startTimestamp"};
                            case enabled:
                                return new String[]{"tt.enabled"};
                            case type:
                                return new String[]{"tt.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("playlistId", playlistId);
                    if (channelIds!=null)
                        query.setParameterList("channelIds", channelIds);
                    return query.scroll();
                } else {
                    return new HashSet<TimetableSlot>();
                }
            }
        });

        return (Collection<TimetableSlot>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<TimetableSlot> getTimetableSlotsForPlaylist(final GroupAndSort<TIMETABLE_TAGS> constraint, final String playlistId) throws DataAccessException {
        return getTimetableSlotsWithRightsByChannelsForPlaylist(null, constraint, playlistId);
    }

    public Collection<Channel> getAllChannels() {
        Collection<Channel> list = (Collection<Channel>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr, rar, pmr, lr  ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        ;/*.append("     LEFT JOIN ra.programmes pr ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ");*/
                Query query = session.createQuery(hqlCommand.toString());
                return query.list();
            }
        });
        return (list == null) ? new HashSet<Channel>() : list;
    }

    public Collection<Channel> getAllChannels(final GroupAndSort<CHANNEL_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")//, pr, rar, pmr, lr ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("     LEFT JOIN ra.programmes pr ")
                        .append("     LEFT JOIN ra.channelAdministratorRights rar ")
                        .append("     LEFT JOIN ra.programmeManagerRights pmr ")
                        .append("     LEFT JOIN ra.listenerRights lr ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<CHANNEL_TAGS>() {

                    @Override
                    public String[] onTag(CHANNEL_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"ra.description"};
                            case label:
                                return new String[]{"ra.label"};
                            case programmes:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"rar.username", "pmr.username", "lr.username"};
                            default:
                                return null;
                        }
                    }
                });

                return query != null ? query.scroll() : new HashSet<Channel>();
            }
        });

        return (Collection<Channel>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Channel> getAllChannelsWithNoRight(final GroupAndSort<CHANNEL_TAGS> constraint) throws DataAccessException {
        return getAllChannelsWithNoRightForCreator(null, constraint);
    }

    public Collection<QueueItem> getQueueItemsForChannel(final GroupAndSort<QUEUE_TAGS> constraint, final String channelId) throws DataAccessException {

        if (channelId == null) {
            return new HashSet<QueueItem>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT qi ")//
                        .append("FROM ").append(QueueItem.class.getCanonicalName()).append(" qi ")//
                        /*.append("     LEFT JOIN qi.location.channel qc ")//
                        .append("WHERE ")//
                        .append("     qc.label = :channelId ");*/
                        .append("WHERE ")//
                        .append("     qi.location.channel.label = :channelId ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<QUEUE_TAGS>() {

                    @Override
                    public String[] onTag(QUEUE_TAGS currentTag) {
                        switch (currentTag) {
                            case time:
                                return new String[]{"qi.location.sequenceIndex"};
                            case duration:
                                return new String[]{"qi.duration"};
                            case type:
                                return new String[]{"qi.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("channelId", channelId);
                    return query.scroll();
                } else {
                    return new HashSet<QueueItem>();
                }
            }
        });

        return (Collection<QueueItem>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<QueueItem> getQueueItemsWithRightsByChannelsForChannel(
            final Collection<String> channelIds,
            final GroupAndSort<QUEUE_TAGS> constraint,
            final String channelId) throws DataAccessException {
        if (channelId == null || channelIds == null || channelIds.isEmpty()) {
            return new HashSet<QueueItem>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!channelIds.contains(channelId)) {
            return new HashSet<QueueItem>();
        }

        return getQueueItemsForChannel(constraint, channelId);
    }

    public Collection<QueueItem> getQueueItemsWithRightsByChannels(
            final Collection<String> channelIds,
            final GroupAndSort<QUEUE_TAGS> constraint) throws DataAccessException {
        if (channelIds != null && channelIds.isEmpty()) {
            return new HashSet<QueueItem>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT qi ")
                        .append("FROM ").append(QueueItem.class.getCanonicalName()).append(" qi ")
                        /*.append("     LEFT JOIN qc.channel ");
                if (channelIds != null) {
                    hqlCommand.append("WHERE ")//
                            .append("     qc.label IN (:channelIds) ");//
                }*/
                        ;
                if (channelIds != null) {
                    hqlCommand.append("WHERE ")//
                            .append("     qi.channel.label IN (:channelIds) ");//
                }
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<QUEUE_TAGS>() {

                    @Override
                    public String[] onTag(QUEUE_TAGS currentTag) {
                        switch (currentTag) {
                            case time:
                                return new String[]{"qi.location.sequenceIndex"};
                            case duration:
                                return new String[]{"qi.duration"};
                            case type:
                                return new String[]{"qi.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (channelIds != null) {
                        query.setParameterList("channelIds", channelIds);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<QueueItem>();
                }
            }
        });

        return (Collection<QueueItem>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<QueueItem> getAllQueueItems(final GroupAndSort<QUEUE_TAGS> constraint) throws DataAccessException {
        return getQueueItemsWithRightsByChannels(null, constraint);
    }

    public Collection<String> getLinkedChannelIdsByRestrictedUser(final String username, final RightsBundle rightsBundle) throws DataAccessException {

        if (username == null || rightsBundle == null || !rightsBundle.isRightsSelected()) {
            return new HashSet<String>();
        }

        Collection<String> list = (Collection<String>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra.label ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("WHERE ")
                        .append(" (")
                        .append((rightsBundle.isChannelAdministratorRights()
                        ? "     ra.channelAdministratorRights.username = :username " : ""))//"     rar IS NOT NULL " : "")
                        .append((rightsBundle.isChannelAdministratorRights() && rightsBundle.isProgrammeManagerRights()
                        ? " OR " : ""))
                        .append((rightsBundle.isProgrammeManagerRights()
                        ? "     ra.programmeManagerRights.username = :username " : ""))//"     pmr IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights()) && rightsBundle.isListenerRights()
                        ? " OR " : ""))
                        .append((rightsBundle.isListenerRights()
                        ? "     ra.listenerRights.username = :username " : ""))//"     lr IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights()) && rightsBundle.isAnimatorRights()
                        ? " OR " : ""))
                        .append((rightsBundle.isAnimatorRights()
                        ? "     ra.programmes.animatorRights.username = :username " : ""))//"     ar IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights() || rightsBundle.isAnimatorRights()) && rightsBundle.isCuratorRights()
                        ? " OR " : ""))
                        .append((rightsBundle.isCuratorRights()
                        ? "     ra.programmes.curatorRights.username = :username " : ""))//"     cur IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights() || rightsBundle.isAnimatorRights() || rightsBundle.isCuratorRights()) && rightsBundle.isContributorRights()
                        ? " OR " : ""))
                        .append((rightsBundle.isContributorRights()
                        ? "     ra.programmes.contributorRights.username = :username " : ""))//"     cor IS NOT NULL " : "")
                        .append(") OR ")
                        .append("( ")
                        .append("     ra.channelAdministratorRights IS NULL AND ")
                        .append("     ra.programmeManagerRights IS NULL AND ")
                        .append("     ra.listenerRights IS NULL AND ")
                        .append("     ra.programmes.animatorRights IS NULL AND ")
                        .append("     ra.programmes.curatorRights IS NULL AND ")
                        .append("     ra.programmes.contributorRights IS NULL AND ")
                        .append("     ( ")
                        .append("         us.latestEditor IS NULL OR ")
                        .append("         us.latestEditor.username = :username ")
                        .append("     ) ")
                        .append(")");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("username", username);
                return query.list();
            }
        });

        return (list == null) ? new HashSet<String>() : list;
    }

    public Collection<String> getOpenChannelLabels() throws DataAccessException {

        Collection<String> list = (Collection<String>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra.label ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("WHERE ")
                        .append("   ra.openRegistration = TRUE ");

                Query query = session.createQuery(hqlCommand.toString());
                return query.list();
            }
        });

        return (list == null) ? new HashSet<String>() : list;
    }

    public void purgeHits(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        if (expirationDate != null) {
            getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("DELETE ").append(Hit.class.getCanonicalName()).append(" hi ")//
                            .append("WHERE ")//
                            .append("     hi.refID.timestamp < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    return query.executeUpdate();
                }
            });
        }
    }

    public Long getNumberOfHitsAfterTimeThreshold(final String apiKey, final YearMonthWeekDayHourMinuteSecondMillisecond thresholdTime) throws DataAccessException {
        if (apiKey==null || thresholdTime==null) {
            return null;
        }

        Long numOfRequests = (Long) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        //DO NOT use "fetch" if the joins are not developed in the SELECT clause, like it is the case with COUNT
                        .append("SELECT COUNT(hi.refID.timestamp) ")//
                        .append("FROM ").append(Hit.class.getCanonicalName()).append(" hi ")//
                        .append("WHERE ")//
                        .append("       hi.refID.webservice = :apiKey ")//
                        .append("   AND ")//
                        .append("       hi.refID.timestamp > :thresholdDate ");

                return session.createQuery(hqlCommand.toString())//
                    .setString("apiKey", apiKey)//
                    .setLong("thresholdDate", thresholdTime.getUnixTime())//
                    .uniqueResult();

            }

        });

        return numOfRequests;
    }
    
    public Collection<Channel> getChannelsForSeatNumber(final long seatNumber) throws DataAccessException {

        Collection<Channel> list = (Collection<Channel>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("WHERE ")
                        .append("   ra.seatNumber = :seatNumber ");

                Query query = session.createQuery(hqlCommand.toString())//
                        .setLong("seatNumber", seatNumber);
                return query.list();
            }
        });

        return (list == null) ? new HashSet<Channel>() : list;
    }
    
    public Collection<Channel> getChannelsForDynamicSeats() throws DataAccessException {

        Collection<Channel> list = (Collection<Channel>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()
                        .append("SELECT DISTINCT ra ")
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ra ")
                        .append("WHERE ")
                        //Ability to select unbound channels (i.e. no pre-allocated seat)
                        .append("   ra.seatNumber IS NULL ");

                Query query = session.createQuery(hqlCommand.toString());
                return query.list();
            }
        });

        return (list == null) ? new HashSet<Channel>() : list;
    }
    
    public Long getNextFreeSeatNumber(final long seatNumberStart) throws DataAccessException {
        Collection<Long> seats = (Collection<Long>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        
                        .append("SELECT ca.seatNumber+1 ")//
                        .append("FROM ").append(Channel.class.getCanonicalName()).append(" ca ")//
                        .append("WHERE ")//
                        .append("       ca.seatNumber >= :seatNumberStart ")//
                        .append("       AND NOT EXISTS ( ")//
                        .append("           SELECT cb.seatNumber ")//
                        .append("           FROM ").append(Channel.class.getCanonicalName()).append(" cb ")//
                        .append("           WHERE ca.seatNumber+1 = cb.seatNumber" )//
                        .append("       ) " )//
                        .append("ORDER BY ca.seatNumber ASC ");

                Query query = session.createQuery(hqlCommand.toString());
                query.setLong("seatNumberStart", seatNumberStart);
                //Unique result
                //TODO: remove the joins to issue a real LIMIT in SQL and make it a lot faster
                query.setFirstResult(0);
                query.setMaxResults(1);
                /*DB paging-only FIX*///Hibernate is sometimes insane; it cannot add the proper LIMIT SQL statemennt in such a query: it will first get everything in MEMORY(!) and then filter out, madness!
                return query.list();
            }
        });
        if (seats!=null && !seats.isEmpty()) {
            Iterator<Long> i = seats.iterator();
            //It doesn't matter if the set is not sorted, setFirstResult() and setMaxResults() will do the job
            if (i.hasNext()) return i.next();
        }
        return null;
    }
}
