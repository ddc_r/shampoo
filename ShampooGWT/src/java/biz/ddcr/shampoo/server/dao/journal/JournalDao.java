package biz.ddcr.shampoo.server.dao.journal;

import biz.ddcr.shampoo.client.form.journal.LogForm.LOG_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 *
 * @author okay_awright
 **/
public class JournalDao extends GenericHibernateDao {

    public void addLog(Log log) throws DataAccessException {
        if (log != null) {
            getTemplate().save(log);
        }
    }

    public void addBatchLog(Collection<? extends Log> logs) throws DataAccessException {
        if (logs != null) {
            this.batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    logs);
        }
    }

    public Log getLog(final String id) throws DataAccessException {
        return getTemplate().get(Log.class, id);
    }

    public Collection<Log> getLogs(Collection<String> ids) throws DataAccessException {
        Collection<Log> logs = null;
        if (ids != null) {
            logs = new HashSet<Log>();
            for (String id : ids) {
                Log log = getTemplate().get(Log.class, id);
                if (log != null) {
                    logs.add(log);
                }
            }
        }
        return logs;
    }

    public Log loadLog(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Log) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT lo "//
                            + "FROM " + Log.class.getCanonicalName() + " lo "//
                            + "     LEFT JOIN FETCH lo.channels "//
                            + "WHERE "//
                            + "     lo.refID = :id ";//

                    Query query = session.createQuery(hqlCommand);
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    public void deleteLog(Log log) throws DataAccessException {
        if (log != null) {
            getTemplate().delete(log);
        }
    }

    public void deleteBatchLogs(Collection<Log> logs) throws DataAccessException {
        if (logs != null) {
            getTemplate().deleteAll(logs);
        }
    }

    public Collection<Log> getLogsWithRightsByChannelsForChannel(
            final Collection<String> channelIds,
            final GroupAndSort<LOG_TAGS> constraint,
            final String channelId) throws DataAccessException {
        if (channelId == null || channelIds == null || channelIds.isEmpty()) {
            return new HashSet<Log>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!channelIds.contains(channelId)) {
            return new HashSet<Log>();
        }

        return getLogsForChannel(constraint, channelId);
    }

    public Collection<Log> getLogsForChannel(final GroupAndSort<LOG_TAGS> constraint, final String channelId) throws DataAccessException {

        if (channelId == null) {
            return new HashSet<Log>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT lo ")//
                        .append("FROM ").append(Log.class.getCanonicalName()).append(" lo ")//
                        /*.append("     LEFT JOIN lo.channels loc ")//
                        .append("WHERE ")//
                        .append("     loc.label = :channelId ");//*/
                        .append("     JOIN lo.channels loc")//
                        .append("       WITH loc.label = :channelId ");//
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<LOG_TAGS>() {

                    @Override
                    public String[] onTag(LOG_TAGS currentTag) {
                        switch (currentTag) {
                            case actee:
                                return new String[]{"lo.acteeID"};
                            case action:
                                return new String[]{"lo.action"};
                            case actor:
                                return new String[]{"lo.actorID"};
                            case time:
                                return new String[]{"lo.logTimestamp"};
                            case type:
                                return new String[]{"lo.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("channelId", channelId);
                    return query.scroll();
                } else {
                    return new HashSet<Log>();
                }
            }
        });

        return (Collection<Log>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Log> getLogsWithRightsByChannels(
            final Collection<String> channelIds,
            final GroupAndSort<LOG_TAGS> constraint) throws DataAccessException {
        if (channelIds != null && channelIds.isEmpty()) {
            return new HashSet<Log>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT lo ")//
                        .append("FROM ").append(Log.class.getCanonicalName()).append(" lo ")//
                        /*.append("     LEFT JOIN lo.channels loc ");//
                if (channelIds != null) {
                    hqlCommand.append("WHERE ")//
                            .append("     loc.label IN (:channelIds) ");//
                }*/
                        ;
                if (channelIds != null) {
                    hqlCommand.append("     JOIN lo.channels loc")//
                            .append("           WITH loc.label IN (:channelIds) ");//
                }
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<LOG_TAGS>() {

                    @Override
                    public String[] onTag(LOG_TAGS currentTag) {
                        switch (currentTag) {
                            case actee:
                                return new String[]{"lo.acteeID"};
                            case action:
                                return new String[]{"lo.action"};
                            case actor:
                                return new String[]{"lo.actorID"};
                            case time:
                                return new String[]{"lo.logTimestamp"};
                            case type:
                                return new String[]{"lo.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (channelIds != null) {
                        query.setParameterList("channelIds", channelIds);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Log>();
                }
            }
        });

        return (Collection<Log>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Log> getAllLogs(final GroupAndSort<LOG_TAGS> constraint) throws DataAccessException {
        return getLogsWithRightsByChannels(null, constraint);
    }

    /** HQL delete doesn't provide many-to-many cascades. Better use ORM facilities so as to drop foreign keys references too, the problem is that it's not appropriate for huge deletions
     It's an EJB3 limitation: cannot use purgeLogs() anymore because of foreign key constraints not respected: a Delete HQL statement does not handle foreign keys
     and a many-to-many directive cannot use on-delete="cascade" so it's a no-go, unless you're willing to rewrite the many-to-many into many-to-ones
     As a result, the request is abysmally slow
    @deprecated
     **/
    public void purgeJournals(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) {
        if (expirationDate != null) {
            getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("DELETE ").append(Log.class.getCanonicalName()).append(" lo ")//
                            .append("WHERE ")//
                            .append("     lo.logTimestamp >= :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    return query.executeUpdate();
                }
            });
        }
    }

    public Collection<Log> getAllLogsAfterExpiration(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        Collection<Log> list = null;
        if (expirationDate != null) {
            list = (Collection<Log>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT lo ")//
                            .append("FROM ").append(Log.class.getCanonicalName()).append(" lo ")//
                            .append("WHERE ")//
                            .append("     lo.logTimestamp < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    return query.list();
                }
            });
        }
        return (list == null) ? new HashSet<Log>() : list;
    }
}
