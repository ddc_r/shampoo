package biz.ddcr.shampoo.server.dao.notification;

import biz.ddcr.shampoo.client.form.notification.NotificationForm.NOTIFICATION_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 *
 * @author okay_awright
 **/
public class NotificationDao extends GenericHibernateDao {

    public void addNotification(Notification message) throws DataAccessException {
        if (message != null) {
            getTemplate().save(message);
        }
    }

    public void addBatchNotification(Collection<? extends Notification> messages) throws DataAccessException {
        if (messages != null) {
            this.batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    messages);
        }
    }

    public Notification getNotification(final String id) throws DataAccessException {
        return getTemplate().get(Notification.class, id);
    }

    public Collection<Notification> getNotifications(Collection<String> ids) throws DataAccessException {
        Collection<Notification> messages = null;
        if (ids != null) {
            messages = new HashSet<Notification>();
            for (String id : ids) {
                Notification message = getTemplate().get(Notification.class, id);
                if (message != null) {
                    messages.add(message);
                }
            }
        }
        return messages;
    }

    public Notification loadNotification(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Notification) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT no "//
                            + "FROM " + Notification.class.getCanonicalName() + " no "//
                            + "     LEFT JOIN FETCH no.recipient "//
                            + "WHERE "//
                            + "     no.refID = :id ";//

                    Query query = session.createQuery(hqlCommand);
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    public void deleteNotification(Notification message) throws DataAccessException {
        if (message != null) {
            getTemplate().delete(message);
        }
    }

    public void deleteBatchNotifications(Collection<Notification> messages) throws DataAccessException {
        if (messages != null) {
            getTemplate().deleteAll(messages);
        }
    }

    public Collection<Notification> getNotificationsForRestrictedUser(final String restrictedUserName, final GroupAndSort<NOTIFICATION_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT no ")//
                        .append("FROM ").append(Notification.class.getCanonicalName()).append(" no ")//
                        //.append("     LEFT JOIN no.recipient nor ");
                        ;
                if (restrictedUserName != null) {
                    hqlCommand.append("WHERE ")//
                            //.append("     nor.username = :restricteduserid ");
                            .append("     no.recipient.username = :restricteduserid ");
                }

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<NOTIFICATION_TAGS>() {

                    @Override
                    public String[] onTag(NOTIFICATION_TAGS currentTag) {
                        switch (currentTag) {
                            case time:
                                return new String[]{"no.logTimestamp"};
                            case message:
                                return new String[]{"no.class"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (restrictedUserName != null) {
                        query.setString("restricteduserid", restrictedUserName);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Notification>();
                }
            }
        });

        return (Collection<Notification>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    /**
     * Dumb Hibernate/JTA limitation (or bug?): DON'T use scrollsets in schedulers because of concurrent accesses with multiple tasks.
     * @return
     * @throws DataAccessException
     */
    public Collection<Notification> getAllUnseenNotificationsForOptedRestrictedUsersOnly() throws DataAccessException {

        Collection<Notification> list = (Collection<Notification>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT no ")//
                        .append("FROM ").append(Notification.class.getCanonicalName()).append(" no ")//
                        //.append("     LEFT JOIN no.recipient nor ")//
                        .append("WHERE ")//
                        //.append("     nor.emailNotification = true ")//
                        .append("     no.recipient.emailNotification = true ")//
                        .append("AND ")//
                        .append("     no.recipientNotified = false ")//
                        ;/*.append("ORDER BY ")//
                        .append("     no.logDate DESC, no.logMillisecond DESC, no.action ASC, no.actorID ASC");*/

                Query query = session.createQuery(hqlCommand.toString());
                return query.list();
            }
        });
        return (list == null) ? new HashSet<Notification>() : list;

    }

    public Collection<Notification> getAllNotifications(final GroupAndSort<NOTIFICATION_TAGS> constraint) throws DataAccessException {
        return getNotificationsForRestrictedUser(null, constraint);
    }

    /** HQL delete doesn't provide many-to-many cascades. Better use ORM facilities so as to drop foreign keys references too, the problem is that it's not appropriate for huge deletions
     It's an EJB3 limitation: cannot use purgeLogs() anymore because of foreign key constraints not respected: a Delete HQL statement does not handle foreign keys
     and a many-to-many directive cannot use on-delete="cascade" so it's a no-go, unless you're willing to rewrite the many-to-many into many-to-ones
     As a result, the request is abysmally slow
     **/
    public void purgeNotifications(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) {
        if (expirationDate != null) {
            getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("DELETE ").append(Notification.class.getCanonicalName()).append(" no ")//
                            .append("WHERE ")//
                            .append("     no.logTimestamp < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    return query.executeUpdate();
                }
            });
        }
    }
    public Collection<Notification> getAllNotificationsBeforeTimeThreshold(final YearMonthWeekDayHourMinuteSecondMillisecond expirationDate) throws DataAccessException {
        Collection<Notification> list = null;
        if (expirationDate != null) {
            list = (Collection<Notification>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT no ")//
                            .append("FROM ").append(Notification.class.getCanonicalName()).append(" no ")//
                            .append("WHERE ")//
                            .append("     no.logTimestamp < :expiryTimestamp ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setLong("expiryTimestamp", expirationDate.getUnixTime());
                    return query.list();
                }
            });
        }
        return (list == null) ? new HashSet<Notification>() : list;
    }

    /**
     * Much, much faster than using pure ORM mechanisms for such a simple task
     */
    public void updateAllNotificationUnseenFlags() {
        getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("UPDATE ").append(Notification.class.getCanonicalName()).append(" no ")//
                        .append("SET ")//
                        .append("     no.recipientNotified = true ")//
                        .append("WHERE ")//
                        .append("     no.recipientNotified = false");

                Query query = session.createQuery(hqlCommand.toString());
                return query.executeUpdate();
            }
        });
    }
}
