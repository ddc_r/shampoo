package biz.ddcr.shampoo.server.dao.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 *
 * @author okay_awright
 *
 */
public class ProgrammeDao extends GenericHibernateDao {

    public void addProgramme(Programme programme) throws DataAccessException {
        if (programme != null) {
            getTemplate().save(programme);
        }
    }

    public void addPlaylist(Playlist playlist) throws DataAccessException {
        if (playlist != null) {
            /*
             * Serializable newId =
             */ getTemplate().save(playlist);
            //return ((Long) newId).longValue();
        }
        //return -1;
    }

    /**
     * * TODO: getTemplate().getSessionFactory().getCurrentSession() hurls that
     * no session is bound when used within an UnmanagedTransactionUnit, WTF!!!
     * The transactionManager is correctly bound and a transaction is meant to
     * be open in the FacadeService via annotation
     *
     * @param tracks
     * @throws DataAccessException
     */
    public void addBatchPlaylists(final Collection<Playlist> playlists) throws DataAccessException {
        if (playlists != null) {

            batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    playlists);
        }
    }

    public void updateProgramme(Programme programme) throws DataAccessException {
        if (programme != null) {
            getTemplate().update(programme);
        }
    }

    public void updateBatchProgrammes(Collection<Programme> programmes) throws DataAccessException {
        if (programmes != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    programmes);
        }
    }

    public void updatePlaylist(Playlist playlist) throws DataAccessException {
        if (playlist != null) {
            getTemplate().update(playlist);
        }
    }

    public void updateBatchPlaylists(Collection<Playlist> playlists) throws DataAccessException {
        if (playlists != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    playlists);
        }
    }

    public Programme getProgramme(final String id) throws DataAccessException {
        return getTemplate().get(Programme.class, id);
    }

    public Collection<Programme> getProgrammes(Collection<String> ids) throws DataAccessException {
        Collection<Programme> programmes = null;
        if (ids != null) {
            programmes = new HashSet<Programme>();
            for (String id : ids) {
                Programme programme = getTemplate().get(Programme.class, id);
                if (programme != null) {
                    programmes.add(programme);
                }
            }
        }
        return programmes;
    }

    public Programme loadProgramme(final String id) throws DataAccessException {
        return loadProgramme(id, false);
    }
    public Programme loadProgramme(final String id, final boolean prefetchedTracks) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Programme) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT pr ")//
                            .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ");//
                            /**
                             * Hibernate BUG: broken proxy hydration with JOIN
                             * and inheritance*
                             */
                            //FIXED: dropped inheritance on trackprogramme, now handled by the business layer
                            if (prefetchedTracks) {
                                hqlCommand
                                    .append(" LEFT JOIN FETCH pr.trackProgrammes trpr ")//
                                    .append("     LEFT JOIN FETCH trpr.location.track ");//
                            }
                            hqlCommand
                                .append(" LEFT JOIN FETCH pr.channels ")//
                                .append(" LEFT JOIN FETCH pr.animatorRights ")//
                                .append(" LEFT JOIN FETCH pr.curatorRights ")//
                                .append(" LEFT JOIN FETCH pr.contributorRights ")//
                                .append(" LEFT JOIN FETCH pr.playlists ")//
                                .append(" LEFT JOIN FETCH pr.timetableSlots ")//
                                .append("WHERE ")//
                                .append("     pr.label = :id ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    public Collection<Programme> loadProgrammes(final Collection<String> ids) throws DataAccessException {
        return loadProgrammes(ids, false);
    }
    public Collection<Programme> loadProgrammes(final Collection<String> ids, final boolean prefetchedTracks) throws DataAccessException {
        if (ids == null || ids.isEmpty()) {
            return new HashSet<Programme>();
        } else {
            Collection<Programme> list = (Collection<Programme>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT pr ")//
                            .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ");//
                            if (prefetchedTracks) {
                                hqlCommand
                                    .append(" LEFT JOIN FETCH pr.trackProgrammes trpr ")//
                                    .append("     LEFT JOIN FETCH trpr.location.track ");//
                            }
                            hqlCommand//
                                .append("   LEFT JOIN FETCH pr.channels ")//
                                .append("   LEFT JOIN FETCH pr.animatorRights ")//
                                .append("   LEFT JOIN FETCH pr.curatorRights ")//
                                .append("   LEFT JOIN FETCH pr.contributorRights ")//
                                .append("   LEFT JOIN FETCH pr.playlists ")//
                                .append("   LEFT JOIN FETCH pr.timetableSlots ")//
                                .append("WHERE ")//
                                .append("     pr.label IN (:ids) ");
                    Query query = session.createQuery(hqlCommand.toString());
                    query.setParameterList("ids", ids);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<Programme>() : list;
        }
    }
    
    public Playlist getPlaylist(final String id) throws DataAccessException {
        return getTemplate().get(Playlist.class, id);
    }

    public Collection<Playlist> getPlaylists(Collection<String> ids) throws DataAccessException {
        Collection<Playlist> playlists = null;
        if (ids != null) {
            playlists = new HashSet<Playlist>();
            for (String id : ids) {
                Playlist playlist = getTemplate().get(Playlist.class, id);
                if (playlist != null) {
                    playlists.add(playlist);
                }
            }
        }
        return playlists;
    }

    public Playlist loadPlaylist(final String id) throws DataAccessException {
        return (Playlist) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                String hqlCommand =
                        "SELECT pl "
                        + "FROM " + Playlist.class.getCanonicalName() + " pl "
                        + "     LEFT JOIN FETCH pl.playlistEntries "
                        + "     LEFT JOIN FETCH pl.timetableSlots "
                        + "     LEFT JOIN FETCH pl.programme "
                        + "WHERE "
                        + "     pl.refID = :id ";

                Query query = session.createQuery(hqlCommand);
                query.setString("id", id);
                return query.uniqueResult();
            }
        });
    }

    public void deleteProgramme(Programme programme) throws DataAccessException {
        if (programme != null) {
            getTemplate().delete(programme);
        }
    }

    public void deleteBatchProgrammes(Collection<Programme> programmes) throws DataAccessException {
        if (programmes != null) {
            getTemplate().deleteAll(programmes);
        }
    }

    public void deletePlaylist(Playlist playlist) throws DataAccessException {
        if (playlist != null) {
            getTemplate().delete(playlist);
        }
    }

    public void deleteBatchPlaylists(Collection<Playlist> playlists) throws DataAccessException {
        if (playlists != null) {
            getTemplate().deleteAll(playlists);
        }
    }

    public void deletePlaylistEntry(PlaylistEntry playlistEntry) throws DataAccessException {
        if (playlistEntry != null) {
            getTemplate().delete(playlistEntry);
        }
    }

    public void deleteBatchPlaylistEntries(Collection<PlaylistEntry> playlistEntries) throws DataAccessException {
        if (playlistEntries != null) {
            getTemplate().deleteAll(playlistEntries);
        }
    }

    public Collection<Playlist> loadPlaylists(final Collection<String> ids) throws DataAccessException {
        if (ids == null || ids.isEmpty()) {
            return new HashSet<Playlist>();
        } else {
            Collection<Playlist> list = (Collection<Playlist>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT pl ")//
                            .append("FROM ").append(Playlist.class.getCanonicalName()).append(" pl ")//
                            .append("     LEFT JOIN FETCH pl.playlistEntries ")//
                            .append("     LEFT JOIN FETCH pl.timetableSlots ")//
                            .append("     LEFT JOIN FETCH pl.programme ")//
                            .append("WHERE ")//
                            .append("     pl.refID IN (:refids) ");
                    Query query = session.createQuery(hqlCommand.toString());
                    query.setParameterList("refids", ids);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<Playlist>() : list;
        }
    }

    /**
     * Find all Programmes that are linked to the specified rights Results may
     * be narrowed to a specific channel if necessary This query should be
     * cached since it is complex and ubiquitous
     *
     * @param channels
     * @return
     * @throws DataAccessException
     *
     */
    public Collection<Programme> getProgrammesWithRightsByProgrammesAndOrphans(final Collection<String> programmeIds, final GroupAndSort<PROGRAMME_TAGS> constraint, final String currentlyAuthenticatedUserName) throws DataAccessException {
        return getProgrammesWithRightsByProgrammesForChannelAndOrphans(programmeIds, constraint, null, currentlyAuthenticatedUserName, true);
    }

    public Collection<Programme> getProgrammesWithRightsByProgrammesForChannel(final Collection<String> programmeIds, final GroupAndSort<PROGRAMME_TAGS> constraint, final String channelId) throws DataAccessException {
        return getProgrammesWithRightsByProgrammesForChannelAndOrphans(programmeIds, constraint, null, null, false);
    }

    public Collection<Programme> getProgrammesWithRightsByProgrammesForChannelAndOrphans(final Collection<String> programmeIds, final GroupAndSort<PROGRAMME_TAGS> constraint, final String channelId, final String currentlyAuthenticatedUserName) throws DataAccessException {
        return getProgrammesWithRightsByProgrammesForChannelAndOrphans(programmeIds, constraint, channelId, currentlyAuthenticatedUserName, true);
    }

    protected Collection<Programme> getProgrammesWithRightsByProgrammesForChannelAndOrphans(final Collection<String> programmeIds, final GroupAndSort<PROGRAMME_TAGS> constraint, final String channelId, final String currentlyAuthenticatedUserName, final boolean fetchOrphans) throws DataAccessException {

        if ((programmeIds != null && !programmeIds.isEmpty()) || (channelId != null && channelId.length() != 0) || fetchOrphans) {
            ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr ")//, ra, ar, cur, cor ")
                            .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ")//
                            .append("     LEFT JOIN pr.channels ra ")//
                            .append("     LEFT JOIN pr.animatorRights ar ")//
                            .append("     LEFT JOIN pr.curatorRights cur ")//
                            .append("     LEFT JOIN pr.contributorRights cor ")//
                            .append("WHERE ");
                    if (programmeIds != null && !programmeIds.isEmpty()) {
                        hqlCommand//
                                .append("   ( pr.label IN (:programmeIds) ");
                        if (channelId != null && channelId.length() != 0) {
                            hqlCommand//
                                    .append(" AND ra.label = :channelId ");
                        }
                        hqlCommand//
                                .append(" ) ");
                        if (fetchOrphans) {
                            hqlCommand.append(" OR ");
                        }
                    } else if (channelId != null && channelId.length() != 0) {
                        hqlCommand//
                                .append("  ra.label = :channelId ");
                        if (fetchOrphans) {
                            hqlCommand.append(" OR ");
                        }
                    }
                    if (fetchOrphans) {
                        hqlCommand//
                                .append("  ( ra IS NULL ");
                        if (currentlyAuthenticatedUserName != null) {
                            hqlCommand//
                                    .append("     AND (   (pr.creator IS NULL AND pr.latestEditor IS NULL) ")//
                                    .append("         OR (pr.creator.username = :restrictedUserId) ")//
                                    .append("         OR (pr.latestEditor.username = :restrictedUserId) ")//
                                    .append("       ) ");
                        }
                        hqlCommand//
                                .append("   ) ");
                    }

                    Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PROGRAMME_TAGS>() {

                        @Override
                        public String[] onTag(PROGRAMME_TAGS currentTag) {
                            switch (currentTag) {
                                case description:
                                    return new String[]{"pr.description"};
                                case label:
                                    return new String[]{"pr.label"};
                                case rights:
                                    return new String[]{"ar.username", "cur.username", "cor.username"};
                                case channels:
                                    return new String[]{"ra.label"};
                                default:
                                    return null;
                            }
                        }
                    });

                    if (query != null) {
                        if (programmeIds != null && !programmeIds.isEmpty()) {
                            query.setParameterList("programmeIds", programmeIds);
                        }
                        if (channelId != null && channelId.length() != 0) {
                            query.setString("channelId", channelId);
                        }
                        if (currentlyAuthenticatedUserName != null) {
                            query.setString("restrictedUserId", currentlyAuthenticatedUserName);
                        }
                        return query.scroll();
                    } else {
                        return new HashSet<Programme>();
                    }
                }
            });

            return (Collection<Programme>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

                @Override
                public GenericEntityInterface onRow(ScrollableResults inputRow) {
                    return (GenericEntityInterface) inputRow.get(0);
                }
            });
        } else {
            return new HashSet<Programme>();
        }
    }

    public Collection<Programme> getProgrammesWithRightsByProgrammesForTrack(final Collection<String> ProgrammeIds, final GroupAndSort<PROGRAMME_TAGS> constraint, final String trackId) throws DataAccessException {
        if (ProgrammeIds == null || ProgrammeIds.isEmpty()) {
            return new HashSet<Programme>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr ")//, ra, ar, cur, cor ")
                        .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ")//
                        .append("     LEFT JOIN pr.channels ra ")//
                        .append("     LEFT JOIN pr.animatorRights ar ")//
                        .append("     LEFT JOIN pr.curatorRights cur ")//
                        .append("     LEFT JOIN pr.contributorRights cor ")//
                        /*
                         * .append(" LEFT JOIN pr.trackProgrammes trpr ")//
                         * .append(" LEFT JOIN trpr.location trprl ")//
                         * .append(" LEFT JOIN trprl.track tr ")//
                         */;
                if (trackId != null) {
                    hqlCommand//
                            .append("     JOIN pr.trackProgrammes trpr ")//
                            .append("           JOIN trpr.location trprl ")//
                            .append("               JOIN trprl.track tr ")//
                            .append("                   WITH tr.refID = :trackId ");//
                }
                hqlCommand.append("WHERE ") /*
                         * .append((trackId != null ? " tr.refID = :trackId " +
                         * " AND " : ""))
                         */.append("     pr.label IN (:programmeIds) ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PROGRAMME_TAGS>() {

                    @Override
                    public String[] onTag(PROGRAMME_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pr.description"};
                            case label:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"ar.username", "cur.username", "cor.username"};
                            case channels:
                                return new String[]{"ra.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setParameterList("programmeIds", ProgrammeIds);
                    if (trackId != null) {
                        query.setString("trackId", trackId);
                    }
                    //TODO BUG: scroll() doesn't fetch all rows the generated SQL produce
                    return query.scroll();
                } else {
                    return new HashSet<Programme>();
                }
            }
        });

        return (Collection<Programme>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Playlist> getPlaylistsWithRightsByProgrammesForProgramme(
            final Collection<String> programmeIds,
            final GroupAndSort<PLAYLIST_TAGS> constraint,
            final String programmeId) throws DataAccessException {
        if (programmeId == null || programmeIds == null || programmeIds.isEmpty()) {
            return new HashSet<Playlist>();
        }

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (!programmeIds.contains(programmeId)) {
            return new HashSet<Playlist>();
        }

        return getPlaylistsForProgramme(constraint, programmeId);
    }

    public Collection<Programme> getAllProgrammesWithNoRightForCreator(final String restrictedUserId, final GroupAndSort<PROGRAMME_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr")//, ra ")
                        .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ").append("     LEFT JOIN pr.animatorRights ar ").append("     LEFT JOIN pr.curatorRights cur ").append("     LEFT JOIN pr.contributorRights cor ").append("     LEFT JOIN pr.channels ra ").append("WHERE (").append("   ar IS NULL AND ").append("   cur IS NULL AND ").append("   cor IS NULL ").append(") ").append((restrictedUserId != null
                        ? " AND pr.creator.username = :restrictedUserId " : ""));
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PROGRAMME_TAGS>() {

                    @Override
                    public String[] onTag(PROGRAMME_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pr.description"};
                            case label:
                                return new String[]{"pr.label"};
                            case channels:
                                return new String[]{"ra.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (restrictedUserId != null) {
                        query.setString("restrictedUserId", restrictedUserId);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<Programme>();
                }
            }
        });

        return (Collection<Programme>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Programme> getProgrammesForChannel(final GroupAndSort<PROGRAMME_TAGS> constraint, final String channelId) throws DataAccessException {

        if (channelId == null) {
            return new HashSet<Programme>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr ")//, ra, ar, cur, cor ")//, ra, ar, cur, cor ")
                        .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ") /*
                         * .append(" LEFT JOIN pr.channels ra ")
                         */.append("     LEFT JOIN pr.animatorRights ar ").append("     LEFT JOIN pr.curatorRights cur ").append("     LEFT JOIN pr.contributorRights cor ").append("     JOIN pr.channels ra ")//
                        .append("           WITH ra.label = :channelId ");//
                        /*
                 * .append("WHERE ") .append(" ra.label = :channelId ");
                 */
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PROGRAMME_TAGS>() {

                    @Override
                    public String[] onTag(PROGRAMME_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pr.description"};
                            case label:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"ar.username", "cur.username", "cor.username"};
                            case channels:
                                return new String[]{"ra.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("channelId", channelId);
                    return query.scroll();
                } else {
                    return new HashSet<Programme>();
                }
            }
        });

        return (Collection<Programme>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Programme> getProgrammesForTrack(final GroupAndSort<PROGRAMME_TAGS> constraint, final String trackId) throws DataAccessException {

        if (trackId == null) {
            return new HashSet<Programme>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr ")//, ra, ar, cur, cor ")
                        .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ").append("     LEFT JOIN pr.channels ra ").append("     LEFT JOIN pr.animatorRights ar ").append("     LEFT JOIN pr.curatorRights cur ").append("     LEFT JOIN pr.contributorRights cor ") /*
                         * .append(" LEFT JOIN pr.trackProgrammes trpr ")//
                         * .append(" LEFT JOIN trpr.location trprl ")//
                         * .append(" LEFT JOIN trprl.track tr ")//
                         * .append("WHERE ") .append(" tr.refID = :trackId ");
                         */.append("     JOIN pr.trackProgrammes trpr ")//
                        .append("           JOIN trpr.location trprl ")//
                        .append("               JOIN trprl.track tr ")//
                        .append("                   WITH tr.refID = :trackId ");//
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PROGRAMME_TAGS>() {

                    @Override
                    public String[] onTag(PROGRAMME_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pr.description"};
                            case label:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"ar.username", "cur.username", "cor.username"};
                            case channels:
                                return new String[]{"ra.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("trackId", trackId);
                    return query.scroll();
                } else {
                    return new HashSet<Programme>();
                }
            }
        });

        return (Collection<Programme>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Programme> getProgrammesWithRightsByProgrammesForTrackAndProgrammes(final Class<? extends BroadcastableTrack> clazz, final Collection<String> userProgrammeIds, final String author, String title, final Collection<String> intersectProgrammeIds) throws DataAccessException {
        if (userProgrammeIds == null || userProgrammeIds.isEmpty()) {
            return new HashSet<Programme>();
        }
        userProgrammeIds.retainAll(intersectProgrammeIds);
        return getProgrammesForTrackAndProgrammes(clazz, author, title, userProgrammeIds);
    }

    public Collection<Programme> getProgrammesForTrackAndProgrammes(final Class<? extends BroadcastableTrack> clazz, final String author, final String title, final Collection<String> programmeIds) throws DataAccessException {

        if (programmeIds == null || programmeIds.isEmpty()) {
            return new HashSet<Programme>();
        }

        Collection<Programme> list = (Collection<Programme>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr ").append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ").append("     JOIN pr.trackProgrammes trpr ")//
                        .append("           JOIN trpr.location trprl ")//
                        .append("               JOIN trprl.track tr ")//
                        .append("                   WITH tr.title = :title ") //.append("           WITH tr.class = " + clazz.getCanonicalName() + " ")
                        .append("WHERE ").append("     tr.author = :author  ") //Hibernate BUG no joins and WITH clause with anything else than scalar or strings
                        .append("     AND tr.class = ").append(clazz.getCanonicalName()).append(" ") //.append("     AND tr.title = :title  ")
                        .append("     AND pr.label IN (:programmeIds) ");
                Query query = session.createQuery(hqlCommand.toString());
                query.setString("author", author);
                query.setString("title", title);
                query.setParameterList("programmeIds", programmeIds);
                return query.list();
            }
        });
        return (list == null) ? new HashSet<Programme>() : list;

    }

    public Collection<Programme> getAllProgrammes(final GroupAndSort<PROGRAMME_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr ")//, ra, ar, cur, cor ")
                        .append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ").append("     LEFT JOIN pr.channels ra ").append("     LEFT JOIN pr.animatorRights ar ").append("     LEFT JOIN pr.curatorRights cur ").append("     LEFT JOIN pr.contributorRights cor ");
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PROGRAMME_TAGS>() {

                    @Override
                    public String[] onTag(PROGRAMME_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pr.description"};
                            case label:
                                return new String[]{"pr.label"};
                            case rights:
                                return new String[]{"ar.username", "cur.username", "cor.username"};
                            case channels:
                                return new String[]{"ra.label"};
                            default:
                                return null;
                        }
                    }
                });

                return query != null ? query.scroll() : new HashSet<Programme>();
            }
        });

        return (Collection<Programme>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Playlist> getPlaylistsForProgramme(final GroupAndSort<PLAYLIST_TAGS> constraint, final String programmeId) throws DataAccessException {

        if (programmeId == null) {
            return new HashSet<Playlist>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pl ")//, tts, pr ")
                        .append("FROM ").append(Playlist.class.getCanonicalName()).append(" pl ") /*
                         * .append(" LEFT JOIN FETCH pl.playlistEntries ")
                         * .append(" LEFT JOIN FETCH ple.filters ")
                         */.append("     LEFT JOIN pl.timetableSlots tts ") //.append("     LEFT JOIN pl.programme pr ")
                        .append("     JOIN pl.programme pr ").append("           WITH pr.label = :programmeId ");
                //.append("     LEFT JOIN pl.live li ")
                        /*
                 * .append("WHERE ") .append(" pr.label = :programmeId ");
                 */
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PLAYLIST_TAGS>() {

                    @Override
                    public String[] onTag(PLAYLIST_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pl.description"};
                            case label:
                                return new String[]{"pl.label"};
                            /*
                             * case live: return new String[]{"li.broadcaster",
                             * "li.login"};
                             */
                            case live:
                                return new String[]{"pl.live.broadcaster", "pl.live.login"};
                            case timetableSlots:
                                return new String[]{"tts.location.channel", "tts.location.startTimestamp"};
                            case programme:
                                return new String[]{"pr.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    query.setString("programmeId", programmeId);
                    return query.scroll();
                } else {
                    return new HashSet<Playlist>();
                }
            }
        });

        return (Collection<Playlist>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Playlist> getAllPlaylists(final GroupAndSort<PLAYLIST_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pl ")//, tts ")
                        .append("FROM ").append(Playlist.class.getCanonicalName()).append(" pl ") /*
                         * .append(" LEFT JOIN pl.playlistEntries ple ")
                         * .append(" LEFT JOIN ple.filters ")
                         */.append("     LEFT JOIN pl.timetableSlots tts ");
                /*
                 * .append(" LEFT JOIN pl.programme pr ") .append(" LEFT JOIN
                 * pl.live li ");
                 */
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PLAYLIST_TAGS>() {

                    @Override
                    public String[] onTag(PLAYLIST_TAGS currentTag) {
                        switch (currentTag) {
                            case description:
                                return new String[]{"pl.description"};
                            case label:
                                return new String[]{"pl.label"};
                            /*
                             * case live: return new String[]{"li.broadcaster",
                             * "li.login"};
                             */
                            case live:
                                return new String[]{"pl.live.broadcaster", "pl.live.login"};
                            case timetableSlots:
                                return new String[]{"tts.location.channel", "tts.location.startTimestamp"};
                            /*
                             * case programme: return new String[]{"pr.label"};
                             */
                            case programme:
                                return new String[]{"pl.programme.label"};
                            default:
                                return null;
                        }
                    }
                });

                return query != null ? query.scroll() : new HashSet<Playlist>();
            }
        });

        return (Collection<Playlist>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Programme> getAllProgrammesWithNoRight(final GroupAndSort<PROGRAMME_TAGS> constraint) throws DataAccessException {
        return getAllProgrammesWithNoRightForCreator(null, constraint);
    }

    public Collection<String> getLinkedProgrammeIdsByRestrictedUser(final String username, final RightsBundle rightsBundle) {

        if (username == null || rightsBundle == null || !rightsBundle.isRightsSelected()) {
            return new HashSet<String>();
        }

        Collection<String> list = (Collection<String>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT pr.label ").append("FROM ").append(Programme.class.getCanonicalName()).append(" pr ").append("WHERE ").append(" (").append((rightsBundle.isChannelAdministratorRights()
                        ? "     pr.channels.channelAdministratorRights.username = :username " : ""))//"     rar IS NOT NULL " : "")
                        .append((rightsBundle.isChannelAdministratorRights() && rightsBundle.isProgrammeManagerRights()
                        ? " OR " : "")).append((rightsBundle.isProgrammeManagerRights()
                        ? "     pr.channels.programmeManagerRights.username = :username " : ""))//"     pmr IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights()) && rightsBundle.isListenerRights()
                        ? " OR " : "")).append((rightsBundle.isListenerRights()
                        ? "     pr.channels.listenerRights.username = :username " : ""))//"     lr IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights()) && rightsBundle.isAnimatorRights()
                        ? " OR " : "")).append((rightsBundle.isAnimatorRights()
                        ? "     pr.animatorRights.username = :username " : ""))//"     ar IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights() || rightsBundle.isAnimatorRights()) && rightsBundle.isCuratorRights()
                        ? " OR " : "")).append((rightsBundle.isCuratorRights()
                        ? "     pr.curatorRights.username = :username " : ""))//"     cur IS NOT NULL " : "")
                        .append(((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights() || rightsBundle.isAnimatorRights() || rightsBundle.isCuratorRights()) && rightsBundle.isContributorRights()
                        ? " OR " : "")).append((rightsBundle.isContributorRights()
                        ? "     pr.contributorRights.username = :username " : ""))//"     cor IS NOT NULL " : "")
                        .append(") OR ").append("( ").append("     pr.channels.channelAdministratorRights IS NULL AND ").append("     pr.channels.programmeManagerRights IS NULL AND ").append("     pr.channels.listenerRights IS NULL AND ").append("     pr.animatorRights IS NULL AND ").append("     pr.curatorRights IS NULL AND ").append("     pr.contributorRights IS NULL AND ").append("     ( ").append("         us.latestEditor IS NULL OR ").append("         us.latestEditor.username = :username ").append("     ) ").append(")");

                Query query = session.createQuery(hqlCommand.toString());
                query.setString("username", username);
                return query.list();
            }
        });

        return (list == null) ? new HashSet<String>() : list;
    }
}
