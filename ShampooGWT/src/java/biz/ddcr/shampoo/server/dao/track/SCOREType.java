/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.dao.track;

import biz.ddcr.shampoo.server.domain.track.SCORE;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class SCOREType implements UserType {

    private static final int[] SQL_TYPES = {Types.SMALLINT};

    @Override
    public int[] sqlTypes() {
        //TODO: check whether clone() fullfills its job
        return SQL_TYPES!=null ? SQL_TYPES.clone() : null;
    }

    @Override
    public Class returnedClass() {
        return SCORE.class;
    }

    @Override
    public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner) throws HibernateException, SQLException {
        Integer value = resultSet.getInt(names[0]);
        SCORE result = null;
        if (!resultSet.wasNull()) {
            result = SCORE.get(value.intValue());
        }
        return result;
    }

    @Override
    public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index) throws HibernateException, SQLException {
        if (null == value) {
            preparedStatement.setNull(index, Types.SMALLINT);
        } else {
            preparedStatement.setInt(index, ((SCORE)value).getNumericScore());
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException{
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
         return cached;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable)value;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }
    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if (x == y)
            return true;
        if (null == x || null == y)
            return false;
        return x.equals(y);
    }
}

