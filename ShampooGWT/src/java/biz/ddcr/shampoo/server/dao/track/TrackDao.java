package biz.ddcr.shampoo.server.dao.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PEGIRating;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.SORT;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE;
import biz.ddcr.shampoo.server.domain.track.filter.AlphabeticalFilter;
import biz.ddcr.shampoo.server.domain.track.filter.CATEGORY_FEATURE;
import biz.ddcr.shampoo.server.domain.track.filter.CategoryFilter;
import biz.ddcr.shampoo.server.domain.track.filter.FEATURE;
import biz.ddcr.shampoo.server.domain.track.filter.Filter;
import biz.ddcr.shampoo.server.domain.track.filter.FilterVisitor;
import biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE;
import biz.ddcr.shampoo.server.domain.track.filter.NumericalFilter;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 *
 * @author okay_awright
 **/
public class TrackDao extends GenericHibernateDao {

    //Code stink for Hibernate which doesn't automatically explode object properties for GROUP BY it uses in SELECT
    //Fragile code that will break very soon, I cannot even rely on reflection to automate the task since some fields are unused by Hibernate
    private static final String EXPLODED_BROADCASTTRACK_PROPERTIES =
            " tr.refID, tr.version, tr.creationDate, tr.creator, tr.latestModificationDate, tr.latestEditor, tr.author, tr.title, tr.ready, tr.album, tr.genre, tr.dateOfRelease, tr.description, tr.copyright, tr.tag, tr.rating.age, tr.rating.violence, tr.rating.profanity, tr.rating.fear, tr.rating.sex, tr.rating.drugs, tr.rating.discrimination, tr.trackContainer.format, tr.trackContainer.size, tr.trackContainer.duration, tr.trackContainer.bitrate, tr.trackContainer.samplerate, tr.trackContainer.channels, tr.trackContainer.vbr, tr.coverArtContainer.format, tr.coverArtContainer.size, tr.enabled, tr.class ";

    private interface XQLFilterFeatureTranslatorHandler<K extends FEATURE> {

        public String onFeature(K currentFilterFeature);
    }

    private interface XQLSortTranslatorHandler<K extends SORT> {

        public String[] onSort(K currentSort);
    }

    private interface FilterVisitorHQL extends FilterVisitor {

        public StringBuilder getHQLStatement();
    }

    private abstract class HQLParamWildcard<T> {

        private String wildcard;
        private T value;

        public T getValue() {
            return value;
        }

        public String getWildcard() {
            return wildcard;
        }

        HQLParamWildcard(String wildcard, T value) {
            this.wildcard = wildcard;
            this.value = value;
        }

        public abstract void bindParamToQuery(Query query);
    }

    public void addTrack(Track track) throws DataAccessException {
        if (track != null) {
            /*Serializable newId =*/ getTemplate().save(track);
            //return ((Long) newId).longValue();
        }
        //return null;
    }

    /**
     * * TODO: getTemplate().getSessionFactory().getCurrentSession() hurls that no session is bound when used within an UnmanagedTransactionUnit, WTF!!! The transactionManager is correctly bound and a transaction is meant to be open in the FacadeService via annotation
     * @param tracks
     * @throws DataAccessException
     */
    public void addBatchTracks(final Collection<? extends Track> tracks) throws DataAccessException {
        if (tracks != null) {

            /*try {
            SessionFactory sf = getTemplate().getSessionFactory();
            if (sf.isClosed())
            logger.warn("Session is close");
            } catch (Exception e) {
            logger.error(e);
            }

            getTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {

            try {
            Session s = SessionFactoryUtils.getSession(getSessionFactory(), false);
            logger.warn("Is there a session ready? : "+s+"and current session is "+session);
            } catch (Exception e) {
            logger.error(e);
            }
            try {
            logger.warn("Does factory has transactional sessions ready? : "+SessionFactoryUtils.hasTransactionalSession(getSessionFactory()));
            } catch (Exception e) {
            logger.error(e);
            }
            try {
            logger.warn("Session transactional? : "+SessionFactoryUtils.isSessionTransactional(session, getSessionFactory()));
            } catch (Exception e) {
            logger.error(e);
            }

            batchSave(
            session,
            tracks);
            return null;
            }
            });*/

            batchSave(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    tracks);
        }
    }

    public void updateTrack(Track track) throws DataAccessException {
        if (track != null) {
            getTemplate().update(track);
        }
    }

    /**
     * * TODO: getTemplate().getSessionFactory().getCurrentSession() hurls that no session is bound when used within an UnmanagedTransactionUnit, WTF!!! The transactionManager is correctly bound and a transaction is meant to be open in the FacadeService via annotation
     * @param tracks
     * @throws DataAccessException
     */
    public void updateBatchTracks(final Collection<? extends Track> tracks) throws DataAccessException {
        if (tracks != null) {

            /*getTemplate().execute(new HibernateCallback() {
            public Object doInHibernate(Session session) throws HibernateException {
            batchUpdate(
            session,
            tracks);
            return null;
            }
            });*/

            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    tracks);
        }
    }

    public void deleteTrack(Track track) throws DataAccessException {
        if (track != null) {
            getTemplate().delete(track);
        }
    }

    public void deleteBatchTracks(Collection<? extends Track> tracks) throws DataAccessException {
        if (tracks != null) {
            getTemplate().deleteAll(tracks);
        }
    }

    public void addVote(Voting vote) throws DataAccessException {
        if (vote != null) {
            getTemplate().save(vote);
        }
    }

    public void addRequest(Request request) throws DataAccessException {
        if (request != null) {
            getTemplate().save(request);
        }
    }

    public void updateVote(Voting vote) throws DataAccessException {
        if (vote != null) {
            getTemplate().update(vote);
        }
    }

    public void updateRequest(Request request) throws DataAccessException {
        if (request != null) {
            getTemplate().update(request);
        }
    }

    public void removeVote(Voting vote) throws DataAccessException {
        if (vote != null) {
            getTemplate().delete(vote);
        }
    }

    public void removeRequest(Request request) throws DataAccessException {
        if (request != null) {
            getTemplate().delete(request);
        }
    }

    public void addRating(PEGIRating rating) throws DataAccessException {
        if (rating != null) {
            getTemplate().save(rating);
        }
    }

    public void updateRating(PEGIRating rating) throws DataAccessException {
        if (rating != null) {
            getTemplate().update(rating);
        }
    }

    public void removeRating(PEGIRating rating) throws DataAccessException {
        if (rating != null) {
            getTemplate().delete(rating);
        }
    }

    public Track getTrack(final Class<? extends Track> clazz, final String id) throws DataAccessException {
        return (Track) getTemplate().load(clazz, id);
    }

    /** BUG Hibernate has problems coping up with the polymorphic trackProgrammes in HQL queries*/
    public Track loadTrack(final Class<? extends Track> clazz, final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (Track) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT tr ")//
                            .append("FROM ").append(clazz.getCanonicalName()).append(" tr ")//
                            //BUG: Hibernate confused if FETCH on: wrong proxied class fo trrackProgramme
                            .append("     LEFT JOIN FETCH tr.trackProgrammes trpr ")//
                            .append("           LEFT JOIN FETCH trpr.location.programme ")//
                            .append("WHERE ")//
                            .append("     tr.refID = :refid ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("refid", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    public Collection<Track> getTracks(final Class<? extends Track> clazz, Collection<String> ids) throws DataAccessException {
        Collection<Track> tracks = null;
        if (ids != null) {
            tracks = new HashSet<Track>();
            for (String id : ids) {
                Track track = (Track) getTemplate().load(clazz, id);
                if (track != null) {
                    tracks.add(track);
                }
            }
        }
        return tracks;
    }

    /** BUG Hibernate has problems coping up with the polymorphic trackProgrammes in HQL queries*/
    public Collection<Track> loadTracks(final Class<? extends Track> clazz, final Collection<String> ids) throws DataAccessException {
        if (ids == null || ids.isEmpty()) {
            return new HashSet<Track>();
        } else {
            Collection<Track> list = (Collection<Track>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT tr ")//
                            .append("FROM ").append(clazz.getCanonicalName()).append(" tr ")//
                            //BUG: Hibernate confused if FETCH on: wrong proxied class fo trrackProgramme
                            .append("     LEFT JOIN FETCH tr.trackProgrammes trpr ")//
                            .append("           LEFT JOIN FETCH trpr.location.programme ")//
                            .append("WHERE ")//
                            .append("     tr.refID IN (:refids) ");
                    Query query = session.createQuery(hqlCommand.toString());
                    query.setParameterList("refids", ids);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<Track>() : list;
        }
    }

    public Voting getUserVote(final String restrictedUserId, final String songId) throws DataAccessException {
        if (restrictedUserId == null || songId == null) {
            return null;
        } else {
            return (Voting) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT vo "
                            + "FROM " + Voting.class.getCanonicalName() + " vo "
                            + "WHERE "
                            + "     vo.location.user = :restrictedUserId "
                            + "     AND "
                            + "     vo.location.song = :songId ";
                    Query query = session.createQuery(hqlCommand);
                    query.setString("restrictedUserId", restrictedUserId);
                    query.setString("songId", songId);
                    return query.uniqueResult();
                }
            });
        }
    }

    public Request getUserRequest(final String restrictedUserId, final String songId) throws DataAccessException {
        if (restrictedUserId == null || songId == null) {
            return null;
        } else {
            return (Request) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT DISTINCT rq "
                            + "FROM " + Request.class.getCanonicalName() + " rq "
                            /*+ "     LEFT JOIN rq.requester rqr "
                            + "     LEFT JOIN rq.location.song rqs "
                            + "     LEFT JOIN rq.location.channel rqc "
                            + "WHERE "
                            + "     rqr.username = :restrictedUserId "
                            + "     AND "
                            + "     rqs.refID = :songId ";*/
                            + "WHERE "
                            + "     rq.requester.username = :restrictedUserId "
                            + "     AND "
                            + "     rq.location.song.refID = :songId ";
                    Query query = session.createQuery(hqlCommand);
                    query.setString("restrictedUserId", restrictedUserId);
                    query.setString("songId", songId);
                    return query.uniqueResult();
                }
            });
        }
    }

    public Collection<? extends Track> getTracks(final Class<? extends Track> clazz, final String author, final String title) throws DataAccessException {
        if (author == null || author.length()==0 || title == null || title.length()==0) {
            return new HashSet<Track>();
        } else {
            Collection<? extends Track> list = (Collection<? extends Track>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT tr ")//
                            .append("FROM ").append(clazz.getCanonicalName()).append(" tr ")//
                            /*.append("     LEFT JOIN tr.trackProgrammes trpr ")//
                            .append("           LEFT JOIN trpr.location trprl ")//
                            .append("             LEFT JOIN trprl.programme ")//*/
                            .append("WHERE ")//
                            .append("     tr.author = :author ")//
                            .append("     AND tr.title = :title ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("author", author);
                    query.setString("title", title);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<Track>() : list;
        }
    }

    /*public Collection<? extends BroadcastableTrack> getBroadcastTracksWithRightsByProgrammesForProgrammes(final Class<? extends BroadcastableTrack> clazz, final Collection<String> userProgrammeIds, final String author, final String title, final Collection<String> requiredProgrammeIds) throws DataAccessException {
    if (userProgrammeIds == null || userProgrammeIds.length()==0) {
    return new HashSet<BroadcastableTrack>();
    }

    //If a channelId has been provided narrow down the ones in channelIds to this one only
    if (requiredProgrammeIds != null) {
    userProgrammeIds.retainAll(requiredProgrammeIds);
    }

    return getBroadcastTracksForProgrammes(BroadcastableTrack.class, author, title, userProgrammeIds);
    }

    public Collection<? extends BroadcastableTrack> getBroadcastTracksForProgrammes(final Class<? extends BroadcastableTrack> clazz, final String author, final String title, final Collection<String> programmeIds) {
    if (author == null || author.length()==0 || title == null || title.length()==0 || programmeIds == null || programmeIds.length()==0) {
    return new HashSet<BroadcastableTrack>();
    }
    Collection<? extends BroadcastableTrack> list = (Collection<? extends BroadcastableTrack>) getTemplate().execute(new HibernateCallback() {

    public Object doInHibernate(Session session) throws HibernateException, SQLException {
    StringBuilder hqlCommand = new StringBuilder()//
    .append("SELECT DISTINCT tr ")//, pr "
    .append("FROM ").append(clazz.getCanonicalName()).append(" tr ")//
    .append("     LEFT JOIN tr.trackProgrammes trpr ")//
    .append("           LEFT JOIN trpr.location trprl ")//
    .append("             LEFT JOIN trprl.programme pr ")//
    .append("     LEFT JOIN tr.queueItems trqi ")//
    .append("WHERE ")//
    .append("     tr.author = :author ")//
    .append("     AND tr.title = :title ")//
    .append("     AND pr.label IN (:programmeIds) ");

    Query query = session.createQuery(hqlCommand.toString());
    query.setString("author", author);
    query.setString("title", title);
    query.setParameterList("programmeIds", programmeIds);
    return query.list();
    }
    });
    return (list == null) ? new HashSet<BroadcastableTrack>() : list;
    }*/
    public Collection<? extends BroadcastableTrack> getBroadcastTracksWithRightsByProgrammesForProgramme(final Collection<String> programmeIds, final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, final String programmeId, final String currentlyAuthenticatedUserName) throws DataAccessException {

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (programmeIds != null && programmeId != null) {
            if (programmeIds.contains(programmeId)) {
                return getBroadcastTracksForProgramme(constraint, false, programmeId, currentlyAuthenticatedUserName);
            }
        }
        return getBroadcastTracksForProgramme(constraint, false, null, currentlyAuthenticatedUserName);
    }

    public Collection<? extends BroadcastableTrack> getBroadcastTracksWithRightsByProgrammesForProgrammes(final Collection<String> programmeIds, final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, final String currentlyAuthenticatedUserName) throws DataAccessException {

        return getBroadcastTracksForProgrammesAndOrphans(BroadcastableTrack.class, constraint, false, programmeIds, currentlyAuthenticatedUserName);
    }

    public Collection<? extends BroadcastableTrack> getAllBroadcastTracks(final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws DataAccessException {
        return getBroadcastTracksForProgrammesAndOrphans(BroadcastableTrack.class, constraint, true, null, null);
    }

    public Collection<? extends BroadcastableTrack> getBroadcastTracksForProgramme(final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, final String programmeId, final String currentlyAuthenticatedUserName) throws DataAccessException {
        return getBroadcastTracksForProgramme(constraint, false, programmeId, currentlyAuthenticatedUserName);
    }

    public Collection<? extends BroadcastableTrack> getBroadcastTracksForProgramme(final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, final String programmeId) throws DataAccessException {
        return getBroadcastTracksForProgramme(constraint, false, programmeId, null);
    }

    public Collection<? extends BroadcastableTrack> getBroadcastTracksForProgramme(final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, boolean fetchAll, final String programmeId, final String currentlyAuthenticatedUserName) throws DataAccessException {
        Collection<String> programmeIds = null;
        if (programmeId != null) {
            programmeIds = new HashSet<String>();
            programmeIds.add(programmeId);
        }
        return getBroadcastTracksForProgrammesAndOrphans(BroadcastableTrack.class, constraint, fetchAll, programmeIds, currentlyAuthenticatedUserName);
    }

    public Collection<? extends BroadcastableTrack> getBroadcastTracksForProgrammesAndOrphans(final Class<? extends BroadcastableTrack> clazz, final GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, final boolean fetchAll, final Collection<String> programmeIds, final String currentlyAuthenticatedUserName) throws DataAccessException {

        if (clazz == null) {
            return new HashSet<BroadcastableTrack>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT tr ")//, trpr, pr ")//
                        .append(((constraint != null && constraint.getSortByFeature() != null && constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.averageVote)
                        ? ", AVG(vo.value) AS avg_vo_value " : ""))//
                        /*.append(((constraint != null && constraint.getSortByFeature() != null && (constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myVote || constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myVote) && currentlyAuthenticatedUserName != null)
                        ? ", mvo " : ""))//
                        .append(((constraint != null && constraint.getSortByFeature() != null && (constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest || constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest) && currentlyAuthenticatedUserName != null)
                        ? ", mrq " : ""))//*/
                        .append(" FROM ").append(clazz.getCanonicalName()).append(" tr ")//
                        .append("   LEFT JOIN tr.trackProgrammes trpr ")//
                        .append("           LEFT JOIN trpr.location.programme pr ")//
                        .append(((constraint != null && constraint.getSortByFeature() != null && constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.averageVote)
                        ? "         LEFT JOIN tr.votes vo " : ""))//
                        .append(((constraint != null && constraint.getSortByFeature() != null && (constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myVote || constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myVote) && currentlyAuthenticatedUserName != null)
                        ? "         LEFT JOIN tr.votes mvo "
                        + "             WITH mvo.location.user.username = :restrictedUserId " : ""))//
                        .append(((constraint != null && constraint.getSortByFeature() != null && (constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest || constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest) && currentlyAuthenticatedUserName != null)
                        ? "         LEFT JOIN tr.requests mrq "
                        + "             WITH mrq.requester.username = :restrictedUserId " : ""))//
                        ;

                if (!fetchAll) {
                    hqlCommand.append(" WHERE ");
                    if (programmeIds != null && !programmeIds.isEmpty()) {
                        hqlCommand.append("   pr.label IN (:programmeIds) OR ");
                    }
                    hqlCommand.append("   ( pr IS NULL ");
                    if (currentlyAuthenticatedUserName != null) {
                        hqlCommand.append("     AND (   (tr.creator IS NULL AND tr.latestEditor IS NULL) ")//
                                .append("         OR (tr.creator.username = :restrictedUserId) ")//
                                .append("         OR (tr.latestEditor.username = :restrictedUserId) ")//
                                .append("       ) ");
                    }
                    hqlCommand.append("   ) ");
                }

                //GROUP BY when averaging vote scores is required
                //Needed to compute aggregate scores off the database
                if (constraint != null && constraint.getSortByFeature() != null && constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.averageVote) {
                    hqlCommand//
                            .append(" GROUP BY ")//
                            //Code stink for Hibernate which doesn't automatically explode object properties for GROUP BY it uses in SELECT
                            //Fragile code that will break very soon, I cannot even rely on reflection to automate the task since some fields are unused by Hibernate
                            .append(EXPLODED_BROADCASTTRACK_PROPERTIES);
                    if ((constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myVote) && currentlyAuthenticatedUserName != null) {
                        hqlCommand.append(", mvo.value");
                    }
                    if ((constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest) && currentlyAuthenticatedUserName != null) {
                        hqlCommand.append(", mrq.creationDate");
                    }
                    if ((constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.rotationCount)) {
                        hqlCommand.append(", trpr.rotation");
                    }
                    if ((constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.programmes)) {
                        hqlCommand.append(", pr.label");
                    }
                }
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<BROADCASTABLE_TRACK_TAGS>() {

                    @Override
                    public String[] onTag(BROADCASTABLE_TRACK_TAGS currentTag) {
                        switch (currentTag) {
                            case type:
                                return new String[]{"tr.class"};
                            case enabled:
                                return new String[]{"tr.enabled"};
                            case title:
                                return new String[]{"tr.title"};
                            case author:
                                return new String[]{"tr.author"};
                            case album:
                                return new String[]{"tr.album"};
                            case duration:
                                return new String[]{"tr.trackContainer.duration"};
                            case rotationCount:
                                return new String[]{"trpr.rotation"};
                            case averageVote:
                                //BUG: MySQL (all versions) doesn't support sorting by aliases of aggregate functions; http://forum.hibernate.org/viewtopic.php?t=925363 and http://bugs.mysql.com/bug.php?id=5478
                                //FIXED: sorting by aliases fixed in current Hibernate releases, now MySQL works
                                return new String[]{"avg_vo_value"};
                            case myVote:
                                return (currentlyAuthenticatedUserName != null)
                                        ? new String[]{"mvo.value"}
                                        : null;
                            case myRequest:
                                return (currentlyAuthenticatedUserName != null)
                                        ? new String[]{"mrq.creationDate"}
                                        : null;
                            case genre:
                                return new String[]{"tr.genre"};
                            case yearOfRelease:
                                return new String[]{"tr.dateOfRelease"};
                            case description:
                                return new String[]{"tr.description"};
                            case programmes:
                                return new String[]{"pr.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (currentlyAuthenticatedUserName != null && (!fetchAll || (constraint != null && constraint.getSortByFeature() != null && (constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myVote || constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest || constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myVote || constraint.getFilterByFeature() == BROADCASTABLE_TRACK_TAGS.myRequest)))) {
                        query.setString("restrictedUserId", currentlyAuthenticatedUserName);
                    }
                    if (programmeIds != null && !programmeIds.isEmpty()) {
                        query.setParameterList("programmeIds", programmeIds);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<BroadcastableTrack>();
                }
            }
        });

        return (Collection<? extends BroadcastableTrack>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            //int trackLocationIndex = -1;
            @Override
            public BroadcastableTrack onRow(ScrollableResults inputRow) {

                //Hibernate BUG 1? does not return an array of Object but an array of an array of Object when an attribute is specified within the SELECT statement, i.e. in this case when data must be sorted by user votes
                //Hibernate Bug 2? this array of array of Object is not sequentially ordered!
                //WORKAROUND huge kludge: fully parse the first line and then remember where the correct object location is for the next rows
                //FIXED in Hibernate 3.6.X
                /*if (constraint != null && constraint.getSortByFeature() != null && constraint.getSortByFeature() == BROADCASTABLE_TRACK_TAGS.myVote && currentlyAuthenticatedUserName != null) {
                Object[] unorderedCompositeRow = ((Object[]) inputRow.get(0));
                if (trackLocationIndex < 0) {
                for (int i = 0; i < unorderedCompositeRow.length; i++) {
                if (ProxiedClassUtil.castableAs(unorderedCompositeRow[i], BroadcastableTrack.class)) {
                trackLocationIndex = i;
                break;
                }
                }
                }
                return (BroadcastableTrack) unorderedCompositeRow[trackLocationIndex];
                } else {*/
                return (BroadcastableTrack) inputRow.get(0);
                //}
            }
        });
    }

    public Collection<? extends PendingTrack> getPendingTracksWithRightsByProgrammesForProgramme(final Collection<String> programmeIds, final GroupAndSort<PENDING_TRACK_TAGS> constraint, final String programmeId, final String currentlyAuthenticatedUserName) throws DataAccessException {

        //If a channelId has been provided narrow down the ones in channelIds to this one only
        if (programmeIds != null && programmeId != null) {
            if (programmeIds.contains(programmeId)) {
                return getPendingTracksForProgramme(constraint, false, programmeId, currentlyAuthenticatedUserName);
            }
        }
        return getPendingTracksForProgramme(constraint, false, null, currentlyAuthenticatedUserName);
    }

    public Collection<? extends PendingTrack> getPendingTracksWithRightsByProgrammesForProgrammes(final Collection<String> programmeIds, final GroupAndSort<PENDING_TRACK_TAGS> constraint, final String currentlyAuthenticatedUserName) throws DataAccessException {

        return getPendingTracksForProgrammesAndOrphans(PendingTrack.class, constraint, false, programmeIds, currentlyAuthenticatedUserName);
    }

    public Collection<? extends PendingTrack> getAllPendingTracks(final GroupAndSort<PENDING_TRACK_TAGS> constraint) throws DataAccessException {
        return getPendingTracksForProgrammesAndOrphans(PendingTrack.class, constraint, true, null, null);
    }

    public Collection<? extends PendingTrack> getPendingTracksForProgramme(final GroupAndSort<PENDING_TRACK_TAGS> constraint, final String programmeId, final String currentlyAuthenticatedUserName) throws DataAccessException {
        return getPendingTracksForProgramme(constraint, false, programmeId, currentlyAuthenticatedUserName);
    }

    public Collection<? extends PendingTrack> getPendingTracksForProgramme(final GroupAndSort<PENDING_TRACK_TAGS> constraint, final String programmeId) throws DataAccessException {
        return getPendingTracksForProgramme(constraint, false, programmeId, null);
    }

    public Collection<? extends PendingTrack> getPendingTracksForProgramme(final GroupAndSort<PENDING_TRACK_TAGS> constraint, boolean fetchAll, final String programmeId, final String currentlyAuthenticatedUserName) throws DataAccessException {
        Collection<String> programmeIds = null;
        if (programmeId != null) {
            programmeIds = new HashSet<String>();
            programmeIds.add(programmeId);
        }
        return getPendingTracksForProgrammesAndOrphans(PendingTrack.class, constraint, fetchAll, programmeIds, currentlyAuthenticatedUserName);
    }

    public Collection<? extends PendingTrack> getPendingTracksForProgrammesAndOrphans(final Class<? extends PendingTrack> clazz, final GroupAndSort<PENDING_TRACK_TAGS> constraint, final boolean fetchAll, final Collection<String> programmeIds, final String currentlyAuthenticatedUserName) throws DataAccessException {

        if (clazz == null || programmeIds != null && programmeIds.isEmpty()) {
            return new HashSet<PendingTrack>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT tr ")/*, pr ")*///
                        .append(" FROM ").append(PendingTrack.class.getCanonicalName()).append(" tr ")//
                        .append("   LEFT JOIN tr.trackProgrammes trpr ")//
                        .append("       LEFT JOIN trpr.location trprl ")//
                        .append("           LEFT JOIN trprl.programme pr ");
                if (!fetchAll) {
                    hqlCommand.append(" WHERE ");
                    if (programmeIds != null && !programmeIds.isEmpty()) {
                        hqlCommand.append("   pr.label IN (:programmeIds) OR ");
                    }
                    hqlCommand.append("   ( pr IS NULL ");
                    if (currentlyAuthenticatedUserName != null) {
                        hqlCommand.append("     AND (   (tr.creator IS NULL AND tr.latestEditor IS NULL) ")//
                                .append("         OR (tr.creator.username = :restrictedUserId) ")//
                                .append("         OR (tr.latestEditor.username = :restrictedUserId) ")//
                                .append("       ) ");
                    }
                    hqlCommand.append("   ) ");
                }
                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<PENDING_TRACK_TAGS>() {

                    @Override
                    public String[] onTag(PENDING_TRACK_TAGS currentTag) {
                        switch (currentTag) {
                            case type:
                                return new String[]{"tr.class"};
                            case title:
                                return new String[]{"tr.title"};
                            case author:
                                return new String[]{"tr.author"};
                            case album:
                                return new String[]{"tr.album"};
                            case duration:
                                return new String[]{"tr.trackContainer.duration"};
                            case genre:
                                return new String[]{"tr.genre"};
                            case yearOfRelease:
                                return new String[]{"tr.dateOfRelease"};
                            case description:
                                return new String[]{"tr.description"};
                            case programmes:
                                return new String[]{"pr.label"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (programmeIds != null) {
                        query.setParameterList("programmeIds", programmeIds);
                    }
                    if (currentlyAuthenticatedUserName != null) {
                        query.setString("restrictedUserId", currentlyAuthenticatedUserName);
                    }
                    return query.scroll();
                } else {
                    return new HashSet<PendingTrack>();
                }
            }
        });

        return (Collection<? extends PendingTrack>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public PendingTrack onRow(ScrollableResults inputRow) {
                return (PendingTrack) inputRow.get(0);
            }
        });
    }

    public boolean isTrackReady(final String trackId) throws DataAccessException {
        if (trackId == null) {
            return false;
        }

        Boolean isReady = (Boolean) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                String hqlCommand =
                        "SELECT tr.ready "
                        + "FROM " + Track.class.getCanonicalName() + " tr "
                        + "WHERE "
                        + "     tr.refID = :trackId ";

                Query query = session.createQuery(hqlCommand);
                query.setString("trackId", trackId);
                return query.uniqueResult();
            }
        });

        return (isReady != null && isReady);
    }

    /**
     * BUG: Derby 10 rejects this non-standard SQL request (COUNT() used along with ORDER BY is no valid SQL)
     * FIX: force featureSortSelection to NULL
     * @param programmeId
     * @param featureSortSelection
     * @param startSortSelection
     * @param endSortSelection
     * @param filtersSelection
     * @return
     * @throws DataAccessException
     */
    public Long getNumberOfBroadcastableReadyAndEnabledTracksInRange(
            final String programmeId,
            final SORT featureSortSelection,
            final Long startSortSelection,
            final Long endSortSelection,
            final Collection<Filter> filtersSelection) throws DataAccessException {
        if (programmeId == null) {
            return null;
        }

        Long numOfTracks = (Long) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        //DO NOT use "fetch" if the joins are not developed in the SELECT clause, like it is the case with COUNT
                        .append("SELECT COUNT(DISTINCT tr) ")//
                        .append("FROM ").append(BroadcastableTrack.class.getCanonicalName()).append(" tr ")//
                        .append("     JOIN tr.trackProgrammes trpr ")//
                        .append("         JOIN trpr.location trprl ")//
                        .append("             JOIN trprl.programme pr ")//
                        .append("                   WITH pr.label = :programmeId ")
                        .append("     LEFT JOIN tr.rating rt ")//
                        .append("     LEFT JOIN tr.votes vo ")//
                        .append("WHERE ")//
                        //files MUST be accessible
                        .append("       tr.ready = TRUE ")//
                        .append("   AND ")//
                        //And enabled
                        .append("       tr.enabled = TRUE ")//
                        ;/*.append("   AND ")//
                        .append("       pr.label = :programmeId ");*/

                Query query = makeQuery(
                        session,
                        hqlCommand,
                        filtersSelection,
                        new XQLFilterFeatureTranslatorHandler() {

                            @Override
                            public String onFeature(FEATURE currentFilterFeature) {
                                if (currentFilterFeature instanceof ALPHABETICAL_FEATURE) {
                                    switch ((ALPHABETICAL_FEATURE) currentFilterFeature) {
                                        case title:
                                            return "tr.title";
                                        case author:
                                            return "tr.author";
                                        case album:
                                            return "tr.album";
                                        case genre:
                                            return "tr.genre";
                                        case flag:
                                            return "tr.tag";
                                        case copyright:
                                            return "tr.copyright";
                                        case advisory:
                                            return "rt.age";
                                        default:
                                            return null;
                                    }
                                } else if (currentFilterFeature instanceof NUMERICAL_FEATURE) {
                                    switch ((NUMERICAL_FEATURE) currentFilterFeature) {
                                        case averageRating:
                                            return "AVG(vo.value)";
                                        case databaseRecentness:
                                            return "tr.creationDate";
                                        case dateOfRelease:
                                            return "tr.dateOfRelease";
                                        case rotationCount:
                                            return "trpr.rotation";
                                        default:
                                            return null;
                                    }
                                } else if (currentFilterFeature instanceof CATEGORY_FEATURE) {
                                    switch ((CATEGORY_FEATURE) currentFilterFeature) {
                                        case song:
                                            return "tr.class";
                                        case advert:
                                            return "tr.class";
                                        case jingle:
                                            return "tr.class";
                                        default:
                                            return null;
                                    }
                                } else {
                                    return null;
                                }
                            }
                        },
                        //WORKAROUND for unique Apache Derby behaviour: https://issues.apache.org/jira/browse/DERBY-2351
                        null,
                        null,
                        //Hibernate setMaxResults() and setFirstResult() are slow, don't use them if you can
                        //TODO Computationally handle selection offset and size afterwards, very easy to do
                        //UPDATE re-activated them
                        null,//startSortSelection,
                        null);//endSortSelection);
                if (query != null) {
                    query.setString("programmeId", programmeId);
                    return query.uniqueResult();
                } else {
                    return null;
                }

            }
        });

        //Programmatically take into account offsets and selection sizes, because we can and Hibenrate is slow for this task
        if (numOfTracks != null) {

            if (endSortSelection == null) {
                if (startSortSelection != null) {
                    numOfTracks -= startSortSelection;
                }
            } else {
                numOfTracks = Math.min(numOfTracks, endSortSelection + 1) - (startSortSelection != null ? startSortSelection : 0L);
            }

        }

        return numOfTracks;
    }

    /**
     * Hibernate BUG with Postgres: RAND() doesn't use PostgreSQL's random()! see http://opensource.atlassian.com/projects/hibernate/browse/HHH-3458
     * @deprecated
     * @param programmeId
     * @return
     * @throws DataAccessException
     */
    public BroadcastableTrack getBroadcastableReadyAndEnabledEmergencyTrack(final String programmeId) throws DataAccessException {
        /*if (programmeId == null) {
        return null;
        }

        BroadcastableTrack track = (BroadcastableTrack) getTemplate().execute(new HibernateCallback() {

        public Object doInHibernate(Session session) throws HibernateException, SQLException {
        StringBuilder hqlCommand = new StringBuilder()//
        .append("SELECT DISTINCT tr ")//
        .append("FROM ").append(BroadcastableTrack.class.getCanonicalName()).append(" tr ")//
        .append("     JOIN tr.trackProgrammes trprs ")//
        .append("           JOIN trprs.location trprl ")//
        .append("             JOIN trprl.programme pr ")//
        .append("     LEFT JOIN tr.rating rt ")//
        .append("     LEFT JOIN tr.trackProgrammes trpr ")//
        .append("     LEFT JOIN tr.votes vo ")//
        .append("WHERE ")//
        //files MUST be accessible
        .append("       tr.ready = TRUE ")//
        .append("   AND ")//
        //And enabled
        .append("       tr.enabled = TRUE ")//
        .append("   AND ")//
        .append("       pr.label = :programmeId ")//
        .append("ORDER BY ")//
        .append("   RAND()");
        return session.createQuery(hqlCommand.toString())//
        .setString("programmeId", programmeId)//
        .setMaxResults(1)//
        .uniqueResult();
        }

        });

        return track;*/
        throw new UnsupportedOperationException("Deprecated method");
    }

    /**
     * WORKAROUND for unique Apache Derby behaviour: https://issues.apache.org/jira/browse/DERBY-2351
     * + WORKAROUND for bogus Hibernate mappings with LEFT JOINED SELECT elements
     * Don't directly extract a DISTINCT track from the query, but just an ID. Then hydrate the track corresponding to this ID only.
     */
    public BroadcastableTrack getBroadcastableReadyAndEnabledTrackAtIndex(
            final String programmeId,
            final SORT featureSortSelection,
            final long index,
            final Collection<Filter> filtersSelection) throws DataAccessException {
        if (programmeId == null) {
            return null;
        }

        //Don't directly reèuse this collection within your code, it's actually a collection of objects of hybrid types
        //Yep, it smells like the dirty hack it really is
        Collection<Object[]> tracks = (Collection<Object[]>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {

                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT tr ");
                if (featureSortSelection != null) {
                    if (featureSortSelection == SORT.averageRatingAsc || featureSortSelection == SORT.averageRatingDesc) {
                        hqlCommand.append(", AVG(vo.value) AS avg_vo_value ");
                    } else if (featureSortSelection == SORT.rotationCountAsc || featureSortSelection == SORT.rotationCountDesc) {
                        hqlCommand.append(", trpr.rotation ");
                    }
                }
                hqlCommand//
                        .append(" FROM ").append(BroadcastableTrack.class.getCanonicalName()).append(" tr ")//
                        .append("   JOIN tr.trackProgrammes trpr ")//
                        .append("       JOIN trpr.location trprl ")//
                        .append("           JOIN trprl.programme pr ")//
                        .append("               WITH pr.label = :programmeId ")
                        //If tracks are meant to be sorted by votes then be sure that only tracks with votes will be output by using a FILTER
                        //Same for rotation counts
                        .append("   LEFT JOIN tr.votes vo ")//
                        .append("   LEFT JOIN tr.rating rt ")//
                        .append("WHERE ")//
                        //files MUST be accessible
                        .append("       tr.ready = TRUE ")//
                        .append("   AND ")//
                        //And enabled
                        .append("       tr.enabled = TRUE ")//
                        ;/*.append("   AND ")//
                        .append("       pr.label = :programmeId ");*/

                //GROUP BY when averaging vote scores is required
                //Needed to compute aggregate scores off the database
                if (featureSortSelection != null && (featureSortSelection == SORT.averageRatingAsc || featureSortSelection == SORT.averageRatingDesc)) {
                    hqlCommand//
                            .append(" GROUP BY ")//
                            //Code stink for Hibernate which doesn't automatically explode object properties for GROUP BY it uses in SELECT
                            //Fragile code that will break very soon, I cannot even rely on reflection to automate the task since some fields are unused by Hibernate
                            .append(EXPLODED_BROADCASTTRACK_PROPERTIES);
                    /*if (featureSortSelection != null && (featureSortSelection == SORT.rotationCountAsc || featureSortSelection == SORT.rotationCountDesc)) {
                    hqlCommand.append(", trpr.rotation");
                    }*/
                }

                Query query = makeQuery(
                        session,
                        hqlCommand,
                        filtersSelection,
                        new XQLFilterFeatureTranslatorHandler() {

                            @Override
                            public String onFeature(FEATURE currentFilterFeature) {
                                if (currentFilterFeature instanceof ALPHABETICAL_FEATURE) {
                                    switch ((ALPHABETICAL_FEATURE) currentFilterFeature) {
                                        case title:
                                            return "tr.title";
                                        case author:
                                            return "tr.author";
                                        case album:
                                            return "tr.album";
                                        case genre:
                                            return "tr.genre";
                                        case flag:
                                            return "tr.tag";
                                        case copyright:
                                            return "tr.copyright";
                                        case advisory:
                                            return "rt.age";
                                        default:
                                            return null;
                                    }
                                } else if (currentFilterFeature instanceof NUMERICAL_FEATURE) {
                                    switch ((NUMERICAL_FEATURE) currentFilterFeature) {
                                        case averageRating:
                                            //Kludge to re-use the SORT pre-processed header variable if applicable
                                            if (featureSortSelection != null && (featureSortSelection == SORT.averageRatingAsc || featureSortSelection == SORT.averageRatingDesc)) {
                                                return "avg_vo_value";
                                            } else {
                                                return "AVG(vo.value)";
                                            }
                                        case databaseRecentness:
                                            return "tr.creationDate";
                                        case dateOfRelease:
                                            return "tr.dateOfRelease";
                                        case rotationCount:
                                            return "trpr.rotation";
                                        default:
                                            return null;
                                    }
                                } else if (currentFilterFeature instanceof CATEGORY_FEATURE) {
                                    switch ((CATEGORY_FEATURE) currentFilterFeature) {
                                        case song:
                                            return "tr.class";
                                        case advert:
                                            return "tr.class";
                                        case jingle:
                                            return "tr.class";
                                        default:
                                            return null;
                                    }
                                } else {
                                    return null;
                                }
                            }
                        },
                        featureSortSelection,
                        new XQLSortTranslatorHandler() {

                            @Override
                            public String[] onSort(SORT currentSort) {
                                switch (currentSort) {
                                    case creationDateAsc:
                                        return new String[]{"tr.creationDate ASC"};
                                    case creationDateDesc:
                                        return new String[]{"tr.creationDate DESC"};
                                    case releaseDateAsc:
                                        return new String[]{"tr.dateOfRelease ASC"};
                                    case releaseDateDesc:
                                        return new String[]{"tr.dateOfRelease DESC"};
                                    case rotationCountAsc:
                                        return new String[]{"trpr.rotation ASC"};
                                    case rotationCountDesc:
                                        return new String[]{"trpr.rotation DESC"};
                                    case averageRatingAsc:
                                        //BUG: MySQL (all versions) doesn't support sorting by aliases of aggregate functions; http://forum.hibernate.org/viewtopic.php?t=925363 and http://bugs.mysql.com/bug.php?id=5478
                                        //FIXED: sorting by aliases fixed in current Hibernate releases, now MySQL works
                                        return new String[]{"avg_vo_value ASC"};
                                    case averageRatingDesc:
                                        //BUG: MySQL (all versions) doesn't support sorting by aliases of aggregate functions; http://forum.hibernate.org/viewtopic.php?t=925363 and http://bugs.mysql.com/bug.php?id=5478
                                        //FIXED: sorting by aliases fixed in current Hibernate releases, now MySQL works
                                        return new String[]{"avg_vo_value DESC"};
                                    default:
                                        return null;
                                }
                            }
                        },
                        index,
                        index);
                if (query != null) {
                    query.setString("programmeId", programmeId);
                    //Hibernate is sometimes insane; it cannot add the proper LIMIT SQL statemennt in such a query: it will first get everything in MEMORY(!) and then filter out, madness!
                    //TODO: hard to come up with a fast query for this one, but it would be essential to get one
                    /*DB paging-only FIX*/ return query.list();
                } else {
                    return null;
                }

            }
        });

        if (tracks != null && !tracks.isEmpty()) {
            Iterator i = tracks.iterator();
            //It doesn't matter if the set is not sorted, setFirstResult() and setMaxResults() will do the job: only 1 row is returned
            //BUG yet another Hibernate bug: setMaxresults() not correctly interpreted for Derby: don't assume there will a unique row with Derby, there will be setMaxresults() + 1 entries actually
            //FIX: use the AlternativeDerbyDialect, not the original one
            if (i.hasNext()) {
                //Follow up to the kludge for Apache Derby
                //If there were more than one element in the SELECT clause then it's an Object[], otherwise it's just the plain, reassociated track
                if (featureSortSelection != null && (featureSortSelection == SORT.averageRatingAsc || featureSortSelection == SORT.averageRatingDesc || featureSortSelection == SORT.rotationCountAsc || featureSortSelection == SORT.rotationCountDesc)) {
                    return (BroadcastableTrack) (((Object[]) i.next())[0]);
                } else {
                    return (BroadcastableTrack) i.next();
                }
            }
        }
        return null;
    }

    /**
     * Pop the next available request
     * @param author
     * @param title
     * @param programmeIds
     * @return
     */
    public Request getNextAvailableRequest(final String channelId, final String programmeId, final byte offset) {
        if (channelId == null || channelId.length()==0 || programmeId == null || programmeId.length()==0) {
            return null;
        }
        return (Request) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT rq ")//
                        .append("FROM ").append(Request.class.getCanonicalName()).append(" rq ")//
                        /*.append("     LEFT JOIN rq.location.song rqs ")//
                        .append("       LEFT JOIN rqs.trackProgrammes rqstp ")//*/
                        .append("     JOIN rq.location.song rqs ")//
                        .append("       JOIN rqs.trackProgrammes rqstp ")
                        .append("           WITH rqstp.location.programme.label = :programmeId ")
                        .append("WHERE ")//
                        .append("     rq.location.channel = :channelId ")//
                        //.append("     AND rqstp.location.programme = :programmeId ")//
                        .append("     AND rq.queueItem IS NULL ")//
                        .append("     AND rq.streamItem IS NULL ")//
                        .append(" ORDER BY ")//
                        .append("     rq.creationDate ASC ");

                return session.createQuery(hqlCommand.toString())//
                        .setString("channelId", channelId)//
                        .setString("programmeId", programmeId)//
                        .setMaxResults(1)//
                        .setFirstResult(offset)//
                        .uniqueResult();
            }
        });
    }

    public Long getNumberOfRequestsForUserAfterTimeThreshold(final String channelId, final String restrictedUserId, final YearMonthWeekDayHourMinuteSecondMillisecondInterface timeThreshold) {
        if (channelId == null || restrictedUserId == null || timeThreshold == null) {
            return null;
        }

        Long numOfRequests = (Long) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        //DO NOT use "fetch" if the joins are not developed in the SELECT clause, like it is the case with COUNT
                        //Can't use the primary key in the COUNT since it's a composite key, find a workaround
                        .append("SELECT COUNT(rq) ")//
                        .append("FROM ").append(Request.class.getCanonicalName()).append(" rq ")//
                        .append("WHERE ")//
                        .append("       rq.requester = :requesterId ")//
                        .append("   AND ")//
                        .append("       rq.location.channel = :channelId ")//
                        .append("   AND ")//
                        .append("       rq.creationDate > :pastDate ");//

                return session.createQuery(hqlCommand.toString())//
                        .setString("requesterId", restrictedUserId)//
                        .setString("channelId", channelId)//
                        .setString("pastDate", timeThreshold.toSQLDate())//
                        .uniqueResult();

            }
        });

        return numOfRequests;
    }

    private StringBuilder generateHQLTrackFilterSetCommand(Collection<Filter> filters, final XQLFilterFeatureTranslatorHandler featureConverter, Collection<HQLParamWildcard> hqlParamWildcards) {
        final String operator = "AND";
        StringBuilder statement = new StringBuilder();

        if (filters != null && featureConverter != null) {
            boolean firstItem = true;
            StringBuilder item;
            if (hqlParamWildcards == null) {
                hqlParamWildcards = new HashSet<HQLParamWildcard>();
            }
            for (Filter filter : filters) {

                item = generateHQLTrackFilterCommand(filter, featureConverter, hqlParamWildcards);

                if (item != null && item.length() > 0) {

                    if (firstItem) {
                        firstItem = false;
                    } else {
                        statement.append(" ").append(operator).append(" ");
                    }

                    statement.append(item);
                }

            }

            if (statement.length() > 0) {
                statement.insert(0, " ( ");
                statement.append(" ) ");
            }
        }

        return statement;
    }

    private StringBuilder generateHQLSortCommand(SORT sortConstraint, XQLSortTranslatorHandler sortConverter) {
        String[] sortCommand = null;
        if (sortConstraint == null
                || sortConverter == null
                || (sortCommand = sortConverter.onSort(sortConstraint)) == null
                || sortCommand.length < 1) {
            return null;
        } else {
            StringBuilder translatedCommand = new StringBuilder();
            for (int i = 0; i < sortCommand.length; i++) {
                if (i != 0) {
                    translatedCommand.append(", ");
                }
                //DESC and ASC directives should directly be specified within the result of the callback
                translatedCommand.append(sortCommand[i]);
            }
            return translatedCommand;
        }
    }

    private Query makeQuery(
            Session session,
            StringBuilder hqlQuery,
            Collection<Filter> filters,
            final XQLFilterFeatureTranslatorHandler filterFeatureConverter,
            SORT sort,
            final XQLSortTranslatorHandler sortConverter,
            /** zero-based **/
            Long from,
            /** zero-based **/
            Long to) {
        Query query = null;
        if (session != null && hqlQuery != null) {

            //First try to add the filter commands, if applicable
            Collection<HQLParamWildcard> hqlParamWildcards = new HashSet<HQLParamWildcard>();
            StringBuilder filterCommand = generateHQLTrackFilterSetCommand(filters, filterFeatureConverter, hqlParamWildcards);
            hqlQuery = insertAfterWhereStatement(hqlQuery, filterCommand);

            //Then try to add the sort commands, if applicable
            StringBuilder sortCommand = generateHQLSortCommand(sort, sortConverter);
            if (sortCommand != null && sortCommand.length() > 0) {
                hqlQuery.append(" ORDER BY ").append(sortCommand);
            }

            //Now generate the query
            query = session.createQuery(hqlQuery.toString());

            //Then the limit of the selection if applicable
            if (from != null) {
                //Do only that it results are sorted out
                if (sortCommand != null && sortCommand.length() > 0) {
                    query.setFirstResult(from.intValue());
                }
            }
            if (to != null) {
                //Do only that it results are sorted out
                if (sortCommand != null && sortCommand.length() > 0) {
                    //BUG yet another Hibernate bug: setMaxresults() not correctly interpreted for Derby
                    //TODO dirty downcast
                    query.setMaxResults(NumberUtil.safeLongToInt(from != null ? to - from + 1 : to + 1));
                }
            }

            //And add the track filters wildcard, if applicable
            if (hqlParamWildcards != null) {
                for (HQLParamWildcard hqlParamWildcard : hqlParamWildcards) {
                    hqlParamWildcard.bindParamToQuery(query);
                }
            }

        }
        return query;
    }

    private StringBuilder generateHQLTrackFilterCommand(Filter filter, final XQLFilterFeatureTranslatorHandler featureConverter, final Collection<HQLParamWildcard> hqlParamWildcards) {
        StringBuilder statement = null;

        if (filter != null && featureConverter != null && hqlParamWildcards != null) {

            FilterVisitorHQL filterVisitorHQL = new FilterVisitorHQL() {

                private StringBuilder hqlStatement = null;
                private long i = 0;

                @Override
                public StringBuilder getHQLStatement() {
                    return hqlStatement;
                }

                @Override
                public void visit(AlphabeticalFilter filter) throws Exception {
                    if (filter != null) {

                        ALPHABETICAL_FEATURE feature = filter.getFeature();
                        if (feature != null) {
                            String featureString = featureConverter.onFeature(feature);
                            if (featureString != null && featureString.length()!=0) {
                                hqlStatement = new StringBuilder();
                                if (!filter.isInclude()) {
                                    hqlStatement.append(" NOT ");
                                }
                                hqlStatement.append("(");
                                hqlStatement.append(featureString);
                                hqlStatement.append(" LIKE ");
                                String alphabeticalFeatureVarName = "alphabeticFilter" + (++i);
                                hqlStatement.append(":").append(alphabeticalFeatureVarName);
                                hqlStatement.append(") ");
                                HQLParamWildcard<String> sortWildcard = new HQLParamWildcard<String>(
                                        alphabeticalFeatureVarName,
                                        filter.getContains()) {

                                    @Override
                                    public void bindParamToQuery(Query query) {
                                        query.setString(getWildcard(), "%" + getValue() + "%");
                                    }
                                };
                                hqlParamWildcards.add(sortWildcard);
                            }
                        }
                    }
                }

                @Override
                public void visit(NumericalFilter filter) throws Exception {
                    if (filter != null) {

                        NUMERICAL_FEATURE feature = filter.getFeature();
                        if (feature != null) {
                            String featureString = featureConverter.onFeature(feature);
                            if (featureString != null && featureString.length()!=0) {
                                hqlStatement = new StringBuilder();
                                if (!filter.isInclude()) {
                                    hqlStatement.append(" NOT ");
                                }
                                hqlStatement.append("(");
                                hqlStatement.append(featureString);
                                hqlStatement.append(" >= ");
                                String numericalFeatureVarName1 = "numericFilter" + (++i);
                                hqlStatement.append(":").append(numericalFeatureVarName1);
                                hqlStatement.append(" AND ");
                                hqlStatement.append(featureString);
                                hqlStatement.append(" <= ");
                                String numericalFeatureVarName2 = "numericFilter" + (++i);
                                hqlStatement.append(":").append(numericalFeatureVarName2);
                                hqlStatement.append(") ");
                                HQLParamWildcard<Short> sortWildcard1 = new HQLParamWildcard<Short>(
                                        numericalFeatureVarName1,
                                        filter.getMin()) {

                                    @Override
                                    public void bindParamToQuery(Query query) {
                                        query.setShort(getWildcard(), getValue());
                                    }
                                };
                                HQLParamWildcard<Short> sortWildcard2 = new HQLParamWildcard<Short>(
                                        numericalFeatureVarName2,
                                        filter.getMax()) {

                                    @Override
                                    public void bindParamToQuery(Query query) {
                                        query.setShort(getWildcard(), getValue());
                                    }
                                };
                                hqlParamWildcards.add(sortWildcard1);
                                hqlParamWildcards.add(sortWildcard2);
                            }
                        }
                    }
                }

                @Override
                public void visit(CategoryFilter filter) throws Exception {
                    if (filter != null) {

                        CATEGORY_FEATURE feature = filter.getFeature();
                        if (feature != null) {
                            String featureString = featureConverter.onFeature(feature);
                            if (featureString != null && featureString.length()!=0) {
                                hqlStatement = new StringBuilder();
                                if (!filter.isInclude()) {
                                    hqlStatement.append(" NOT ");
                                }
                                hqlStatement.append("(");
                                hqlStatement.append(featureString);
                                hqlStatement.append(" = ");
                                switch (feature) {
                                    case song:
                                        hqlStatement.append(BroadcastableSong.class.getCanonicalName());
                                        break;
                                    case jingle:
                                        hqlStatement.append(BroadcastableJingle.class.getCanonicalName());
                                        break;
                                    case advert:
                                        hqlStatement.append(BroadcastableAdvert.class.getCanonicalName());
                                        break;
                                    default:
                                        hqlStatement.append(BroadcastableTrack.class.getCanonicalName());
                                }
                                hqlStatement.append(") ");
                            }
                        }
                    }
                }
            };
            try {
                filter.acceptVisit(filterVisitorHQL);
            } catch (Exception ex) {
                logger.error("generateHQLTrackFilterCommand failed", ex);
            }
            statement = filterVisitorHQL.getHQLStatement();
        }

        return statement;
    }

    /**
     * Only used within the CLI tool for Discogs metadata merging
     * @param authorIds
     * @return
     */
    public Collection<BroadcastableSong> getFuzzyBroadcastSongsForAuthors(final List<String> authorIds, final boolean combineNames) {
        if (authorIds == null || authorIds.isEmpty()) {
            return new HashSet<BroadcastableSong>();
        }
        Collection<BroadcastableSong> list = (Collection<BroadcastableSong>) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT tr ")//, pr "
                        .append("FROM ").append(BroadcastableSong.class.getCanonicalName()).append(" tr ")//
                        .append("WHERE ")//
                        .append("     LOWER(tr.author) ").append(combineNames ? " LIKE :author " : " IN (:author) ");

                Query query = session.createQuery(hqlCommand.toString());
                if (combineNames) {
                    StringBuilder authorList = new StringBuilder();
                    authorList.append(authorIds.get(0).toLowerCase());
                    for (String authorId : authorIds) {
                        authorList.append('%').append(authorId.toLowerCase());
                    }
                    query.setString("author", authorList.toString());
                } else {
                    Collection<String> lowerCasedAuthors = new HashSet<String>();
                    for (String author : authorIds) {
                        lowerCasedAuthors.add(author.toLowerCase());
                    }
                    query.setParameterList("author", lowerCasedAuthors);
                }
                return query.list();
            }
        });
        return (list == null) ? new HashSet<BroadcastableSong>() : list;
    }
}
