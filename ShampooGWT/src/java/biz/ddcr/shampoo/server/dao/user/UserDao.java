/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.dao.user;

import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.dao.GenericHibernateDao;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;

/**
 *
 * @author okay_awright
 **/
public class UserDao extends GenericHibernateDao {

    public Collection<User> getAllUsers(final GroupAndSort<USER_TAGS> constraint) throws DataAccessException {
        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT us ")//, rar, pmr, lr, ar, cur, cor ")
                        .append("FROM ").append(User.class.getCanonicalName()).append(" us ") //Do some pre-fetching, because lazy-loading s not a solution
                        .append(" LEFT JOIN us.channelAdministratorRights rar ")//
                        .append(" LEFT JOIN us.programmeManagerRights pmr ")//
                        .append(" LEFT JOIN us.listenerRights lr ")//
                        .append(" LEFT JOIN us.animatorRights ar ")//
                        .append(" LEFT JOIN us.curatorRights cur ")//
                        .append(" LEFT JOIN us.contributorRights cor ");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<USER_TAGS>() {

                    @Override
                    public String[] onTag(USER_TAGS currentTag) {
                        switch (currentTag) {
                            case type:
                                return new String[]{"us.class"};
                            case enabled:
                                return new String[]{"us.enabled"};
                            case firstAndLastName:
                                return new String[]{"us.firstAndLastName"};
                            case rights:
                                return new String[]{"rar.label", "pmr.label", "lr.label", "ar.label", "cur.label", "cor.label"};
                            case username:
                                return new String[]{"us.username"};
                            default:
                                return null;
                        }
                    }
                });
                return query != null ? query.scroll() : new HashSet<User>();
            }
        });

        return (Collection<User>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<RestrictedUser> getAllRestrictedUsers(final GroupAndSort<USER_TAGS> constraint) throws DataAccessException {
        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT us ")//, rar, pmr, lr, ar, cur, cor ")
                        .append("FROM ").append(RestrictedUser.class.getCanonicalName()).append(" us ") //Do some pre-fetching, because lazy-loading s not a solution
                        .append(" LEFT JOIN us.channelAdministratorRights rar ")//
                        .append(" LEFT JOIN us.programmeManagerRights pmr ")//
                        .append(" LEFT JOIN us.listenerRights lr ")//
                        .append(" LEFT JOIN us.animatorRights ar ")//
                        .append(" LEFT JOIN us.curatorRights cur ")//
                        .append(" LEFT JOIN us.contributorRights cor ");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<USER_TAGS>() {

                    @Override
                    public String[] onTag(USER_TAGS currentTag) {
                        switch (currentTag) {
                            case type:
                                return new String[]{"us.class"};
                            case enabled:
                                return new String[]{"us.enabled"};
                            case firstAndLastName:
                                return new String[]{"us.firstAndLastName"};
                            case rights:
                                return new String[]{"rar.label", "pmr.label", "lr.label", "ar.label", "cur.label", "cor.label"};
                            case username:
                                return new String[]{"us.username"};
                            default:
                                return null;
                        }
                    }
                });
                return query != null ? query.scroll() : new HashSet<RestrictedUser>();
            }
        });

        return (Collection<RestrictedUser>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<RestrictedUser> getAllLinkedRestrictedUsers(
            final Collection<String> channelIds,
            final Collection<String> programmeIds,
            final String restrictedUserId,
            final GroupAndSort<USER_TAGS> constraint) throws DataAccessException {
        if (restrictedUserId == null) {
            return new HashSet<RestrictedUser>();
        }

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT us ")//, rar, pmr, lr, ar, cur, cor ")
                        .append("FROM ").append(RestrictedUser.class.getCanonicalName()).append(" us ") //Do some pre-fetching, because lazy-loading s not a solution
                        .append("   LEFT JOIN us.channelAdministratorRights rar ")//
                        .append("   LEFT JOIN us.programmeManagerRights pmr ")//
                        .append("   LEFT JOIN us.listenerRights lr ")//
                        .append("   LEFT JOIN us.animatorRights ar ")//
                        .append("   LEFT JOIN us.curatorRights cur ")//
                        .append("   LEFT JOIN us.contributorRights cor ")//
                        .append("WHERE ")//
                        .append(((channelIds != null && !channelIds.isEmpty()) || (programmeIds != null && !programmeIds.isEmpty())
                        ? " ( " : ""))//
                        .append((channelIds != null && !channelIds.isEmpty()
                        ? "     rar.label IN (:channelIds) OR "
                        + "   pmr.label IN (:channelIds) OR "
                        + "   lr.label IN (:channelIds) " : ""))//
                        .append(((channelIds != null && !channelIds.isEmpty()) && (programmeIds != null && !programmeIds.isEmpty())
                        ? " OR " : ""))//
                        .append((programmeIds != null && !programmeIds.isEmpty()
                        ? "     ar.label IN (:programmeIds) OR "
                        + "   cur.label IN (:programmeIds) OR "
                        + "   cor.label IN (:programmeIds) " : ""))//
                        .append(((channelIds != null && !channelIds.isEmpty()) || (programmeIds != null && !programmeIds.isEmpty())
                        ? " ) OR " : ""))//
                        .append("( ")//
                        .append("     rar IS NULL AND ")//
                        .append("     pmr IS NULL AND ")//
                        .append("     lr IS NULL AND ")//
                        .append("     ar IS NULL AND ")//
                        .append("     cur IS NULL AND ")//
                        .append("     cor IS NULL AND ")//
                        .append("     (   (us.creator IS NULL AND us.latestEditor IS NULL) ")//
                        .append("         OR (us.creator.username = :username) ")//
                        .append("         OR (us.latestEditor.username = :username) ")//
                        .append("     ) ")//
                        .append(")");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<USER_TAGS>() {

                    @Override
                    public String[] onTag(USER_TAGS currentTag) {
                        switch (currentTag) {
                            case type:
                                return new String[]{"us.class"};
                            case enabled:
                                return new String[]{"us.enabled"};
                            case firstAndLastName:
                                return new String[]{"us.firstAndLastName"};
                            case rights:
                                return new String[]{"rar.label", "pmr.label", "lr.label", "ar.label", "cur.label", "cor.label"};
                            case username:
                                return new String[]{"us.username"};
                            default:
                                return null;
                        }
                    }
                });

                if (query != null) {
                    if (channelIds != null && !channelIds.isEmpty()) {
                        query.setParameterList("channelIds", channelIds);
                    }
                    if (programmeIds != null && !programmeIds.isEmpty()) {
                        query.setParameterList("programmeIds", programmeIds);
                    }
                    query.setString("username", restrictedUserId);
                    return query.scroll();
                } else {
                    return new HashSet<RestrictedUser>();
                }
            }
        });

        return (Collection<RestrictedUser>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public Collection<Administrator> getAllAdmininistrators(final GroupAndSort<USER_TAGS> constraint) throws DataAccessException {

        ScrollableResults rawResults = (ScrollableResults) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                StringBuilder hqlCommand = new StringBuilder()//
                        .append("SELECT DISTINCT us ")//
                        .append("FROM ").append(Administrator.class.getCanonicalName()).append(" us ");

                Query query = makeQuery(session, hqlCommand, constraint, new XQLObjectTranslatorHandler<USER_TAGS>() {

                    @Override
                    public String[] onTag(USER_TAGS currentTag) {
                        switch (currentTag) {
                            case type:
                                return new String[]{"us.class"};
                            case enabled:
                                return new String[]{"us.enabled"};
                            case firstAndLastName:
                                return new String[]{"us.firstAndLastName"};
                            case username:
                                return new String[]{"us.username"};
                            default:
                                return null;
                        }
                    }
                });

                return query != null ? query.scroll() : new HashSet<Administrator>();
            }
        });

        return (Collection<Administrator>) scrollResults(getSession(), rawResults, constraint, new RowHandler() {

            @Override
            public GenericEntityInterface onRow(ScrollableResults inputRow) {
                return (GenericEntityInterface) inputRow.get(0);
            }
        });
    }

    public void addUser(User user) throws DataAccessException {
        if (user != null) {
            getTemplate().save(user);
        }
    }

    public void updateUser(User user) throws DataAccessException {
        if (user != null) {
            getTemplate().update(user);
        }
    }

    public void updateBatchUsers(Collection<? extends User> users) throws DataAccessException {
        if (users != null) {
            batchUpdate(
                    getTemplate().getSessionFactory().getCurrentSession(),
                    users);
        }
    }

    public Administrator getAdministrator(String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return getTemplate().get(Administrator.class, id);
        }
    }

    public User getUser(String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return getTemplate().get(User.class, id);
        }
    }

    public Collection<? extends User> getUsers(Class<? extends User> clazz, Collection<String> ids) throws DataAccessException {
        Collection<User> users = null;
        if (ids != null) {
            users = new HashSet<User>();
            for (String id : ids) {
                User user = (User) getTemplate().get(clazz, id);
                if (user != null) {
                    users.add(user);
                }
            }
        }
        return users;
    }

    public RestrictedUser getRestrictedUser(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return getTemplate().get(RestrictedUser.class, id);
        }
    }

    public User getUserByCaseInsensitiveId(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (User) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT us ")//
                            .append("FROM ").append(User.class.getCanonicalName()).append(" us ")//
                            .append("WHERE ")//
                            .append("     LOWER(us.username) = :username ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("username", id.toLowerCase());
                    return query.uniqueResult();
                }
            });
        }
    }

    public Collection<RestrictedUser> loadRestrictedUsers(final Collection<String> ids) throws DataAccessException {
        if (ids == null || ids.isEmpty()) {
            return new HashSet<RestrictedUser>();
        } else {
            Collection<RestrictedUser> list = (Collection<RestrictedUser>) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT us ")//
                            .append("FROM ").append(RestrictedUser.class.getCanonicalName()).append(" us ")//
                            //BUG: Hibernate confused if FETCH on: wrong proxied class fo trrackProgramme
                            .append("     LEFT JOIN FETCH us.channelAdministratorRights ")//
                            .append("     LEFT JOIN FETCH us.programmeManagerRights ")//
                            .append("     LEFT JOIN FETCH us.listenerRights ")//
                            .append("     LEFT JOIN FETCH us.animatorRights ")//
                            .append("     LEFT JOIN FETCH us.curatorRights ")//
                            .append("     LEFT JOIN FETCH us.contributorRights ")//
                            .append("WHERE ")//
                            .append("     us.username IN (:usernames) ");
                    Query query = session.createQuery(hqlCommand.toString());
                    query.setParameterList("usernames", ids);
                    return query.list();
                }
            });
            return (list == null) ? new HashSet<RestrictedUser>() : list;
        }
    }
    
    public User getUserByCaseInsensitiveEmail(final String emailAddress) throws DataAccessException {
        if (emailAddress == null) {
            return null;
        } else {
            return (User) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT us ")//
                            .append("FROM ").append(User.class.getCanonicalName()).append(" us ")//
                            .append("WHERE ")//
                            .append("     LOWER(us.email) = :emailAddress ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("emailAddress", emailAddress.toLowerCase());
                    return query.uniqueResult();
                }
            });
        }
    }

    public RestrictedUser loadRestrictedUser(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (RestrictedUser) getTemplate().execute(new HibernateCallback() {

                @Override
                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT us ")//
                            .append("FROM ").append(RestrictedUser.class.getCanonicalName()).append(" us ")//
                            .append("     LEFT JOIN FETCH us.channelAdministratorRights ")//
                            .append("     LEFT JOIN FETCH us.programmeManagerRights ")//
                            .append("     LEFT JOIN FETCH us.listenerRights ")//
                            .append("     LEFT JOIN FETCH us.animatorRights ")//
                            .append("     LEFT JOIN FETCH us.curatorRights ")//
                            .append("     LEFT JOIN FETCH us.contributorRights ")//
                            .append("WHERE ")//
                            .append("     us.username = :id ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            });
        }
    }

    /*public RestrictedUser getRestrictedUserWithVotesHydrated(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (RestrictedUser) getTemplate().execute(new HibernateCallback() {

                public Object doInHibernate(Session session) throws HibernateException, SQLException {
                    String hqlCommand =
                            "SELECT DISTINCT us "
                            + "FROM " + RestrictedUser.class.getCanonicalName() + " us "
                            + "     LEFT JOIN FETCH us.votes vo "
                            + "         LEFT JOIN FETCH vo.refID.song vos "
                            + "WHERE "
                            + "     us.username = :id ";

                    Query query = session.createQuery(hqlCommand);
                    query.setString("id", id);
                    return query.uniqueResult();
                }
            }));
        }
    }*/

    public RestrictedUser getFullyHydratedRestrictedUser(final String id) throws DataAccessException {
        if (id == null) {
            return null;
        } else {
            return (RestrictedUser) getTemplate().execute(new HibernateCallback() {

                @Override
                public RestrictedUser doInHibernate(Session session) throws HibernateException, SQLException {
                    StringBuilder hqlCommand = new StringBuilder().append("SELECT DISTINCT us ")//
                            .append("FROM ").append(RestrictedUser.class.getCanonicalName()).append(" us ")//
                            .append("     LEFT JOIN FETCH us.channelAdministratorRights rar ")//
                            .append("         LEFT JOIN FETCH rar.programmes ")//
                            .append("     LEFT JOIN FETCH us.programmeManagerRights pmr ")//
                            .append("         LEFT JOIN FETCH pmr.programmes ")//
                            .append("     LEFT JOIN FETCH us.listenerRights lr ")//
                            .append("         LEFT JOIN FETCH lr.programmes ")//
                            .append("     LEFT JOIN FETCH us.animatorRights ar ")//
                            .append("         LEFT JOIN FETCH ar.channels arr ")//
                            .append("           LEFT JOIN FETCH arr.links ")//
                            .append("     LEFT JOIN FETCH us.curatorRights cur ")//
                            .append("         LEFT JOIN FETCH cur.channels curr ")//
                            .append("           LEFT JOIN FETCH curr.links ")//
                            .append("     LEFT JOIN FETCH us.contributorRights cor ")//
                            .append("         LEFT JOIN FETCH cor.channels corr ")//
                            .append("           LEFT JOIN FETCH corr.links ")//
                            .append("WHERE ")//
                            .append("     us.username = :id ");

                    Query query = session.createQuery(hqlCommand.toString());
                    query.setString("id", id);
                    return (RestrictedUser) query.uniqueResult();
                }
            });
        }
    }

    public void deleteUser(User user) throws DataAccessException {
        if (user != null) {
            getTemplate().delete(user);
        }
    }

    public void deleteBatchUsers(Collection<? extends User> users) throws DataAccessException {
        if (users != null) {
            getTemplate().deleteAll(users);
        }
    }

    public boolean isAdministrator(final String userId) throws DataAccessException {
        if (userId == null) {
            return false;
        }

        Long numOfUsers = (Long) getTemplate().execute(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
                String hqlCommand =
                        "SELECT COUNT(us) "
                        + "FROM " + Administrator.class.getCanonicalName() + " us "
                        + "WHERE "
                        + "     us.username = :userId ";

                Query query = session.createQuery(hqlCommand);
                query.setString("userId", userId);
                return query.uniqueResult();
            }
        });

        return (numOfUsers != null && numOfUsers > 0);
    }

    /*public Collection<RestrictedUser> getLinkedRestrictedUserIdsByProgrammesOrChannels(final Collection<String> channelIds, final Collection<String> programmeIds, final RightsBundle rightsBundle) {

        if (channelIds == null || programmeIds == null || (channelIds.isEmpty() && programmeIds.isEmpty()) || rightsBundle == null || !rightsBundle.isRightsSelected()) {
            return new HashSet<RestrictedUser>();
        }
        //Furthermore, do only continue if there are channels or programmes to inspect but only when the relevant rights are present
        if (
                (!channelIds.isEmpty() && (rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights()))
                || (!programmeIds.isEmpty() && (rightsBundle.isAnimatorRights() || rightsBundle.isCuratorRights() || rightsBundle.isContributorRights()))
                ) {

            Collection<RestrictedUser> result = (Collection<RestrictedUser>) getTemplate().execute(new HibernateCallback() {

                public Object doInHibernate(Session session) throws HibernateException, SQLException {

                    StringBuilder hqlCommand = new StringBuilder()//
                            .append("SELECT DISTINCT us ")//, rar, pmr, lr, ar, cur, cor ")
                            .append("FROM " + RestrictedUser.class.getCanonicalName() + " us "); //Do some pre-fetching, because lazy-loading s not a solution
                    if (rightsBundle.isChannelAdministratorRights() && !channelIds.isEmpty()) {
                        hqlCommand.append(" LEFT JOIN us.channelAdministratorRights rar ");//
                    }
                    if (rightsBundle.isProgrammeManagerRights() && !channelIds.isEmpty()) {
                        hqlCommand.append(" LEFT JOIN us.programmeManagerRights pmr ");//
                    }
                    if (rightsBundle.isListenerRights() && !channelIds.isEmpty()) {
                        hqlCommand.append(" LEFT JOIN us.listenerRights lr ");//
                    }
                    if (rightsBundle.isAnimatorRights() && !programmeIds.isEmpty()) {
                        hqlCommand.append(" LEFT JOIN us.animatorRights ar ");//
                    }
                    if (rightsBundle.isCuratorRights() && !programmeIds.isEmpty()) {
                        hqlCommand.append(" LEFT JOIN us.curatorRights cur ");//
                    }
                    if (rightsBundle.isContributorRights() && !programmeIds.isEmpty()) {
                        hqlCommand.append(" LEFT JOIN us.contributorRights cor ");//
                    }
                    hqlCommand.append("WHERE ");//
                    if (!channelIds.isEmpty()) {
                        if (rightsBundle.isChannelAdministratorRights()) {
                            hqlCommand.append("     rar.label IN (:channelIds) ");//
                        }
                        if (rightsBundle.isChannelAdministratorRights() && rightsBundle.isProgrammeManagerRights()) {
                            hqlCommand.append(" OR ");
                        }
                        if (rightsBundle.isProgrammeManagerRights()) {
                            hqlCommand.append("     pmr.label IN (:channelIds) ");
                        }
                        if ((rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights()) && rightsBundle.isListenerRights()) {
                            hqlCommand.append(" OR ");
                        }
                        if (rightsBundle.isListenerRights()) {
                            hqlCommand.append("     lr.label IN (:channelIds) ");
                        }
                    }
                    if (!programmeIds.isEmpty()) {
                        if ((!channelIds.isEmpty() && (rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights())) && rightsBundle.isAnimatorRights()) {
                            hqlCommand.append(" OR ");
                        }
                        if (rightsBundle.isAnimatorRights()) {
                            hqlCommand.append("     ar.label IN (:programmeIds) ");
                        }
                        if ((!channelIds.isEmpty() && (rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights()) || rightsBundle.isAnimatorRights()) && rightsBundle.isCuratorRights()) {
                            hqlCommand.append(" OR ");
                        }
                        if (rightsBundle.isCuratorRights()) {
                            hqlCommand.append("     cur.label IN (:programmeIds) ");
                        }
                        if ((!channelIds.isEmpty() && (rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights()) || rightsBundle.isAnimatorRights() || rightsBundle.isCuratorRights()) && rightsBundle.isContributorRights()) {
                            hqlCommand.append(" OR ");
                        }
                        if (rightsBundle.isContributorRights()) {
                            hqlCommand.append("     cor.label IN (:programmeIds) ");
                        }
                    }

                    Query query = session.createQuery(hqlCommand.toString());
                    if (!channelIds.isEmpty() && (rightsBundle.isChannelAdministratorRights() || rightsBundle.isProgrammeManagerRights() || rightsBundle.isListenerRights())) {
                        query.setParameterList("channelIds", channelIds);
                    }
                    if (!programmeIds.isEmpty() && (rightsBundle.isAnimatorRights() || rightsBundle.isCuratorRights() || rightsBundle.isContributorRights())) {
                        query.setParameterList("programmeIds", programmeIds);
                    }
                    return query.list();
                }
            });

            return result;
        } else {
            return new HashSet<RestrictedUser>();
        }
    }*/
}
