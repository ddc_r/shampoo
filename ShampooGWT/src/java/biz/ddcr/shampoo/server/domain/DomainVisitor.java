/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface.Visitor;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.domain.track.filter.Filter;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.webservice.Hit;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface DomainVisitor extends Visitor {
    public void visit(Archive object) throws Exception;
    public void visit(Report object) throws Exception;
    public void visit(Channel object) throws Exception;
    public void visit(Log object) throws Exception;
    public void visit(Notification object) throws Exception;
    public void visit(Playlist object) throws Exception;
    public void visit(Programme object) throws Exception;
    public void visit(QueueItem object) throws Exception;
    public void visit(TimetableSlot object) throws Exception;
    public void visit(BroadcastableTrack object) throws Exception;
    public void visit(PendingTrack object) throws Exception;
    public void visit(PlaylistEntry object) throws Exception;
    public void visit(StreamItem object) throws Exception;
    public void visit(Filter object) throws Exception;
    public void visit(Request object) throws Exception;
    public void visit(Voting object) throws Exception;
    public void visit(User object) throws Exception;
    public void visit(Webservice object) throws Exception;
    public void visit(Hit object) throws Exception;
}
