/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain;

/**
 *
 * Provides an alternative equals() method, more thorough aand free of business keys
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface DuplicateCheckingInterface<T> {

    /**
     * Alternative equality test. Inspect all attributes this time, except primary keys.
     * @param otherPlaylistEntry
     * @return
     */
    public abstract boolean isDuplicate(T otherObject);
    /**
     * Same as isDuplicate(T) except that reverse relationships can be verified or not, so as to avoid circular checks
     */
    public abstract boolean isDuplicate(boolean doReverseChecking, T otherObject);
}
