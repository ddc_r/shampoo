/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain;

import biz.ddcr.shampoo.server.domain.user.User;
import java.sql.Timestamp;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface GenericPersistentEntityInterface extends GenericEntityInterface {

    @Override
    public int getVersion();
    @Override
    public void setVersion(int version);

    public void setLatestModificationDate(Timestamp latestModificationDate);
    public Timestamp getLatestModificationDate();

    public void setCreationDate(Timestamp creationDate);
    public Timestamp getCreationDate();

    public void setCreator(User creator);
    public User getCreator();

    public User getLatestEditor();
    public void setLatestEditor(User latestEditor);
    
}
