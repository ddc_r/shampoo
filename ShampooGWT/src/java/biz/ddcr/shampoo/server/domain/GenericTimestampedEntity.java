/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain;

import biz.ddcr.shampoo.server.domain.user.User;
import java.sql.Timestamp;

/**
 *
 * The basic and generic class inherited by all entities persisted in the database, i.e. all the pivotal objects manipulated by the application such as channels, applications, tracks, etc
 *
 * @author okay_awright
 **/
public abstract class GenericTimestampedEntity implements GenericPersistentEntityInterface {

    /** Hibernate versioning */
    private int version;
    /** Immutable logs, i.e. when the log entries are pruned they will remain, even if the creator or the editor is not a user anymore **/
    private User creator;
    private Timestamp creationDate;
    private User latestEditor;
    private Timestamp latestModificationDate;

    protected GenericTimestampedEntity() {
    }
    public GenericTimestampedEntity(GenericTimestampedEntity o) {
        setCreator( o.getCreator() );
        setLatestEditor( o.getLatestEditor() );
        setLatestModificationDate( o.getLatestModificationDate() );
        setCreationDate( o.getCreationDate() );
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public void setLatestModificationDate(Timestamp latestModificationDate) {
        this.latestModificationDate = latestModificationDate;
    }

    @Override
    public Timestamp getLatestModificationDate() {
        return latestModificationDate;
    }

    @Override
    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
    
    //Automatically specified if null
    @Override
    public Timestamp getCreationDate() {
        if (creationDate==null)
            creationDate = new Timestamp(System.currentTimeMillis());
        return creationDate;
    }

    //Do only update the creator if none is already linked to
    @Override
    public void setCreator(User creator) {
        if (this.creator==null)
            this.creator = creator;
    }

    @Override
    public User getCreator() {
        return creator;
    }

    @Override
    public User getLatestEditor() {
        return latestEditor;
    }

    @Override
    public void setLatestEditor(User latestEditor) {
        this.latestEditor = latestEditor;
    }

    /**
     * Return all channels linked to this entity, either directly or indirectly
     * The returned collection is not mutable
     * @return
     */
    //public abstract Set<Channel> getDependentChannelsImmutableSet();
    
    /*@Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be GenericTimestampedEntity at this point
        GenericTimestampedEntity test = (GenericTimestampedEntity)obj;
        return super.equals(test)
                //&& (getVersion() == test.getVersion())
                && (getCreator() == test.getCreator() || (getCreator() != null && getCreator().equals(test.getCreator())))
                //&& (getLatestEditor() == test.getLatestEditor() || (getLatestEditor() != null && getLatestEditor().equals(test.getLatestEditor())))
                && (getCreationDate() == test.getCreationDate() || (getCreationDate() != null && getCreationDate().equals(test.getCreationDate())))
                //&& (getLatestModificationDate() == test.getLatestModificationDate() || (getLatestModificationDate() != null && getLatestModificationDate().equals(test.getLatestModificationDate())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //Cannot put the User hashcode herein or else we face some serious recursive problems
        hash = 31 * hash + this.getVersion();
        hash = 31 * hash + (null == getCreationDate() ? 0 : getCreationDate().hashCode());
        hash = 31 * hash + (null == getLatestModificationDate() ? 0 : getLatestModificationDate().hashCode());
        return hash;
    }*/
}
