package biz.ddcr.shampoo.server.domain;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import org.joda.time.DateTimeZone;

/**
 * Time-based note skeleton, used by either logs or notifications
 *
 * @author okay_awright
 */
public abstract class TimeBasedNote implements Comparable, GenericEntityInterface {

    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();

    /** Hibernate versioning */
    private int version;
    private String refID;
    private YearMonthWeekDayHourMinuteSecondMillisecond dateStamp;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;

    protected TimeBasedNote() {
    }
    public TimeBasedNote(TimeBasedNote o) {
        setDateStamp( o.getDateStamp() );
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    public String getRefID() {
        if (refID==null) refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public static String getFixedTimezone() {
        return LOG_TIMEZONE;
    }

    protected Long getLogTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (dateStamp == null) {
            return _timestamp;
        } else {
            return getDateStamp().getUnixTime();
        }
    }

    protected void setLogTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond getDateStamp() {
        if (dateStamp == null && _timestamp != null) {
            dateStamp = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return dateStamp;
    }

    public void setDateStamp(YearMonthWeekDayHourMinuteSecondMillisecond dateStamp) {
        this.dateStamp = dateStamp;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // actee must be User at this point
        TimeBasedNote test = ProxiedClassUtil.cast(obj, TimeBasedNote.class);
        return /*super.equals(test)
                &&*/ (getRefID().equals(test.getRefID()))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getRefID() ? 0 : getRefID().hashCode());
        return hash;
    }

    @Override
    public int compareTo(Object anotherObject) {
        if (anotherObject != null && TimeBasedNote.class.isAssignableFrom(anotherObject.getClass())) {
            TimeBasedNote anotherTimeBasedNote = (TimeBasedNote)anotherObject;
            //Sort by date, descending order
            if (anotherTimeBasedNote.getDateStamp()==null) return 0;
            return anotherTimeBasedNote.getDateStamp().compareTo(getDateStamp());
        }
        return 0;
    }

}
