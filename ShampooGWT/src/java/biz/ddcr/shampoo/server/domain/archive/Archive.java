package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 * A track or a live that has been played on a specific channel
 * Its reference is mostly recorded for logging purpose, but it can be also used by a streamer for computing a working queue
 * dateStamp marks the time of play for this item
 * Most attributes are specified as soft references only, the objects may be removed the attributes will remain
 *
 * @author okay_awright
 **/
public abstract class Archive extends GenericTimestampedEntity implements ArchiveInterface {

    private String refID;
    private ArchiveLocation location;

    private String playlist;

    private String timetableSlot;

    /**
     * marks the length of play for this item; in milliseconds
     * can be null if the item has been put in the archives but has still not been played in full
     */
    private Long duration;

    /**soft reference to the playlist it belongs to **/
    private String playlistCaption;
    /**soft reference to the programme it belongs to **/
    private String programmeCaption;
    
    protected Archive() {
    }

    public Archive(
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            TimetableSlot timetableSlot) {
        addChannel(channel);
        setStartTimeWithCorrection(startTime);
        setDuration(duration);
        if (timetableSlot!=null) {
            if (timetableSlot.getPlaylist()!=null) {
                setPlaylist(timetableSlot.getPlaylist().getRefID());
                setPlaylistCaption(timetableSlot.getPlaylist().getLabel());
                if (timetableSlot.getPlaylist().getProgramme()!=null)
                    setProgrammeCaption(timetableSlot.getPlaylist().getProgramme().getLabel());
            }
        setTimetableSlot( timetableSlot!=null ? timetableSlot.getRefID() : null );
        }
    }
    public Archive(
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            String timetableSlotId,
            String playlistCaption,
            String programmeCaption) {
        addChannel(channel);
        setStartTimeWithCorrection(startTime);
        setDuration(duration);
        setPlaylist(null);
        setPlaylistCaption(playlistCaption);
        setProgrammeCaption(programmeCaption);
        setTimetableSlot(timetableSlotId);
    }

    public Archive(Archive o) {
        super(o);
        addChannel( o.getChannel() );
        setStartTimeWithCorrection( o.getStartTime() );
        setDuration( o.getDuration() );
        setPlaylist( o.getPlaylist() );
        setPlaylistCaption( o.getPlaylistCaption() );
        setProgrammeCaption( o.getProgrammeCaption() );
        setTimetableSlot( o.getTimetableSlot() );
    }
    
    @Override
    public abstract String getFriendlyEntryCaption();

    @Override
    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    /** Hibernate artifact**/
    @Override
    public void setRefID(String refID) {
        this.refID = refID;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getStartCalendar() {
        return getLocation().getStartCalendar();
    }

    /** Hibernate artifact**/
    @Override
    public void setLocation(ArchiveLocation location) {
        this.location = location;
    }

    @Override
    public ArchiveLocation getLocation() {
        if (location == null) {
            location = new ArchiveLocation();
        }
        return location;
    }

    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getStartTime() {
        return getLocation().getStartTime();
    }

    protected boolean setStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setStartTimeWithCorrection(date);
    }

    protected boolean setRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setRawStartTime(date);
    }

    @Override
    public Channel getChannel() {
        return getLocation().getChannel();
    }

    @Override
    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    @Override
    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            if (doLink) {
                channel.addArchive(this, false);
            }
            getLocation().setChannel(channel);
        }
    }

    @Override
    public void removeChannel() {
        removeChannel(true);
    }

    @Override
    public void removeChannel(boolean doLink) {
        if (getLocation().getChannel() != null) {
            if (doLink) {
                getLocation().getChannel().removeArchive(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setChannel(null);
        }
    }

    @Override
    public long getDuration() {
        return duration;
    }

    @Override
    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String getPlaylist() {
        return playlist;
    }

    @Override
    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }

    @Override
    public String getPlaylistCaption() {
        return playlistCaption;
    }

    @Override
    public void setPlaylistCaption(String playlistCaption) {
        this.playlistCaption = playlistCaption;
    }

    @Override
    public String getProgrammeCaption() {
        return programmeCaption;
    }

    @Override
    public void setProgrammeCaption(String programmeCaption) {
        this.programmeCaption = programmeCaption;
    }

    @Override
    public String getTimetableSlot() {
        return timetableSlot;
    }

    @Override
    public void setTimetableSlot(String timetableSlot) {
        this.timetableSlot = timetableSlot;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        Archive test = ProxiedClassUtil.cast(obj, Archive.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }

        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLocation() ? 0 : getLocation().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(ArchiveInterface anotherArchive) {
        //Sort by date, then by minute
        //Descending order
        if (anotherArchive!=null)
            return (this.getLocation() == null ?
                0 :
                (anotherArchive.getLocation() == null ?
                    0 :
                    -(getLocation().compareTo(anotherArchive.getLocation()) )));
        else
            return 0;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setListenerRights(true);
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getDeleteRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getNotificationRights() {
        return getDeleteRights();
    }

    public void dropMeDead() {
        removeChannel();
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
