package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.GenericPersistentEntityInterface;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.streamer.GenericPersistableItemInterface;
import java.io.Serializable;
import java.util.Comparator;

/**
 * A track or a live that has been played on a specific channel
 * Its reference is mostly recorded for logging purpose, but it can be also used by a streamer for computing a working queue
 * dateStamp marks the time of play for this item
 * Most attributes are specified as soft references only, the objects may be removed the attributes will remain
 *
 * @author okay_awright
 **/
public interface ArchiveInterface extends GenericPersistentEntityInterface, GenericPersistableItemInterface, Comparable<ArchiveInterface>, VisitorPatternInterface<ArchiveVisitor> {

    /** Used for Hibernate sorting of SortedSets, it must be static! **/
    /** But Ehcache needs it to be Serializable vene if static **/
    public static class HibernateComparator implements Serializable, Comparator<ArchiveInterface> {

        @Override
        public int compare(ArchiveInterface t, ArchiveInterface t1) {
            if (t==null)
                return t1==null?0:-1;
            else
                return t.compareTo(t1);
        }

    }

    /** Do not use: Hibernate artefact **/
    public void setRefID(String refID);

    public String getFriendlyEntryCaption();
    
    public String getPlaylist();
    public void setPlaylist(String playlist);

    public String getTimetableSlot();
    public void setTimetableSlot(String timetableSlot);

    public String getPlaylistCaption();
    public void setPlaylistCaption(String caption);

    public String getProgrammeCaption();
    public void setProgrammeCaption(String caption);

    public ArchiveLocation getLocation();
    /** Do not use: Hibernate artefact **/
    public void setLocation(ArchiveLocation location);

    public void addChannel(Channel channel);
    public void addChannel(Channel channel, boolean doLink);
    public void removeChannel();
    public void removeChannel(boolean doLink);

    @Override
    public long getDuration();
    public void setDuration(long duration);

}
