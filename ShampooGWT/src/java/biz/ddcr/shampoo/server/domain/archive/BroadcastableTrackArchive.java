/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BroadcastableTrackArchive extends QueuedArchive implements QueuedArchiveInterface {

    private String track;
    private String trackAuthorCaption;
    private String trackTitleCaption;
    //Reporting data
    private String trackAlbumCaption;
    private String trackPublisherCaption;
    private String trackCopyrightCaption;

    /** soft reference to the corresponding original queue item; can be null if the item was off the queue **/
    private String queueID;
    
    protected BroadcastableTrackArchive() {
        super();
    }

    public BroadcastableTrackArchive(
            BroadcastableTrack track,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            TimetableSlot timetableSlot,
            String queueID) {
        super( channel, startTime, duration, timetableSlot );
        if (track!=null) {
            setTrack(track.getRefID());
            setTrackAuthorCaption(track.getAuthor());
            setTrackTitleCaption(track.getTitle());
            //Reporting data
            setTrackAlbumCaption(track.getAlbum());
            setTrackPublisherCaption(track.getPublisher());
            setTrackCopyrightCaption(track.getCopyright());
        }
        setQueueID(queueID);
    }
    public BroadcastableTrackArchive(
            BroadcastableTrack track,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            String timetableSlotId,
            String playlistCaption,
            String programmeCaption,
            String queueID) {
        super( channel, startTime, duration, timetableSlotId, playlistCaption, programmeCaption );
        if (track!=null) {
            setTrack(track.getRefID());
            setTrackAuthorCaption(track.getAuthor());
            setTrackTitleCaption(track.getTitle());
            //Reporting data
            setTrackAlbumCaption(track.getAlbum());
            setTrackPublisherCaption(track.getPublisher());
            setTrackCopyrightCaption(track.getCopyright());
        }
        setQueueID(queueID);
    }

    public BroadcastableTrackArchive(BroadcastableTrackArchive o) {
        super( o );
        setTrack(o.getTrack());
        setTrackAuthorCaption(o.getTrackAuthorCaption());
        setTrackTitleCaption(o.getTrackTitleCaption());
        //Reporting data
        setTrackAlbumCaption(o.getTrackAlbumCaption());
        setTrackPublisherCaption(o.getTrackPublisherCaption());
        setTrackCopyrightCaption(o.getTrackCopyrightCaption());
        setQueueID( o.getQueueID() );
    }

    @Override
    public String getFriendlyEntryCaption() {
        return getTrackAuthorCaption() + " - " + getTrackTitleCaption();
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getTrackAuthorCaption() {
        return trackAuthorCaption;
    }

    public void setTrackAuthorCaption(String trackAuthorCaption) {
        this.trackAuthorCaption = trackAuthorCaption;
    }

    public String getTrackTitleCaption() {
        return trackTitleCaption;
    }

    public void setTrackTitleCaption(String trackTitleCaption) {
        this.trackTitleCaption = trackTitleCaption;
    }

    public String getTrackAlbumCaption() {
        return trackAlbumCaption;
    }

    public void setTrackAlbumCaption(String trackAlbumCaption) {
        this.trackAlbumCaption = trackAlbumCaption;
    }

    public String getTrackCopyrightCaption() {
        return trackCopyrightCaption;
    }

    public void setTrackCopyrightCaption(String trackCopyrightCaption) {
        this.trackCopyrightCaption = trackCopyrightCaption;
    }

    public String getTrackPublisherCaption() {
        return trackPublisherCaption;
    }

    public void setTrackPublisherCaption(String trackPublisherCaption) {
        this.trackPublisherCaption = trackPublisherCaption;
    }

    @Override
    public BroadcastableTrackArchive shallowCopy() {
        return new BroadcastableTrackArchive(this);
    }

    @Override
    public String getQueueID() {
        return queueID;
    }

    @Override
    public void setQueueID(String queueID) {
        this.queueID = queueID;
    }

    @Override
    public void acceptVisit(ArchiveVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
