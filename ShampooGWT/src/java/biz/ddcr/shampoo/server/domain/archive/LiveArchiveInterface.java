package biz.ddcr.shampoo.server.domain.archive;

/**
 * An Archive created from a Queued Item
 *
 * @author okay_awright
 **/
public interface LiveArchiveInterface extends ArchiveInterface {
    public String getBroadcasterCaption();

    public void setBroadcasterCaption(String broadcasterCaption);
}
