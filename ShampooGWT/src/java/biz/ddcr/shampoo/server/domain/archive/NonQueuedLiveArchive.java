/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 * Archive of a Non-Queued Live
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class NonQueuedLiveArchive extends NonQueuedArchive implements LiveArchiveInterface, NonQueuedArchiveInterface {

    private String broadcasterCaption;

    protected NonQueuedLiveArchive() {
        super();
    }

    public NonQueuedLiveArchive(
            Live live,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            TimetableSlot timetableSlot) {
        super( channel, startTime, duration, timetableSlot );
        if (live!=null) {
            setBroadcasterCaption(live.getBroadcaster());
        }
    }
    public NonQueuedLiveArchive(
            String broadcasterId,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            String timetableSlotId,
            String playlistCaption,
            String programmeCaption) {
        super( channel, startTime, duration, timetableSlotId, playlistCaption, programmeCaption );
        setBroadcasterCaption(broadcasterId);
    }

    public NonQueuedLiveArchive(NonQueuedLiveArchive o) {
        super( o );
        setBroadcasterCaption(o.getBroadcasterCaption());
    }

    @Override
    public String getFriendlyEntryCaption() {
        return getBroadcasterCaption() + " - " + getPlaylistCaption();
    }

    @Override
    public String getBroadcasterCaption() {
        return broadcasterCaption;
    }

    @Override
    public void setBroadcasterCaption(String broadcasterCaption) {
        this.broadcasterCaption = broadcasterCaption;
    }
    
    @Override
    public NonQueuedLiveArchive shallowCopy() {
        return new NonQueuedLiveArchive(this);
    }

    @Override
    public void acceptVisit(ArchiveVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
