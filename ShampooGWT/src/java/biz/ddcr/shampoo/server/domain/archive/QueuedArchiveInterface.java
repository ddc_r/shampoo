package biz.ddcr.shampoo.server.domain.archive;

/**
 * An Archive created from a Queued Item
 *
 * @author okay_awright
 **/
public interface QueuedArchiveInterface extends ArchiveInterface {
   
    public String getQueueID();

    public void setQueueID(String queueID);
    
}
