package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 * An Archive created from a Queued Item
 *
 * @author okay_awright
 **/
public class QueuedLiveArchive extends QueuedArchive implements QueuedArchiveInterface, LiveArchiveInterface {
   
    private String broadcasterCaption;
    /** soft reference to the corresponding original queue item; can be null if the item was off the queue **/
    private String queueID;
    
    protected QueuedLiveArchive() {
        super();
    }

    public QueuedLiveArchive(
            Live live,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            TimetableSlot timetableSlot,
            String queuedID) {
        super( channel, startTime, duration, timetableSlot );
        if (live!=null) {
            setBroadcasterCaption(live.getBroadcaster());
        }
        setQueueID(queuedID);
    }
    public QueuedLiveArchive(
            String broadcasterId,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            String timetableSlotId,
            String playlistCaption,
            String programmeCaption,
            String queuedID) {
        super( channel, startTime, duration, timetableSlotId, playlistCaption, programmeCaption );
        setBroadcasterCaption(broadcasterId);
        setQueueID(queuedID);
    }

    public QueuedLiveArchive(QueuedLiveArchive o) {
        super( o );
        setBroadcasterCaption(o.getBroadcasterCaption());
        setQueueID( o.getQueueID() );
    }

    @Override
    public String getFriendlyEntryCaption() {
        return getBroadcasterCaption() + " - " + getPlaylistCaption();
    }

    @Override
    public String getBroadcasterCaption() {
        return broadcasterCaption;
    }

    @Override
    public void setBroadcasterCaption(String broadcasterCaption) {
        this.broadcasterCaption = broadcasterCaption;
    }
    
    @Override
    public String getQueueID() {
        return queueID;
    }

    @Override
    public void setQueueID(String queueID) {
        this.queueID = queueID;
    }

    @Override
    public QueuedLiveArchive shallowCopy() {
        return new QueuedLiveArchive(this);
    }

    @Override
    public void acceptVisit(ArchiveVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
