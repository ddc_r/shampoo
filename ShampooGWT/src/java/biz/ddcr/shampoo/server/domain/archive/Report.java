package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.archive.format.TextFileInfo;
import biz.ddcr.shampoo.server.helper.*;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 * A set of archives collected for auditing or logging Mostly useful for
 * copyright collecting agencies
 *
 * @author okay_awright
 *
 */
public class Report extends GenericTimestampedEntity implements Comparable<Report> {

    private String refID;
    private ReportLocation location;
    /**
     * special flag that specifies if the file represented by the report is
     * available for processing. The report may, for example, be generated or
     * still being transfered to the datastore and thus is not yet ready for
     * viewing
     *
     */
    private boolean ready;

    /** metadata of the current object linked to this report; null if no file */
    private TextFileInfo textContainer;
    
    protected Report() {
    }

    public Report(
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime) {
        addChannel(channel);
        setStartTimeWithCorrection(startTime);
        setEndTimeWithCorrection(endTime);
        //Deactivated by default
        setReady(false);
    }

    public Report(Report o) {
        super(o);
        addChannel(o.getChannel());
        setStartTimeWithCorrection(o.getStartTime());
        setEndTimeWithCorrection(o.getEndTime());
        setReady( o.isReady() );
        setTextContainer( o.getTextContainer() );
    }

    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    /**
     * Hibernate artifact*
     */
    public void setRefID(String refID) {
        this.refID = refID;
    }

    public TextFileInfo getTextContainer() {
        return textContainer;
    }

    public void setTextContainer(TextFileInfo textContainer) {
        this.textContainer = textContainer;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getStartCalendar() {
        return getLocation().getStartCalendar();
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar() {
        return getLocation().getEndCalendar();
    }

    /**
     * Hibernate artifact*
     */
    public void setLocation(ReportLocation location) {
        this.location = location;
    }

    public ReportLocation getLocation() {
        if (location == null) {
            location = new ReportLocation();
        }
        return location;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getStartTime() {
        return getLocation().getStartTime();
    }

    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getEndTime() {
        return getLocation().getEndTime();
    }

    protected boolean setStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setStartTimeWithCorrection(date);
    }

    protected boolean setEndTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setEndTimeWithCorrection(date);
    }

    protected boolean setRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setRawStartTime(date);
    }

    protected boolean setRawEndTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setRawEndTime(date);
    }

    public Channel getChannel() {
        return getLocation().getChannel();
    }

    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            if (doLink) {
                channel.addReport(this, false);
            }
            getLocation().setChannel(channel);
        }
    }

    public void removeChannel() {
        removeChannel(true);
    }

    public void removeChannel(boolean doLink) {
        if (getLocation().getChannel() != null) {
            if (doLink) {
                getLocation().getChannel().removeReport(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setChannel(null);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        Report test = ProxiedClassUtil.cast(obj, Report.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }

        return /*
                 * super.equals(test)
                &&
                 */ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLocation() ? 0 : getLocation().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(Report anotherReport) {
        //Sort by date, then by minute
        //Descending order
        if (anotherReport != null) {
            return (this.getLocation() == null
                    ? 0
                    : (anotherReport.getLocation() == null
                    ? 0
                    : -(getLocation().compareTo(anotherReport.getLocation()))));
        } else {
            return 0;
        }
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    public static RightsBundle getAddRights() {
        return getDeleteRights();
    }

    public static RightsBundle getUpdateRights() {
        return getDeleteRights();
    }
    
    public static RightsBundle getDeleteRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    public static RightsBundle getNotificationRights() {
        return getDeleteRights();
    }

    public void dropMeDead() {
        removeChannel();
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new Report(this);
    }    
    
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
