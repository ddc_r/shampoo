package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;

/**
 * The SpecialPlaylist TimetableSlot for a given Channel SpecialPlaylists can be
 * freely scheduled
 *
 * @author okay_awright
 */
public class ReportLocation implements Serializable, Comparable<ReportLocation> {

    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    private YearMonthWeekDayHourMinuteSecondMillisecond endCalendar;
    /**
     * Do not use it: internal ORM placeholder *
     */
    private Long _starttimestamp;
    /**
     * Do not use it: internal ORM placeholder *
     */
    private Long _endtimestamp;
    /**
     * Where this slot will be played
     */
    private Channel channel;

    protected ReportLocation() {
    }

    public ReportLocation(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecond startDate, YearMonthWeekDayHourMinuteSecondMillisecond endDate) {
        //Channel first, since setting time requires the channel timezone
        setChannel(channel);
        setStartTimeWithCorrection(startDate);
        setEndTimeWithCorrection(endDate);
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getTimeZone() {
        return getChannel().getTimezone();
    }

    protected Long getStartTimestamp() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _starttimestamp;
        } else {
            return getStartCalendar().getUnixTime();
        }
    }

    protected Long getEndTimestamp() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endtimestamp;
        } else {
            return getEndCalendar().getUnixTime();
        }
    }

    protected void setStartTimestamp(Long unixEpoch) {
        _starttimestamp = unixEpoch;
    }

    protected void setEndTimestamp(Long unixEpoch) {
        _endtimestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getStartCalendar() {
        if (startCalendar == null && _starttimestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _starttimestamp,
                    getTimeZone());
        }
        return startCalendar;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar() {
        if (endCalendar == null && _endtimestamp != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _endtimestamp,
                    getTimeZone());
        }
        return endCalendar;
    }

    /**
     *
     * Specifies when this report starts
     *
     *
     */
    public YearMonthWeekDayHourMinuteSecondMillisecond getStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getStartCalendar().getYear(),
                getStartCalendar().getMonthOfYear(),
                getStartCalendar().getDayOfMonth(),
                getStartCalendar().getHourOfDay(),
                getStartCalendar().getMinuteOfHour(),
                getStartCalendar().getSecondOfMinute(),
                getStartCalendar().getMillisecondOfSecond(),
                getStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies when this report stops
     *
     *
     */
    public YearMonthWeekDayHourMinuteSecondMillisecond getEndTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getEndCalendar().getYear(),
                getEndCalendar().getMonthOfYear(),
                getEndCalendar().getDayOfMonth(),
                getEndCalendar().getHourOfDay(),
                getEndCalendar().getMinuteOfHour(),
                getEndCalendar().getSecondOfMinute(),
                getEndCalendar().getMillisecondOfSecond(),
                getEndCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time the report begins The date passed as a parameter
     * will be corrected to the current timezone
     *
     *
     */
    public boolean setStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    getTimeZone());
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time the report ends The date passed as a parameter
     * will be corrected to the current timezone
     *
     *
     */
    public boolean setEndTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    getTimeZone());
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this report begins The date passed as a parameter
     * will NOT be re-interpreted in the current timezone and be used as-is
     *
     *
     */
    public boolean setRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getStartCalendar().setYear(date.getYear());
            getStartCalendar().setMonthOfYear(date.getMonthOfYear());
            getStartCalendar().setDayOfMonth(date.getDayOfMonth());
            getStartCalendar().setHourOfDay(date.getHourOfDay());
            getStartCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getStartCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getStartCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this report ends The date passed as a parameter
     * will NOT be re-interpreted in the current timezone and be used as-is
     *
     *
     */
    public boolean setRawEndTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getEndCalendar().setYear(date.getYear());
            getEndCalendar().setMonthOfYear(date.getMonthOfYear());
            getEndCalendar().setDayOfMonth(date.getDayOfMonth());
            getEndCalendar().setHourOfDay(date.getHourOfDay());
            getEndCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getEndCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getEndCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        ReportLocation test = ProxiedClassUtil.cast(obj, ReportLocation.class);
        return /*
                 * super.equals(test)
                &&
                 */ (getChannel() == test.getChannel() || (getChannel() != null && getChannel().equals(test.getChannel())))
                && (getStartCalendar() == test.getStartCalendar() || (getStartCalendar() != null && getStartCalendar().equals(test.getStartCalendar())))
                && (getEndCalendar() == test.getEndCalendar() || (getEndCalendar() != null && getEndCalendar().equals(test.getEndCalendar())));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getChannel() ? 0 : getChannel().hashCode());
        hash = 31 * hash + (null == getStartCalendar() ? 0 : getStartCalendar().hashCode());
        hash = 31 * hash + (null == getEndCalendar() ? 0 : getEndCalendar().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(ReportLocation anotherReportLocation) {
        //Sort by channel, then by time boundaries, then by format
        if (anotherReportLocation != null) {
            int diff = this.getChannel() == null
                    ? 0
                    : (anotherReportLocation.getChannel() == null
                    ? 0
                    : getChannel().compareTo(anotherReportLocation.getChannel()));
            if (diff == 0) {
                diff = this.getStartCalendar() == null
                        ? 0
                        : (anotherReportLocation.getStartCalendar() == null
                        ? 0
                        : getStartCalendar().compareTo(anotherReportLocation.getStartCalendar()));
            }
            if (diff == 0) {
                diff = this.getEndCalendar() == null
                        ? 0
                        : (anotherReportLocation.getEndCalendar() == null
                        ? 0
                        : getEndCalendar().compareTo(anotherReportLocation.getEndCalendar()));
            }
            return diff;
        } else {
            return 0;
        }
    }
}
