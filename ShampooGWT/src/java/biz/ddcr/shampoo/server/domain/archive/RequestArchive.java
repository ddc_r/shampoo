/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.archive;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class RequestArchive extends BroadcastableTrackArchive implements QueuedArchiveInterface {

    private String requester;

    protected RequestArchive() {
        super();
    }

    public RequestArchive(
            BroadcastableTrack track,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            TimetableSlot timetableSlot,
            RestrictedUser requester,
            String queueID) {
        super( track, channel, startTime, duration, timetableSlot, queueID );
        setRequester( requester!=null ? requester.getUsername() : null );
    }
    public RequestArchive(
            BroadcastableTrack track,
            Channel channel,
            YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime,
            Long duration,
            String timetableSlotId,
            String playlistCaption,
            String programmeCaption,
            RestrictedUser requester,
            String queueID) {
        super( track, channel, startTime, duration, timetableSlotId, playlistCaption, programmeCaption, queueID );
        setRequester( requester!=null ? requester.getUsername() : null );
    }

    public RequestArchive(RequestArchive o) {
        super( o );
        setRequester( o.getRequester() );
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    @Override
    public void acceptVisit(ArchiveVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
