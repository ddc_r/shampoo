/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.archive.format;

import biz.ddcr.shampoo.server.domain.archive.format.TextFormat.TEXT_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.*;

/**
 *
 * @author okay_awright
 **/
public class TextFileInfo implements FileInfoInterface<TEXT_FORMAT> {

    private TEXT_FORMAT format;
    /** size in bytes */
    private long size;

    public TextFileInfo() {
    }

    public TextFileInfo(FileInfoInterface<TEXT_FORMAT> objectFileInfo) {
        if (objectFileInfo!=null) {
            setFormat(objectFileInfo.getFormat());
            setSize(objectFileInfo.getSize());
        }
    }

    @Override
    public TEXT_FORMAT getFormat() {
        return format;
    }

    public void setFormat(TEXT_FORMAT format) {
        this.format = format;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public long getSize() {
        return size;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TextFileInfo other = (TextFileInfo) obj;
        if (this.format != other.format && (this.format == null || !this.format.equals(other.format))) {
            return false;
        }
        if (this.size != other.size) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.format != null ? this.format.hashCode() : 0);
        hash = 59 * hash + (int) (this.size ^ (this.size >>> 32));
        return hash;
    }
    
    @Override
    public int fullHashCode() {
        return hashCode();
    }
}
