/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.domain.archive.format;

import biz.ddcr.shampoo.server.domain.track.format.*;
import biz.ddcr.shampoo.server.io.helper.MIME;
import biz.ddcr.shampoo.server.io.helper.MIME.CATEGORY;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class TextFormat {

    public enum TEXT_FORMAT implements FILE_FORMAT {
        structured;

        public final static String FILE_NAME_SUFFIX = "_t";

        @Override
        public String getFileNameSuffix() {
            return FILE_NAME_SUFFIX;
        }
    }

    public static final EnumMap<TextFormat.TEXT_FORMAT, Collection<MIME>> MIME_TEXT_FORMAT = new EnumMap<TextFormat.TEXT_FORMAT, Collection<MIME>>(TextFormat.TEXT_FORMAT.class);
    static {
        Collection<MIME> structTextMimes = new HashSet<MIME>();
        structTextMimes.add(new MIME(CATEGORY.textResource, "text", "plain"));
        structTextMimes.add(new MIME(CATEGORY.textResource, "text", "csv"));
        structTextMimes.add(new MIME(CATEGORY.textResource, "application", "json"));
        structTextMimes.add(new MIME(CATEGORY.textResource, "application", "xml"));
        structTextMimes.add(new MIME(CATEGORY.textResource, "text", "xml"));
        
        MIME_TEXT_FORMAT.put(TextFormat.TEXT_FORMAT.structured, structTextMimes);
    }

    public static final EnumMap<TextFormat.TEXT_FORMAT, Collection<String>> FILE_EXTENSIONS_TEXT_FORMAT = new EnumMap<TextFormat.TEXT_FORMAT, Collection<String>>(TextFormat.TEXT_FORMAT.class);
    static {
        Collection<String> structTextExts = new HashSet<String>();
        structTextExts.add("txt");
        structTextExts.add("csv");
        structTextExts.add("json");
        structTextExts.add("xml");

        FILE_EXTENSIONS_TEXT_FORMAT.put(TextFormat.TEXT_FORMAT.structured, structTextExts);
    }
    
}
