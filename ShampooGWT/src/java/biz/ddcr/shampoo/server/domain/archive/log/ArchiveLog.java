/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.archive.log;

import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.archive.ArchiveInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ArchiveLog extends Log {

    public static enum ARCHIVE_LOG_OPERATION implements LOG_OPERATION {

        delete;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case delete:
                        return systemConfigurationHelper.isLogArchiveDelete();
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, ArchiveInterface archive) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (archive!=null && canLog(systemConfigurationHelper))
                allBoundChannels.add(archive.getChannel());
            return allBoundChannels;
        }
    }
    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** The current operation performed on the actee **/
    private ARCHIVE_LOG_OPERATION action;
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;
    /** What this archive is bound to */
    private String channelCaption;

    protected ArchiveLog() {
        super();
    }

    @Override
    public ArchiveLog shallowCopy() {
        return new ArchiveLog(this);
    }
    
    public ArchiveLog(ARCHIVE_LOG_OPERATION operation, String refID, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, String channelId, Set<Channel> channels) {
        super(refID, channels);
        setAction(operation);
        setArchiveChannelCaption(channelId);
        setArchiveStartTimeWithCorrection(startTime);
    }

    public ArchiveLog(ArchiveLog o) {
        super(o);
        setAction(o.getAction());
        setArchiveChannelCaption(o.getArchiveChannelCaption());
        setArchiveRawStartTime(o.getArchiveStartTime());
    }

    @Override
    public ARCHIVE_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(ARCHIVE_LOG_OPERATION action) {
        this.action = action;
    }

    public String getArchiveChannelCaption() {
        return channelCaption;
    }

    public void setArchiveChannelCaption(String channelCaption) {
        this.channelCaption = channelCaption;
    }

    protected Long getArchiveStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _timestamp;
        } else {
            return getArchiveStartCalendar().getUnixTime();
        }
    }

    protected void setArchiveStartTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getArchiveStartCalendar() {
        if (startCalendar == null && _timestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getArchiveStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getArchiveStartCalendar().getYear(),
                getArchiveStartCalendar().getMonthOfYear(),
                getArchiveStartCalendar().getDayOfMonth(),
                getArchiveStartCalendar().getHourOfDay(),
                getArchiveStartCalendar().getMinuteOfHour(),
                getArchiveStartCalendar().getSecondOfMinute(),
                getArchiveStartCalendar().getMillisecondOfSecond(),
                getArchiveStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    public boolean setArchiveStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setArchiveRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getArchiveStartCalendar().setYear(date.getYear());
            getArchiveStartCalendar().setMonthOfYear(date.getMonthOfYear());
            getArchiveStartCalendar().setDayOfMonth(date.getDayOfMonth());
            getArchiveStartCalendar().setHourOfDay(date.getHourOfDay());
            getArchiveStartCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getArchiveStartCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getArchiveStartCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
