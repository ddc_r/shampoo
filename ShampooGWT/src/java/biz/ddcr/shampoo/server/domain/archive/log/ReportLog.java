/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.archive.log;

import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ReportLog extends Log {

    public static enum REPORT_LOG_OPERATION implements LOG_OPERATION {

        add,
        delete;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case add:
                        return systemConfigurationHelper.isLogReportCreate();
                    case delete:
                        return systemConfigurationHelper.isLogReportDelete();    
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, Report report) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (report!=null && canLog(systemConfigurationHelper))
                allBoundChannels.add(report.getChannel());
            return allBoundChannels;
        }
    }
    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** The current operation performed on the actee **/
    private REPORT_LOG_OPERATION action;
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _startTimestamp;
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond endCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _endTimestamp;
    /** What this archive is bound to */
    private String channelCaption;

    protected ReportLog() {
        super();
    }
    
    @Override
    public ReportLog shallowCopy() {
        return new ReportLog(this);
    }
    
    public ReportLog(REPORT_LOG_OPERATION operation, String refID, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime, String channelId, Set<Channel> channels) {
        super(refID, channels);
        setAction(operation);
        setReportChannelCaption(channelId);
        setReportStartTimeWithCorrection(startTime);
        setReportEndTimeWithCorrection(endTime);
    }

    public ReportLog(ReportLog o) {
        super(o);
        setAction(o.getAction());
        setReportChannelCaption(o.getReportChannelCaption());
        setReportRawStartTime(o.getReportStartTime());
        setReportRawEndTime(o.getReportEndTime());
    }

    @Override
    public REPORT_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(REPORT_LOG_OPERATION action) {
        this.action = action;
    }

    public String getReportChannelCaption() {
        return channelCaption;
    }

    public void setReportChannelCaption(String channelCaption) {
        this.channelCaption = channelCaption;
    }

    protected Long getReportStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _startTimestamp;
        } else {
            return getReportStartCalendar().getUnixTime();
        }
    }

    protected void setReportStartTimestamp(Long unixEpoch) {
        _startTimestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportStartCalendar() {
        if (startCalendar == null && _startTimestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _startTimestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getReportStartCalendar().getYear(),
                getReportStartCalendar().getMonthOfYear(),
                getReportStartCalendar().getDayOfMonth(),
                getReportStartCalendar().getHourOfDay(),
                getReportStartCalendar().getMinuteOfHour(),
                getReportStartCalendar().getSecondOfMinute(),
                getReportStartCalendar().getMillisecondOfSecond(),
                getReportStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    public boolean setReportStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setReportRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getReportStartCalendar().setYear(date.getYear());
            getReportStartCalendar().setMonthOfYear(date.getMonthOfYear());
            getReportStartCalendar().setDayOfMonth(date.getDayOfMonth());
            getReportStartCalendar().setHourOfDay(date.getHourOfDay());
            getReportStartCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getReportStartCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getReportStartCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    protected Long getReportEndTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endTimestamp;
        } else {
            return getReportEndCalendar().getUnixTime();
        }
    }

    protected void setReportEndTimestamp(Long unixEpoch) {
        _endTimestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportEndCalendar() {
        if (endCalendar == null && _endTimestamp != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _endTimestamp,
                    LOG_TIMEZONE);
        }
        return endCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportEndTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getReportEndCalendar().getYear(),
                getReportEndCalendar().getMonthOfYear(),
                getReportEndCalendar().getDayOfMonth(),
                getReportEndCalendar().getHourOfDay(),
                getReportEndCalendar().getMinuteOfHour(),
                getReportEndCalendar().getSecondOfMinute(),
                getReportEndCalendar().getMillisecondOfSecond(),
                getReportEndCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    public boolean setReportEndTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setReportRawEndTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getReportEndCalendar().setYear(date.getYear());
            getReportEndCalendar().setMonthOfYear(date.getMonthOfYear());
            getReportEndCalendar().setDayOfMonth(date.getDayOfMonth());
            getReportEndCalendar().setHourOfDay(date.getHourOfDay());
            getReportEndCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getReportEndCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getReportEndCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }
    
    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
