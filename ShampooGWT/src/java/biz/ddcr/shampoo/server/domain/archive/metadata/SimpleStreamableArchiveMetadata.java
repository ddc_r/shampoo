/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.archive.metadata;

import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.DurationMilliseconds;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller.BasicDataMarshalledChunk;
import biz.ddcr.shampoo.server.io.serializer.MarshallableSupporter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SimpleStreamableArchiveMetadata implements Serializable, Copiable, StreamableArchiveMetadataInterface {

    private String author;
    private String title;
    private String album;
    private String typeCaption;
    private String playlistLabel;
    private String programmeLabel;
    private String channelLabel;
    private Long startTime;
    /**
     * associated timezone
     */
    private String timezone;
    private Long duration;
    private String publisher;
    private String copyright;

    public SimpleStreamableArchiveMetadata() {
    }

    public SimpleStreamableArchiveMetadata(SimpleStreamableArchiveMetadata o) {
        setAuthor(o.getAuthor());
        setTitle(o.getTitle());
        setAlbum(o.getAlbum());
        setPlaylistLabel(o.getPlaylistLabel());
        setProgrammeLabel(o.getProgrammeLabel());
        setChannelLabel(o.getChannelLabel());
        setStartTime(o.getStartTime());
        setTimezone(o.getTimezone());
        setDuration(o.getDuration());
        setPublisher(o.getPublisher());
        setCopyright(o.getCopyright());
        setTypeCaption(o.getTypeCaption());
    }

    @Override
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getChannelLabel() {
        return channelLabel;
    }

    public void setChannelLabel(String channelLabel) {
        this.channelLabel = channelLabel;
    }

    @Override
    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    @Override
    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    @Override
    public String getPlaylistLabel() {
        return playlistLabel;
    }

    public void setPlaylistLabel(String playlistLabel) {
        this.playlistLabel = playlistLabel;
    }

    @Override
    public String getProgrammeLabel() {
        return programmeLabel;
    }

    public void setProgrammeLabel(String programmeLabel) {
        this.programmeLabel = programmeLabel;
    }

    @Override
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Override
    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getTypeCaption() {
        return typeCaption;
    }

    public void setTypeCaption(String typeCaption) {
        this.typeCaption = typeCaption;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleStreamableArchiveMetadata other = (SimpleStreamableArchiveMetadata) obj;
        if ((this.author == null) ? (other.author != null) : !this.author.equals(other.author)) {
            return false;
        }
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.album == null) ? (other.album != null) : !this.album.equals(other.album)) {
            return false;
        }
        if ((this.typeCaption == null) ? (other.typeCaption != null) : !this.typeCaption.equals(other.typeCaption)) {
            return false;
        }
        if ((this.playlistLabel == null) ? (other.playlistLabel != null) : !this.playlistLabel.equals(other.playlistLabel)) {
            return false;
        }
        if ((this.programmeLabel == null) ? (other.programmeLabel != null) : !this.programmeLabel.equals(other.programmeLabel)) {
            return false;
        }
        if ((this.channelLabel == null) ? (other.channelLabel != null) : !this.channelLabel.equals(other.channelLabel)) {
            return false;
        }
        if (this.startTime != other.startTime && (this.startTime == null || !this.startTime.equals(other.startTime))) {
            return false;
        }
        if ((this.timezone == null) ? (other.timezone != null) : !this.timezone.equals(other.timezone)) {
            return false;
        }
        if (this.duration != other.duration && (this.duration == null || !this.duration.equals(other.duration))) {
            return false;
        }
        if ((this.publisher == null) ? (other.publisher != null) : !this.publisher.equals(other.publisher)) {
            return false;
        }
        if ((this.copyright == null) ? (other.copyright != null) : !this.copyright.equals(other.copyright)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (this.author != null ? this.author.hashCode() : 0);
        hash = 73 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 73 * hash + (this.album != null ? this.album.hashCode() : 0);
        hash = 73 * hash + (this.typeCaption != null ? this.typeCaption.hashCode() : 0);
        hash = 73 * hash + (this.playlistLabel != null ? this.playlistLabel.hashCode() : 0);
        hash = 73 * hash + (this.programmeLabel != null ? this.programmeLabel.hashCode() : 0);
        hash = 73 * hash + (this.channelLabel != null ? this.channelLabel.hashCode() : 0);
        hash = 73 * hash + (this.startTime != null ? this.startTime.hashCode() : 0);
        hash = 73 * hash + (this.timezone != null ? this.timezone.hashCode() : 0);
        hash = 73 * hash + (this.duration != null ? this.duration.hashCode() : 0);
        hash = 73 * hash + (this.publisher != null ? this.publisher.hashCode() : 0);
        hash = 73 * hash + (this.copyright != null ? this.copyright.hashCode() : 0);
        return hash;
    }

    @Override
    public int fullHashCode() {
        return hashCode();
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new SimpleStreamableArchiveMetadata(this);
    }

    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public void supports(MarshallableSupporter otherObject) throws Exception {
        otherObject.support(this);
    }

    @Override
    public BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getAuthor());
                s.add(getTitle());
                s.add(getAlbum());
                s.add(getPlaylistLabel());
                s.add(getProgrammeLabel());
                s.add(getChannelLabel());
                if (userFriendlyUnitValues) {
                    s.add(getStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(new DurationMilliseconds(getDuration()).getFriendlyEnglishCaption());
                } else {
                    s.add(getStartTime() != null ? getStartTime().toString() : null);
                    s.add(getDuration() != null ? getDuration().toString() : null);
                }
                s.add(getPublisher());
                s.add(getCopyright());
                s.add(getTypeCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("author", getAuthor());
                m.put("title",getTitle());
                m.put("album",getAlbum());
                m.put("playlistLabel",getPlaylistLabel());
                m.put("programmeLabel",getProgrammeLabel());
                m.put("channelLabel",getChannelLabel());
                if (userFriendlyUnitValues) {
                    m.put("startTime", getStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    m.put("duration", new DurationMilliseconds(getDuration()).getFriendlyEnglishCaption());
                } else {
                    m.put("startTime", getStartTime());
                    m.put("duration", getDuration());
                }
                m.put("publisher", getPublisher());
                m.put("copyright", getCopyright());
                m.put("typeCaption", getTypeCaption());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("author", getAuthor());
                m.put("title",getTitle());
                m.put("album",getAlbum());
                m.put("playlist-label",getPlaylistLabel());
                m.put("programme-label",getProgrammeLabel());
                m.put("channel-label",getChannelLabel());
                if (userFriendlyUnitValues) {
                    m.put("start-time", getStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    m.put("duration", new DurationMilliseconds(getDuration()).getFriendlyEnglishCaption());
                } else {
                    m.put("start-time", getStartTime());
                    m.put("duration", getDuration());
                }
                m.put("publisher", getPublisher());
                m.put("copyright", getCopyright());
                m.put("type-caption", getTypeCaption());
                return MarshallUtil.toSimpleXML("archive",m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("artist", getAuthor());
                m.put("title",getTitle());
                m.put("album",getAlbum());
                m.put("playlist_label",getPlaylistLabel());
                m.put("programme_id",getProgrammeLabel());
                m.put("channel_id",getChannelLabel());
                m.put("liq_queue_start_time", getStartTime());
                m.put("liq_cue_in", new Float(0.0));
                m.put("liq_cue_out", getDuration() != null ? new Float(getDuration()/1000.0) : null);                
                m.put("publisher", getPublisher());
                m.put("copyright", getCopyright());
                m.put("type", getTypeCaption());
                return MarshallUtil.toSimpleAnnotate(m, null);
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("artist", getAuthor());
                m.put("title",getTitle());
                m.put("album",getAlbum());
                m.put("playlist_label",getPlaylistLabel());
                m.put("programme_id",getProgrammeLabel());
                m.put("channel_id",getChannelLabel());
                m.put("liq_queue_start_time", getStartTime()!=null?getStartTime().toString():null);
                m.put("liq_cue_in", (new Float(0.0)).toString());
                m.put("liq_cue_out", getDuration() != null ? (new Float(getDuration()/1000.0)).toString() : null);
                m.put("publisher", getPublisher());
                m.put("copyright", getCopyright());
                m.put("type", getTypeCaption());
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }    
}
