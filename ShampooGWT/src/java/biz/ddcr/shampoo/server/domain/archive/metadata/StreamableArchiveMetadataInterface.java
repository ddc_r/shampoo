/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.archive.metadata;

import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface StreamableArchiveMetadataInterface extends SerializableMetadataInterface  {

    public String getPlaylistLabel();
    public String getProgrammeLabel();
    public String getChannelLabel();
    public String getAuthor();
    public String getTitle();
    public String getAlbum();
    public String getTypeCaption();
    public String getPublisher();
    public String getCopyright();
    /** Unix Epoch 64bit */
    public Long getStartTime();
    /** display timezone */
    public String getTimezone();
    /** Milliseconds**/
    public Long getDuration();
    
}
