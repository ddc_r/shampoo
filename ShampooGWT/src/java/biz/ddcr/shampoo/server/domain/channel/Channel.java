package biz.ddcr.shampoo.server.domain.channel;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.archive.ArchiveInterface;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.util.*;

/**
 * An actual channel station
 *
 * @author okay_awright
 */
public class Channel extends GenericTimestampedEntity implements Comparable<Channel> {

    private String label;
    private String description;
    private String timezone;
    private Set<RestrictedUser> channelAdministratorRights;
    private Set<RestrictedUser> programmeManagerRights;
    private Set<RestrictedUser> listenerRights;
    private Set<Programme> programmes;
    private Set<TimetableSlot> timetableSlots;
    private Set<Log> logs;
    private SortedSet<QueueItem> queueItems;
    private StreamItem streamItem;
    private Long seatNumber;
    private SortedSet<ArchiveInterface> archives;
    private Set<Report> reports;

    /** public webservices **/
    private Set<Webservice> webservices;

    private Set<Request> requests;

    private Integer maxDailyRequestLimitPerUser;
    
    //Misc. data, only used along with Icecast metadata
    private String url;
    private String tag;

    //Is the channel open for self registration as listeners by both authenticated and non-authenticated users?
    private boolean openRegistration;

    public Channel() {
    }

    public Channel(Channel o) {
        super(o);
        addChannelAdministratorRights(o.getChannelAdministratorRights());
        setDescription(o.getDescription());
        setLabel(o.getLabel());
        addListenerRights(o.getListenerRights());
        addProgrammeManagerRights(o.getProgrammeManagerRights());
        addProgrammes(o.getProgrammes());
        addTimetableSlots(o.getTimetableSlots());
        setTimezone(o.getTimezone());
        addLogs( o.getLogs() );
        addQueueItems( o.getQueueItems() );
        setSeatNumber( o.getSeatNumber() );
        setMaxDailyRequestLimitPerUser( o.getMaxDailyRequestLimitPerUser() );
        addArchives( o.getArchives() );
        addReports( o.getReports() );
        setUrl( o.getUrl() );
        setTag( o.getTag() );
        addStreamItem( o.getStreamItem() );
        addRequests( o.getRequests() );
        setOpenRegistration( o.isOpenRegistration() );
        addWebservices( o.getWebservices() );
    }

    public boolean isOpenRegistration() {
        return openRegistration;
    }

    public void setOpenRegistration(boolean openRegistration) {
        this.openRegistration = openRegistration;
    }

    public Integer getMaxDailyRequestLimitPerUser() {
        return maxDailyRequestLimitPerUser;
    }

    public void setMaxDailyRequestLimitPerUser(Integer maxDailyRequestLimitPerUser) {
        this.maxDailyRequestLimitPerUser = maxDailyRequestLimitPerUser;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Set<Log> getLogs() {
        if (logs == null) {
            logs = new HashSet<Log>();
        }
        return logs;
    }

    private void setLogs(Set<Log> logs) {
        this.logs = logs;
    }

    public void addLog(Log log) {
        if (log != null) {
            getLogs().add(log);
            log.getChannels().add(this);
        }
    }

    public void addLogs(Set<Log> logs) {
        if (logs != null) {
            for (Log log : logs) {
                addLog(log);
            }
        }
    }

    public void removeLog(Log log) {
        if (log != null) {
            getLogs().remove(log);
            log.getChannels().remove(this);
        }
    }

    public void removeLogs(Set<Log> logs) {
        if (logs != null) {
            for (Log log : logs) {
                removeLog(log);
            }
        }
    }

    public void clearLogs() {
        for (Log log : getLogs()) {
            log.getChannels().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getLogs().clear();
        //logs = new HashSet<RestrictedUser>();
    }

    public Set<RestrictedUser> getListenerRights() {
        if (listenerRights == null) {
            listenerRights = new HashSet<RestrictedUser>();
        }
        return listenerRights;
    }

    private void setListenerRights(Set<RestrictedUser> listenerRights) {
        this.listenerRights = listenerRights;
    }

    public void addListenerRight(RestrictedUser listenerRight) {
        if (listenerRight != null) {
            getListenerRights().add(listenerRight);
            listenerRight.getListenerRights().add(this);
        }
    }

    public void addListenerRights(Set<RestrictedUser> listenerRights) {
        if (listenerRights != null) {
            for (RestrictedUser listenerRight : listenerRights) {
                addListenerRight(listenerRight);
            }
        }
    }

    public void removeListenerRight(RestrictedUser listenerRight) {
        if (listenerRight != null) {
            getListenerRights().remove(listenerRight);
            listenerRight.getListenerRights().remove(this);
        }
    }

    public void removeListenerRights(Set<RestrictedUser> listenerRights) {
        if (listenerRights != null) {
            for (RestrictedUser listenerRight : listenerRights) {
                removeListenerRight(listenerRight);
            }
        }
    }

    public void clearListenerRights(boolean doDereference) {
        for (RestrictedUser listenerRight : getListenerRights()) {
            listenerRight.getListenerRights().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getListenerRights().clear();
        //listenerRights = new HashSet<RestrictedUser>();
    }

    public Set<RestrictedUser> getProgrammeManagerRights() {
        if (programmeManagerRights == null) {
            programmeManagerRights = new HashSet<RestrictedUser>();
        }
        return programmeManagerRights;
    }

    private void setProgrammeManagerRights(Set<RestrictedUser> programmeManagerRights) {
        this.programmeManagerRights = programmeManagerRights;
    }

    public void addProgrammeManagerRight(RestrictedUser programmeManagerRight) {
        if (programmeManagerRight != null) {
            getProgrammeManagerRights().add(programmeManagerRight);
            programmeManagerRight.getProgrammeManagerRights().add(this);
        }
    }

    public void addProgrammeManagerRights(Set<RestrictedUser> programmeManagerRights) {
        if (programmeManagerRights != null) {
            for (RestrictedUser programmeManagerRight : programmeManagerRights) {
                addProgrammeManagerRight(programmeManagerRight);
            }
        }
    }

    public void removeProgrammeManagerRight(RestrictedUser programmeManagerRight) {
        if (programmeManagerRight != null) {
            getProgrammeManagerRights().remove(programmeManagerRight);
            programmeManagerRight.getProgrammeManagerRights().remove(this);
        }
    }

    public void removeProgrammeManagerRights(Set<RestrictedUser> programmeManagerRights) {
        if (programmeManagerRights != null) {
            for (RestrictedUser programmeManagerRight : programmeManagerRights) {
                removeProgrammeManagerRight(programmeManagerRight);
            }
        }
    }

    public void clearProgrammeManagerRights(boolean doDereference) {
        for (RestrictedUser programmeManagerRight : getListenerRights()) {
            programmeManagerRight.getProgrammeManagerRights().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getProgrammeManagerRights().clear();
        //programmeManagerRights = new HashSet<RestrictedUser>();
    }

    public Set<RestrictedUser> getChannelAdministratorRights() {
        if (channelAdministratorRights == null) {
            channelAdministratorRights = new HashSet<RestrictedUser>();
        }
        return channelAdministratorRights;
    }

    private void setChannelAdministratorRights(Set<RestrictedUser> channelAdministratorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
    }

    public void addChannelAdministratorRight(RestrictedUser channelAdministratorRight) {
        if (channelAdministratorRight != null) {
            getChannelAdministratorRights().add(channelAdministratorRight);
            channelAdministratorRight.getChannelAdministratorRights().add(this);
        }
    }

    public void addChannelAdministratorRights(Set<RestrictedUser> channelAdministratorRights) {
        if (channelAdministratorRights != null) {
            for (RestrictedUser channelAdministratorRight : channelAdministratorRights) {
                addChannelAdministratorRight(channelAdministratorRight);
            }
        }
    }

    public void removeChannelAdministratorRight(RestrictedUser channelAdministratorRight) {
        if (channelAdministratorRight != null) {
            getChannelAdministratorRights().remove(channelAdministratorRight);
            channelAdministratorRight.getChannelAdministratorRights().remove(this);
        }
    }

    public void removeChannelAdministratorRights(Set<RestrictedUser> channelAdministratorRights) {
        if (channelAdministratorRights != null) {
            for (RestrictedUser channelAdministratorRight : channelAdministratorRights) {
                removeChannelAdministratorRight(channelAdministratorRight);
            }
        }
    }

    public void clearChannelAdministratorRights() {
        for (RestrictedUser channelAdministratorRight : getListenerRights()) {
            channelAdministratorRight.getChannelAdministratorRights().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getChannelAdministratorRights().clear();
        //channelAdministratorRights = new HashSet<RestrictedUser>();
    }

    public Set<Programme> getProgrammes() {
        if (programmes == null) {
            programmes = new HashSet<Programme>();
        }
        return programmes;
    }

    /** Hibernate setter **/
    private void setProgrammes(Set<Programme> programmes) {
        this.programmes = programmes;
    }

    public void addProgramme(Programme programme) {
        addProgramme(programme, true);
    }

    public void addProgramme(Programme programme, boolean doLink) {
        if (programme != null) {
            getProgrammes().add(programme);
            if (doLink) {
                programme.addChannel(this, false);
            }
        }
    }
    
    public void addProgrammes(Set<Programme> programmes) {
        if (programmes != null) {
            for (Programme programme : programmes) {
                addProgramme(programme);
            }
        }
    }

    public void removeProgramme(Programme programme) {
        removeProgramme(programme, true);
    }

    public void removeProgramme(Programme programme, boolean doLink) {
        if (programme != null) {
            getProgrammes().remove(programme);
            if (doLink) {
                programme.removeChannel(this, false);
            }
        }
    }    
    
    public void removeProgrammes(Set<Programme> programmes) {
        if (programmes != null) {
            for (Programme programme : programmes) {
                removeProgramme(programme);
            }
        }
    }

    public void clearProgrammes() {
        for (Programme programme : getProgrammes()) {
            programme.removeChannel(this, false);
        }
        getProgrammes().clear();
    }

    public Set<TimetableSlot> getTimetableSlots() {
        if (timetableSlots == null) {
            timetableSlots = new HashSet<TimetableSlot>();
        }
        return timetableSlots;
    }

    /** Hibernate setter **/
    private void setTimetableSlots(Set<TimetableSlot> slots) {
        this.timetableSlots = slots;
    }

    public void addTimetableSlot(TimetableSlot timetableSlot) {
        addTimetableSlot(timetableSlot, true);
    }

    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            getTimetableSlots().add(timetableSlot);
            if (doLink) {
                timetableSlot.addChannel(this, false);
            }
        }
    }

    public void addTimetableSlots(Set<TimetableSlot> timetableSlots) {
        if (timetableSlots != null) {
            for (TimetableSlot timetableSlot : timetableSlots) {
                addTimetableSlot(timetableSlot);
            }
        }
    }

    public void removeTimetableSlot(TimetableSlot timetableSlot) {
        removeTimetableSlot(timetableSlot, true);
    }

    public void removeTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            getTimetableSlots().remove(timetableSlot);
            if (doLink) {
                timetableSlot.removeChannel(false);
            }
        }
    }

    public void removeTimetableSlots(Set<TimetableSlot> timetableSlots) {
        if (timetableSlots != null) {
            for (TimetableSlot timetableSlot : timetableSlots) {
                removeTimetableSlot(timetableSlot);
            }
        }
    }

    public void clearTimetableSlots() {
        for (TimetableSlot timetableSlot : getTimetableSlots()) {
            timetableSlot.removeChannel(false);
        }
        getTimetableSlots().clear();
    }

    public Set<RestrictedUser> getImmutableRestrictedUsers() {
        Set<RestrictedUser> users = new HashSet<RestrictedUser>();
        users.addAll(getChannelAdministratorRights());
        users.addAll(getProgrammeManagerRights());
        users.addAll(getListenerRights());
        return users;
    }

    public SortedSet<QueueItem> getQueueItems() {
        if (queueItems == null) {
            queueItems = new TreeSet<QueueItem>();
        }
        return queueItems;
    }

    /** Hibernate setter **/
    private void setQueueItems(SortedSet<QueueItem> items) {
        this.queueItems = items;
    }

    public void addQueueItem(QueueItem queueItem) {
        addQueueItem(queueItem, true);
    }

    public void addQueueItem(QueueItem queueItem, boolean doLink) {
        if (queueItem != null) {
            getQueueItems().add(queueItem);
            if (doLink) {
                queueItem.addChannel(this, false);
            }
        }
    }

    public void addQueueItems(Set<QueueItem> queueItems) {
        if (queueItems != null) {
            for (QueueItem queueItem : queueItems) {
                addQueueItem(queueItem);
            }
        }
    }

    public void removeQueueItem(QueueItem queueItem) {
        removeQueueItem(queueItem, true);
    }

    public void removeQueueItem(QueueItem queueItem, boolean doLink) {
        if (queueItem != null) {
            getQueueItems().remove(queueItem);
            if (doLink) {
                queueItem.removeChannel(false);
            }
        }
    }

    public void removeQueueItems(Set<QueueItem> queueItems) {
        if (queueItems != null) {
            for (QueueItem queueItem : queueItems) {
                removeQueueItem(queueItem);
            }
        }
    }

    public void clearQueueItems() {
        for (QueueItem queueItem : getQueueItems()) {
            queueItem.removeChannel(false);
        }
        getQueueItems().clear();
    }

    public Long getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(Long seatNumber) {
        this.seatNumber = seatNumber;
    }

    public Set<Webservice> getWebservices() {
        if (webservices == null) {
            webservices = new HashSet<Webservice>();
        }
        return webservices;
    }

    /** Hibernate setter **/
    private void setWebservices(Set<Webservice> webservices) {
        this.webservices = webservices;
    }

    public void addWebservice(Webservice webservice) {
        addWebservice(webservice, true);
    }

    public void addWebservice(Webservice webservice, boolean doLink) {
        if (webservice != null) {
            getWebservices().add(webservice);
            if (doLink) {
                webservice.addChannel(this, false);
            }
        }
    }

    public void addWebservices(Set<Webservice> webservices) {
        if (webservices != null) {
            for (Webservice webservice : webservices) {
                addWebservice(webservice);
            }
        }
    }

    public void removeWebservice(Webservice webservice) {
        removeWebservice(webservice, true);
    }

    public void removeWebservice(Webservice webservice, boolean doLink) {
        if (webservice != null) {
            getWebservices().remove(webservice);
            if (doLink) {
                webservice.removeChannel(false);
            }
        }
    }

    public void removeWebservices(Set<Webservice> webservices) {
        if (webservices != null) {
            for (Webservice webservice : webservices) {
                removeWebservice(webservice);
            }
        }
    }

    public void clearWebservices() {
        for (Webservice webservice : getWebservices()) {
            webservice.removeChannel(false);
        }
        getWebservices().clear();
    }

    public SortedSet<ArchiveInterface> getArchives() {
        if (archives == null) {
            archives = new TreeSet<ArchiveInterface>();
        }
        return archives;
    }

    /** Hibernate setter **/
    private void setArchives(SortedSet<ArchiveInterface> archives) {
        this.archives = archives;
    }

    public void addArchive(ArchiveInterface archive) {
        addArchive(archive, true);
    }

    public void addArchive(ArchiveInterface archive, boolean doLink) {
        if (archive != null) {
            getArchives().add(archive);
            if (doLink) {
                archive.addChannel(this, false);
            }
        }
    }

    public void addArchives(Set<ArchiveInterface> archives) {
        if (archives != null) {
            for (ArchiveInterface archive : archives) {
                addArchive(archive);
            }
        }
    }

    public void removeArchive(ArchiveInterface archive) {
        removeArchive(archive, true);
    }

    public void removeArchive(ArchiveInterface archive, boolean doLink) {
        if (archive != null) {
            getArchives().remove(archive);
            if (doLink) {
                archive.removeChannel(false);
            }
        }
    }

    public void removeArchives(Set<ArchiveInterface> archives) {
        if (archives != null) {
            for (ArchiveInterface archive : archives) {
                removeArchive(archive);
            }
        }
    }

    public void clearArchives() {
        for (ArchiveInterface archive : getArchives()) {
            archive.removeChannel(false);
        }
        getArchives().clear();
    }

    public Set<Report> getReports() {
        if (reports == null) {
            reports = new TreeSet<Report>();
        }
        return reports;
    }

    /** Hibernate setter **/
    private void setReports(Set<Report> reports) {
        this.reports = reports;
    }

    public void addReport(Report report) {
        addReport(report, true);
    }

    public void addReport(Report report, boolean doLink) {
        if (report != null) {
            getReports().add(report);
            if (doLink) {
                report.addChannel(this, false);
            }
        }
    }

    public void addReports(Set<Report> reports) {
        if (reports != null) {
            for (Report report : reports) {
                addReport(report);
            }
        }
    }

    public void removeReport(Report report) {
        removeReport(report, true);
    }

    public void removeReport(Report report, boolean doLink) {
        if (report != null) {
            getReports().remove(report);
            if (doLink) {
                report.removeChannel(false);
            }
        }
    }

    public void removeReports(Set<Report> reports) {
        if (reports != null) {
            for (Report report : reports) {
                removeReport(report);
            }
        }
    }

    public void clearReports() {
        for (Report report : getReports()) {
            report.removeChannel(false);
        }
        getReports().clear();
    }
    
    public StreamItem getStreamItem() {
        return streamItem;
    }
    protected void setStreamItem(StreamItem streamItem) {
        this.streamItem = streamItem;
    }

    public void addStreamItem(StreamItem streamItem) {
        addStreamItem(streamItem, true);
    }

    public void addStreamItem(StreamItem streamItem, boolean doLink) {
        if (streamItem != null) {
            setStreamItem(streamItem);
            if (doLink) {
                streamItem.addChannel(this, false);
            }
        }
    }

    public void removeStreamItem() {
        removeStreamItem(true);
    }

    public void removeStreamItem(boolean doLink) {
        if (getStreamItem() != null) {
            if (doLink) {
                getStreamItem().removeChannel(false);
            }
            //Put delete-orphan in the hibernate mapping
            setStreamItem(null);
        }
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<Request> getRequests() {
        if (requests == null) {
            requests = new TreeSet<Request>();
        }
        return requests;
    }

    /** Hibernate setter **/
    private void setRequests(Set<Request> requests) {
        this.requests = requests;
    }

    public void addRequest(Request request) {
        addRequest(request, true);
    }

    public void addRequest(Request request, boolean doLink) {
        if (request != null) {
            getRequests().add(request);
            if (doLink) {
                request.addChannel(this, false);
            }
        }
    }

    public void addRequests(Set<Request> requests) {
        if (requests != null) {
            for (Request request : requests) {
                addRequest(request);
            }
        }
    }

    public void removeRequest(Request request) {
        removeRequest(request, true);
    }

    public void removeRequest(Request request, boolean doLink) {
        if (request != null) {
            /*getRequests().remove(request);
            if (doLink) {
                request.removeChannel(false);
            }*/
            request.drop(doLink, true, true);
        }
    }

    public void removeRequests(Set<Request> requests) {
        if (requests != null) {
            for (Request request : requests) {
                removeRequest(request);
            }
        }
    }

    public void clearRequests() {
        for (Iterator<Request> i = getRequests().iterator();i.hasNext();) {
            Request request = i.next();
            i.remove();
            //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
            //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
            request.drop(false, true, true);
        }
    }

    @Override
    public Channel shallowCopy() {
        return new Channel(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        Channel test = ProxiedClassUtil.cast(obj, Channel.class);
        return /*super.equals(test)
                &&*/ (getLabel().equals(test.getLabel()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLabel() ? 0 : getLabel().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setListenerRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        RightsBundle rightsBundle = new RightsBundle();
        //Void, only admins can do that
        return rightsBundle;
    }
    public static RightsBundle getLimitedProgrammeBindingRights() {
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }
    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }

    @Override
    public int compareTo(Channel anotherChannel) {
        if (anotherChannel!=null)
            return (getLabel() == null ?
                (anotherChannel.getLabel()==null?0:-1)
                : getLabel().compareTo(anotherChannel.getLabel()) );
        else
            return 1;
    }

    public static Collection<Programme> getProgrammesFromChannels(Collection<Channel> channels) {
        Collection<Programme> programmes = new HashSet<Programme>();
        if (channels!=null && !channels.isEmpty()) {
            for (Channel channel : channels)
                programmes.addAll(channel.getProgrammes());
        }
        return programmes;
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
