/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.channel.log;

import biz.ddcr.shampoo.server.domain.journal.Log.LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelLog extends Log {

    public static enum CHANNEL_LOG_OPERATION implements LOG_OPERATION {

        edit,
        delete,
        add;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case add:
                        return systemConfigurationHelper.isLogChannelCreate();
                    case edit:
                        return systemConfigurationHelper.isLogChannelUpdate();
                    case delete:
                        return systemConfigurationHelper.isLogChannelDelete();
                }
            }
            return false;
        }

    }
    /** The current operation performed on the actee **/
    private CHANNEL_LOG_OPERATION action;

    protected ChannelLog() {
        super();
    }

    @Override
    public ChannelLog shallowCopy() {
        return new ChannelLog(this);
    }
    
    public ChannelLog(CHANNEL_LOG_OPERATION operation, Channel channel) {
        super(channel != null ? channel.getLabel() : null, null);
        setAction(operation);
        //FIX Okay, here's a kludge to prevent cyclic Hibernate dependencies
        //If the operation is delete then don't re-add the channel here, otherwise it will be re-saved in cascade
        if (operation!=CHANNEL_LOG_OPERATION.delete) addChannel(channel);
    }

    public ChannelLog(ChannelLog o) {
        super(o);
        setAction(o.getAction());
    }

    @Override
    public CHANNEL_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(CHANNEL_LOG_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
