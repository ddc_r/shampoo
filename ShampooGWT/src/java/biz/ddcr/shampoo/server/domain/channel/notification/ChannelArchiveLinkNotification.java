/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.channel.notification;

import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.ArchiveInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelArchiveLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyDestinationCaption() {
        return getArchiveStartCalendar().getFriendlyEnglishCaption() + " @ " + getArchiveChannelCaption() + " [" +getDestinationID() + "]";
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "Archive "+getFriendlyDestinationCaption()+" is no longer linked to channel "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    public static enum CHANNELARCHIVE_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        delete;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel, ArchiveInterface archive) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (channel!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(channel),
                                    channel.getProgrammes(),
                                    Channel.getNotificationRights())
                            );
                }

                if (archive!=null) {

                    Collection<Channel> channels = new HashSet<Channel>();
                    channels.add(archive.getChannel());
                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    channels,
                                    null,
                                    Archive.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;
    /** Where this slot will be played */
    private String archiveChannelCaption;
    private CHANNELARCHIVE_NOTIFICATION_OPERATION action;

    protected ChannelArchiveLinkNotification() {
        super();
    }

    @Override
    public ChannelArchiveLinkNotification shallowCopy() {
        return new ChannelArchiveLinkNotification(this);
    }
    
    public ChannelArchiveLinkNotification(CHANNELARCHIVE_NOTIFICATION_OPERATION action, Channel channel, ArchiveInterface archive, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              archive!=null ? archive.getRefID() : null,
              recipient);
        setAction( action );
        setArchiveStartTimeWithCorrection(archive!=null ? archive.getStartTime() : null);
        setArchiveChannelCaption(archive!=null ? (archive.getChannel()!=null ? archive.getChannel().getLabel() : null) : null);
    }
    public ChannelArchiveLinkNotification(ChannelArchiveLinkNotification o) {
        super( o );
        setAction( o.getAction() );
        setArchiveStartTimeWithCorrection( o.getArchiveStartTime() );
        setArchiveChannelCaption( o.getArchiveChannelCaption() );
    }

    @Override
    public CHANNELARCHIVE_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(CHANNELARCHIVE_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getArchiveChannelCaption() {
        return archiveChannelCaption;
    }

    protected void setArchiveChannelCaption(String channelCaption) {
        this.archiveChannelCaption = channelCaption;
    }

    protected Long getArchiveStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _timestamp;
        } else {
            return getArchiveStartCalendar().getUnixTime();
        }
    }

    protected void setArchiveStartTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getArchiveStartCalendar() {
        if (startCalendar == null && _timestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getArchiveStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getArchiveStartCalendar().getYear(),
                getArchiveStartCalendar().getMonthOfYear(),
                getArchiveStartCalendar().getDayOfMonth(),
                getArchiveStartCalendar().getHourOfDay(),
                getArchiveStartCalendar().getMinuteOfHour(),
                getArchiveStartCalendar().getSecondOfMinute(),
                getArchiveStartCalendar().getMillisecondOfSecond(),
                getArchiveStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    protected boolean setArchiveStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

}
