/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.channel.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelProgrammeLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "Programme "+getFriendlyDestinationCaption()+" is no longer linked to channel "+getFriendlySourceCaption();
                case add:
                    return "Programme "+getFriendlyDestinationCaption()+" is now linked to channel "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    @Override
    public String getFriendlyDestinationCaption() {
        return getDestinationID();
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    public static enum CHANNELPROGRAMME_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        delete,
        add;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel, Programme programme) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (channel!=null) {

                    //if the operation is "delete" then re-add the original programme to the list bound to the track
                    Collection<Programme> originalProgrammes;
                    if (this==delete) {
                        originalProgrammes = new HashSet<Programme>();
                        originalProgrammes.addAll(channel.getProgrammes());
                        originalProgrammes.add(programme);
                    } else {
                        originalProgrammes = channel.getProgrammes();
                    }

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(channel),
                                    originalProgrammes,
                                    Channel.getNotificationRights())
                            );
                }

                if (programme!=null) {

                    //if the operation is "delete" then re-add the original channels to the list bound to the track
                    Collection<Channel> originalChannels;
                    if (this==delete) {
                        originalChannels = new HashSet<Channel>();
                        originalChannels.addAll(programme.getChannels());
                        originalChannels.add(channel);
                    } else {
                        originalChannels = programme.getChannels();
                    }

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    originalChannels,
                                    Collections.singleton(programme),
                                    Programme.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }

    }

    private CHANNELPROGRAMME_NOTIFICATION_OPERATION action;

    protected ChannelProgrammeLinkNotification() {
        super();
    }

    @Override
    public ChannelProgrammeLinkNotification shallowCopy() {
        return new ChannelProgrammeLinkNotification(this);
    }
    
    public ChannelProgrammeLinkNotification(CHANNELPROGRAMME_NOTIFICATION_OPERATION action, Channel channel, Programme programme, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              programme!=null ? programme.getLabel() : null,
              recipient);
        setAction( action );
    }
    public ChannelProgrammeLinkNotification(ChannelProgrammeLinkNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public CHANNELPROGRAMME_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(CHANNELPROGRAMME_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
