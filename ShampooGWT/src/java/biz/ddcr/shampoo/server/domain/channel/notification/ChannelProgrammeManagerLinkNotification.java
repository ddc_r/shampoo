/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.channel.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelProgrammeManagerLinkNotification extends ChannelRestrictedUserLinkNotification {

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "User "+getFriendlyDestinationCaption()+" is no longer a programme manager of channel "+getFriendlySourceCaption();
                case add:
                    return "User "+getFriendlyDestinationCaption()+" is now a programme manager of channel "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    public static enum CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION implements CHANNELRESTRICTEDUSER_NOTIFICATION_OPERATION {

        delete,
        add;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        @Override
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel, RestrictedUser restrictedUser) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (channel!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(channel),
                                    channel.getProgrammes(),
                                    Channel.getNotificationRights())
                            );
                }

                if (restrictedUser!=null) {

                    //if the operation is "delete" then re-add the original channel to the list bound to the track
                    Collection<Channel> originalChannels = restrictedUser.getImmutableChannels();
                    if (this==delete) {
                        originalChannels.add(channel);
                    }

                    //Add channels from programmes, as all user-based decisions
                    Collection<Programme> programmes = restrictedUser.getImmutableProgrammes();
                    originalChannels.addAll(Programme.getChannelsFromProgrammes(programmes));
                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    originalChannels,
                                    programmes,
                                    RestrictedUser.getNotificationRights())
                            );

                    //Add the concerned user too
                    recipients.add(restrictedUser);
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION action;

    protected ChannelProgrammeManagerLinkNotification() {
        super();
    }

    @Override
    public ChannelProgrammeManagerLinkNotification shallowCopy() {
        return new ChannelProgrammeManagerLinkNotification(this);
    }
    
    public ChannelProgrammeManagerLinkNotification(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION action, Channel channel, RestrictedUser restrictedUser, RestrictedUser recipient) {
        super(channel, restrictedUser, recipient);
        setAction( action );
    }
    public ChannelProgrammeManagerLinkNotification(ChannelProgrammeManagerLinkNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
