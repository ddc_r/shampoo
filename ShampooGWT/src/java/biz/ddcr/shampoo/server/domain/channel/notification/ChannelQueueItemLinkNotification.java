/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.channel.notification;

import biz.ddcr.shampoo.server.domain.queue.QueueItemInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelQueueItemLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyDestinationCaption() {
        return getQueueItemStartCalendar().getFriendlyEnglishCaption() + " @ " + getQueueItemChannelCaption() + " [" +getDestinationID() + "]";
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "Queue item "+getFriendlyDestinationCaption()+" is no longer linked to channel "+getFriendlySourceCaption();
                case add:
                    return "Queue item "+getFriendlyDestinationCaption()+" is now linked to channel "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    public static enum CHANNELQUEUE_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        delete,
        add;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel, QueueItemInterface queue) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (channel!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(channel),
                                    channel.getProgrammes(),
                                    Channel.getNotificationRights())
                            );
                }

                if (queue!=null) {

                    Collection<Channel> channels = new HashSet<Channel>();
                    channels.add(queue.getChannel());
                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    channels,
                                    null,
                                    QueueItem.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;
    /** Where this slot will be played */
    private String queueChannelCaption;
    private CHANNELQUEUE_NOTIFICATION_OPERATION action;

    protected ChannelQueueItemLinkNotification() {
        super();
    }

    @Override
    public ChannelQueueItemLinkNotification shallowCopy() {
        return new ChannelQueueItemLinkNotification(this);
    }
    
    public ChannelQueueItemLinkNotification(CHANNELQUEUE_NOTIFICATION_OPERATION action, Channel channel, QueueItemInterface queue, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              queue!=null ? queue.getRefID() : null,
              recipient);
        setAction( action );
        setQueueItemStartTimeWithCorrection(queue!=null ? queue.getStartTime() : null);
        setQueueItemChannelCaption(queue!=null ? (queue.getChannel()!=null ? queue.getChannel().getLabel() : null) : null);
    }
    public ChannelQueueItemLinkNotification(ChannelQueueItemLinkNotification o) {
        super( o );
        setAction( o.getAction() );
        setQueueItemStartTimeWithCorrection( o.getQueueItemStartTime() );
        setQueueItemChannelCaption( o.getQueueItemChannelCaption() );
    }

    @Override
    public CHANNELQUEUE_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(CHANNELQUEUE_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getQueueItemChannelCaption() {
        return queueChannelCaption;
    }

    protected void setQueueItemChannelCaption(String channelCaption) {
        this.queueChannelCaption = channelCaption;
    }

    protected Long getQueueItemStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _timestamp;
        } else {
            return getQueueItemStartCalendar().getUnixTime();
        }
    }

    protected void setQueueItemStartTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getQueueItemStartCalendar() {
        if (startCalendar == null && _timestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getQueueItemStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getQueueItemStartCalendar().getYear(),
                getQueueItemStartCalendar().getMonthOfYear(),
                getQueueItemStartCalendar().getDayOfMonth(),
                getQueueItemStartCalendar().getHourOfDay(),
                getQueueItemStartCalendar().getMinuteOfHour(),
                getQueueItemStartCalendar().getSecondOfMinute(),
                getQueueItemStartCalendar().getMillisecondOfSecond(),
                getQueueItemStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    protected boolean setQueueItemStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

}
