/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.channel.notification;

import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelReportLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyDestinationCaption() {
        return getReportStartCalendar().getFriendlyEnglishCaption() + " - " + getReportEndCalendar().getFriendlyEnglishCaption() + " @ " + getReportChannelCaption() + " [" +getDestinationID() + "]";
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case add:
                    return "Report "+getFriendlyDestinationCaption()+" is now linked to channel "+getFriendlySourceCaption();
                case delete:
                    return "Report "+getFriendlyDestinationCaption()+" is no longer linked to channel "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    public static enum CHANNELREPORT_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        add,
        delete;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel, Report report) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (channel!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(channel),
                                    channel.getProgrammes(),
                                    Channel.getNotificationRights())
                            );
                }

                if (report!=null) {

                    Collection<Channel> channels = new HashSet<Channel>();
                    channels.add(report.getChannel());
                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    channels,
                                    null,
                                    Report.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _startTimestamp;
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond endCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _endTimestamp;    
    /** Where this slot will be played */
    private String reportChannelCaption;
    private CHANNELREPORT_NOTIFICATION_OPERATION action;

    protected ChannelReportLinkNotification() {
        super();
    }

    @Override
    public ChannelReportLinkNotification shallowCopy() {
        return new ChannelReportLinkNotification(this);
    }
    
    public ChannelReportLinkNotification(CHANNELREPORT_NOTIFICATION_OPERATION action, Channel channel, Report report, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              report!=null ? report.getRefID() : null,
              recipient);
        setAction( action );
        setReportStartTimeWithCorrection(report!=null ? report.getStartTime() : null);
        setReportEndTimeWithCorrection(report!=null ? report.getEndTime() : null);
        setReportChannelCaption(report!=null ? (report.getChannel()!=null ? report.getChannel().getLabel() : null) : null);
    }
    public ChannelReportLinkNotification(ChannelReportLinkNotification o) {
        super( o );
        setAction( o.getAction() );
        setReportStartTimeWithCorrection( o.getReportStartTime() );
        setReportEndTimeWithCorrection( o.getReportEndTime() );        
        setReportChannelCaption( o.getReportChannelCaption() );
    }

    @Override
    public CHANNELREPORT_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(CHANNELREPORT_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getReportChannelCaption() {
        return reportChannelCaption;
    }

    protected void setReportChannelCaption(String channelCaption) {
        this.reportChannelCaption = channelCaption;
    }

    protected Long getReportStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _startTimestamp;
        } else {
            return getReportStartCalendar().getUnixTime();
        }
    }

    protected void setReportStartTimestamp(Long unixEpoch) {
        _startTimestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportStartCalendar() {
        if (startCalendar == null && _startTimestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _startTimestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getReportStartCalendar().getYear(),
                getReportStartCalendar().getMonthOfYear(),
                getReportStartCalendar().getDayOfMonth(),
                getReportStartCalendar().getHourOfDay(),
                getReportStartCalendar().getMinuteOfHour(),
                getReportStartCalendar().getSecondOfMinute(),
                getReportStartCalendar().getMillisecondOfSecond(),
                getReportStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    protected boolean setReportStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

    protected Long getReportEndTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endTimestamp;
        } else {
            return getReportEndCalendar().getUnixTime();
        }
    }

    protected void setReportEndTimestamp(Long unixEpoch) {
        _endTimestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportEndCalendar() {
        if (endCalendar == null && _endTimestamp != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _endTimestamp,
                    LOG_TIMEZONE);
        }
        return endCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getReportEndTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getReportEndCalendar().getYear(),
                getReportEndCalendar().getMonthOfYear(),
                getReportEndCalendar().getDayOfMonth(),
                getReportEndCalendar().getHourOfDay(),
                getReportEndCalendar().getMinuteOfHour(),
                getReportEndCalendar().getSecondOfMinute(),
                getReportEndCalendar().getMillisecondOfSecond(),
                getReportEndCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    protected boolean setReportEndTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }
    
}
