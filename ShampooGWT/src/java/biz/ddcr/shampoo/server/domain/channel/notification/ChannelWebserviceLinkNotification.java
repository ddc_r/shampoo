/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.channel.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelWebserviceLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "Webservice "+getFriendlyDestinationCaption()+" is no longer linked to channel "+getFriendlySourceCaption();
                case add:
                    return "Webservice "+getFriendlyDestinationCaption()+" is now linked to channel "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    @Override
    public String getFriendlyDestinationCaption() {
        return getDestinationID();
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    public static enum CHANNELWEBSERVICE_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        delete,
        add;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel, Webservice webservice) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (channel!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(channel),
                                    channel.getProgrammes(),
                                    Webservice.getNotificationRights())
                            );
                }

                if (webservice!=null) {

                    //if the operation is "delete" then re-add the original channels to the list bound to the track
                    Channel originalChannel;
                    if (this==delete) {
                        originalChannel = channel;
                    } else {
                        originalChannel = webservice.getChannel();
                    }

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Collections.singleton(originalChannel),
                                    originalChannel.getProgrammes(),
                                    Webservice.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }

    }

    private CHANNELWEBSERVICE_NOTIFICATION_OPERATION action;

    protected ChannelWebserviceLinkNotification() {
        super();
    }

    @Override
    public ChannelWebserviceLinkNotification shallowCopy() {
        return new ChannelWebserviceLinkNotification(this);
    }
    
    public ChannelWebserviceLinkNotification(CHANNELWEBSERVICE_NOTIFICATION_OPERATION action, Channel channel, Webservice webservice, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              webservice!=null ? webservice.getApiKey() : null,
              recipient);
        setAction( action );
    }
    public ChannelWebserviceLinkNotification(ChannelWebserviceLinkNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public CHANNELWEBSERVICE_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(CHANNELWEBSERVICE_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
