/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.feedback;

import biz.ddcr.shampoo.server.helper.NumberUtil;
import java.io.Serializable;

/**
 * Lightweight feedback signals used by MessageBroadcaster
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class Feedback implements Serializable, Comparable<Feedback> {
    private String recipientId;
    private String senderId;
    /** When this feedback was created, used for sorting purposes, but client side*/
    private long timestamp;

    public Feedback(String senderId, String recipientId) {
        this.senderId = senderId;
        this.recipientId = recipientId;
        //Auto generate a time
        this.timestamp = System.currentTimeMillis();
    }

    public Feedback(String senderId) {
        this(senderId, null);
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public int compareTo(Feedback o) {
        return NumberUtil.safeLongToInt(getTimestamp()-o.getTimestamp());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Feedback other = (Feedback) obj;
        if ((this.recipientId == null) ? (other.recipientId != null) : !this.recipientId.equals(other.recipientId)) {
            return false;
        }
        if ((this.senderId == null) ? (other.senderId != null) : !this.senderId.equals(other.senderId)) {
            return false;
        }
        if (this.timestamp != other.timestamp) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.recipientId != null ? this.recipientId.hashCode() : 0);
        hash = 23 * hash + (this.senderId != null ? this.senderId.hashCode() : 0);
        hash = 23 * hash + (int) (this.timestamp ^ (this.timestamp >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        return "Feedback{" + "recipientId=" + recipientId + ", senderId=" + senderId + ", timestamp=" + timestamp + '}';
    }
    
}
