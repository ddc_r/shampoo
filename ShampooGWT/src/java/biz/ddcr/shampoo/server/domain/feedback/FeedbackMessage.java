/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.feedback;

/**
 * Lightweight feedback signals used by MessageBroadcaster
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class FeedbackMessage extends Feedback {
    public enum TYPE {
        error,
        warning,
        info
    }
    private TYPE type;
    private String text;

    public FeedbackMessage(TYPE type, String text, String senderId) {
        super(senderId);
        this.type = type;
        this.text = text;
    }

    public FeedbackMessage(TYPE type, String text, String senderId, String recipientId) {
        super(senderId, recipientId);
        this.type = type;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeedbackMessage other = (FeedbackMessage) obj;
        if (!super.equals(obj)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + super.hashCode();
        hash = 83 * hash + (this.getRecipientId() != null ? this.getRecipientId().hashCode() : 0);
        hash = 83 * hash + (this.getSenderId() != null ? this.getSenderId().hashCode() : 0);
        hash = 83 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 83 * hash + (this.text != null ? this.text.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return this.getText()+", sender:"+this.getSenderId()+" recipient:"+this.getRecipientId();
    }

    
}
