/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.feedback;

/**
 * Lightweight feedback signals used by MessageBroadcaster
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class FeedbackObject extends Feedback {

    public enum TYPE {

        archive,
        report,
        channel,
        journal,
        notification,
        playlist,
        playlistentry,
        programme,
        queueitem,
        streamitem,
        streamer,
        timetableslot,
        broadcasttrack,
        pendingtrack,
        trackfilter,
        request,
        user,
        webservice,
        vote,
        hit
    }

    public enum FLAG {

        add,
        edit,
        delete
    }
    private TYPE type;
    private String objectId;
    private FLAG flag;

    public FeedbackObject(TYPE type, String objectId, FLAG flag, String senderId, String recipientId) {
        super(senderId, recipientId);
        this.type = type;
        this.flag = flag;
        this.objectId = objectId;
    }
    
    public FeedbackObject(TYPE type, String objectId, FLAG flag, String senderId) {
        this(type, objectId, flag, senderId, null);
    }
    
    public FeedbackObject(TYPE type, FLAG flag, String senderId) {
        this(type, null, flag, senderId, null);
    }

    public FLAG getFlag() {
        return flag;
    }

    public void setFlag(FLAG flag) {
        this.flag = flag;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeedbackObject other = (FeedbackObject) obj;
        if (!super.equals(obj)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if ((this.objectId == null) ? (other.objectId != null) : !this.objectId.equals(other.objectId)) {
            return false;
        }
        if (this.flag != other.flag) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + super.hashCode();
        hash = 61 * hash + (this.getRecipientId() != null ? this.getRecipientId().hashCode() : 0);
        hash = 61 * hash + (this.getSenderId() != null ? this.getSenderId().hashCode() : 0);
        hash = 61 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 61 * hash + (this.objectId != null ? this.objectId.hashCode() : 0);
        hash = 61 * hash + (this.flag != null ? this.flag.hashCode() : 0);
        return hash;
    }
    
    @Override
    public String toString() {
        return this.getObjectId()+", "+this.getFlag().toString()+", "+this.getType().toString()+", sender:"+this.getSenderId()+" recipient:"+this.getRecipientId();
    }
}
