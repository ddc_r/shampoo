/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.journal;

import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ExceptionLog extends Log {

    public static enum EXCEPTION_LOG_OPERATION implements LOG_OPERATION {

        data,
        io,
        general;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case data:
                        return systemConfigurationHelper.isLogErrorData();
                    case io:
                        return systemConfigurationHelper.isLogErrorIO();
                    case general:
                        return systemConfigurationHelper.isLogErrorGeneral();
                }
            }
            return false;
        }
    }
    /** The current operation performed on the actee **/
    private EXCEPTION_LOG_OPERATION action;
    /** uncryptic version of acteeID **/
    private String exceptionCaption;

    protected ExceptionLog() {
        super();
    }

    @Override
    public ExceptionLog shallowCopy() {
        return new ExceptionLog(this);
    }
    
    public ExceptionLog(EXCEPTION_LOG_OPERATION operation, Exception e) {
        super(e != null ? e.getClass().getCanonicalName() : null, /** no bound channels; limitation of proxies**/
                null);
        setAction(operation);
        setExceptionCaption(e != null ? (e.getMessage()!=null ? e.getMessage() : "") + (e.getStackTrace()!=null && e.getStackTrace().length>0 ? "\r\n"+e.getStackTrace()[0] : "") : null);
    }

    public ExceptionLog(ExceptionLog o) {
        super(o);
        setAction(o.getAction());
        setExceptionCaption(o.getExceptionCaption());
    }

    @Override
    public EXCEPTION_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(EXCEPTION_LOG_OPERATION action) {
        this.action = action;
    }

    public String getExceptionCaption() {
        return exceptionCaption;
    }

    public void setExceptionCaption(String exceptionCaption) {
        //Do only store the first 255 characters
        if (exceptionCaption!=null && exceptionCaption.length()>255)
            this.exceptionCaption = exceptionCaption.substring(0, 255);
        else
            this.exceptionCaption = exceptionCaption;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
