package biz.ddcr.shampoo.server.domain.journal;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.TimeBasedNote;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import java.util.HashSet;
import java.util.Set;

/**
 * A generic skeleton for a journal entry
 *
 * @author okay_awright
 */
public abstract class Log extends TimeBasedNote implements VisitorPatternInterface<LogVisitor> {

    public interface LOG_OPERATION {

        /** chekc whteher this operation must be logged or not **/
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper);
    }
    /** who performed an action of the kind LOG_OPERATION on actee **/
    private String actorID;
    /** The action-associated actee identifier - i.e. on which the action has been performed **/
    private String acteeID;
    /** relinked ou unlinked entity to the actee, for most LOG_OPERATIONs this value remains null **/
    //private String associatedActeeID;
    /** free-form text with misc. info about the calling method, call stack, or anything else**/
    //private String info;
    /** Channel-based rights for the access to this log **/
    private Set<Channel> channels;

    protected Log() {
        super();
    }

    public Log(String actee, Set<Channel> channels) {
        super();
        setActeeID(actee);
        setChannels(channels);
    }

    public Log(Log o) {
        super(o);
        setActorID(o.getActorID());
        setActeeID(o.getActeeID());
        //setAssociatedActeeID( o.getAssociatedActeeID() );
        setChannels(o.getChannels());
        //setInfo( o.getInfo() );
    }

    public String getActorID() {
        return actorID;
    }

    public void setActorID(String actorID) {
        this.actorID = actorID;
    }

    public abstract LOG_OPERATION getAction();

    public String getActeeID() {
        return acteeID;
    }

    public void setActeeID(String acteeID) {
        this.acteeID = acteeID;
    }

    /*public String getAssociatedActeeID() {
    return associatedActeeID;
    }

    public void setAssociatedActeeID(String associatedActeeID) {
    this.associatedActeeID = associatedActeeID;
    }*/
    public Set<Channel> getChannels() {
        if (channels == null) {
            channels = new HashSet<Channel>();
        }
        return channels;
    }

    private void setChannels(Set<Channel> channels) {
        this.channels = channels;
    }

    public void addChannel(Channel channel) {
        if (channel != null) {
            getChannels().add(channel);
            channel.getLogs().add(this);
        }
    }

    public void addChannels(Set<Channel> channels) {
        if (channels != null) {
            for (Channel channel : channels) {
                addChannel(channel);
            }
        }
    }

    public void removeChannel(Channel channel) {
        if (channel != null) {
            getChannels().remove(channel);
            channel.getLogs().remove(this);
        }
    }

    public void removeChannels(Set<Channel> channels) {
        if (channels != null) {
            for (Channel channel : channels) {
                removeChannel(channel);
            }
        }
    }

    public void clearChannels() {
        for (Channel channel : getChannels()) {
            channel.getLogs().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getChannels().clear();
        //channels = new HashSet<Channel>();
    }

    /*public String getInfo() {
    return info;
    }

    public void setInfo(String info) {
    this.info = info;
    }*/

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    public static RightsBundle getDeleteRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        //none, only admins can do that
        return rightsBundle;
    }

    public static RightsBundle getNotificationRights() {
        return getDeleteRights();
    }
    
    @Override
    public int compareTo(Object anotherObject) {
        int diff = super.compareTo(anotherObject);
        if (diff == 0 && anotherObject != null && anotherObject.getClass().isAssignableFrom(Log.class)) {
            Log anotherLog = (Log) anotherObject;
            //sort by actor, and finally by actee
            diff = (getActorID() != null ? getActorID().compareTo(anotherLog.getActorID()) : 0);
            if (diff == 0) {
                diff = (getActeeID() != null ? getActeeID().compareTo(anotherLog.getActeeID()) : 0);
            }
        }
        return diff;
    }
    
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
