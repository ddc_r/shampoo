/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.journal;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface.Visitor;
import biz.ddcr.shampoo.server.domain.archive.log.ArchiveLog;
import biz.ddcr.shampoo.server.domain.archive.log.ReportLog;
import biz.ddcr.shampoo.server.domain.channel.log.ChannelLog;
import biz.ddcr.shampoo.server.domain.playlist.log.PlaylistLog;
import biz.ddcr.shampoo.server.domain.programme.log.ProgrammeLog;
import biz.ddcr.shampoo.server.domain.queue.log.QueueLog;
import biz.ddcr.shampoo.server.domain.timetable.log.TimetableLog;
import biz.ddcr.shampoo.server.domain.track.log.TrackLog;
import biz.ddcr.shampoo.server.domain.user.log.ProfileLog;
import biz.ddcr.shampoo.server.domain.user.log.UserLog;
import biz.ddcr.shampoo.server.domain.webservice.log.WebserviceLog;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface LogVisitor extends Visitor {

    public void visit(ExceptionLog log) throws Exception;

    public void visit(UserLog log) throws Exception;

    public void visit(ProfileLog log) throws Exception;

    public void visit(TimetableLog log) throws Exception;

    public void visit(TrackLog log) throws Exception;

    public void visit(ProgrammeLog log) throws Exception;

    public void visit(PlaylistLog log) throws Exception;

    public void visit(ChannelLog log) throws Exception;
    
    public void visit(ArchiveLog log) throws Exception;
    
    public void visit(ReportLog log) throws Exception;

    public void visit(QueueLog log) throws Exception;

    public void visit(WebserviceLog log) throws Exception;

}
