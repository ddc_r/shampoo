/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.notification;

import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import java.io.Serializable;

/**
 *
 * Notification of the removal or the addition of a link between two entities
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class AssociationUpdatedNotification<T extends Serializable, U extends Serializable> extends Notification {

    private T sourceID;
    private U destinationID;

    protected AssociationUpdatedNotification() {
        super();
    }
    public AssociationUpdatedNotification(T sourceID, U destinationID, RestrictedUser recipient) {
        super();
        setSourceID( sourceID );
        setDestinationID( destinationID );
        addRecipient( recipient );
    }
    public AssociationUpdatedNotification(AssociationUpdatedNotification<T,U> o) {
        super( o );
        setSourceID( o.getSourceID() );
        setDestinationID( o.getDestinationID() );
    }

    public abstract String getFriendlyDestinationCaption();

    public U getDestinationID() {
        return destinationID;
    }

    public void setDestinationID(U destinationID) {
        this.destinationID = destinationID;
    }

    public abstract String getFriendlySourceCaption();

    public T getSourceID() {
        return sourceID;
    }

    public void setSourceID(T sourceID) {
        this.sourceID = sourceID;
    }

    @Override
    public abstract LINK_NOTIFICATION_OPERATION getAction();

}
