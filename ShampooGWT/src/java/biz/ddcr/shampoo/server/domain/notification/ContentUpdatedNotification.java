/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.notification;

import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import java.io.Serializable;

/**
 *
 * Notification of the edition of an entity
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class ContentUpdatedNotification<T extends Serializable> extends Notification {

    private T entityID;

    protected ContentUpdatedNotification() {
        super();
    }

    public ContentUpdatedNotification(T entityID, RestrictedUser recipient) {
        super();
        setEntityID( entityID );
        addRecipient( recipient );
    }
    public ContentUpdatedNotification(ContentUpdatedNotification<T> o) {
        super( o );
        setEntityID( o.getEntityID() );
    }

    public abstract String getFriendlyEntityCaption();

    public T getEntityID() {
        return entityID;
    }

    public void setEntityID(T entityID) {
        this.entityID = entityID;
    }

    @Override
    public abstract CONTENT_NOTIFICATION_OPERATION getAction();

}
