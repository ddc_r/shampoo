package biz.ddcr.shampoo.server.domain.notification;

import biz.ddcr.shampoo.server.helper.FriendlyEnglishCaptionInterface;
import biz.ddcr.shampoo.server.domain.*;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.RightsBundle;

/**
 * A generic skeleton of a notification sent to a user when something that he's responsible for is externally modified in some way
 *
 * @author okay_awright
 */
public abstract class Notification extends TimeBasedNote implements FriendlyEnglishCaptionInterface, VisitorPatternInterface<NotificationVisitor> {

    /** who performed an action of the kind NOTIFICATION_OPERATION **/
    private String actorID;
    /** Who has access to this log **/
    private RestrictedUser recipient;
    /** has the user been notified of this event? e.g. mail sent, etc **/
    private boolean recipientNotified;
    /** free text associated with the notification; most of the time unused except for track rejection notifications where it's used for keep track of the justification of the rejection by the submitter **/
    private String freeText;

    protected Notification() {
        super();
        //Always start by assuming that the recipient is not yet notified
        recipientNotified = false;
    }

    public Notification(Notification o) {
        super(o);
        setActorID(o.getActorID());
        addRecipient(o.getRecipient());
        setRecipientNotified(o.isRecipientNotified());
        setFreeText(o.getFreeText());
    }

    public String getActorID() {
        return actorID;
    }

    public void setActorID(String actorID) {
        this.actorID = actorID;
    }

    public RestrictedUser getRecipient() {
        return recipient;
    }

    private void setRecipient(RestrictedUser restrictedUser) {
        recipient = restrictedUser;
    }

    public void addRecipient(RestrictedUser restrictedUser) {
        addRecipient(restrictedUser, true);
    }

    public void addRecipient(RestrictedUser restrictedUser, boolean doLink) {
        if (restrictedUser != null) {
            if (doLink) {
                restrictedUser.addNotification(this, false);
            }
            setRecipient(restrictedUser);
        }
    }

    public void removeRecipient() {
        removeRecipient(true);
    }

    public void removeRecipient(boolean doLink) {
        if (getRecipient() != null) {
            if (doLink) {
                getRecipient().removeNotification(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setRecipient(null);
        }
    }

    public boolean isRecipientNotified() {
        return recipientNotified;
    }

    public void setRecipientNotified(boolean recipientNotified) {
        this.recipientNotified = recipientNotified;
    }

    public abstract NOTIFICATION_OPERATION getAction();

    public String getFreeText() {
        return freeText;
    }

    public void setFreeText(String freeText) {
        this.freeText = freeText;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setListenerRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    public static RightsBundle getAddRights() {
        RightsBundle rightsBundle = new RightsBundle();
        //Void, only admins can do that
        return rightsBundle;
    }

    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }

    public static RightsBundle getDeleteRights() {
        return getViewRights();
    }

    @Override
    public int compareTo(Object anotherObject) {
        int diff = super.compareTo(anotherObject);
        if (diff == 0 && anotherObject != null && Notification.class.isAssignableFrom(anotherObject.getClass())) {
            Notification anotherNotification = (Notification) anotherObject;
            //sort by actor, and finally by actee
            diff = (getAction() != null ? getAction().toString().compareTo(anotherNotification.getAction().toString()) : 0);
            if (diff == 0) {
                diff = (getActorID() != null ? getActorID().compareTo(anotherNotification.getActorID()) : 0);
                if (diff == 0) {
                    diff = (getRecipient() != null ? getRecipient().compareTo(anotherNotification.getRecipient()) : 0);
                }
            }
        }
        return diff;
    }
    
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
