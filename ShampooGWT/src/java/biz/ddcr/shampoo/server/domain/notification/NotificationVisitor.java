/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.notification;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface.Visitor;
import biz.ddcr.shampoo.server.domain.channel.notification.*;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistContentNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContentNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.streamer.notification.StreamerStatusNotification;
import biz.ddcr.shampoo.server.domain.timetable.notification.TimetableContentNotification;
import biz.ddcr.shampoo.server.domain.track.notification.BroadcastableTrackContentNotification;
import biz.ddcr.shampoo.server.domain.track.notification.PendingTrackContentNotification;
import biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification;
import biz.ddcr.shampoo.server.domain.webservice.notification.WebserviceContentNotification;

/**
 *
 * Dummy visitor pattern interface for Hibernate proxies
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface NotificationVisitor extends Visitor {

    public void visit(ChannelContentNotification message) throws Exception;

    public void visit(PlaylistContentNotification message) throws Exception;

    public void visit(ProgrammeContentNotification message) throws Exception;

    public void visit(TimetableContentNotification message) throws Exception;

    public void visit(BroadcastableTrackContentNotification message) throws Exception;

    public void visit(PendingTrackContentNotification message) throws Exception;

    public void visit(RestrictedUserContentNotification message) throws Exception;

    public void visit(ChannelChannelAdministratorLinkNotification message) throws Exception;

    public void visit(ChannelListenerLinkNotification message) throws Exception;

    public void visit(ChannelProgrammeLinkNotification message) throws Exception;

    public void visit(ChannelProgrammeManagerLinkNotification message) throws Exception;

    public void visit(ChannelTimetableSlotLinkNotification message) throws Exception;

    public void visit(PlaylistProgrammeLinkNotification message) throws Exception;

    public void visit(PlaylistTimetableSlotLinkNotification message) throws Exception;

    public void visit(ProgrammeAnimatorLinkNotification message) throws Exception;

    public void visit(ProgrammeBroadcastableTrackLinkNotification message) throws Exception;

    public void visit(ProgrammeContributorLinkNotification message) throws Exception;

    public void visit(ProgrammeCuratorLinkNotification message) throws Exception;

    public void visit(ProgrammePendingTrackLinkNotification message) throws Exception;

    public void visit(ProgrammeTimetableSlotLinkNotification message) throws Exception;

    public void visit(ChannelArchiveLinkNotification message) throws Exception;
    
    public void visit(ChannelReportLinkNotification message) throws Exception;

    public void visit(ChannelQueueItemLinkNotification message) throws Exception;
    
    public void visit(StreamerStatusNotification message) throws Exception;

    public void visit(WebserviceContentNotification message) throws Exception;

    public void visit(ChannelWebserviceLinkNotification message) throws Exception;
}
