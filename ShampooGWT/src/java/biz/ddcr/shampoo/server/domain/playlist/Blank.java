package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;


/**
 * Simulates the off-air event
 * It essentially represents null values
 * Workaround for Liquidsoap 0.91
 *
 * @author okay_awright
 */
public class Blank implements Serializable, PlayableItemInterface {

    public Blank() {
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Blank at this point
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public void is(PlayableItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
