package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.track.filter.Filter;
import biz.ddcr.shampoo.server.domain.track.SELECTION;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.util.HashSet;
import java.util.Set;

/**
 * A static PlaylistEntry is an Playlist item and represents a dynamic selection of Selections
 *
 * @author okay_awright
 */
public class DynamicPlaylistEntry extends PlaylistEntry {

    //Selection parameters
    private SELECTION selection;
    //Limit; it is applied AFTER filters
    private SubSelection subSelection;
    //Do not play the same Track if it's last been played under this threshold (in minutes)
    private Long noReplayDelay;
    //Do not replay the same Track if it's already been played in this Playlist
    private boolean noReplayInPlaylist;
    //Filters; they are applied BEFORE limit
    private Set<Filter> filters;

    protected DynamicPlaylistEntry() {
    }

    public DynamicPlaylistEntry(Playlist playlist, long sequenceIndex) {
        super(playlist, sequenceIndex);
    }

    public DynamicPlaylistEntry(DynamicPlaylistEntry o) {
        super(o);
        setNoReplayDelay(o.getNoReplayDelay());
        setNoReplayInPlaylist(o.isNoReplayInPlaylist());
        setSelection(o.getSelection());
        setSubSelection(o.getSubSelection());
        addFilters(o.getFilters());
    }

    public Long getNoReplayDelay() {
        return noReplayDelay;
    }

    public void setNoReplayDelay(Long noReplayDelay) {
        this.noReplayDelay = noReplayDelay;
    }

    public boolean isNoReplayInPlaylist() {
        return noReplayInPlaylist;
    }

    public void setNoReplayInPlaylist(boolean noReplayInPlaylist) {
        this.noReplayInPlaylist = noReplayInPlaylist;
    }

    public SELECTION getSelection() {
        return selection;
    }

    public void setSelection(SELECTION selection) {
        this.selection = selection;
    }

    public SubSelection getSubSelection() {
        return subSelection;
    }

    public void setSubSelection(SubSelection subSelection) {
        this.subSelection = subSelection;
    }

    public Set<Filter> getFilters() {
        if (filters == null) {
            filters = new HashSet<Filter>();
        }
        return filters;
    }

    /** Hibernate setter **/
    private void setFilters(Set<Filter> entries) {
        this.filters = entries;
    }

    public void addFilter(Filter filter) {
        addFilter(filter, true);
    }

    public void addFilter(Filter filter, boolean doLink) {
        if (filter != null) {
            getFilters().add(filter);
            if (doLink) {
                filter.addEntry(this, false);
            }
        }
    }

    public void addFilters(Set<Filter> filters) {
        if (filters != null) {
            for (Filter filter : filters) {
                addFilter(filter);
            }
        }
    }

    public void removeFilter(Filter filter) {
        removeFilter(filter, true);
    }

    public void removeFilter(Filter filter, boolean doLink) {
        if (filter != null) {
            getFilters().remove(filter);
            if (doLink) {
                filter.removeEntry(false);
            }
        }
    }

    public void removeFilters(Set<Filter> filters) {
        if (filters != null) {
            for (Filter filter : filters) {
                removeFilter(filter);
            }
        }
    }

    public void clearFilters() {
        for (Filter filter : getFilters()) {
            filter.removeEntry(false);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getFilters().clear();
        //filters = new HashSet<Filter>();
    }

    @Override
    public DynamicPlaylistEntry shallowCopy() {
        return new DynamicPlaylistEntry(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        DynamicPlaylistEntry test = ProxiedClassUtil.cast(obj, DynamicPlaylistEntry.class);
        return super.equals(test);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(PlaylistEntryVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean isDuplicate(PlaylistEntry otherPlaylistEntry) {
        return isDuplicate(true, otherPlaylistEntry);
    }

    @Override
    public boolean isDuplicateNoPlaylist(PlaylistEntry otherPlaylistEntry) {
        return isDuplicate(false, otherPlaylistEntry);
    }

    @Override
    public boolean isDuplicate(boolean doReverseChecking, PlaylistEntry otherPlaylistEntry) {
        if (otherPlaylistEntry == null || !ProxiedClassUtil.castableAs(otherPlaylistEntry, DynamicPlaylistEntry.class)) {
            return false;
        }
        DynamicPlaylistEntry otherDynamicPlaylistEntry = ProxiedClassUtil.cast(otherPlaylistEntry, DynamicPlaylistEntry.class);
        //Just check attributes not relationships used as business keys
        if (doReverseChecking) {
            if (getPlaylist() != null) {
                if (!getPlaylist().equals(otherDynamicPlaylistEntry.getPlaylist())) {
                    return false;
                }
            } else {
                if (otherDynamicPlaylistEntry.getPlaylist() != null) {
                    return false;
                }
            }
        }
        if (getSequenceIndex() != otherDynamicPlaylistEntry.getSequenceIndex()) {
            return false;
        }
        if (getLoop() != null) {
            if (!getLoop().equals(otherDynamicPlaylistEntry.getLoop())) {
                return false;
            }
        } else {
            if (otherDynamicPlaylistEntry.getLoop() != null) {
                return false;
            }
        }
        if (getFadeIn()!=null) {
            if (!getFadeIn().equals(otherDynamicPlaylistEntry.getFadeIn())) return false;
        } else {
            if (otherDynamicPlaylistEntry.getFadeIn()!=null) return false;
        }
        if (isRequestAllowed() != otherDynamicPlaylistEntry.isRequestAllowed()) {
            return false;
        }
        if (getSelection() != null) {
            if (!getSelection().equals(otherDynamicPlaylistEntry.getSelection())) {
                return false;
            }
        } else {
            if (otherDynamicPlaylistEntry.getSelection() != null) {
                return false;
            }
        }
        if (getSubSelection() != null) {
            if (!getSubSelection().equals(otherDynamicPlaylistEntry.getSubSelection())) {
                return false;
            }
        } else {
            if (otherDynamicPlaylistEntry.getSubSelection() != null) {
                return false;
            }
        }
        if (getNoReplayDelay() != null) {
            if (!getNoReplayDelay().equals(otherDynamicPlaylistEntry.getNoReplayDelay())) {
                return false;
            }
        } else {
            if (otherDynamicPlaylistEntry.getNoReplayDelay() != null) {
                return false;
            }
        }
        if (isNoReplayInPlaylist() != otherDynamicPlaylistEntry.isNoReplayInPlaylist()) {
            return false;
        }
        if (getFilters() != null) {
            if (otherDynamicPlaylistEntry.getFilters() == null || getFilters().size() != otherDynamicPlaylistEntry.getFilters().size()) {
                return false;
            } else //Since no two filters from a same set can have the same location, this naive approach for duplicate checking works
            {
                for (Filter thisSetFilter : getFilters()) {
                    boolean isFilterMatch = false;
                    for (Filter otherSetFilter : otherDynamicPlaylistEntry.getFilters()) //Don't check if the filter belongs to the current playlist, because first it's obvious, and second it will lead to circular calls
                    {
                        if (thisSetFilter.isDuplicate(false, otherSetFilter)) {
                            isFilterMatch = true;
                            break;
                        }
                    }
                    if (!isFilterMatch) {
                        return false;
                    }
                }
            }
        } else {
            if (otherDynamicPlaylistEntry.getFilters() != null) {
                return false;
            }
        }

        return true;
    }
}
