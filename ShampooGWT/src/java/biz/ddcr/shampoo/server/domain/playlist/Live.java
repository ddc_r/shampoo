package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.domain.track.HasPEGIRatingInterface;
import biz.ddcr.shampoo.server.domain.track.PEGIRating;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;


/**
 * A Live is a special binding for live performance and is not directly handled by the application
 * A Live can take over a special playlist anytime
 *
 * @author okay_awright
 */
public class Live implements Serializable, PlayableItemInterface, HasPEGIRatingInterface {

    private String broadcaster;
    private String login;
    private String password;
    private PEGIRating rating;

    public Live() {
    }

    public String getBroadcaster() {
        return broadcaster;
    }

    public void setBroadcaster(String broadcaster) {
        this.broadcaster = broadcaster;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public PEGIRating getRating() {
        return rating;
    }

    public void setRating(PEGIRating rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Live at this point
        Live test = ProxiedClassUtil.cast(obj, Live.class);
        return /*super.equals(test)
                &&*/ (getBroadcaster().equals(test.getBroadcaster()))
                && (getLogin().equals(test.getLogin()))
                && (getPassword().equals(test.getPassword()))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (getBroadcaster() != null ? getBroadcaster().hashCode() : 0);
        hash = 41 * hash + (getLogin() != null ? getLogin().hashCode() : 0);
        hash = 41 * hash + (getPassword() != null ? getPassword().hashCode() : 0);
        return hash;
    }

    @Override
    public void is(PlayableItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
