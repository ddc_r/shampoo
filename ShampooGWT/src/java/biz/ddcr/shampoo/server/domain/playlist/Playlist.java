package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import java.util.HashSet;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * A special Playlist which defines the content that is played by a special Programme
 *
 * @author okay_awright
 */
public class Playlist extends GenericTimestampedEntity {

    private String refID;
    private String label;
    private String description;
    private SortedSet<PlaylistEntry> playlistEntries;
    private Programme programme;
    private Live live;
    private Set<TimetableSlot> timetableSlots;
    private PlaylistTagSet globalTagSet;

    private Integer maxUserRequestLimit;
    //Do not play the same Request if it's last been played under this threshold (in minutes)
    private Long noRequestReplayDelay;
    //Do not replay the same Request if it's already been played in this Playlist
    private boolean noRequestReplayInPlaylist;

    /** special flag that specifies if the flyer for the playlist is available for processing. The flyer may, for example, be transcoded or still being transfered to the datastore and thus is not yet ready for broadcast
     **/
    private boolean ready;
    /** metadata of the current cover art picture linked to this track; null if no file */
    private PictureFileInfo coverArtContainer;

    public Playlist() {
    }

    public Playlist(Playlist o) {
        super(o);
        setDescription(o.getDescription());
        setLabel(o.getLabel());
        setLive(o.getLive());
        setGlobalTagSet(o.getGlobalTagSet());
        addPlaylistEntries(o.getPlaylistEntries());
        addProgramme(o.getProgramme());
        addTimetableSlots(o.getTimetableSlots());
        setMaxUserRequestLimit( o.getMaxUserRequestLimit() );
        setNoRequestReplayDelay( o.getNoRequestReplayDelay() );
        setNoRequestReplayInPlaylist( o.isNoRequestReplayInPlaylist() );
        setCoverArtContainer( o.getCoverArtContainer() );
        setReady( o.isReady() );
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getMaxUserRequestLimit() {
        return maxUserRequestLimit;
    }

    public void setMaxUserRequestLimit(Integer maxUserRequestLimit) {
        this.maxUserRequestLimit = maxUserRequestLimit;
    }

    public Long getNoRequestReplayDelay() {
        return noRequestReplayDelay;
    }

    public void setNoRequestReplayDelay(Long noRequestReplayDelay) {
        this.noRequestReplayDelay = noRequestReplayDelay;
    }

    public boolean isNoRequestReplayInPlaylist() {
        return noRequestReplayInPlaylist;
    }

    public void setNoRequestReplayInPlaylist(boolean noRequestReplayInPlaylist) {
        this.noRequestReplayInPlaylist = noRequestReplayInPlaylist;
    }

    public PictureFileInfo getCoverArtContainer() {
        return coverArtContainer;
    }

    public void setCoverArtContainer(PictureFileInfo container) {
        this.coverArtContainer = container;
    }    

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public SortedSet<PlaylistEntry> getPlaylistEntries() {
        if (playlistEntries == null) {
            playlistEntries = new TreeSet<PlaylistEntry>();
        }
        return playlistEntries;
    }

    /** Hibernate setter **/
    private void setPlaylistEntries(SortedSet<PlaylistEntry> entries) {
        this.playlistEntries = entries;
    }

    public void addPlaylistEntry(PlaylistEntry playlistEntry) {
        addPlaylistEntry(playlistEntry, true);
    }

    public void addPlaylistEntry(PlaylistEntry playlistEntry, boolean doLink) {
        if (playlistEntry != null) {
            getPlaylistEntries().add(playlistEntry);
            if (doLink) {
                playlistEntry.addPlaylist(this, false);
            }
        }
    }

    public void addPlaylistEntries(Set<PlaylistEntry> playlistEntries) {
        if (playlistEntries != null) {
            for (PlaylistEntry playlistEntry : playlistEntries) {
                addPlaylistEntry(playlistEntry);
            }
        }
    }

    public void removePlaylistEntry(PlaylistEntry playlistEntry) {
        removePlaylistEntry(playlistEntry, true);
    }

    public void removePlaylistEntry(PlaylistEntry playlistEntry, boolean doLink) {
        if (playlistEntry != null) {
            getPlaylistEntries().remove(playlistEntry);            
            if (doLink) {
                playlistEntry.removePlaylist(false);
            }
        }
    }

    public void removePlaylistEntries(Set<PlaylistEntry> playlistEntries) {
        if (playlistEntries != null) {
            for (PlaylistEntry playlistEntry : playlistEntries) {
                removePlaylistEntry(playlistEntry);
            }
        }
    }

    public void clearPlaylistEntries() {
        for (PlaylistEntry playlistEntry : getPlaylistEntries()) {
            playlistEntry.removePlaylist(false);
        }
        getPlaylistEntries().clear();
        //playlistEntries = new HashSet<PlaylistEntry>();
    }

    public Live getLive() {
        return live;
    }

    public void setLive(Live live) {
        this.live = live;
    }

    public PlaylistTagSet getGlobalTagSet() {
        return globalTagSet;
    }

    public void setGlobalTagSet(PlaylistTagSet globalTagSet) {
        this.globalTagSet = globalTagSet;
    }

    public Programme getProgramme() {
        return programme;
    }

    private void setProgramme(Programme programme) {
        this.programme = programme;
    }

    public void addProgramme(Programme Programme) {
        addProgramme(Programme, true);
    }

    public void addProgramme(Programme Programme, boolean doLink) {
        if (Programme != null) {
            setProgramme(Programme);
            if (doLink) {
                Programme.addPlaylist(this, false);
            }
        }
    }

    public void removeProgramme() {
        removeProgramme(true);
    }

    public void removeProgramme(boolean doLink) {
        if (getProgramme() != null) {
            if (doLink) {
                getProgramme().removePlaylist(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setProgramme(null);
        }
    }

    public Set<TimetableSlot> getTimetableSlots() {
        if (timetableSlots == null) {
            timetableSlots = new HashSet<TimetableSlot>();
        }
        return timetableSlots;
    }

    /** Hibernate setter **/
    private void setTimetableSlots(Set<TimetableSlot> timetableSlots) {
        this.timetableSlots = timetableSlots;
    }

    public void addTimetableSlot(TimetableSlot timetableSlot) {
        addTimetableSlot(timetableSlot, true);
    }

    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            getTimetableSlots().add(timetableSlot);
            if (doLink) {
                timetableSlot.addPlaylist(this, false);
            }
        }
    }

    public void addTimetableSlots(Set<TimetableSlot> timetableSlots) {
        if (timetableSlots != null) {
            for (TimetableSlot timetableSlot : timetableSlots) {
                addTimetableSlot(timetableSlot);
            }
        }
    }

    public void removeTimetableSlot(TimetableSlot timetableSlot) {
        removeTimetableSlot(timetableSlot, true);
    }

    public void removeTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            getTimetableSlots().remove(timetableSlot);
            if (doLink) {
                timetableSlot.removePlaylist(false);
            }
        }
    }

    public void removeTimetableSlots(Set<TimetableSlot> timetableSlots) {
        if (timetableSlots != null) {
            for (TimetableSlot timetableSlot : timetableSlots) {
                removeTimetableSlot(timetableSlot);
            }
        }
    }

    public void clearTimetableSlots() {
        for (TimetableSlot timetableSlot : getTimetableSlots()) {
            timetableSlot.removePlaylist(false);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getTimetableSlots().clear();
        //timetableSlots = new HashSet<TimetableSlot>();
    }

    @Override
    public Playlist shallowCopy() {
        return new Playlist(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        Playlist test = ProxiedClassUtil.cast(obj, Playlist.class);
        return (getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (getRefID() != null ? getRefID().hashCode() : 0);
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getLimitedTimetableBindingRights() {
        //on purpose, even Programme Managers cannot do that, only Animators
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }
    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }
        
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
