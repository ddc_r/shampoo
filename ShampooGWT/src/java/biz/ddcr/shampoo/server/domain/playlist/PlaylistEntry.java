package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.DuplicateCheckingInterface;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import java.io.Serializable;
import java.util.Comparator;

/**
 * A PlaylistEntry is a Playlist item
 *
 * @author okay_awright
 */
public abstract class PlaylistEntry implements Comparable<PlaylistEntry>, GenericEntityInterface, DuplicateCheckingInterface<PlaylistEntry> {

    /** Used for Hibernate sorting of SortedSets, it must be static! **/
    /** But Ehcache needs it to be Serializable vene if static **/
    public static class HibernateComparator implements Serializable, Comparator<PlaylistEntry> {

        @Override
        public int compare(PlaylistEntry t, PlaylistEntry t1) {
            return t==null ? 0 : t.compareTo(t1);
        }
    }

    /** Hibernate versioning */
    private int version;

    private String refID;
    /**
     * Which playlist it belongs to and which order in the sequence is it registered with
     **/
    private PlaylistEntryLocation location;
    /**
     * Amount of seconds for fading it in into the previous track
     * 0 means disable
     * null means use default value
     */
    private Integer fadeIn;
    /**
     * The number of times this slot is looped
     * 0 means infinite
     **/
    private Long loop;
    /**
     * Can a listener request his own track instead of playing the one defined by this slot?
     */
    private boolean requestAllowed = false;

    protected PlaylistEntry() {
    }

    public PlaylistEntry(Playlist playlist, long sequenceIndex) {
        super();
        addPlaylist(playlist);
        setSequenceIndex(sequenceIndex);
    }

    public PlaylistEntry(PlaylistEntry o) {
        setFadeIn(o.getFadeIn());
        setLoop(o.getLoop());
        addPlaylist(o.getPlaylist());
        setSequenceIndex(o.getSequenceIndex());
        setRequestAllowed(o.isRequestAllowed());
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    private PlaylistEntryLocation getLocation() {
        if (location == null) {
            location = new PlaylistEntryLocation();
        }
        return location;
    }

    /** Hibernate setter **/
    private void setLocation(PlaylistEntryLocation location) {
        this.location = location;
    }

    public Integer getFadeIn() {
        return fadeIn;
    }

    public void setFadeIn(Integer fadeIn) {
        this.fadeIn = fadeIn;
    }

    public Long getLoop() {
        return loop;
    }

    public void setLoop(Long loop) {
        this.loop = loop;
    }

    public boolean isRequestAllowed() {
        return requestAllowed;
    }

    public void setRequestAllowed(boolean requestAllowed) {
        this.requestAllowed = requestAllowed;
    }

    public Playlist getPlaylist() {
        return getLocation().getPlaylist();
    }

    public void addPlaylist(Playlist playlist) {
        addPlaylist(playlist, true);
    }

    public void addPlaylist(Playlist playlist, boolean doLink) {
        if (playlist != null) {
            if (doLink) {
                playlist.addPlaylistEntry(this, false);
            }
            getLocation().setPlaylist(playlist);
        }
    }

    public void removePlaylist() {
        removePlaylist(true);
    }

    public void removePlaylist(boolean doLink) {
        if (getLocation().getPlaylist() != null) {
            if (doLink) {
                getLocation().getPlaylist().removePlaylistEntry(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setPlaylist(null);
        }
    }

    public long getSequenceIndex() {
        return getLocation().getSequenceIndex();
    }

    public void setSequenceIndex(long sequenceIndex) {
        getLocation().setSequenceIndex(sequenceIndex);
    }

    /**
     * Alternative equality test. Inspect all attributes this time, except primary keys.
     * @param otherPlaylistEntry
     * @return
     */
    @Override
    public abstract boolean isDuplicate(PlaylistEntry otherPlaylistEntry);
    public abstract boolean isDuplicateNoPlaylist(PlaylistEntry otherPlaylistEntry);

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        PlaylistEntry test = ProxiedClassUtil.cast(obj, PlaylistEntry.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }
        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLocation() ? 0 : getLocation().hashCode());
        //hash = 31 * hash + (null == getRefID() ? 0 : getRefID().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(PlaylistEntry anotherPlaylistEntry) {
        //Sort by sequence index, ascending order
        if (anotherPlaylistEntry!=null)
            return (this.getLocation() == null ?
                0 :
                (anotherPlaylistEntry.getLocation() == null ?
                    0 :
                    getLocation().compareTo(anotherPlaylistEntry.getLocation()) ));
        else
            return 0;
    }

    /** visitor pattern inspection for Hibernate proxies **/
    public abstract void acceptVisit(PlaylistEntryVisitor visitor) throws Exception;
    
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
