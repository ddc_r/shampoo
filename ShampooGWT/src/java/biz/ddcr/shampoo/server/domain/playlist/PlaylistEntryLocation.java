/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class PlaylistEntryLocation implements Serializable, Comparable<PlaylistEntryLocation> {

    /**
     * PlaylistEntry Playlist == Playlist playlist
     **/
    private Playlist playlist;
    /**
     * The sequence order of the item; zero-based
     **/
    private long sequenceIndex;

    public PlaylistEntryLocation() {
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public long getSequenceIndex() {
        return sequenceIndex;
    }

    public void setSequenceIndex(long sequenceIndex) {
        this.sequenceIndex = sequenceIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        PlaylistEntryLocation test = ProxiedClassUtil.cast(obj, PlaylistEntryLocation.class);
        return /*super.equals(test)
                &&*/ (getPlaylist() == test.getPlaylist() || (getPlaylist() != null && getPlaylist().equals(test.getPlaylist())))
                && (getSequenceIndex() == test.getSequenceIndex());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (int) (this.getSequenceIndex() ^ (this.getSequenceIndex() >>> 32));
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(PlaylistEntryLocation anotherPlaylistEntryID) {
        //Sort by sequence order
        //Sorted through ascending filter
        if (anotherPlaylistEntryID==null) return 1;
        long diff = getSequenceIndex() - anotherPlaylistEntryID.getSequenceIndex();
        return diff==0?0:(diff>0?1:-1);
    }
}
