/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.track.CommonTagInterface;
import biz.ddcr.shampoo.server.domain.track.PEGIRating;

/**
 *
 * Selected overriding tags for all tracks played within the same playlist
 * This tag set will be used instead of the playlist tracks's
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PlaylistTagSet implements CommonTagInterface {

    private String title;
    private String author;
    private String album;
    private String genre;
    private Short dateOfRelease;
    private String description;
    private String publisher;
    private String copyright;
    private PEGIRating rating;
    //Barely used misc. flag
    private String tag;

    public PlaylistTagSet() {
    }
    public PlaylistTagSet(PlaylistTagSet o) {
        setAlbum( o.getAlbum() );
        setAuthor( o.getAuthor() );
        setDateOfRelease( o.getDateOfRelease() );
        setDescription( o.getDescription() );
        setGenre( o.getGenre() );
        setTitle( o.getTitle() );
        setPublisher( o.getPublisher() );
        setCopyright( o.getCopyright() );
        setRating( o.getRating() );
        setTag( o.getTag() );
    }

    @Override
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public Short getDateOfRelease() {
        return dateOfRelease;
    }

    public void setDateOfRelease(Short dateOfRelease) {
        this.dateOfRelease = dateOfRelease;
    }

    @Override
    public PEGIRating getRating() {
        return rating;
    }

    public void setRating(PEGIRating rating) {
        this.rating = rating;
    }

    @Override
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public PlaylistTagSet shallowCopy() {
        return new PlaylistTagSet(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlaylistTagSet other = (PlaylistTagSet) obj;
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.author == null) ? (other.author != null) : !this.author.equals(other.author)) {
            return false;
        }
        if ((this.album == null) ? (other.album != null) : !this.album.equals(other.album)) {
            return false;
        }
        if ((this.genre == null) ? (other.genre != null) : !this.genre.equals(other.genre)) {
            return false;
        }
        if (this.dateOfRelease != other.dateOfRelease && (this.dateOfRelease == null || !this.dateOfRelease.equals(other.dateOfRelease))) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        if ((this.publisher == null) ? (other.publisher != null) : !this.publisher.equals(other.publisher)) {
            return false;
        }
        if ((this.copyright == null) ? (other.copyright != null) : !this.copyright.equals(other.copyright)) {
            return false;
        }
        if (this.rating != other.rating && (this.rating == null || !this.rating.equals(other.rating))) {
            return false;
        }
        if ((this.tag == null) ? (other.tag != null) : !this.tag.equals(other.tag)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 67 * hash + (this.author != null ? this.author.hashCode() : 0);
        hash = 67 * hash + (this.album != null ? this.album.hashCode() : 0);
        hash = 67 * hash + (this.genre != null ? this.genre.hashCode() : 0);
        hash = 67 * hash + (this.dateOfRelease != null ? this.dateOfRelease.hashCode() : 0);
        hash = 67 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 67 * hash + (this.publisher != null ? this.publisher.hashCode() : 0);
        hash = 67 * hash + (this.copyright != null ? this.copyright.hashCode() : 0);
        hash = 67 * hash + (this.rating != null ? this.rating.hashCode() : 0);
        hash = 67 * hash + (this.tag != null ? this.tag.hashCode() : 0);
        return hash;
    }

}
