package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;


/**
 * A static PlaylistEntry is an Playlist item and represents a single Track
 *
 * @author okay_awright
 */
public class StaticPlaylistEntry extends PlaylistEntry {

    private BroadcastableTrack track;

    protected StaticPlaylistEntry() {
    }
    public StaticPlaylistEntry(Playlist playlist, long sequenceIndex) {
        super(playlist, sequenceIndex);
    }
    public StaticPlaylistEntry(StaticPlaylistEntry o) {
        super(o);
        addTrack( o.getTrack() );
    }

    public BroadcastableTrack getTrack() {
        return track;
    }

    private void setTrack(BroadcastableTrack track) {
        this.track = track;
    }
    public void addTrack(BroadcastableTrack track) {
        addTrack(track, true);
    }
    public void addTrack(BroadcastableTrack track, boolean doLink) {
        if (track!=null) {
            setTrack(track);
            if (doLink) track.addSlot(this, false);
        }
    }
    public void removeTrack() {
        removeTrack(true);
    }
    public void removeTrack(boolean doLink) {
        if (getTrack()!=null) {
            if (doLink) getTrack().removeSlot(this, false);
            setTrack(null);
        }
    }

    @Override
    public void removePlaylist(boolean doLink) {
        super.removePlaylist(doLink);
        //If the playlist is unlinked, the entry is meant to be deleted, thus the bound track reference must be cleared out as well
        removeTrack();
    }


    @Override
    public StaticPlaylistEntry shallowCopy() {
        return new StaticPlaylistEntry(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        StaticPlaylistEntry test = ProxiedClassUtil.cast(obj, StaticPlaylistEntry.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(PlaylistEntryVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean isDuplicate(PlaylistEntry otherPlaylistEntry) {
        return isDuplicate(true, otherPlaylistEntry);
    }
    @Override
    public boolean isDuplicateNoPlaylist(PlaylistEntry otherPlaylistEntry) {
        return isDuplicate(false, otherPlaylistEntry);
    }
    @Override
    public boolean isDuplicate(boolean doReverseChecking, PlaylistEntry otherPlaylistEntry) {
        if (otherPlaylistEntry==null || !ProxiedClassUtil.castableAs(otherPlaylistEntry, StaticPlaylistEntry.class))
            return false;
        StaticPlaylistEntry otherStaticPlaylistEntry = (StaticPlaylistEntry)otherPlaylistEntry;
        //Just check attributes not relationships used as business keys
        if (doReverseChecking) {
            if (getPlaylist()!=null) {
                if (!getPlaylist().equals(otherStaticPlaylistEntry.getPlaylist())) return false;
            } else {
                if (otherStaticPlaylistEntry.getPlaylist()!=null) return false;
            }
        }
        if (getSequenceIndex()!=otherStaticPlaylistEntry.getSequenceIndex()) return false;
        if (getLoop()!=null) {
            if (!getLoop().equals(otherStaticPlaylistEntry.getLoop())) return false;
        } else {
            if (otherStaticPlaylistEntry.getLoop()!=null) return false;
        }
        if (getFadeIn()!=null) {
            if (!getFadeIn().equals(otherStaticPlaylistEntry.getFadeIn())) return false;
        } else {
            if (otherStaticPlaylistEntry.getFadeIn()!=null) return false;
        }
        if (isRequestAllowed()!=otherStaticPlaylistEntry.isRequestAllowed()) return false;
        if (getTrack()!=null) {
            //Hibernate workaround, we won't force the proxy to be resolved but we rely on the fact both playlist should be linked to the same proxy if equal
            if (getTrack() != otherStaticPlaylistEntry.getTrack()) return false;
            //if (!getTrack().equals(otherStaticPlaylistEntry.getTrack())) return false;
        } else {
            if (otherStaticPlaylistEntry.getTrack()!=null) return false;
        }

        return true;
    }

}
