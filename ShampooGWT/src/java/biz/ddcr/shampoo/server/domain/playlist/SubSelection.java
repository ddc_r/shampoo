/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.playlist;

import biz.ddcr.shampoo.server.domain.track.SORT;
import java.io.Serializable;

/**
 *
 * Rule feature
 *
 * @author okay_awright
 **/
public class SubSelection implements Serializable {

    /** criterion for the rule; must be not null for non-random rules, if used within RandomRules it is used as a subset delimiter only **/
    private SORT sort;
    /**
     * Zero-based index of the beginning of the selection
     *
     **/
    private Long startIndex;
    /**
     *
     * Zero-based index of the end of the selection
     *
     **/
    private Long endIndex;
    /**
     *
     * Last used index within the boundaries of startIndex and endIndex; used only by OrderedSequenceRules to determine the next track to play
     *
     */
    private Long currentIndex;

    public SubSelection() {
    }

    public Long getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Long endIndex) {
        this.endIndex = endIndex;
    }

    public Long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Long startIndex) {
        this.startIndex = startIndex;
    }

    public Long getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(Long currentIndex) {
        this.currentIndex = currentIndex;
    }

    public SORT getSort() {
        return sort;
    }

    public void setSort(SORT sort) {
        this.sort = sort;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubSelection other = (SubSelection) obj;
        if (this.sort != other.sort) {
            return false;
        }
        if (this.startIndex != other.startIndex && (this.startIndex == null || !this.startIndex.equals(other.startIndex))) {
            return false;
        }
        if (this.endIndex != other.endIndex && (this.endIndex == null || !this.endIndex.equals(other.endIndex))) {
            return false;
        }
        if (this.currentIndex != other.currentIndex && (this.currentIndex == null || !this.currentIndex.equals(other.currentIndex))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.sort != null ? this.sort.hashCode() : 0);
        hash = 53 * hash + (this.startIndex != null ? this.startIndex.hashCode() : 0);
        hash = 53 * hash + (this.endIndex != null ? this.endIndex.hashCode() : 0);
        hash = 53 * hash + (this.currentIndex != null ? this.currentIndex.hashCode() : 0);
        return hash;
    }

}
