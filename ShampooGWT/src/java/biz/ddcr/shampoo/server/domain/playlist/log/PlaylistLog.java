/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.playlist.log;

import biz.ddcr.shampoo.server.domain.journal.Log.LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PlaylistLog extends Log {

    public static enum PLAYLIST_LOG_OPERATION implements LOG_OPERATION {

        processing,
        edit,
        delete,
        add;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case processing:
                        return systemConfigurationHelper.isLogPlaylistCreate();
                    case add:
                        return systemConfigurationHelper.isLogPlaylistCreate();
                    case edit:
                        return systemConfigurationHelper.isLogPlaylistUpdate();
                    case delete:
                        return systemConfigurationHelper.isLogPlaylistDelete();
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, Playlist playlist) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (playlist!=null && canLog(systemConfigurationHelper))
                allBoundChannels.addAll(playlist.getProgramme().getChannels());
            return allBoundChannels;
        }
    }
    /** The current operation performed on the actee **/
    private PLAYLIST_LOG_OPERATION action;
    /** uncryptic version of acteeID **/
    private String playlistCaption;

    protected PlaylistLog() {
        super();
    }

    @Override
    public PlaylistLog shallowCopy() {
        return new PlaylistLog(this);
    }
    
    public PlaylistLog(PLAYLIST_LOG_OPERATION operation, String refID, String label, Set<Channel> channels) {
        super(refID, channels);
        setAction(operation);
        setPlaylistCaption(label);
    }

    public PlaylistLog(PlaylistLog o) {
        super(o);
        setAction(o.getAction());
        setPlaylistCaption(o.getPlaylistCaption());
    }

    @Override
    public PLAYLIST_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(PLAYLIST_LOG_OPERATION action) {
        this.action = action;
    }

    public String getPlaylistCaption() {
        return playlistCaption;
    }

    public void setPlaylistCaption(String playlistCaption) {
        this.playlistCaption = playlistCaption;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
