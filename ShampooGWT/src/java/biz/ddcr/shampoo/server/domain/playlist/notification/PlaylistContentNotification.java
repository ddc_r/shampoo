/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.playlist.notification;

import biz.ddcr.shampoo.server.domain.notification.CONTENT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.ContentUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PlaylistContentNotification extends ContentUpdatedNotification<String> {

    @Override
    public String getFriendlyEntityCaption() {
        return getPlaylistCaption() + " [" + getEntityID() + "]";
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case add:
                    return "Playlist "+getFriendlyEntityCaption()+" has been added";
                case delete:
                    return "Playlist "+getFriendlyEntityCaption()+" has been deleted";
                case processing:
                    return "Processing playlist "+getFriendlyEntityCaption();
                case edit:
                    return "Playlist "+getFriendlyEntityCaption()+" has been edited";
            }
        }
        return null;
    }

    public static enum PLAYLIST_NOTIFICATION_OPERATION implements CONTENT_NOTIFICATION_OPERATION {

        add,
        delete,
        processing,
        edit;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Playlist playlist) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {
                //Find recipients from the computed list of channels and programmes
                return securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                        playlist.getProgramme()!=null ? playlist.getProgramme().getChannels() : null,
                        Collections.singleton(playlist.getProgramme()),
                        Playlist.getNotificationRights());
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private String playlistCaption;
    private PLAYLIST_NOTIFICATION_OPERATION action;

    protected PlaylistContentNotification() {
        super();
    }

    @Override
    public PlaylistContentNotification shallowCopy() {
        return new PlaylistContentNotification(this);
    }
    
    public PlaylistContentNotification(PLAYLIST_NOTIFICATION_OPERATION action, Playlist playlist, RestrictedUser recipient) {
        super(playlist!=null ? playlist.getRefID() : null,
              recipient);
        setAction( action );
        setPlaylistCaption(playlist!=null ? playlist.getLabel() : null);
    }
    public PlaylistContentNotification(PlaylistContentNotification o) {
        super( o );
        setAction( o.getAction() );
        setPlaylistCaption( o.getPlaylistCaption() );
    }

    @Override
    public PLAYLIST_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(PLAYLIST_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getPlaylistCaption() {
        return playlistCaption;
    }

    protected void setPlaylistCaption(String playlistCaption) {
        this.playlistCaption = playlistCaption;
    }

}
