/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.playlist.notification;

import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PlaylistProgrammeLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyDestinationCaption() {
        return getDestinationID();
    }

    @Override
    public String getFriendlySourceCaption() {
        return getPlaylistCaption() + " [" + getSourceID() + "]";
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "Playlist "+getFriendlySourceCaption()+" is no longer linked to programme "+getFriendlyDestinationCaption();
                case add:
                    return "Playlist "+getFriendlySourceCaption()+" is now linked to programme "+getFriendlyDestinationCaption();
            }
        }
        return null;
    }

    public static enum PLAYLISTPROGRAMME_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        delete,
        add;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Playlist playlist, Programme programme) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (playlist!=null) {

                    //if the operation is "delete" then re-add the original programme to the list bound to the track
                    Programme originalProgramme;
                    if (this==delete) {
                        originalProgramme = programme;
                    } else {
                        originalProgramme = playlist.getProgramme();
                    }

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    originalProgramme.getChannels(),
                                    Collections.singleton(originalProgramme),
                                    Playlist.getNotificationRights())
                            );
                }

                if (programme!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    programme.getChannels(),
                                    Collections.singleton(programme),
                                    Programme.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private String playlistCaption;
    private PLAYLISTPROGRAMME_NOTIFICATION_OPERATION action;

    protected PlaylistProgrammeLinkNotification() {
        super();
    }

    @Override
    public PlaylistProgrammeLinkNotification shallowCopy() {
        return new PlaylistProgrammeLinkNotification(this);
    }
    
    public PlaylistProgrammeLinkNotification(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION action, Playlist playlist, Programme programme, RestrictedUser recipient) {
        super(playlist!=null ? playlist.getRefID() : null,
              programme!=null ? programme.getLabel() : null,
              recipient);
        setAction( action );
        setPlaylistCaption(playlist!=null ? playlist.getLabel() : null);
    }
    public PlaylistProgrammeLinkNotification(PlaylistProgrammeLinkNotification o) {
        super( o );
        setAction( o.getAction() );
        setPlaylistCaption( o.getPlaylistCaption() );
    }

    @Override
    public PLAYLISTPROGRAMME_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getPlaylistCaption() {
        return playlistCaption;
    }

    protected void setPlaylistCaption(String playlistCaption) {
        this.playlistCaption = playlistCaption;
    }

}
