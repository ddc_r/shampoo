package biz.ddcr.shampoo.server.domain.programme;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.TrackProgramme;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A programme scheduled on a specific channel
 *
 * @author okay_awright
 **/
public class Programme extends GenericTimestampedEntity implements Comparable<Programme> {

    private String label;
    private String description;
    private Set<RestrictedUser> animatorRights;
    private Set<RestrictedUser> curatorRights;
    private Set<RestrictedUser> contributorRights;
    private Set<Channel> channels;
    private Set<TrackProgramme> trackProgrammes;
    private Set<Playlist> playlists;
    private Set<TimetableSlot> timetableSlots;
    /**
     * The pattern that will be displayed in the listener's player through Shoutcast, Icecast, etc
     * It's made of a sequence of substitutable variables that are resolved when an item metadata must be displayed
     **/
    private String metaInfoPattern;
    /**
     * Check whether the playlist are cut short or are allowed the overlap the next one until their last item has stopped
     * i.e. let the last item from a playlist plays even if it overlaps the next playlist start time
     * This feature depends on the abilities of the streamer that handles the channel, liquidsoap 0.91 cannot respect this directive
     */
    private boolean overlappingPlaylistAllowed;

    public Programme() {
    }

    public Programme(Programme o) throws Exception {
        super(o);
        addAnimatorRights(o.getAnimatorRights());
        addChannels(o.getChannels());
        addContributorRights(o.getContributorRights());
        addCuratorRights(o.getContributorRights());
        setDescription(o.getDescription());
        setLabel(o.getLabel());
        setMetaInfoPattern(o.getMetaInfoPattern());
        addPlaylists(o.getPlaylists());
        addTrackProgrammes(o.getTrackProgrammes());
        addTimetableSlots(o.getTimetableSlots());
        setOverlappingPlaylistAllowed(o.isOverlappingPlaylistAllowed());
    }

    public Set<RestrictedUser> getAnimatorRights() {
        if (animatorRights == null) {
            animatorRights = new HashSet<RestrictedUser>();
        }
        return animatorRights;
    }

    private void setAnimatorRights(Set<RestrictedUser> animatorRights) {
        this.animatorRights = animatorRights;
    }

    public void addAnimatorRight(RestrictedUser animatorRight) {
        if (animatorRight != null) {
            getAnimatorRights().add(animatorRight);
            animatorRight.getAnimatorRights().add(this);
        }
    }

    public void addAnimatorRights(Set<RestrictedUser> animatorRights) {
        if (animatorRights != null) {
            for (RestrictedUser animatorRight : animatorRights) {
                addAnimatorRight(animatorRight);
            }
        }
    }

    public void removeAnimatorRight(RestrictedUser animatorRight) {
        if (animatorRight != null) {
            getAnimatorRights().remove(animatorRight);
            animatorRight.getAnimatorRights().remove(this);
        }
    }

    public void removeAnimatorRights(Set<RestrictedUser> animatorRights) {
        if (animatorRights != null) {
            for (RestrictedUser animatorRight : animatorRights) {
                removeAnimatorRight(animatorRight);
            }
        }
    }

    public void clearAnimatorRights() {
        for (RestrictedUser animatorRight : getAnimatorRights()) {
            animatorRight.getAnimatorRights().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getAnimatorRights().clear();
        //animatorRights = new HashSet<RestrictedUser>();
    }

    public Set<RestrictedUser> getContributorRights() {
        if (contributorRights == null) {
            contributorRights = new HashSet<RestrictedUser>();
        }
        return contributorRights;
    }

    private void setContributorRights(Set<RestrictedUser> contributorRights) {
        this.contributorRights = contributorRights;
    }

    public void addContributorRight(RestrictedUser contributorRight) {
        if (contributorRight != null) {
            getContributorRights().add(contributorRight);
            contributorRight.getContributorRights().add(this);
        }
    }

    public void addContributorRights(Set<RestrictedUser> contributorRights) {
        if (contributorRights != null) {
            for (RestrictedUser contributorRight : contributorRights) {
                addContributorRight(contributorRight);
            }
        }
    }

    public void removeContributorRight(RestrictedUser contributorRight) {
        if (contributorRight != null) {
            getContributorRights().remove(contributorRight);
            contributorRight.getContributorRights().remove(this);
        }
    }

    public void removeContributorRights(Set<RestrictedUser> contributorRights) {
        if (contributorRights != null) {
            for (RestrictedUser contributorRight : contributorRights) {
                removeContributorRight(contributorRight);
            }
        }
    }

    public void clearContributorRights() {
        for (RestrictedUser contributorRight : getContributorRights()) {
            contributorRight.getContributorRights().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getContributorRights().clear();
        //contributorRights = new HashSet<RestrictedUser>();
    }

    public Set<RestrictedUser> getCuratorRights() {
        if (curatorRights == null) {
            curatorRights = new HashSet<RestrictedUser>();
        }
        return curatorRights;
    }

    private void setCuratorRights(Set<RestrictedUser> curatorRights) {
        this.curatorRights = curatorRights;
    }

    public void addCuratorRight(RestrictedUser curatorRight) {
        if (curatorRight != null) {
            getCuratorRights().add(curatorRight);
            curatorRight.getCuratorRights().add(this);
        }
    }

    public void addCuratorRights(Set<RestrictedUser> curatorRights) {
        if (curatorRights != null) {
            for (RestrictedUser curatorRight : curatorRights) {
                addCuratorRight(curatorRight);
            }
        }
    }

    public void removeCuratorRight(RestrictedUser curatorRight) {
        if (curatorRight != null) {
            getCuratorRights().remove(curatorRight);
            curatorRight.getCuratorRights().remove(this);
        }
    }

    public void removeCuratorRights(Set<RestrictedUser> curatorRights) {
        if (curatorRights != null) {
            for (RestrictedUser curatorRight : curatorRights) {
                removeCuratorRight(curatorRight);
            }
        }
    }

    public void clearCuratorRights() {
        for (RestrictedUser curatorRight : getCuratorRights()) {
            curatorRight.getCuratorRights().remove(this);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getCuratorRights().clear();
        //curatorRights = new HashSet<RestrictedUser>();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<Channel> getChannels() {
        if (channels == null) {
            channels = new HashSet<Channel>();
        }
        return channels;
    }

    private void setChannels(Set<Channel> channels) {
        this.channels = channels;
    }

    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            getChannels().add(channel);
            if (doLink) {
                channel.addProgramme(this, false);
            }
        }
    }
    
    public void addChannels(Set<Channel> channels) {
        if (channels != null) {
            for (Channel channel : channels) {
                addChannel(channel);
            }
        }
    }

    public void removeChannel(Channel channel) {
        if (channel != null) {
            getChannels().remove(channel);
            channel.getProgrammes().remove(this);
        }
    }

    public void removeChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            getChannels().remove(channel);
            if (doLink) {
                channel.removeProgramme(this, false);
            }
        }
    }
    
    public void removeChannels(Set<Channel> channels) {
        if (channels != null) {
            for (Channel channel : channels) {
                removeChannel(channel);
            }
        }
    }

    public void clearChannels() {
        for (Channel channel : getChannels()) {
            channel.removeProgramme(this, false);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getChannels().clear();
        //channels = new HashSet<Channel>();
    }

    public Collection<Track> getImmutableTracks() {
        Collection<Track> tracks = new HashSet<Track>();
        for (Iterator<TrackProgramme> i = getTrackProgrammes().iterator(); i.hasNext(); ) {
            TrackProgramme trackProgramme = i.next();
            if (trackProgramme.getTrack()!=null)
                tracks.add(trackProgramme.getTrack());
            //DIRTY FIX Cleanup fix for trackProgramme that should be removed from the list
            else if (trackProgramme.getProgramme()==null)
                i.remove();
        }
        return tracks;
    }

    public Set<TrackProgramme> getTrackProgrammes() {
        if (trackProgrammes == null) {
            trackProgrammes = new HashSet<TrackProgramme>();
        }
        return trackProgrammes;
    }

    private void setTrackProgrammes(Set<TrackProgramme> trackProgrammes) {
        this.trackProgrammes = trackProgrammes;
    }

    public void addTrackProgramme(TrackProgramme trackProgramme) throws Exception {
        addTrackProgramme(trackProgramme, true);
    }

    public void addTrackProgramme(final TrackProgramme trackProgramme, boolean doLink) throws Exception {
        if (trackProgramme != null) {
            if (doLink && trackProgramme.getTrack() != null) {
                TrackVisitor visitor = new TrackVisitor() {

                    @Override
                    public void visit(PendingSong track) throws Exception {
                        trackProgramme.setProgramme(Programme.this);
                        trackProgramme.setRotation(null);
                        getTrackProgrammes().add(trackProgramme);
                        trackProgramme.getTrack().addTrackProgramme(trackProgramme, false);
                    }

                    @Override
                    public void visit(PendingJingle track) throws Exception {
                        trackProgramme.setProgramme(Programme.this);
                        trackProgramme.setRotation(null);
                        getTrackProgrammes().add(trackProgramme);
                        trackProgramme.getTrack().addTrackProgramme(trackProgramme, false);
                    }

                    @Override
                    public void visit(PendingAdvert track) throws Exception {
                        trackProgramme.setProgramme(Programme.this);
                        trackProgramme.setRotation(null);
                        getTrackProgrammes().add(trackProgramme);
                        trackProgramme.getTrack().addTrackProgramme(trackProgramme, false);
                    }

                    @Override
                    public void visit(BroadcastableSong track) throws Exception {
                        trackProgramme.setProgramme(Programme.this);
                        getTrackProgrammes().add(trackProgramme);
                        trackProgramme.getTrack().addTrackProgramme(trackProgramme, false);
                    }

                    @Override
                    public void visit(BroadcastableJingle track) throws Exception {
                        trackProgramme.setProgramme(Programme.this);
                        getTrackProgrammes().add(trackProgramme);
                        trackProgramme.getTrack().addTrackProgramme(trackProgramme, false);
                    }

                    @Override
                    public void visit(BroadcastableAdvert track) throws Exception {
                        trackProgramme.setProgramme(Programme.this);
                        getTrackProgrammes().add(trackProgramme);
                        trackProgramme.getTrack().addTrackProgramme(trackProgramme, false);
                    }
                };
                trackProgramme.getTrack().acceptVisit(visitor);
            }
        }
    }

    protected void addTrackProgrammes(Set<TrackProgramme> trackProgrammes) throws Exception {
        if (trackProgrammes != null) {
            for (TrackProgramme trackProgramme : trackProgrammes) {
                addTrackProgramme(trackProgramme);
            }
        }
    }

    public void removeTrackProgramme(final TrackProgramme trackProgramme) throws Exception {
        removeTrackProgramme(trackProgramme, true);
    }
    
    public void removeTrackProgramme(final TrackProgramme trackProgramme, boolean doLink) throws Exception {
        if (trackProgramme != null) {                 
            //Unlink the trackprogramme both on the track (there) and on the programme (here) ends
            //Since we use delete-orphan, don't clear the actual references in trackprogramme
            trackProgramme.drop(doLink, true);
        }
    }

    protected void removeTrackProgrammes(Set<TrackProgramme> trackProgrammes) throws Exception {
        if (trackProgrammes != null) {
            for (TrackProgramme trackProgramme : trackProgrammes) {
                removeTrackProgramme(trackProgramme);
            }
        }
    }

    public void clearTrackProgrammes() throws Exception {
        for (Iterator<TrackProgramme> i = getTrackProgrammes().iterator();i.hasNext();) {
            TrackProgramme trackProgramme = i.next();
            i.remove();
            //Unlink the trackprogramme both on the track (there) and on the programme (here) ends
            //Since we use delete-orphan, don't clear the actual references in trackprogramme
            trackProgramme.drop(false, true);
        }
    }

    public void addTrack(Track track) throws Exception {
        addTrack(track, true);
    }

    public void addTrack(Track track, final boolean doLink) throws Exception {
        if (track != null) {
            final TrackProgramme newTrackProgramme = new TrackProgramme();
            TrackVisitor visitor = new TrackVisitor() {

                @Override
                public void visit(PendingAdvert track) throws Exception {
                    newTrackProgramme.setTrack(track);
                    newTrackProgramme.setRotation(null);
                    addTrackProgramme(newTrackProgramme, doLink);
                }

                @Override
                public void visit(PendingJingle track) throws Exception {
                    newTrackProgramme.setTrack(track);
                    newTrackProgramme.setRotation(null);
                    addTrackProgramme(newTrackProgramme, doLink);
                }

                @Override
                public void visit(PendingSong track) throws Exception {
                    newTrackProgramme.setTrack(track);
                    newTrackProgramme.setRotation(null);
                    addTrackProgramme(newTrackProgramme, doLink);
                }

                @Override
                public void visit(BroadcastableAdvert track) throws Exception {
                    newTrackProgramme.setTrack(track);
                    //Add a new track means there's no rotation yet most of the time
                    //to override this default behaviour, directly inject a TrackProgramme directly
                    newTrackProgramme.setRotation(0L);
                    addTrackProgramme(newTrackProgramme, doLink);
                }

                @Override
                public void visit(BroadcastableJingle track) throws Exception {
                    newTrackProgramme.setTrack(track);
                    //Add a new track means there's no rotation yet most of the time
                    //to override this default behaviour, directly inject a TrackProgramme directly
                    newTrackProgramme.setRotation(0L);
                    addTrackProgramme(newTrackProgramme, doLink);
                }

                @Override
                public void visit(BroadcastableSong track) throws Exception {
                    newTrackProgramme.setTrack(track);
                    //Add a new track means there's no rotation yet most of the time
                    //to override this default behaviour, directly inject a TrackProgramme directly
                    newTrackProgramme.setRotation(0L);
                    addTrackProgramme(newTrackProgramme, doLink);
                }
            };
            track.acceptVisit(visitor);
        }
    }

    public void addTracks(Collection<Track> tracks) throws Exception {
        if (tracks != null) {
            for (Track track : tracks) {
                addTrack(track);
            }
        }
    }

    /*public void removeTrack(Track track) throws Exception {
        removeTrack(track, true);
    }

    public void removeTrack(Track track, final boolean doLink) throws Exception {
        if (track != null) {
            TrackProgramme newTrackProgramme = new TrackProgramme();
            newTrackProgramme.setTrack(track);
            newTrackProgramme.setProgramme(Programme.this);
            removeTrackProgramme(newTrackProgramme, doLink);
        }
    }

    public void removeTracks(Collection<Track> tracks) throws Exception {
        if (tracks != null) {
            for (Track track : tracks) {
                removeTrack(track);
            }
        }
    }

    public void clearTracks() throws Exception {
        clearTrackProgrammes();
    }*/

    public Set<Playlist> getPlaylists() {
        if (playlists == null) {
            playlists = new HashSet<Playlist>();
        }
        return playlists;
    }

    private void setPlaylists(Set<Playlist> playlists) {
        this.playlists = playlists;
    }

    public void addPlaylist(Playlist playlist) {
        addPlaylist(playlist, true);
    }

    public void addPlaylist(Playlist playlist, boolean doLink) {
        if (playlist != null) {
            getPlaylists().add(playlist);
            if (doLink) {
                playlist.addProgramme(this, false);
            }
        }
    }

    public void addPlaylists(Set<Playlist> playlists) {
        if (playlists != null) {
            for (Playlist playlist : playlists) {
                addPlaylist(playlist);
            }
        }
    }

    public void removePlaylist(Playlist playlist) {
        removePlaylist(playlist, true);
    }

    public void removePlaylist(Playlist playlist, boolean doLink) {
        if (playlist != null) {
            getPlaylists().remove(playlist);
            if (doLink) {
                playlist.removeProgramme(false);
            }
        }
    }

    public void removePlaylists(Set<Playlist> playlists) {
        if (playlists != null) {
            for (Playlist playlist : playlists) {
                removePlaylist(playlist);
            }
        }
    }

    public void clearPlaylists() {
        for (Playlist playlist : getPlaylists()) {
            playlist.removeProgramme(false);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getPlaylists().clear();
        //playlists = new HashSet<Playlist>();
    }

    public String getMetaInfoPattern() {
        return metaInfoPattern;
    }

    public void setMetaInfoPattern(String metaInfoPattern) {
        this.metaInfoPattern = metaInfoPattern;
    }

    public Set<RestrictedUser> getImmutableRestrictedUsers() {
        Set<RestrictedUser> users = new HashSet<RestrictedUser>();
        users.addAll(getCuratorRights());
        users.addAll(getContributorRights());
        users.addAll(getAnimatorRights());
        return users;
    }

    public Set<TimetableSlot> getTimetableSlots() {
        if (timetableSlots == null) {
            timetableSlots = new HashSet<TimetableSlot>();
        }
        return timetableSlots;
    }

    private void setTimetableSlots(Set<TimetableSlot> slots) {
        this.timetableSlots = slots;
    }

    public void addTimetableSlot(TimetableSlot timetableSlot) {
        addTimetableSlot(timetableSlot, true);
    }

    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            getTimetableSlots().add(timetableSlot);
            if (doLink) {
                timetableSlot.addProgramme(this, false);
            }
        }
    }

    public void addTimetableSlots(Set<TimetableSlot> timetableSlots) {
        if (timetableSlots != null) {
            for (TimetableSlot timetableSlot : timetableSlots) {
                addTimetableSlot(timetableSlot);
            }
        }
    }

    public void removeTimetableSlot(TimetableSlot timetableSlot) {
        removeTimetableSlot(timetableSlot, true);
    }

    public void removeTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            getTimetableSlots().remove(timetableSlot);
            if (doLink) {
                timetableSlot.removeProgramme(false);
            }
        }
    }

    public void removeTimetableSlots(Set<TimetableSlot> timetableSlots) {
        if (timetableSlots != null) {
            for (TimetableSlot timetableSlot : timetableSlots) {
                removeTimetableSlot(timetableSlot);
            }
        }
    }

    public void clearTimetableSlots() {
        for (TimetableSlot timetableSlot : getTimetableSlots()) {
            timetableSlot.removeProgramme(false);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getTimetableSlots().clear();
        //timetableSlots = new HashSet<TimetableSlot>();
    }

    public boolean isOverlappingPlaylistAllowed() {
        return overlappingPlaylistAllowed;
    }

    public void setOverlappingPlaylistAllowed(boolean overlappingPlaylistAllowed) {
        this.overlappingPlaylistAllowed = overlappingPlaylistAllowed;
    }

    @Override
    public Programme shallowCopy() throws Exception {
        return new Programme(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        Programme test = ProxiedClassUtil.cast(obj, Programme.class);
        return /*super.equals(test)
                &&*/ (getLabel().equals(test.getLabel()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLabel() ? 0 : getLabel().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setListenerRights(true);
        return rightsBundle;
    }

    public static RightsBundle getLimitedTrackBindingRights() {
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    public static RightsBundle getAddRights() {
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }

    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }

    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }

    @Override
    public int compareTo(Programme anotherProgramme) {
        if (anotherProgramme != null) {
            return (this.getLabel() == null
                    ? 0
                    : (anotherProgramme.getLabel() == null
                    ? 0
                    : getLabel().compareTo(anotherProgramme.getLabel())));
        } else {
            return 0;
        }
    }

    public static Collection<Channel> getChannelsFromProgrammes(Collection<Programme> programmes) {
        Collection<Channel> channels = new HashSet<Channel>();
        if (programmes != null && !programmes.isEmpty()) {
            for (Programme programme : programmes) {
                channels.addAll(programme.getChannels());
            }
        }
        return channels;
    }
        
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
