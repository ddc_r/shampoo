/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.programme.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ProgrammeAnimatorLinkNotification extends ProgrammeRestrictedUserLinkNotification {

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "User "+getFriendlyDestinationCaption()+" is no longer an animator of programme "+getFriendlySourceCaption();
                case add:
                    return "User "+getFriendlyDestinationCaption()+" is now an animator of programme "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    public static enum PROGRAMMEANIMATOR_NOTIFICATION_OPERATION implements PROGRAMMERESTRICTEDUSER_NOTIFICATION_OPERATION {

        delete,
        add;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Programme programme, RestrictedUser restrictedUser) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (programme!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    programme.getChannels(),
                                    Collections.singleton(programme),
                                    Programme.getNotificationRights())
                            );
                }

                if (restrictedUser!=null) {

                    //if the operation is "delete" then re-add the original programme to the list bound to the track
                    Collection<Programme> originalProgrammes = restrictedUser.getImmutableProgrammes();
                    if (this==delete) {
                        originalProgrammes.add(programme);
                    }

                    //Add channels from programmes, as all user-based decisions
                    Collection<Channel> channels = restrictedUser.getImmutableChannels();
                    channels.addAll(Programme.getChannelsFromProgrammes(originalProgrammes));
                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    channels,
                                    originalProgrammes,
                                    RestrictedUser.getNotificationRights())
                            );

                    //Add the concerned user too
                    recipients.add(restrictedUser);
                }

                //Find recipients from the computed list of programmes and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private PROGRAMMEANIMATOR_NOTIFICATION_OPERATION action;

    protected ProgrammeAnimatorLinkNotification() {
        super();
    }

    @Override
    public ProgrammeAnimatorLinkNotification shallowCopy() {
        return new ProgrammeAnimatorLinkNotification(this);
    }
    
    public ProgrammeAnimatorLinkNotification(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION action, Programme programme, RestrictedUser restrictedUser, RestrictedUser recipient) {
        super(programme, restrictedUser, recipient);
        setAction( action );
    }
    public ProgrammeAnimatorLinkNotification(ProgrammeAnimatorLinkNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public PROGRAMMEANIMATOR_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
