/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.programme.notification;

import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ProgrammeBroadcastableTrackLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyDestinationCaption() {
        return getTrackAuthorCaption() + " - " + getTrackTitleCaption() + " [" + getDestinationID() + "]";
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case delete:
                    return "Broadcastable track "+getFriendlyDestinationCaption()+" is no longer linked to programme "+getFriendlySourceCaption();
                case add:
                    return "Broadcastable track "+getFriendlyDestinationCaption()+" is now linked to programme "+getFriendlySourceCaption();
                case validate:
                    return "Broadcastable track "+getFriendlyDestinationCaption()+" has been validated for programme "+getFriendlySourceCaption();
            }
        }
        return null;
    }

    public static enum PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION implements LINK_NOTIFICATION_OPERATION {

        delete,
        add,
        validate;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Programme programme, BroadcastableTrack broadcastTrack) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {

                Collection<RestrictedUser> recipients = new HashSet<RestrictedUser>();

                if (programme!=null) {

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    programme.getChannels(),
                                    Collections.singleton(programme),
                                    Programme.getNotificationRights())
                            );
                }

                if (broadcastTrack!=null) {

                    //if the operation is "delete" then re-add the original programme to the list bound to the track
                    Collection<Programme> originalProgrammes;
                    if (this==delete) {
                        originalProgrammes = new HashSet<Programme>();
                        originalProgrammes.addAll(broadcastTrack.getImmutableProgrammes());
                        originalProgrammes.add(programme);
                    } else {
                        originalProgrammes = broadcastTrack.getImmutableProgrammes();
                    }

                    recipients.addAll(
                                securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                                    Programme.getChannelsFromProgrammes(originalProgrammes),
                                    originalProgrammes,
                                    BroadcastableTrack.getNotificationRights())
                            );
                }

                //Find recipients from the computed list of channels and programmes
                return recipients;
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private String trackAuthorCaption;
    private String trackTitleCaption;
    private PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION action;

    protected ProgrammeBroadcastableTrackLinkNotification() {
        super();
    }

    @Override
    public ProgrammeBroadcastableTrackLinkNotification shallowCopy() {
        return new ProgrammeBroadcastableTrackLinkNotification(this);
    }
    
    public ProgrammeBroadcastableTrackLinkNotification(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION action, Programme programme, BroadcastableTrack broadcastTrack, RestrictedUser recipient) {
        super(programme!=null ? programme.getLabel() : null,
              broadcastTrack!=null ? broadcastTrack.getRefID() : null,
              recipient);
        setAction( action );
        setTrackAuthorCaption( broadcastTrack!=null ? broadcastTrack.getAuthor() : null );
        setTrackTitleCaption( broadcastTrack!=null ? broadcastTrack.getTitle() : null );
    }
    public ProgrammeBroadcastableTrackLinkNotification(ProgrammeBroadcastableTrackLinkNotification o) {
        super( o );
        setAction( o.getAction() );
        setTrackAuthorCaption( o.getTrackAuthorCaption() );
        setTrackTitleCaption( o.getTrackTitleCaption() );
    }

    @Override
    public PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getTrackAuthorCaption() {
        return trackAuthorCaption;
    }

    protected void setTrackAuthorCaption(String trackAuthorCaption) {
        this.trackAuthorCaption = trackAuthorCaption;
    }

    public String getTrackTitleCaption() {
        return trackTitleCaption;
    }

    protected void setTrackTitleCaption(String trackTitleCaption) {
        this.trackTitleCaption = trackTitleCaption;
    }

}
