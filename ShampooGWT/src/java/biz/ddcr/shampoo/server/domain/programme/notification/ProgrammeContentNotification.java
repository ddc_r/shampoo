/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.programme.notification;

import biz.ddcr.shampoo.server.domain.playlist.*;
import biz.ddcr.shampoo.server.domain.notification.CONTENT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.ContentUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ProgrammeContentNotification extends ContentUpdatedNotification<String> {

    @Override
    public String getFriendlyEntityCaption() {
        return getEntityID();
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case add:
                    return "Programme "+getFriendlyEntityCaption()+" has been added";
                case delete:
                    return "Programme "+getFriendlyEntityCaption()+" has been deleted";
                case edit:
                    return "Programme "+getFriendlyEntityCaption()+" has been edited";
            }
        }
        return null;
    }

    public static enum PROGRAMME_NOTIFICATION_OPERATION implements CONTENT_NOTIFICATION_OPERATION {

        add,
        delete,
        edit;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Programme programme) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {
                //Find recipients from the computed list of channels and programmes
                return securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                        programme.getChannels(),
                        Collections.singleton(programme),
                        Playlist.getNotificationRights());
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private PROGRAMME_NOTIFICATION_OPERATION action;

    protected ProgrammeContentNotification() {
        super();
    }

    @Override
    public ProgrammeContentNotification shallowCopy() {
        return new ProgrammeContentNotification(this);
    }
    
    public ProgrammeContentNotification(PROGRAMME_NOTIFICATION_OPERATION action, Programme programme, RestrictedUser recipient) {
        super(programme!=null ? programme.getLabel() : null,
              recipient);
        setAction( action );
    }
    public ProgrammeContentNotification(ProgrammeContentNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public PROGRAMME_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(PROGRAMME_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
