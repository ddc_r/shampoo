/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.programme.notification;

import biz.ddcr.shampoo.server.domain.notification.AssociationUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.LINK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class ProgrammeRestrictedUserLinkNotification extends AssociationUpdatedNotification<String, String> {

    @Override
    public String getFriendlyDestinationCaption() {
        return getDestinationID();
    }

    @Override
    public String getFriendlySourceCaption() {
        return getSourceID();
    }

    public interface PROGRAMMERESTRICTEDUSER_NOTIFICATION_OPERATION extends LINK_NOTIFICATION_OPERATION {};

    protected ProgrammeRestrictedUserLinkNotification() {
        super();
    }

    public ProgrammeRestrictedUserLinkNotification(Programme channel, RestrictedUser restrictedUser, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              restrictedUser!=null ? restrictedUser.getUsername() : null,
              recipient);
    }
    public ProgrammeRestrictedUserLinkNotification(ProgrammeRestrictedUserLinkNotification o) {
        super( o );
    }

}
