/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Blank;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BlankQueueItem extends NonSchedulableQueueItem implements BlankQueueItemInterface {

    private Long naturalDuration;

    protected BlankQueueItem() {
        super();
    }

    public BlankQueueItem(Channel channel, long sequenceIndex, long naturalDuration, long timeMarkerBuffer) {
        super( channel, sequenceIndex, timeMarkerBuffer );
        setNaturalDuration(naturalDuration);
    }
    public BlankQueueItem(Channel channel, long sequenceIndex, long naturalDuration) {
        super( channel, sequenceIndex );
        setNaturalDuration(naturalDuration);
    }

    public BlankQueueItem(BlankQueueItemInterface o) {
        super( o );
    }

    @Override
    public Blank getItem() {
        return new Blank();
    }

    /** Hibernate-artifact only **/
    private void setNaturalDuration(long naturalDuration) {
        this.naturalDuration = naturalDuration;
    }

    @Override
    public long getNaturalDuration() {
        //A duration must have been filled in when the item was created
        return naturalDuration;
    }

    @Override
    public BlankQueueItem shallowCopy() {
        return new BlankQueueItem(this);
    }

    @Override
    public void acceptVisit(QueueItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    /** Required by WrappedItem but Blank is not a SchedulableItem **/
    public TimetableSlot getTimetableSlot() {
        return null;
    }

}
