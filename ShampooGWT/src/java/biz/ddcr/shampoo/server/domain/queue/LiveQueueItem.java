/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class LiveQueueItem extends SchedulableQueueItem implements LiveQueueItemInterface {

    private TimetableSlot timetableSlot;
    private Long naturalDuration;

    protected LiveQueueItem() {
        super();
    }

    public LiveQueueItem(Channel channel, long sequenceIndex, TimetableSlot timetableSlot, long naturalDuration, long timeMarkerBuffer) {
        super(channel, sequenceIndex, timeMarkerBuffer);
        addTimetableSlot(timetableSlot);
        setNaturalDuration(naturalDuration);
    }
    public LiveQueueItem(Channel channel, long sequenceIndex, TimetableSlot timetableSlot, long naturalDuration) {
        super(channel, sequenceIndex);
        addTimetableSlot(timetableSlot);
        setNaturalDuration(naturalDuration);
    }

    public LiveQueueItem(LiveQueueItemInterface o) {
        super(o);
        addTimetableSlot(o.getTimetableSlot());
    }

    /** Hibernate-artifact only **/
    private void setNaturalDuration(long naturalDuration) {
        this.naturalDuration = naturalDuration;
    }

    @Override
    public long getNaturalDuration() {
        //A duration must have been filled in when the item was created
        return naturalDuration;
    }

    @Override
    public Live getItem() {
        return getTimetableSlot() != null && getTimetableSlot().getPlaylist() != null ? getTimetableSlot().getPlaylist().getLive() : null;
    }

    @Override
    public Live getLive() {
        return getItem();
    }

    @Override
    public TimetableSlot getTimetableSlot() {
        return timetableSlot;
    }

    @Override
    public void setTimetableSlot(TimetableSlot timetableSlot) {
        this.timetableSlot = timetableSlot;
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot) {
        addTimetableSlot(timetableSlot, true);
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            setTimetableSlot(timetableSlot);
            if (doLink) {
                timetableSlot.addQueueItem(this, false);
            }
        }
    }

    @Override
    public void removeTimetableSlot() {
        removeTimetableSlot(true);
    }

    @Override
    public void removeTimetableSlot(boolean doLink) {
        if (getTimetableSlot() != null) {
            if (doLink) {
                getTimetableSlot().removeQueueItem(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setTimetableSlot(null);
        }
    }

    /*public Long getNaturalDuration() {
        //There's no definite length for a live, we'll assume it will last as long as the playlist it belongs to
        if (getTimetableSlot() != null) {
            return getTimetableSlot().getEndTime().getUnixTime() - getStartTime().getUnixTime();
        }
        return null;
    }*/

    @Override
    public LiveQueueItem shallowCopy() {
        return new LiveQueueItem(this);
    }

    @Override
    public void acceptVisit(QueueItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
