package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.playlist.Live;

public interface LiveQueueItemInterface extends SchedulableQueueItemInterface {

    public Live getLive();

}
