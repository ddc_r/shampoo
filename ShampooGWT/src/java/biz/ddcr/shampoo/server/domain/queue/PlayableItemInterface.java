/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import java.io.Serializable;

/**
 * Tag an object as being able to be queued up for streaming
 *
 * Classes that use this interface are required to provide a visitor-pattern kind of inspection mechanism
 * Implement it everywhere an Hibernate proxy is used or where the list of instanceof is too cumbersome
 * Can be used along with the standard VisitorPatternInterface method
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface PlayableItemInterface<T extends PlayableItemVisitor> extends Serializable {
    /** visitor pattern inspection **/
    public void is(T visitor) throws Exception;
    
    /**
     * Dummy visitor pattern marker interface to extend
     *
     * @author okay_awright <okay_awright AT ddcr DOT biz>
     */
    public interface Visitor {}
}
