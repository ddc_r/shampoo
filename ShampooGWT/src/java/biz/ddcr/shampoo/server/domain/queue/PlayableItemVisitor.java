/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface.Visitor;
import biz.ddcr.shampoo.server.domain.playlist.Blank;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.Request;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface PlayableItemVisitor extends Visitor {

    public void visit(BroadcastableAdvert track) throws Exception;
    public void visit(BroadcastableJingle track) throws Exception;
    public void visit(BroadcastableSong track) throws Exception;
    public void visit(Live live) throws Exception;
    public void visit(Blank blank) throws Exception;
    public void visit(Request request) throws Exception;

}
