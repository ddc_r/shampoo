/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import org.joda.time.DateTimeZone;

/**
 *
 * An entry in a Channel queue
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class QueueItem implements QueueItemInterface {

    //Timezone defaults to the JVM's own
    private final static String QUEUE_TIMEZONE = DateTimeZone.getDefault().getID();

    /** Hibernate versioning */
    private int version;

    private String refID;

    private QueueItemLocation location;

    /**
     * specifies if the item has already been pooled by StreamerListenerInterface using getNextQueueItem()
     * It also means if it's pooled, then it's already been stored within the streamer internal queue and cannot be edited anymore
     */
    private boolean pooled;
    
    /** mark the indicative time of play for this item, it may be updated at any time
     * it should not be used for any other purpose than computing a next likely item for the queue
     * It should not be null
     */
    private YearMonthWeekDayHourMinuteSecondMillisecondInterface likelyStartTime;
    /** mark the indicative length of play for this item, it may be updated at any time
     * it should not be used for any other purpose than computing a next likely item for the queue
     * it is a millisecond-based variable
     */
    /** Do not use it: internal ORM placeholder **/
    private Long _likelyStartUnixTimestamp;
    
    private long likelyDuration;

    private Long timeMarkerAtEnd;

    protected QueueItem() {
    }

    public QueueItem(Channel channel, long sequenceIndex, long timeMarkerAtEnd) {
        setSequenceIndex(sequenceIndex);
        setTimeMarkerAtEnd(timeMarkerAtEnd);
        //Add senesible default values for both pooled and playing
        setPooled(false);
        addChannel(channel);
    }
    public QueueItem(Channel channel, long sequenceIndex) {
        setSequenceIndex(sequenceIndex);
        //Add senesible default values for both pooled and playing
        setPooled(false);
        setTimeMarkerAtEnd(null);
        addChannel(channel);
    }

    public QueueItem(QueueItemInterface o) {
        setSequenceIndex( o.getSequenceIndex() );
        setPooled( o.isPooled() );
        setLikelyStartTime( o.getStartTime() );
        setLikelyDuration( o.getLikelyDuration() );
        setTimeMarkerAtEnd( o.getTimeMarkerAtEnd() );
        addChannel( o.getChannel() );
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    @Override
    public void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public QueueItemLocation getLocation() {
        if (location == null) {
            location = new QueueItemLocation();
        }
        return location;
    }

    @Override
    public void setLocation(QueueItemLocation location) {
        this.location = location;
    }

    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getLikelyStartTime() {
        if (likelyStartTime == null && _likelyStartUnixTimestamp != null) {
            likelyStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _likelyStartUnixTimestamp,
                    QUEUE_TIMEZONE);
        }
        return likelyStartTime;
    }
    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getStartTime() {
        return getLikelyStartTime();
    }

    @Override
    public void setLikelyStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface likelyStartTime) {
        this.likelyStartTime = likelyStartTime;
    }
    public void setStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface likelyStartTime) {
        setLikelyStartTime(likelyStartTime);
    }

    @Override
    public Long getLikelyStartTimestamp() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (likelyStartTime == null) {
            return _likelyStartUnixTimestamp;
        } else {
            return likelyStartTime.getUnixTime();
        }
    }

    @Override
    public void setLikelyStartTimestamp(Long unixTimestamp) {
        _likelyStartUnixTimestamp = unixTimestamp;
    }    
    
    @Override
    public long getLikelyDuration() {
        return likelyDuration;
    }
    @Override
    public long getDuration() {
        return getLikelyDuration();
    }

    @Override
    public void setLikelyDuration(long likelyDuration) {
        this.likelyDuration = likelyDuration;
    }

    @Override
    public Channel getChannel() {
        return getLocation().getChannel();
    }

    @Override
    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    @Override
    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            getLocation().setChannel(channel);
            if (doLink) {
                channel.addQueueItem(this, false);
            }
        }
    }

    @Override
    public void removeChannel() {
        removeChannel(true);
    }

    @Override
    public void removeChannel(boolean doLink) {
        if (getLocation().getChannel() != null) {
            if (doLink) {
                getLocation().getChannel().removeQueueItem(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setChannel(null);
        }
    }

    @Override
    public long getSequenceIndex() {
        return getLocation().getSequenceIndex();
    }

    @Override
    public void setSequenceIndex(long sequenceIndex) {
        getLocation().setSequenceIndex(sequenceIndex);
    }

    @Override
    public boolean isPooled() {
        return pooled;
    }

    @Override
    public void setPooled(boolean pooled) {
        this.pooled = pooled;
    }

    @Override
    public void setTimeMarkerAtEnd(Long timeMarkerBuffer) {
        this.timeMarkerAtEnd = timeMarkerBuffer;
    }

    @Override
    public Long getTimeMarkerAtEnd() {
        return timeMarkerAtEnd;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        final QueueItem test = ProxiedClassUtil.cast(obj, QueueItem.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }

        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLocation() ? 0 : getLocation().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(QueueItemInterface anotherQueueItem) {
        //Sort by rank
        if (anotherQueueItem!=null)
            return (getLocation() == null ?
                (anotherQueueItem.getLocation()==null?0:-1)
                : getLocation().compareTo(anotherQueueItem.getLocation()) );
        else
            return 1;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setListenerRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }
    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }
    
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
