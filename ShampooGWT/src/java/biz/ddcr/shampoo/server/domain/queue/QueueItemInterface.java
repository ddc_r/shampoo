package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.streamer.GenericPersistableItemInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;
import java.util.Comparator;

/**
 * A track or a live that has been played on a specific channel
 * Its reference is mostly recorded for logging purpose, but it can be also used by a streamer for computing a working queue
 * dateStamp marks the time of play for this item
 * Most attributes are specified as soft references only, the objects may be removed the attributes will remain
 *
 * @author okay_awright
 **/
public interface QueueItemInterface extends GenericPersistableItemInterface, Comparable<QueueItemInterface>, VisitorPatternInterface<QueueItemVisitor> {

    /** Used for Hibernate sorting of SortedSets, it must be static! **/
    /** But Ehcache needs it to be Serializable vene if static **/
    public static class HibernateComparator implements Serializable, Comparator<QueueItemInterface> {

        @Override
        public int compare(QueueItemInterface t, QueueItemInterface t1) {
            if (t==null)
                return t1==null?0:-1;
            else
                return t.compareTo(t1);
        }

    }

    /** Do not use: Hibernate artefact **/
    public void setRefID(String refID);

    public QueueItemLocation getLocation();
    /** Do not use: Hibernate artefact **/
    public void setLocation(QueueItemLocation location);

    /** Do not use: Hibernate artefact **/
    public Long getLikelyStartTimestamp();
    /** Do not use: Hibernate artefact **/
    public void setLikelyStartTimestamp(Long unixTimestamp);

    public void setLikelyStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime);

    public long getLikelyDuration();
    public void setLikelyDuration(long duration);

    public long getNaturalDuration();

    public void addChannel(Channel channel);
    public void addChannel(Channel channel, boolean doLink);
    public void removeChannel();
    public void removeChannel(boolean doLink);

    public long getSequenceIndex();
    public void setSequenceIndex(long sequenceIndex);

    public boolean isPooled();
    public void setPooled(boolean pooled);

    public PlayableItemInterface getItem();

    /** Marker of playlist change or other misc. events, located between this tiem and the next one
     getStartTime()+getLikelyDuration()+getTimeMarkerAtEnd() == time marker **/
    public Long getTimeMarkerAtEnd();
    public void setTimeMarkerAtEnd(Long timeMarkerAtEnd);
    
}
