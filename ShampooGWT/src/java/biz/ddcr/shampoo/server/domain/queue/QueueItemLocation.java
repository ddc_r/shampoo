/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class QueueItemLocation implements Serializable, Comparable<QueueItemLocation> {

    private Channel channel;
    /**
     * The sequence order of the item in the channel queue; -1 if it's an emergency item and is not actually registered within the queue
     **/
    private long sequenceIndex;

    public QueueItemLocation() {
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public long getSequenceIndex() {
        return sequenceIndex;
    }

    public void setSequenceIndex(long sequenceIndex) {
        this.sequenceIndex = sequenceIndex;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        final QueueItemLocation other = ProxiedClassUtil.cast(obj, QueueItemLocation.class);
        if (this.channel != other.channel && (this.channel == null || !this.channel.equals(other.channel))) {
            return false;
        }
        if (this.sequenceIndex != other.sequenceIndex) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.sequenceIndex ^ (this.sequenceIndex >>> 32));
        return hash;
    }

    @Override
    public int compareTo(QueueItemLocation anotherQueueItemLocation) {
        //Sort by channel, then by sequence order
        //Sorted through ascending filter
        int result = 0;
        if (anotherQueueItemLocation!=null)
            result = (getChannel() == null ?
                (anotherQueueItemLocation.getChannel()==null?0:-1)
                : getChannel().compareTo(anotherQueueItemLocation.getChannel()) );
        else
            return 1;
        if (result==0) {
            long diff = getSequenceIndex() - anotherQueueItemLocation.getSequenceIndex();
            return diff==0?0:(diff>0?1:-1);
        } else
            return 0;
    }

}
