/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.Request;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class RequestQueueItem extends TrackQueueItem implements RequestQueueItemInterface {

    /** The actual request;provides channel, programme, and song to this object **/
    private Request request;

    protected RequestQueueItem() {
        super();
    }

    public RequestQueueItem(Request request, long sequenceIndex, TimetableSlot timetableSlot, long timeMarkerBuffer) {
        super( request.getChannel(), sequenceIndex, timetableSlot, timeMarkerBuffer );
        addRequest(request);
    }
    public RequestQueueItem(Request request, long sequenceIndex, TimetableSlot timetableSlot) {
        super( request.getChannel(), sequenceIndex, timetableSlot );
        addRequest(request);
    }

    public RequestQueueItem(RequestQueueItemInterface o) {
        super( o );
        addRequest( o.getRequest() );
    }

    @Override
    public Request getRequest() {
        return request;
    }

    @Override
    public void setRequest(Request request) {
        this.request = request;
    }

    @Override
    public void addRequest(Request request) {
        addRequest(request, true);
    }

    @Override
    public void addRequest(Request request, boolean doLink) {
        if (request != null) {
            setRequest(request);
            if (doLink) {
                request.addQueueItem(this, false);
            }
        }
    }

    @Override
    public void removeRequest() {
        removeRequest(true);
    }

    @Override
    public void removeRequest(boolean doLink) {
        if (getRequest() != null) {
            if (doLink) {
                getRequest().removeQueueItem(false);
            }
            //Put delete-orphan in the hibernate mapping
            setRequest(null);
        }
    }

    @Override
    public BroadcastableSong getItem() {
        return getRequest().getSong();
    }

    @Override
    public long getNaturalDuration() {
        //No duration specified along with this item, compute a likely duration based on the file's own duration
        if (getItem() != null && getItem().getTrackContainer() != null) {
            //getDuration() is a second-based variable
            return (long)(getItem().getTrackContainer().getDuration() * 1000.);
        }
        //No item, no time
        return 0L;
    }

    @Override
    public RequestQueueItem shallowCopy() {
        return new RequestQueueItem(this);
    }

    @Override
    public void acceptVisit(QueueItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
