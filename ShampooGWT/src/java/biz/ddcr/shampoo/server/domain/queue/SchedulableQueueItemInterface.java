package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.streamer.GenericSchedulableItemInterface;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

public interface SchedulableQueueItemInterface extends QueueItemInterface, GenericSchedulableItemInterface {

    /** Do not use: Hibernate artefact **/
    public void setTimetableSlot(TimetableSlot timetableSlot);
    public void addTimetableSlot(TimetableSlot timetableSlot);
    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink);
    public void removeTimetableSlot();
    public void removeTimetableSlot(boolean doLink);

}
