/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SimpleTrackQueueItem extends TrackQueueItem implements SimpleTrackQueueItemInterface {

    private BroadcastableTrack item;

    protected SimpleTrackQueueItem() {
        super();
    }

    public SimpleTrackQueueItem(Channel channel, long sequenceIndex, TimetableSlot timetableSlot, BroadcastableTrack track, long timeMarkerBuffer) {
        super( channel, sequenceIndex, timetableSlot, timeMarkerBuffer );
        addItem(track);
    }
    public SimpleTrackQueueItem(Channel channel, long sequenceIndex, TimetableSlot timetableSlot, BroadcastableTrack track) {
        super( channel, sequenceIndex, timetableSlot );
        addItem(track);
    }

    public SimpleTrackQueueItem(TrackQueueItemInterface o) {
        super( o );
        addItem( o.getItem() );
    }

    @Override
    public BroadcastableTrack getItem() {
        return item;
    }

    @Override
    public void setItem(BroadcastableTrack item) {
        this.item = item;
    }

    @Override
    public void addItem(BroadcastableTrack item) {
        addItem(item, true);
    }

    @Override
    public void addItem(BroadcastableTrack item, boolean doLink) {
        if (item != null) {
            if (doLink) {
                item.addQueueItem(this, false);
            }
            setItem(item);
        }
    }

    @Override
    public void removeItem() {
        removeItem(true);
    }

    @Override
    public void removeItem(boolean doLink) {
        if (getItem() != null) {
            if (doLink) {
                getItem().removeQueueItem(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setItem(null);
        }
    }

    @Override
    public long getNaturalDuration() {
        //No duration specified along with this item, compute a likely duration based on the file's own duration
        if (getItem() != null && getItem().getTrackContainer() != null) {
            //getDuration() is a second-based variable
            return (long)(getItem().getTrackContainer().getDuration() * 1000.);
        }
        //No item, no time
        return 0L;
    }

    @Override
    public SimpleTrackQueueItem shallowCopy() {
        return new SimpleTrackQueueItem(this);
    }

    @Override
    public void acceptVisit(QueueItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
