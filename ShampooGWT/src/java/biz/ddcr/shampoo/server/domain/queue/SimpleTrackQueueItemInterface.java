package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;

public interface SimpleTrackQueueItemInterface extends TrackQueueItemInterface {

    /**Do NOT use: Hibernate artefact**/
    public void setItem(BroadcastableTrack item);
    public void addItem(BroadcastableTrack item);
    public void addItem(BroadcastableTrack item, boolean doLink);

    public void removeItem();
    public void removeItem(boolean doLink);

}
