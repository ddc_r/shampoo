/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class TrackQueueItem extends SchedulableQueueItem implements TrackQueueItemInterface {

    /**
     * Mark where exactly in the playlist the item is located
     * it should not be used for any other purpose than computing a next likely item for the queue
     */
    private long playlistEntryCurrentSequenceIndex;
    private long playlistEntryCurrentLoopIndex;

    private Integer playlistEntryCurrentFadeIn;
    
    private TimetableSlot timetableSlot;

    protected TrackQueueItem() {
        super();
    }

    protected TrackQueueItem(Channel channel, long sequenceIndex, TimetableSlot timetableSlot, long timeMarkerBuffer) {
        super( channel, sequenceIndex, timeMarkerBuffer );
        addTimetableSlot(timetableSlot);
    }
    protected TrackQueueItem(Channel channel, long sequenceIndex, TimetableSlot timetableSlot) {
        super( channel, sequenceIndex );
        addTimetableSlot(timetableSlot);
     }

    public TrackQueueItem(TrackQueueItemInterface o) {
        super( o );
        addTimetableSlot( o.getTimetableSlot() );
        setPlaylistEntryCurrentFadeIn( o.getPlaylistEntryCurrentFadeIn() );
    }

    @Override
    public abstract long getNaturalDuration();

    @Override
    public long getPlaylistEntryCurrentLoopIndex() {
        return playlistEntryCurrentLoopIndex;
    }

    @Override
    public void setPlaylistEntryCurrentLoopIndex(long playlistEntryCurrentLoopIndex) {
        this.playlistEntryCurrentLoopIndex = playlistEntryCurrentLoopIndex;
    }

    @Override
    public long getPlaylistEntryCurrentSequenceIndex() {
        return playlistEntryCurrentSequenceIndex;
    }

    @Override
    public void setPlaylistEntryCurrentSequenceIndex(long playlistEntryCurrentSequenceIndex) {
        this.playlistEntryCurrentSequenceIndex = playlistEntryCurrentSequenceIndex;
    }

    @Override
    public Integer getPlaylistEntryCurrentFadeIn() {
        return playlistEntryCurrentFadeIn;
    }

    @Override
    public void setPlaylistEntryCurrentFadeIn(Integer playlistEntryCurrentFadeIn) {
        this.playlistEntryCurrentFadeIn = playlistEntryCurrentFadeIn;
    }

    @Override
    public TimetableSlot getTimetableSlot() {
        return timetableSlot;
    }

    @Override
    public void setTimetableSlot(TimetableSlot timetableSlot) {
        this.timetableSlot = timetableSlot;
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot) {
        addTimetableSlot(timetableSlot, true);
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            setTimetableSlot(timetableSlot);
            if (doLink) {
                timetableSlot.addQueueItem(this, false);
            }
        }
    }

    @Override
    public void removeTimetableSlot() {
        removeTimetableSlot(true);
    }

    @Override
    public void removeTimetableSlot(boolean doLink) {
        if (getTimetableSlot() != null) {
            if (doLink) {
                getTimetableSlot().removeQueueItem(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setTimetableSlot(null);
        }
    }

}
