package biz.ddcr.shampoo.server.domain.queue;

import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;

public interface TrackQueueItemInterface extends SchedulableQueueItemInterface {

    @Override
    public BroadcastableTrack getItem();

    public long getPlaylistEntryCurrentLoopIndex();
    public void setPlaylistEntryCurrentLoopIndex(long playlistEntryCurrentLoopIndex);

    public long getPlaylistEntryCurrentSequenceIndex();
    public void setPlaylistEntryCurrentSequenceIndex(long playlistEntryCurrentSequenceIndex);

        /** Whether and how it should blend into the previous queueditem*/
    public Integer getPlaylistEntryCurrentFadeIn();
    public void setPlaylistEntryCurrentFadeIn(Integer fadeIn);

}
