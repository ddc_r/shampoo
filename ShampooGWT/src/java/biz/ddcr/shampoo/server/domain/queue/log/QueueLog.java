/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.queue.log;

import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.queue.QueueItemInterface;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemInterface;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class QueueLog extends Log {

    public static enum QUEUE_LOG_OPERATION implements LOG_OPERATION {

        add,
        delete;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case add:
                        return systemConfigurationHelper.isLogQueueCreate();
                    case delete:
                        return systemConfigurationHelper.isLogQueueDelete();
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, QueueItemInterface queueItem) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (queueItem!=null && canLog(systemConfigurationHelper))
                allBoundChannels.add(queueItem.getChannel());
            return allBoundChannels;
        }
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, StreamItemInterface streamItem) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (streamItem!=null && canLog(systemConfigurationHelper))
                allBoundChannels.add(streamItem.getChannel());
            return allBoundChannels;
        }
    }
    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** The current operation performed on the actee **/
    private QUEUE_LOG_OPERATION action;
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;
    /** What this queue is bound to */
    private String channelCaption;

    protected QueueLog() {
        super();
    }

    @Override
    public QueueLog shallowCopy() {
        return new QueueLog(this);
    }
    
    public QueueLog(QUEUE_LOG_OPERATION operation, String refID, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, String channelId, Set<Channel> channels) {
        super(refID, channels);
        setAction(operation);
        setQueueItemChannelCaption(channelId);
        setQueueItemStartTimeWithCorrection(startTime);
    }

    public QueueLog(QueueLog o) {
        super(o);
        setAction(o.getAction());
        setQueueItemChannelCaption(o.getQueueItemChannelCaption());
        setQueueItemRawStartTime(o.getQueueItemStartTime());
    }

    @Override
    public QUEUE_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(QUEUE_LOG_OPERATION action) {
        this.action = action;
    }

    public String getQueueItemChannelCaption() {
        return channelCaption;
    }

    public void setQueueItemChannelCaption(String channelCaption) {
        this.channelCaption = channelCaption;
    }

    protected Long getQueueItemStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _timestamp;
        } else {
            return getQueueItemStartCalendar().getUnixTime();
        }
    }

    protected void setQueueItemStartTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecondInterface getQueueItemStartCalendar() {
        if (startCalendar == null && _timestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getQueueItemStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getQueueItemStartCalendar().getYear(),
                getQueueItemStartCalendar().getMonthOfYear(),
                getQueueItemStartCalendar().getDayOfMonth(),
                getQueueItemStartCalendar().getHourOfDay(),
                getQueueItemStartCalendar().getMinuteOfHour(),
                getQueueItemStartCalendar().getSecondOfMinute(),
                getQueueItemStartCalendar().getMillisecondOfSecond(),
                getQueueItemStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    public boolean setQueueItemStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setQueueItemRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getQueueItemStartCalendar().setYear(date.getYear());
            getQueueItemStartCalendar().setMonthOfYear(date.getMonthOfYear());
            getQueueItemStartCalendar().setDayOfMonth(date.getDayOfMonth());
            getQueueItemStartCalendar().setHourOfDay(date.getHourOfDay());
            getQueueItemStartCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getQueueItemStartCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getQueueItemStartCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
