/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BlankQueueStreamItem extends SchedulableStreamItem implements BlankQueueStreamItemInterface {

    protected BlankQueueStreamItem() {
        super();
    }

    public BlankQueueStreamItem(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration, String queueId) {
        super(channel, startTime, duration, queueId);
    }

    public BlankQueueStreamItem(BlankQueueStreamItemInterface o) {
        super( o );
    }

    @Override
    public TimetableSlot getTimetableSlot() {
        //No timetable slot for blanks
        //TODO: play with interfaces instead of providing quirks for exceptions
        return null;
    }

    /** Do not use: phony Hibernate inheritance property **/
    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot) {
        //Do nothing
    }

    /** Do not use: phony Hibernate inheritance property **/
    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        //Do nothing
    }

    /** Do not use: phony Hibernate inheritance property **/
    @Override
    public void removeTimetableSlot() {
        //Do nothing
    }

    /** Do not use: phony Hibernate inheritance property **/
    @Override
    public void removeTimetableSlot(boolean doLink) {
        //Do nothing
    }

    /** Do not use: phony Hibernate inheritance property **/
    @Override
    public void setTimetableSlot(TimetableSlot timetableSlot) {
        //Do nothing
    }

    @Override
    public BlankQueueStreamItem shallowCopy() {
        return new BlankQueueStreamItem(this);
    }

    @Override
    public void acceptVisit(StreamItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
