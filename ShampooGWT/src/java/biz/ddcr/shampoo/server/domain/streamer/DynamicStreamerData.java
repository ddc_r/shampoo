/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import java.io.Serializable;

/**
 *
 * Settings for the streaming webservice bound to a channel
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DynamicStreamerData implements Serializable {

    /** protocol format for REST-like inetrfaces **/
    private SERIALIZATION_METADATA_FORMAT metadataFormat;
    /** number of items to queue within the application for the streamer, 0 or less means disable it **/
    private int queueMinItems = 1;
    /** For whatever reason, if the application cannot pick a track to send to the streamer, should we send a default file instead of an exception **/
    private boolean enableEmergencyItems = false;
    /** minimum duration of an item to play, 0 or lower means disable it **/
    private long minPlayableDuration = 1000;
    /** If a live can be queued, specify its URI path; it's a a string where variables are substituted **/
    private String queueableLiveURI;
    /** Can lives be picked by getNextTrack() and queued like normal items; set to false for liquidsoap 0.91 **/
    private boolean queueableLive = false;
    /** Should the live be split into multiple queue items of queueableLiveChunkSize minutes or queued as a whole; 0 or less means do not split it **/
    private long queueableLiveChunkSize = 0;
    /** If a blank can be queued, specify its URI path; it's a a string where variables are substituted **/
    private String queueableBlankURI;    
    /** Can off-air events (not bound to any timetable slot) be picked by getNextTrack() and queued like normal items; set to true for liquidsoap 0.91 **/
    private boolean queueableBlank = false;
    /** Should the blank be split into multiple queue items of queueableBlankChunkSize milliseconds or queued as a whole; 0 or less means do not split it **/
    private long queueableBlankChunkSize = 0;
    /** Loose URI to a playlist that contains listenable streams the streamer operates */
    private String streamURI;
    /** current streamer id caption handling this stream */
    private String userAgentID;
    /** latest recorded heartbeat, Unix Epoch; null means none yet **/
    private Long latestHeartbeat;
    /** max. interval between heartbeats before declaring a streamer dead and its seat free for use; O or null means don't use this feature, in milliseconds*/
    private Long ttl;
    /** attached channel streaming? void means initializing */
    private Boolean streaming;

    public DynamicStreamerData() {
        //Do nothing
    }
    
    public SERIALIZATION_METADATA_FORMAT getMetadataFormat() {
        if (metadataFormat==null)
            metadataFormat = SERIALIZATION_METADATA_FORMAT.genericXML;
        return metadataFormat;
    }

    public void setMetadataFormat(SERIALIZATION_METADATA_FORMAT metadataFormat) {
        this.metadataFormat = metadataFormat;
    }

    public boolean isEnableEmergencyItems() {
        return enableEmergencyItems;
    }

    public void setEnableEmergencyItems(boolean enableEmergencyItems) {
        this.enableEmergencyItems = enableEmergencyItems;
    }

    public boolean isQueueableLive() {
        return queueableLive;
    }

    public void setQueueableLive(boolean liveBehavesLikeTrack) {
        this.queueableLive = liveBehavesLikeTrack;
    }

    public boolean isQueueableBlank() {
        return queueableBlank;
    }

    public void setQueueableBlank(boolean queueableBlank) {
        this.queueableBlank = queueableBlank;
    }

    public int getQueueMinItems() {
        if (queueMinItems<0) queueMinItems = 0;
        return queueMinItems;
    }

    public void setQueueMinItems(int queueMinItems) {
        this.queueMinItems = queueMinItems;
    }

    public long getMinPlayableDuration() {
        if (minPlayableDuration<0) minPlayableDuration = 0;
        return minPlayableDuration;
    }

    public void setMinPlayableDuration(long minPlayableDuration) {
        this.minPlayableDuration = minPlayableDuration;
    }

    public long getQueueableBlankChunkSize() {
        if (queueableBlankChunkSize<0) queueableBlankChunkSize = 0;
        return queueableBlankChunkSize;
    }

    public void setQueueableBlankChunkSize(long queueableBlankChunkSize) {
        this.queueableBlankChunkSize = queueableBlankChunkSize;
    }

    public long getQueueableLiveChunkSize() {
        if (queueableLiveChunkSize<0) queueableLiveChunkSize = 0;
        return queueableLiveChunkSize;
    }

    public void setQueueableLiveChunkSize(long queueableLiveChunkSize) {
        this.queueableLiveChunkSize = queueableLiveChunkSize;
    }

    public String getStreamURI() {
        if (streamURI==null) streamURI="";
        return streamURI;
    }

    public void setStreamURI(String streamURI) {
        this.streamURI = streamURI;
    }

    public String getUserAgentID() {
        return userAgentID;
    }

    public void setUserAgentID(String userAgentID) {
        this.userAgentID = userAgentID;
    }    

    public Long getLatestHeartbeat() {
        return latestHeartbeat;
    }

    public void setLatestHeartbeat(Long latestHeartbeat) {
        this.latestHeartbeat = latestHeartbeat;
    }

    public Long getTtl() {
        return ttl;
    }

    public void setTtl(Long ttl) {
        this.ttl = ttl;
    }

    public String getQueueableLiveURI() {
        return queueableLiveURI;
    }

    public void setQueueableLiveURI(String queueableLiveURI) {
        this.queueableLiveURI = queueableLiveURI;
    }

    public String getQueueableBlankURI() {
        return queueableBlankURI;
    }

    public void setQueueableBlankURI(String queueableBlankURI) {
        this.queueableBlankURI = queueableBlankURI;
    }

    public Boolean isStreaming() {
        return streaming;
    }

    public void setStreaming(Boolean streaming) {
        this.streaming = streaming;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.metadataFormat != null ? this.metadataFormat.hashCode() : 0);
        hash = 37 * hash + this.queueMinItems;
        hash = 37 * hash + (this.enableEmergencyItems ? 1 : 0);
        hash = 37 * hash + (int) (this.minPlayableDuration ^ (this.minPlayableDuration >>> 32));
        hash = 37 * hash + (this.queueableLiveURI != null ? this.queueableLiveURI.hashCode() : 0);
        hash = 37 * hash + (this.queueableLive ? 1 : 0);
        hash = 37 * hash + (int) (this.queueableLiveChunkSize ^ (this.queueableLiveChunkSize >>> 32));
        hash = 37 * hash + (this.queueableBlankURI != null ? this.queueableBlankURI.hashCode() : 0);
        hash = 37 * hash + (this.queueableBlank ? 1 : 0);
        hash = 37 * hash + (int) (this.queueableBlankChunkSize ^ (this.queueableBlankChunkSize >>> 32));
        hash = 37 * hash + (this.streamURI != null ? this.streamURI.hashCode() : 0);
        hash = 37 * hash + (this.userAgentID != null ? this.userAgentID.hashCode() : 0);
        hash = 37 * hash + (this.latestHeartbeat != null ? this.latestHeartbeat.hashCode() : 0);
        hash = 37 * hash + (this.ttl != null ? this.ttl.hashCode() : 0);
        hash = 37 * hash + (this.streaming != null ? this.streaming.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DynamicStreamerData other = (DynamicStreamerData) obj;
        if (this.metadataFormat != other.metadataFormat) {
            return false;
        }
        if (this.queueMinItems != other.queueMinItems) {
            return false;
        }
        if (this.enableEmergencyItems != other.enableEmergencyItems) {
            return false;
        }
        if (this.minPlayableDuration != other.minPlayableDuration) {
            return false;
        }
        if ((this.queueableLiveURI == null) ? (other.queueableLiveURI != null) : !this.queueableLiveURI.equals(other.queueableLiveURI)) {
            return false;
        }
        if (this.queueableLive != other.queueableLive) {
            return false;
        }
        if (this.queueableLiveChunkSize != other.queueableLiveChunkSize) {
            return false;
        }
        if ((this.queueableBlankURI == null) ? (other.queueableBlankURI != null) : !this.queueableBlankURI.equals(other.queueableBlankURI)) {
            return false;
        }
        if (this.queueableBlank != other.queueableBlank) {
            return false;
        }
        if (this.queueableBlankChunkSize != other.queueableBlankChunkSize) {
            return false;
        }
        if ((this.streamURI == null) ? (other.streamURI != null) : !this.streamURI.equals(other.streamURI)) {
            return false;
        }
        if ((this.userAgentID == null) ? (other.userAgentID != null) : !this.userAgentID.equals(other.userAgentID)) {
            return false;
        }
        if (this.latestHeartbeat != other.latestHeartbeat && (this.latestHeartbeat == null || !this.latestHeartbeat.equals(other.latestHeartbeat))) {
            return false;
        }
        if (this.ttl != other.ttl && (this.ttl == null || !this.ttl.equals(other.ttl))) {
            return false;
        }
        if (this.streaming != other.streaming && (this.streaming == null || !this.streaming.equals(other.streaming))) {
            return false;
        }
        return true;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        return rightsBundle;
    }
    public static RightsBundle getQueryStatusRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setListenerRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        RightsBundle rightsBundle = new RightsBundle();
        return rightsBundle;
    }
    public static RightsBundle getNotificationRights() {
        //Channel Administrators can receive notifications, even if they cannot do much otherwise
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getListenRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setListenerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    
}
