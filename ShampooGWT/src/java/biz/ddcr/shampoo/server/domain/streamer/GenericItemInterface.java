package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;

/**
 * A track or a live that is now being played and streamed on a specific channel
 * It was either a QueueItem or a NonQueueItem, and may become a Stream item, and then an Archive
 *
 * @author okay_awright
 **/
public interface GenericItemInterface extends Serializable {

    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getStartTime();

    public Channel getChannel();

    public long getDuration();

}
