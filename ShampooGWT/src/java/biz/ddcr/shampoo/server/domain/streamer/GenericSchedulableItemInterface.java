package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

/**
 * A track or a live that is now being played and streamed on a specific channel
 * It was either a QueueItem or a NonQueueItem, and may become an Archive
 *
 * @author okay_awright
 **/
public interface GenericSchedulableItemInterface extends GenericItemInterface {

    public TimetableSlot getTimetableSlot();

}
