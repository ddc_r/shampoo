/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class LiveNonPersistableNonQueueItem extends NonPersistableNonQueueItem implements LiveNonPersistableNonQueueItemInterface {

    protected LiveNonPersistableNonQueueItem() {
        super();
    }

    public LiveNonPersistableNonQueueItem(TimetableSlot timetableSlot) {
        super( timetableSlot );
    }
    public LiveNonPersistableNonQueueItem(TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime) {
        super( timetableSlot, startTime, endTime );
    }

    public LiveNonPersistableNonQueueItem(LiveNonPersistableNonQueueItem o) {
        super( o );
    }

    @Override
    public Live getItem() {
        return getTimetableSlot()!=null && getTimetableSlot().getPlaylist()!=null ? getTimetableSlot().getPlaylist().getLive() : null;
    }
    @Override
    public Live getLive() {
        return getItem();
    }
    
    public LiveNonPersistableNonQueueItem shallowCopy() {
        return new LiveNonPersistableNonQueueItem(this);
    }

    @Override
    public void acceptVisit(NonPersistableNonQueueVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
