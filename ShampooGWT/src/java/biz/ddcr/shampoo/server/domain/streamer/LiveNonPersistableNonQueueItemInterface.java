package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.playlist.Live;

public interface LiveNonPersistableNonQueueItemInterface extends NonPersistableNonQueueItemInterface {

    public Live getLive();

}
