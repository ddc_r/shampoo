/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class LiveNonQueueItem extends NonPersistentNonQueueItem<Live> {

    protected LiveNonQueueItem() {
        super();
    }

    public LiveNonQueueItem(TimetableSlot timetableSlot) {
        super( timetableSlot );
    }

    public LiveNonQueueItem(LiveNonQueueItem o) {
        super( o );
    }

    @Override
    public Live getItem() {
        return getTimetableSlot()!=null && getTimetableSlot().getPlaylist()!=null ? getTimetableSlot().getPlaylist().getLive() : null;
    }

    public Long getStartTime() {
        return getTimetableSlot()!=null ? getTimetableSlot().getStartTimestamp() : null;
    }
    public Long getEndTime() {
        return getTimetableSlot()!=null ? getTimetableSlot().getEndTimestamp() : null;
    }
    
    public LiveNonQueueItem shallowCopy() {
        return new LiveNonQueueItem(this);
    }

    @Override
    public void acceptVisit(NonPersistentNonQueueItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
