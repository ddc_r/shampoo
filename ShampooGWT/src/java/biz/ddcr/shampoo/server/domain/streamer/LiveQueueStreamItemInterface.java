package biz.ddcr.shampoo.server.domain.streamer;

public interface LiveQueueStreamItemInterface extends LiveStreamItemInterface, SchedulableStreamItemInterface {

}
