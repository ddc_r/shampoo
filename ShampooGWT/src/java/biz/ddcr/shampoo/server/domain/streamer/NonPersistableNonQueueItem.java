/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class NonPersistableNonQueueItem implements NonPersistableNonQueueItemInterface {

    private TimetableSlot timetableSlot;
    private YearMonthWeekDayHourMinuteSecondMillisecondInterface fixedStartTime;
    private YearMonthWeekDayHourMinuteSecondMillisecondInterface fixedEndTime;

    protected NonPersistableNonQueueItem() {
    }

    public NonPersistableNonQueueItem(TimetableSlot timetableSlot) {
        setTimetableSlot(timetableSlot);
    }
    public NonPersistableNonQueueItem(TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime) {
        setTimetableSlot(timetableSlot);
        fixedStartTime = startTime;
        fixedEndTime = endTime;
    }

    public NonPersistableNonQueueItem(NonPersistableNonQueueItemInterface o) {
        setTimetableSlot( o.getTimetableSlot() );
    }

    @Override
    public Channel getChannel() {
        return getTimetableSlot().getChannel();
    }

    @Override
    public TimetableSlot getTimetableSlot() {
        return timetableSlot;
    }

    public void setTimetableSlot(TimetableSlot timetableSlot) {
        this.timetableSlot = timetableSlot;
    }

    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getStartTime() {
        if (fixedStartTime!=null)
            return fixedStartTime;
        else
            return getTimetableSlot()!=null ? getTimetableSlot().getStartTime() : null;
    }
    @Override
    public Long getStartTimestamp() {
        if (fixedStartTime!=null)
            return fixedStartTime.getUnixTime();
        else
            return getTimetableSlot()!=null ? getTimetableSlot().getStartTimestamp() : null;
    }

    @Override
    public void setStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface fixedStartTime) {
        this.fixedStartTime = fixedStartTime;
    }

    @Override
    public Long getEndTimestamp() {
        if (fixedEndTime!=null)
            return fixedEndTime.getUnixTime();
        else
            return getTimetableSlot()!=null ? getTimetableSlot().getEndTimestamp() : null;
    }

    @Override
    public void setEndTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface fixedEndTime) {
        this.fixedEndTime = fixedEndTime;
    }

    @Override
    public long getDuration() {
        Long absStart = getStartTimestamp();
        Long absEnd = getEndTimestamp();
        if (absStart!=null && absEnd!=null)
            return absEnd-absStart;
        else
            return -1;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        NonPersistableNonQueueItem test = ProxiedClassUtil.cast(obj, NonPersistableNonQueueItem.class);
        return super.equals(test)
                && (getTimetableSlot() == test.getTimetableSlot() || (getTimetableSlot() != null && getTimetableSlot().equals(test.getTimetableSlot())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getTimetableSlot() ? 0 : getTimetableSlot().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(NonPersistableNonQueueItemInterface anotherNonQueueItem) {
        //Sort by rank
        if (anotherNonQueueItem!=null)
            return (this.getTimetableSlot() == null ?
                0 :
                (anotherNonQueueItem.getTimetableSlot() == null ?
                    0 :
                    getTimetableSlot().compareTo(anotherNonQueueItem.getTimetableSlot()) ));
        else
            return 0;
    }

}
