package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 * A track or a live that has been played on a specific channel
 * Its reference is mostly recorded for logging purpose, but it can be also used by a streamer for computing a working queue
 * dateStamp marks the time of play for this item
 * Most attributes are specified as soft references only, the objects may be removed the attributes will remain
 *
 * @author okay_awright
 **/
public interface NonPersistableNonQueueItemInterface extends GenericSchedulableItemInterface, Comparable<NonPersistableNonQueueItemInterface>, VisitorPatternInterface<NonPersistableNonQueueVisitor> {

    public Long getStartTimestamp();
    public void setStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime);
    public Long getEndTimestamp();
    public void setEndTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime);

    public PlayableItemInterface getItem();

}
