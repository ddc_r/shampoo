/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.queue.*;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class NonPersistentNonQueueItem<T extends PlayableItemInterface> implements GenericItem<T>, VisitorPatternInterface<NonPersistentNonQueueItemVisitor> {

    private TimetableSlot timetableSlot;

    protected NonPersistentNonQueueItem() {
    }

    public NonPersistentNonQueueItem(TimetableSlot timetableSlot) {
        setTimetableSlot(timetableSlot);
    }

    public NonPersistentNonQueueItem(NonPersistentNonQueueItem<T> o) {
        setTimetableSlot( o.getTimetableSlot() );
    }

    @Override
    public abstract T getItem();

    public Channel getChannel() {
        return getTimetableSlot().getChannel();
    }

    @Override
    public TimetableSlot getTimetableSlot() {
        return timetableSlot;
    }

    public void setTimetableSlot(TimetableSlot timetableSlot) {
        this.timetableSlot = timetableSlot;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        NonPersistentNonQueueItem test = ProxiedClassUtil.cast(obj, NonPersistentNonQueueItem.class);
        return super.equals(test)
                && (getTimetableSlot() == test.getTimetableSlot() || (getTimetableSlot() != null && getTimetableSlot().equals(test.getTimetableSlot())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getTimetableSlot() ? 0 : getTimetableSlot().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

}
