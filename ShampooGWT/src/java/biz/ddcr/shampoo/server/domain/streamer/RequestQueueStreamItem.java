/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class RequestQueueStreamItem extends TrackQueueStreamItem implements RequestQueueStreamItemInterface {

    private Request request;

    protected RequestQueueStreamItem() {
        super();
    }

    public RequestQueueStreamItem(TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration, String queueId, Request request) {
        super(request.getChannel(), timetableSlot, startTime, duration, queueId);
        addRequest(request);
    }

    public RequestQueueStreamItem(RequestQueueStreamItemInterface o) {
        super( o );
        addRequest( o.getRequest() );
    }

    @Override
    public BroadcastableSong getTrack() {
        return getRequest().getSong();
    }

    @Override
    public Request getRequest() {
        return request;
    }

    @Override
    public void setRequest(Request request) {
        this.request = request;
    }

    @Override
    public void addRequest(Request request) {
        addRequest(request, true);
    }

    @Override
    public void addRequest(Request request, boolean doLink) {
        if (request != null) {
            if (doLink) {
                request.addStreamItem(this, false);
            }
            setRequest(request);
        }
    }

    @Override
    public void removeRequest() {
        removeRequest(true);
    }

    @Override
    public void removeRequest(boolean doLink) {
        if (getRequest() != null) {
            if (doLink) {
                getRequest().removeStreamItem(false);
            }
            //Put delete-orphan in the hibernate mapping
            setRequest(null);
        }
    }

    @Override
    public RequestQueueStreamItem shallowCopy() {
        return new RequestQueueStreamItem(this);
    }

    @Override
    public void acceptVisit(StreamItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
