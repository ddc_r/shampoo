package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.track.Request;

public interface RequestQueueStreamItemInterface extends TrackQueueStreamItemInterface {

    public Request getRequest();
    /**Do NOT use: Hibernate artefact**/
    public void setRequest(Request request);
    public void addRequest(Request request);
    public void addRequest(Request request, boolean doLink);
    public void removeRequest();
    public void removeRequest(boolean doLink);

}
