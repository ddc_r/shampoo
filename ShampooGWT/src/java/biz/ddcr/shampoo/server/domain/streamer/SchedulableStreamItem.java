/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * An entry in a Channel queue
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class SchedulableStreamItem extends StreamItem implements SchedulableStreamItemInterface {

    /** soft reference to the the orginal queueitem**/
    private String queueId;

    protected SchedulableStreamItem() {
    }

    public SchedulableStreamItem(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration, String queueId) {
        super(channel, startTime, duration);
        setQueueId(queueId);
    }

    public SchedulableStreamItem(SchedulableStreamItemInterface o) {
        super( o );
        setQueueId( o.getQueueId() );
    }

    @Override
    public String getQueueId() {
        return queueId;
    }

    @Override
    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }

}
