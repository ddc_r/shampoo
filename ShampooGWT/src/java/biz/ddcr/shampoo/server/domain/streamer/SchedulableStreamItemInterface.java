package biz.ddcr.shampoo.server.domain.streamer;

/**
 * An Archive created from a Queued Item
 *
 * @author okay_awright
 **/
public interface SchedulableStreamItemInterface extends StreamItemInterface {
   
    public String getQueueId();
    public void setQueueId(String queueId);
    
}
