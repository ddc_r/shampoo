/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SimpleTrackQueueStreamItem extends TrackQueueStreamItem implements SimpleTrackQueueStreamItemInterface {

    private BroadcastableTrack track;

    protected SimpleTrackQueueStreamItem() {
        super();
    }

    public SimpleTrackQueueStreamItem(Channel channel, TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration, String queueId, BroadcastableTrack track) {
        super(channel, timetableSlot, startTime, duration, queueId);
        addTrack(track);
    }

    public SimpleTrackQueueStreamItem(TrackQueueStreamItemInterface o) {
        super( o );
        addTrack( o.getTrack() );
    }

    @Override
    public BroadcastableTrack getTrack() {
        return track;
    }

    @Override
    public void setTrack(BroadcastableTrack track) {
        this.track = track;
    }

    @Override
    public void addTrack(BroadcastableTrack track) {
        addTrack(track, true);
    }

    @Override
    public void addTrack(BroadcastableTrack track, boolean doLink) {
        if (track != null) {
            if (doLink) {
                track.addStreamItem(this, false);
            }
            setTrack(track);
        }
    }

    @Override
    public void removeTrack() {
        removeTrack(true);
    }

    @Override
    public void removeTrack(boolean doLink) {
        if (getTrack() != null) {
            if (doLink) {
                getTrack().removeStreamItem(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setTrack(null);
        }
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new SimpleTrackQueueStreamItem(this);
    }

    @Override
    public void acceptVisit(StreamItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
