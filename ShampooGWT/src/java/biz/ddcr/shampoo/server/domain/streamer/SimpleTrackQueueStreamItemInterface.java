package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;

public interface SimpleTrackQueueStreamItemInterface extends TrackQueueStreamItemInterface {

    /**Do NOT use: Hibernate artefact**/
    public void setTrack(BroadcastableTrack track);
    public void addTrack(BroadcastableTrack track);
    public void addTrack(BroadcastableTrack track, boolean doLink);
    public void removeTrack();
    public void removeTrack(boolean doLink);

}
