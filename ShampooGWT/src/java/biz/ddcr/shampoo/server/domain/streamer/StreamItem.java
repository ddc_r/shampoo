/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import org.joda.time.DateTimeZone;

/**
 *
 * An entry in a Channel queue
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class StreamItem implements StreamItemInterface {

    //Timezone defaults to the JVM's own
    private final static String STREAM_TIMEZONE = DateTimeZone.getDefault().getID();

    /** Hibernate versioning */
    private int version;

    private String refID;

    private Channel channel;

    /** mark the indicative time of play for this item, it may be updated at any time
     * it should not be used for any other purpose than computing a next likely item for the queue
     * It should not be null
     */
    private YearMonthWeekDayHourMinuteSecondMillisecondInterface recordedStartTime;
    /** mark the indicative length of play for this item, it may be updated at any time
     * it should not be used for any other purpose than computing a next likely item for the queue
     * it is a millisecond-based variable
     */
    /** Do not use it: internal ORM placeholder **/
    private Long _recordedStartUnixTimestamp;
    
    private long likelyDuration;

    protected StreamItem() {
    }

    public StreamItem(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration) {
        addChannel(channel);
        setStartTime(startTime);
        setLikelyDuration(duration);
    }

    public StreamItem(StreamItemInterface o) {
        addChannel( o.getChannel() );
        setStartTimestamp( o.getStartTimestamp() );
        setLikelyDuration( o.getLikelyDuration() );
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    @Override
    public void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getStartTime() {
        if (recordedStartTime == null && _recordedStartUnixTimestamp != null) {
            recordedStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _recordedStartUnixTimestamp,
                    STREAM_TIMEZONE);
        }
        return recordedStartTime;
    }

    public void setStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface recordedStartTime) {
        this.recordedStartTime = recordedStartTime;
    }

    @Override
    public Long getStartTimestamp() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (recordedStartTime == null) {
            return _recordedStartUnixTimestamp;
        } else {
            return recordedStartTime.getUnixTime();
        }
    }

    @Override
    public void setStartTimestamp(Long unixTimestamp) {
        _recordedStartUnixTimestamp = unixTimestamp;
    }    
    
    @Override
    public long getLikelyDuration() {
        return likelyDuration;
    }
    @Override
    public long getDuration() {
        return getLikelyDuration();
    }

    @Override
    public void setLikelyDuration(long likelyDuration) {
        this.likelyDuration = likelyDuration;
    }

    @Override
    public Channel getChannel() {
        return channel;
    }
    @Override
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    @Override
    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            this.channel = channel;
            if (doLink) {
                channel.addStreamItem(this, false);
            }
        }
    }

    @Override
    public void removeChannel() {
        removeChannel(true);
    }

    @Override
    public void removeChannel(boolean doLink) {
        if (getChannel() != null) {
            if (doLink) {
                getChannel().removeStreamItem(false);
            }
            //Put delete-orphan in the hibernate mapping
            this.channel = null;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        StreamItem test = ProxiedClassUtil.cast(obj, StreamItem.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }
        return /*super.equals(test)
                &&*/ (getChannel() == test.getChannel() || (getChannel() != null && getChannel().equals(test.getChannel())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        //hash = 31 * hash + (null == getLocation() ? 0 : getLocation().hashCode());
        hash = 31 * hash + (null == getChannel() ? 0 : getChannel().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(StreamItemInterface anotherStreamItem) {
        //Sort by rank
        if (anotherStreamItem!=null)
            return (this.getChannel() == null ?
                0 :
                (anotherStreamItem.getChannel() == null ?
                    0 :
                    getChannel().compareTo(anotherStreamItem.getChannel()) ));
        else
            return 0;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setListenerRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }    
    
}
