package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;

/**
 * An item (queued track, blank or live, or unqueue) that is now played by a streamer
 *
 * @author okay_awright
 **/
public interface StreamItemInterface extends GenericPersistableItemInterface, Comparable<StreamItemInterface>, VisitorPatternInterface<StreamItemVisitor> {

    /** Do not use: Hibernate artefact **/
    public void setRefID(String refID);

    /** Do not use: Hibernate artefact **/
    public Long getStartTimestamp();
    /** Do not use: Hibernate artefact **/
    public void setStartTimestamp(Long unixTimestamp);

    public long getLikelyDuration();
    public void setLikelyDuration(long duration);

    /** Do not use: Hibernate artefact **/
    public void setChannel(Channel channel);
    public void addChannel(Channel channel);
    public void addChannel(Channel channel, boolean doLink);
    public void removeChannel();
    public void removeChannel(boolean doLink);

    public TimetableSlot getTimetableSlot();
    /** Do not use: Hibernate artefact **/
    public void setTimetableSlot(TimetableSlot timetableSlot);
    public void addTimetableSlot(TimetableSlot timetableSlot);
    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink);
    public void removeTimetableSlot();
    public void removeTimetableSlot(boolean doLink);

}
