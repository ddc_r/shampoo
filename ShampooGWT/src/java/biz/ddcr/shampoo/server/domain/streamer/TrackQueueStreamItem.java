/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class TrackQueueStreamItem extends SchedulableStreamItem implements TrackQueueStreamItemInterface {

    private TimetableSlot timetableSlot;

    protected TrackQueueStreamItem() {
        super();
    }

    protected TrackQueueStreamItem(Channel channel, TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration, String queueId) {
        super(channel, startTime, duration, queueId);
        addTimetableSlot(timetableSlot);
    }

    public TrackQueueStreamItem(TrackQueueStreamItemInterface o) {
        super( o );
        addTimetableSlot( o.getTimetableSlot() );
    }

    @Override
    public TimetableSlot getTimetableSlot() {
        return timetableSlot;
    }

    @Override
    public void setTimetableSlot(TimetableSlot timetableSlot) {
        this.timetableSlot = timetableSlot;
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot) {
        addTimetableSlot(timetableSlot, true);
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot, boolean doLink) {
        if (timetableSlot != null) {
            if (doLink) {
                timetableSlot.addStreamItem(this, false);
            }
            setTimetableSlot(timetableSlot);
        }
    }

    @Override
    public void removeTimetableSlot() {
        removeTimetableSlot(true);
    }

    @Override
    public void removeTimetableSlot(boolean doLink) {
        if (getTimetableSlot() != null) {
            if (doLink) {
                getTimetableSlot().removeStreamItem(false);
            }
            //Put delete-orphan in the hibernate mapping
            setTimetableSlot(null);
        }
    }

}
