package biz.ddcr.shampoo.server.domain.streamer;

import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;

public interface TrackQueueStreamItemInterface extends SchedulableStreamItemInterface {

    public BroadcastableTrack getTrack();

}
