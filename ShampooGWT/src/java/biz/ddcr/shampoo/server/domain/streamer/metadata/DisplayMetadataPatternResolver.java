/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.streamer.metadata;

import org.apache.commons.lang.text.StrSubstitutor;

/**
 *
 * Substitutes metadata variables within a string by values. Meant to be called by getFriendlyCaption() in StreamableItemMetadataInterface.
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class DisplayMetadataPatternResolver extends MetadataPatternResolver {

    /** enum that allows to compile at compile time the list of strings that are used within the switch/case of lookup() */
    private static enum MetadataEnum {
        playlistLabel,
        playlistDescription,
        playlistID,
        timetableSlotID,
        programmeLabel,
        channelLabel,
        pegiRatingAge,
        pegiRatingFeatures,
        authorizedPlayingDuration,
        scheduledStartTime,
        itemID,
        itemTitle,
        itemAuthor,
        itemAlbum,
        itemGenre,
        itemDateOfRelease,
        itemDescription,
        itemType,
        tag,
        itemPublisher,
        copyright,
        programmeRotationCount,
        requestAuthor,
        requestMessage,
        averageVote;
    }

    public DisplayMetadataPatternResolver() {
    }

    @Override
    protected String resolveVariable(String variable) {
        if (variable!=null && variable.length()!=0) {
        //Resolve the parameter
            switch (MetadataEnum.valueOf(variable)) {
                case playlistLabel: return getPlaylistLabel();
                case playlistDescription: return getPlaylistDescription();
                case playlistID: return getPlaylistID();
                case timetableSlotID: return getTimetableSlotID();
                case programmeLabel: return getProgrammeLabel();
                case channelLabel: return getChannelLabel();
                case pegiRatingAge: return getPEGIRatingAge();
                case pegiRatingFeatures: return getPEGIRatingFeatures();
                case authorizedPlayingDuration: return getAuthorizedPlayingDuration();
                case scheduledStartTime: return getScheduledStartTime();
                case itemID: return getItemID();
                case itemTitle: return getItemTitle();
                case itemAuthor: return getItemAuthor();
                case itemAlbum: return getItemAlbum();
                case itemGenre: return getItemGenre();
                case itemDateOfRelease: return getItemDateOfRelease();
                case itemDescription: return getItemDescription();
                case itemType: return getItemType();
                case tag: return getTag();
                case itemPublisher: return getItemPublisher();
                case copyright: return getCopyright();
                case programmeRotationCount: return getProgrammeRotationCount();
                case requestAuthor: return getRequestAuthor();
                case requestMessage: return getRequestMessage();
                case averageVote: return getAverageVote();
            }
        }
        return "";
    }

    public abstract String getPlaylistLabel();
    public abstract String getPlaylistDescription();
    public abstract String getPlaylistID();
    public abstract String getTimetableSlotID();
    public abstract String getProgrammeLabel();
    public abstract String getChannelLabel();
    public abstract String getPEGIRatingAge();
    public abstract String getPEGIRatingFeatures();
    public abstract String getAuthorizedPlayingDuration();
    public abstract String getScheduledStartTime();
    public abstract String getItemID();
    public abstract String getItemTitle();
    public abstract String getItemAuthor();
    public abstract String getItemAlbum();
    public abstract String getItemGenre();
    public abstract String getItemDateOfRelease();
    public abstract String getItemDescription();
    public abstract String getItemType();
    public abstract String getTag();
    public abstract String getItemPublisher();
    public abstract String getCopyright();
    public abstract String getProgrammeRotationCount();
    public abstract String getRequestAuthor();
    public abstract String getRequestMessage();
    public abstract String getAverageVote();

    /** Automated procedure that checks whether the provided pattern is valid **/
    public static void check(String testPattern) {
        if (testPattern != null && testPattern.length() != 0) {
            StrSubstitutor friendlycaptionMaker = new StrSubstitutor(new DisplayMetadataPatternResolver() {

                @Override
                public String getPlaylistLabel() {
                    return "test";
                }

                @Override
                public String getPlaylistDescription() {
                    return "test";
                }

                @Override
                public String getPlaylistID() {
                    return "test";
                }

                @Override
                public String getTimetableSlotID() {
                    return "test";
                }

                @Override
                public String getProgrammeLabel() {
                    return "test";
                }

                @Override
                public String getChannelLabel() {
                    return "test";
                }

                @Override
                public String getPEGIRatingAge() {
                    return "test";
                }

                @Override
                public String getPEGIRatingFeatures() {
                    return "test";
                }

                @Override
                public String getAuthorizedPlayingDuration() {
                    return "test";
                }

                @Override
                public String getScheduledStartTime() {
                    return "test";
                }

                @Override
                public String getItemID() {
                    return "test";
                }

                @Override
                public String getItemTitle() {
                    return "test";
                }

                @Override
                public String getItemAuthor() {
                    return "test";
                }

                @Override
                public String getItemAlbum() {
                    return "test";
                }

                @Override
                public String getItemGenre() {
                    return "test";
                }

                @Override
                public String getItemDateOfRelease() {
                    return "test";
                }

                @Override
                public String getItemDescription() {
                    return "test";
                }

                @Override
                public String getItemType() {
                    return "test";
                }

                @Override
                public String getTag() {
                    return "test";
                }

                @Override
                public String getItemPublisher() {
                    return "test";
                }

                @Override
                public String getCopyright() {
                    return "test";
                }

                @Override
                public String getProgrammeRotationCount() {
                    return "test";
                }

                @Override
                public String getRequestAuthor() {
                    return "test";
                }

                @Override
                public String getRequestMessage() {
                    return "test";
                }

                @Override
                public String getAverageVote() {
                    return "test";
                }
            });
            String result = friendlycaptionMaker.replace(testPattern);
            if (result == null || result.length() == 0) {
                throw new IllegalArgumentException("Malformed pattern "+testPattern);
            }
        }
    }
}
