/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.streamer.metadata;

import java.io.Serializable;
import org.apache.commons.lang.text.StrLookup;

/**
 *
 * Substitutes metadata variables within a string by values.
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class MetadataPatternResolver extends StrLookup implements Serializable {

    public MetadataPatternResolver() {
    }

    private String output(String prefix, String value, String suffix) {
        return (value == null || value.length() == 0) ? "" : prefix + value + suffix;
    }

    protected abstract String resolveVariable(String variable);

    @Override
    public String lookup(String variable) {
        //The string may contain an optional prefix and suffix
        //Those prefix and suffix will only be displayed iff the variable can be resolved
        //If you wish to use an optional prefix or suffix, variable must follow this pattern PREFIX:VARIABLE:SUFFIX
        //$ { } become illegal characters for inclusion in variable
        if (variable != null && variable.length() != 0) {

            String prefix = "";
            String suffix = "";
            String parameter = "";
            String[] parts = variable.split("\\$");
            switch (parts.length) {
                case 1:
                    //No prefix and suffix
                    parameter = variable;
                    break;
                case 2:
                    //Suffix is missing
                    prefix = parts[0];
                    parameter = parts[1];
                    break;
                case 3:
                    //Prefix + suffix present
                    prefix = parts[0];
                    parameter = parts[1];
                    suffix = parts[2];
                    break;
                default:
                    throw new IllegalArgumentException("The number of parts in the variable "+variable+" is incorrect");
            }

            return output(prefix, resolveVariable(parameter), suffix);
        }
        return "";
    }

}
