/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.DurationMilliseconds;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.text.StrSubstitutor;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class MinimalSimpleStreamableBlankQueueItemMetadata extends SimpleStreamableChannelGenericMetadata implements Serializable, Copiable, MinimalStreamableItemMetadataInterface, StreamableChannelMetadataInterface {

    protected static final String ITEM_TYPE = "blank";
    
    private Float authorizedPlayingDuration;
    private Long scheduledStartTime;    
    /**
     * associated timezone
     */
    private String timezone;

    private String friendlyCaptionPattern;

    private class SimpleStreamableDisplayMetadataResolver extends DisplayMetadataPatternResolver {

        @Override
        public String getPlaylistLabel() {
            return MinimalSimpleStreamableBlankQueueItemMetadata.this.getPlaylistLabel();
        }
        @Override
        public String getPlaylistDescription() {
            return MinimalSimpleStreamableBlankQueueItemMetadata.this.getPlaylistDescription();
        }
        @Override
        public String getPlaylistID() {
            return MinimalSimpleStreamableBlankQueueItemMetadata.this.getPlaylistID();
        }
        @Override
        public String getTimetableSlotID() {
            return null;
        }
        @Override
        public String getProgrammeLabel() {
            return MinimalSimpleStreamableBlankQueueItemMetadata.this.getProgrammeLabel();
        }
        @Override
        public String getChannelLabel() {
            return MinimalSimpleStreamableBlankQueueItemMetadata.this.getChannelLabel();
        }
        @Override
        public String getPEGIRatingAge() {
            return null;
        }
        @Override
        public String getPEGIRatingFeatures() {
            return null;
        }
        @Override
        public String getAuthorizedPlayingDuration() {
            Float f = MinimalSimpleStreamableBlankQueueItemMetadata.this.getAuthorizedPlayingDuration();
            return f!=null?f.toString():null;
        }
        @Override
        public String getScheduledStartTime() {
            Long l = MinimalSimpleStreamableBlankQueueItemMetadata.this.getScheduledStartTime();
            return l!=null?(new YearMonthWeekDayHourMinuteSecondMillisecond(l, getTimezone())).getFriendlyEnglishCaption():null;
        }
        @Override
        public String getItemID() {
            return null;
        }
        @Override
        public String getItemTitle() {
            return "Off Air";
        }
        @Override
        public String getItemAuthor() {
            return null;
        }
        @Override
        public String getItemAlbum() {
            return null;
        }
        @Override
        public String getItemGenre() {
            return null;
        }
        @Override
        public String getItemDateOfRelease() {
            return null;
        }
        @Override
        public String getItemDescription() {
            return null;
        }
        @Override
        public String getItemType() {
            return MinimalSimpleStreamableBlankQueueItemMetadata.this.getType();
        }
        @Override
        public String getTag() {
            return null;
        }
        @Override
        public String getItemPublisher() {
            return null;
        }
        @Override
        public String getCopyright() {
            return null;
        }
        @Override
        public String getProgrammeRotationCount() {
            return null;
        }
        @Override
        public String getRequestAuthor() {
            return null;
        }
        @Override
        public String getRequestMessage() {
            return null;
        }
        @Override
        public String getAverageVote() {
            return null;
        }
    }

    private SimpleStreamableDisplayMetadataResolver displayPatternResolver = new SimpleStreamableDisplayMetadataResolver();

    public MinimalSimpleStreamableBlankQueueItemMetadata() {
    }

    public MinimalSimpleStreamableBlankQueueItemMetadata(MinimalSimpleStreamableBlankQueueItemMetadata o) {
        super( o );
        setAuthorizedPlayingDuration( o.getAuthorizedPlayingDuration() );
        setScheduledStartTime( o.getScheduledStartTime() );        
        setChannelLabel( o.getChannelLabel() );
        setFriendlyCaptionPattern( o.getFriendlyCaptionPattern() );
        setTimezone( o.getTimezone() );
    }

    protected String getFriendlyCaptionPattern() {
        return friendlyCaptionPattern;
    }

    @Override
    public void setFriendlyCaptionPattern(String pattern) {
        this.friendlyCaptionPattern = pattern;
    }

    @Override
    public String getFriendlyCaption() {
        StrSubstitutor friendlycaptionMaker = new StrSubstitutor(displayPatternResolver);
        return friendlycaptionMaker.replace(getFriendlyCaptionPattern());
    }

    @Override
    public String getType() {
        return ITEM_TYPE;
    }
       
    public Float getAuthorizedPlayingDuration() {
        return authorizedPlayingDuration;
    }

    public void setAuthorizedPlayingDuration(Float authorizedPlayingDuration) {
        this.authorizedPlayingDuration = authorizedPlayingDuration;
    }

    public Long getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(Long scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }
    
    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
    
    @Override
    public String getPlaylistLabel() {
        return null;
    }

    @Override
    public String getPlaylistDescription() {
        return null;
    }

    @Override
    public String getPlaylistID() {
        return null;
    }

    @Override
    public String getProgrammeLabel() {
        return null;
    }

    @Override
    public Byte getPEGIRatingAge() {
        return null;
    }

    @Override
    public String[] getPEGIRatingFeatures() {
        return null;
    }

    @Override
    public boolean hasPicture() {
        return false;
    }

    @Override
    public String getPictureFormat() {
        return null;
    }

    @Override
    public Long getPictureSize() {
        return null;
    }

    @Override
    public String getPictureURLEndpoint() {
        return null;
    }
   
    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MinimalSimpleStreamableBlankQueueItemMetadata other = (MinimalSimpleStreamableBlankQueueItemMetadata) obj;
        if ((this.getChannelLabel() == null) ? (other.getChannelLabel() != null) : !this.getChannelLabel().equals(other.getChannelLabel())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.getChannelLabel() != null ? this.getChannelLabel().hashCode() : 0);
        return hash;
    }

    @Override
    public int fullHashCode() {
        int hash = 7;
        hash = 59 * hash + super.fullHashCode();
        hash = 59 * hash + (this.authorizedPlayingDuration != null ? this.authorizedPlayingDuration.hashCode() : 0);
        hash = 59 * hash + (this.scheduledStartTime != null ? this.scheduledStartTime.hashCode() : 0);
        hash = 59 * hash + (this.timezone != null ? this.timezone.hashCode() : 0);
        hash = 59 * hash + (this.friendlyCaptionPattern != null ? this.friendlyCaptionPattern.hashCode() : 0);
        return hash;
    }

    @Override
    public MinimalSimpleStreamableBlankQueueItemMetadata shallowCopy() {
        return new MinimalSimpleStreamableBlankQueueItemMetadata(this);
    }

    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getType());
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());
                if (userFriendlyUnitValues) {
                    s.add(getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(getAuthorizedPlayingDuration() != null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    s.add(getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                }
                s.add(getFriendlyCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription",getChannelDescription());
                m.put("channelURL",getChannelURL());
                m.put("channelTag",getChannelTag());
                if (userFriendlyUnitValues) {
                    m.put("scheduledStartTime", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("authorizedPlayingDuration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                } else {
                    m.put("scheduledStartTime", getScheduledStartTime());
                    m.put("authorizedPlayingDuration", getAuthorizedPlayingDuration());
                }
                m.put("friendlyCaption", getFriendlyCaption());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel-label", getChannelLabel());
                m.put("channel-description",getChannelDescription());
                m.put("channel-url",getChannelURL());
                m.put("channel-tag",getChannelTag());
                if (userFriendlyUnitValues) {
                    m.put("scheduled-start-time", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("authorized-playing-duration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                } else {
                    m.put("scheduled-start-time", getScheduledStartTime());
                    m.put("authorized-playing-duration", getAuthorizedPlayingDuration());
                }
                m.put("friendly-caption", getFriendlyCaption());
                return MarshallUtil.toSimpleXML("minimal-queue-item",m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());
                m.put("song", getFriendlyCaption());
                m.put("liq_queue_start_time", getScheduledStartTime());
                m.put("liq_cue_in", new Float(0.0));
                m.put("liq_cue_out", getAuthorizedPlayingDuration());                
                return MarshallUtil.toSimpleAnnotate(m, null);
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());
                m.put("song", getFriendlyCaption());                
                m.put("liq_queue_start_time", getScheduledStartTime()!=null?getScheduledStartTime().toString():null);
                m.put("liq_cue_in", (new Float(0.0)).toString());
                m.put("liq_cue_out", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);                
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }
    
}
