/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.SizeBytes;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.text.StrSubstitutor;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class MinimalSimpleStreamableLiveGenericItemMetadata extends SimpleStreamableChannelGenericMetadata implements Serializable, Copiable, MinimalStreamableLiveMetadataInterface {

    protected static final String ITEM_TYPE = "live";
    
    private String liveBroadcaster;

    private String liveTag;
    private String livePublisher;
    private String liveCopyright;
    private String liveGenre;
    
    private String timetableSlotID;

    private Byte pegiRatingAge;
    private String[] pegiRatingFeatures;

    private String playlistID;
    private String playlistLabel;
    private String playlistDescription;
    private String programmeLabel;
    private boolean picture;
    private String pictureFormat;
    private Long pictureSize;
    private String pictureURLEndpoint;
    
    private Long startTime;
    /** associated timezone*/
    private String timezone;
    private Long endTime;

    private String friendlyCaptionPattern;

    private class SimpleStreamableMetadataResolver extends DisplayMetadataPatternResolver {

        @Override
        public String getPlaylistLabel() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getPlaylistLabel();
        }
        @Override
        public String getPlaylistDescription() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getPlaylistDescription();
        }
        @Override
        public String getPlaylistID() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getPlaylistID();
        }
        @Override
        public String getTimetableSlotID() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getTimetableSlotID();
        }
        @Override
        public String getProgrammeLabel() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getProgrammeLabel();
        }
        @Override
        public String getChannelLabel() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getChannelLabel();
        }
        @Override
        public String getPEGIRatingAge() {
            Byte b = MinimalSimpleStreamableLiveGenericItemMetadata.this.getPEGIRatingAge();
            return b!=null?b.toString():null;
        }
        @Override
        public String getPEGIRatingFeatures() {
            String[] s = MinimalSimpleStreamableLiveGenericItemMetadata.this.getPEGIRatingFeatures();
            return s!=null?MarshallUtil.toSimpleCSV(s).toString():null;
        }
        @Override
        public String getAuthorizedPlayingDuration() {
            Long l2 = MinimalSimpleStreamableLiveGenericItemMetadata.this.getEndTime();
            Long l1 = MinimalSimpleStreamableLiveGenericItemMetadata.this.getStartTime();
            Float f = (l2!=null && l1!=null)?new Float((l2-l1)/1000.0):null;
            return f!=null?f.toString():null;
        }
        @Override
        public String getScheduledStartTime() {
            Long l = MinimalSimpleStreamableLiveGenericItemMetadata.this.getStartTime();
            return l!=null?(new YearMonthWeekDayHourMinuteSecondMillisecond(l, getTimezone())).getFriendlyEnglishCaption():null;
        }
        @Override
        public String getItemID() {
            return null;
        }
        @Override
        public String getItemTitle() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getPlaylistLabel();
        }
        @Override
        public String getItemAuthor() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getLiveBroadcaster();
        }
        @Override
        public String getItemAlbum() {
            return null;
        }
        @Override
        public String getItemGenre() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getLiveGenre();
        }
        @Override
        public String getItemDateOfRelease() {
            return null;
        }
        @Override
        public String getItemDescription() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getPlaylistDescription();
        }
        @Override
        public String getItemType() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getType();
        }
        @Override
        public String getTag() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getLiveTag();
        }
        @Override
        public String getItemPublisher() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getLivePublisher();
        }
        @Override
        public String getCopyright() {
            return MinimalSimpleStreamableLiveGenericItemMetadata.this.getLiveCopyright();
        }
        @Override
        public String getProgrammeRotationCount() {
            return null;
        }
        @Override
        public String getRequestAuthor() {
            return null;
        }
        @Override
        public String getRequestMessage() {
            return null;
        }
        @Override
        public String getAverageVote() {
            return null;
        }
    }

    private SimpleStreamableMetadataResolver patternResolver = new SimpleStreamableMetadataResolver();

    public MinimalSimpleStreamableLiveGenericItemMetadata() {
    }

    public MinimalSimpleStreamableLiveGenericItemMetadata(MinimalStreamableLiveMetadataInterface o) {
        super( o );
        setLiveBroadcaster( o.getLiveBroadcaster() );
        setPlaylistID( o.getPlaylistID() );
        setPlaylistLabel( o.getPlaylistLabel() );
        setPlaylistDescription( o.getPlaylistDescription() );
        setProgrammeLabel( o.getProgrammeLabel() );
        setChannelLabel( o.getChannelLabel() );
        setPEGIRatingAge( o.getPEGIRatingAge() );
        setPEGIRatingFeatures( o.getPEGIRatingFeatures() );
        setPicture( o.hasPicture() );
        setPictureFormat( o.getPictureFormat() );
        setPictureSize( o.getPictureSize() );
        setPictureURLEndpoint( o.getPictureURLEndpoint() );
        setStartTime( o.getStartTime() );
        setEndTime( o.getEndTime() );
        setTimetableSlotID( o.getTimetableSlotID() );
        setLiveGenre( o.getLiveGenre() );
        setLiveTag( o.getLiveTag() );
        setLivePublisher( o.getLivePublisher() );
        setLiveCopyright( o.getLiveCopyright() );
        setTimezone( o.getTimezone() );
    }

    @Override
    public String getFriendlyCaption() {
        StrSubstitutor friendlycaptionMaker = new StrSubstitutor(patternResolver);
        return friendlycaptionMaker.replace(friendlyCaptionPattern);
    }

    @Override
    public void setFriendlyCaptionPattern(String pattern) {
        this.friendlyCaptionPattern = pattern;
    }

    @Override
    public String getType() {
        return ITEM_TYPE;
    }
    
    @Override
    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Override
    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setLiveBroadcaster(String liveBroadcaster) {
        this.liveBroadcaster = liveBroadcaster;
    }

    @Override
    public String getLiveBroadcaster() {
        return liveBroadcaster;
    }

    @Override
    public Byte getPEGIRatingAge() {
        return pegiRatingAge;
    }

    public void setPEGIRatingAge(Byte pegiRatingAge) {
        this.pegiRatingAge = pegiRatingAge;
    }

    @Override
    public String[] getPEGIRatingFeatures() {
        //TODO: check whether clone() fullfills its job
        return pegiRatingFeatures!=null ? pegiRatingFeatures.clone() : null;
    }

    public void setPEGIRatingFeatures(String[] pegiRatingFeatures) {
        this.pegiRatingFeatures = pegiRatingFeatures;
    }

    @Override
    public String getLivePublisher() {
        return livePublisher;
    }

    public void setLivePublisher(String livePublisher) {
        this.livePublisher = livePublisher;
    }

    @Override
    public String getLiveCopyright() {
        return liveCopyright;
    }

    public void setLiveCopyright(String copyright) {
        liveCopyright = copyright;
    }

    @Override
    public String getLiveTag() {
        return liveTag;
    }

    public void setLiveTag(String tag) {
        liveTag = tag;
    }

    @Override
    public String getLiveGenre() {
        return liveGenre;
    }

    public void setLiveGenre(String genre) {
        liveGenre = genre;
    }

    @Override
    public boolean hasPicture() {
        return picture;
    }

    public void setPicture(boolean picture) {
        this.picture = picture;
    }

    @Override
    public String getPictureFormat() {
        return pictureFormat;
    }

    public void setPictureFormat(String pictureFormat) {
        this.pictureFormat = pictureFormat;
    }

    @Override
    public Long getPictureSize() {
        return pictureSize;
    }

    public void setPictureSize(Long pictureSize) {
        this.pictureSize = pictureSize;
    }

    @Override
    public String getPictureURLEndpoint() {
        return pictureURLEndpoint;
    }

    public void setPictureURLEndpoint(String pictureURLEndpoint) {
        this.pictureURLEndpoint = pictureURLEndpoint;
    }

    @Override
    public String getPlaylistDescription() {
        return playlistDescription;
    }

    public void setPlaylistDescription(String playlistDescription) {
        this.playlistDescription = playlistDescription;
    }

    @Override
    public String getPlaylistID() {
        return playlistID;
    }

    public void setPlaylistID(String playlistID) {
        this.playlistID = playlistID;
    }

    @Override
    public String getPlaylistLabel() {
        return playlistLabel;
    }

    public void setPlaylistLabel(String playlistLabel) {
        this.playlistLabel = playlistLabel;
    }

    @Override
    public String getProgrammeLabel() {
        return programmeLabel;
    }

    public void setProgrammeLabel(String programmeLabel) {
        this.programmeLabel = programmeLabel;
    }

    @Override
    public String getTimetableSlotID() {
        return timetableSlotID;
    }

    public void setTimetableSlotID(String timetableSlotID) {
        this.timetableSlotID = timetableSlotID;
    }

    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MinimalSimpleStreamableLiveGenericItemMetadata other = (MinimalSimpleStreamableLiveGenericItemMetadata) obj;
        if ((this.liveBroadcaster == null) ? (other.liveBroadcaster != null) : !this.liveBroadcaster.equals(other.liveBroadcaster)) {
            return false;
        }
        if ((this.playlistID == null) ? (other.playlistID != null) : !this.playlistID.equals(other.playlistID)) {
            return false;
        }
        if ((this.timetableSlotID == null) ? (other.timetableSlotID != null) : !this.timetableSlotID.equals(other.timetableSlotID)) {
            return false;
        }
        if ((this.programmeLabel == null) ? (other.programmeLabel != null) : !this.programmeLabel.equals(other.programmeLabel)) {
            return false;
        }
        if ((this.getChannelLabel() == null) ? (other.getChannelLabel() != null) : !this.getChannelLabel().equals(other.getChannelLabel())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.liveBroadcaster != null ? this.liveBroadcaster.hashCode() : 0);
        hash = 59 * hash + (this.playlistID != null ? this.playlistID.hashCode() : 0);
        hash = 59 * hash + (this.timetableSlotID != null ? this.timetableSlotID.hashCode() : 0);
        hash = 59 * hash + (this.programmeLabel != null ? this.programmeLabel.hashCode() : 0);
        hash = 59 * hash + (this.getChannelLabel() != null ? this.getChannelLabel().hashCode() : 0);
        return hash;
    }

    @Override
    public int fullHashCode() {
        int hash = 7;
        hash = 37 * hash + super.fullHashCode();
        hash = 37 * hash + (this.liveBroadcaster != null ? this.liveBroadcaster.hashCode() : 0);
        hash = 37 * hash + (this.liveTag != null ? this.liveTag.hashCode() : 0);
        hash = 37 * hash + (this.livePublisher != null ? this.livePublisher.hashCode() : 0);
        hash = 37 * hash + (this.liveCopyright != null ? this.liveCopyright.hashCode() : 0);
        hash = 37 * hash + (this.liveGenre != null ? this.liveGenre.hashCode() : 0);
        hash = 37 * hash + (this.timetableSlotID != null ? this.timetableSlotID.hashCode() : 0);
        hash = 37 * hash + (this.pegiRatingAge != null ? this.pegiRatingAge.hashCode() : 0);
        hash = 37 * hash + Arrays.deepHashCode(this.pegiRatingFeatures);
        hash = 37 * hash + (this.playlistID != null ? this.playlistID.hashCode() : 0);
        hash = 37 * hash + (this.playlistLabel != null ? this.playlistLabel.hashCode() : 0);
        hash = 37 * hash + (this.playlistDescription != null ? this.playlistDescription.hashCode() : 0);
        hash = 37 * hash + (this.programmeLabel != null ? this.programmeLabel.hashCode() : 0);
        hash = 37 * hash + (this.picture ? 1 : 0);
        hash = 37 * hash + (this.pictureFormat != null ? this.pictureFormat.hashCode() : 0);
        hash = 37 * hash + (this.pictureSize != null ? this.pictureSize.hashCode() : 0);
        hash = 37 * hash + (this.pictureURLEndpoint != null ? this.pictureURLEndpoint.hashCode() : 0);
        hash = 37 * hash + (this.startTime != null ? this.startTime.hashCode() : 0);
        hash = 37 * hash + (this.timezone != null ? this.timezone.hashCode() : 0);
        hash = 37 * hash + (this.endTime != null ? this.endTime.hashCode() : 0);
        hash = 37 * hash + (this.friendlyCaptionPattern != null ? this.friendlyCaptionPattern.hashCode() : 0);
        return hash;
    }
    
    @Override
    public MinimalSimpleStreamableLiveGenericItemMetadata shallowCopy() {
        return new MinimalSimpleStreamableLiveGenericItemMetadata(this);
    }

    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getType());
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());
                s.add(getLiveBroadcaster());
                s.add(getLiveTag());
                s.add(getLivePublisher());
                s.add(getLiveCopyright());
                s.add(getLiveGenre());
                s.add(getTimetableSlotID());
                s.add(getPEGIRatingAge()!=null?getPEGIRatingAge().toString():null);
                s.add(getPEGIRatingFeatures()!=null?MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString():null);
                s.add(getPlaylistID());
                s.add(getPlaylistLabel());
                s.add(getPlaylistDescription());
                s.add(getProgrammeLabel());
                s.add(getPictureFormat());
                if (userFriendlyUnitValues) {
                    s.add(getPictureSize() != null ? new SizeBytes(getPictureSize()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(Long.toString(getPictureSize()));
                }
                s.add(getPictureURLEndpoint());
                if (userFriendlyUnitValues) {
                    s.add(getStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(getEndTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getEndTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getStartTime() != null ? getStartTime().toString() : null);
                    s.add(getEndTime() != null ? getEndTime().toString() : null);
                }
                s.add(getFriendlyCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription",getChannelDescription());
                m.put("channelURL",getChannelURL());
                m.put("channelTag",getChannelTag());
                m.put("timetableSlotID",getTimetableSlotID());                
                m.put("playlistID",getPlaylistID());
                m.put("playlistLabel",getPlaylistLabel());
                m.put("playlistDescription",getPlaylistDescription());
                m.put("programmeLabel",getProgrammeLabel());                
                m.put("liveBroadcaster",getLiveBroadcaster());
                m.put("liveTag",getLiveTag());
                m.put("livePublisher",getLivePublisher());
                m.put("liveCopyright",getLiveCopyright());
                m.put("liveGenre",getLiveGenre());
                m.put("pegiRatingAge",getPEGIRatingAge());
                m.put("pegiRatingFeatures",getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("pictureFormat",getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("pictureSize",getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("pictureSize",getPictureSize());
                    }
                    m.put("pictureURLEndpoint",getPictureURLEndpoint());
                }
                if (userFriendlyUnitValues) {
                    m.put("startTime",getStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getStartTime().longValue(), getTimezone()) : null);
                    m.put("endTime",getEndTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getEndTime().longValue(), getTimezone()) : null);
                } else {
                    m.put("startTime",getStartTime() != null ? getStartTime().toString() : null);
                    m.put("endTime",getEndTime() != null ? getEndTime().toString() : null);
                }
                m.put("friendlyCaption", getFriendlyCaption());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel-label", getChannelLabel());
                m.put("channel-description",getChannelDescription());
                m.put("channel-url",getChannelURL());
                m.put("channel-tag",getChannelTag());
                m.put("timetable-slot-id",getTimetableSlotID());
                m.put("playlist-id",getPlaylistID());
                m.put("playlist-label",getPlaylistLabel());
                m.put("playlist-description",getPlaylistDescription());
                m.put("programme-label",getProgrammeLabel());                
                m.put("live-broadcaster",getLiveBroadcaster());
                m.put("live-tag",getLiveTag());
                m.put("live-publisher",getLivePublisher());
                m.put("live-copyright",getLiveCopyright());
                m.put("live-genre",getLiveGenre());                
                m.put("pegi-rating-age",getPEGIRatingAge());
                m.put("pegi-rating-features",getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("picture-format",getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("picture-size",getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("picture-size",getPictureSize());
                    }
                    m.put("picture-url-endpoint",getPictureURLEndpoint());
                }
                if (userFriendlyUnitValues) {
                    m.put("start-time",getStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getStartTime().longValue(), getTimezone()) : null);
                    m.put("end-time",getEndTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getEndTime().longValue(), getTimezone()) : null);
                } else {
                    m.put("start-time",getStartTime() != null ? getStartTime().toString() : null);
                    m.put("end-time",getEndTime() != null ? getEndTime().toString() : null);
                }
                m.put("friendly-caption", getFriendlyCaption());
                return MarshallUtil.toSimpleXML("minimal-item",m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());    
                m.put("timetable_slot_id",getTimetableSlotID());
                m.put("programme_id",getProgrammeLabel());
                m.put("playlist_id",getPlaylistID());
                m.put("playlist_label",getPlaylistLabel());
                m.put("playlist_description",getPlaylistDescription());
                m.put("live_broadcaster",getLiveBroadcaster());
                m.put("live_tag",getLiveTag());
                m.put("live_publisher",getLivePublisher());
                m.put("live_copyright",getLiveCopyright());
                m.put("live_genre",getLiveGenre());
                m.put("pegi_rating_age",getPEGIRatingAge());
                m.put("pegi_rating_features",getPEGIRatingFeatures());                
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format",getPictureFormat());
                    m.put("picture_size",getPictureSize());
                    m.put("picture_url_endpoint",getPictureURLEndpoint());
                }
                m.put("liq_queue_start_time", getStartTime());
                m.put("liq_queue_end_time", getEndTime());
                return MarshallUtil.toSimpleAnnotate(m, null);
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());    
                m.put("timetable_slot_id",getTimetableSlotID());
                m.put("programme_id",getProgrammeLabel());
                m.put("playlist_id",getPlaylistID());
                m.put("playlist_label",getPlaylistLabel());
                m.put("playlist_description",getPlaylistDescription());
                m.put("live_broadcaster",getLiveBroadcaster());
                m.put("live_tag",getLiveTag());
                m.put("live_publisher",getLivePublisher());
                m.put("live_copyright",getLiveCopyright());
                m.put("live_genre",getLiveGenre());
                m.put("pegi_rating_age",getPEGIRatingAge()!=null ? getPEGIRatingAge().toString() : null);
                m.put("pegi_rating_features",getPEGIRatingFeatures()!=null ? MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString() : null);                
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format",getPictureFormat());
                    m.put("picture_size",getPictureSize()!=null ? getPictureSize().toString() : null);
                    m.put("picture_url_endpoint",getPictureURLEndpoint());
                }
                m.put("liq_queue_start_time", getStartTime()!=null ? getStartTime().toString() : null);
                m.put("liq_queue_end_time", getEndTime()!=null ? getEndTime().toString() : null);
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }
    
}
