/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.helper.DurationMilliseconds;
import biz.ddcr.shampoo.server.helper.SizeBytes;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class MinimalSimpleStreamableLiveQueueItemMetadata extends MinimalSimpleStreamableLiveGenericItemMetadata implements MinimalStreamableTimedItemMetadataInterface {

    private Float authorizedPlayingDuration;
    private Long scheduledStartTime;

    public MinimalSimpleStreamableLiveQueueItemMetadata() {
    }

    public MinimalSimpleStreamableLiveQueueItemMetadata(StreamableLiveMetadataInterface o) {
        super(o);
    }

    public MinimalSimpleStreamableLiveQueueItemMetadata(MinimalSimpleStreamableLiveQueueItemMetadata o) {
        super(o);
        setAuthorizedPlayingDuration( o.getAuthorizedPlayingDuration() );
        setScheduledStartTime( o.getScheduledStartTime() );
    }
   
    @Override
    public Float getAuthorizedPlayingDuration() {
        return authorizedPlayingDuration;
    }

    public void setAuthorizedPlayingDuration(Float authorizedPlayingDuration) {
        this.authorizedPlayingDuration = authorizedPlayingDuration;
    }

    @Override
    public Long getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(Long scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public MinimalSimpleStreamableLiveQueueItemMetadata shallowCopy() {
        return new MinimalSimpleStreamableLiveQueueItemMetadata(this);
    }

    @Override
    public int fullHashCode() {
        int hash = 7;
        hash = 73 * hash + super.fullHashCode();
        hash = 73 * hash + (this.authorizedPlayingDuration != null ? this.authorizedPlayingDuration.hashCode() : 0);
        hash = 73 * hash + (this.scheduledStartTime != null ? this.scheduledStartTime.hashCode() : 0);
        return hash;
    }

    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getType());
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());
                s.add(getLiveBroadcaster());
                s.add(getLiveTag());
                s.add(getLivePublisher());
                s.add(getLiveCopyright());
                s.add(getLiveGenre());
                s.add(getTimetableSlotID());
                s.add(getPEGIRatingAge()!=null?getPEGIRatingAge().toString():null);
                s.add(getPEGIRatingFeatures()!=null?MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString():null);
                s.add(getPlaylistID());
                s.add(getPlaylistLabel());
                s.add(getPlaylistDescription());
                s.add(getProgrammeLabel());
                s.add(getPictureFormat());
                if (userFriendlyUnitValues) {
                    s.add(getPictureSize() != null ? new SizeBytes(getPictureSize()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(Long.toString(getPictureSize()));
                }
                s.add(getPictureURLEndpoint());
                
                if (userFriendlyUnitValues) {
                    s.add(getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    s.add(getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                }
                
                s.add(getFriendlyCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription",getChannelDescription());
                m.put("channelURL",getChannelURL());
                m.put("channelTag",getChannelTag());
                m.put("timetableSlotID",getTimetableSlotID());                
                m.put("playlistID",getPlaylistID());
                m.put("playlistLabel",getPlaylistLabel());
                m.put("playlistDescription",getPlaylistDescription());
                m.put("programmeLabel",getProgrammeLabel());                
                m.put("liveBroadcaster",getLiveBroadcaster());
                m.put("liveTag",getLiveTag());
                m.put("livePublisher",getLivePublisher());
                m.put("liveCopyright",getLiveCopyright());
                m.put("liveGenre",getLiveGenre());
                m.put("pegiRatingAge",getPEGIRatingAge());
                m.put("pegiRatingFeatures",getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("pictureFormat",getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("pictureSize",getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("pictureSize",getPictureSize());
                    }
                    m.put("pictureURLEndpoint",getPictureURLEndpoint());
                }
                
                if (userFriendlyUnitValues) {
                    m.put("startTime", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("duration", getAuthorizedPlayingDuration() != null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                } else {
                    m.put("startTime", getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    m.put("duration", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                }
                
                m.put("friendlyCaption", getFriendlyCaption());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel-label", getChannelLabel());
                m.put("channel-description",getChannelDescription());
                m.put("channel-url",getChannelURL());
                m.put("channel-tag",getChannelTag());
                m.put("timetable-slot-id",getTimetableSlotID());
                m.put("playlist-id",getPlaylistID());
                m.put("playlist-label",getPlaylistLabel());
                m.put("playlist-description",getPlaylistDescription());
                m.put("programme-label",getProgrammeLabel());                
                m.put("live-broadcaster",getLiveBroadcaster());
                m.put("live-tag",getLiveTag());
                m.put("live-publisher",getLivePublisher());
                m.put("live-copyright",getLiveCopyright());
                m.put("live-genre",getLiveGenre());                
                m.put("pegi-rating-age",getPEGIRatingAge());
                m.put("pegi-rating-features",getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("picture-format",getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("picture-size",getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("picture-size",getPictureSize());
                    }
                    m.put("picture-url-endpoint",getPictureURLEndpoint());
                }
                
                if (userFriendlyUnitValues) {
                    m.put("start-time", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("duration", getAuthorizedPlayingDuration() != null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                } else {
                    m.put("start-time", getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    m.put("duration", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                }
                
                m.put("friendly-caption", getFriendlyCaption());
                return MarshallUtil.toSimpleXML("minimal-queue-item",m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());    
                m.put("timetable_slot_id",getTimetableSlotID());
                m.put("programme_id",getProgrammeLabel());
                m.put("playlist_id",getPlaylistID());
                m.put("playlist_label",getPlaylistLabel());
                m.put("playlist_description",getPlaylistDescription());
                m.put("live_broadcaster",getLiveBroadcaster());
                m.put("live_tag",getLiveTag());
                m.put("live_publisher",getLivePublisher());
                m.put("live_copyright",getLiveCopyright());
                m.put("live_genre",getLiveGenre());
                m.put("pegi_rating_age",getPEGIRatingAge());
                m.put("pegi_rating_features",getPEGIRatingFeatures());                
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format",getPictureFormat());
                    m.put("picture_size",getPictureSize());
                    m.put("picture_url_endpoint",getPictureURLEndpoint());
                }
                
                m.put("liq_queue_start_time", getScheduledStartTime());
                m.put("liq_cue_in", new Float(0.0));
                m.put("liq_cue_out", getAuthorizedPlayingDuration());
                
                return MarshallUtil.toSimpleAnnotate(m, null);
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());    
                m.put("timetable_slot_id",getTimetableSlotID());
                m.put("programme_id",getProgrammeLabel());
                m.put("playlist_id",getPlaylistID());
                m.put("playlist_label",getPlaylistLabel());
                m.put("playlist_description",getPlaylistDescription());
                m.put("live_broadcaster",getLiveBroadcaster());
                m.put("live_tag",getLiveTag());
                m.put("live_publisher",getLivePublisher());
                m.put("live_copyright",getLiveCopyright());
                m.put("live_genre",getLiveGenre());
                m.put("pegi_rating_age",getPEGIRatingAge()!=null ? getPEGIRatingAge().toString() : null);
                m.put("pegi_rating_features",getPEGIRatingFeatures()!=null ? MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString() : null);                
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format",getPictureFormat());
                    m.put("picture_size",getPictureSize()!=null ? getPictureSize().toString() : null);
                    m.put("picture_url_endpoint",getPictureURLEndpoint());
                }
                
                m.put("liq_queue_start_time", getScheduledStartTime()!=null ? getScheduledStartTime().toString() : null);
                m.put("liq_cue_in", (new Float(0.0)).toString());
                m.put("liq_cue_out", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }
    
}
