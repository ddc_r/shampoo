/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.DurationMilliseconds;
import biz.ddcr.shampoo.server.helper.SizeBytes;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.text.StrSubstitutor;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class MinimalSimpleStreamableTrackQueueItemMetadata extends SimpleStreamableChannelGenericMetadata implements Serializable, Copiable, MinimalStreamableTrackMetadataInterface {

    protected static final String ITEM_TYPE = "track";
    private String trackID;
    private String trackTitle;
    private String trackAuthor;
    private String trackAlbum;
    private String trackGenre;
    private Short trackDateOfRelease;
    private String trackDescription;
    private String trackType;
    private String tag;
    private String trackPublisher;
    private String copyright;
    private long programmeRotationCount;
    private Byte pegiRatingAge;
    private String[] pegiRatingFeatures;
    private Float authorizedPlayingDuration;
    private Long scheduledStartTime;
    /**
     * associated timezone
     */
    private String timezone;
    private String playlistID;
    private String playlistLabel;
    private String playlistDescription;
    private String programmeLabel;
    private boolean picture;
    private String pictureFormat;
    private Long pictureSize;
    private String pictureURLEndpoint;
    private boolean requested;
    private String requestAuthor;
    private String requestMessage;
    private Float averageVote;
    private String friendlyCaptionPattern;
    private Integer fadeInAmount;

    private class SimpleStreamableMetadataResolver extends DisplayMetadataPatternResolver {

        @Override
        public String getPlaylistLabel() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getPlaylistLabel();
        }

        @Override
        public String getPlaylistDescription() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getPlaylistDescription();
        }

        @Override
        public String getPlaylistID() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getPlaylistID();
        }

        @Override
        public String getTimetableSlotID() {
            return null;
        }

        @Override
        public String getProgrammeLabel() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getProgrammeLabel();
        }

        @Override
        public String getChannelLabel() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getChannelLabel();
        }

        @Override
        public String getPEGIRatingAge() {
            Byte b = MinimalSimpleStreamableTrackQueueItemMetadata.this.getPEGIRatingAge();
            return b != null ? b.toString() : null;
        }

        @Override
        public String getPEGIRatingFeatures() {
            String[] s = MinimalSimpleStreamableTrackQueueItemMetadata.this.getPEGIRatingFeatures();
            return s != null ? MarshallUtil.toSimpleCSV(s).toString() : null;
        }

        @Override
        public String getAuthorizedPlayingDuration() {
            Float f = MinimalSimpleStreamableTrackQueueItemMetadata.this.getAuthorizedPlayingDuration();
            return f != null ? f.toString() : null;
        }

        @Override
        public String getScheduledStartTime() {
            Long l = MinimalSimpleStreamableTrackQueueItemMetadata.this.getScheduledStartTime();
            return l != null ? (new YearMonthWeekDayHourMinuteSecondMillisecond(l, getTimezone())).getFriendlyEnglishCaption() : null;
        }

        @Override
        public String getItemID() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackID();
        }

        @Override
        public String getItemTitle() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackTitle();
        }

        @Override
        public String getItemAuthor() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackAuthor();
        }

        @Override
        public String getItemAlbum() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackAlbum();
        }

        @Override
        public String getItemGenre() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackGenre();
        }

        @Override
        public String getItemDateOfRelease() {
            Short s = MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackDateOfRelease();
            return s != null ? s.toString() : null;
        }

        @Override
        public String getItemDescription() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackDescription();
        }

        @Override
        public String getItemType() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackType();
        }

        @Override
        public String getTag() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTag();
        }

        @Override
        public String getItemPublisher() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getTrackPublisher();
        }

        @Override
        public String getCopyright() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.getCopyright();
        }

        @Override
        public String getProgrammeRotationCount() {
            return (Long.valueOf(MinimalSimpleStreamableTrackQueueItemMetadata.this.getProgrammeRotationCount())).toString();
        }

        @Override
        public String getRequestAuthor() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.isRequested() ? MinimalSimpleStreamableTrackQueueItemMetadata.this.getRequestAuthor() : null;
        }

        @Override
        public String getRequestMessage() {
            return MinimalSimpleStreamableTrackQueueItemMetadata.this.isRequested() ? MinimalSimpleStreamableTrackQueueItemMetadata.this.getRequestMessage() : null;
        }

        @Override
        public String getAverageVote() {
            Float f = MinimalSimpleStreamableTrackQueueItemMetadata.this.getAverageVote();
            return f != null ? f.toString() : null;
        }
    }
    private SimpleStreamableMetadataResolver patternResolver = new SimpleStreamableMetadataResolver();

    public MinimalSimpleStreamableTrackQueueItemMetadata() {
    }

    public MinimalSimpleStreamableTrackQueueItemMetadata(MinimalSimpleStreamableTrackQueueItemMetadata o) {
        super(o);
        setTrackID(o.getTrackID());
        setTrackTitle(o.getTrackTitle());
        setTrackAuthor(o.getTrackAuthor());
        setTrackAlbum(o.getTrackAlbum());
        setTrackGenre(o.getTrackGenre());
        setTrackDateOfRelease(o.getTrackDateOfRelease());
        setTrackDescription(o.getTrackDescription());
        setTrackType(o.getTrackType());
        setTag(o.getTag());
        setTrackPublisher(o.getTrackPublisher());
        setCopyright(o.getCopyright());
        setProgrammeRotationCount(o.getProgrammeRotationCount());
        setPEGIRatingAge(o.getPEGIRatingAge());
        setPEGIRatingFeatures(o.getPEGIRatingFeatures());
        setAuthorizedPlayingDuration(o.getAuthorizedPlayingDuration());
        setScheduledStartTime(o.getScheduledStartTime());
        setPlaylistID(o.getPlaylistID());
        setPlaylistLabel(o.getPlaylistLabel());
        setPlaylistDescription(o.getPlaylistDescription());
        setProgrammeLabel(o.getProgrammeLabel());
        setPicture(o.hasPicture());
        setPictureFormat(o.getPictureFormat());
        setPictureSize(o.getPictureSize());
        setPictureURLEndpoint(o.getPictureURLEndpoint());
        setRequested(o.isRequested());
        setRequestAuthor(o.getRequestAuthor());
        setRequestMessage(o.getRequestMessage());
        setFriendlyCaptionPattern(o.getFriendlyCaptionPattern());
        setAverageVote(o.getAverageVote());
        setTimezone(o.getTimezone());
        setFadeInAmount(o.getFadeInAmount());
    }

    protected String getFriendlyCaptionPattern() {
        return friendlyCaptionPattern;
    }

    @Override
    public void setFriendlyCaptionPattern(String pattern) {
        this.friendlyCaptionPattern = pattern;
    }

    @Override
    public String getFriendlyCaption() {
        StrSubstitutor friendlycaptionMaker = new StrSubstitutor(patternResolver);
        return friendlycaptionMaker.replace(getFriendlyCaptionPattern());
    }

    @Override
    public String getType() {
        return ITEM_TYPE;
    }

    public void setRequestAuthor(String requestAuthor) {
        this.requestAuthor = requestAuthor;
    }

    @Override
    public String getRequestAuthor() {
        return requestAuthor;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    @Override
    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequested(boolean requested) {
        this.requested = requested;
    }

    @Override
    public boolean isRequested() {
        return requested;
    }

    public Float getAuthorizedPlayingDuration() {
        return authorizedPlayingDuration;
    }

    public void setAuthorizedPlayingDuration(Float authorizedPlayingDuration) {
        this.authorizedPlayingDuration = authorizedPlayingDuration;
    }

    public Long getScheduledStartTime() {
        return scheduledStartTime;
    }

    public void setScheduledStartTime(Long scheduledStartTime) {
        this.scheduledStartTime = scheduledStartTime;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String getTrackPublisher() {
        return trackPublisher;
    }

    public void setTrackPublisher(String trackPublisher) {
        this.trackPublisher = trackPublisher;
    }

    @Override
    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    @Override
    public Byte getPEGIRatingAge() {
        return pegiRatingAge;
    }

    public void setPEGIRatingAge(Byte pegiRatingAge) {
        this.pegiRatingAge = pegiRatingAge;
    }

    @Override
    public String[] getPEGIRatingFeatures() {
        //TODO: check whether clone() fullfills its job
        return pegiRatingFeatures!=null ? pegiRatingFeatures.clone() : null;
    }

    public void setPEGIRatingFeatures(String[] pegiRatingFeatures) {
        this.pegiRatingFeatures = pegiRatingFeatures;
    }

    @Override
    public Float getAverageVote() {
        return averageVote;
    }

    public void setAverageVote(Float averageVote) {
        this.averageVote = averageVote;
    }

    @Override
    public boolean hasPicture() {
        return picture;
    }

    public void setPicture(boolean picture) {
        this.picture = picture;
    }

    @Override
    public String getPictureFormat() {
        return pictureFormat;
    }

    public void setPictureFormat(String pictureFormat) {
        this.pictureFormat = pictureFormat;
    }

    @Override
    public Long getPictureSize() {
        return pictureSize;
    }

    public void setPictureSize(Long pictureSize) {
        this.pictureSize = pictureSize;
    }

    @Override
    public String getPictureURLEndpoint() {
        return pictureURLEndpoint;
    }

    public void setPictureURLEndpoint(String pictureURLEndpoint) {
        this.pictureURLEndpoint = pictureURLEndpoint;
    }

    @Override
    public String getPlaylistDescription() {
        return playlistDescription;
    }

    public void setPlaylistDescription(String playlistDescription) {
        this.playlistDescription = playlistDescription;
    }

    @Override
    public String getPlaylistID() {
        return playlistID;
    }

    public void setPlaylistID(String playlistID) {
        this.playlistID = playlistID;
    }

    @Override
    public String getPlaylistLabel() {
        return playlistLabel;
    }

    public void setPlaylistLabel(String playlistLabel) {
        this.playlistLabel = playlistLabel;
    }

    @Override
    public String getProgrammeLabel() {
        return programmeLabel;
    }

    public void setProgrammeLabel(String programmeLabel) {
        this.programmeLabel = programmeLabel;
    }

    @Override
    public long getProgrammeRotationCount() {
        return programmeRotationCount;
    }

    public void setProgrammeRotationCount(long programmeRotationCount) {
        this.programmeRotationCount = programmeRotationCount;
    }

    @Override
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String getTrackAlbum() {
        return trackAlbum;
    }

    public void setTrackAlbum(String trackAlbum) {
        this.trackAlbum = trackAlbum;
    }

    @Override
    public String getTrackAuthor() {
        return trackAuthor;
    }

    public void setTrackAuthor(String trackAuthor) {
        this.trackAuthor = trackAuthor;
    }

    @Override
    public Short getTrackDateOfRelease() {
        return trackDateOfRelease;
    }

    public void setTrackDateOfRelease(Short trackDateOfRelease) {
        this.trackDateOfRelease = trackDateOfRelease;
    }

    @Override
    public String getTrackDescription() {
        return trackDescription;
    }

    public void setTrackDescription(String trackDescription) {
        this.trackDescription = trackDescription;
    }

    @Override
    public String getTrackGenre() {
        return trackGenre;
    }

    public void setTrackGenre(String trackGenre) {
        this.trackGenre = trackGenre;
    }

    public String getTrackID() {
        return trackID;
    }

    public void setTrackID(String trackID) {
        this.trackID = trackID;
    }

    @Override
    public String getTrackTitle() {
        return trackTitle;
    }

    public void setTrackTitle(String trackTitle) {
        this.trackTitle = trackTitle;
    }

    @Override
    public String getTrackType() {
        return trackType;
    }

    public void setTrackType(String trackType) {
        this.trackType = trackType;
    }

    @Override
    public Integer getFadeInAmount() {
        return fadeInAmount;
    }

    public void setFadeInAmount(Integer fadeInAmount) {
        this.fadeInAmount = fadeInAmount;
    }

    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MinimalSimpleStreamableTrackQueueItemMetadata other = (MinimalSimpleStreamableTrackQueueItemMetadata) obj;
        if ((this.trackID == null) ? (other.trackID != null) : !this.trackID.equals(other.trackID)) {
            return false;
        }
        if ((this.trackAlbum == null) ? (other.trackAlbum != null) : !this.trackAlbum.equals(other.trackAlbum)) {
            return false;
        }
        if ((this.playlistID == null) ? (other.playlistID != null) : !this.playlistID.equals(other.playlistID)) {
            return false;
        }
        if ((this.programmeLabel == null) ? (other.programmeLabel != null) : !this.programmeLabel.equals(other.programmeLabel)) {
            return false;
        }
        if ((this.getChannelLabel() == null) ? (other.getChannelLabel() != null) : !this.getChannelLabel().equals(other.getChannelLabel())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.trackID != null ? this.trackID.hashCode() : 0);
        hash = 29 * hash + (this.playlistID != null ? this.playlistID.hashCode() : 0);
        hash = 29 * hash + (this.programmeLabel != null ? this.programmeLabel.hashCode() : 0);
        hash = 29 * hash + (this.getChannelLabel() != null ? this.getChannelLabel().hashCode() : 0);
        return hash;
    }

    @Override
    public int fullHashCode() {
        int hash = 3;
        hash = 89 * hash + super.fullHashCode();
        hash = 89 * hash + (this.trackID != null ? this.trackID.hashCode() : 0);
        hash = 89 * hash + (this.trackTitle != null ? this.trackTitle.hashCode() : 0);
        hash = 89 * hash + (this.trackAuthor != null ? this.trackAuthor.hashCode() : 0);
        hash = 89 * hash + (this.trackAlbum != null ? this.trackAlbum.hashCode() : 0);
        hash = 89 * hash + (this.trackGenre != null ? this.trackGenre.hashCode() : 0);
        hash = 89 * hash + (this.trackDateOfRelease != null ? this.trackDateOfRelease.hashCode() : 0);
        hash = 89 * hash + (this.trackDescription != null ? this.trackDescription.hashCode() : 0);
        hash = 89 * hash + (this.trackType != null ? this.trackType.hashCode() : 0);
        hash = 89 * hash + (this.tag != null ? this.tag.hashCode() : 0);
        hash = 89 * hash + (this.trackPublisher != null ? this.trackPublisher.hashCode() : 0);
        hash = 89 * hash + (this.copyright != null ? this.copyright.hashCode() : 0);
        hash = 89 * hash + (int) (this.programmeRotationCount ^ (this.programmeRotationCount >>> 32));
        hash = 89 * hash + (this.pegiRatingAge != null ? this.pegiRatingAge.hashCode() : 0);
        hash = 89 * hash + Arrays.deepHashCode(this.pegiRatingFeatures);
        hash = 89 * hash + (this.authorizedPlayingDuration != null ? this.authorizedPlayingDuration.hashCode() : 0);
        hash = 89 * hash + (this.scheduledStartTime != null ? this.scheduledStartTime.hashCode() : 0);
        hash = 89 * hash + (this.timezone != null ? this.timezone.hashCode() : 0);
        hash = 89 * hash + (this.playlistID != null ? this.playlistID.hashCode() : 0);
        hash = 89 * hash + (this.playlistLabel != null ? this.playlistLabel.hashCode() : 0);
        hash = 89 * hash + (this.playlistDescription != null ? this.playlistDescription.hashCode() : 0);
        hash = 89 * hash + (this.programmeLabel != null ? this.programmeLabel.hashCode() : 0);
        hash = 89 * hash + (this.picture ? 1 : 0);
        hash = 89 * hash + (this.pictureFormat != null ? this.pictureFormat.hashCode() : 0);
        hash = 89 * hash + (this.pictureSize != null ? this.pictureSize.hashCode() : 0);
        hash = 89 * hash + (this.pictureURLEndpoint != null ? this.pictureURLEndpoint.hashCode() : 0);
        hash = 89 * hash + (this.requested ? 1 : 0);
        hash = 89 * hash + (this.requestAuthor != null ? this.requestAuthor.hashCode() : 0);
        hash = 89 * hash + (this.requestMessage != null ? this.requestMessage.hashCode() : 0);
        hash = 89 * hash + (this.averageVote != null ? this.averageVote.hashCode() : 0);
        hash = 89 * hash + (this.friendlyCaptionPattern != null ? this.friendlyCaptionPattern.hashCode() : 0);
        hash = 89 * hash + (this.fadeInAmount != null ? this.fadeInAmount.hashCode() : 0);
        return hash;
    }
    
    @Override
    public MinimalSimpleStreamableTrackQueueItemMetadata shallowCopy() {
        return new MinimalSimpleStreamableTrackQueueItemMetadata(this);
    }

    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getType());
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());

                s.add(getTrackID());
                s.add(getTrackTitle());
                s.add(getTrackAuthor());
                s.add(getTrackAlbum());
                s.add(getTrackGenre());
                s.add(getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                s.add(getTrackDescription());
                s.add(getTrackType());
                s.add(getTag());
                s.add(getTrackPublisher());
                s.add(getCopyright());
                s.add(Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    s.add(getRequestAuthor());
                    s.add(getRequestMessage());
                }

                s.add(getPEGIRatingAge() != null ? getPEGIRatingAge().toString() : null);
                s.add(getPEGIRatingFeatures() != null ? MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString() : null);
                s.add(getPlaylistID());
                s.add(getPlaylistLabel());
                s.add(getPlaylistDescription());
                s.add(getProgrammeLabel());
                s.add(getPictureFormat());
                if (userFriendlyUnitValues) {
                    s.add(getPictureSize() != null ? new SizeBytes(getPictureSize()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getPictureSize() != null ? Long.toString(getPictureSize()) : null);
                }
                s.add(getPictureURLEndpoint());
                if (userFriendlyUnitValues) {
                    s.add(getScheduledStartTime()!=null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                    s.add(getFadeInAmount()!=null ? new DurationMilliseconds(getFadeInAmount()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    s.add(getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                    s.add(getFadeInAmount() != null ? getFadeInAmount().toString() : null);
                }
                s.add(getFriendlyCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription", getChannelDescription());
                m.put("channelURL", getChannelURL());
                m.put("channelTag", getChannelTag());
                m.put("playlistID", getPlaylistID());
                m.put("playlistLabel", getPlaylistLabel());
                m.put("playlistDescription", getPlaylistDescription());
                m.put("programmeLabel", getProgrammeLabel());
                
                m.put("trackID",getTrackID());
                m.put("trackTitle",getTrackTitle());
                m.put("trackAuthor",getTrackAuthor());
                m.put("trackAlbum",getTrackAlbum());
                m.put("trackGenre",getTrackGenre());
                m.put("trackDateOfRelease",getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                m.put("trackDescription",getTrackDescription());
                m.put("trackType",getTrackType());
                m.put("tag",getTag());
                m.put("trackPublisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programmeRotationCount",Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    m.put("requestAuthor",getRequestAuthor());
                    m.put("requestMessage",getRequestMessage());
                }
                
                m.put("pegiRatingAge", getPEGIRatingAge());
                m.put("pegiRatingFeatures", getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("pictureFormat", getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("pictureSize", getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("pictureSize", getPictureSize());
                    }
                    m.put("pictureURLEndpoint", getPictureURLEndpoint());
                }
                if (userFriendlyUnitValues) {
                    m.put("startTime", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("duration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                    m.put("fadeIn", getFadeInAmount()!=null ? new DurationMilliseconds(getFadeInAmount()) : null);
                } else {
                    m.put("startTime", getScheduledStartTime());
                    m.put("duration", getAuthorizedPlayingDuration());
                    m.put("fadeIn", getFadeInAmount());
                }
                m.put("friendlyCaption", getFriendlyCaption());                
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel-label", getChannelLabel());
                m.put("channel-description", getChannelDescription());
                m.put("channel-url", getChannelURL());
                m.put("channel-tag", getChannelTag());
                m.put("playlist-id", getPlaylistID());
                m.put("playlist-label", getPlaylistLabel());
                m.put("playlist-description", getPlaylistDescription());
                m.put("programme-label", getProgrammeLabel());
                
                m.put("track-id",getTrackID());
                m.put("track-title",getTrackTitle());
                m.put("track-author",getTrackAuthor());
                m.put("track-album",getTrackAlbum());
                m.put("track-genre",getTrackGenre());
                m.put("track-date-of-release",getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                m.put("track-description",getTrackDescription());
                m.put("track-type",getTrackType());
                m.put("tag",getTag());
                m.put("track-publisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programme-rotation-count",Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    m.put("request-author",getRequestAuthor());
                    m.put("request-message",getRequestMessage());
                }
                
                m.put("pegi-rating-age", getPEGIRatingAge());
                m.put("pegi-rating-features", getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("picture-format", getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("picture-size", getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("picture-size", getPictureSize());
                    }
                    m.put("picture-url-endpoint", getPictureURLEndpoint());
                }
                if (userFriendlyUnitValues) {
                    m.put("start-time", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("duration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                    m.put("fade-in", getFadeInAmount()!=null ? new DurationMilliseconds(getFadeInAmount()) : null);
                } else {
                    m.put("start-time", getScheduledStartTime());
                    m.put("duration", getAuthorizedPlayingDuration());
                    m.put("fade-in", getFadeInAmount());
                }
                m.put("friendly-caption", getFriendlyCaption());
                return MarshallUtil.toSimpleXML("minimal-queue-item", m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description", getChannelDescription());
                m.put("channel_url", getChannelURL());
                m.put("channel_tag", getChannelTag());
                m.put("programme_id", getProgrammeLabel());
                m.put("playlist_id", getPlaylistID());
                m.put("playlist_label", getPlaylistLabel());
                m.put("playlist_description", getPlaylistDescription());
                
                m.put("track_id",getTrackID());
                m.put("title",getTrackTitle());
                m.put("artist",getTrackAuthor());
                m.put("album",getTrackAlbum());
                m.put("genre",getTrackGenre());
                m.put("date",getTrackDateOfRelease());
                m.put("comment",getTrackDescription());
                m.put("sub_type",getTrackType());
                m.put("tag",getTag());
                m.put("publisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programme_rotation_count",getProgrammeRotationCount());
                if (isRequested()) {
                    m.put("request_author",getRequestAuthor());
                    m.put("request_message",getRequestMessage());
                }
                
                m.put("pegi_rating_age", getPEGIRatingAge());
                m.put("pegi_rating_features", getPEGIRatingFeatures());
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format", getPictureFormat());
                    m.put("picture_size", getPictureSize());
                    m.put("picture_url_endpoint", getPictureURLEndpoint());
                }
                m.put("liq_queue_start_time", getScheduledStartTime());
                m.put("liq_cue_in", new Float(0.0));
                m.put("liq_cue_out", getAuthorizedPlayingDuration());
                m.put("liq_fade_in", getFadeInAmount() != null ? new Float(getFadeInAmount()/1000.0) : null);
                m.put("liq_fade_out", getFadeInAmount() != null ? new Float(getFadeInAmount()/500.0) : null); /* == liq_fade_in * 2*/
                m.put("liq_start_next", getFadeInAmount() != null ? new Float(getFadeInAmount()*0.003) : null); /* == liq_fade_in + liq_fade_out*/
                return MarshallUtil.toSimpleAnnotate(m, null);
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description", getChannelDescription());
                m.put("channel_url", getChannelURL());
                m.put("channel_tag", getChannelTag());
                
                m.put("programme_id", getProgrammeLabel());
                m.put("playlist_id", getPlaylistID());
                m.put("playlist_label", getPlaylistLabel());
                m.put("playlist_description", getPlaylistDescription());
                
                m.put("track_id",getTrackID());
                m.put("title",getTrackTitle());
                m.put("artist",getTrackAuthor());
                m.put("album",getTrackAlbum());
                m.put("genre",getTrackGenre());
                m.put("date",getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                m.put("comment",getTrackDescription());
                m.put("sub_type",getTrackType());
                m.put("tag",getTag());
                m.put("publisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programme_rotation_count",Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    m.put("request_author",getRequestAuthor());
                    m.put("request_message",getRequestMessage());
                }
                
                m.put("pegi_rating_age", getPEGIRatingAge() != null ? getPEGIRatingAge().toString() : null);
                m.put("pegi_rating_features", getPEGIRatingFeatures() != null ? MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString() : null);
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format", getPictureFormat());
                    m.put("picture_size", getPictureSize() != null ? getPictureSize().toString() : null);
                    m.put("picture_url_endpoint", getPictureURLEndpoint());
                }
                m.put("liq_queue_start_time", getScheduledStartTime()!=null ? getScheduledStartTime().toString() : null);
                m.put("liq_cue_in", (new Float(0.0)).toString());
                m.put("liq_cue_out", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                m.put("liq_fade_in", getFadeInAmount() != null ? new Float(getFadeInAmount()/1000.0).toString() : null);
                m.put("liq_fade_out", getFadeInAmount() != null ? new Float(getFadeInAmount()/500.0).toString() : null); /* == liq_fade_in * 2*/
                m.put("liq_start_next", getFadeInAmount() != null ? new Float(getFadeInAmount()*0.003).toString() : null); /* == liq_fade_in + liq_fade_out*/
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }
}
