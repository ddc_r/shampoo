/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

/**
 * Simple transfer object for webservices, with primitive types only
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface MinimalStreamableItemMetadataInterface extends StreamableChannelMetadataInterface {

    public String getType();

    public String getPlaylistID();
    public String getPlaylistLabel();
    public String getPlaylistDescription();
    public String getProgrammeLabel();

    /** PEGI Rating age**/
    public Byte getPEGIRatingAge();
    /** PEGI Rating features:
        violence,
        profanity,
        fear,
        sex,
        drugs,
        discrimination **/
    public String[] getPEGIRatingFeatures();

    /** Does the live or track have an associated picture? **/
    public boolean hasPicture();

    /** where the picture is available for download if needed,
        the URL contains the protocol, the host, the port, the private session key, etc...
        It's the responsibility of the streamer to properly decode it **/
    public String getPictureURLEndpoint();

    public String getPictureFormat();
    /** physical size in bytes; it's the full length of the track, header included */
    public Long getPictureSize();

    /**
     * Friendly caption representing the item, it's made of substituable variables.
     * @param pattern
     */
    public void setFriendlyCaptionPattern(String pattern);
    /**
     * Friendly caption representing the item, once substitutable variables have been resolved
     * @return
     */
    public String getFriendlyCaption();
}
