/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface MinimalStreamableTrackMetadataInterface extends MinimalStreamableItemMetadataInterface  {

    public String getTrackTitle();
    public String getTrackAuthor();
    public String getTrackAlbum();
    public String getTrackGenre();
    public Short getTrackDateOfRelease();
    public String getTrackDescription();
    /** Advert, Jingle, or Song **/
    public String getTrackType();
    public String getTag();
    public String getTrackPublisher();
    public String getCopyright();
    public long getProgrammeRotationCount();

    public Float getAverageVote();

    public boolean isRequested();
    public String getRequestAuthor();
    public String getRequestMessage();

    public Integer getFadeInAmount();
}
