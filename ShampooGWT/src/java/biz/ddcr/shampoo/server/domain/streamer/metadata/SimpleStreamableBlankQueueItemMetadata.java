/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.helper.DurationMilliseconds;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.text.StrSubstitutor;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SimpleStreamableBlankQueueItemMetadata extends MinimalSimpleStreamableBlankQueueItemMetadata implements StreamableItemMetadataInterface, StreamableChannelMetadataInterface, StreamableQueueItemMetadataInterface {

    private String queueID;

    private String itemURLEndpoint;


    private class SimpleStreamableURISchemeMetadataResolver extends URISchemeMetadataPatternResolver {

        @Override
        public String getQueueID() {
            return SimpleStreamableBlankQueueItemMetadata.this.getQueueID();
        }
        @Override
        public String getPlaylistID() {
            return SimpleStreamableBlankQueueItemMetadata.this.getPlaylistID();
        }
        @Override
        public String getTimetableSlotID() {
            return null;
        }
        @Override
        public String getProgrammeLabel() {
            return SimpleStreamableBlankQueueItemMetadata.this.getProgrammeLabel();
        }
        @Override
        public String getChannelLabel() {
            return SimpleStreamableBlankQueueItemMetadata.this.getChannelLabel();
        }
    }

    private SimpleStreamableURISchemeMetadataResolver uriSchemePatternResolver = new SimpleStreamableURISchemeMetadataResolver();

    public SimpleStreamableBlankQueueItemMetadata() {
    }

    public SimpleStreamableBlankQueueItemMetadata(SimpleStreamableBlankQueueItemMetadata o) {
        super( o );
        setQueueID( o.getQueueID() );
        setItemURLEndpointPattern( o.getItemURLEndpointPattern() );
    }

    public void setQueueID(String queueID) {
        this.queueID = queueID;
    }
    
    public String getQueueID() {
        return queueID;
    }
    
    @Override
    public void setItemURLEndpointPattern(String pattern) {
        itemURLEndpoint = pattern;
    }

    protected String getItemURLEndpointPattern() {
        return itemURLEndpoint;
    }

    @Override
    public String getItemURLEndpoint() {
        StrSubstitutor friendlycaptionMaker = new StrSubstitutor(uriSchemePatternResolver);
        return friendlycaptionMaker.replace(getItemURLEndpointPattern());
    }
    
    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public SimpleStreamableBlankQueueItemMetadata shallowCopy() {
        return new SimpleStreamableBlankQueueItemMetadata(this);
    }

    @Override
    public int fullHashCode() {
        int hash = 7;
        hash = 79 * hash + super.fullHashCode();
        hash = 79 * hash + (this.queueID != null ? this.queueID.hashCode() : 0);
        hash = 79 * hash + (this.itemURLEndpoint != null ? this.itemURLEndpoint.hashCode() : 0);
        hash = 79 * hash + (this.uriSchemePatternResolver != null ? this.uriSchemePatternResolver.hashCode() : 0);
        return hash;
    }

    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getType());
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());
                
                s.add(getQueueID());
                s.add(getItemURLEndpoint());
                
                if (userFriendlyUnitValues) {
                    s.add(getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    s.add(getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                }
                s.add(getFriendlyCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription",getChannelDescription());
                m.put("channelURL",getChannelURL());
                m.put("channelTag",getChannelTag());
                
                m.put("queueID",getQueueID());
                m.put("itemURLEndpoint",getItemURLEndpoint());
                
                if (userFriendlyUnitValues) {
                    m.put("scheduledStartTime", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    m.put("authorizedPlayingDuration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                } else {
                    m.put("scheduledStartTime", getScheduledStartTime());
                    m.put("authorizedPlayingDuration", getAuthorizedPlayingDuration());
                }
                m.put("friendlyCaption", getFriendlyCaption());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel-label", getChannelLabel());
                m.put("channel-description",getChannelDescription());
                m.put("channel-url",getChannelURL());
                m.put("channel-tag",getChannelTag());
                
                m.put("queue-id",getQueueID());
                m.put("item-url-endpoint",getItemURLEndpoint());
                
                if (userFriendlyUnitValues) {
                    m.put("scheduled-start-time", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    m.put("authorized-playing-duration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                } else {
                    m.put("scheduled-start-time", getScheduledStartTime());
                    m.put("authorized-playing-duration", getAuthorizedPlayingDuration());
                }
                m.put("friendly-caption", getFriendlyCaption());
                return MarshallUtil.toSimpleXML("queue-item",m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());
                
                m.put("item_url_endpoint",getItemURLEndpoint());
                m.put("queue_id",getQueueID());
                
                m.put("song", getFriendlyCaption());                
                m.put("liq_queue_start_time", getScheduledStartTime());
                m.put("liq_cue_in", new Float(0.0));
                m.put("liq_cue_out", getAuthorizedPlayingDuration());                
                return MarshallUtil.toSimpleAnnotate(m, getItemURLEndpoint());
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description",getChannelDescription());
                m.put("channel_url",getChannelURL());
                m.put("channel_tag",getChannelTag());
                
                m.put("item_url_endpoint",getItemURLEndpoint());
                m.put("queue_id",getQueueID());                
                
                m.put("song", getFriendlyCaption());                
                m.put("liq_queue_start_time", getScheduledStartTime()!=null?getScheduledStartTime().toString():null);
                m.put("liq_cue_in", (new Float(0.0)).toString());
                m.put("liq_cue_out", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);                
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }     
}
