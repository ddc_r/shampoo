/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import biz.ddcr.shampoo.server.io.serializer.MarshallableSupporter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SimpleStreamableChannelGenericMetadata implements Serializable, Copiable, StreamableChannelMetadataInterface {

    private String channelLabel;
    private String channelDescription;
    private String channelURL;
    private String channelTag;

    public SimpleStreamableChannelGenericMetadata() {
    }

    public SimpleStreamableChannelGenericMetadata(StreamableChannelMetadataInterface o) {
        setChannelLabel( o.getChannelLabel() );
        setChannelDescription( o.getChannelDescription() );
        setChannelURL( o.getChannelURL() );
        setChannelTag( o.getChannelTag() );
    }

    @Override
    public String getChannelDescription() {
        return channelDescription;
    }

    public void setChannelDescription(String channelDescription) {
        this.channelDescription = channelDescription;
    }

    @Override
    public String getChannelLabel() {
        return channelLabel;
    }

    public void setChannelLabel(String channelLabel) {
        this.channelLabel = channelLabel;
    }

    @Override
    public String getChannelTag() {
        return channelTag;
    }

    public void setChannelTag(String channelTag) {
        this.channelTag = channelTag;
    }

    @Override
    public String getChannelURL() {
        return channelURL;
    }

    public void setChannelURL(String channelURL) {
        this.channelURL = channelURL;
    }

    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleStreamableChannelGenericMetadata other = (SimpleStreamableChannelGenericMetadata) obj;
        if ((this.channelLabel == null) ? (other.channelLabel != null) : !this.channelLabel.equals(other.channelLabel)) {
            return false;
        }
        if ((this.channelDescription == null) ? (other.channelDescription != null) : !this.channelDescription.equals(other.channelDescription)) {
            return false;
        }
        if ((this.channelURL == null) ? (other.channelURL != null) : !this.channelURL.equals(other.channelURL)) {
            return false;
        }
        if ((this.channelTag == null) ? (other.channelTag != null) : !this.channelTag.equals(other.channelTag)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.channelLabel != null ? this.channelLabel.hashCode() : 0);
        hash = 59 * hash + (this.channelDescription != null ? this.channelDescription.hashCode() : 0);
        hash = 59 * hash + (this.channelURL != null ? this.channelURL.hashCode() : 0);
        hash = 59 * hash + (this.channelTag != null ? this.channelTag.hashCode() : 0);
        return hash;
    }

    @Override
    public int fullHashCode() {
        return hashCode();
    }

    @Override
    public SimpleStreamableChannelGenericMetadata shallowCopy() {
        return new SimpleStreamableChannelGenericMetadata(this);
    }

    @Override
    public void supports(MarshallableSupporter otherObject) throws Exception {
        otherObject.support(this);
    }

    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription", getChannelDescription());
                m.put("channelURL", getChannelURL());
                m.put("channelTag", getChannelTag());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("channel-label", getChannelLabel());
                m.put("channel-description", getChannelDescription());
                m.put("channel-url", getChannelURL());
                m.put("channel-tag", getChannelTag());
                return MarshallUtil.toSimpleXML("channel", m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("channel_id", getChannelLabel());
                m.put("channel_description", getChannelDescription());
                m.put("channel_url", getChannelURL());
                m.put("channel_tag", getChannelTag());
                return MarshallUtil.toSimpleAnnotate(m, null);
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("channel_id", getChannelLabel());
                m.put("channel_description", getChannelDescription());
                m.put("channel_url", getChannelURL());
                m.put("channel_tag", getChannelTag());
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }
}
