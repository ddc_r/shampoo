/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.server.helper.BitrateBitsPerSecond;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataVisitor;
import biz.ddcr.shampoo.server.helper.Copiable;
import biz.ddcr.shampoo.server.helper.DurationMilliseconds;
import biz.ddcr.shampoo.server.helper.SamplerateSamplesPerSecond;
import biz.ddcr.shampoo.server.helper.SizeBytes;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SimpleStreamableTrackQueueItemMetadata extends MinimalSimpleStreamableTrackQueueItemMetadata implements Serializable, Copiable, StreamableTrackMetadataInterface, StreamableQueueItemMetadataInterface {

    private String trackFormat;
    private long trackSize;
    private float trackDuration;
    private long trackBitrate;
    private int trackSamplerate;
    private byte trackChannels;
    private boolean trackVBR;
    private String itemURLEndpoint;
    private String queueID;

    public SimpleStreamableTrackQueueItemMetadata() {
    }

    public SimpleStreamableTrackQueueItemMetadata(SimpleStreamableTrackQueueItemMetadata o) {
        super( o );
        setTrackFormat( o.getTrackFormat() );
        setTrackSize( o.getTrackSize() );
        setTrackDuration( o.getTrackDuration() );
        setTrackBitrate( o.getTrackBitrate() );
        setTrackSamplerate( o.getTrackSamplerate() );
        setTrackChannels( o.getTrackChannels() );
        setTrackVBR( o.isTrackVBR() );
        setItemURLEndpoint( o.getItemURLEndpoint() );
        setQueueID( o.getQueueID() );
    }

    public void setQueueID(String queueID) {
        this.queueID = queueID;
    }
    
    public String getQueueID() {
        return queueID;
    }
    
    @Override
    public void setItemURLEndpointPattern(String pattern) {
        //Unsupported, do nothing
    }

    @Override
    public String getItemURLEndpoint() {
        return itemURLEndpoint;
    }

    public void setItemURLEndpoint(String itemURLEndpoint) {
        this.itemURLEndpoint = itemURLEndpoint;
    }

    @Override
    public long getTrackBitrate() {
        return trackBitrate;
    }

    public void setTrackBitrate(long trackBitrate) {
        this.trackBitrate = trackBitrate;
    }

    @Override
    public byte getTrackChannels() {
        return trackChannels;
    }

    public void setTrackChannels(byte trackChannels) {
        this.trackChannels = trackChannels;
    }

    @Override
    public float getTrackDuration() {
        return trackDuration;
    }

    public void setTrackDuration(float trackDuration) {
        this.trackDuration = trackDuration;
    }

    @Override
    public String getTrackFormat() {
        return trackFormat;
    }

    public void setTrackFormat(String trackFormat) {
        this.trackFormat = trackFormat;
    }

    @Override
    public int getTrackSamplerate() {
        return trackSamplerate;
    }

    public void setTrackSamplerate(int trackSamplerate) {
        this.trackSamplerate = trackSamplerate;
    }

    @Override
    public long getTrackSize() {
        return trackSize;
    }

    public void setTrackSize(long trackSize) {
        this.trackSize = trackSize;
    }

    @Override
    public boolean isTrackVBR() {
        return trackVBR;
    }

    public void setTrackVBR(boolean trackVBR) {
        this.trackVBR = trackVBR;
    }

    @Override
    public void acceptVisit(SerializableMetadataVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public SimpleStreamableTrackQueueItemMetadata shallowCopy() {
        return new SimpleStreamableTrackQueueItemMetadata(this);
    }

    @Override
    public int fullHashCode() {
        int hash = 5;
        hash = 31 * hash + super.fullHashCode();
        hash = 31 * hash + (this.trackFormat != null ? this.trackFormat.hashCode() : 0);
        hash = 31 * hash + (int) (this.trackSize ^ (this.trackSize >>> 32));
        hash = 31 * hash + Float.floatToIntBits(this.trackDuration);
        hash = 31 * hash + (int) (this.trackBitrate ^ (this.trackBitrate >>> 32));
        hash = 31 * hash + this.trackSamplerate;
        hash = 31 * hash + this.trackChannels;
        hash = 31 * hash + (this.trackVBR ? 1 : 0);
        hash = 31 * hash + (this.itemURLEndpoint != null ? this.itemURLEndpoint.hashCode() : 0);
        hash = 31 * hash + (this.queueID != null ? this.queueID.hashCode() : 0);
        return hash;
    }
    
    @Override
    public BasicHTTPDataMarshaller.BasicDataMarshalledChunk marshall(final boolean userFriendlyUnitValues) {

        return new BasicHTTPDataMarshaller.BasicDataMarshalledChunk() {
            @Override
            public StringBuilder toCSV() {
                Collection<String> s = new ArrayList<String>();
                s.add(getType());
                s.add(getChannelLabel());
                s.add(getChannelDescription());
                s.add(getChannelURL());
                s.add(getChannelTag());

                s.add(getTrackID());
                s.add(getTrackTitle());
                s.add(getTrackAuthor());
                s.add(getTrackAlbum());
                s.add(getTrackGenre());
                s.add(getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                s.add(getTrackDescription());
                s.add(getTrackType());
                s.add(getTag());
                s.add(getTrackPublisher());
                s.add(getCopyright());
                s.add(Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    s.add(getRequestAuthor());
                    s.add(getRequestMessage());
                }
                
                s.add(getTrackFormat() );
                if (userFriendlyUnitValues) {
                    s.add(new SizeBytes(getTrackSize()).getFriendlyEnglishCaption());
                    s.add(new DurationMilliseconds(getTrackDuration()).getFriendlyEnglishCaption());
                    s.add(new BitrateBitsPerSecond(getTrackBitrate()).getFriendlyEnglishCaption());
                    s.add(new SamplerateSamplesPerSecond(getTrackSamplerate()).getFriendlyEnglishCaption());
                } else {
                    s.add(Long.toString(getTrackSize()));
                    s.add(Float.toString(getTrackDuration()));
                            s.add(Long.toString(getTrackBitrate()));
                     s.add(Long.toString(getTrackSamplerate()));
                }                
                s.add(Byte.toString(getTrackChannels()));
                s.add(Boolean.toString(isTrackVBR()));
                s.add(getItemURLEndpoint());
                s.add(getQueueID());

                s.add(getPEGIRatingAge() != null ? getPEGIRatingAge().toString() : null);
                s.add(getPEGIRatingFeatures() != null ? MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString() : null);
                s.add(getPlaylistID());
                s.add(getPlaylistLabel());
                s.add(getPlaylistDescription());
                s.add(getProgrammeLabel());
                s.add(getPictureFormat());
                if (userFriendlyUnitValues) {
                    s.add(getPictureSize() != null ? new SizeBytes(getPictureSize()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getPictureSize()!=null ? Long.toString(getPictureSize()) : null);
                }
                s.add(getPictureURLEndpoint());
                if (userFriendlyUnitValues) {
                    s.add(getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()).getFriendlyEnglishCaption() : null);
                    s.add(getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()).getFriendlyEnglishCaption() : null);
                    s.add(getFadeInAmount()!=null ? new DurationMilliseconds(getFadeInAmount()).getFriendlyEnglishCaption() : null);
                } else {
                    s.add(getScheduledStartTime() != null ? getScheduledStartTime().toString() : null);
                    s.add(getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                    s.add(getFadeInAmount() != null ? getFadeInAmount().toString() : null);
                }
                s.add(getFriendlyCaption());
                return MarshallUtil.toSimpleCSV(s);
            }

            @Override
            public StringBuilder toJSON() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channelLabel", getChannelLabel());
                m.put("channelDescription", getChannelDescription());
                m.put("channelURL", getChannelURL());
                m.put("channelTag", getChannelTag());
                m.put("playlistID", getPlaylistID());
                m.put("playlistLabel", getPlaylistLabel());
                m.put("playlistDescription", getPlaylistDescription());
                m.put("programmeLabel", getProgrammeLabel());
                
                m.put("trackID",getTrackID());
                m.put("trackTitle",getTrackTitle());
                m.put("trackAuthor",getTrackAuthor());
                m.put("trackAlbum",getTrackAlbum());
                m.put("trackGenre",getTrackGenre());
                m.put("trackDateOfRelease",getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                m.put("trackDescription",getTrackDescription());
                m.put("trackType",getTrackType());
                m.put("tag",getTag());
                m.put("trackPublisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programmeRotationCount",Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    m.put("requestAuthor",getRequestAuthor());
                    m.put("requestMessage",getRequestMessage());
                }
                
                m.put("trackFormat",getTrackFormat() );
                if (userFriendlyUnitValues) {
                    m.put("trackSize",new SizeBytes(getTrackSize()));
                    m.put("trackDuration",new DurationMilliseconds(getTrackDuration()));
                    m.put("trackBitrate",new BitrateBitsPerSecond(getTrackBitrate()));
                    m.put("trackSamplerate",new SamplerateSamplesPerSecond(getTrackSamplerate()));
                } else {
                    m.put("trackSize",getTrackSize());
                    m.put("trackDuration",getTrackDuration());
                    m.put("trackBitrate",getTrackBitrate());
                    m.put("trackSamplerate",getTrackSamplerate());
                }                
                m.put("trackChannels",getTrackChannels());
                m.put("trackVBR",isTrackVBR());
                m.put("itemURLEndpoint",getItemURLEndpoint());
                m.put("queueID",getQueueID());
                
                m.put("pegiRatingAge", getPEGIRatingAge());
                m.put("pegiRatingFeatures", getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("pictureFormat", getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("pictureSize", getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("pictureSize", getPictureSize());
                    }
                    m.put("pictureURLEndpoint", getPictureURLEndpoint());
                }
                if (userFriendlyUnitValues) {
                    m.put("startTime", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("duration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                    m.put("fadeIn", getFadeInAmount()!=null ? new DurationMilliseconds(getFadeInAmount()): null);
                } else {
                    m.put("startTime", getScheduledStartTime());
                    m.put("duration", getAuthorizedPlayingDuration());
                    m.put("fadeIn", getFadeInAmount());
                }
                m.put("friendlyCaption", getFriendlyCaption());
                return MarshallUtil.toSimpleJSON(m);
            }

            @Override
            public StringBuilder toXML() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel-label", getChannelLabel());
                m.put("channel-description", getChannelDescription());
                m.put("channel-url", getChannelURL());
                m.put("channel-tag", getChannelTag());
                m.put("playlist-id", getPlaylistID());
                m.put("playlist-label", getPlaylistLabel());
                m.put("playlist-description", getPlaylistDescription());
                m.put("programme-label", getProgrammeLabel());
                
                m.put("track-id",getTrackID());
                m.put("track-title",getTrackTitle());
                m.put("track-author",getTrackAuthor());
                m.put("track-album",getTrackAlbum());
                m.put("track-genre",getTrackGenre());
                m.put("track-date-of-release",getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                m.put("track-description",getTrackDescription());
                m.put("track-type",getTrackType());
                m.put("tag",getTag());
                m.put("track-publisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programme-rotation-count",Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    m.put("request-author",getRequestAuthor());
                    m.put("request-message",getRequestMessage());
                }
                
                m.put("track-format",getTrackFormat() );
                if (userFriendlyUnitValues) {
                    m.put("track-size",new SizeBytes(getTrackSize()));
                    m.put("track-duration",new DurationMilliseconds(getTrackDuration()));
                    m.put("track-bitrate",new BitrateBitsPerSecond(getTrackBitrate()));
                    m.put("track-samplerate",new SamplerateSamplesPerSecond(getTrackSamplerate()));
                } else {
                    m.put("track-size",getTrackSize());
                    m.put("track-duration",getTrackDuration());
                    m.put("track-bitrate",getTrackBitrate());
                    m.put("track-samplerate",getTrackSamplerate());
                }                
                m.put("track-channels",getTrackChannels());
                m.put("track-vbr",isTrackVBR());
                m.put("item-url-endpoint",getItemURLEndpoint());
                m.put("queue-id",getQueueID());
                
                m.put("pegi-rating-age", getPEGIRatingAge());
                m.put("pegi-rating-features", getPEGIRatingFeatures());
                if (hasPicture()) {
                    m.put("picture-format", getPictureFormat());
                    if (userFriendlyUnitValues) {
                        m.put("picture-size", getPictureSize() != null ? new SizeBytes(getPictureSize()) : null);
                    } else {
                        m.put("picture-size", getPictureSize());
                    }
                    m.put("picture-url-endpoint", getPictureURLEndpoint());
                }
                if (userFriendlyUnitValues) {
                    m.put("start-time", getScheduledStartTime() != null ? new YearMonthWeekDayHourMinuteSecondMillisecond(getScheduledStartTime().longValue(), getTimezone()) : null);
                    m.put("duration", getAuthorizedPlayingDuration()!=null ? new DurationMilliseconds(getAuthorizedPlayingDuration()) : null);
                    m.put("fade-in", getFadeInAmount()!=null ? new DurationMilliseconds(getFadeInAmount()) : null);
                } else {
                    m.put("start-time", getScheduledStartTime());
                    m.put("duration", getAuthorizedPlayingDuration());
                    m.put("fade-in", getFadeInAmount());
                }
                m.put("friendly-caption", getFriendlyCaption());
                return MarshallUtil.toSimpleXML("minimal-queue-item", m);
            }

            @Override
            public StringBuilder toLiquidsoapAnnotate() {
                Map<String, Object> m = new HashMap<String, Object>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description", getChannelDescription());
                m.put("channel_url", getChannelURL());
                m.put("channel_tag", getChannelTag());
                m.put("programme_id", getProgrammeLabel());
                m.put("playlist_id", getPlaylistID());
                m.put("playlist_label", getPlaylistLabel());
                m.put("playlist_description", getPlaylistDescription());
                
                m.put("track_id",getTrackID());
                m.put("title",getTrackTitle());
                m.put("artist",getTrackAuthor());
                m.put("album",getTrackAlbum());
                m.put("genre",getTrackGenre());
                m.put("date",getTrackDateOfRelease());
                m.put("comment",getTrackDescription());
                m.put("sub_type",getTrackType());
                m.put("tag",getTag());
                m.put("publisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programme_rotation_count",getProgrammeRotationCount());
                if (isRequested()) {
                    m.put("request_author",getRequestAuthor());
                    m.put("request_message",getRequestMessage());
                }
                
                m.put("track_format",getTrackFormat() );
                if (userFriendlyUnitValues) {
                    m.put("track_size",new SizeBytes(getTrackSize()));
                    m.put("track_duration",new DurationMilliseconds(getTrackDuration()));
                    m.put("track_bitrate",new BitrateBitsPerSecond(getTrackBitrate()));
                    m.put("track_samplerate",new SamplerateSamplesPerSecond(getTrackSamplerate()));
                } else {
                    m.put("track_size",getTrackSize());
                    m.put("track_duration",getTrackDuration());
                    m.put("track_bitrate",getTrackBitrate());
                    m.put("track_samplerate",getTrackSamplerate());
                }                
                m.put("track_channels",getTrackChannels());
                m.put("track_vbr",isTrackVBR());
                m.put("queue_id",getQueueID());
                
                m.put("pegi_rating_age", getPEGIRatingAge());
                m.put("pegi_rating_features", getPEGIRatingFeatures());
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format", getPictureFormat());
                    m.put("picture_size", getPictureSize());
                    m.put("picture_url_endpoint", getPictureURLEndpoint());
                }
                m.put("liq_queue_start_time", getScheduledStartTime());
                m.put("liq_cue_in", new Float(0.0));
                m.put("liq_cue_out", getAuthorizedPlayingDuration());
                m.put("liq_fade_in", getFadeInAmount() != null ? new Float(getFadeInAmount()/1000.0) : null);
                m.put("liq_fade_out", getFadeInAmount() != null ? new Float(getFadeInAmount()/500.0) : null); /* == liq_fade_in * 2*/
                m.put("liq_start_next", getFadeInAmount() != null ? new Float(getFadeInAmount()*0.003) : null); /* == liq_fade_in + liq_fade_out*/
                return MarshallUtil.toSimpleAnnotate(m, getItemURLEndpoint());
            }

            @Override
            public StringBuilder toLiquidsoapJSON() {
                Map<String, String> m = new HashMap<String, String>();
                m.put("type", getType());
                m.put("channel_id", getChannelLabel());
                m.put("channel_description", getChannelDescription());
                m.put("channel_url", getChannelURL());
                m.put("channel_tag", getChannelTag());
                
                m.put("programme_id", getProgrammeLabel());
                m.put("playlist_id", getPlaylistID());
                m.put("playlist_label", getPlaylistLabel());
                m.put("playlist_description", getPlaylistDescription());
                
                m.put("track_id",getTrackID());
                m.put("title",getTrackTitle());
                m.put("artist",getTrackAuthor());
                m.put("album",getTrackAlbum());
                m.put("genre",getTrackGenre());
                m.put("date",getTrackDateOfRelease() != null ? getTrackDateOfRelease().toString() : null);
                m.put("comment",getTrackDescription());
                m.put("sub_type",getTrackType());
                m.put("tag",getTag());
                m.put("publisher",getTrackPublisher());
                m.put("copyright",getCopyright());
                m.put("programme_rotation_count",Long.toString(getProgrammeRotationCount()));
                if (isRequested()) {
                    m.put("request_author",getRequestAuthor());
                    m.put("request_message",getRequestMessage());
                }
                
                m.put("track_format",getTrackFormat() );
                if (userFriendlyUnitValues) {
                    m.put("track_size",new SizeBytes(getTrackSize()).getFriendlyEnglishCaption());
                    m.put("track_duration",new DurationMilliseconds(getTrackDuration()).getFriendlyEnglishCaption());
                    m.put("track_bitrate",new BitrateBitsPerSecond(getTrackBitrate()).getFriendlyEnglishCaption());
                    m.put("track_samplerate",new SamplerateSamplesPerSecond(getTrackSamplerate()).getFriendlyEnglishCaption());
                } else {
                    m.put("track_size",Long.toString(getTrackSize()));
                    m.put("track_duration",Float.toString(getTrackDuration()));
                    m.put("track_bitrate",Long.toString(getTrackBitrate()));
                    m.put("track_samplerate",Long.toString(getTrackSamplerate()));
                }                
                m.put("track_channels",Byte.toString(getTrackChannels()));
                m.put("track_vbr",Boolean.toString(isTrackVBR()));
                m.put("item_url_endpoint",getItemURLEndpoint());
                m.put("queue_id",getQueueID());
                
                m.put("pegi_rating_age", getPEGIRatingAge() != null ? getPEGIRatingAge().toString() : null);
                m.put("pegi_rating_features", getPEGIRatingFeatures() != null ? MarshallUtil.toSimpleCSV(getPEGIRatingFeatures()).toString() : null);
                m.put("song", getFriendlyCaption());
                if (hasPicture()) {
                    m.put("picture_format", getPictureFormat());
                    m.put("picture_size", getPictureSize() != null ? getPictureSize().toString() : null);
                    m.put("picture_url_endpoint", getPictureURLEndpoint());
                }
                m.put("liq_queue_start_time", getScheduledStartTime()!=null ? getScheduledStartTime().toString() : null);
                m.put("liq_cue_in", (new Float(0.0)).toString());
                m.put("liq_cue_out", getAuthorizedPlayingDuration() != null ? getAuthorizedPlayingDuration().toString() : null);
                m.put("liq_fade_in", getFadeInAmount() != null ? new Float(getFadeInAmount()/1000.0).toString() : null);
                m.put("liq_fade_out", getFadeInAmount() != null ? new Float(getFadeInAmount()/500.0).toString() : null); /* == liq_fade_in * 2*/
                m.put("liq_start_next", getFadeInAmount() != null ? new Float(getFadeInAmount()*0.003).toString() : null); /* == liq_fade_in + liq_fade_out*/
                return MarshallUtil.toSimpleLiquidsoapJSON(m);
            }
        };
    }
    
}
