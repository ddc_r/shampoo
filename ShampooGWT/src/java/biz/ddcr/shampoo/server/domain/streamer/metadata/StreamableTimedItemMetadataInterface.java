/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

/**
 * Simple transfer object for webservices, with primitive types only
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface StreamableTimedItemMetadataInterface extends StreamableItemMetadataInterface {

    /**
     * Duration in seconds
     * Specify if the track or live should not be played out for longer than the value, the streamer does not have to blindly follow this attribute, it usage is not compulsory
     * Null means full length
     */
    public Float getAuthorizedPlayingDuration();

    /**
     * Scheduled start time
     * Unix Epoch timestamp
     * Null means undefined it could mean ASAP or up to the streamer
     * @return
     */
    public Long getScheduledStartTime();
    
}
