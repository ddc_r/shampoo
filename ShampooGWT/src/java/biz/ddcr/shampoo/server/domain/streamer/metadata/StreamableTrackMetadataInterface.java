/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.streamer.metadata;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface StreamableTrackMetadataInterface extends MinimalStreamableTrackMetadataInterface  {

    public String getTrackID();

    /** Track codec/container info:
        mp3,
        mp4_aac,
        native_flac,
        ogg_vorbis **/
    public String getTrackFormat();
    /** physical size in bytes; it's the full length of the track, header included */
    public long getTrackSize();
    /** length in seconds */
    public float getTrackDuration();
    /** bitrate in bps */
    public long getTrackBitrate();
    /** bitrate in hz */
    public int getTrackSamplerate();
    /** 1 for mono, 2 for stereo, can be more; shouldn't be 0 */
    public byte getTrackChannels();
    /** variable bit rate? otherwise could be constant or average for example */
    public boolean isTrackVBR();

}
