/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.streamer.metadata;

import biz.ddcr.shampoo.client.helper.errors.MalformedURIException;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.lang.text.StrSubstitutor;

/**
 *
 * Substitutes metadata variables within a string by values. Meant to be called by getItemURLEnpoint() in StreamableBlankQueueItemMetadataInterface or StreamableLiveQueueItemMetadataInterface.
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class URISchemeMetadataPatternResolver extends MetadataPatternResolver {

    /** enum that allows to compile at compile time the list of strings that are used within the switch/case of lookup() */
    private static enum MetadataEnum {
        queueID,
        playlistID,
        timetableSlotID,
        programmeLabel,
        channelLabel;
    }

    public URISchemeMetadataPatternResolver() {
    }

    @Override
    protected String resolveVariable(String variable) {
        if (variable!=null && variable.length()!=0) {
        //Resolve the parameter
            switch (MetadataEnum.valueOf(variable)) {
                case queueID: return getQueueID();
                case playlistID: return getPlaylistID();
                case timetableSlotID: return getTimetableSlotID();
                case programmeLabel: return getProgrammeLabel();
                case channelLabel: return getChannelLabel();
            }
        }
            return "";
    }

    public abstract String getQueueID();
    public abstract String getPlaylistID();
    public abstract String getTimetableSlotID();
    public abstract String getProgrammeLabel();
    public abstract String getChannelLabel();

    /** Automated procedure that checks whether the provided pattern is valid **/
    public static void check(String testPattern) {
        if (testPattern != null && testPattern.length() != 0) {
            StrSubstitutor friendlycaptionMaker = new StrSubstitutor(new URISchemeMetadataPatternResolver() {

                @Override
                public String getQueueID() {
                    return "test";
                }

                @Override
                public String getPlaylistID() {
                    return "test";
                }

                @Override
                public String getTimetableSlotID() {
                    return "test";
                }

                @Override
                public String getProgrammeLabel() {
                    return "test";
                }

                @Override
                public String getChannelLabel() {
                    return "test";
                }

            });
            String result = friendlycaptionMaker.replace(testPattern);
            if (result == null || result.length() == 0) {
                throw new IllegalArgumentException("Malformed pattern "+testPattern);
            } else {
                //We're not done yet, the reult URI must be well formed as well
                try {
                    new URI(result);
                } catch (URISyntaxException e) {
                    throw new MalformedURIException(testPattern);
                }
            }
        }
    }
}
