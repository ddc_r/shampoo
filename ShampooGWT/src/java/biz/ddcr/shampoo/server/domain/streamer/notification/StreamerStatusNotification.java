/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.streamer.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.CONTENT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.ContentUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class StreamerStatusNotification extends ContentUpdatedNotification<String> {

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case channel_reserved:
                    return "Channel "+getFriendlyEntityCaption()+" is reserved";
                case channel_free:
                    return "Channel "+getFriendlyEntityCaption()+" is free";
                case channel_onair:
                    return "Channel "+getFriendlyEntityCaption()+" is online";
                case channel_offair:
                    return "Channel "+getFriendlyEntityCaption()+" is offline";
            }
        }
        return null;
    }

    @Override
    public String getFriendlyEntityCaption() {
        return getEntityID();
    }

    public static enum STREAMER_STATUS_OPERATION implements CONTENT_NOTIFICATION_OPERATION {

        channel_reserved,
        channel_free,
        channel_onair,
        channel_offair;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, Channel channel) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {
                //Find recipients from the computed list of channels and programmes
                return securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                        Collections.singleton(channel),
                        channel.getProgrammes(),
                        Channel.getNotificationRights());
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private STREAMER_STATUS_OPERATION action;

    protected StreamerStatusNotification() {
        super();
    }

    @Override
    public StreamerStatusNotification shallowCopy() {
        return new StreamerStatusNotification(this);
    }
    
    public StreamerStatusNotification(STREAMER_STATUS_OPERATION action, Channel channel, RestrictedUser recipient) {
        super(channel!=null ? channel.getLabel() : null,
              recipient);
        setAction( action );
    }
    public StreamerStatusNotification(StreamerStatusNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public STREAMER_STATUS_OPERATION getAction() {
        return action;
    }

    protected void setAction(STREAMER_STATUS_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
