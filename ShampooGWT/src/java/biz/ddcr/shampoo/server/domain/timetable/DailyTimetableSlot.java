package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDay;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 * A daily-recurring Timetable slot for a given Channel
 *
 * @author okay_awright
 */
public class DailyTimetableSlot extends RecurringTimetableSlot {

    /** Do not use it: internal ORM placeholder **/
    private Long _endMillisecondFromPeriod;

    protected DailyTimetableSlot() {
    }

    public DailyTimetableSlot(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) {
        super(channel, startTime);
    }

    public DailyTimetableSlot(DailyTimetableSlot o) {
        super(o);
        setDecommissioningTimeWithCorrection(o.getDecommissioningCalendar());
        setEndTimeWithCorrection(o.getEndCalendar());
    }

    @Override
    protected YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar() {
        if (endCalendar == null && _endMillisecondFromPeriod != null) {
            //.switchTimeZone() is mandatory: One must be sure that we play with a common timezone since we directly compute differences with relative times
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(getStartCalendar().switchTimeZone(getTimeZone()));
            if (getStartMillisecondFromPeriod()>=_endMillisecondFromPeriod)
                endCalendar.addDays(1);
            endCalendar.addMilliseconds(_endMillisecondFromPeriod - getStartMillisecondFromPeriod());
        }
        return endCalendar;
    }

    @Override
    public Long getEndMillisecondFromPeriod() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endMillisecondFromPeriod;
        } else {
            final Long e = getEndCalendar().getMillisecondInDayPeriod();
            return e==null||e!=0 ? e : _getEndMillisecondFromPastPeriod();
        }
    }
    private Long _getEndMillisecondFromPastPeriod() {
        return getEndCalendar().getUnixTime() - (new YearMonthWeekDay(getStartCalendar())).getUnixTime();        
    }

    protected void setEndMillisecondFromPeriod(Long _endMillisecondFromPeriod) {
        this._endMillisecondFromPeriod = _endMillisecondFromPeriod;
    }

    @Override
    public Long getStartMillisecondFromPeriod() {
        return Long.valueOf(getStartCalendar().getMillisecondFromStartOfDay());
    }

    protected void setStartMillisecondFromPeriod(Long _startMillisecondFromPeriod) {
        //Do nothing, it's computed from startTime
    }

    @Override
    public Long getEndTimestamp() {
        return getEndCalendar().getUnixTime();
    }    
    
    @Override
    public DailyTimetableSlot shallowCopy() {
        return new DailyTimetableSlot(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be a timetable slot at this point
        DailyTimetableSlot test = ProxiedClassUtil.cast(obj, DailyTimetableSlot.class);
        return super.equals(test);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(TimetableSlotVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
