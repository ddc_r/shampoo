/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDay;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface;

/**
 *
 * @author okay_awright
 **/
public abstract class RecurringTimetableSlot extends TimetableSlot {

    //The decommissioning calendar may be null, i.e. the timetable slot repeats itself endlessly
    private YearMonthWeekDayInterface decommissioningCalendar;
    protected boolean canBeDecommissioned = false;    
    /** Do not use it: internal ORM placeholder **/
    protected Long _decommissioningUnixTimestamp;
    
    protected RecurringTimetableSlot() {
    }
    public RecurringTimetableSlot(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) {
        super(channel, startTime);
    }
    public RecurringTimetableSlot(RecurringTimetableSlot o) {
        super(o);
    }

    public abstract Long getStartMillisecondFromPeriod();
    public abstract Long getEndMillisecondFromPeriod();

    protected YearMonthWeekDayInterface getDecommissioningCalendar() {
        if (canBeDecommissioned) {
            //lazy loading
            if (decommissioningCalendar == null && _decommissioningUnixTimestamp != null) {
                decommissioningCalendar = fixDecommissioningCalendar(new YearMonthWeekDay(
                        _decommissioningUnixTimestamp,
                        getTimeZone()));
            }
            return decommissioningCalendar;
        }
        return null;
    }    
    
    protected YearMonthWeekDayInterface fixDecommissioningCalendar(YearMonthWeekDayInterface decommissioningCalendar) {
        return decommissioningCalendar;
    }
    
    protected Long getDecommissioningTimestamp() {
        if (getDecommissioningCalendar()!=null)
            return getDecommissioningCalendar().getUnixTime();
        else
            return null;
    }

    protected void setDecommissioningTimestamp(Long timestamp) {
        _decommissioningUnixTimestamp = timestamp;
        canBeDecommissioned = (_decommissioningUnixTimestamp!=null);
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     * Immutable result
     *
     **/
    public YearMonthWeekDayInterface getDecommissioningTime() {
        if (getDecommissioningCalendar() != null) {
            return new YearMonthWeekDay(
                    getDecommissioningCalendar().getYear(),
                    getDecommissioningCalendar().getMonthOfYear(),
                    getDecommissioningCalendar().getWeekOfMonth(),
                    getDecommissioningCalendar().getDayOfWeek(),
                    getDecommissioningCalendar().getTimeZone());
        } else {
            return null;
        }
    }

    
    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the current timezone
     *
     **/
    public boolean setDecommissioningTimeWithCorrection(YearMonthWeekDayInterface date) {
        //Check if decommissioning is greater than or equals to end
        if (date != null && date.compareTo(getEndCalendar()) >= 0) {
            //Cheap cloning
            decommissioningCalendar = fixDecommissioningCalendar(new YearMonthWeekDay(
                    date.getUnixTime(),
                    getTimeZone()));
            canBeDecommissioned = true;
            return true;
        } else {
            canBeDecommissioned = false;
            return false;
        }
    }

    /**
     * Makes this recurring event loops endlessly
     *
     **/
    public void unsetDecommissioningTime() {
        decommissioningCalendar = fixDecommissioningCalendar(null);
        canBeDecommissioned = false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setRawDecommissioningTime(YearMonthWeekDayInterface date) {
        //Check if decommissioning is greater than or equals to end
        if (date != null && date.compareTo(getEndCalendar()) >= 0) {
            decommissioningCalendar = fixDecommissioningCalendar(new YearMonthWeekDay(
                    date.getYear(),
                    date.getMonthOfYear(),
                    date.getWeekOfMonth(),
                    date.getDayOfWeek(),
                    getTimeZone()));
            canBeDecommissioned = true;
            return true;
        } else {
            canBeDecommissioned = false;
            return false;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be a timetable slot at this point
        RecurringTimetableSlot test = ProxiedClassUtil.cast(obj, RecurringTimetableSlot.class);
        return super.equals(test)
                //&& (playlist == test.playlist || (playlist != null && playlist.equals(test.playlist)))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

}
