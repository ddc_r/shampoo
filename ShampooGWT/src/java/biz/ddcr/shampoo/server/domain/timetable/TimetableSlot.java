/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.queue.SchedulableQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemInterface;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author okay_awright
 **/
public abstract class TimetableSlot extends GenericTimestampedEntity implements Serializable, Comparable<TimetableSlot>, VisitorPatternInterface<TimetableSlotVisitor> {

    private String refID;
    private TimetableSlotLocation location;
    /** The actual plylist bound to this slot; it can be null */
    private Playlist playlist;
    /** enforce a subset of playlists to be bound to this timetable slot only; it cannot be null and it must be a programme that is played by the channel contained in the identifier */
    private Programme programme;
    /** has the show been cancelled and should be queried when looking for something to air?
     * An animator should be granted the right to enable or disable a programme at will
     **/
    private boolean enabled;

    private StreamItemInterface streamItem;
    private SortedSet<SchedulableQueueItem> queueItems;

    protected YearMonthWeekDayHourMinuteSecondMillisecond endCalendar;
    
    protected TimetableSlot() {
    }

    public TimetableSlot(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) {
        //Channel must be set first, fixing the startTime requires a time zone, which comes from the channel
        addChannel(channel);
        setStartTimeWithCorrection(startTime);
    }

    public TimetableSlot(TimetableSlot o) {
        super(o);
        /*this._date = o._date;
        this._time = o._time;**/
        addProgramme(o.getProgramme());
        addChannel(o.getChannel());
        addPlaylist(o.getPlaylist());
        setStartTimeWithCorrection(o.getStartCalendar());
        setEndTimeWithCorrection(o.getEndCalendar());
        setEnabled(o.isEnabled());
        addQueueItems( o.getQueueItems() );
        addStreamItem( o.getStreamItem() );
    }


    protected YearMonthWeekDayHourMinuteSecondMillisecond getStartCalendar() {
        return getLocation().getStartCalendar();
    }

    protected abstract YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar();
    
    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    private void setLocation(TimetableSlotLocation location) {
        this.location = location;
    }

    protected TimetableSlotLocation getLocation() {
        if (location == null) {
            location = new TimetableSlotLocation();
        }
        return location;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    private void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public void addPlaylist(Playlist playlist) {
        addPlaylist(playlist, true);
    }

    public void addPlaylist(Playlist playlist, boolean doLink) {
        if (playlist != null) {
            if (doLink) {
                playlist.addTimetableSlot(this, false);
            }
            setPlaylist(playlist);
        }
    }

    public void removePlaylist() {
        removePlaylist(true);
    }

    public void removePlaylist(boolean doLink) {
        if (getPlaylist() != null) {
            if (doLink) {
                getPlaylist().removeTimetableSlot(this, false);
            }
            setPlaylist(null);
        }
    }
    
    public Long getStartTimestamp() {
        return getLocation().getStartTimestamp();
    }
    public abstract Long getEndTimestamp();
    
    public YearMonthWeekDayHourMinuteSecondMillisecond getStartTime() {
        return getLocation().getStartTime();
    }

    public boolean setStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setStartTimeWithCorrection(date);
    }

    public boolean setRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return getLocation().setRawStartTime(date);
    }
    
    public YearMonthWeekDayHourMinuteSecondMillisecond getEndTime() {
        //cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getEndCalendar().getYear(),
                getEndCalendar().getMonthOfYear(),
                getEndCalendar().getDayOfMonth(),
                getEndCalendar().getHourOfDay(),
                getEndCalendar().getMinuteOfHour(),
                getEndCalendar().getSecondOfMinute(),
                getEndCalendar().getMillisecondOfSecond(),
                getEndCalendar().getTimeZone());
    }

     /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the current timezone
     *
     **/
    public boolean setEndTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Check if end is greater than start
        if (date != null && date.compareTo(getStartCalendar()) > 0) {
            //Cheap cloning
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    getTimeZone());
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     * i.e. the time zone meta info contained within the argument object is discarded
     *
     **/
    public boolean setRawEndTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Check if end is greater than start
        if (date != null && date.compareTo(getStartCalendar()) > 0) {
            getEndCalendar().setYear(date.getYear());
            getEndCalendar().setMonthOfYear(date.getMonthOfYear());
            getEndCalendar().setDayOfMonth(date.getDayOfMonth());
            getEndCalendar().setHourOfDay(date.getHourOfDay());
            getEndCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getEndCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getEndCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    public Programme getProgramme() {
        return programme;
    }

    private void setProgramme(Programme programme) {
        this.programme = programme;
    }

    public void addProgramme(Programme programme) {
        addProgramme(programme, true);
    }

    public void addProgramme(Programme programme, boolean doLink) {
        if (programme != null) {
            if (doLink) {
                programme.addTimetableSlot(this, false);
            }
            setProgramme(programme);
        }
    }

    public void removeProgramme() {
        removeProgramme(true);
    }

    public void removeProgramme(boolean doLink) {
        if (getProgramme() != null) {
            if (doLink) {
                getProgramme().removeTimetableSlot(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setProgramme(null);
        }
    }

    public Channel getChannel() {
        return getLocation().getChannel();
    }

    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            if (doLink) {
                channel.addTimetableSlot(this, false);
            }
            getLocation().setChannel(channel);
        }
    }

    public void removeChannel() {
        removeChannel(true);
    }

    public void removeChannel(boolean doLink) {
        if (getLocation().getChannel() != null) {
            if (doLink) {
                getLocation().getChannel().removeTimetableSlot(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setChannel(null);
        }
    }

    public SortedSet<SchedulableQueueItem> getQueueItems() {
        if (queueItems == null) {
            queueItems = new TreeSet<SchedulableQueueItem>();
        }
        return queueItems;
    }

    private void setQueueItems(SortedSet<SchedulableQueueItem> items) {
        this.queueItems = items;
    }

    public void addQueueItem(SchedulableQueueItem queueItem) {
        addQueueItem(queueItem, true);
    }

    public void addQueueItem(SchedulableQueueItem queueItem, boolean doLink) {
        if (queueItem != null) {
            getQueueItems().add(queueItem);
            if (doLink) {
                queueItem.addTimetableSlot(this, false);
            }
        }
    }

    public void addQueueItems(Set<SchedulableQueueItem> queueItems) {
        if (queueItems != null) {
            for (SchedulableQueueItem queueItem : queueItems) {
                addQueueItem(queueItem);
            }
        }
    }

    public void removeQueueItem(SchedulableQueueItem queueItem) {
        removeQueueItem(queueItem, true);
    }

    public void removeQueueItem(SchedulableQueueItem queueItem, boolean doLink) {
        if (queueItem != null) {
            getQueueItems().remove(queueItem);
            if (doLink) {
                queueItem.removeTimetableSlot(false);
            }
        }
    }

    public void removeQueueItems(Set<SchedulableQueueItem> queueItems) {
        if (queueItems != null) {
            for (SchedulableQueueItem queueItem : queueItems) {
                removeQueueItem(queueItem);
            }
        }
    }

    public void clearQueueItems() {
        for (SchedulableQueueItem queueItem : getQueueItems()) {
            queueItem.removeTimetableSlot(false);
        }
        getQueueItems().clear();
    }

    public String getTimeZone() {
        return getChannel().getTimezone();
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean active) {
        enabled = active;
    }

    /**
     * Length between the start and end time, computed in milliseconds
     * @return
     **/
    public long getLengthInMilliseconds() {
        return (getEndCalendar().getUnixTime() - getStartCalendar().getUnixTime());
    }

    public boolean setLengthInMilliseconds(long length) {
        //TODO sync this with streamer getMinDuration()
        if (length > 0) {
            //Cheap-cloning
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(getStartCalendar());
            endCalendar.addMilliseconds(length);
            return true;
        }
        return false;
    }

    public StreamItemInterface getStreamItem() {
        return streamItem;
    }
    protected void setStreamItem(StreamItemInterface streamItem) {
        this.streamItem = streamItem;
    }

    public void addStreamItem(StreamItemInterface streamItem) {
        addStreamItem(streamItem, true);
    }

    public void addStreamItem(StreamItemInterface streamItem, boolean doLink) {
        if (streamItem != null) {
            setStreamItem(streamItem);
            if (doLink) {
                streamItem.addTimetableSlot(this, false);
            }
        }
    }

    public void removeStreamItem() {
        removeStreamItem(true);
    }

    public void removeStreamItem(boolean doLink) {
        if (getStreamItem() != null) {
            if (doLink) {
                getStreamItem().removeTimetableSlot(false);
            }
            //Put delete-orphan in the hibernate mapping
            setStreamItem(null);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        TimetableSlot test = ProxiedClassUtil.cast(obj, TimetableSlot.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }
        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getLocation() ? 0 : getLocation().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(TimetableSlot anotherTimetableSlot) {
        //Sort by date, then by minute
        if (anotherTimetableSlot!=null)
            return (this.getLocation() == null ?
                0 :
                (anotherTimetableSlot.getLocation() == null ?
                    0 :
                    getLocation().compareTo(anotherTimetableSlot.getLocation()) ));
        else
            return 0;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setListenerRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }
    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
