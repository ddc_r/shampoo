package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;

/**
 * The SpecialPlaylist TimetableSlot for a given Channel
 * SpecialPlaylists can be freely scheduled
 *
 * @author okay_awright
 */
public class TimetableSlotLocation implements Serializable, Comparable<TimetableSlotLocation> {

    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _startUnixTimestamp;
    /** Where this slot will be played */
    private Channel channel;

    protected TimetableSlotLocation() {
    }

    public TimetableSlotLocation(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecond date) {
        //Channel first, since setting time requires the channel timezone
        setChannel(channel);
        setStartTimeWithCorrection(date);
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getTimeZone() {
        return getChannel().getTimezone();
    }

    protected Long getStartTimestamp() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _startUnixTimestamp;
        } else {
            return getStartCalendar().getUnixTime();
        }
    }

    protected void setStartTimestamp(Long unixTimestamp) {
        _startUnixTimestamp = unixTimestamp;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getStartCalendar() {
        if (startCalendar == null && _startUnixTimestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _startUnixTimestamp,
                    getTimeZone());
        }
        return startCalendar;
    }
    
    /**
     *
     * Specifies when this timetable slot is scheduled
     * Immutable result
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecond getStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getStartCalendar().getYear(),
                getStartCalendar().getMonthOfYear(),
                getStartCalendar().getDayOfMonth(),
                getStartCalendar().getHourOfDay(),
                getStartCalendar().getMinuteOfHour(),
                getStartCalendar().getSecondOfMinute(),
                getStartCalendar().getMillisecondOfSecond(),
                getStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the current timezone
     *
     **/
    public boolean setStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    getTimeZone());
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getStartCalendar().setYear(date.getYear());
            getStartCalendar().setMonthOfYear(date.getMonthOfYear());
            getStartCalendar().setDayOfMonth(date.getDayOfMonth());
            getStartCalendar().setHourOfDay(date.getHourOfDay());
            getStartCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getStartCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getStartCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        TimetableSlotLocation test = ProxiedClassUtil.cast(obj, TimetableSlotLocation.class);
        return /*super.equals(test)
                &&*/ (getChannel() == test.getChannel() || (getChannel() != null && getChannel().equals(test.getChannel())))
                && (getStartCalendar() == test.getStartCalendar() || (getStartCalendar() != null && getStartCalendar().equals(test.getStartCalendar())));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getChannel() ? 0 : getChannel().hashCode());
        hash = 31 * hash + (null == getStartCalendar() ? 0 : getStartCalendar().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(TimetableSlotLocation anotherTimetableSlotLocation) {
        //Sort by time
        if (anotherTimetableSlotLocation!=null)
            return (this.getStartCalendar() == null ?
                0 :
                (anotherTimetableSlotLocation.getStartCalendar() == null ?
                    0 :
                    getStartCalendar().compareTo(anotherTimetableSlotLocation.getStartCalendar()) ));
        else
            return 0;
    }
}
