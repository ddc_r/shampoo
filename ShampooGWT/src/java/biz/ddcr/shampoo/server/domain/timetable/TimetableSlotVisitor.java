/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface.Visitor;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface TimetableSlotVisitor extends Visitor {

    public void visit(UniqueTimetableSlot timetableSlot) throws Exception;
    public void visit(DailyTimetableSlot timetableSlot) throws Exception;
    public void visit(WeeklyTimetableSlot timetableSlot) throws Exception;
    public void visit(MonthlyTimetableSlot timetableSlot) throws Exception;
    public void visit(YearlyTimetableSlot timetableSlot) throws Exception;

}
