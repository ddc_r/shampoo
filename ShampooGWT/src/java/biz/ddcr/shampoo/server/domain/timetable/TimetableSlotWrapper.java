/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.io.Serializable;

/**
 *
 * A simple wrapper for recurring slots and a computed absolute start time.
 * Convenience wrapper only for unique slots.
 * This is not a real domain object, since it's never persisted in the database
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TimetableSlotWrapper<T extends TimetableSlot> implements Serializable, VisitorPatternInterface<TimetableSlotVisitor> {
    
    private T slot;
    private YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime;
    
    public TimetableSlotWrapper(T slot, YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime) {
        this.slot = slot;
        this.absoluteStartTime = absoluteStartTime;
    }

    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getAbsoluteStartTime() {
        return absoluteStartTime;
    }

    public T getSlot() {
        return slot;
    }

    @Override
    public void acceptVisit(TimetableSlotVisitor visitor) throws Exception {
        slot.acceptVisit(visitor);
    }
    
}
