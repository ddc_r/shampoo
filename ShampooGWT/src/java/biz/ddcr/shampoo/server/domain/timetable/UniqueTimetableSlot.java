package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;


/**
 * A not-recurring Timetable slot for a given Channel
 *
 * @author okay_awright
 */
public class UniqueTimetableSlot extends TimetableSlot {

    /** Do not use it: internal ORM placeholder **/
    private Long _endUnixTimestamp;

    protected UniqueTimetableSlot() {
    }
    public UniqueTimetableSlot(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) {
        super(channel, startTime);
    }
    public UniqueTimetableSlot(UniqueTimetableSlot o) {
        super(o);
        setEndTimeWithCorrection(o.getEndCalendar());
    }

    @Override
    public Long getEndTimestamp() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endUnixTimestamp;
        } else {
            return getEndCalendar().getUnixTime();
        }
    }

    protected void setEndTimestamp(Long unixTimestamp) {
        _endUnixTimestamp = unixTimestamp;
    }
    
    @Override
    protected YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar() {
        if (endCalendar == null && _endUnixTimestamp != null) {
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _endUnixTimestamp,
                    getTimeZone());
        }
        return endCalendar;
    }

    @Override
    public UniqueTimetableSlot shallowCopy() {
        return new UniqueTimetableSlot(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        UniqueTimetableSlot test = ProxiedClassUtil.cast(obj, UniqueTimetableSlot.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(TimetableSlotVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
