package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.YearMonthWeek;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface;


/**
 * A daily-recurring Timetable slot for a given Channel
 *
 * @author okay_awright
 */
public class WeeklyTimetableSlot extends DailyTimetableSlot {

    /** Do not use it: internal ORM placeholder **/
    private Long _endMillisecondFromPeriod;

    protected WeeklyTimetableSlot() {
    }
    public WeeklyTimetableSlot(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) {
        super(channel, startTime);
    }
    public WeeklyTimetableSlot(WeeklyTimetableSlot o) {
        super(o);
    }

    public byte getStartDayOfWeek() {
        return getStartCalendar().getDayOfWeek();
    }

    protected void setStartDayOfWeek(byte dayOfWeek) {
        //Do nothing
    }

    public byte getEndDayOfWeek() {
        return getEndCalendar().getDayOfWeek();
    }

    protected void setEndDayOfWeek(byte dayOfWeek) {
        //Do nothing
    }
    
    @Override
    protected YearMonthWeekDayInterface fixDecommissioningCalendar(YearMonthWeekDayInterface decommissioningCalendar) {
        /*if (decommissioningCalendar!=null) {
            //Just make sure there is no day in yearmonthweekday
            decommissioningCalendar.setDayOfWeek((byte)1);
        }*/
        return decommissioningCalendar;
    }

    @Override
    protected YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar() {
        if (endCalendar == null && _endMillisecondFromPeriod != null) {
            //.switchTimeZone() is mandatory: One must be sure that we play with a common timezone since we directly compute differences with relative times
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(getStartCalendar().switchTimeZone(getTimeZone()));
            if (getStartMillisecondFromPeriod()>=_endMillisecondFromPeriod)
                endCalendar.addWeeks(1);
            endCalendar.addMilliseconds(_endMillisecondFromPeriod - getStartMillisecondFromPeriod());
        }
        return endCalendar;
    }

    @Override
    public Long getEndMillisecondFromPeriod() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endMillisecondFromPeriod;
        } else {
            final Long e = getEndCalendar().getMillisecondInWeekPeriod();
            return e==null||e!=0 ? e : _getEndMillisecondFromPastPeriod();
        }
    }
    private Long _getEndMillisecondFromPastPeriod() {
        return getEndCalendar().getUnixTime() - (new YearMonthWeek(getStartCalendar())).getUnixTime();        
    }

    @Override
    protected void setEndMillisecondFromPeriod(Long _endMillisecondFromPeriod) {
        this._endMillisecondFromPeriod = _endMillisecondFromPeriod;
    }

    @Override
    public Long getStartMillisecondFromPeriod() {
        return getStartCalendar().getMillisecondInWeekPeriod();
    }
    
    @Override
    public WeeklyTimetableSlot shallowCopy() {
        return new WeeklyTimetableSlot(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be a timetable slot at this point
        WeeklyTimetableSlot test = ProxiedClassUtil.cast(obj, WeeklyTimetableSlot.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(TimetableSlotVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
