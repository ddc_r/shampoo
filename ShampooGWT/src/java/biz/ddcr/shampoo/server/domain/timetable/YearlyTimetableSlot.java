package biz.ddcr.shampoo.server.domain.timetable;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.Year;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface;

/**
 * A daily-recurring Timetable slot for a given Channel
 *
 * @author okay_awright
 */
public class YearlyTimetableSlot extends MonthlyTimetableSlot {

    /** Do not use it: internal ORM placeholder **/
    private Long _endMillisecondFromPeriod;

    protected YearlyTimetableSlot() {
    }
    public YearlyTimetableSlot(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) {
        super(channel, startTime);
    }
    public YearlyTimetableSlot(YearlyTimetableSlot o) {
        super(o);
    }

    public byte getStartMonth() {
        return getStartCalendar().getMonthOfYear();
    }

    protected void setStartMonth(byte month) {
        //Do nothing
    }

    public byte getEndMonth() {
        return getEndCalendar().getMonthOfYear();
    }

    protected void setEndMonth(byte month) {
        //Do nothing
    }
    
    @Override
    protected YearMonthWeekDayInterface fixDecommissioningCalendar(YearMonthWeekDayInterface decommissioningCalendar) {
        if (decommissioningCalendar!=null) {
            //Just make sure there is no day, week, and month in yearmonthweekday
            decommissioningCalendar.setMonthOfYear((byte)1);
            decommissioningCalendar.setWeekOfMonth((byte)1);
            decommissioningCalendar.setDayOfWeek((byte)1);
        }
        return decommissioningCalendar;
    }

    @Override
    protected YearMonthWeekDayHourMinuteSecondMillisecond getEndCalendar() {
        if (endCalendar == null && _endMillisecondFromPeriod != null) {
            //.switchTimeZone() is mandatory: One must be sure that we play with a common timezone since we directly compute differences with relative times
            endCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(getStartCalendar().switchTimeZone(getTimeZone()));
            if (getStartMillisecondFromPeriod()>=_endMillisecondFromPeriod)
                endCalendar.addYears(1);
            endCalendar.addMilliseconds(_endMillisecondFromPeriod - getStartMillisecondFromPeriod());
        }
        return endCalendar;
    }

    @Override
    public Long getEndMillisecondFromPeriod() {
        //Beware of Hibernate trying to using this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (endCalendar == null) {
            return _endMillisecondFromPeriod;
        } else {
            final Long e = getEndCalendar().getMillisecondInYearPeriod();
            return e==null||e!=0 ? e : _getEndMillisecondFromPastPeriod();
        }
    }
    private Long _getEndMillisecondFromPastPeriod() {
        return getEndCalendar().getUnixTime() - (new Year(getStartCalendar())).getUnixTime();        
    }

    @Override
    protected void setEndMillisecondFromPeriod(Long _endMillisecondFromPeriod) {
        this._endMillisecondFromPeriod = _endMillisecondFromPeriod;
    }

    @Override
    public Long getStartMillisecondFromPeriod() {
        return getStartCalendar().getMillisecondInYearPeriod();
    }

    @Override
    public YearlyTimetableSlot shallowCopy() {
        return new YearlyTimetableSlot(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be a timetable slot at this point
        YearlyTimetableSlot test = (YearlyTimetableSlot)obj;
        return super.equals(test);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(TimetableSlotVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
