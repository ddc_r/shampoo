/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.timetable.log;

import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import java.util.HashSet;
import java.util.Set;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TimetableLog extends Log {

    public static enum TIMETABLE_LOG_OPERATION implements LOG_OPERATION {

        edit,
        delete,
        add,
        clone,
        shift,
        resize,
        cancel,
        confirm;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case add:
                        return systemConfigurationHelper.isLogTimetableCreate();
                    case edit:
                        return systemConfigurationHelper.isLogTimetableUpdate();
                    case delete:
                        return systemConfigurationHelper.isLogTimetableDelete();
                    case clone:
                        return systemConfigurationHelper.isLogTimetableClone();
                    case shift:
                        return systemConfigurationHelper.isLogTimetableShift();
                    case resize:
                        return systemConfigurationHelper.isLogTimetableResize();
                    case cancel:
                        return systemConfigurationHelper.isLogTimetableCancel();
                    case confirm:
                        return systemConfigurationHelper.isLogTimetableConfirm();
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, TimetableSlot timetableSlot) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (timetableSlot!=null && canLog(systemConfigurationHelper))
                allBoundChannels.add(timetableSlot.getChannel());
            return allBoundChannels;
        }
    }
    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** The current operation performed on the actee **/
    private TIMETABLE_LOG_OPERATION action;
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;
    /** Where this slot will be played */
    private String channelCaption;

    protected TimetableLog() {
        super();
    }

    @Override
    public TimetableLog shallowCopy() {
        return new TimetableLog(this);
    }
    
    public TimetableLog(TIMETABLE_LOG_OPERATION operation, String refID, YearMonthWeekDayHourMinuteSecondMillisecond startTime, String channelId, Set<Channel> channels) {
        super(refID, channels);
        setAction(operation);
        setSlotChannelCaption(channelId);
        setSlotStartTimeWithCorrection(startTime);
    }

    public TimetableLog(TimetableLog o) {
        super(o);
        setAction(o.getAction());
        setSlotChannelCaption(o.getSlotChannelCaption());
        setSlotRawStartTime(o.getSlotStartTime());
    }

    @Override
    public TIMETABLE_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(TIMETABLE_LOG_OPERATION action) {
        this.action = action;
    }

    public String getSlotChannelCaption() {
        return channelCaption;
    }

    public void setSlotChannelCaption(String channelCaption) {
        this.channelCaption = channelCaption;
    }

    protected Long getSlotStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _timestamp;
        } else {
            return getSlotStartCalendar().getUnixTime();
        }
    }

    protected void setSlotStartTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getSlotStartCalendar() {
        if (startCalendar == null && _timestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecond getSlotStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getSlotStartCalendar().getYear(),
                getSlotStartCalendar().getMonthOfYear(),
                getSlotStartCalendar().getDayOfMonth(),
                getSlotStartCalendar().getHourOfDay(),
                getSlotStartCalendar().getMinuteOfHour(),
                getSlotStartCalendar().getSecondOfMinute(),
                getSlotStartCalendar().getMillisecondOfSecond(),
                getSlotStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    public boolean setSlotStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecond date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will NOT be re-interpreted in the current timezone and be used as-is
     *
     **/
    public boolean setSlotRawStartTime(YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        if (date != null) {
            getSlotStartCalendar().setYear(date.getYear());
            getSlotStartCalendar().setMonthOfYear(date.getMonthOfYear());
            getSlotStartCalendar().setDayOfMonth(date.getDayOfMonth());
            getSlotStartCalendar().setHourOfDay(date.getHourOfDay());
            getSlotStartCalendar().setMinuteOfHour(date.getMinuteOfHour());
            getSlotStartCalendar().setSecondOfMinute(date.getSecondOfMinute());
            getSlotStartCalendar().setMillisecondOfSecond(date.getMillisecondOfSecond());
            return true;
        }
        return false;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
