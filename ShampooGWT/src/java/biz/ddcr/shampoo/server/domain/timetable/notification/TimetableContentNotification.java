/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.timetable.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.CONTENT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.ContentUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TimetableContentNotification extends ContentUpdatedNotification<String> {

    @Override
    public String getFriendlyEntityCaption() {
        return getSlotStartCalendar().getFriendlyEnglishCaption() + " @ " + getSlotChannelCaption() + " [" +getEntityID() + "]";
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case edit:
                    return "Timetable slot "+getEntityID()+" has been edited";
                case add:
                    return "Timetable slot "+getEntityID()+" has been added";
                case delete:
                    return "Timetable slot "+getEntityID()+" has been deleted";
            }
        }
        return null;
    }

    public static enum TIMETABLE_NOTIFICATION_OPERATION implements CONTENT_NOTIFICATION_OPERATION {

        add,
        delete,
        edit;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, TimetableSlot timetableSlot) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {
                //Find recipients from the computed list of channels and programmes
                Collection<Channel> channels = new HashSet<Channel>();
                    channels.add(timetableSlot.getChannel());
                    if (timetableSlot.getProgramme()!=null)
                        channels.addAll(timetableSlot.getProgramme().getChannels());
                return securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                        channels,
                        Collections.singleton(timetableSlot.getProgramme()),
                        TimetableSlot.getNotificationRights());
            }
            return new HashSet<RestrictedUser>();
        }
    }

    //Timezone defaults to the JVM's own
    private final static String LOG_TIMEZONE = DateTimeZone.getDefault().getID();
    /** friendly captions to interpret the raw ID**/
    private YearMonthWeekDayHourMinuteSecondMillisecond startCalendar;
    /** Do not use it: internal ORM placeholder **/
    private Long _timestamp;
    /** Where this slot will be played */
    private String slotChannelCaption;
    private TIMETABLE_NOTIFICATION_OPERATION action;

    protected TimetableContentNotification() {
        super();
    }

    @Override
    public TimetableContentNotification shallowCopy() {
        return new TimetableContentNotification(this);
    }
    
    public TimetableContentNotification(TIMETABLE_NOTIFICATION_OPERATION action, TimetableSlot timetableSlot, RestrictedUser recipient) {
        super(timetableSlot!=null ? timetableSlot.getRefID() : null,
              recipient);
        setAction( action );
        setSlotStartTimeWithCorrection(timetableSlot!=null ? timetableSlot.getStartTime() : null);
        setSlotChannelCaption(timetableSlot!=null ? (timetableSlot.getChannel()!=null ? timetableSlot.getChannel().getLabel() : null) : null);
    }

    public TimetableContentNotification(TimetableContentNotification o) {
        super(o);
        setAction(o.getAction());
        setSlotStartTimeWithCorrection( o.getSlotStartTime() );
        setSlotChannelCaption( o.getSlotChannelCaption() );
    }

    @Override
    public TIMETABLE_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(TIMETABLE_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getSlotChannelCaption() {
        return slotChannelCaption;
    }

    protected void setSlotChannelCaption(String slotChannelCaption) {
        this.slotChannelCaption = slotChannelCaption;
    }

    protected Long getSlotStartTimestamp() {
        //Beware of Hibernate trying to use this getter just after setting it up: in this case only output what Hibernate is expecting
        //This is tricky but should be foolproof
        if (startCalendar == null) {
            return _timestamp;
        } else {
            return getSlotStartCalendar().getUnixTime();
        }
    }

    protected void setSlotStartTimestamp(Long unixEpoch) {
        _timestamp = unixEpoch;
    }

    protected YearMonthWeekDayHourMinuteSecondMillisecond getSlotStartCalendar() {
        if (startCalendar == null && _timestamp != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    _timestamp,
                    LOG_TIMEZONE);
        }
        return startCalendar;
    }

    /**
     *
     * Specifies when this timetable slot is scheduled
     *
     **/
    public YearMonthWeekDayHourMinuteSecondMillisecond getSlotStartTime() {
        //Cheap cloning
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getSlotStartCalendar().getYear(),
                getSlotStartCalendar().getMonthOfYear(),
                getSlotStartCalendar().getDayOfMonth(),
                getSlotStartCalendar().getHourOfDay(),
                getSlotStartCalendar().getMinuteOfHour(),
                getSlotStartCalendar().getSecondOfMinute(),
                getSlotStartCalendar().getMillisecondOfSecond(),
                getSlotStartCalendar().getTimeZone());
    }

    /**
     *
     * Specifies at what time this timetable slot is scheduled
     * The date passed as a parameter will be corrected to the UTC timezone
     *
     **/
    protected boolean setSlotStartTimeWithCorrection(YearMonthWeekDayHourMinuteSecondMillisecond date) {
        //Cheap cloning
        if (date != null) {
            startCalendar = new YearMonthWeekDayHourMinuteSecondMillisecond(
                    date.getUnixTime(),
                    LOG_TIMEZONE);
            return true;
        }
        return false;
    }

}
