package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.util.Set;

/**
 * A BroadcastableJingle track
 *
 * @author okay_awright
 */
public class BroadcastableJingle extends BroadcastableTrack {
    
    public BroadcastableJingle() {
    }
    public BroadcastableJingle(BroadcastableJingle o) throws Exception {
        super(o);
    }
    
    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Voting> getVotes() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setVotes(Set<Voting> ratings) {
        //Do nothing
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Request> getRequests() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setRequests(Set<Request> requests) {
        //Do nothing
    }

    @Override
    public BroadcastableJingle shallowCopy() throws Exception {
        return new BroadcastableJingle(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        BroadcastableJingle test = ProxiedClassUtil.cast(obj, BroadcastableJingle.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        return hash;
    }

    @Override
    public void is(PlayableItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public void acceptVisit(TrackVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
