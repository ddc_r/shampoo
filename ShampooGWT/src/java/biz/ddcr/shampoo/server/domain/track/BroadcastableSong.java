package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A BroadcastableSong track
 *
 * @author okay_awright
 */
public class BroadcastableSong extends BroadcastableTrack {

    /** protected, not private, for easier Hibernate inheritance **/
    protected Set<Request> requests;
    
    private Set<Voting> votes;
    /** internal dynamic value stored to avoid computing a sum over ratings each time the average rating must be read */
    private Long sumOfVotes;

    public BroadcastableSong() {
    }

    public BroadcastableSong(BroadcastableSong o) throws Exception {
        super(o);
        addVotes(o.getVotes());
    }
   
    private long _computeSumOfVotes() {
        long total = 0;
        for (Voting rating : getVotes()) {
            total += rating.getValue().getNumericScore();
        }
        return total;
    }

    private long _adjustSumOfVotes(int newValue) {
        if (sumOfVotes == null) {
            sumOfVotes = _computeSumOfVotes();
        }
        return sumOfVotes += newValue;
    }

    public Float getAverageVote() {
        //Lazily recompute the average score only when needed
        if (getVotes().isEmpty()) {
            return null;
        }
        if (sumOfVotes == null) {
            sumOfVotes = _computeSumOfVotes();
        }
        return (float) sumOfVotes / (float) (getVotes().size());
    }

    public Set<Voting> getVotes() {
        if (votes == null) {
            votes = new HashSet<Voting>();
        }
        return votes;
    }

    protected void setVotes(Set<Voting> votes) {
        this.votes = votes;
    }

    public void addVote(Voting vote) {
        addVote(vote, true);
    }

    public void addVote(Voting vote, boolean doLink) {
        if (vote != null) {
            if (getVotes().add(vote)) //Reinit the average temp value so that it will be lazily recomputed when needed
            {
                _adjustSumOfVotes(vote.getValue().getNumericScore());
            }
            if (doLink) {
                vote.addSong(this, false);
            }
        }
    }

    public void addVotes(Set<Voting> votes) {
        if (votes != null) {
            for (Voting vote : votes) {
                addVote(vote);
            }
        }
    }

    public void removeVote(Voting vote) {
        removeVote(vote, true);
    }

    public void removeVote(Voting vote, boolean doLink) {
        if (vote != null) {            
            if (vote.drop(true, doLink)) //Reinit the average temp value so that it will be lazily recomputed when needed
            {
                _adjustSumOfVotes(-vote.getValue().getNumericScore());
            }
        }
    }

    public void removeVotes(Set<Voting> votes) {
        if (votes != null) {
            for (Voting vote : votes) {
                removeVote(vote);
            }
        }
    }

    public void clearVotes() {
        for (Iterator<Voting> i = getVotes().iterator();i.hasNext();) {
            Voting vote = i.next();
            i.remove();
            //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
            //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
            vote.drop(true, false);
        }
        //Reinit the average temp value so that it will be lazily recomputed when needed
        sumOfVotes = null;
    }

    public Set<Request> getRequests() {
        if (requests == null) {
            requests = new HashSet<Request>();
        }
        return requests;
    }

    protected void setRequests(Set<Request> requests) {
        this.requests = requests;
    }

    public void addRequest(Request request) {
        addRequest(request, true);
    }

    public void addRequest(Request request, boolean doLink) {
        if (request != null) {
            getRequests().add(request);
            if (doLink) {
                request.addSong(this, false);
            }
        }
    }

    public void addRequests(Set<Request> requests) {
        if (requests != null) {
            for (Request request : requests) {
                addRequest(request);
            }
        }
    }

    public void removeRequest(Request request) {
        removeRequest(request, true);
    }

    public void removeRequest(Request request, boolean doLink) {
        if (request != null) {
            /*getRequests().remove(request);
            if (doLink) {
                request.removeSong(false);
            }*/
            request.drop(true, doLink, true);
        }
    }

    public void removeRequests(Set<Request> requests) {
        if (requests != null) {
            for (Request request : requests) {
                removeRequest(request);
            }
        }
    }

    public void clearRequests() {
        for (Iterator<Request> i = getRequests().iterator();i.hasNext();) {
            Request request = i.next();
            i.remove();
            //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
            //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
            request.drop(true, false, true);
        }
    }

    @Override
    public BroadcastableSong shallowCopy() throws Exception {
        return new BroadcastableSong(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        BroadcastableSong test = ProxiedClassUtil.cast(obj, BroadcastableSong.class);
        return super.equals(test);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        return hash;
    }

    @Override
    public void is(PlayableItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public void acceptVisit(TrackVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
