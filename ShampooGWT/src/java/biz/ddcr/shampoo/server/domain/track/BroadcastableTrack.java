package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.playlist.StaticPlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.domain.queue.SimpleTrackQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.SimpleTrackQueueStreamItem;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * An actual file to be broadcast and that has been approved by a curator
 *
 * @author okay_awright
 */
public abstract class BroadcastableTrack extends Track implements PlayableItemInterface {

    private boolean enabled;
    private SortedSet<StaticPlaylistEntry> slots;

    private SortedSet<SimpleTrackQueueItem> queueItems;
    private Set<SimpleTrackQueueStreamItem> streamItems;

    public BroadcastableTrack() {
    }

    public BroadcastableTrack(BroadcastableTrack o) throws Exception {
        super(o);
        setEnabled(o.isEnabled());
        addSlots(o.getSlots());
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public SortedSet<StaticPlaylistEntry> getSlots() {
        if (slots == null) {
            slots = new TreeSet<StaticPlaylistEntry>();
        }
        return slots;
    }

    private void setSlots(SortedSet<StaticPlaylistEntry> slots) {
        this.slots = slots;
    }

    public void addSlot(StaticPlaylistEntry slot) {
        addSlot(slot, true);
    }

    public void addSlot(StaticPlaylistEntry slot, boolean doLink) {
        if (slot != null) {
            getSlots().add(slot);
            if (doLink) {
                slot.addTrack(this, false);
            }
        }
    }

    public void addSlots(Set<StaticPlaylistEntry> slots) {
        if (slots != null) {
            for (StaticPlaylistEntry slot : slots) {
                addSlot(slot);
            }
        }
    }

    public void removeSlot(StaticPlaylistEntry slot) {
        removeSlot(slot, true);
    }

    public void removeSlot(StaticPlaylistEntry slot, boolean doLink) {
        if (slot != null) {
            getSlots().remove(slot);
            if (doLink) {
                slot.removeTrack(false);
            }
        }
    }

    public void removeSlots(Set<StaticPlaylistEntry> slots) {
        if (slots != null) {
            for (StaticPlaylistEntry slot : slots) {
                removeSlot(slot);
            }
        }
    }

    public void clearSlots() {
        for (StaticPlaylistEntry slot : getSlots()) {
            slot.removeTrack(false);
        }
        //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
        //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getSlots().clear();
        //slots = new HashSet<StaticPlaylistEntry>();
    }

    public Long getRotationCount(Programme programme) {
        Long result = null;
        if (programme!=null) {

            //Instantiate a mock probe, actually disconnected to other objects, delegate this task to the super method
            //be sure that the equality test uses programme + track only
            TrackProgramme newTrackProgramme = new TrackProgramme();
            newTrackProgramme.setTrack(this);
            newTrackProgramme.setProgramme(programme);

            for (TrackProgramme programmeProgramme : getTrackProgrammes()) {
                if (programmeProgramme!=null && programmeProgramme.equals(newTrackProgramme)) {
                    result = programmeProgramme.getRotation();
                    break;
                }
            }

        }
        return result;
    }

    public void setRotationCount(Programme programme, long rotation) {
        if (programme!=null) {

            //Instantiate a mock probe, actually disconnected to other objects, delegate this task to the super method
            //be sure that the equality test uses programme + track only
            TrackProgramme newTrackProgramme = new TrackProgramme();
            newTrackProgramme.setTrack(this);
            newTrackProgramme.setProgramme(programme);

            for (TrackProgramme trackProgramme : getTrackProgrammes()) {
                if (trackProgramme!=null && trackProgramme.equals(newTrackProgramme)) {
                    trackProgramme.setRotation(rotation);
                    break;
                }
            }

        }
    }

    public SortedSet<SimpleTrackQueueItem> getQueueItems() {
        if (queueItems == null) {
            queueItems = new TreeSet<SimpleTrackQueueItem>();
        }
        return queueItems;
    }

    private void setQueueItems(SortedSet<SimpleTrackQueueItem> items) {
        this.queueItems = items;
    }

    public void addQueueItem(SimpleTrackQueueItem queueItem) {
        addQueueItem(queueItem, true);
    }

    public void addQueueItem(SimpleTrackQueueItem queueItem, boolean doLink) {
        if (queueItem != null) {
            getQueueItems().add(queueItem);
            if (doLink) {
                queueItem.addItem(this, false);
            }
        }
    }

    public void addQueueItems(Set<SimpleTrackQueueItem> queueItems) {
        if (queueItems != null) {
            for (SimpleTrackQueueItem queueItem : queueItems) {
                addQueueItem(queueItem);
            }
        }
    }

    public void removeQueueItem(SimpleTrackQueueItem queueItem) {
        removeQueueItem(queueItem, true);
    }

    public void removeQueueItem(SimpleTrackQueueItem queueItem, boolean doLink) {
        if (queueItem != null) {
            getQueueItems().remove(queueItem);
            if (doLink) {
                queueItem.removeItem(false);
            }
        }
    }

    public void removeQueueItems(Set<SimpleTrackQueueItem> queueItems) {
        if (queueItems != null) {
            for (SimpleTrackQueueItem queueItem : queueItems) {
                removeQueueItem(queueItem);
            }
        }
    }

    public void clearQueueItems() {
        for (SimpleTrackQueueItem queueItem : getQueueItems()) {
            queueItem.removeItem(false);
        }
        getQueueItems().clear();
    }

    public Set<SimpleTrackQueueStreamItem> getStreamItems() {
        if (streamItems == null) {
            streamItems = new HashSet<SimpleTrackQueueStreamItem>();
        }
        return streamItems;
    }

    private void setStreamItems(Set<SimpleTrackQueueStreamItem> items) {
        this.streamItems = items;
    }

    public void addStreamItem(SimpleTrackQueueStreamItem streamItem) {
        addStreamItem(streamItem, true);
    }

    public void addStreamItem(SimpleTrackQueueStreamItem streamItem, boolean doLink) {
        if (streamItem != null) {
            getStreamItems().add(streamItem);
            if (doLink) {
                streamItem.addTrack(this, false);
            }
        }
    }

    public void addStreamItems(Set<SimpleTrackQueueStreamItem> streamItems) {
        if (streamItems != null) {
            for (SimpleTrackQueueStreamItem streamItem : streamItems) {
                addStreamItem(streamItem);
            }
        }
    }

    public void removeStreamItem(SimpleTrackQueueStreamItem streamItem) {
        removeStreamItem(streamItem, true);
    }

    public void removeStreamItem(SimpleTrackQueueStreamItem streamItem, boolean doLink) {
        if (streamItem != null) {
            getStreamItems().remove(streamItem);
            if (doLink) {
                streamItem.removeTrack(false);
            }
        }
    }

    public void removeStreamItems(Set<SimpleTrackQueueStreamItem> streamItems) {
        if (streamItems != null) {
            for (SimpleTrackQueueStreamItem streamItem : streamItems) {
                removeStreamItem(streamItem);
            }
        }
    }

    public void clearStreamItems() {
        for (SimpleTrackQueueStreamItem streamItem : getStreamItems()) {
            streamItem.removeTrack(false);
        }
        getStreamItems().clear();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        BroadcastableTrack test = ProxiedClassUtil.cast(obj, BroadcastableTrack.class);
        return super.equals(test);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setListenerRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getExtendedViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setAnimatorRights(true);
        rightsBundle.setContributorRights(true);
        rightsBundle.setCuratorRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setCuratorRights(true);
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getUpdateRights();
    }
    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
