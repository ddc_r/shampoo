package biz.ddcr.shampoo.server.domain.track;

import java.io.Serializable;

/**
 * The specific Pan European Gaming Industry rating for a track
 *
 * @author okay_awright
 */
public class PEGIRating implements Serializable {

    /** PEGI Rating age */
    private RATING_AGE age;

    /**
     * PEGI Rating content
     **/
    private Boolean violence;
    private Boolean profanity;
    private Boolean fear;
    private Boolean sex;
    private Boolean drugs;
    private Boolean discrimination;

    public PEGIRating(){
    }

    public RATING_AGE getAge() {
        return age;
    }

    public void setAge(RATING_AGE age) {
        this.age = age;
    }

    public Boolean getDiscrimination() {
        return discrimination;
    }

    public void setDiscrimination(Boolean discrimination) {
        this.discrimination = discrimination;
    }

    public Boolean getDrugs() {
        return drugs;
    }

    public void setDrugs(Boolean drugs) {
        this.drugs = drugs;
    }

    public Boolean getFear() {
        return fear;
    }

    public void setFear(Boolean fear) {
        this.fear = fear;
    }

    public Boolean getProfanity() {
        return profanity;
    }

    public void setProfanity(Boolean profanity) {
        this.profanity = profanity;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Boolean getViolence() {
        return violence;
    }

    public void setViolence(Boolean violence) {
        this.violence = violence;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PEGIRating other = (PEGIRating) obj;
        if (this.age != other.age && (this.age == null || !this.age.equals(other.age))) {
            return false;
        }
        if (this.violence != other.violence && (this.violence == null || !this.violence.equals(other.violence))) {
            return false;
        }
        if (this.profanity != other.profanity && (this.profanity == null || !this.profanity.equals(other.profanity))) {
            return false;
        }
        if (this.fear != other.fear && (this.fear == null || !this.fear.equals(other.fear))) {
            return false;
        }
        if (this.sex != other.sex && (this.sex == null || !this.sex.equals(other.sex))) {
            return false;
        }
        if (this.drugs != other.drugs && (this.drugs == null || !this.drugs.equals(other.drugs))) {
            return false;
        }
        if (this.discrimination != other.discrimination && (this.discrimination == null || !this.discrimination.equals(other.discrimination))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.age != null ? this.age.hashCode() : 0);
        hash = 71 * hash + (this.violence != null ? this.violence.hashCode() : 0);
        hash = 71 * hash + (this.profanity != null ? this.profanity.hashCode() : 0);
        hash = 71 * hash + (this.fear != null ? this.fear.hashCode() : 0);
        hash = 71 * hash + (this.sex != null ? this.sex.hashCode() : 0);
        hash = 71 * hash + (this.drugs != null ? this.drugs.hashCode() : 0);
        hash = 71 * hash + (this.discrimination != null ? this.discrimination.hashCode() : 0);
        return hash;
    }

}
