package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 * Aa BroadcastableAdvert track
 *
 * @author okay_awright
 */
public class PendingAdvert extends PendingTrack {
    
    public PendingAdvert() {
    }
    public PendingAdvert(PendingAdvert o) throws Exception {
        super(o);
    }
    
    @Override
    public PendingAdvert shallowCopy() throws Exception {
        return new PendingAdvert(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        PendingAdvert test = ProxiedClassUtil.cast(obj, PendingAdvert.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        return hash;
    }
    
    @Override
    public void acceptVisit(TrackVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
