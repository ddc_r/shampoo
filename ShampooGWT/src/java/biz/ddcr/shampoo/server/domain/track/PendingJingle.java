package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 * A BroadcastableJingle track
 *
 * @author okay_awright
 */
public class PendingJingle extends PendingTrack {
    
    public PendingJingle() {
    }
    public PendingJingle(PendingJingle o) throws Exception {
        super(o);
    }
   
    @Override
    public PendingJingle shallowCopy() throws Exception {
        return new PendingJingle(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        PendingJingle test = ProxiedClassUtil.cast(obj, PendingJingle.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        return hash;
    }
    
    @Override
    public void acceptVisit(TrackVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
