package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 * A Pending Song track
 *
 * @author okay_awright
 */
public class PendingSong extends PendingTrack {

    public PendingSong() {
    }
    public PendingSong(PendingSong o) throws Exception {
        super(o);
    }
        
    @Override
    public PendingSong shallowCopy() throws Exception {
        return new PendingSong(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        PendingSong test = ProxiedClassUtil.cast(obj, PendingSong.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        return hash;
    }
    
    @Override
    public void acceptVisit(TrackVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
