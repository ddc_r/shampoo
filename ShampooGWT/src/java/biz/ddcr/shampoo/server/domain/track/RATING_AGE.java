/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track;

/**
 *
 * @author okay_awright
 **/
public enum RATING_AGE {
    earlyChildhood(3),
    everyone(7),
    teen(12),
    mature(16),
    adultsOnly(18);

    private final int age;

    RATING_AGE(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

}
