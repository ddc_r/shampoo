package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.RequestQueueItemInterface;
import biz.ddcr.shampoo.server.domain.streamer.RequestQueueStreamItemInterface;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;

/**
 * A song requested by a user for braodcasting on a specific programme and channel
 * This is not enforced in the schema, but queueItem AND streamItem CANNOT be set together at all times.
 *
 * @author okay_awright
 */
public class Request extends GenericTimestampedEntity implements PlayableItemInterface {

    private String refID;
    private RequestLocation location;
    private RestrictedUser requester;
    private String message;
    private RequestQueueItemInterface queueItem;
    private RequestQueueStreamItemInterface streamItem;

    protected Request() {
        //For hibernate only
    }

    public Request(RestrictedUser requester, BroadcastableSong song, Channel channel, String message) {
        addRequester(requester);
        addSong(song);
        addChannel(channel);
        setMessage(message);
    }

    public Request(Request o) {
        super(o);
        addRequester(o.getRequester());
        addSong(o.getSong());
        addChannel(o.getChannel());
        setMessage(o.getMessage());
    }

    public String getRefID() {
        if (refID==null) refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public RequestLocation getLocation() {
        if (location == null) {
            location = new RequestLocation();
        }
        return location;
    }

    private void setLocation(RequestLocation location) {
        this.location = location;
    }

    /** Hibernate quirk; do not use **/
    public void setSong(BroadcastableSong song) {
        getLocation().setSong(song);
    }

    public BroadcastableSong getSong() {
        return getLocation().getSong();
    }

    public void addSong(BroadcastableSong song) {
        addSong(song, true);
    }

    public void addSong(BroadcastableSong song, boolean doLink) {
        if (song != null) {
            if (doLink) {
                song.addRequest(this, false);
            }
            getLocation().setSong(song);
        }
    }

    /*public void removeSong() {
        removeSong(true);
    }

    public void removeSong(boolean doLink) {
        if (getLocation().getSong() != null) {
            if (doLink) {
                getLocation().getSong().removeRequest(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setSong(null);
        }
    }*/

    public RestrictedUser getRequester() {
        return requester;
    }

    protected void setRequester(RestrictedUser requester) {
        this.requester = requester;
    }

    public void addRequester(RestrictedUser requester) {
        addRequester(requester, true);
    }

    public void addRequester(RestrictedUser requester, boolean doLink) {
        if (requester != null) {
            if (doLink) {
                requester.addRequest(this, false);
            }
            setRequester(requester);
        }
    }

    /*public void removeRequester() {
        removeRequester(true);
    }

    public void removeRequester(boolean doLink) {
        if (getRequester() != null) {
            if (doLink) {
                getRequester().removeRequest(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setRequester(null);
        }
    }*/

    protected void setChannel(Channel channel) {
        getLocation().setChannel(channel);
    }
    
    public Channel getChannel() {
        return getLocation().getChannel();
    }

    public void addChannel(Channel song) {
        addChannel(song, true);
    }

    public void addChannel(Channel song, boolean doLink) {
        if (song != null) {
            if (doLink) {
                song.addRequest(this, false);
            }
            getLocation().setChannel(song);
        }
    }

    /*public void removeChannel() {
        removeChannel(true);
    }

    public void removeChannel(boolean doLink) {
        if (getLocation().getChannel() != null) {
            if (doLink) {
                getLocation().getChannel().removeRequest(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            getLocation().setChannel(null);
        }
    }*/

    public RequestQueueStreamItemInterface getStreamItem() {
        return streamItem;
    }

    protected void setStreamItem(RequestQueueStreamItemInterface streamItem) {
        this.streamItem = streamItem;
    }

    public void addStreamItem(RequestQueueStreamItemInterface streamItem) {
        addStreamItem(streamItem, true);
    }

    public void addStreamItem(RequestQueueStreamItemInterface streamItem, boolean doLink) {
        if (streamItem != null) {
            setStreamItem(streamItem);
            if (doLink) {
                streamItem.addRequest(this, false);
            }
        }
    }

    public void removeStreamItem() {
        removeStreamItem(true);
    }

    public void removeStreamItem(boolean doLink) {
        if (getStreamItem() != null) {
            if (doLink) {
                getStreamItem().removeRequest(false);
            }
            //Put delete-orphan in the hibernate mapping
            setStreamItem(null);
        }
    }

    public RequestQueueItemInterface getQueueItem() {
        return queueItem;
    }

    protected void setQueueItem(RequestQueueItemInterface queueItem) {
        this.queueItem = queueItem;
    }

    public void addQueueItem(RequestQueueItemInterface queueItem) {
        addQueueItem(queueItem, true);
    }

    public void addQueueItem(RequestQueueItemInterface queueItem, boolean doLink) {
        if (queueItem != null) {
            setQueueItem(queueItem);
            if (doLink) {
                queueItem.addRequest(this, false);
            }
        }
    }

    public void removeQueueItem() {
        removeQueueItem(true);
    }

    public void removeQueueItem(boolean doLink) {
        if (getQueueItem() != null) {
            if (doLink) {
                getQueueItem().removeRequest(false);
            }
            //Put delete-orphan in the hibernate mapping
            setQueueItem(null);
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean drop() {
        return drop(true, true, true);
    }
    
    public boolean drop(boolean doUnlinkChannel, boolean doUnlinkSong, boolean doUnlinkRequester) {
        boolean result = true;
        if (doUnlinkChannel && getChannel()!=null) {
            result = getChannel().getRequests().remove(this) & result;            
        }
        if (doUnlinkSong && getSong()!=null) {
            result = getSong().getRequests().remove(this) & result; 
        }
        if (doUnlinkRequester && getRequester()!=null) {
            result = getRequester().getRequests().remove(this) & result; 
        }
        setChannel(null);
        setSong(null);
        setRequester(null);
        return result;
    }
    
    @Override
    public Request shallowCopy() {
        return new Request(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        Request test = ProxiedClassUtil.cast(obj, Request.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }
        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (this.getLocation() != null ? this.getLocation().hashCode() : 0);
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void is(PlayableItemVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
