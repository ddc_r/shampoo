/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class RequestLocation implements Serializable {

    private BroadcastableSong song;
    private Channel channel;

    public RequestLocation() {
    }
    
    public BroadcastableSong getSong() {
        return song;
    }

    public void setSong(BroadcastableSong song) {
        this.song = song;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Programme at this point
        RequestLocation test = ProxiedClassUtil.cast(obj, RequestLocation.class);
        return /*super.equals(test)
                &&*/ (getSong() == test.getSong() || (getSong() != null && getSong().equals(test.getSong())))
                && (getChannel() == test.getChannel() || (getChannel() != null && getChannel().equals(test.getChannel())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (getSong() != null ? getSong().hashCode() : 0);
        hash = 23 * hash + (getChannel() != null ? getChannel().hashCode() : 0);
        return hash;
    }

}
