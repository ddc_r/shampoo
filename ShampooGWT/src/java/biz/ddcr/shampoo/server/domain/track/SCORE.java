/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author okay_awright
 **/
public enum SCORE {
    bad(0),
    poor(1),
    average(2),
    good(3),
    excellent(4);

    private static final Map<Integer,SCORE> lookup = new HashMap<Integer,SCORE>();
    static {
        for(SCORE s : EnumSet.allOf(SCORE.class))
            lookup.put(s.getNumericScore(), s);
     }
    private final int score;
    private SCORE(int score) {
        if (score<0) score=0;
        else if (score>4) score=4;
        this.score = score;
    }

    public int getNumericScore() {
        return score;
    }

    public static SCORE get(int score) {
        if (score<0) score=0;
        else if (score>4) score=4;
        return lookup.get(score);
    }

}
