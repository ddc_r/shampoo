package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * An actual file to be broadcast
 *
 * @author okay_awright
 */
public abstract class Track extends GenericTimestampedEntity implements CommonTagInterface, VisitorPatternInterface<TrackVisitor> {

    private String refID;
    private String title;
    private String author;
    private String album;
    private String genre;
    private Short dateOfRelease;
    private String description;
    /** user-defined field */
    private String tag;
    private String publisher;
    private String copyright;
    private PEGIRating rating;
    /** special flag that specifies if the file represented by the track is available for processing. The track may, for example, be transcoded or still being transfered to the datastore and thus is not yet ready for broadcast
     **/
    private boolean ready;

    /** metadata of the current cover art picture linked to this track; null if no file */
    private PictureFileInfo coverArtContainer;
    /** metadata of the current audio track linked to this track; null if no file */
    private AudioFileInfo trackContainer;

    /** protected, not private, for easier Hibernate inheritance **/
    protected Set<TrackProgramme> trackProgrammes;

    public Track() {
    }
    public Track(Track o) throws Exception {
        super(o);
        setAlbum( o.getAlbum() );
        setAuthor( o.getAuthor() );
        setDateOfRelease( o.getDateOfRelease() );
        setDescription( o.getDescription() );
        setGenre( o.getGenre() );
        setTitle( o.getTitle() );
        setTag( o.getTag() );
        setPublisher( o.getPublisher() );
        setCopyright( o.getCopyright() );
        setRating( o.getRating() );
        setCoverArtContainer( o.getCoverArtContainer() );
        setTrackContainer( o.getTrackContainer() );
        setReady( o.isReady() );
        addTrackProgrammes( o.getTrackProgrammes() );
    }

    public String getRefID() {
        if (refID==null) refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public Short getDateOfRelease() {
        return dateOfRelease;
    }

    public void setDateOfRelease(Short dateOfRelease) {
        this.dateOfRelease = dateOfRelease;
    }

    @Override
    public PEGIRating getRating() {
        return rating;
    }

    public void setRating(PEGIRating rating) {
        this.rating = rating;
    }

    @Override
    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @Override
    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    @Override
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public PictureFileInfo getCoverArtContainer() {
        return coverArtContainer;
    }

    public void setCoverArtContainer(PictureFileInfo container) {
        this.coverArtContainer = container;
    }

    public AudioFileInfo getTrackContainer() {
        return trackContainer;
    }

    public void setTrackContainer(AudioFileInfo container) {
        this.trackContainer = container;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public Collection<Programme> getImmutableProgrammes() {
        Collection<Programme> programmes = new HashSet<Programme>();
        for (Iterator<TrackProgramme> i = getTrackProgrammes().iterator(); i.hasNext(); ) {
            TrackProgramme trackProgramme = i.next();
            if (trackProgramme.getProgramme()!=null)
                programmes.add(trackProgramme.getProgramme());
            //DIRTY FIX Cleanup fix for trackProgramme that should be removed from the list
            else if (trackProgramme.getTrack()==null)
                i.remove();
        }
        return programmes;
    }

    public Set<TrackProgramme> getTrackProgrammes() {
        if (trackProgrammes==null) trackProgrammes=new HashSet<TrackProgramme>();
        return trackProgrammes;
    }
    /** Hibernate kludge **/
    protected void setTrackProgrammes(Set<TrackProgramme> trackProgrammes) {
        this.trackProgrammes = trackProgrammes;
    }

    public void addTrackProgramme(TrackProgramme trackProgramme) throws Exception {
        addTrackProgramme(trackProgramme, true);
    }
    public void addTrackProgramme(TrackProgramme trackProgramme, boolean doLink) throws Exception {
        if (trackProgramme!=null) {
            trackProgramme.setTrack(this);
            getTrackProgrammes().add(trackProgramme);
            if (doLink && trackProgramme.getProgramme()!=null) trackProgramme.getProgramme().addTrackProgramme(trackProgramme, false);
        }
    }
    public void addTrackProgrammes(Collection<TrackProgramme> trackProgrammes) throws Exception {
        if (trackProgrammes!=null) {
            for (TrackProgramme trackProgramme : trackProgrammes)
                addTrackProgramme(trackProgramme);
        }
    }
    public void removeTrackProgramme(TrackProgramme trackProgramme) throws Exception {
        removeTrackProgramme(trackProgramme, true);
    }
    public void removeTrackProgramme(TrackProgramme trackProgramme, boolean doLink) throws Exception {
        if (trackProgramme!=null) {
            //Unlink the trackprogramme both on the track (there) and on the programme (here) ends
            //Since we use delete-orphan, don't clear the actual references in trackprogramme
            trackProgramme.drop(true, doLink);
        }
    }
    public void removeTrackProgrammes(Collection<TrackProgramme> trackProgrammes) throws Exception {
        if (trackProgrammes!=null) {
            for (TrackProgramme trackProgramme : trackProgrammes)
                removeTrackProgramme(trackProgramme);
        }
    }
    public void clearTrackProgrammes() throws Exception {
        for (Iterator<TrackProgramme> i = getTrackProgrammes().iterator();i.hasNext();) {
            TrackProgramme trackProgramme = i.next();
            i.remove();
            //Unlink the trackprogramme both on the track (there) and on the programme (here) ends
            //Since we use delete-orphan, don't clear the actual references in trackprogramme
            trackProgramme.drop(true, false);
        }
    }

    public void addProgramme(Programme programme) throws Exception {
        addProgramme(programme, true);
    }
    public void addProgramme(Programme programme, boolean doLink) throws Exception {
        if (programme!=null) {
            TrackProgramme newTrackProgramme = new TrackProgramme();
            newTrackProgramme.setProgramme(programme);
            newTrackProgramme.setRotation(0L);
            addTrackProgramme(newTrackProgramme, doLink);
        }
    }
    public void addProgrammes(Collection<Programme> programmes) throws Exception {
        if (programmes!=null) {
            for (Programme programme : programmes)
                addProgramme(programme);
        }
    }
    /*public void removeProgramme(Programme programme) throws Exception {
        removeProgramme(programme, true);
    }
    public void removeProgramme(Programme programme, boolean doLink) throws Exception {
        if (programme!=null) {
            TrackProgramme newTrackProgramme = new TrackProgramme();
            newTrackProgramme.setTrack(this);
            newTrackProgramme.setProgramme(programme);
            removeTrackProgramme(newTrackProgramme, doLink);
        }
    }
    public void removeProgrammes(Collection<Programme> programmes) throws Exception {
        if (programmes!=null) {
            for (Programme programme : programmes)
                removeProgramme(programme);
        }
    }
    public void clearProgrammes() throws Exception {
        clearTrackProgrammes();
    }*/

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        Track test = ProxiedClassUtil.cast(obj, Track.class);
        return /*super.equals(test)
                &&*/ (getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())
                /*&& (getArtist().equals(test.getArtist()))
                && (getTitle().equals(test.getTitle()))**/
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (getRefID() != null ? getRefID().hashCode() : 0);
        /*hash = 31 * hash + (null == getTitle() ? 0 : getTitle().hashCode());
        hash = 31 * hash + (null == getArtist() ? 0 : getArtist().hashCode());**/
        //No lazily loaded objects can be used
        return hash;
    }
    
}
