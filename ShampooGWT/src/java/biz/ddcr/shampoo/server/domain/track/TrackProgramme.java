package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.Versioning;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import java.io.Serializable;

/**
 * Which tracks are linked to which programmes?
 *
 * @author okay_awright
 */
public class TrackProgramme implements Serializable, Versioning {

    /** Hibernate versioning */
    private int version;
    private String refID;
    private TrackProgrammeLocation location;
    /** Number of times this track has been broadcast on a specific programme
     * Should be a primitive type but Hibernate doesn't allow it with the current inheritance strategy
     */
    private Long rotation;

    public TrackProgramme() {
        //
    }

    public TrackProgramme(Programme programme, Track track) {
        setProgramme(programme);
        setTrack(track);
        //Assume it's a new one so it can't have been played yet
        setRotation(0L);
    }
    
    public TrackProgrammeLocation getLocation() {
        if (location == null) {
            location = new TrackProgrammeLocation();
        }
        return location;
    }

    private void setLocation(TrackProgrammeLocation location) {
        this.location = location;
    }

    public Long getRotation() {
        return rotation;
    }

    public void setRotation(Long rotation) {
        this.rotation = rotation;
    }

    public Track getTrack() {
        return getLocation().getTrack();
    }

    public void setTrack(Track track) {
        getLocation().setTrack(track);
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    public String getRefID() {
        if (refID == null) {
            refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        }
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public Programme getProgramme() {
        return getLocation().getProgramme();
    }

    public void setProgramme(Programme programme) {
        getLocation().setProgramme(programme);
    }

    public boolean drop() {
        return drop(true, true);
    }
    
    public boolean drop(boolean doUnlinkProgramme, boolean doUnlinkTrack) {
        boolean result = true;
        if (doUnlinkProgramme && getProgramme()!=null) {
            result = getProgramme().getTrackProgrammes().remove(this) & result;
        }
        if (doUnlinkTrack && getTrack()!=null) {
            result = getTrack().getTrackProgrammes().remove(this) & result;
        }
        setTrack(null);
        setProgramme(null);
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be TrackProgramme at this point
        TrackProgramme test = ProxiedClassUtil.cast(obj, TrackProgramme.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }
        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())));
    }

    @Override
    public int hashCode() {
        int hash = 6;
        hash = 31 * hash + (this.getLocation() != null ? this.getLocation().hashCode() : 0);
        return hash;
    }
}
