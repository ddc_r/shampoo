/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;

/**
 *
 * @author okay_awright
 **/
public class TrackProgrammeLocation implements Serializable {

    private Programme programme;
    private Track track;

    public TrackProgrammeLocation() {
    }
    
    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Programme getProgramme() {
        return programme;
    }

    public void setProgramme(Programme programme) {
        this.programme = programme;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Track at this point
        TrackProgrammeLocation test = ProxiedClassUtil.cast(obj, TrackProgrammeLocation.class);
        return /*super.equals(test)
                &&*/ (getTrack() == test.getTrack() || (getTrack() != null && getTrack().equals(test.getTrack())))
                && (getProgramme() == test.getProgramme() || (getProgramme() != null && getProgramme().equals(test.getProgramme())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 6;
        hash = 23 * hash + (getTrack() != null ? getTrack().hashCode() : 0);
        hash = 23 * hash + (getProgramme() != null ? getProgramme().hashCode() : 0);
        return hash;
    }

}
