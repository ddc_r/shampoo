package biz.ddcr.shampoo.server.domain.track;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import java.util.HashSet;
import java.util.Set;

/**
 * A rating for a song, by a specific user
 *
 * @author okay_awright
 */
public class Voting extends GenericTimestampedEntity {

    private String refID;
    private VotingLocation location;
    private SCORE value;

    protected Voting(){
        //For hibernate only
    }
    public Voting(RestrictedUser user, BroadcastableSong song, SCORE score) {
        //value must be set first: addSong uses it
        setValue( score );
        addRestrictedUser( user );
        addSong( song );
    }
    public Voting(Voting o) {
        super(o);
        //value must be set first: addSong uses it
        setValue( o.getValue() );
        addRestrictedUser( o.getRestrictedUser() );
        addSong( o.getSong() );
    }

    public String getRefID() {
        if (refID==null) refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        return refID;
    }

    public void setRefID(String refID) {
        this.refID = refID;
    }

    public VotingLocation getLocation() {
        if (location == null) location = new VotingLocation();
        return location;
    }

    private void setLocation(VotingLocation location) {
        this.location = location;
    }

    protected void setSong(BroadcastableSong song) {
        getLocation().setSong(song);
    }
    
    public BroadcastableSong getSong() {
        return getLocation().getSong();
    }

    public void addSong(BroadcastableSong song) {
        addSong(song, true);
    }
    public void addSong(BroadcastableSong song, boolean doLink) {
        if (song!=null) {
            if (doLink) song.addVote(this, false);
            getLocation().setSong(song);
        }
    }
    /*public void removeSong() {
        removeSong(true);
    }
    public void removeSong(boolean doLink) {
        if (getLocation().getSong()!=null) {
            if (doLink) getLocation().getSong().removeVote(this, false);
            //Put delete-orphan in the hibernate mapping
            getLocation().setSong(null);
        }
    }*/

    public Set<Channel> getDependentChannelsImmutableSet() {
        Set<Channel> allBoundChannels = new HashSet<Channel>();
        if (getSong()!=null)
            for (TrackProgramme tp : getSong().getTrackProgrammes())
                if (tp.getProgramme()!=null)
                    allBoundChannels.addAll(tp.getProgramme().getChannels());
        return allBoundChannels;
    }

    protected void setRestrictedUser(RestrictedUser restrictedUser) {
        getLocation().setUser(restrictedUser);
    }
    
    public RestrictedUser getRestrictedUser() {
        return getLocation().getUser();
    }

    public void addRestrictedUser(RestrictedUser restrictedUser) {
        addRestrictedUser(restrictedUser, true);
    }
    public void addRestrictedUser(RestrictedUser restrictedUser, boolean doLink) {
        if (restrictedUser!=null) {
            if (doLink) restrictedUser.addVote(this, false);
            getLocation().setUser(restrictedUser);
        }
    }
    /*public void removeRestrictedUser() {
        removeRestrictedUser(true);
    }
    public void removeRestrictedUser(boolean doLink) {
        if (getLocation().getUser()!=null) {
            if (doLink) getLocation().getUser().removeVote(this, false);
            //Put delete-orphan in the hibernate mapping
            getLocation().setUser(null);
        }
    }*/
    
    public boolean drop() {
        return drop(true, true);
    }
    
    public boolean drop(boolean doUnlinkRestrictedUser, boolean doUnlinkSong) {
        boolean result = true;
        if (doUnlinkRestrictedUser && getRestrictedUser()!=null) {
            result = getRestrictedUser().getVotes().remove(this) & result;            
        }
        if (doUnlinkSong && getSong()!=null) {
            result = getSong().getVotes().remove(this) & result; 
        }
        setSong(null);
        setRestrictedUser(null);
        return result;
    }

    public SCORE getValue() {
        return value;
    }

    public void setValue(SCORE value) {
        this.value = value;
    }

    @Override
    public Voting shallowCopy() {
        return new Voting(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        Voting test = ProxiedClassUtil.cast(obj, Voting.class);
        //Special case when binding to an object collection during instantiation and where this object is also part of the business key
        if ((getRefID() == null) ? (test.getRefID() == null) : getRefID().equals(test.getRefID())) {
            return true;
        }
        return /*super.equals(test)
                &&*/ (getLocation() == test.getLocation() || (getLocation() != null && getLocation().equals(test.getLocation())))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (this.getLocation() != null ? this.getLocation().hashCode() : 0);
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
