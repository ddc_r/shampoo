/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track.filter;

import biz.ddcr.shampoo.server.domain.playlist.DynamicPlaylistEntry;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 *
 * @author okay_awright
 **/
public class AlphabeticalFilter extends Filter {

    private ALPHABETICAL_FEATURE feature;
    private String contains;

    protected AlphabeticalFilter() {
    }
    public AlphabeticalFilter(DynamicPlaylistEntry entry) {
        super(entry);
    }
    public AlphabeticalFilter(AlphabeticalFilter o) {
        super(o);
        setContains( o.getContains() );
        setFeature( o.getFeature() );
    }

    public String getContains() {
        return contains;
    }

    public void setContains(String contains) {
        this.contains = contains;
    }

    @Override
    public ALPHABETICAL_FEATURE getFeature() {
        return feature;
    }

    public void setFeature(ALPHABETICAL_FEATURE feature) {
        this.feature = feature;
    }

    @Override
    public AlphabeticalFilter shallowCopy() {
        return new AlphabeticalFilter(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        AlphabeticalFilter test = ProxiedClassUtil.cast(obj, AlphabeticalFilter.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(FilterVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    @Override
    public boolean isDuplicate(Filter otherFilter) {
        return isDuplicate(true, otherFilter);
    }
    @Override
    public boolean isDuplicate(boolean doReverseChecking, Filter otherFilter) {
        if (otherFilter==null || !ProxiedClassUtil.castableAs(otherFilter, AlphabeticalFilter.class))
            return false;
        AlphabeticalFilter otherAlphabeticalFilter = (AlphabeticalFilter)otherFilter;
        if (!getFeature().equals(otherAlphabeticalFilter.getFeature())) return false;
        //Forget about checking bound playlistEntries so as to avoid circular calls, except if enforce
        if (doReverseChecking)
            if (!getEntry().equals(otherFilter.getEntry())) return false;
        if (isInclude()!=otherAlphabeticalFilter.isInclude()) return false;
        if (!getContains().equals(otherAlphabeticalFilter.getContains())) return false;
        return true;
    }

}
