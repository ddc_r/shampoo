/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track.filter;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.DuplicateCheckingInterface;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.playlist.DynamicPlaylistEntry;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;

/**
 *
 * Filtered sub-selection to be processed by a Rule of a Dynamic Slot
 *
 * @author okay_awright
 **/
public abstract class Filter implements GenericEntityInterface, DuplicateCheckingInterface<Filter>, VisitorPatternInterface<FilterVisitor> {

    /** Hibernate versioning */
    private int version;
    private FEATURE feature;
    private String refID;
    private DynamicPlaylistEntry entry;
    private boolean include;

    protected Filter() {
    }
    public Filter(DynamicPlaylistEntry entry) {
        super();
        addEntry( entry );
    }
    public Filter(Filter o) {
        setFeature( o.getFeature() );
        addEntry( o.getEntry() );
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    public FEATURE getFeature() {
        return feature;
    }

    public void setFeature(FEATURE feature) {
        this.feature = feature;
    }

    public DynamicPlaylistEntry getEntry() {
        return entry;
    }

    private void setEntry(DynamicPlaylistEntry entry) {
        this.entry = entry;
    }
    //protected access; use the constructor instead
    public void addEntry(DynamicPlaylistEntry entry) {
        addEntry(entry, true);
    }
    public void addEntry(DynamicPlaylistEntry entry, boolean doLink) {
        if (entry!=null) {
            setEntry(entry);
            if (doLink) entry.addFilter(this, false);
        }
    }
    public void removeEntry() {
        removeEntry(true);
    }
    public void removeEntry(boolean doLink) {
        if (getEntry()!=null) {
            if (doLink) getEntry().removeFilter(this, false);
            //Put delete-orphan in the hibernate mapping
            setEntry(null);
        }
    }

    public boolean isInclude() {
        return include;
    }

    public void setInclude(boolean include) {
        this.include = include;
    }

    /*public long getFrom() {
        return from;
    }

    public void setFrom(long from) {
        this.from = from;
    }

    public long getTo() {
        return to;
    }

    public void setTo(long to) {
        this.to = to;
    }*/

    public String getRefID() {
        if (refID==null) refID = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        return refID;
    }

    private void setRefID(String refID) {
        this.refID = refID;
    }

    @Override
    public Filter shallowCopy() {
        if (ProxiedClassUtil.castableAs(this, AlphabeticalFilter.class))
            return ((AlphabeticalFilter)this).shallowCopy();
        else if (ProxiedClassUtil.castableAs(this, NumericalFilter.class))
            return ((NumericalFilter)this).shallowCopy();
        else if (ProxiedClassUtil.castableAs(this, CategoryFilter.class))
            return ((CategoryFilter)this).shallowCopy();
        //new class unsynchronized with this method
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        Filter test = ProxiedClassUtil.cast(obj, Filter.class);
        return /*super.equals(test)
                &&*/ (getRefID().equals(test.getRefID()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getRefID() ? 0 : getRefID().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    /**
     * Alternative equality test. Inspect all attributes this time, except primary keys.
     * @param otherPlaylistEntry
     * @return
     */
    @Override
    public abstract boolean isDuplicate(Filter otherFilter);

    /** visitor pattern inspection for Hibernate proxies **/
    @Override
    public abstract void acceptVisit(FilterVisitor visitor) throws Exception;

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
