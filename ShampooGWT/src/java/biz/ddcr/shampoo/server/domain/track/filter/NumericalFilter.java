/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track.filter;

import biz.ddcr.shampoo.server.domain.playlist.DynamicPlaylistEntry;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;

/**
 *
 * @author okay_awright
 **/
public class NumericalFilter extends Filter {

    private NUMERICAL_FEATURE feature;
    private short min;
    private short max;

    protected NumericalFilter() {
    }
    public NumericalFilter(DynamicPlaylistEntry entry) {
        super(entry);
    }
    public NumericalFilter(NumericalFilter o) {
        super(o);
        setFeature( o.getFeature() );
        setMax( o.getMax() );
        setMin( o.getMin() );
    }

    @Override
    public NUMERICAL_FEATURE getFeature() {
        return feature;
    }

    public void setFeature(NUMERICAL_FEATURE feature) {
        this.feature = feature;
    }

    public short getMax() {
        return max;
    }

    public void setMax(short max) {
        this.max = max;
    }

    public short getMin() {
        return min;
    }

    public void setMin(short min) {
        this.min = min;
    }

    @Override
    public NumericalFilter shallowCopy() {
        return new NumericalFilter(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        NumericalFilter test = ProxiedClassUtil.cast(obj, NumericalFilter.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public void acceptVisit(FilterVisitor visitor) throws Exception {
        visitor.visit(this);
    }


    @Override
    public boolean isDuplicate(Filter otherFilter) {
        return isDuplicate(true, otherFilter);
    }
    @Override
    public boolean isDuplicate(boolean doReverseChecking, Filter otherFilter) {
        if (otherFilter==null || !ProxiedClassUtil.castableAs(otherFilter, NumericalFilter.class))
            return false;
        NumericalFilter otherNumericalFilter = (NumericalFilter)otherFilter;
        if (!getFeature().equals(otherNumericalFilter.getFeature())) return false;
        //Forget about checking bound playlistEntries so as to avoid circular calls, except if enforce
        if (doReverseChecking)
            if (!getEntry().equals(otherFilter.getEntry())) return false;
        if (isInclude()!=otherNumericalFilter.isInclude()) return false;
        if (getMin()!=otherNumericalFilter.getMin()) return false;
        if (getMax()!=otherNumericalFilter.getMax()) return false;
        return true;
    }

}
