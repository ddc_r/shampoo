/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.track.format;

import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;

/**
 *
 * @author okay_awright
 **/
public class AudioFileInfo implements FileInfoInterface<AUDIO_FORMAT> {

    private AUDIO_FORMAT format;
    /** physical size in bytes; it's the full length, header included */
    private long size;
    /** length in seconds */
    private float duration;
    /** bitrate in bps */
    private long bitrate;
    /** bitrate in hz */
    private int samplerate;
    /** 1 for mono, 2 for stereo, can be more; shouldn't be 0 */
    private byte channels;
    /** variable bit rate? otherwise could be constant or average for example */
    private boolean vbr;

    public AudioFileInfo() {
    }

    public AudioFileInfo(FileInfoInterface<AUDIO_FORMAT> audioFileInfo) {
        if (audioFileInfo!=null) {
            setFormat(audioFileInfo.getFormat());
            setSize(audioFileInfo.getSize());
        }
    }

    @Override
    public AUDIO_FORMAT getFormat() {
        return format;
    }

    public void setFormat(AUDIO_FORMAT format) {
        this.format = format;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public long getSize() {
        return size;
    }

    public long getBitrate() {
        return bitrate;
    }

    public void setBitrate(long bitrate) {
        this.bitrate = bitrate;
    }

    public byte getChannels() {
        return channels;
    }

    public void setChannels(byte channels) {
        this.channels = channels;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public int getSamplerate() {
        return samplerate;
    }

    public void setSamplerate(int samplerate) {
        this.samplerate = samplerate;
    }

    public boolean isVbr() {
        return vbr;
    }

    public void setVbr(boolean vbr) {
        this.vbr = vbr;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AudioFileInfo other = (AudioFileInfo) obj;
        if (this.format != other.format && (this.format == null || !this.format.equals(other.format))) {
            return false;
        }
        if (this.size != other.size) {
            return false;
        }
        if (this.duration != other.duration) {
            return false;
        }
        if (this.bitrate != other.bitrate) {
            return false;
        }
        if (this.samplerate != other.samplerate) {
            return false;
        }
        if (this.channels != other.channels) {
            return false;
        }
        if (this.vbr != other.vbr) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.format != null ? this.format.hashCode() : 0);
        hash = 59 * hash + (int) (this.size ^ (this.size >>> 32));
        hash = 59 * hash + Float.floatToIntBits(this.duration);
        hash = 59 * hash + (int) (this.bitrate ^ (this.bitrate >>> 32));
        hash = 59 * hash + this.samplerate;
        hash = 59 * hash + this.channels;
        hash = 59 * hash + (this.vbr ? 1 : 0);
        return hash;
    }

    @Override
    public int fullHashCode() {
        return hashCode();
    }
}
