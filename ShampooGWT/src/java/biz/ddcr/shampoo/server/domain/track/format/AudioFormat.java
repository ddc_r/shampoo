/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.domain.track.format;

import biz.ddcr.shampoo.server.io.helper.MIME;
import biz.ddcr.shampoo.server.io.helper.MIME.CATEGORY;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class AudioFormat {

    public enum AUDIO_FORMAT implements FILE_FORMAT {
        mp3,
        mp4_aac,
        native_flac,
        ogg_vorbis;

        public final static String FILE_NAME_SUFFIX = "_a";

        @Override
        public String getFileNameSuffix() {
            return FILE_NAME_SUFFIX;
        }
    }

    public static final EnumMap<AUDIO_FORMAT, Collection<MIME>> MIME_AUDIO_FORMAT = new EnumMap<AUDIO_FORMAT, Collection<MIME>>(AUDIO_FORMAT.class);
    static {
        Collection<MIME> mp3Mimes = new HashSet<MIME>();
        //Official MIME, is also shared with all other MPEGs
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mpeg"));
        //via RTP
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "MPA"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mpa-robust"));
        //Unofficial
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mpeg3"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-mpeg3"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mp3"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-mpeg"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-mp3"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mpg"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-mpg"));
        mp3Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-mpegaudio"));
        
        Collection<MIME> mp4Mimes = new HashSet<MIME>();
        //Official MIME, covers Quictime and ISO containers, not 3GP
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mp4"));
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "application", "mp4"));
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "MP4A-LATM"));
        //Unofficial
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-mp4"));
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-m4a"));
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mpeg4-generic"));        
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "mp4-generic"));      
        //Applicable but container might be ADTS and not MP4
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "aac"));
        mp4Mimes.add(new MIME(CATEGORY.audioTrack, "audio", "aacp"));

        Collection<MIME> oggVorbisMimes = new HashSet<MIME>();
        //Official; the codec inside may greatly vary, more than MP4
        oggVorbisMimes.add(new MIME(CATEGORY.audioTrack, "audio", "ogg"));
        oggVorbisMimes.add(new MIME(CATEGORY.audioTrack, "application", "ogg"));
        oggVorbisMimes.add(new MIME(CATEGORY.audioTrack, "audio", "vorbis"));
        //Unofficial
        oggVorbisMimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-ogg"));
        oggVorbisMimes.add(new MIME(CATEGORY.audioTrack, "application", "ogg"));
        oggVorbisMimes.add(new MIME(CATEGORY.audioTrack, "application", "x-ogg"));

        Collection<MIME> nativeFlacMimes = new HashSet<MIME>();
        //Official
        nativeFlacMimes.add(new MIME(CATEGORY.audioTrack, "audio", "x-flac"));
        //Unofficial
        nativeFlacMimes.add(new MIME(CATEGORY.audioTrack, "audio", "flac"));

        MIME_AUDIO_FORMAT.put(AUDIO_FORMAT.mp3, mp3Mimes);
        MIME_AUDIO_FORMAT.put(AUDIO_FORMAT.mp4_aac, mp4Mimes);
        MIME_AUDIO_FORMAT.put(AUDIO_FORMAT.native_flac, nativeFlacMimes);
        MIME_AUDIO_FORMAT.put(AUDIO_FORMAT.ogg_vorbis, oggVorbisMimes);
    }

    public static final EnumMap<AUDIO_FORMAT, Collection<String>> FILE_EXTENSIONS_AUDIO_FORMAT = new EnumMap<AUDIO_FORMAT, Collection<String>>(AUDIO_FORMAT.class);
    static {
        Collection<String> mp3Exts = new HashSet<String>();
        mp3Exts.add("mp3");

        Collection<String> mp4Exts = new HashSet<String>();
        mp4Exts.add("m4a");
        mp4Exts.add("mp4");

        Collection<String> oggVorbisExts = new HashSet<String>();
        oggVorbisExts.add("ogg");
        oggVorbisExts.add("oga");

        Collection<String> nativeFlacExts = new HashSet<String>();
        nativeFlacExts.add("flac");

        FILE_EXTENSIONS_AUDIO_FORMAT.put(AUDIO_FORMAT.mp3, mp3Exts);
        FILE_EXTENSIONS_AUDIO_FORMAT.put(AUDIO_FORMAT.mp4_aac, mp4Exts);
        FILE_EXTENSIONS_AUDIO_FORMAT.put(AUDIO_FORMAT.native_flac, nativeFlacExts);
        FILE_EXTENSIONS_AUDIO_FORMAT.put(AUDIO_FORMAT.ogg_vorbis, oggVorbisExts);
    }

    private AudioFormat() {
    }
}
