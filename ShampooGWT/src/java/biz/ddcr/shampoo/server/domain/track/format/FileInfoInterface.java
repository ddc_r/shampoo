package biz.ddcr.shampoo.server.domain.track.format;

import biz.ddcr.shampoo.server.io.HasFullHashCode;
import java.io.Serializable;

/**
 * Data related to the current physical support of a Track
 *
 * @author okay_awright
 */
public interface FileInfoInterface<T extends FILE_FORMAT> extends Serializable, HasFullHashCode {

    public T getFormat();
    
    public long getSize();

}
