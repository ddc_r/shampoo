/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.domain.track.format;

import biz.ddcr.shampoo.server.io.helper.MIME;
import biz.ddcr.shampoo.server.io.helper.MIME.CATEGORY;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class PictureFormat {

    public enum PICTURE_FORMAT implements FILE_FORMAT {
        jpeg,
        gif,
        png,
        wbmp;

        public final static String FILE_NAME_SUFFIX = "_p";

        @Override
        public String getFileNameSuffix() {
            return FILE_NAME_SUFFIX;
        }
    }
    public static final EnumMap<PICTURE_FORMAT, Collection<MIME>> MIME_PICTURE_FORMAT = new EnumMap<PICTURE_FORMAT, Collection<MIME>>(PICTURE_FORMAT.class);

    static {
        Collection<MIME> jpegMimes = new HashSet<MIME>();
        jpegMimes.add(new MIME(CATEGORY.coverArt, "image", "jpeg"));

        Collection<MIME> gifMimes = new HashSet<MIME>();
        gifMimes.add(new MIME(CATEGORY.coverArt, "image", "gif"));

        Collection<MIME> pngMimes = new HashSet<MIME>();
        pngMimes.add(new MIME(CATEGORY.coverArt, "image", "png"));

        Collection<MIME> wbmpMimes = new HashSet<MIME>();
        wbmpMimes.add(new MIME(CATEGORY.coverArt, "image", "vnd.wap.wbmp"));

        MIME_PICTURE_FORMAT.put(PICTURE_FORMAT.jpeg, jpegMimes);
        MIME_PICTURE_FORMAT.put(PICTURE_FORMAT.gif, gifMimes);
        MIME_PICTURE_FORMAT.put(PICTURE_FORMAT.png, pngMimes);
        MIME_PICTURE_FORMAT.put(PICTURE_FORMAT.wbmp, wbmpMimes);
    }
    public static final EnumMap<PICTURE_FORMAT, Collection<String>> FILE_EXTENSIONS_PICTURE_FORMAT = new EnumMap<PICTURE_FORMAT, Collection<String>>(PICTURE_FORMAT.class);

    static {
        Collection<String> jpegExts = new HashSet<String>();
        jpegExts.add("jpg");
                jpegExts.add("jpeg");
                jpegExts.add("jpe");
                jpegExts.add("jif");

        Collection<String> gifExts = new HashSet<String>();
        gifExts.add("gif");

        Collection<String> pngExts = new HashSet<String>();
        pngExts.add("png");

        Collection<String> wbmpsExts = new HashSet<String>();
        wbmpsExts.add("wbmp");

        FILE_EXTENSIONS_PICTURE_FORMAT.put(PICTURE_FORMAT.jpeg, jpegExts);
        FILE_EXTENSIONS_PICTURE_FORMAT.put(PICTURE_FORMAT.gif, gifExts);
        FILE_EXTENSIONS_PICTURE_FORMAT.put(PICTURE_FORMAT.png, pngExts);
        FILE_EXTENSIONS_PICTURE_FORMAT.put(PICTURE_FORMAT.wbmp, wbmpsExts);
    }

    private PictureFormat() {
    }
}
