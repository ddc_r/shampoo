/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.track.log;

import biz.ddcr.shampoo.server.domain.journal.Log.LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class TrackLog extends Log {

    public static enum TRACK_LOG_OPERATION implements LOG_OPERATION {

        processing,
        edit,
        delete,
        add,
        reject,
        validate;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case processing:
                        return systemConfigurationHelper.isLogTrackProcessing();
                    case add:
                        return systemConfigurationHelper.isLogTrackCreate();
                    case edit:
                        return systemConfigurationHelper.isLogTrackUpdate();
                    case delete:
                        return systemConfigurationHelper.isLogTrackDelete();
                    case reject:
                        return systemConfigurationHelper.isLogTrackReject();
                    case validate:
                        return systemConfigurationHelper.isLogTrackValidate();
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, Track track) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (track!=null && canLog(systemConfigurationHelper)) {
                Collection<Programme> ps = track.getImmutableProgrammes();
                for (Programme p : ps)
                    allBoundChannels.addAll(p.getChannels());
            }
            return allBoundChannels;
        }
    }
    /** The current operation performed on the actee **/
    private TRACK_LOG_OPERATION action;
    /** friendly captions to interpret the raw ID**/
    private String trackTitleCaption;
    private String trackAuthorCaption;

    protected TrackLog() {
        super();
    }

    @Override
    public TrackLog shallowCopy() {
        return new TrackLog(this);
    }
    
    public TrackLog(TRACK_LOG_OPERATION operation, String refID, String trackTitle, String trackAuthor, Set<Channel> channels) {
        super(refID, channels);
        setAction(operation);
        setTrackTitleCaption(trackTitle);
        setTrackAuthorCaption(trackAuthor);
    }

    public TrackLog(TrackLog o) {
        super(o);
        setAction(o.getAction());
        setTrackTitleCaption(o.getTrackTitleCaption());
        setTrackAuthorCaption(o.getTrackAuthorCaption());
    }

    @Override
    public TRACK_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(TRACK_LOG_OPERATION action) {
        this.action = action;
    }

    public String getTrackAuthorCaption() {
        return trackAuthorCaption;
    }

    public void setTrackAuthorCaption(String trackAuthorCaption) {
        this.trackAuthorCaption = trackAuthorCaption;
    }

    public String getTrackTitleCaption() {
        return trackTitleCaption;
    }

    public void setTrackTitleCaption(String trackTitleCaption) {
        this.trackTitleCaption = trackTitleCaption;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
