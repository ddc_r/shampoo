/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.track.notification;

import biz.ddcr.shampoo.server.domain.notification.CONTENT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.ContentUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PendingTrackContentNotification extends ContentUpdatedNotification<String> {

    @Override
    public String getFriendlyEntityCaption() {
        return getTrackAuthorCaption() + " - " + getTrackTitleCaption() + " [" + getEntityID() + "]";
    }

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case add:
                    return "Pending track "+getFriendlyEntityCaption()+" has been added";
                case delete:
                    return "Pending track "+getFriendlyEntityCaption()+" has been deleted";
                case processing:
                    return "Processing pending track "+getFriendlyEntityCaption();
                case edit:
                    return "Pending track "+getFriendlyEntityCaption()+" has been edited";
            }
        }
        return null;
    }

    public static enum PENDINGTRACK_NOTIFICATION_OPERATION implements CONTENT_NOTIFICATION_OPERATION {

        add,
        delete,
        processing,
        edit;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, PendingTrack pendingTrack) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {
                //Find recipients from the computed list of channels and programmes
                Collection<Programme> trackProgrammes = pendingTrack.getImmutableProgrammes();
                return securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                        Programme.getChannelsFromProgrammes(trackProgrammes),
                        trackProgrammes,
                        PendingTrack.getNotificationRights());
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private String trackAuthorCaption;
    private String trackTitleCaption;
    private PENDINGTRACK_NOTIFICATION_OPERATION action;

    protected PendingTrackContentNotification() {
        super();
    }

    @Override
    public PendingTrackContentNotification shallowCopy() {
        return new PendingTrackContentNotification(this);
    }
    
    public PendingTrackContentNotification(PENDINGTRACK_NOTIFICATION_OPERATION action, PendingTrack pendingTrack, RestrictedUser recipient) {
        super(pendingTrack!=null ? pendingTrack.getRefID() : null,
              recipient);
        setAction( action );
        setTrackAuthorCaption( pendingTrack!=null ? pendingTrack.getAuthor() : null );
        setTrackTitleCaption( pendingTrack!=null ? pendingTrack.getTitle() : null );
    }
    public PendingTrackContentNotification(PendingTrackContentNotification o) {
        super(o);
        setAction(o.getAction());
        setTrackAuthorCaption( o.getTrackAuthorCaption() );
        setTrackTitleCaption( o.getTrackTitleCaption() );
    }

    @Override
    public PENDINGTRACK_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(PENDINGTRACK_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }

    public String getTrackAuthorCaption() {
        return trackAuthorCaption;
    }

    protected void setTrackAuthorCaption(String trackAuthorCaption) {
        this.trackAuthorCaption = trackAuthorCaption;
    }

    public String getTrackTitleCaption() {
        return trackTitleCaption;
    }

    protected void setTrackTitleCaption(String trackTitleCaption) {
        this.trackTitleCaption = trackTitleCaption;
    }

}
