/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.domain.user;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.util.Set;

/**
 *
 * A user which is full administrative privileges, i.e. can access and control everything, superseeds the use of groups
 *
 * @author okay_awright
 **/
public class Administrator extends User {

    public Administrator() {
    }
    public Administrator(Administrator o) {
        super(o);
    }

    /**
     * 
     * Convenience method for HQL querying; otherwise unused
     * 
     * @return
     **/
    protected Set<Programme> getAnimatorRights() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setAnimatorRights(Set<Programme> animatorRights) {
        //Do nothing
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Programme> getContributorRights() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setContributorRights(Set<Programme> contributorRights) {
        //Do nothing
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Programme> getCuratorRights() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setCuratorRights(Set<Programme> curatorRights) {
        //Do nothing
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Channel> getListenerRights() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setListenerRights(Set<Channel> listenerRights) {
        //Do nothing
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Channel> getProgrammeManagerRights() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setProgrammeManagerRights(Set<Channel> programmeManagerRights) {
        //Do nothing
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected Set<Channel> getChannelAdministratorRights() {
        return null;
    }

    /**
     *
     * Convenience method for HQL querying; otherwise unused
     *
     * @return
     **/
    protected void setChannelAdministratorRights(Set<Channel> channelAdministratorRights) {
        //Do nothing
    }

    @Override
    public Administrator shallowCopy() {
        return new Administrator(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        Administrator test = ProxiedClassUtil.cast(obj, Administrator.class);
        return super.equals(test)
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        //none, only admins can do that
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        //none, only admins can do that
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }

    @Override
    public void acceptVisit(UserVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
