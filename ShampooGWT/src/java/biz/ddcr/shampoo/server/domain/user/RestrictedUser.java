/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.user;

import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.util.HashSet;
import java.util.Iterator;

import java.util.Set;


/**
 *
 * A user who can interact with specific sections of the frontend according to the groups he belongs to
 *
 * @author okay_awright
 **/
public class RestrictedUser extends User {

    private Set<Channel> channelAdministratorRights;
    private Set<Channel> programmeManagerRights;
    private Set<Channel> listenerRights;
    private Set<Programme> animatorRights;
    private Set<Programme> curatorRights;
    private Set<Programme> contributorRights;
    private Set<Voting> votes;
    private Set<Notification> notifications;

    private Set<Request> requests;

    /** misc. options**/
    private boolean emailNotification;

    public RestrictedUser() {
    }
    public RestrictedUser(RestrictedUser o) {
        super(o);
        addAnimatorRights( o.getAnimatorRights() );
        addChannelAdministratorRights( o.getChannelAdministratorRights() );
        addContributorRights( o.getContributorRights() );
        addCuratorRights( o.getCuratorRights() );
        addListenerRights( o.getListenerRights() );
        addProgrammeManagerRights( o.getProgrammeManagerRights() );
        addVotes( o.getVotes() );
        setEmailNotification( o.isEmailNotification() );
        addNotifications( o.getNotifications() );
        addRequests( o.getRequests() );
    }

    public boolean isEmailNotification() {
        return emailNotification;
    }

    public void setEmailNotification(boolean emailNotification) {
        this.emailNotification = emailNotification;
    }

    public boolean isAnimator(Programme programme) {
        return getAnimatorRights().contains(programme);
    }

    public boolean isAnimator() {
        return !getAnimatorRights().isEmpty();
    }

    public Set<Programme> getAnimatorRights() {
        if (animatorRights==null) animatorRights=new HashSet<Programme>();
        return animatorRights;
    }

    private void setAnimatorRights(Set<Programme> animatorRights) {
        this.animatorRights = animatorRights;
    }
    public void addAnimatorRight(Programme animatorRight) {
        if (animatorRight!=null) {
            getAnimatorRights().add(animatorRight);
            animatorRight.getAnimatorRights().add(this);
        }
    }
    public void addAnimatorRights(Set<Programme> animatorRights) {
        if (animatorRights!=null) {
            for (Programme animatorRight : animatorRights)
                addAnimatorRight(animatorRight);
        }
    }
    public void removeAnimatorRight(Programme animatorRight) {
        if (animatorRight!=null) {
            getAnimatorRights().remove(animatorRight);
            animatorRight.getAnimatorRights().remove(this);
        }
    }
    public void removeAnimatorRights(Set<Programme> animatorRights) {
        if (animatorRights!=null) {
            for (Programme animatorRight : animatorRights)
                removeAnimatorRight(animatorRight);
        }
    }
    public void clearAnimatorRights() {
         for (Programme animatorRight : getAnimatorRights())
            animatorRight.getAnimatorRights().remove(this);
         //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getAnimatorRights().clear();
        //animatorRights = new HashSet<Programme>();
    }

    public boolean isContributor(Programme programme) {
        return getContributorRights().contains(programme);
    }

    public boolean isContributor() {
        return !getContributorRights().isEmpty();
    }

    public Set<Programme> getContributorRights() {
        if (contributorRights==null) contributorRights=new HashSet<Programme>();
        return contributorRights;
    }

    private void setContributorRights(Set<Programme> contributorRights) {
        this.contributorRights = contributorRights;
    }
    public void addContributorRight(Programme contributorRight) {
        if (contributorRight!=null) {
            getContributorRights().add(contributorRight);
            contributorRight.getContributorRights().add(this);
        }
    }
    public void addContributorRights(Set<Programme> contributorRights) {
        if (contributorRights!=null) {
            for (Programme contributorRight : contributorRights)
                addContributorRight(contributorRight);
        }
    }
    public void removeContributorRight(Programme contributorRight) {
        if (contributorRight!=null) {
            getContributorRights().remove(contributorRight);
            contributorRight.getContributorRights().remove(this);
        }
    }
    public void removeContributorRights(Set<Programme> contributorRights) {
        if (contributorRights!=null) {
            for (Programme contributorRight : contributorRights)
                removeContributorRight(contributorRight);
        }
    }
    public void clearContributorRights() {
         for (Programme contributorRight : getContributorRights())
            contributorRight.getContributorRights().remove(this);
         //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getContributorRights().clear();
        //contributorRights = new HashSet<Programme>();
    }

    public boolean isCurator(Programme programme) {
        return getCuratorRights().contains(programme);
    }

    public boolean isCurator() {
        return !getCuratorRights().isEmpty();
    }

    public Set<Programme> getCuratorRights() {
        if (curatorRights==null) curatorRights=new HashSet<Programme>();
        return curatorRights;
    }

    private void setCuratorRights(Set<Programme> curatorRights) {
        this.curatorRights = curatorRights;
    }
    public void addCuratorRight(Programme curatorRight) {
        if (curatorRight!=null) {
            getCuratorRights().add(curatorRight);
            curatorRight.getCuratorRights().add(this);
        }
    }
    public void addCuratorRights(Set<Programme> curatorRights) {
        if (curatorRights!=null) {
            for (Programme curatorRight : curatorRights)
                addCuratorRight(curatorRight);
        }
    }
    public void removeCuratorRight(Programme curatorRight) {
        if (curatorRight!=null) {
            getCuratorRights().remove(curatorRight);
            curatorRight.getCuratorRights().remove(this);
        }
    }
    public void removeCuratorRights(Set<Programme> curatorRights) {
        if (curatorRights!=null) {
            for (Programme curatorRight : curatorRights)
                removeCuratorRight(curatorRight);
        }
    }
    public void clearCuratorRights() {
         for (Programme curatorRight : getCuratorRights())
            curatorRight.getCuratorRights().remove(this);
         //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getCuratorRights().clear();
        //curatorRights = new HashSet<Programme>();
    }

    public boolean isListener(Channel channel) {
        return getListenerRights().contains(channel);
    }

    public boolean isListener() {
        return !getListenerRights().isEmpty();
    }

    public Set<Channel> getListenerRights() {
        if (listenerRights==null) listenerRights=new HashSet<Channel>();
        return listenerRights;
    }

    private void setListenerRights(Set<Channel> listenerRights) {
        this.listenerRights = listenerRights;
    }
    public void addListenerRight(Channel listenerRight) {
        if (listenerRight!=null) {
            getListenerRights().add(listenerRight);
            listenerRight.getListenerRights().add(this);
        }
    }
    public void addListenerRights(Set<Channel> listenerRights) {
        if (listenerRights!=null) {
            for (Channel listenerRight : listenerRights)
                addListenerRight(listenerRight);
        }
    }
    public void removeListenerRight(Channel listenerRight) {
        if (listenerRight!=null) {
            getListenerRights().remove(listenerRight);
            listenerRight.getListenerRights().remove(this);
        }
    }
    public void removeListenerRights(Set<Channel> listenerRights) {
        if (listenerRights!=null) {
            for (Channel listenerRight : listenerRights)
                removeListenerRight(listenerRight);
        }
    }
    public void clearListenerRights() {
         for (Channel listenerRight : getListenerRights())
            listenerRight.getListenerRights().remove(this);
         //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getListenerRights().clear();
        //listenerRights = new HashSet<Channel>();
    }

    public boolean isProgrammeManager(Channel channel) {
        return getProgrammeManagerRights().contains(channel);
    }

    public boolean isProgrammeManager() {
        return !getProgrammeManagerRights().isEmpty();
    }

    public Set<Channel> getProgrammeManagerRights() {
        if (programmeManagerRights==null) programmeManagerRights=new HashSet<Channel>();
        return programmeManagerRights;
    }

    private void setProgrammeManagerRights(Set<Channel> programmeManagerRights) {
        this.programmeManagerRights = programmeManagerRights;
    }
    public void addProgrammeManagerRight(Channel programmeManagerRight) {
        if (programmeManagerRight!=null) {
            getProgrammeManagerRights().add(programmeManagerRight);
            programmeManagerRight.getProgrammeManagerRights().add(this);
        }
    }
    public void addProgrammeManagerRights(Set<Channel> programmeManagerRights) {
        if (programmeManagerRights!=null) {
            for (Channel programmeManagerRight : programmeManagerRights)
                addProgrammeManagerRight(programmeManagerRight);
        }
    }
    public void removeProgrammeManagerRight(Channel programmeManagerRight) {
        if (programmeManagerRight!=null) {
            getProgrammeManagerRights().remove(programmeManagerRight);
            programmeManagerRight.getProgrammeManagerRights().remove(this);
        }
    }
    public void removeProgrammeManagerRights(Set<Channel> programmeManagerRights) {
        if (programmeManagerRights!=null) {
            for (Channel programmeManagerRight : programmeManagerRights)
                removeProgrammeManagerRight(programmeManagerRight);
        }
    }
    public void clearProgrammeManagerRights() {
         for (Channel programmeManagerRight : getListenerRights())
            programmeManagerRight.getProgrammeManagerRights().remove(this);
          //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getProgrammeManagerRights().clear();
        //programmeManagerRights = new HashSet<Channel>();
    }

    public boolean isChannelAdministrator(Channel channel) {
        return getChannelAdministratorRights().contains(channel);
    }

    public boolean isChannelAdministrator() {
        return !getChannelAdministratorRights().isEmpty();
    }

    public Set<Channel> getChannelAdministratorRights() {
        if (channelAdministratorRights==null) channelAdministratorRights=new HashSet<Channel>();
        return channelAdministratorRights;
    }

    private void setChannelAdministratorRights(Set<Channel> channelAdministratorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
    }
    public void addChannelAdministratorRight(Channel channelAdministratorRight) {
        if (channelAdministratorRight!=null) {
            getChannelAdministratorRights().add(channelAdministratorRight);
            channelAdministratorRight.getChannelAdministratorRights().add(this);
        }
    }
    public void addChannelAdministratorRights(Set<Channel> channelAdministratorRights) {
        if (channelAdministratorRights!=null) {
            for (Channel channelAdministratorRight : channelAdministratorRights)
                addChannelAdministratorRight(channelAdministratorRight);
        }
    }
    public void removeChannelAdministratorRight(Channel channelAdministratorRight) {
        if (channelAdministratorRight!=null) {
            getChannelAdministratorRights().remove(channelAdministratorRight);
            channelAdministratorRight.getChannelAdministratorRights().remove(this);
        }
    }
    public void removeChannelAdministratorRights(Set<Channel> channelAdministratorRights) {
        if (channelAdministratorRights!=null) {
            for (Channel channelAdministratorRight : channelAdministratorRights)
                removeChannelAdministratorRight(channelAdministratorRight);
        }
    }
    public void clearChannelAdministratorRights() {
         for (Channel channelAdministratorRight : getListenerRights())
            channelAdministratorRight.getChannelAdministratorRights().remove(this);
          //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getChannelAdministratorRights().clear();
        //channelAdministratorRights = new HashSet<Channel>();
    }

    public Set<Channel> getImmutableChannels() {
        Set<Channel> labels = new HashSet<Channel>();
        labels.addAll(getChannelAdministratorRights());
        labels.addAll(getProgrammeManagerRights());
        labels.addAll(getListenerRights());
        return labels;
    }

    public Set<Programme> getImmutableProgrammes() {
        Set<Programme> labels = new HashSet<Programme>();
        labels.addAll(getCuratorRights());
        labels.addAll(getContributorRights());
        labels.addAll(getAnimatorRights());
        return labels;
    }

    public Set<Voting> getVotes() {
        if (votes==null) votes=new HashSet<Voting>();
        return votes;
    }

    private void setVotes(Set<Voting> votes) {
        this.votes = votes;
    }
    public void addVote(Voting vote) {
        addVote(vote, true);
    }
    public void addVote(Voting vote, boolean doLink) {
        if (vote!=null) {
            getVotes().add(vote);
            if (doLink) vote.addRestrictedUser(this, false);
        }
    }
    public void addVotes(Set<Voting> votes) {
        if (votes!=null) {
            for (Voting vote : votes)
                addVote(vote);
        }
    }
    public void removeVote(Voting vote) {
        removeVote(vote, true);
    }
    public void removeVote(Voting vote, boolean doLink) {
        if (vote != null) {            
            vote.drop(doLink, true);
        }
    }
    public void removeVotes(Set<Voting> votes) {
        if (votes!=null) {
            for (Voting vote : votes)
                removeVote(vote);
        }
    }
    public void clearVotes() {
        for (Iterator<Voting> i = getVotes().iterator();i.hasNext();) {
            Voting vote = i.next();
            i.remove();
            //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
            //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
            vote.drop(false, true);
        }
    }

    public Set<Notification> getNotifications() {
        if (notifications==null) notifications=new HashSet<Notification>();
        return notifications;
    }

    private void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }
    public void addNotification(Notification notification) {
        addNotification(notification, true);
    }
    public void addNotification(Notification notification, boolean doLink) {
        if (notification!=null) {
            getNotifications().add(notification);
            if (doLink) notification.addRecipient(this, false);
        }
    }
    public void addNotifications(Set<Notification> notifications) {
        if (notifications!=null) {
            for (Notification notification : notifications)
                addNotification(notification);
        }
    }
    public void removeNotification(Notification notification) {
        removeNotification(notification, true);
    }
    public void removeNotification(Notification notification, boolean doLink) {
        if (notification!=null) {
            getNotifications().remove(notification);
            if (doLink) notification.removeRecipient(false);
        }
    }
    public void removeNotifications(Set<Notification> notifications) {
        if (notifications!=null) {
            for (Notification notification : notifications)
                removeNotification(notification);
        }
    }
    public void clearNotifications() {
         for (Notification notification : getNotifications())
            notification.removeRecipient(false);
          //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
        //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
         //FIX: nope, use clear(), because it doesn't work as stated, dereferencing will just make Hibernate forget about the past relationship
        getNotifications().clear();
        //notifications = new HashSet<Notification>();
    }

    public Set<Request> getRequests() {
        if (requests == null) {
            requests = new HashSet<Request>();
        }
        return requests;
    }

    private void setRequests(Set<Request> requests) {
        this.requests = requests;
    }

    public void addRequest(Request request) {
        addRequest(request, true);
    }

    public void addRequest(Request request, boolean doLink) {
        if (request != null) {
            getRequests().add(request);
            if (doLink) {
                request.addRequester(this, false);
            }
        }
    }

    public void addRequests(Set<Request> requests) {
        if (requests != null) {
            for (Request request : requests) {
                addRequest(request);
            }
        }
    }

    public void removeRequest(Request request) {
        removeRequest(request, true);
    }

    public void removeRequest(Request request, boolean doLink) {
        if (request != null) {
            /*getRequests().remove(request);
            if (doLink) {
                request.removeRequester(false);
            }*/
            request.drop(true, true, doLink);
        }
    }

    public void removeRequests(Set<Request> requests) {
        if (requests != null) {
            for (Request request : requests) {
                removeRequest(request);
            }
        }
    }

    public void clearRequests() {
        for (Iterator<Request> i = getRequests().iterator();i.hasNext();) {
            Request request = i.next();
            i.remove();
            //Don't clear() but de-reference the original collection in order to perform deletions before insertions (otherwise primary key violations are most likely expected)
            //Remove individual entries instead if most of the original entries are meant to be reinserted afterward
            request.drop(true, true, false);
        }
    }

    @Override
    public RestrictedUser shallowCopy() {
        return new RestrictedUser(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        RestrictedUser test = ProxiedClassUtil.cast(obj, RestrictedUser.class);
        return super.equals(test);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getLimitedProgrammeBindingRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        rightsBundle.setProgrammeManagerRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }
    public static RightsBundle getNotificationRights() {
        return getUpdateRights();
    }

    @Override
    public void acceptVisit(UserVisitor visitor) throws Exception {
        visitor.visit(this);
    }

}
