package biz.ddcr.shampoo.server.domain.user;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.VisitorPatternInterface;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.util.Collection;

/**
 * An individual who can login and actually manage data through the frontend according to his credentials
 *
 * @author okay_awright
 */
public abstract class User extends GenericTimestampedEntity implements Comparable<User>, VisitorPatternInterface<UserVisitor> {

    private String username;
    private String password;
    private String firstAndLastName;
    private String email;
    private boolean enabled;
    /**
     * the current user time zone
     **/
    private String timezone;

    /**phony fields -> Hibernate kludge for nullifying objects' 'creator' and 'latesteditor' fields when the user is suppressed*/
    private Collection<Archive> createdArchives;
    private Collection<Channel> createdChannels;
    private Collection<Playlist> createdPlaylists;
    private Collection<Programme> createdProgrammes;
    private Collection<Request> createdRequests;
    private Collection<TimetableSlot> createdTimetableSlots;
    private Collection<Track> createdTracks;
    private Collection<User> createdUsers;
    private Collection<Voting> createdVotes;
    private Collection<Webservice> createdWebservices;
    private Collection<Archive> editedArchives;
    private Collection<Channel> editedChannels;
    private Collection<Playlist> editedPlaylists;
    private Collection<Programme> editedProgrammes;
    private Collection<Request> editedRequests;
    private Collection<TimetableSlot> editedTimetableSlots;
    private Collection<Track> editedTracks;
    private Collection<User> editedUsers;
    private Collection<Voting> editedVotes;
    private Collection<Webservice> editedWebservices;

    public User() {
    }

    public User(User o) {
        super(o);
        setEmail(o.getEmail());
        setEnabled(o.isEnabled());
        setFirstAndLastName(o.getFirstAndLastName());
        setPassword(o.getPassword());
        setTimezone(o.getTimezone());
        setUsername(o.getUsername());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstAndLastName() {
        return firstAndLastName;
    }

    public void setFirstAndLastName(String name) {
        firstAndLastName = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String login) {
        username = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean active) {
        enabled = active;
    }

    public boolean isAccountNonExpired() {
        //Not actually used
        return isEnabled();
    }

    public boolean isAccountNonLocked() {
        //Not actually used
        return isEnabled();
    }

    public boolean isCredentialsNonExpired() {
        //Not actually used
        return isEnabled();
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be User at this point
        User test = ProxiedClassUtil.cast(obj, User.class);
        return /*super.equals(test)
                &&*/ (getUsername().equals(test.getUsername()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getUsername() ? 0 : getUsername().hashCode());
        return hash;
    }

    @Override
    public int compareTo(User anotherUser) {
        if (anotherUser != null) {
            return (this.getUsername() == null
                    ? 0
                    : (anotherUser.getUsername() == null
                    ? 0
                    : getUsername().compareTo(anotherUser.getUsername())));
        } else {
            return 0;
        }
    }

    public Collection<Archive> getCreatedArchives() {
        return createdArchives;
    }

    public void setCreatedArchives(Collection<Archive> createdArchives) {
        this.createdArchives = createdArchives;
    }

    public Collection<Channel> getCreatedChannels() {
        return createdChannels;
    }

    public void setCreatedChannels(Collection<Channel> createdChannels) {
        this.createdChannels = createdChannels;
    }

    public Collection<Playlist> getCreatedPlaylists() {
        return createdPlaylists;
    }

    public void setCreatedPlaylists(Collection<Playlist> createdPlaylists) {
        this.createdPlaylists = createdPlaylists;
    }

    public Collection<Programme> getCreatedProgrammes() {
        return createdProgrammes;
    }

    public void setCreatedProgrammes(Collection<Programme> createdProgrammes) {
        this.createdProgrammes = createdProgrammes;
    }

    public Collection<Request> getCreatedRequests() {
        return createdRequests;
    }

    public void setCreatedRequests(Collection<Request> createdRequests) {
        this.createdRequests = createdRequests;
    }

    public Collection<TimetableSlot> getCreatedTimetableSlots() {
        return createdTimetableSlots;
    }

    public void setCreatedTimetableSlots(Collection<TimetableSlot> createdTimetableSlots) {
        this.createdTimetableSlots = createdTimetableSlots;
    }

    public Collection<Track> getCreatedTracks() {
        return createdTracks;
    }

    public void setCreatedTracks(Collection<Track> createdTracks) {
        this.createdTracks = createdTracks;
    }

    public Collection<User> getCreatedUsers() {
        return createdUsers;
    }

    public void setCreatedUsers(Collection<User> createdUsers) {
        this.createdUsers = createdUsers;
    }

    public Collection<Voting> getCreatedVotes() {
        return createdVotes;
    }

    public void setCreatedVotes(Collection<Voting> createdVotes) {
        this.createdVotes = createdVotes;
    }

    public Collection<Webservice> getCreatedWebservices() {
        return createdWebservices;
    }

    public void setCreatedWebservices(Collection<Webservice> createdWebservices) {
        this.createdWebservices = createdWebservices;
    }

    public Collection<Archive> getEditedArchives() {
        return editedArchives;
    }

    public void setEditedArchives(Collection<Archive> editedArchives) {
        this.editedArchives = editedArchives;
    }

    public Collection<Channel> getEditedChannels() {
        return editedChannels;
    }

    public void setEditedChannels(Collection<Channel> editedChannels) {
        this.editedChannels = editedChannels;
    }

    public Collection<Playlist> getEditedPlaylists() {
        return editedPlaylists;
    }

    public void setEditedPlaylists(Collection<Playlist> editedPlaylists) {
        this.editedPlaylists = editedPlaylists;
    }

    public Collection<Programme> getEditedProgrammes() {
        return editedProgrammes;
    }

    public void setEditedProgrammes(Collection<Programme> editedProgrammes) {
        this.editedProgrammes = editedProgrammes;
    }

    public Collection<Request> getEditedRequests() {
        return editedRequests;
    }

    public void setEditedRequests(Collection<Request> editedRequests) {
        this.editedRequests = editedRequests;
    }

    public Collection<TimetableSlot> getEditedTimetableSlots() {
        return editedTimetableSlots;
    }

    public void setEditedTimetableSlots(Collection<TimetableSlot> editedTimetableSlots) {
        this.editedTimetableSlots = editedTimetableSlots;
    }

    public Collection<Track> getEditedTracks() {
        return editedTracks;
    }

    public void setEditedTracks(Collection<Track> editedTracks) {
        this.editedTracks = editedTracks;
    }

    public Collection<User> getEditedUsers() {
        return editedUsers;
    }

    public void setEditedUsers(Collection<User> editedUsers) {
        this.editedUsers = editedUsers;
    }

    public Collection<Voting> getEditedVotes() {
        return editedVotes;
    }

    public void setEditedVotes(Collection<Voting> editedVotes) {
        this.editedVotes = editedVotes;
    }

    public Collection<Webservice> getEditedWebservices() {
        return editedWebservices;
    }

    public void setEditedWebservices(Collection<Webservice> editedWebservices) {
        this.editedWebservices = editedWebservices;
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
