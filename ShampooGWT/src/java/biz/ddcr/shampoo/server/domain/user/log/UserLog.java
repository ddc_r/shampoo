/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.user.log;

import biz.ddcr.shampoo.server.domain.journal.Log.LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class UserLog extends Log {

    private interface ChannelsUserVisitor extends UserVisitor {

        public Set<Channel> get();
    }

    public static enum USER_LOG_OPERATION implements LOG_OPERATION {

        edit,
        delete,
        add;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case add:
                        return systemConfigurationHelper.isLogUserCreate();
                    case edit:
                        return systemConfigurationHelper.isLogUserUpdate();
                    case delete:
                        return systemConfigurationHelper.isLogUserDelete();
                }
            }
            return false;
        }

        /**
         * Return all channels linked to this entity, either directly or indirectly
         * The returned collection is not mutable
         * @return
         */
        public Set<Channel> getChannels(final SystemConfigurationHelper systemConfigurationHelper, User user) throws Exception {
            ChannelsUserVisitor visitor = new ChannelsUserVisitor() {

                private Set<Channel> allBoundChannels;

                @Override
                public Set<Channel> get() {
                    return allBoundChannels;
                }

                @Override
                public void visit(RestrictedUser restrictedUser) throws Exception {
                    allBoundChannels = new HashSet<Channel>();
                    if (restrictedUser != null && canLog(systemConfigurationHelper)) {
                        allBoundChannels.addAll(restrictedUser.getChannelAdministratorRights());
                        allBoundChannels.addAll(restrictedUser.getProgrammeManagerRights());
                        allBoundChannels.addAll(restrictedUser.getListenerRights());
                        for (Programme p : restrictedUser.getCuratorRights()) {
                            allBoundChannels.addAll(p.getChannels());
                        }
                        for (Programme p : restrictedUser.getContributorRights()) {
                            allBoundChannels.addAll(p.getChannels());
                        }
                        for (Programme p : restrictedUser.getAnimatorRights()) {
                            allBoundChannels.addAll(p.getChannels());
                        }
                    }
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    allBoundChannels = null;
                }
            };
            user.acceptVisit(visitor);
            return visitor.get();
        }
    }
    /** The current operation performed on the actee **/
    private USER_LOG_OPERATION action;

    protected UserLog() {
        super();
    }

    @Override
    public UserLog shallowCopy() {
        return new UserLog(this);
    }
    
    public UserLog(USER_LOG_OPERATION operation, String username, Set<Channel> channels) {
        super(username, channels);
        setAction(operation);
    }

    public UserLog(UserLog o) {
        super(o);
        setAction(o.getAction());
    }

    @Override
    public USER_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(USER_LOG_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
