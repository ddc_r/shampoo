/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.user.notification;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.notification.CONTENT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.notification.ContentUpdatedNotification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class RestrictedUserContentNotification extends ContentUpdatedNotification<String> {

    @Override
    public String getFriendlyEnglishCaption() {
        if (getAction()!=null) {
            switch (getAction()) {
                case add:
                    return "User "+getFriendlyEntityCaption()+" has been added";
                case delete:
                    return "User "+getFriendlyEntityCaption()+" has been deleted";
                case edit:
                    return "User "+getFriendlyEntityCaption()+" has been edited";                   
            }
        }
        return null;
    }

    @Override
    public String getFriendlyEntityCaption() {
        return getEntityID();
    }

    public static enum RESTRICTEDUSER_NOTIFICATION_OPERATION implements CONTENT_NOTIFICATION_OPERATION {

        add,
        delete,
        edit;

        @Override
        public boolean canNotify(SystemConfigurationHelper systemConfigurationHelper) {
            //TODO: implement it based on system-wide preferences
            return true;
        }

        /** returns the list of users that should get this notification **/
        public Collection<RestrictedUser> getRecipients(SecurityManagerInterface securityManager, RestrictedUser restrictedUser) throws Exception {
            if (securityManager != null && canNotify(securityManager.getSystemConfigurationHelper())) {
                //Find recipients from the computed list of channels and restrictedUsers
                //Add channels from programmes, as all user-based decisions
                Collection<Channel> channels = restrictedUser.getImmutableChannels();
                Collection<Programme> programmes = restrictedUser.getImmutableProgrammes();
                channels.addAll(Programme.getChannelsFromProgrammes(programmes));
                return securityManager.getRestrictedUsersWithRightsOverChannelsOrProgrammes(
                        channels,
                        programmes,
                        RestrictedUser.getNotificationRights());
            }
            return new HashSet<RestrictedUser>();
        }
    }

    private RESTRICTEDUSER_NOTIFICATION_OPERATION action;

    protected RestrictedUserContentNotification() {
        super();
    }

    @Override
    public RestrictedUserContentNotification shallowCopy() {
        return new RestrictedUserContentNotification(this);
    }
    
    public RestrictedUserContentNotification(RESTRICTEDUSER_NOTIFICATION_OPERATION action, RestrictedUser restrictedUser, RestrictedUser recipient) {
        super(restrictedUser!=null ? restrictedUser.getUsername() : null,
              recipient);
        setAction( action );
    }
    public RestrictedUserContentNotification(RestrictedUserContentNotification o) {
        super( o );
        setAction( o.getAction() );
    }

    @Override
    public RESTRICTEDUSER_NOTIFICATION_OPERATION getAction() {
        return action;
    }

    protected void setAction(RESTRICTEDUSER_NOTIFICATION_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(NotificationVisitor visitor) throws Exception {
        visitor.visit(this);
    }


}
