/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.domain.webservice;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * Access timestamp to a webservice
 *
 * @author okay_awright
 **/
public class Hit implements GenericEntityInterface, Comparable<Hit> {

    /** Used for Hibernate sorting of SortedSets, it must be static! **/
    /** But Ehcache needs it to be Serializable vene if static **/
    public static class HibernateComparator implements Serializable, Comparator<Hit> {

        @Override
        public int compare(Hit t, Hit t1) {
            if (t==null)
                return t1==null?0:-1;
            else
                return t.compareTo(t1);
        }

    }

    /** Hibernate versioning */
    private int version;
    private HitID refID;

    protected Hit() {
    }
    public Hit(Webservice webservice, long timestamp) {
        addWebservice(webservice);
        setTimestamp(timestamp);
    }
    public Hit(Hit o) {
        addWebservice(o.getWebservice());
        setTimestamp(o.getTimestamp());
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public void setVersion(int version) {
        this.version = version;
    }

    /** Hibernate artifact**/
    protected HitID getRefID() {
        if (refID == null) {
            refID = new HitID();
        }
        return refID;
    }

    /** Hibernate artifact**/
    protected void setRefID(HitID refID) {
        this.refID = refID;
    }

    public long getTimestamp() {
        return getRefID().getTimestamp();
    }

    public void setTimestamp(long timestamp) {
        getRefID().setTimestamp(timestamp);
    }

    public Webservice getWebservice() {
        return getRefID().getWebservice();
    }

    /** Hibernate setter **/
    private void setWebservice(Webservice webservice) {
        getRefID().setWebservice(webservice);
    }

    public void addWebservice(Webservice webservice) {
        addWebservice(webservice, true);
    }

    public void addWebservice(Webservice webservice, boolean doLink) {
        if (webservice != null) {
            if (doLink) {
                webservice.addHit(this, false);
            }
            setWebservice(webservice);
        }
    }

    public void removeWebservice() {
        removeWebservice(true);
    }

    public void removeWebservice(boolean doLink) {
        if (getWebservice() != null) {
            if (doLink) {
                getWebservice().removeHit(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setWebservice(null);
        }
    }

    @Override
    public Hit shallowCopy() {
        return new Hit(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        Hit test = ProxiedClassUtil.cast(obj, Hit.class);
        return /*super.equals(test)
                &&*/ (getRefID().equals(test.getRefID()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getRefID() ? 0 : getRefID().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(Hit t) {
        if (t==null)
            return 1;
        return this.getRefID().compareTo(t.getRefID());
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
