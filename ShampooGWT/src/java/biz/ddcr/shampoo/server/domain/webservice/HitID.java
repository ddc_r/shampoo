/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.webservice;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.io.Serializable;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class HitID implements Serializable, Comparable<HitID> {

    private long timestamp;
    private Webservice webservice;

    protected HitID() {
    }

    public Webservice getWebservice() {
        return webservice;
    }

    public void setWebservice(Webservice webservice) {
        this.webservice = webservice;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        HitID test = ProxiedClassUtil.cast(obj, HitID.class);
        return /*super.equals(test)
                &&*/ (getWebservice() == test.getWebservice() || (getWebservice() != null && getWebservice().equals(test.getWebservice())))
                && (getTimestamp() == test.getTimestamp());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + (int) (getTimestamp() ^ (getTimestamp() >>> 32));
        hash = 73 * hash + (getWebservice() != null ? getWebservice().hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(HitID anotherHitID) {
        //Sort by webservice, then by timestamp
        //Sorted through timestamp descending filter
        int result = 0;
        if (anotherHitID!=null)
            result = (getWebservice() == null ?
                (anotherHitID.getWebservice()==null?0:-1)
                : getWebservice().compareTo(anotherHitID.getWebservice()) );
        else
            return 1;
        if (result==0) {
            long diff = anotherHitID.getTimestamp() - getTimestamp();
            return diff==0?0:(diff>0?1:-1);
        } else
            return 0;
    }
}
