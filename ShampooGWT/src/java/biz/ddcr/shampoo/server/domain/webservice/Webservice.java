/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.domain.webservice;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 *
 * A webservice access rights bean
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class Webservice extends GenericTimestampedEntity implements Comparable<Webservice> {

    /** enum that allows to compile at compile time the list of strings that are used within the switch/case of lookup() */
    public static enum MODULE {
        nowPlaying,
        comingNext,
        vote,
        request,
        history,
        timetable,
        download;
    }

    private String apiKey;
    private String privateKey;
    private Channel channel;
    /** Min. time interval between two calls to the webservice**/
    private Long maxFireRate;
    /** Max. number of requests per day **/
    private Long maxDailyLimit;
    private SortedSet<Hit> hits;
    /** IP whitelist, null means no restriction */
    private Pattern whitelistRegexp;
    /** Activated features**/
    private boolean nowPlayingModule;
    private boolean comingNextModule;
    private boolean timetableModule;
    private boolean archiveModule;
    private boolean requestModule;
    private boolean voteModule;

    public Webservice() {}

    public Webservice(Webservice o) {
        setApiKey(o.getApiKey());
        setPrivateKey(o.getPrivateKey());
        addChannel(o.getChannel());
        setMaxFireRate(o.getMaxFireRate());
        addHits(o.getHits());
        setWhitelistRegexp(o.getWhitelistRegexp());
        setNowPlayingModule(o.isNowPlayingModule());
        setComingNextModule(o.isComingNextModule());
        setTimetableModule(o.isTimetableModule());
        setArchiveModule(o.isArchiveModule());
        setRequestModule(o.isRequestModule());
        setVoteModule(o.isVoteModule());
        setMaxDailyLimit(o.getMaxDailyLimit());
    }
    
    public String getWhitelistRegexp() {
        return whitelistRegexp!=null?whitelistRegexp.pattern():null;
    }

    public void setWhitelistRegexp(String whitelistRegexp) {
        if (whitelistRegexp!=null)
            this.whitelistRegexp = Pattern.compile(whitelistRegexp);
    }

    public Long getMaxDailyLimit() {
        return maxDailyLimit;
    }

    public void setMaxDailyLimit(Long maxDailyLimit) {
        this.maxDailyLimit = maxDailyLimit;
    }

    public boolean isNowPlayingModule() {
        return nowPlayingModule;
    }

    public void setNowPlayingModule(boolean nowPlayingModule) {
        this.nowPlayingModule = nowPlayingModule;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean isArchiveModule() {
        return archiveModule;
    }

    public void setArchiveModule(boolean archiveModule) {
        this.archiveModule = archiveModule;
    }

    public Channel getChannel() {
        return channel;
    }
        
    /** Hibernate setter **/
    private void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void addChannel(Channel channel) {
        addChannel(channel, true);
    }

    public void addChannel(Channel channel, boolean doLink) {
        if (channel != null) {
            if (doLink) {
                channel.addWebservice(this, false);
            }
            setChannel(channel);
        }
    }

    public void removeChannel() {
        removeChannel(true);
    }

    public void removeChannel(boolean doLink) {
        if (getChannel() != null) {
            if (doLink) {
                getChannel().removeWebservice(this, false);
            }
            //Put delete-orphan in the hibernate mapping
            setChannel(null);
        }
    }

    public boolean isComingNextModule() {
        return comingNextModule;
    }

    public void setComingNextModule(boolean comingNextModule) {
        this.comingNextModule = comingNextModule;
    }

    public SortedSet<Hit> getHits() {
        if (hits == null) {
            hits = new TreeSet<Hit>();
        }
        return hits;
    }

    /** Hibernate setter **/
    private void setHits(SortedSet<Hit> hits) {
        this.hits = hits;
    }

    public void addHit(Hit hit) {
        addHit(hit, true);
    }

    public void addHit(Hit hit, boolean doLink) {
        if (hit != null) {
            getHits().add(hit);
            if (doLink) {
                hit.addWebservice(this, false);
            }
        }
    }

    public void addHits(Set<Hit> hits) {
        if (hits != null) {
            for (Hit hit : hits) {
                addHit(hit);
            }
        }
    }

    public void removeHit(Hit hit) {
        removeHit(hit, true);
    }

    public void removeHit(Hit hit, boolean doLink) {
        if (hit != null) {
            getHits().remove(hit);
            if (doLink) {
                hit.removeWebservice(false);
            }
        }
    }

    public void removeHits(Set<Hit> hits) {
        if (hits != null) {
            for (Hit hit : hits) {
                removeHit(hit);
            }
        }
    }

    public void clearHits() {
        for (Hit hit : getHits()) {
            hit.removeWebservice(false);
        }
        getHits().clear();
    }

    public Long getMaxFireRate() {
        return maxFireRate;
    }

    public void setMaxFireRate(Long maxFireRate) {
        this.maxFireRate = maxFireRate;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public boolean isRequestModule() {
        return requestModule;
    }

    public void setRequestModule(boolean requestModule) {
        this.requestModule = requestModule;
    }

    public boolean isTimetableModule() {
        return timetableModule;
    }

    public void setTimetableModule(boolean timetableModule) {
        this.timetableModule = timetableModule;
    }

    public boolean isVoteModule() {
        return voteModule;
    }

    public void setVoteModule(boolean voteModule) {
        this.voteModule = voteModule;
    }

    public boolean isModuleOn(MODULE module) {
        if (module!=null) {
            //Resolve the num and see if the module is on
            switch (module) {
                case nowPlaying: return isNowPlayingModule();
                case comingNext: return isComingNextModule();
                case timetable: return isTimetableModule();
                case history: return isArchiveModule();
                case vote: return isVoteModule();
                case request: return isRequestModule();
                /** as of now thhe following module is activated by default**/
                case download: return true;
            }
        }
        return false;
    }

    @Override
    public Webservice shallowCopy() {
        return new Webservice(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || !ProxiedClassUtil.isSameClass(obj, this)) {
            return false;
        }
        // object must be Channel at this point
        Webservice test = ProxiedClassUtil.cast(obj, Webservice.class);
        return /*super.equals(test)
                &&*/ (getApiKey().equals(test.getApiKey()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        //hash = 31 * hash + super.hashCode();
        hash = 31 * hash + (null == getApiKey() ? 0 : getApiKey().hashCode());
        //No lazily loaded objects can be used
        return hash;
    }

    public static RightsBundle getViewRights() {
        //At least one request must return true to grant access
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getAddRights() {
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }
    public static RightsBundle getUpdateRights() {
        return getAddRights();
    }
    public static RightsBundle getDeleteRights() {
        return getAddRights();
    }
    public static RightsBundle getNotificationRights() {
        RightsBundle rightsBundle = new RightsBundle();
        rightsBundle.setChannelAdministratorRights(true);
        return rightsBundle;
    }

    @Override
    public int compareTo(Webservice anotherWebservice) {
        if (anotherWebservice!=null)
            return (getApiKey() == null ?
                (anotherWebservice.getApiKey()==null?0:-1)
                : getApiKey().compareTo(anotherWebservice.getApiKey()) );
        else
            return 1;
    }

    @Override
    public void acceptDomainVisit(DomainVisitor visitor) throws Exception {
        visitor.visit(this);
    }
    
}
