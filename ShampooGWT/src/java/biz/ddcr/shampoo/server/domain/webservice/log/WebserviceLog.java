/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.domain.webservice.log;

import biz.ddcr.shampoo.server.domain.journal.Log.LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class WebserviceLog extends Log {

    public static enum WEBSERVICE_LOG_OPERATION implements LOG_OPERATION {

        edit,
        delete,
        add;

        @Override
        public boolean canLog(SystemConfigurationHelper systemConfigurationHelper) {
            if (systemConfigurationHelper != null) {
                switch (this) {
                    case add:
                        return systemConfigurationHelper.isLogWebserviceCreate();
                    case edit:
                        return systemConfigurationHelper.isLogWebserviceUpdate();
                    case delete:
                        return systemConfigurationHelper.isLogWebserviceDelete();
                }
            }
            return false;
        }

        /**
        * Return all channels linked to this entity, either directly or indirectly
        * The returned collection is not mutable
        * @return
        */
        public Set<Channel> getChannels(SystemConfigurationHelper systemConfigurationHelper, Webservice webservice) {
            Set<Channel> allBoundChannels = new HashSet<Channel>();
            if (webservice!=null && canLog(systemConfigurationHelper))
                allBoundChannels.add(webservice.getChannel());
            return allBoundChannels;
        }

    }
    /** The current operation performed on the actee **/
    private WEBSERVICE_LOG_OPERATION action;

    protected WebserviceLog() {
        super();
    }

    @Override
    public WebserviceLog shallowCopy() {
        return new WebserviceLog(this);
    }
    
    public WebserviceLog(WEBSERVICE_LOG_OPERATION operation, String apiKey, String channelId, Set<Channel> channels) {
        super(apiKey, channels);
        setAction(operation);
    }

    public WebserviceLog(WebserviceLog o) {
        super(o);
        setAction(o.getAction());
    }

    @Override
    public WEBSERVICE_LOG_OPERATION getAction() {
        return action;
    }

    public void setAction(WEBSERVICE_LOG_OPERATION action) {
        this.action = action;
    }

    @Override
    public void acceptVisit(LogVisitor visitor) throws Exception {
        visitor.visit(this);
    }
}
