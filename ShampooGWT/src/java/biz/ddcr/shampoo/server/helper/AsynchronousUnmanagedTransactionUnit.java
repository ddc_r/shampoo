/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import javax.servlet.http.HttpSession;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class AsynchronousUnmanagedTransactionUnit<T extends Serializable> implements AsynchronousUnmanagedTransactionUnitInterface<T> {

    public AsynchronousUnmanagedTransactionUnit() throws Exception {
        transactions = new HashSet<Integer>();
    }
    private HttpSession sessionData;
    //Current queued transaction units
    private Collection<Integer> transactions;

    public void addTransactions(Collection<Integer> ids) {
        if (ids != null) {
            transactions.addAll(ids);
        }
    }

    public void clearTransactions() {
        transactions.clear();
    }

    public Collection<Integer> getTransactions() {
        return transactions;
    }

    @Override
    public void asynchronousInitialization() throws Exception {
        sessionData = ServletUtils2.getLocalSession();
        init();
    }

    public abstract void init() throws Exception;

    @Override
    public void asynchronousProcessing() throws Exception {
        ServletUtils2.setLocalSession(sessionData);
        try {
            processing();
        } finally {
            ServletUtils2.setLocalSession(null);
        }
    }

    public abstract void processing() throws Exception;

    @Override
    public void asynchronousCommit() {
        ServletUtils2.setLocalSession(sessionData);
        try {
            commit();
        } finally {
            ServletUtils2.setLocalSession(null);
        }            
    }

    public abstract void commit();

    @Override
    public void asynchronousRollback(Exception originalException) {
        ServletUtils2.setLocalSession(sessionData);
        try {
            rollback(originalException);
        } finally {
            ServletUtils2.setLocalSession(null);
        }
    }

    public abstract void rollback(Exception originalException);
}
