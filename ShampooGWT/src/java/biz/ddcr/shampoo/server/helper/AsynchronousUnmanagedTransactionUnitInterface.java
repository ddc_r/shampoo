/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import java.io.Serializable;


/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
/**
 * Internal interim struct; atomic transaction-like operations unmanaged by Spring
 * Currently used to provide a two-pass synchronized commit transaction support between Spring-managed DB operations and datastore stream transfers
 **/
public interface AsynchronousUnmanagedTransactionUnitInterface<T extends Serializable> {

    /**
     * When this unit of work is delegated to an asynchronous thread, the localThread UserSessionData is not available anymore.
     * These data can be specified and be used when required while normally unavailable.
     * @param currentUserSessionData
     */

    /** This method MUST be called from within a pre-existing transaction context */
    public void asynchronousInitialization() throws Exception;

    /** tag it as synchronous so as to automatically embed it in a R/W transaction scope
     This method MUST be called outside any transaction context */
    public void asynchronousProcessing() throws Exception;

    /** tag it as synchronous so as to automatically embed it in a R/W transaction scope
     This method MUST be called outside any transaction context*/
    public void asynchronousRollback(Exception originalException);

    /** tag it as synchronous so as to automatically embed it in a R/W transaction scope
     This method MUST be called outside any transaction context*/
    public void asynchronousCommit();
};
