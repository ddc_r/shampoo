/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.helper.MessagingOutServiceInterface.MessageWrapperInterface;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * The bare minimum to represent an email, the main variables are present including some kind of text structuring
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BasicMessageWrapper implements MessageWrapperInterface, Serializable, Copiable {

    private YearMonthWeekDayHourMinuteSecondMillisecond timestamp;
    private Set<User> recipients;
    private User sender;
    private String subjectTemplateRelativeLocation;
    private String bodyTemplateRelativeLocation;
    private Map templateVariables;

    protected BasicMessageWrapper() {
    }
    public BasicMessageWrapper(
            String subjectTemplateRelativeLocation,
            String bodyTemplateRelativeLocation,
            Map templateVariables,
            Set<User> recipients) {
        setShortTemplateLocation( subjectTemplateRelativeLocation );
        setLongTemplateLocation( bodyTemplateRelativeLocation );
        setTemplateVariables( templateVariables );
        setRecipients(recipients);
    }
    public BasicMessageWrapper(
            String subjectTemplateRelativeLocation,
            String bodyTemplateRelativeLocation,
            Map templateVariables,
            User recipient) {
        setShortTemplateLocation( subjectTemplateRelativeLocation );
        setLongTemplateLocation( bodyTemplateRelativeLocation );
        setTemplateVariables( templateVariables );
        addRecipient(recipient);
    }
            
    public BasicMessageWrapper(BasicMessageWrapper o) {
        setDate( o.getDate() );
        addRecipients( o.getRecipients() );
        setSender( o.getSender() );
        setShortTemplateLocation( o.getShortTemplateLocation() );
        setLongTemplateLocation( o.getLongTemplateLocation() );
        setTemplateVariables( o.getTemplateVariables() );
    }

    @Override
    public String getLongTemplateLocation() {
        return bodyTemplateRelativeLocation;
    }

    public void setLongTemplateLocation(String bodyTemplateRelativeLocation) {
        this.bodyTemplateRelativeLocation = bodyTemplateRelativeLocation;
    }    
    
    @Override
    public Map getTemplateVariables() {
        return templateVariables;
    }
    
    public void setTemplateVariables(Map templateVariables) {
        this.templateVariables = templateVariables;
    }

    @Override
    public String getShortTemplateLocation() {
        return subjectTemplateRelativeLocation;
    }
    
    public void setShortTemplateLocation(String subjectTemplateRelativeLocation) {
        this.subjectTemplateRelativeLocation = subjectTemplateRelativeLocation;
    }
    
    @Override
    public User getSender() {
        return sender;
    }

    @Override
    public void setSender(User sender) {
        this.sender = sender;
    }

    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecondInterface getDate() {
        if (timestamp==null) timestamp = new YearMonthWeekDayHourMinuteSecondMillisecond(
                System.currentTimeMillis(),
                //Do not enforce any timezone
                null);
        return timestamp;
    }

    @Override
    public void setDate(YearMonthWeekDayHourMinuteSecondMillisecondInterface timestamp) {
        this.timestamp = new YearMonthWeekDayHourMinuteSecondMillisecond(timestamp);
    }

    @Override
    public Collection<User> getRecipients() {
        if (recipients==null) recipients=new HashSet<User>();
        return recipients;
    }

    protected void setRecipients(Set<User> recipients) {
        this.recipients = recipients;
    }

    @Override
    public void addRecipient(User recipient) {
        if (recipient!=null) {
            getRecipients().add(recipient);
        }
    }

    @Override
    public void addRecipients(Collection<User> recipients) {
        if (recipients!=null) {
            getRecipients().addAll(recipients);
        }
    }

    @Override
    public void clearRecipients() {
        getRecipients().clear();
    }

    @Override
    public void removeRecipient(User recipient) {
        if (recipient!=null) {
            getRecipients().remove(recipient);
        }
    }

    @Override
    public void removeRecipients(Collection<User> recipients) {
        if (recipients!=null) {
            getRecipients().removeAll(recipients);
        }
    }

    @Override
    public Copiable shallowCopy() {
        return new BasicMessageWrapper(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BasicMessageWrapper other = (BasicMessageWrapper) obj;
        if (this.timestamp != other.timestamp && (this.timestamp == null || !this.timestamp.equals(other.timestamp))) {
            return false;
        }
        if (this.recipients != other.recipients && (this.recipients == null || !this.recipients.equals(other.recipients))) {
            return false;
        }
        if (this.sender != other.sender && (this.sender == null || !this.sender.equals(other.sender))) {
            return false;
        }
        if ((this.subjectTemplateRelativeLocation == null) ? (other.subjectTemplateRelativeLocation != null) : !this.subjectTemplateRelativeLocation.equals(other.subjectTemplateRelativeLocation)) {
            return false;
        }
        if ((this.bodyTemplateRelativeLocation == null) ? (other.bodyTemplateRelativeLocation != null) : !this.bodyTemplateRelativeLocation.equals(other.bodyTemplateRelativeLocation)) {
            return false;
        }
        if ((this.templateVariables == null) ? (other.templateVariables != null) : !this.templateVariables.equals(other.templateVariables)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.timestamp != null ? this.timestamp.hashCode() : 0);
        hash = 71 * hash + (this.recipients != null ? this.recipients.hashCode() : 0);
        hash = 71 * hash + (this.sender != null ? this.sender.hashCode() : 0);
        hash = 71 * hash + (this.subjectTemplateRelativeLocation != null ? this.subjectTemplateRelativeLocation.hashCode() : 0);
        hash = 71 * hash + (this.bodyTemplateRelativeLocation != null ? this.bodyTemplateRelativeLocation.hashCode() : 0);
        hash = 71 * hash + (this.templateVariables != null ? this.templateVariables.hashCode() : 0);
        return hash;
    }

}
