/*
 * Taken from:
 * Numerical Recipes 3rd Edition: The Art of Scientific Computing
 * William H. Press, Saul A. Teukolsky, William T. Vetterling, Brian P. Flannery
 */
package biz.ddcr.shampoo.server.helper;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Mersenne Twister, faster and more random then the default Java Random algorithm
 * @author William H. Press, Saul A. Teukolsky, William T. Vetterling, Brian P. Flannery
 */
public final class BetterRandom extends Random {

    private Lock l = new ReentrantLock();
    private long u;
    private long v = 4101842887655102017L;
    private long w = 1;

    public BetterRandom() {
        this(System.nanoTime());
    }

    public BetterRandom(long seed) {
        l.lock();
        try {
            u = seed ^ v;
            nextLong();
            v = u;
            nextLong();
            w = v;
            nextLong();
        } finally {
            l.unlock();
        }
    }

    @Override
    public long nextLong() {
        l.lock();
        try {
            u = u * 2862933555777941757L + 7046029254386353087L;
            v ^= v >>> 17;
            v ^= v << 31;
            v ^= v >>> 8;
            w = 4294957665L * (w & 0xffffffff) + (w >>> 32);
            long x = u ^ (u << 21);
            x ^= x >>> 35;
            x ^= x << 4;
            long ret = (x + v) ^ w;
            return ret;
        } finally {
            l.unlock();
        }
    }

    @Override
    protected int next(int bits) {
        return (int) (nextLong() >>> (64 - bits));
    }
}
