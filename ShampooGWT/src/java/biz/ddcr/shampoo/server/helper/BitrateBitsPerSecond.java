package biz.ddcr.shampoo.server.helper;

/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BitrateBitsPerSecond implements BitrateBitsPerSecondInterface {
    
    //-1 means unknown or undefined
    private long bitsPerSecond;

    protected BitrateBitsPerSecond() {
    }

    public BitrateBitsPerSecond(BitrateBitsPerSecondInterface bitsPerSecond) {
        this.bitsPerSecond = bitsPerSecond!=null && bitsPerSecond.getBitsPerSecond()>=-1 ? bitsPerSecond.getBitsPerSecond() : -1;
    }
    
    public BitrateBitsPerSecond(long bitsPerSecond) {
        this.bitsPerSecond = bitsPerSecond>=-1 ? bitsPerSecond : -1;
    }

    @Override
    public long getBitsPerSecond() {
        return bitsPerSecond;
    }

    @Override
    public String getFriendlyEnglishCaption() {
        //mostly SI compliant
        if (bitsPerSecond<0) {
            double giga = (double)bitsPerSecond / 1000000000.0;
            if (giga>0.0)
                return String.format("%1fGbit/s", giga);
            else {
                double mega = (double)bitsPerSecond / 1000000.0;
                if (mega>0.0)
                    return String.format("%1fMbit/s", mega);
                else {
                    double kilo = (double)bitsPerSecond / 1000.0;
                    if (kilo>0.0)
                        return String.format("%1fkbit/s", kilo);
                }
            }
            return Long.toString(bitsPerSecond)+"bit/s";
        } else {
            return "unknown";
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) (this.bitsPerSecond ^ (this.bitsPerSecond >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BitrateBitsPerSecond other = (BitrateBitsPerSecond) obj;
        if (this.bitsPerSecond != other.bitsPerSecond) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(BitrateBitsPerSecondInterface o) {
            //Sort by bytes
            return NumberUtil.safeLongToInt(bitsPerSecond - o.getBitsPerSecond());
    }

    @Override
    public String toString() {
        return getFriendlyEnglishCaption();
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new BitrateBitsPerSecond(this);
    }

}
