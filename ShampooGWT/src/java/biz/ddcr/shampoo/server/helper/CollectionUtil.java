/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.helper;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * Provides Collections removeAll() feature without updating the original sets
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class CollectionUtil {

    private CollectionUtil() {
        //Cheap singleton
    }

    /**
     * Return the elements from newCollection not present in originalCollection
     * if either collection is null then it returns null
     * @param <E>
     * @param originalCollection
     * @param newCollection
     * @return
     */
    public static <E> Collection<E> exclusion(Collection<E> originalCollection, Collection<E> newCollection) {

        if (originalCollection==null || newCollection==null) return null;
        Collection<E> exclusionCollection = new HashSet<E>();
        //if (originalCollection.size() < newCollection.size())
            for (Iterator<E> i = newCollection.iterator(); i.hasNext();) {
                E currentRow = i.next();
                if (!originalCollection.contains(currentRow))
                    exclusionCollection.add(currentRow);
            }
        //FIX now that this method is oriented, drop this part
        /*else
            for (Iterator<E> i = originalCollection.iterator(); i.hasNext();) {
                E currentRow = i.next();
                if (!newCollection.contains(currentRow))
                    exclusionCollection.add(currentRow);
        }*/
        return exclusionCollection;
    }

    /** Convenience method for merging multiple collections into one**/
    public static <E> Collection<E> merge(Collection<E>... collections) {
        Collection<E> mergedCollection = new HashSet<E>();
        if (collections!=null)
            for (Collection<E> collection : collections)
                mergedCollection.addAll(collection);
        return mergedCollection;
    }
    
}
