/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.helper;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class CopiableSet<T extends Copiable> implements CopiableSetInterface<T> {

    private Set<T> set;

    public CopiableSet() {
        set = new HashSet<T>();
    }

    /**
     *
     * Do NOT clone the passed argument, just embed its reference within this object
     * Cloning has to be the result of an explicit action
     * Just because it will be used for each and every Hibernate sets, hence a potential massive amount of data to copy within this object if it was meant to be cloned
     *
     * @param set
     **/
    public CopiableSet(CopiableSetInterface<T> set) {
        this.set = ((CopiableSetInterface)set).getBoxedSet();
    }
    public CopiableSet(Set<T> set) {
        this.set = set;
    }

    @Override
    public boolean add(T e) {
        return set.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends T> clctn) {
        return set.addAll(clctn);
    }

    @Override
    public void clear() {
        set.clear();
    }

    @Override
    public boolean contains(Object o) {
        return set.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> clctn) {
        return set.containsAll(clctn);
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public Iterator<T> iterator() {
        return set.iterator();
    }

    @Override
    public boolean remove(Object o) {
        return set.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> clctn) {
        return set.removeAll(clctn);
    }

    @Override
    public boolean retainAll(Collection<?> clctn) {
        return set.removeAll(clctn);
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return set.toArray(ts);
    }

    @Override
    public CopiableSet<T> shallowCopy() throws Exception {
        CopiableSet<T> newSet = new CopiableSet<T>();
        for (Copiable entry: set) {
            newSet.add((T)(entry.shallowCopy()));
        }
        return newSet;
    }

    @Override
    public Set<T> getBoxedSet() {
        return set;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CopiableSet<T> other = (CopiableSet<T>) obj;
        if (this.set != other.set && (this.set == null || !this.set.equals(other.set))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.set != null ? this.set.hashCode() : 0);
        return hash;
    }

}
