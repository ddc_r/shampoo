package biz.ddcr.shampoo.server.helper;

/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DurationMilliseconds implements DurationMillisecondsInterface {
    
    private long milliseconds;

    protected DurationMilliseconds() {
    }

    public DurationMilliseconds(DurationMillisecondsInterface millisecond) {
        this.milliseconds = millisecond!=null ? millisecond.getMilliseconds() : 0;
    }

    public DurationMilliseconds(long milliseconds) {
        this.milliseconds = milliseconds;
    }

    public DurationMilliseconds(float seconds) {
        this.milliseconds = (long) (seconds * 1000);
    }

    public DurationMilliseconds(long hour, byte minute, byte second, short millisecond) {
        milliseconds = 0;

        milliseconds += (hour * 60 * 60 * 1000L);
        if (minute > -1 && minute < 60) {
            milliseconds += (minute * 60 * 1000L);
        }
        if (second > -1 && second < 60) {
            milliseconds += (second * 1000L);
        }
        if (millisecond > -1 && millisecond < 1000) {
            milliseconds += millisecond;
        }
    }

    @Override
    public float getHours() {
        return milliseconds / (float)1000 / (float)60 / (float)60;
    }

    @Override
    public long getMilliseconds() {
        return milliseconds;
    }

    @Override
    public float getMinutes() {
        return milliseconds / (float)1000 / (float)60;
    }

    @Override
    public float getSeconds() {
        return milliseconds / (float)1000;
    }

    @Override
    public String getFriendlyEnglishCaption() {
        long rest = milliseconds;
        long hour = (long) Math.floor(rest / 1000.0 / 60.0 / 60.0);
        rest -= (hour * 1000L * 60 * 60);
        long minute = (long) Math.floor(rest / 1000.0 / 60.0);
        rest -= (minute * 1000L * 60);
        long second = (long) Math.floor(rest / 1000.0);
        rest -= (second * 1000L);
        
        return String.format("%1$02d:%2$02d:%3$02d.%4$03d", hour, minute, second, rest);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DurationMilliseconds other = (DurationMilliseconds) obj;
        if (this.milliseconds != other.milliseconds) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.milliseconds ^ (this.milliseconds >>> 32));
        return hash;
    }

    @Override
    public int compareTo(DurationMillisecondsInterface o) {
            //Sort by hour, then by minute
            return NumberUtil.safeLongToInt(milliseconds - o.getMilliseconds());
    }

    @Override
    public String toString() {
        return getFriendlyEnglishCaption();
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new DurationMilliseconds(this);
    }

}
