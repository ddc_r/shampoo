/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.helper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * Spring-delegated security servlet filter on top of the Public Webservice servlet and checks whether the caller is within a given IP range
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class GenericIPRegexpFilter extends GenericRewriterFilter {

    private Pattern compiledAccessRegexp;

    public String getAccessRegexp() {
        return compiledAccessRegexp!=null?compiledAccessRegexp.pattern():null;
    }

    public void setAccessRegexp(String accessRegexp) {
        compiledAccessRegexp = null;
        try {
            if (accessRegexp!=null && accessRegexp.length()!=0)
                compiledAccessRegexp = Pattern.compile(accessRegexp);
        } catch (PatternSyntaxException e) {
            //Silently fails
            logger.error(e);
        }
    }

    /**
     * Add cross-origin headers if needed
     * @param request
     * @param response 
     */
    protected void setCORS(HttpServletRequest request, HttpServletResponse response) {
        final String origin = request.getHeader("Origin");
        if (origin != null) {
            response.addHeader("Access-Control-Allow-Origin", origin);
            response.setHeader("Allow", "GET, HEAD, POST, OPTIONS, PUT, DELETE, TRACE");
            final String method = request.getHeader("Access-Control-Request-Method");
            final String headers = request.getHeader("Access-Control-Request-Headers");            
            if (method!=null) response.addHeader("Access-Control-Allow-Methods", method);
            if (headers!=null) response.addHeader("Access-Control-Allow-Headers", headers);
        }
    }
    
    abstract protected boolean isProxiedIPAllowed();
    
    @Override
    public void onPreRewrite(ServletRequest request, ServletResponse response) throws IOException, ServletException {
        if (compiledAccessRegexp!=null) {
            final Matcher matcher = compiledAccessRegexp.matcher(ServletUtils2.getRemoteAddr((HttpServletRequest)request, isProxiedIPAllowed()));
            if (!matcher.matches())
                /* Update: do not use UnavailableException, Glassfish automatically shuts down the servlet if you throw it
                 * resulting in no further call possible*/
                throw new SecurityException("Access denied");
        }
        
        //We went past there, we can add cross-origin headers to the response and be friendly with our API users
        setCORS((HttpServletRequest)request, (HttpServletResponse)response);
    }

}
