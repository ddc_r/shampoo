/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.service.GenericService;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;

/**
 *
 * Spring-delegated security servlet filter on top of the GWT servlet
 * Will rewrite cookie context paths to match any user-defined reverse-proxy translated paths
 * Uses proxies instead of plain class compositions in order to play well with Atmosphere on-the-fly changes of Servlet specification revision interfaces
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class GenericRewriterFilter extends GenericService implements Filter {
    
    /**
     * Rewrite default cookie paths with the correct one for reverse-proxy servers
     */
    class AddCookieAdvice implements MethodInterceptor {

        @Override
        public Object invoke(MethodInvocation invocation) throws Throwable {
            
            Cookie parameter = (Cookie) (invocation.getArguments()!=null && invocation.getArguments().length>0 ? invocation.getArguments()[0] : null);
            
            if (parameter != null && getNewContextPath() != null) {
                //Do only rewrite paths you're allowed to modify
                if (parameter.getPath() == null || !parameter.getPath().equals(getNewContextPath())) {
                    parameter.setPath(getNewContextPath());
                    return invocation.getMethod().invoke(invocation.getThis(), new Object[]{parameter});
                }                
            }
            
            return invocation.proceed();
        }
    }
    
    protected HttpServletResponse proxyServletResponse(ServletResponse response) {
        NameMatchMethodPointcut pc = new NameMatchMethodPointcut();
        //addCookie is implemented by *ALL* servlet specification revisions, so it's safe
        pc.addMethodName("addCookie");
        Advisor advisor = new DefaultPointcutAdvisor(pc, new AddCookieAdvice());

        ProxyFactory pf = new ProxyFactory();
        //Do not use CGLIB or JavaAssist and so avoid messing with the constructors: servlets don't have public, no-argument c'tors, CGLIB won't work
        pf.setProxyTargetClass(false);
        pf.addInterface(HttpServletResponse.class);
        pf.setTarget((HttpServletResponse)response);
        pf.addAdvisor(advisor);
        
        return (HttpServletResponse)pf.getProxy();
    }
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //Do nothing
    }

    /**
     * Filter on a servlet which cookies context paths have been rewritten to
     * match any reverse-proxy settings Changed the old class composition way to
     * a method proxy: makes it resilient to chnages in servlet specification
     * revisions
     */
    @Override    
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        onPreRewrite(request, response);

        HttpServletRequest proxiedHttpRequest = (HttpServletRequest)request;
        HttpServletResponse httpResponse = proxyServletResponse(response);

        onRewrite(proxiedHttpRequest, httpResponse);

        onPostRewrite(proxiedHttpRequest, httpResponse);

        chain.doFilter(proxiedHttpRequest, httpResponse);
        
    }

    @Override
    public void destroy() {
        //Do nothing
    }

    /**
     * Return null if you don't want to change it*
     */
    public String getNewContextPath() {
        return null;
    }

    public abstract void onPreRewrite(ServletRequest request, ServletResponse response) throws IOException, ServletException;

    public abstract void onRewrite(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;

    public abstract void onPostRewrite(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
