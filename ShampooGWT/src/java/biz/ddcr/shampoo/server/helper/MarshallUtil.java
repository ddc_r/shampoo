/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author okay_awright
 *
 */
public class MarshallUtil {

    public static String trimText(String text) {
        return text.trim();
    }

    public static String lowerCaseText(String text) {
        return text.toLowerCase();
    }

    public static String escapeText(String string) {
        return StringEscapeUtils.escapeJavaScript(string);
    }

    public static StringBuilder toSimpleLiquidsoapJSON(Map<String, String> elements) {
        return toSimpleJSON(elements);
    }

    public static StringBuilder toSimpleJSON(Map<String, ? extends Object> elements) {
        StringBuilder result = new StringBuilder();
        result.append("{");
        if (elements != null && !elements.isEmpty()) {
            for (Entry<String, ? extends Object> element : elements.entrySet()) {
                if (element.getValue() != null) {
                    result//
                            .append(makeJSONString(element.getKey()))//
                            .append(":");
                    if (element.getValue() instanceof Object[]) {
                        result.append("[");
                        //Doesn't allow arrays within arrays
                        for (Object subElement : (Object[]) element.getValue()) {
                            if (subElement instanceof String) {
                                result.append(makeJSONString((String) subElement));
                            } else {
                                result.append(subElement.toString());
                            }
                            result.append(",");
                        }
                        if (result.charAt(result.length() - 1) == ',') {
                            result.deleteCharAt(result.length() - 1);
                        }
                        result.append("]");
                    } else if (element.getValue() instanceof String) {
                        result.append(makeJSONString((String) element.getValue()));
                    } else {
                        result.append(element.getValue().toString());
                    }
                    result.append(",");
                }
            }
            if (result.charAt(result.length() - 1) == ',') {
                result.deleteCharAt(result.length() - 1);
            }
        }
        result.append("}");
        return result;
    }

    public static StringBuilder toSimpleAnnotate(Map<String, ? extends Object> elements, String url) {
        StringBuilder result = new StringBuilder();
        if (elements != null && !elements.isEmpty()) {
            result.append(":");
            for (Entry<String, ? extends Object> element : elements.entrySet()) {
                if (element.getValue() != null) {
                    result//
                            .append(makeAnnotateAttributeName(element.getKey()))//
                            .append("=");
                    //Doesn't allow arrays within arrays
                    if (element.getValue() instanceof Object[]) {
                        for (Object subElement : (Object[]) element.getValue()) {
                            result//
                                    .append(makeJSONString((String) subElement))//
                                    .append(",");
                        }
                        if (result.charAt(result.length() - 1) == ',') {
                            result.deleteCharAt(result.length() - 1);
                        }
                    } else {
                        result//
                                .append(makeJSONString(element.getValue().toString()));
                    }
                    result//
                            .append(",");
                }
            }
            if (result.charAt(result.length() - 1) == ',') {
                result.deleteCharAt(result.length() - 1);
            }
        }
        if (url!=null && url.length()!=0) {
            result//
                    .append(":")//
                    .append(url);
        }
        if (result.length()>0) {
            result.insert(0, "annotate");
        }
        return result;
    }

    public static StringBuilder toSimpleXML(String outerElement, Map<String, ? extends Object> elements) {
        StringBuilder result = new StringBuilder();
        result.append("<").append(makeXMLElementName(outerElement));
        if (elements != null && !elements.isEmpty()) {
            result.append(">");
            for (Entry<String, ? extends Object> element : elements.entrySet()) {
                if (element.getValue() != null) {
                    if (element.getValue() instanceof Object[]) {
                        String _elementName = makeXMLElementName(element.getKey());
                        String elementNames;
                        String elementName;
                        if (_elementName.length()>1 && _elementName.charAt(_elementName.length()-1)=='s') {
                            elementNames=_elementName;
                            elementName=_elementName.substring(0, _elementName.length()-1);
                        } else {
                            elementNames=_elementName+"s";
                            elementName=_elementName;
                        }
                        result//
                                .append("<")//
                                .append(elementNames)//
                                .append(">");
                        //Doesn't allow arrays within arrays
                        for (Object subElement : (Object[]) element.getValue()) {
                            result//
                                .append("<")//
                                .append(elementName)//
                                .append(">")//
                                .append(escapeXML(subElement.toString()))//
                                .append("</")//
                                .append(elementName)//
                                .append(">");
                        }
                        result//
                                .append("</")//
                                .append(elementNames)//
                                .append(">");
                    } else {
                        result//
                                .append("<")//
                                .append(makeXMLElementName(element.getKey()))//
                                .append(">")//
                                .append(escapeXML(element.getValue().toString()))//
                                .append("</")//
                                .append(makeXMLElementName(element.getKey()))//
                                .append(">");
                    }

                }
            }
            result.append("</").append(makeXMLElementName(outerElement)).append(">");
        } else {
            result.append("/>");
        }
        return result;
    }

    public static StringBuilder toSimpleCSV(Collection<String> strings) {
        if (strings != null && !strings.isEmpty()) {
            return toSimpleCSV(strings.toArray(new String[strings.size()]));
        } else {
            return new StringBuilder();
        }
    }

    public static StringBuilder toSimpleCSV(String[] strings) {
        StringBuilder result = new StringBuilder();
        if (strings != null && strings.length > 0) {
            for (int i = 0; i < strings.length - 1; i++) {
                result.append(escapeCSV(strings[i])).append(',');
            }
            result.append(escapeCSV(strings[strings.length - 1]));
        }
        return result;
    }

    public static String escapeXML(String string) {
        return string != null ? StringEscapeUtils.escapeXml(string) : "";
    }

    public static String escapeCSV(String string) {
        return string != null ? StringEscapeUtils.escapeCsv(string) : "";
    }
    static char[] hex = "0123456789ABCDEF".toCharArray();

    /**
     * Converts character to UTF8
     */
    private static StringBuilder unicode(char c) {
        StringBuilder sb = new StringBuilder();
        sb.append("\\u");

        int n = c;

        for (int i = 0; i < 4; ++i) {
            int digit = (n & 0xf000) >> 12;

            sb.append(hex[digit]);
            n <<= 4;
        }
        return sb;
    }

    /**
     * Simple standard-compliant character escaping for JSON strings.
     *
     */
    public static StringBuilder makeJSONString(Object obj) {
        StringBuilder sb = new StringBuilder();

        //Failsafe for null, convert it to empty string
        if (obj == null) {
            obj = "";
        }
        CharacterIterator it = new StringCharacterIterator(obj.toString());

        for (char c = it.first(); c != CharacterIterator.DONE; c = it.next()) {
            if (c == '"') {
                sb.append("\\\"");
            } else if (c == '\\') {
                sb.append("\\\\");
            } else if (c == '/') {
                sb.append("\\/");
            } else if (c == '\b') {
                sb.append("\\b");
            } else if (c == '\f') {
                sb.append("\\f");
            } else if (c == '\n') {
                sb.append("\\n");
            } else if (c == '\r') {
                sb.append("\\r");
            } else if (c == '\t') {
                sb.append("\\t");
            } else if (Character.isISOControl(c)) {
                sb.append(unicode(c));
            } else {
                sb.append(c);
            }
        }
        return sb.insert(0, "\"").append("\"");
    }

    public static String makeXMLElementName(String name) {
        /**
         * [4] NameStartChar	::= ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] |
         * [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] | [#x37F-#x1FFF] |
         * [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF]
         * | [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF] [4a] NameChar
         * ::= NameStartChar | "-" | "." | [0-9] | #xB7 | [#x0300-#x036F] |
         * [#x203F-#x2040] [5] Name	::= NameStartChar (NameChar)*
         */
        //TODO follow the rules (from the RFC) above
        return name;
    }

    public static String makeAnnotateAttributeName(String name) {
        //TODO
        return name;
    }

    public static String removeTrailingSlash(String url) {
        return url != null ? (url.endsWith("/") ? url.substring(0, url.length() - 1) : url) : "";
    }

    public static String addStartingSlash(String url) {
        return url!=null ? (url.startsWith("/") ? url : '/' + url) : "/";
    }
    
    /** Pad number with given amount of zeroes before*/
    public static String pad(int number, int paddingLength) {
        return String.format("%0"+(paddingLength<0?0:paddingLength)+"d", number);
    }
    
}
