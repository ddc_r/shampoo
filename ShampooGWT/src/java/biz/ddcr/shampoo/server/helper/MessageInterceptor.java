/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.MessageBroadcaster;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 *
 * Will intercept Hibernate-generated feedback messages and make sure they are flushed for the GUI if no Exception occurred
 * Be sure not to bind it inside a Spring-controlled transaction but just after.
 * Should be called before TransactionInterceptor
 *
 * @author okay_awright
 **/
public class MessageInterceptor extends GenericService implements MethodInterceptor {

    /**
     * Make sure a new pile of message is ready before calling the main method and flush it afterwards
     * @param methodInvocation
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
            //Message feedbacks follow the same semantics as transactions
            //We can rely on the fact that hibernate uses threadlocal and doesn't create additional threads as well so that messages are guarnteed to be thread-safe and flushing the queue operates on the same unit of work
            //Create a new queue of messages that can be filled in by Hibernate automatic method interceptors
            MessageBroadcaster.start();

        try {
            
            if (logger.isDebugEnabled())
                logger.debug(Thread.currentThread().getId()+":MessageInterceptor:INVOKE(START):"+methodInvocation.getStaticPart().toString());            
            //Proceed to original method call
            Object result = methodInvocation.proceed();
            if (logger.isDebugEnabled()) {
                logger.debug(Thread.currentThread().getId()+":MessageInterceptor:INVOKE(STOP):"+methodInvocation.getStaticPart().toString());                        
                logger.debug(Thread.currentThread().getId()+":MessageInterceptor:INVOKE(FLUSHING_ALL_COMET):"+MessageBroadcaster.get());
            }
            //everything is fine, flush the queue now
            MessageBroadcaster.flush();
            
            return result;
                        
        } catch (Exception e) {
            if (logger.isDebugEnabled())
                logger.debug(Thread.currentThread().getId()+":MessageInterceptor:INVOKE(FLUSHING_ERROR_COMET)");
            //TODO: feedbacks based on Exceptions are lost right now, refactoring the AOP and proxies is a solution but it complexifies the architecture a lot
            //UPDATE: partial fix
            MessageBroadcaster.flushErrorsOnly();
            //Rethrow, not handled here
            throw e;
        } finally {
            MessageBroadcaster.clear();
        }
    }

}
