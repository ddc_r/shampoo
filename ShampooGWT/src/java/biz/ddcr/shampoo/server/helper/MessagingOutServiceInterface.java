/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.helper.MessagingOutServiceInterface.MessageWrapperInterface;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 *
 * Provides basic messaging features for either email, IM, twitter, whatever.
 * The generics T specify the real implementation of the underlying message server bridge, e.g. it can be JavaMailSender for plain emails
 * U is the message templating system, at the moment it is Apache Velocity
 * V is the message object to send, i.e. the instantiated class that contains the message and that can be handled by T, e.g. SimpleMailMessage for emails
 *
 * @author okay_awright
 **/
public interface MessagingOutServiceInterface<T, U, V extends MessageWrapperInterface> extends Serializable {

    public interface MessageWrapperInterface {

        public void setSender(User sender);
        public User getSender();

        /**
         * Don't use YearMonthDayHourMinuteInterface because it is not precise enough but if one day an extended class can go down as low as the millisecond then use it here
         * Use the Unix time (from Epoch) as a parameter
         * @param sentDate
         */
        public void setDate(YearMonthWeekDayHourMinuteSecondMillisecondInterface timestamp);
        /**
         * Don't use YearMonthDayHourMinuteInterface because it is not precise enough but if one day an extended class can go down as low as the millisecond then use it here
         * Returns the Unix time
         * @param sentDate
         */
        public YearMonthWeekDayHourMinuteSecondMillisecondInterface getDate();

        public String getShortTemplateLocation();
        public String getLongTemplateLocation();        
        public Map getTemplateVariables();

        public void addRecipient(User recipient);
        public void addRecipients(Collection<User> recipients);
        public void removeRecipient(User recipient);
        public void removeRecipients(Collection<User> recipients);
        public void clearRecipients();
        public Collection<User> getRecipients();

    }

    /** Whether the Message Service is on and properly configured**/
    public boolean isEnabled();

    public void setSmtpProvider(T provider);

    public void setTemplateProvider(U provider);

    public void setDefaultSender(String sender);
    public String getDefaultSender();

    public void send(V message) throws Exception;
    public void send(Collection<V> messages) throws Exception;
}
