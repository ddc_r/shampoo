/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.helper;

/**
 *
 * @author okay_awright
 **/
public abstract class MicroTask {

    private long numberOfLoops = 1;

    public MicroTask() {
    }

    public MicroTask(long numberOfIterations) {
        setNumberOfLoops(numberOfIterations);
    }

    /**
     * How many times loop() will be called. 0 means it will never be called and -1 means infinite
     * @param numberOfIterations
     **/
    public void setNumberOfLoops(long numberOfIterations) {
        if (numberOfIterations < -1) {
            numberOfIterations = -1;
        }
        numberOfLoops = numberOfIterations;
    }

    public long getNumberOfLoops() {
        return numberOfLoops;
    }

    /**
     * One iteration of a global process. Iteratively called until numberOfLoops times has been reachd.
     * return false or throw an Exception if you wish to prematurely break the loop sequence and don't go to the next loop
     * The shorter and the more iterations are planned, the more likely it can be interrupted on time when aborted by another thread
     * @return
     **/
    public abstract boolean loop() throws Exception;
}
