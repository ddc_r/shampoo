/*
 *  Copyright (C) 2013 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public final class NumberUtil {

    public interface Fold<T> {

        public void each(T value);
    }

    private final static Log LOGGER = LogFactory.getLog(NumberUtil.class);

    private NumberUtil() {
    }

    private static long minByteFromLong(final long value) {
        if (Byte.MAX_VALUE >= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 64bit integer casting of value " + value + " to 8bit");
            return Byte.MAX_VALUE;
        }
    }
    private static long minIntFromLong(final long value) {
        if (Integer.MAX_VALUE >= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 64bit integer casting of value " + value + " to 32bit");
            return Integer.MAX_VALUE;
        }
    }
    private static int minShortFromInt(final int value) {
        if (Short.MAX_VALUE >= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 32bit integer casting of value " + value + " to 16bit");
            return Short.MAX_VALUE;
        }
    }
    private static int minByteFromInt(final int value) {
        if (Byte.MAX_VALUE >= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 32bit integer casting of value " + value + " to 8bit");
            return Byte.MAX_VALUE;
        }
    }

    private static long maxByteFromLong(final long value) {
        if (Byte.MIN_VALUE <= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 64bit integer casting of value " + value + " to 8bit");
            return Byte.MIN_VALUE;
        }
    }
    private static long maxIntFromLong(final long value) {
        if (Integer.MIN_VALUE <= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 64bit integer casting of value " + value + " to 32bit");
            return Integer.MIN_VALUE;
        }
    }
    private static int maxShortFromInt(final int value) {
        if (Short.MIN_VALUE <= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 32bit integer casting of value " + value + " to 16bit");
            return Short.MIN_VALUE;
        }
    }
    private static int maxByteFromInt(final int value) {
        if (Byte.MIN_VALUE <= value) {
            return value;
        } else {
            LOGGER.warn("Dangerous bit-dropping operation occurred during 32bit integer casting of value " + value + " to 8bit");
            return Byte.MIN_VALUE;
        }
    }

    public static int safeLongToInt(final long l) {
        return (int) maxIntFromLong(minIntFromLong(l));
    }
    public static byte safeLongToByte(final long l) {
        return (byte) maxByteFromLong(minByteFromLong(l));
    }
    public static short safeIntToShort(final int l) {
        return (short) maxShortFromInt(minShortFromInt(l));
    }
    public static byte safeIntToByte(final int l) {
        return (byte) maxByteFromInt(minByteFromInt(l));
    }

    /** BUGFIX: can create thread contention if the number is huge and the operation inside .each() is slow, especially with later JDK 7 revisions: safer not to use it for now */
    @Deprecated
    public static void foldLongToInt(final long l, final Fold<Integer> fold) {
        long total = l;
        if (total > Integer.MAX_VALUE) {
            while (total > Integer.MAX_VALUE) {
                fold.each(Integer.MAX_VALUE);
                total -= Integer.MAX_VALUE;
            }
        } else if (total < Integer.MIN_VALUE) {
            while (total < Integer.MIN_VALUE) {
                fold.each(Integer.MIN_VALUE);
                total += Integer.MIN_VALUE;
            }
        }
        fold.each((int) total);
    }
}
