/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.io.Cacheable;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.GenericService;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.Statistics;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.core.task.TaskExecutor;

/**
 * EHCache controlled cache for business objects
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class ObjectCachingHelper<T extends Cacheable> extends GenericService {

    /**
     * Finds a corresponding object in the cache or, if not present, retrieves a
     * fresh instance using a user method and then automatically caches it for
     * later reuse
     *
     * @param <T>
     */
    public abstract class CacheInspector<T extends Cacheable> {

        public final String id;

        public abstract T getMissed() throws Exception;

        public CacheInspector(final String id) {
            this.id = id;
        }

        public T find() throws Exception {
            if (enabled) {
                //Early exit
                if (id == null) {
                    return null;
                }
                T object = ObjectCachingHelper.this.getObjectInCache(id);
                if (object == null) {
                    object = getMissed();
                    //Put it in the cache
                    if (object != null) {
                        ObjectCachingHelper.this.putObjectInCache(id, object);
                    }
                }
                return object;
            } else {
                 //shortcut
                return getMissed();
            }
        }

    }

    private boolean enabled;
    private transient CacheManager cacheManager;
    private transient TaskPerformer taskPerformer;

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskPerformer = new TaskPerformer(taskExecutor);
    }

    /**
     * @return the cacheManager
     */
    public CacheManager getCacheManager() {
        return cacheManager;
    }

    /**
     * @param cacheManager the cacheManager to set
     */
    public void setCacheManager(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    protected void printLog(Cache cache) {
        if (logger.isInfoEnabled()) {
            Statistics statistics = cache.getStatistics();
            logger.info("uri : " + (ServletUtils2.getRequest() != null ? ServletUtils2.getRequest().getRequestURI() : "untracked") + ", average get time : " + statistics.getAverageGetTime() + " ms");
            logger.info("Cache hits           : " + statistics.getCacheHits());
            logger.info("Cache misses         : " + statistics.getCacheMisses());
            logger.info("Cache object count   : " + statistics.getObjectCount());
            logger.info("Cache eviction count : " + statistics.getEvictionCount());
            logger.info("Cache eviction count : " + statistics.getEvictionCount());
        }
    }

    public abstract String getCacheName();
    
    public Cache getCurrentObjectCache() {
        Cache cache = getCacheManager().getCache(getCacheName());
        if (cache == null) {
            cache = new Cache(new CacheConfiguration(getCacheName(), 1000));
            getCacheManager().addCache(cache);
        }
        return cache;
    }

    /**
     * Puts an object in the EHCache cache in an asynchronous way
     *
     * @param id
     * @param object
     * @throws Exception
     */
    public <T extends Cacheable> void putObjectInCache(final String id, final T object) throws Exception {
        if (enabled) {
            if (id != null && id.length() != 0) {
                if (object != null && object.isCacheable()) {
                    logger.info("putObjectInCache(" + id + "," + object.toString() + ")");
                    //Each processing is performed in its own thread
                    MicroTask loop = new MicroTask() {
                        @Override
                        public boolean loop() throws Exception {
                            //TODO: delay the actual cloning in memory only when we are sure it will actually be stored in the cache, apparently EHCache doesn't provide the required hooks to do that; look further in the documentation...
                            //TODO: cache storing should ideally be done streaming, but once again it's apparently not possible...
                            /*final Cacheable cacheableClone = object.getCacheableInstance();
                             if (cacheableClone != null) {
                             final Element newCacheElement = new Element(id, cacheableClone);
                             getCurrentObjectCache().put(newCacheElement);
                             }*/
                            getCurrentObjectCache().put(new Element(id, object));
                            //Return false to notify that no more loop is required
                            return false;
                        }
                    };
                    taskPerformer.runTask(loop);
                }
            } else {
                throw new NullPointerException("No cache key given, check the identifier");
            }
        }
    }

    /**
     * Gets an object from the EHCache cache
     *
     * @param id
     * @return
     * @throws Exception
     */
    public <T extends Cacheable> T getObjectInCache(String id) throws Exception {
        if (enabled) {
            if (id != null && id.length() != 0) {
                logger.info("getObjectInCache(" + id + ")");
                final Element previousCacheElement = getCurrentObjectCache().get(id);
                printLog(getCurrentObjectCache());
                return (T) (previousCacheElement != null ? previousCacheElement.getObjectValue() : null);
            } else {
                throw new NullPointerException("No cache key given, check the identifier");
            }
        }
        return null;
    }

    /**
     * Invalidate an object from the EHCache object in an asynchronous way
     *
     * @param id
     * @throws Exception
     */
    public void removeObjectInCache(final String id) throws Exception {
        if (enabled) {
            if (id != null && id.length() != 0) {
                logger.info("removeObjectInCache(" + id + ")");
                //Each processing is performed in its own thread
                MicroTask loop = new MicroTask() {
                    @Override
                    public boolean loop() throws Exception {
                        getCurrentObjectCache().remove(id);
                        //Return false to notify that no more loop is required
                        return false;
                    }
                };
                taskPerformer.runTask(loop);
            } else {
                throw new NullPointerException("No cache key given, check the identifier");
            }
        }
    }
}
