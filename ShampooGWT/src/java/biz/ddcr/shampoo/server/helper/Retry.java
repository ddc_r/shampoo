/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.helper;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class Retry {

    protected int retries;
    //TODO: make it user-configurable
    protected final static int SLEEP_TIME = 1000;

    public Retry(int retries) throws Exception {
        this.retries = retries;
        run();
    }

    protected void run() throws Exception {
        boolean result = false;
        do {
            result = retry();
            if (result) break;
            retries--;
            Thread.sleep(SLEEP_TIME);
        } while (retries>0);
        if (!result) throw new Exception("Retry failed, operation uncompleted");
    }

    public abstract boolean retry() throws Exception;
}
