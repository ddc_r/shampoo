/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.helper;

/**
 *
 * @author okay_awright
 **/
public class RightsBundle {
    private boolean channelAdministratorRights;
    private boolean programmeManagerRights;
    private boolean listenerRights;
    private boolean animatorRights;
    private boolean curatorRights;
    private boolean contributorRights;

    public RightsBundle(boolean defaultRightsWildcard) {
        this.channelAdministratorRights =
        this.programmeManagerRights =
        this.listenerRights =
        this.animatorRights =
        this.curatorRights =
        this.contributorRights = defaultRightsWildcard;
    }
    public RightsBundle() {
        this(false);
    }
    public RightsBundle(boolean channelAdministratorRights, boolean programmeManagerRights, boolean listenerRights, boolean animatorRights, boolean curatorRights, boolean contributorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
        this.programmeManagerRights = programmeManagerRights;
        this.listenerRights = listenerRights;
        this.animatorRights = animatorRights;
        this.curatorRights = curatorRights;
        this.contributorRights = contributorRights;
    }

    public boolean isAnimatorRights() {
        return animatorRights;
    }

    public void setAnimatorRights(boolean animatorRights) {
        this.animatorRights = animatorRights;
    }

    public boolean isContributorRights() {
        return contributorRights;
    }

    public void setContributorRights(boolean contributorRights) {
        this.contributorRights = contributorRights;
    }

    public boolean isCuratorRights() {
        return curatorRights;
    }

    public void setCuratorRights(boolean curatorRights) {
        this.curatorRights = curatorRights;
    }

    public boolean isListenerRights() {
        return listenerRights;
    }

    public void setListenerRights(boolean listenerRights) {
        this.listenerRights = listenerRights;
    }

    public boolean isProgrammeManagerRights() {
        return programmeManagerRights;
    }

    public void setProgrammeManagerRights(boolean programmeManagerRights) {
        this.programmeManagerRights = programmeManagerRights;
    }

    public boolean isChannelAdministratorRights() {
        return channelAdministratorRights;
    }

    public void setChannelAdministratorRights(boolean channelAdministratorRights) {
        this.channelAdministratorRights = channelAdministratorRights;
    }

    public boolean isChannelRightsSelected() {
        return (channelAdministratorRights
                ||
                programmeManagerRights
                ||
                listenerRights);
    }

    public boolean isProgrammeRightsSelected() {
        return (animatorRights
                ||
                curatorRights
                ||
                contributorRights);
    }

    public boolean isRightsSelected() {
        return (
                isChannelRightsSelected()
                ||
                isProgrammeRightsSelected());
    }

    public RightsBundle and(RightsBundle o) {
        if (o!=null) {
            setListenerRights( isListenerRights() && o.isListenerRights() );
            setProgrammeManagerRights( isProgrammeManagerRights() && o.isProgrammeManagerRights() );
            setListenerRights( isListenerRights() && o.isListenerRights() );
            setAnimatorRights( isAnimatorRights() && o.isAnimatorRights() );
            setCuratorRights( isCuratorRights() && o.isCuratorRights() );
            setContributorRights( isContributorRights() && o.isContributorRights() );
        }
        return this;
    }

    public RightsBundle or(RightsBundle o) {
        if (o!=null) {
            setListenerRights( isListenerRights() || o.isListenerRights() );
            setProgrammeManagerRights( isProgrammeManagerRights() || o.isProgrammeManagerRights() );
            setListenerRights( isListenerRights() || o.isListenerRights() );
            setAnimatorRights( isAnimatorRights() || o.isAnimatorRights() );
            setCuratorRights( isCuratorRights() || o.isCuratorRights() );
            setContributorRights( isContributorRights() || o.isContributorRights() );
        }
        return this;
    }
}
