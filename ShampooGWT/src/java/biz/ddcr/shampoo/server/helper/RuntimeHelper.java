/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * Dirty kludge for calling OS-specific functions, like chmod, and overcome Java 1.5 inconsistencies
 * FIX: no more JNA, nor JNI, plain Shell piping: works with NetBSD without library recompilation
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class RuntimeHelper {

    public String call(String... params) {
        return call(null, false, params);
    }

    /* Run a binary using binDir as the wd. Return stdout
     * and optinally stderr
     */
    public String call(Map<String, String> env, boolean stderr, String... params) {
        try {

            List<String> chPermList = Arrays.asList(params);
            /* Process process = Runtime.getRuntime().exec(binName, envp, binDir);*/
            ProcessBuilder pb = new ProcessBuilder(chPermList).redirectErrorStream(true);
            if (env != null && !env.isEmpty()) {
                pb.environment().putAll(env);
            }

            Process process = pb.start();
            // Waits for the command to finish.
            process.waitFor();

            BufferedReader outreader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errreader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            StringBuilder output = new StringBuilder();
            try {
                int read;
                char[] buffer = new char[32768];
                while ((read = outreader.read(buffer)) > 0) {
                    output.append(buffer, 0, read);
                }
                if (stderr) {
                    while ((read = errreader.read(buffer)) > 0) {
                        output.append(buffer, 0, read);
                    }
                }
            } finally {
                outreader.close();
                errreader.close();
            }

            return output.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
