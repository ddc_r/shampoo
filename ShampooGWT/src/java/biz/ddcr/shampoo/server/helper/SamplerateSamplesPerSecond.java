package biz.ddcr.shampoo.server.helper;

/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SamplerateSamplesPerSecond implements SamplerateSamplesPerSecondInterface {
    
    //-1 means unknown or undefined
    private long samplesPerSecond;

    protected SamplerateSamplesPerSecond() {
    }

    public SamplerateSamplesPerSecond(SamplerateSamplesPerSecondInterface samplesPerSecond) {
        this.samplesPerSecond = samplesPerSecond!=null && samplesPerSecond.getSamplesPerSecond()>=-1 ? samplesPerSecond.getSamplesPerSecond() : -1;
    }
    
    public SamplerateSamplesPerSecond(long samplesPerSecond) {
        this.samplesPerSecond = samplesPerSecond>=-1 ? samplesPerSecond : -1;
    }

    @Override
    public long getSamplesPerSecond() {
        return samplesPerSecond;
    }

    @Override
    public String getFriendlyEnglishCaption() {
        //mostly SI compliant
        if (samplesPerSecond<0) {
            double giga = (double)samplesPerSecond / 1000000000.0;
            if (giga>0.0)
                return String.format("%1fGHz", giga);
            else {
                double mega = (double)samplesPerSecond / 1000000.0;
                if (mega>0.0)
                    return String.format("%1fMHz", mega);
                else {
                    double kilo = (double)samplesPerSecond / 1000.0;
                    if (kilo>0.0)
                        return String.format("%1fkHz", kilo);
                }
            }
            return Long.toString(samplesPerSecond)+"bit/s";
        } else {
            return "unknown";
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) (this.samplesPerSecond ^ (this.samplesPerSecond >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SamplerateSamplesPerSecond other = (SamplerateSamplesPerSecond) obj;
        if (this.samplesPerSecond != other.samplesPerSecond) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SamplerateSamplesPerSecondInterface o) {
            //Sort by bytes
            return NumberUtil.safeLongToInt(samplesPerSecond - o.getSamplesPerSecond());
    }

    @Override
    public String toString() {
        return getFriendlyEnglishCaption();
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new SamplerateSamplesPerSecond(this);
    }

}
