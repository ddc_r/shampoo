/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * Access the underlying request and response servlet anytime
 *
 * @author okay_awright
 *
 */
public class ServletUtils2 {

    private final static String HTTP_X_FORWARDED_FOR = "X-Forwarded-For";
    private final static ThreadLocal<HttpServletRequest> servletRequest = new ThreadLocal<HttpServletRequest>();
    private final static ThreadLocal<HttpServletResponse> servletResponse = new ThreadLocal<HttpServletResponse>();
    private final static ThreadLocal<HttpSession> localSession = new ThreadLocal<HttpSession>();    

    /**
     * Adjusts HTTP headers so that browsers won't cache response.
     *
     * @param response For more background see <a
     * href="http://www.onjava.com/pub/a/onjava/excerpt/jebp_3/index2.html">this</a>.
     */
    @Deprecated
    public static void disableResponseCaching(HttpServletResponse response) {
        response.setHeader("Expires", "Sat, 1 January 2000 12:00:00 GMT");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    }

    /**
     * Return the request which invokes the service. Valid only if used in the
     * dispatching thread.
     *
     * @return the servlet request
     */
    public static HttpServletRequest getRequest() {
        return servletRequest.get();
    }

    /**
     * Return the response which accompanies the request. Valid only if used in
     * the dispatching thread.
     *
     * @return the servlet response
     */
    public static HttpServletResponse getResponse() {
        return servletResponse.get();
    }

    /**
     * Assign the current servlet request to a thread local variable. Valid only
     * if used inside the invoking thread scope.
     *
     * @param request
     */
    public static void setRequest(HttpServletRequest request) {
        servletRequest.set(request);
        if (request != null) {
            localSession.set(request.getSession());
        }
    }

    /**
     * Assign the current servlet response to a thread local variable. Valid
     * only if used inside the invoking thread scope.
     *
     * @param response
     */
    public static void setResponse(HttpServletResponse response) {
        servletResponse.set(response);
    }

    public static HttpSession getLocalSession() {
        return (localSession.get() == null) ? (getRequest() != null ? getRequest().getSession(false) : null) : localSession.get();
    }

    public static void setLocalSession(HttpSession session) {
        if (session != null) {
            localSession.set(session);
        }
    }

    public static void resetLocalSession() {
        localSession.set(null);
    }

    /**
     * Get the declared calling IP
     */
    public static String getRemoteAddr(HttpServletRequest request, final boolean useProxiedIPIfAvailable) {
        if (useProxiedIPIfAvailable) {
            String x = request.getHeader(HTTP_X_FORWARDED_FOR);
            if (x != null && x.length() != 0) {
                String remoteAddr = x;
                int idx = remoteAddr.indexOf(',');
                if (idx > -1) {
                    remoteAddr = remoteAddr.substring(0, idx).trim();
                }
                return remoteAddr;
            }
        }
        return request.getRemoteAddr();
    }

    private ServletUtils2() {
    }
}
