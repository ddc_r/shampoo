/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public abstract class SimpleHTTPController extends AbstractController {

    /**
     * Disables HTTP response caching by modifying response headers for browsers.
     * Can be overridden by extending classes to change behaviour.
     * @param request
     * @param response
     **/
    protected void preprocessHTTP(HttpServletRequest request, HttpServletResponse response) {
        //FIX harmful
        //ServletUtils2.disableResponseCaching(response);
    }
    
    /**
     * Returns an HTTP error code
     * @param request
     * @param response
     * @return
     * @throws Exception
     **/
    public abstract int delegatedHandleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        int errorCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
        try {
            ServletUtils2.setRequest(request);
            ServletUtils2.setResponse(response);
            errorCode = delegatedHandleRequest(request, response);
        } finally {
            ServletUtils2.setRequest(null);
            ServletUtils2.setResponse(null);
        }

        //Don't overwrite any status previously stored
        if (!response.isCommitted()) {
            //Do only use sendError() if the HTTP error code is greater than 299 (1xx are informational events and 2xx are successes)
            //Otherwise just use setStatus()
            if (errorCode < 300) {
                response.setStatus(errorCode);
            } else {
                response.sendError(errorCode);
            }
        }
        return null;
    }
}

