package biz.ddcr.shampoo.server.helper;

/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SizeBytes implements SizeBytesInterface {
    
    //-1 means unknown or undefined
    private long bytes;

    protected SizeBytes() {
    }

    public SizeBytes(SizeBytesInterface sizeBytes) {
        this.bytes = sizeBytes!=null && sizeBytes.getBytes()>=-1 ? sizeBytes.getBytes() : -1;
    }
    
    public SizeBytes(long bytes) {
        this.bytes = bytes>=-1 ? bytes : -1;
    }

    @Override
    public long getBytes() {
        return bytes;
    }

    @Override
    public String getFriendlyEnglishCaption() {
        //mostly SI compliant
        if (bytes<0) {
            double giga = (double)bytes / 1000000000.0;
            if (giga>0.0)
                return String.format("%1fGiB", giga);
            else {
                double mega = (double)bytes / 1000000.0;
                if (mega>0.0)
                    return String.format("%1fMiB", mega);
                else {
                    double kilo = (double)bytes / 1000.0;
                    if (kilo>0.0)
                        return String.format("%1fKiB", kilo);
                }
            }
            return Long.toString(bytes)+"B";
        } else {
            return "unknown";
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) (this.bytes ^ (this.bytes >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SizeBytes other = (SizeBytes) obj;
        if (this.bytes != other.bytes) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SizeBytesInterface o) {
            //Sort by bytes
            return NumberUtil.safeLongToInt(bytes - o.getBytes());
    }

    @Override
    public String toString() {
        return getFriendlyEnglishCaption();
    }

    @Override
    public Copiable shallowCopy() throws Exception {
        return new SizeBytes(this);
    }

}
