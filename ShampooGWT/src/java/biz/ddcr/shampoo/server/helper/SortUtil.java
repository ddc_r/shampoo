/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class SortUtil {

    private SortUtil() {
        //cheap singleton
    }

    /** Singleton, no need to instantiate multiple objects **/
    //FIX Sun's Random is apparently weak for some reason, static is hence absolutely not recommended
    //FIX switched implementation
    private final static BetterRandom randomGenerator = new BetterRandom();

    /**
     * Converts a Collection into a List, with Generics
     * @param <T>
     * @param c
     * @return
     */
    public static <T extends Comparable<? super T>>  List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<T>(c);
        Collections.sort(list);
        return list;
    }

    /**
     * Generates a random value between min and max, included
     */
    public static long random(long min, long max) {
        //max should be actually greater than min
        if ( min > max ) {
            long temp = min;
            min = max;
            max = temp;
        }

        //compute a fraction of the range, 0 <= fraction < range where range==max - min + 1
        return (long)((double)(max - min + 1) * randomGenerator.nextDouble()) + min;
    }

}
