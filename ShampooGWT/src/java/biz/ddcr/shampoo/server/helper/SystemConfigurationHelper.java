/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.service.GenericService;
import java.net.MalformedURLException;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright
 *
 */
public class SystemConfigurationHelper extends GenericService {

    /**
     * The GWT modulename, as found in X.gwt.xml*
     */
    private static final String GWT_MODULE_NAME = "gwt";

    //Private and public URL endpoint for streamers and ws'es if the media is to be served by the webserver, not by the datastore itself
    //Make sure their values are synchronized with both the relevant servlet XMLs and the main web.xml
    private String downloadAudioGWTDefaultProtectedRelativeURL = "/gwt/http/down/media/audio";
    private String downloadAudioWSDefaultPublicRelativeURL = "/ws/http/public/down/media/audio";
    private String downloadAudioWSDefaultPrivateRelativeURL = "/ws/http/streamer/audio/down";
    private String downloadCoverArtGWTDefaultProtectedRelativeURL = "/gwt/http/down/media/cover";
    private String downloadFlyerGWTDefaultProtectedRelativeURL = "/gwt/http/down/media/flyer";
    private String downloadCoverArtWSDefaultPublicRelativeURL = "/ws/http/public/down/media/cover";
    private String downloadFlyerWSDefaultPublicRelativeURL = "/ws/http/public/down/media/flyer";
    private String downloadCoverArtWSDefaultPrivateRelativeURL = "/ws/http/streamer/cover/down";
    private String downloadFlyerWSDefaultPrivateRelativeURL = "/ws/http/streamer/flyer/down";
    private String downloadReportGWTDefaultProtectedRelativeURL = "/gwt/http/down/metadata/report";

    //Make sure their values are synchronized with both the relevant servlet XMLs and the main web.xml
    private String uploadAudioTempRelativeURL = "/gwt/http/up/media/audio";
    private String downloadAudioTempRelativeURL = "/gwt/http/down/media/audio/draft";
    private String uploadPictureTempRelativeURL = "/gwt/http/up/media/picture";
    private String downloadPictureTempRelativeURL = "/gwt/http/down/media/picture/draft";

    private String tempPath;

    //Host and deployment path
    private String hostAndDeploymentPrivateURL;
    private String hostAndDeploymentPublicURL;
    private String hostAndDeploymentGWTURL;
    private String privateContextPath;
    private String publicContextPath;
    private String gwtContextPath;
    //Private webservice key
    private String privateKey;

    /**
     * Journal paraameters *
     */
    private boolean logErrorData;
    private boolean logErrorIO;
    private boolean logErrorGeneral;
    //generic entities
    private boolean logUserCreate;
    private boolean logUserUpdate;
    private boolean logUserDelete;
    private boolean logProfileLogin;
    private boolean logProfileLogout;
    private boolean logProfileCreate;
    private boolean logProfileUpdate;
    private boolean logTrackProcessing;
    private boolean logTrackCreate;
    private boolean logTrackUpdate;
    private boolean logTrackDelete;
    private boolean logTrackReject;
    private boolean logTrackValidate;
    private boolean logProgrammeCreate;
    private boolean logProgrammeUpdate;
    private boolean logProgrammeDelete;
    private boolean logPlaylistProcessing;
    private boolean logPlaylistCreate;
    private boolean logPlaylistUpdate;
    private boolean logPlaylistDelete;
    private boolean logChannelCreate;
    private boolean logChannelUpdate;
    private boolean logChannelDelete;
    private boolean logTimetableCreate;
    private boolean logTimetableUpdate;
    private boolean logTimetableDelete;
    private boolean logTimetableClone;
    private boolean logTimetableShift;
    private boolean logTimetableResize;
    private boolean logTimetableCancel;
    private boolean logTimetableConfirm;
    private boolean logArchiveDelete;
    private boolean logReportCreate;
    private boolean logReportDelete;
    private boolean logQueueCreate;
    private boolean logQueueDelete;
    private boolean logWebserviceCreate;
    private boolean logWebserviceUpdate;
    private boolean logWebserviceDelete;
    /**
     * automatic cleaning
     */
    //Do only keep the logs that are logExpirationInDays day old when cleaning is performed; 0 or -1 to disable
    private int logMaxAge;
    //Do only keep the notifications that are notificationExpirationInDays day old when cleaning is performed; 0 or -1 to disable
    private int notificationMaxAge;
    //Do only keep the archives that are archiveExpirationInDays day old when cleaning is performed; 0 or -1 to disable
    private int archiveMaxAge;
    //Do only keep the reports that are reportExpirationInDays day old when cleaning is performed; 0 or -1 to disable
    private int reportMaxAge;

    //Public webservice's modules max. number of items to fetch
    private int publicWSComingNextMaxItems;
    private int publicWSArchiveMaxItems;
    private int publicWSTimetableMaxDays;

    private boolean httpETagGenerationEnabled;
    private boolean httpProxiedIPCheckingEnabled;

    public String getGWTInternalModuleName() {
        return GWT_MODULE_NAME;
    }

    public String getDownloadAudioGWTDefaultProtectedRelativeURL() {
        return downloadAudioGWTDefaultProtectedRelativeURL;
    }

    public void setDownloadAudioGWTDefaultProtectedRelativeURL(String downloadAudioGWTDefaultProtectedRelativeURL) {
        if (downloadAudioGWTDefaultProtectedRelativeURL != null && downloadAudioGWTDefaultProtectedRelativeURL.length() != 0) {
            this.downloadAudioGWTDefaultProtectedRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadAudioGWTDefaultProtectedRelativeURL));
        }
    }

    public String getDownloadAudioTempRelativeURL() {
        return downloadAudioTempRelativeURL;
    }

    public void setDownloadAudioTempRelativeURL(String downloadAudioTempRelativeURL) {
        if (downloadAudioTempRelativeURL != null && downloadAudioTempRelativeURL.length() != 0) {
            this.downloadAudioTempRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadAudioTempRelativeURL));
        }
    }

    public String getDownloadAudioWSDefaultPrivateRelativeURL() {
        return downloadAudioWSDefaultPrivateRelativeURL;
    }

    public void setDownloadAudioWSDefaultPrivateRelativeURL(String downloadAudioWSDefaultPrivateRelativeURL) {
        if (downloadAudioWSDefaultPrivateRelativeURL != null && downloadAudioWSDefaultPrivateRelativeURL.length() != 0) {
            this.downloadAudioWSDefaultPrivateRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadAudioWSDefaultPrivateRelativeURL));
        }
    }

    public String getDownloadAudioWSDefaultPublicRelativeURL() {
        return downloadAudioWSDefaultPublicRelativeURL;
    }

    public void setDownloadAudioWSDefaultPublicRelativeURL(String downloadAudioWSDefaultPublicRelativeURL) {
        if (downloadAudioWSDefaultPublicRelativeURL != null && downloadAudioWSDefaultPublicRelativeURL.length() != 0) {
            this.downloadAudioWSDefaultPublicRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadAudioWSDefaultPublicRelativeURL));
        }
    }

    public String getDownloadCoverArtGWTDefaultProtectedRelativeURL() {
        return downloadCoverArtGWTDefaultProtectedRelativeURL;
    }

    public void setDownloadCoverArtGWTDefaultProtectedRelativeURL(String downloadCoverArtGWTDefaultProtectedRelativeURL) {
        if (downloadCoverArtGWTDefaultProtectedRelativeURL != null && downloadCoverArtGWTDefaultProtectedRelativeURL.length() != 0) {
            this.downloadCoverArtGWTDefaultProtectedRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadCoverArtGWTDefaultProtectedRelativeURL));
        }
    }

    public String getDownloadCoverArtWSDefaultPrivateRelativeURL() {
        return downloadCoverArtWSDefaultPrivateRelativeURL;
    }

    public void setDownloadCoverArtWSDefaultPrivateRelativeURL(String downloadCoverArtWSDefaultPrivateRelativeURL) {
        if (downloadCoverArtWSDefaultPrivateRelativeURL != null && downloadCoverArtWSDefaultPrivateRelativeURL.length() != 0) {
            this.downloadCoverArtWSDefaultPrivateRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadCoverArtWSDefaultPrivateRelativeURL));
        }
    }

    public String getDownloadCoverArtWSDefaultPublicRelativeURL() {
        return downloadCoverArtWSDefaultPublicRelativeURL;
    }

    public void setDownloadCoverArtWSDefaultPublicRelativeURL(String downloadCoverArtWSDefaultPublicRelativeURL) {
        if (downloadCoverArtWSDefaultPublicRelativeURL != null && downloadCoverArtWSDefaultPublicRelativeURL.length() != 0) {
            this.downloadCoverArtWSDefaultPublicRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadCoverArtWSDefaultPublicRelativeURL));
        }
    }

    public String getDownloadFlyerGWTDefaultProtectedRelativeURL() {
        return downloadFlyerGWTDefaultProtectedRelativeURL;
    }

    public void setDownloadFlyerGWTDefaultProtectedRelativeURL(String downloadFlyerGWTDefaultProtectedRelativeURL) {
        if (downloadFlyerGWTDefaultProtectedRelativeURL != null && downloadFlyerGWTDefaultProtectedRelativeURL.length() != 0) {
            this.downloadFlyerGWTDefaultProtectedRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadFlyerGWTDefaultProtectedRelativeURL));
        }
    }

    public String getDownloadFlyerWSDefaultPrivateRelativeURL() {
        return downloadFlyerWSDefaultPrivateRelativeURL;
    }

    public void setDownloadFlyerWSDefaultPrivateRelativeURL(String downloadFlyerWSDefaultPrivateRelativeURL) {
        if (downloadFlyerWSDefaultPrivateRelativeURL != null && downloadFlyerWSDefaultPrivateRelativeURL.length() != 0) {
            this.downloadFlyerWSDefaultPrivateRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadFlyerWSDefaultPrivateRelativeURL));
        }
    }

    public String getDownloadFlyerWSDefaultPublicRelativeURL() {
        return downloadFlyerWSDefaultPublicRelativeURL;
    }

    public void setDownloadFlyerWSDefaultPublicRelativeURL(String downloadFlyerWSDefaultPublicRelativeURL) {
        if (downloadFlyerWSDefaultPublicRelativeURL != null && downloadFlyerWSDefaultPublicRelativeURL.length() != 0) {
            this.downloadFlyerWSDefaultPublicRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadFlyerWSDefaultPublicRelativeURL));
        }
    }

    public String getDownloadReportGWTDefaultProtectedRelativeURL() {
        return downloadReportGWTDefaultProtectedRelativeURL;
    }

    public void setDownloadReportGWTDefaultProtectedRelativeURL(String downloadReportGWTDefaultProtectedRelativeURL) {
        if (downloadReportGWTDefaultProtectedRelativeURL != null && downloadReportGWTDefaultProtectedRelativeURL.length() != 0) {
            this.downloadReportGWTDefaultProtectedRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadReportGWTDefaultProtectedRelativeURL));
        }
    }

    public String getDownloadPictureTempRelativeURL() {
        return downloadPictureTempRelativeURL;
    }

    public void setDownloadPictureTempRelativeURL(String downloadPictureTempRelativeURL) {
        if (downloadPictureTempRelativeURL != null && downloadPictureTempRelativeURL.length() != 0) {
            this.downloadPictureTempRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(downloadPictureTempRelativeURL));
        }
    }

    public String getGWTContextPath() {
        return gwtContextPath;
    }

    private void setGWTContextPath(String contextPath) {
        gwtContextPath = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(contextPath));
    }

    public String getPrivateContextPath() {
        return privateContextPath;
    }

    private void setPrivateContextPath(String contextPath) {
        privateContextPath = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(contextPath));
    }

    public String getPublicContextPath() {
        return publicContextPath;
    }

    private void setPublicContextPath(String contextPath) {
        publicContextPath = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(contextPath));
    }

    public String getHostAndDeploymentGWTURL() {
        return hostAndDeploymentGWTURL;
    }

    public void setHostAndDeploymentGWTURL(HttpServletRequest servletRequest) {
        this.hostAndDeploymentGWTURL = servletRequest.getScheme() + "://" + servletRequest.getServerName() + (servletRequest.getServerPort() > 0 ? (":" + servletRequest.getServerPort()) : "");
        setGWTContextPath(servletRequest.getContextPath());
    }

    public void setHostAndDeploymentGWTURL(String hostAndDeploymentGWTURL) {
        if (hostAndDeploymentGWTURL != null && !hostAndDeploymentGWTURL.isEmpty()) {
            try {
                URL _hostAndDeploymentGWTURL = new URL(hostAndDeploymentGWTURL);
                this.hostAndDeploymentGWTURL = _hostAndDeploymentGWTURL.getProtocol() + "://" + _hostAndDeploymentGWTURL.getHost() + (_hostAndDeploymentGWTURL.getPort() > 0 ? (":" + _hostAndDeploymentGWTURL.getPort()) : "");
                setGWTContextPath(_hostAndDeploymentGWTURL.getPath());
            } catch (MalformedURLException ex) {
                logger.error("Malformed hostAndDeploymentGWTURL, check your configuration files, there is no default value to revert to", ex);
            }
        } else {
            logger.warn("hostAndDeploymentGWTURL has not been manually configured, autodetecting the correct value might fail");
        }
    }

    public String getHostAndDeploymentPrivateURL() {
        return hostAndDeploymentPrivateURL;
    }

    public void setHostAndDeploymentPrivateURL(HttpServletRequest servletRequest) {
        this.hostAndDeploymentPrivateURL = servletRequest.getScheme() + "://" + servletRequest.getServerName() + (servletRequest.getServerPort() > 0 ? (":" + servletRequest.getServerPort()) : "");
        setPrivateContextPath(servletRequest.getContextPath());
    }

    public void setHostAndDeploymentPrivateURL(String hostAndDeploymentPrivateURL) {
        if (hostAndDeploymentPrivateURL != null && !hostAndDeploymentPrivateURL.isEmpty()) {
            try {
                URL _hostAndDeploymentPrivateURL = new URL(hostAndDeploymentPrivateURL);
                this.hostAndDeploymentPrivateURL = _hostAndDeploymentPrivateURL.getProtocol() + "://" + _hostAndDeploymentPrivateURL.getHost() + (_hostAndDeploymentPrivateURL.getPort() > 0 ? (":" + _hostAndDeploymentPrivateURL.getPort()) : "");
                setPrivateContextPath(_hostAndDeploymentPrivateURL.getPath());
            } catch (MalformedURLException ex) {
                logger.error("Malformed hostAndDeploymentPrivateURL, check your configuration files, there is no default value to revert to", ex);
            }
        } else {
            logger.warn("hostAndDeploymentPrivateURL has not been manually configured, autodetecting the correct value might fail");
        }
    }

    public String getHostAndDeploymentPublicURL() {
        return hostAndDeploymentPublicURL;
    }

    public void setHostAndDeploymentPublicURL(HttpServletRequest servletRequest) {
        this.hostAndDeploymentPublicURL = servletRequest.getScheme() + "://" + servletRequest.getServerName() + (servletRequest.getServerPort() > 0 ? (":" + servletRequest.getServerPort()) : "");
        setPublicContextPath(servletRequest.getContextPath());
    }

    public void setHostAndDeploymentPublicURL(String hostAndDeploymentPublicURL) {
        if (hostAndDeploymentPublicURL != null && !hostAndDeploymentPublicURL.isEmpty()) {
            try {
                URL _hostAndDeploymentPublicURL = new URL(hostAndDeploymentPublicURL);
                this.hostAndDeploymentPublicURL = _hostAndDeploymentPublicURL.getProtocol() + "://" + _hostAndDeploymentPublicURL.getHost() + (_hostAndDeploymentPublicURL.getPort() > 0 ? (":" + _hostAndDeploymentPublicURL.getPort()) : "");
                setPublicContextPath(_hostAndDeploymentPublicURL.getPath());
            } catch (MalformedURLException ex) {
                logger.error("Malformed hostAndDeploymentPublicURL, check your configuration files, there is no default value to revert to", ex);
            }
        } else {
            logger.warn("hostAndDeploymentPublicURL has not been manually configured, autodetecting the correct value might fail");
        }
    }

    public String getUploadAudioTempRelativeURL() {
        return uploadAudioTempRelativeURL;
    }

    public void setUploadAudioTempRelativeURL(String uploadAudioTempRelativeURL) {
        if (uploadAudioTempRelativeURL != null && uploadAudioTempRelativeURL.length() != 0) {
            this.uploadAudioTempRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(uploadAudioTempRelativeURL));
        }
    }

    public String getUploadPictureTempRelativeURL() {
        return uploadPictureTempRelativeURL;
    }

    public void setUploadPictureTempRelativeURL(String uploadPictureTempRelativeURL) {
        if (uploadPictureTempRelativeURL != null && uploadPictureTempRelativeURL.length() != 0) {
            this.uploadPictureTempRelativeURL = MarshallUtil.addStartingSlash(MarshallUtil.removeTrailingSlash(uploadPictureTempRelativeURL));
        }
    }

    public boolean isLogErrorData() {
        return logErrorData;
    }

    public void setLogErrorData(boolean logErrorData) {
        this.logErrorData = logErrorData;
    }

    public boolean isLogErrorGeneral() {
        return logErrorGeneral;
    }

    public void setLogErrorGeneral(boolean logErrorGeneral) {
        this.logErrorGeneral = logErrorGeneral;
    }

    public boolean isLogErrorIO() {
        return logErrorIO;
    }

    public void setLogErrorIO(boolean logErrorIO) {
        this.logErrorIO = logErrorIO;
    }

    public boolean isLogProfileLogin() {
        return logProfileLogin;
    }

    public void setLogProfileLogin(boolean logProfileLogin) {
        this.logProfileLogin = logProfileLogin;
    }

    public boolean isLogProfileLogout() {
        return logProfileLogout;
    }

    public void setLogProfileLogout(boolean logProfileLogout) {
        this.logProfileLogout = logProfileLogout;
    }

    public boolean isLogProfileUpdate() {
        return logProfileUpdate;
    }

    public void setLogProfileUpdate(boolean logProfileUpdate) {
        this.logProfileUpdate = logProfileUpdate;
    }

    public boolean isLogProfileCreate() {
        return logProfileCreate;
    }

    public void setLogProfileCreate(boolean logProfileCreate) {
        this.logProfileCreate = logProfileCreate;
    }

    public boolean isLogUserCreate() {
        return logUserCreate;
    }

    public void setLogUserCreate(boolean logUserCreate) {
        this.logUserCreate = logUserCreate;
    }

    public boolean isLogUserDelete() {
        return logUserDelete;
    }

    public void setLogUserDelete(boolean logUserDelete) {
        this.logUserDelete = logUserDelete;
    }

    public boolean isLogUserUpdate() {
        return logUserUpdate;
    }

    public void setLogUserUpdate(boolean logUserUpdate) {
        this.logUserUpdate = logUserUpdate;
    }

    public boolean isLogTrackCreate() {
        return logTrackCreate;
    }

    public void setLogTrackCreate(boolean logTrackCreate) {
        this.logTrackCreate = logTrackCreate;
    }

    public boolean isLogTrackDelete() {
        return logTrackDelete;
    }

    public void setLogTrackDelete(boolean logTrackDelete) {
        this.logTrackDelete = logTrackDelete;
    }

    public boolean isLogTrackReject() {
        return logTrackReject;
    }

    public void setLogTrackReject(boolean logTrackReject) {
        this.logTrackReject = logTrackReject;
    }

    public boolean isLogTrackUpdate() {
        return logTrackUpdate;
    }

    public void setLogTrackUpdate(boolean logTrackUpdate) {
        this.logTrackUpdate = logTrackUpdate;
    }

    public boolean isLogTrackValidate() {
        return logTrackValidate;
    }

    public void setLogTrackValidate(boolean logTrackValidate) {
        this.logTrackValidate = logTrackValidate;
    }

    public boolean isLogProgrammeCreate() {
        return logProgrammeCreate;
    }

    public void setLogProgrammeCreate(boolean logProgrammeCreate) {
        this.logProgrammeCreate = logProgrammeCreate;
    }

    public boolean isLogProgrammeDelete() {
        return logProgrammeDelete;
    }

    public void setLogProgrammeDelete(boolean logProgrammeDelete) {
        this.logProgrammeDelete = logProgrammeDelete;
    }

    public boolean isLogProgrammeUpdate() {
        return logProgrammeUpdate;
    }

    public void setLogProgrammeUpdate(boolean logProgrammeUpdate) {
        this.logProgrammeUpdate = logProgrammeUpdate;
    }

    public boolean isLogPlaylistCreate() {
        return logPlaylistCreate;
    }

    public void setLogPlaylistCreate(boolean logPlaylistCreate) {
        this.logPlaylistCreate = logPlaylistCreate;
    }

    public boolean isLogPlaylistDelete() {
        return logPlaylistDelete;
    }

    public void setLogPlaylistDelete(boolean logPlaylistDelete) {
        this.logPlaylistDelete = logPlaylistDelete;
    }

    public boolean isLogPlaylistUpdate() {
        return logPlaylistUpdate;
    }

    public void setLogPlaylistUpdate(boolean logPlaylistUpdate) {
        this.logPlaylistUpdate = logPlaylistUpdate;
    }

    public boolean isLogChannelCreate() {
        return logChannelCreate;
    }

    public void setLogChannelCreate(boolean logChannelCreate) {
        this.logChannelCreate = logChannelCreate;
    }

    public boolean isLogChannelDelete() {
        return logChannelDelete;
    }

    public void setLogChannelDelete(boolean logChannelDelete) {
        this.logChannelDelete = logChannelDelete;
    }

    public boolean isLogChannelUpdate() {
        return logChannelUpdate;
    }

    public void setLogChannelUpdate(boolean logChannelUpdate) {
        this.logChannelUpdate = logChannelUpdate;
    }

    public boolean isLogTimetableCancel() {
        return logTimetableCancel;
    }

    public void setLogTimetableCancel(boolean logTimetableCancel) {
        this.logTimetableCancel = logTimetableCancel;
    }

    public boolean isLogTimetableConfirm() {
        return logTimetableConfirm;
    }

    public void setLogTimetableConfirm(boolean logTimetableConfirm) {
        this.logTimetableConfirm = logTimetableConfirm;
    }

    public boolean isLogTimetableCreate() {
        return logTimetableCreate;
    }

    public void setLogTimetableCreate(boolean logTimetableCreate) {
        this.logTimetableCreate = logTimetableCreate;
    }

    public boolean isLogTimetableDelete() {
        return logTimetableDelete;
    }

    public void setLogTimetableDelete(boolean logTimetableDelete) {
        this.logTimetableDelete = logTimetableDelete;
    }

    public boolean isLogTimetableResize() {
        return logTimetableResize;
    }

    public void setLogTimetableResize(boolean logTimetableResize) {
        this.logTimetableResize = logTimetableResize;
    }

    public boolean isLogTimetableShift() {
        return logTimetableShift;
    }

    public void setLogTimetableShift(boolean logTimetableShift) {
        this.logTimetableShift = logTimetableShift;
    }

    public boolean isLogTimetableUpdate() {
        return logTimetableUpdate;
    }

    public void setLogTimetableUpdate(boolean logTimetableUpdate) {
        this.logTimetableUpdate = logTimetableUpdate;
    }

    public boolean isLogTimetableClone() {
        return logTimetableClone;
    }

    public void setLogTimetableClone(boolean logTimetableClone) {
        this.logTimetableClone = logTimetableClone;
    }

    public boolean isLogArchiveDelete() {
        return logArchiveDelete;
    }

    public void setLogArchiveDelete(boolean logArchiveDelete) {
        this.logArchiveDelete = logArchiveDelete;
    }

    public boolean isLogReportCreate() {
        return logReportCreate;
    }

    public void setLogReportCreate(boolean logReportCreate) {
        this.logReportCreate = logReportCreate;
    }

    public boolean isLogReportDelete() {
        return logReportDelete;
    }

    public void setLogReportDelete(boolean logReportDelete) {
        this.logReportDelete = logReportDelete;
    }

    public boolean isLogQueueCreate() {
        return logQueueCreate;
    }

    public void setLogQueueCreate(boolean logQueueCreate) {
        this.logQueueCreate = logQueueCreate;
    }

    public boolean isLogQueueDelete() {
        return logQueueDelete;
    }

    public void setLogQueueDelete(boolean logQueueDelete) {
        this.logQueueDelete = logQueueDelete;
    }

    public boolean isLogWebserviceCreate() {
        return logWebserviceCreate;
    }

    public void setLogWebserviceCreate(boolean logWebserviceCreate) {
        this.logWebserviceCreate = logWebserviceCreate;
    }

    public boolean isLogWebserviceDelete() {
        return logWebserviceDelete;
    }

    public void setLogWebserviceDelete(boolean logWebserviceDelete) {
        this.logWebserviceDelete = logWebserviceDelete;
    }

    public boolean isLogWebserviceUpdate() {
        return logWebserviceUpdate;
    }

    public void setLogWebserviceUpdate(boolean logWebserviceUpdate) {
        this.logWebserviceUpdate = logWebserviceUpdate;
    }

    public boolean isLogPlaylistProcessing() {
        return logPlaylistProcessing;
    }

    public void setLogPlaylistProcessing(boolean logPlaylistProcessing) {
        this.logPlaylistProcessing = logPlaylistProcessing;
    }

    public boolean isLogTrackProcessing() {
        return logTrackProcessing;
    }

    public void setLogTrackProcessing(boolean logTrackProcessing) {
        this.logTrackProcessing = logTrackProcessing;
    }

    public int getLogMaxAge() {
        return logMaxAge;
    }

    public void setLogMaxAge(int logMaxAge) {
        this.logMaxAge = logMaxAge;
    }

    public int getNotificationMaxAge() {
        return notificationMaxAge;
    }

    public void setNotificationMaxAge(int notificationMaxAge) {
        this.notificationMaxAge = notificationMaxAge;
    }

    public int getArchiveMaxAge() {
        return archiveMaxAge;
    }

    public void setArchiveMaxAge(int archiveMaxAge) {
        this.archiveMaxAge = archiveMaxAge;
    }

    public int getReportMaxAge() {
        return reportMaxAge;
    }

    public void setReportMaxAge(int reportMaxAge) {
        this.reportMaxAge = reportMaxAge;
    }

    public int getPublicWSComingNextMaxItems() {
        return publicWSComingNextMaxItems;
    }

    public void setPublicWSComingNextMaxItems(int publicWSComingNextMaxItems) {
        if (publicWSComingNextMaxItems < 0) {
            throw new IllegalArgumentException("publicWSComingNextMaxItems must be strictly positive");
        }
        this.publicWSComingNextMaxItems = publicWSComingNextMaxItems;
    }

    public int getPublicWSArchiveMaxItems() {
        return publicWSArchiveMaxItems;
    }

    public void setPublicWSArchiveMaxItems(int publicWSArchiveMaxItems) {
        if (publicWSArchiveMaxItems < 0) {
            throw new IllegalArgumentException("publicWSArchiveMaxItems must be strictly positive");
        }
        this.publicWSArchiveMaxItems = publicWSArchiveMaxItems;
    }

    public int getPublicWSTimetableMaxDays() {
        return publicWSTimetableMaxDays;
    }

    public void setPublicWSTimetableMaxDays(int publicWSTimetableMaxDays) {
        if (publicWSTimetableMaxDays < 0) {
            throw new IllegalArgumentException("publicWSTimetableMaxDays must be strictly positive");
        }
        this.publicWSTimetableMaxDays = publicWSTimetableMaxDays;
    }

    public String getTempPath() {

        if (tempPath == null || tempPath.length() == 0) {
            //Defaults to system temporary storage space
            tempPath = System.getProperty("java.io.tmpdir");
        }
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getTemporaryAudioUploadURL() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getUploadAudioTempRelativeURL());
    }

    public String getTemporaryPictureUploadURL() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getUploadPictureTempRelativeURL());
    }

    public String getTemporaryAudioDownloadURL() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadAudioTempRelativeURL());
    }

    public String getTemporaryPictureDownloadURL() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadPictureTempRelativeURL());
    }

    public String getDefaultWSAudioURLPrivateEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentPrivateURL()) + MarshallUtil.removeTrailingSlash(getPrivateContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadAudioWSDefaultPrivateRelativeURL());
    }

    public String getDefaultWSCoverArtURLPrivateEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentPrivateURL()) + MarshallUtil.removeTrailingSlash(getPrivateContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadCoverArtWSDefaultPrivateRelativeURL());
    }

    public String getDefaultWSFlyerURLPrivateEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentPrivateURL()) + MarshallUtil.removeTrailingSlash(getPrivateContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadFlyerWSDefaultPrivateRelativeURL());
    }

    public String getDefaultWSAudioURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentPublicURL()) + MarshallUtil.removeTrailingSlash(getPublicContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadAudioWSDefaultPublicRelativeURL());
    }

    public String getDefaultWSCoverArtURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentPublicURL()) + MarshallUtil.removeTrailingSlash(getPublicContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadCoverArtWSDefaultPublicRelativeURL());
    }

    public String getDefaultWSFlyerURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentPublicURL()) + MarshallUtil.removeTrailingSlash(getPublicContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadFlyerWSDefaultPublicRelativeURL());
    }

    public String getDefaultGWTAudioURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadAudioGWTDefaultProtectedRelativeURL());
    }

    public String getDefaultGWTCoverArtURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadCoverArtGWTDefaultProtectedRelativeURL());
    }

    public String getDefaultGWTFlyerURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadFlyerGWTDefaultProtectedRelativeURL());
    }

    public String getDefaultGWTReportURLPublicEndpoint() {
        return MarshallUtil.removeTrailingSlash(getHostAndDeploymentGWTURL()) + MarshallUtil.removeTrailingSlash(getGWTContextPath()) + MarshallUtil.removeTrailingSlash(getDownloadReportGWTDefaultProtectedRelativeURL());
    }

    public boolean isHttpETagGenerationEnabled() {
        return httpETagGenerationEnabled;
    }

    public void setHttpETagGenerationEnabled(boolean clientHTTPCachingEnabled) {
        this.httpETagGenerationEnabled = clientHTTPCachingEnabled;
    }

    public boolean isHttpProxiedIPCheckingEnabled() {
        return httpProxiedIPCheckingEnabled;
    }

    public void setHttpProxiedIPCheckingEnabled(boolean httpProxiedIPCheckingEnabled) {
        this.httpProxiedIPCheckingEnabled = httpProxiedIPCheckingEnabled;
    }

}
