/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.helper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.task.TaskExecutor;

/**
 *
 * @author okay_awright
 **/
public class TaskPerformer<T extends MicroTask> {

    protected final Log logger = LogFactory.getLog(getClass());
    private int latestTaskId = 0;

    public abstract class RunnableTask implements Runnable {

        private final MicroTask microTask;
        private final TaskCallback statusHandler;
        private RuntimeException wrappedException = null;

        public RunnableTask(MicroTask microTask, TaskCallback statusHandler) {
            if (microTask == null) {
                throw new NullPointerException("MicroTask cannot be void");
            }
            this.microTask = microTask;
            this.statusHandler = statusHandler;
        }

        public RunnableTask(MicroTask microTask) {
            this(microTask, null);
        }

        protected void propagateExceptionOutsideThread(Exception e) throws RuntimeException{
            if (RuntimeException.class.isAssignableFrom(e.getClass()))
                //Re-throw as-is
                wrappedException = (RuntimeException)e;
            else
                //Otherwise wrap everything within a RuntimeException
                wrappedException = new RuntimeException("TaskPerformed failed", e);
        }
        
        protected synchronized void onInit() throws Exception {
            if (statusHandler != null) {
                statusHandler.onInit();
            }
        }

        protected synchronized void onSuccess() throws Exception {
            if (statusHandler != null) {
                statusHandler.onSuccess();
            }
        }

        protected synchronized void onFailure(Exception e) throws Exception {
            if (statusHandler != null) {
                statusHandler.onFailure(e);
            }
        }

        protected synchronized void onProgress(long l) {
            if (statusHandler != null) {
                statusHandler.onProgress(l);
            }
        }

        public boolean isInterrupted() {
            return Thread.currentThread().isInterrupted();
        }

        public void abort() {
            Thread.currentThread().interrupt();
        }

        @Override
        public void run() {
            hasStarted();
            try {
                onInit();
                for (long l = 0; microTask.getNumberOfLoops() < 0 || l < microTask.getNumberOfLoops(); l++) {
                    //Check if the thread is interrupted
                    if (Thread.currentThread().isInterrupted()) {
                        throw new InterruptedException();
                    }
                    //notify the backend which loop we're in
                    onProgress(l);
                    //Run one chunk of the global task
                    if (!microTask.loop()) {
                        break;
                    }
                }
                onSuccess();
            } catch (Exception e) {
                logger.error("Asynchronous operation failed", e);
                try {
                    if (e instanceof InterruptedException) {
                        Thread.currentThread().interrupt();
                    }
                    onFailure(e);
                } catch (Exception ex) {
                    //This will mainly re-throw any exception raised in onFailure()
                    propagateExceptionOutsideThread(ex);
                }
            } finally {
                hasEnded();
            }
            //Check if an exception must be thrown outside of the scope of this thread
            if (wrappedException!=null)
                throw wrappedException;
        }

        public abstract void hasStarted();

        public abstract void hasEnded();
    }
    private Map<Integer, RunnableTask> tasks;
    private TaskExecutor taskExecutor;

    public TaskPerformer(TaskExecutor taskExecutor) {
        if (taskExecutor == null) {
            throw new NullPointerException("TaskExecutor cannot be void");
        }
        this.taskExecutor = taskExecutor;
        tasks = new ConcurrentHashMap<Integer, RunnableTask>();
    }

    public int runTask(T task) throws Exception {
        return runTask(task, null);
    };
    public int runTask(T task, TaskCallback callback) throws Exception {

        //Compute a task handle
        if (latestTaskId == Integer.MAX_VALUE) {
            latestTaskId = Integer.MIN_VALUE;
        }
        final int taskId = latestTaskId++;

        //Prepare the task
        RunnableTask wrappedTask = new RunnableTask(task, callback) {

            @Override
            public void hasStarted() {
                registerTask(taskId, this);
            }

            @Override
            public void hasEnded() {
                unregisterTask(taskId);
            }
        };
        //Run it
        taskExecutor.execute(wrappedTask);
        return taskId;
    }

    protected boolean registerTask(int taskId, RunnableTask t) {
        tasks.put(taskId, t);
        return true;
    }

    protected boolean unregisterTask(int taskId) {
        return tasks.remove(taskId) != null;
    }

    public boolean abortTask(int taskId) {
        RunnableTask task = tasks.get(taskId);
        if (task != null) {
            if (!task.isInterrupted()) {
                task.abort();
            }
            return true;
        }
        return false;
    }
}
