/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.client.helper.errors.DatabaseConcurrencyLockFailedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.GenericDatabaseException;
import biz.ddcr.shampoo.client.helper.errors.GenericPresentationException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.server.domain.journal.ExceptionLog.EXCEPTION_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.journal.ExceptionLog;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.TransientDataAccessException;

/**
 *
 * Will intercept Hibernate transaction exceptions for GWT and make them go through GWT serialization policy white list while outputing something more user-friendly.
 * It will also retry submitting a transaction if an optimistic lock failed.
 * Be sure not to bind it inside a Spring-controlled transaction but just after.
 *
 * @author okay_awright
 **/
public class TransactionInterceptor extends GenericService implements MethodInterceptor {

    private int maxRetries = 1;
    private long retryDelay = 10L; // 10ms
    //Manager used for logging exceptions
    private SecurityManagerInterface securityManager;

    public int getOptimisticLockMaxRetries() {
        return maxRetries;
    }

    public void setOptimisticLockMaxRetries(int maxRetries) {
        this.maxRetries = Math.max(maxRetries, 1);
    }

    public long getOptimisticLockRetryDelay() {
        return retryDelay;
    }

    public void setOptimisticLockRetryDelay(long retryDelay) {
        this.retryDelay = Math.max(retryDelay, 5);
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /**
     * Decide whther we should display the whole stacktrace or not, depending on the kind of Exception
     * @param message
     * @param throwable 
     */
    protected void printError(final String message, final Throwable throwable) {
        if (throwable!=null) {
            final StringBuilder s = new StringBuilder(message);
            if (throwable instanceof GenericPresentationException) {
                s.append(": ").append(throwable.getClass().getCanonicalName());
                if (throwable.getMessage()!=null && throwable.getMessage().length()!=0)
                    s.append(": ").append(throwable.getMessage());
            } else {
                //Does only print stacktraces if the error is not handled
                final ByteArrayOutputStream b = new ByteArrayOutputStream();
                final PrintStream w = new PrintStream(b);
                try {
                    throwable.printStackTrace(w);
                    s.append(": ").append(b.toString());
                } finally {
                    w.close();
                }
            }
            logger.error(s.toString());
        }
    }
    
    /**
     * Wrap each method embedded within a transaction so as to output a user friendly message in case of error, or retry if applicable
     * @param methodInvocation
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        try {
            
            //Proceed to original method call
            return invokeAndRetryIfApplicable(methodInvocation);
            
        } catch (DataIntegrityViolationException e) {
            
            //Log it
            try {
                //Log it into the servlet container as well to ease proper bug reporting
                printError("Failure detected within transaction", e);
                getSecurityManager().addJournal(new ExceptionLog(EXCEPTION_LOG_OPERATION.data, e));
            } catch (Exception e1) {
                logger.error("Logging failed while DataIntegrityViolationException was captured");
            }
            //Exception thrown when an attempt to insert or update data results in violation of an integrity constraint
            //Or it could also be th result of a bad Hibernate mapping
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        } catch (DataRetrievalFailureException e) {
            
            //Log it
            try {
                //Log it into the servlet container as well to ease proper bug reporting
                printError("Failure detected within transaction", e);
                getSecurityManager().addJournal(new ExceptionLog(EXCEPTION_LOG_OPERATION.data, e));                
            } catch (Exception e1) {
                logger.error("Logging failed while DataRetrievalFailureException was captured");
            }
            //Exception thrown if certain expected data could not be retrieved
            throw new NoEntityException(e.getClass().getCanonicalName());
        } catch (DataAccessException e) {
            
            //Log it
            try {
                //Log it into the servlet container as well to ease proper bug reporting
                printError("Failure detected within transaction", e);
                getSecurityManager().addJournal(new ExceptionLog(EXCEPTION_LOG_OPERATION.data, e));                
            } catch (Exception e1) {
                logger.error("Logging failed while DataAccessException was captured");
            }
            //everything else
            throw new GenericDatabaseException(e.getClass().getCanonicalName());
        } catch (IOException e) {
            
            //Log it
            try {
                //Log it into the servlet container as well to ease proper bug reporting
                printError("Failure detected within transaction", e);
                getSecurityManager().addJournal(new ExceptionLog(EXCEPTION_LOG_OPERATION.io, e));
            } catch (Exception e1) {
                logger.error("Logging failed while IOException was captured");
            }
            //Rethrow the unmanaged Exception
            throw e;
        } catch (Exception e) {
            
            //Log it
            try {
                //Log it into the servlet container as well to ease proper bug reporting
                printError("Failure detected within transaction", e);
                getSecurityManager().addJournal(new ExceptionLog(EXCEPTION_LOG_OPERATION.general, e));
            } catch (Exception e1) {
                logger.error("Logging failed while generic Exception was captured");
            }
            //Rethrow the unmanaged Exception
            throw e;
        }
    }

    /**
     * Call the method which ultimately failed until it either succeeds, or the number of allowed retries has been entirely consumed
     * @param methodInvocation
     * @return
     * @throws Throwable
     */
    private Object invokeAndRetryIfApplicable(MethodInvocation methodInvocation) throws Throwable {
        int numRetries = getOptimisticLockMaxRetries();
        while (numRetries-- > 0) {
            try {
                return methodInvocation.proceed();
            } catch (TransientDataAccessException e) {
                //Exception thrown on concurrency failure
                if (numRetries <= 0) {
                    throw new DatabaseConcurrencyLockFailedException(e.getClass().getCanonicalName());
                }
                Thread.sleep(getOptimisticLockRetryDelay());
            }
        }
        //it's hopeless (but you can't end up here)
        throw new DatabaseConcurrencyLockFailedException("The maximum number of automatic retries has been consumed");
    }

}
