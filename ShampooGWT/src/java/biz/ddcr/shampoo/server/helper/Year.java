/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Base for a calendar, representing a year, ISO compliant except for week computations.
 * First day of week is Monday, and not Sunday.
 * First week of month is numbered 1, it's defined as the first full Monday to Sunday period within a month. Week 0 is equivalent to the last week of the previous month.
 * @author okay_awright
 **/
public class Year implements YearInterface {
    
    private DateTimeZone timezone;
    private DateTime calendar;
    
    protected final void setTZ(final String tz) {
        timezone = tz!=null ? DateTimeZone.forID(tz) : null;
    }
    protected final void setTZ(final DateTimeZone tz) {
        timezone = tz;
    }
    protected final DateTimeZone getTZ() {
        if (timezone==null) timezone = DateTimeZone.getDefault();
        return timezone;
    }
    protected final void setDT(final DateTime dt) {
        calendar = dt;
    }
    protected final DateTime getDT() {
        if (calendar==null) calendar = new DateTime(0, getTZ());
        return calendar;
    }
    
    protected Year() {
        //Do not directly use
    }
    
    public Year(long unixTime, String tz) {
        setTZ(tz);
        setUnixTime(unixTime);
    }

    public Year(short year, String tz) {
        setTZ(tz);
        setYear(year);
    }

    public Year(YearInterface yearWithTimeZone) {
        this(
            yearWithTimeZone!=null?yearWithTimeZone.getYear():(short)1970,
            yearWithTimeZone!=null?yearWithTimeZone.getTimeZone():null);
    }    
    
    @Override
    public void setUnixTime(long unixTime) {
        final DateTime _dt = new DateTime(unixTime, getTZ());
        setYear(NumberUtil.safeIntToShort(_dt.getYear()));
    }

    @Override
    public short getYear() {            
        return NumberUtil.safeIntToShort(getDT().getYear());
    }

    @Override
    public void setYear(short year) {
        setDT(getDT().withYear(year));
    }

    @Override
    public void addYears(long years) {
        setDT(getDT().plusYears(NumberUtil.safeLongToInt(years)));
        /*NumberUtil.foldLongToInt(years, new NumberUtil.Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value!=null) setDT(getDT().plusYears(value));
            }
        });*/
    }

    @Override
    public long getUnixTime() {
        return getDT().getMillis();
    }

    @Override
    public String getTimeZone() {
        return getTZ().getID();
    }

    @Override
    public Year shallowCopy() {
        return new Year(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == null) {
            return true;
        }
        if ((obj == null) || getClass() != obj.getClass()) {
            return false;
        }
        Year test = (Year)obj;
        return super.equals(test)
                && (getDT() == test.getDT() || getDT().equals(test.getDT()))
                ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + super.hashCode();
        hash = 31 * hash + getDT().hashCode();
        //No lazily loaded objects can be used
        return hash;
    }

    @Override
    public int compareTo(Object anotherObject) {
        if (anotherObject!=null && ProxiedClassUtil.castableAs(anotherObject, Year.class)) {
            //Sort by time
            Year anotherYearWithTimeZone = (Year)anotherObject;

            long delta = getUnixTime() - anotherYearWithTimeZone.getUnixTime();
            return delta<0?-1:(delta>0?1:0);
        }
        return 0;
    }

    @Override
    public String toString() {
        return String.format("%1$d", getYear());
    }

    @Override
    public String getFriendlyEnglishCaption() {
        return toString();
    }

}
