/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.helper;

import org.joda.time.DateTime;

/**
 *
 * @author okay_awright
 **/
public class YearMonth extends Year implements YearMonthInterface {
   
    protected YearMonth() {
        //Do not directly use
    }
    
    public YearMonth(long unixTime, String timezone) {
        super(unixTime, timezone);
    }

    public YearMonth(YearInterface yearWithTimeZone) {
        super(yearWithTimeZone);
    }
    
    public YearMonth(YearMonthInterface yearMonthWithTimeZone) {
            this(
                yearMonthWithTimeZone!=null?yearMonthWithTimeZone.getYear():(short)1970,
                yearMonthWithTimeZone!=null?yearMonthWithTimeZone.getMonthOfYear():(byte)1,
                yearMonthWithTimeZone!=null?yearMonthWithTimeZone.getTimeZone():null);
    }

    public YearMonth(short year, byte month, String timezone) {
        setTZ(timezone);
        setYearMonthOfYear(year, month);
    }

    public void setYearMonthOfYear(short year, byte monthOfYear) {
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear));
    }
    
    @Override
    public void setUnixTime(long unixTime) {
        final DateTime _dt = new DateTime(unixTime, getTZ());
        setYearMonthOfYear(NumberUtil.safeIntToShort(_dt.getYear()), NumberUtil.safeIntToByte(_dt.getMonthOfYear()));
    }
    
    @Override
    public byte getMonthOfYear() {
        return NumberUtil.safeIntToByte(getDT().getMonthOfYear());
    }

    @Override
    public void setMonthOfYear(byte month) {
        setDT(getDT().withMonthOfYear(month));
    }    
    
    @Override
    public void addMonths(long months) {
        setDT(getDT().plusMonths(NumberUtil.safeLongToInt(months)));
        /*NumberUtil.foldLongToInt(months, new NumberUtil.Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value!=null) setDT(getDT().plusMonths(value));
            }
        });*/
    }

    @Override
    public long getMillisecondInYearPeriod() {
        Year periodStart = new Year(this);
        return getUnixTime() - periodStart.getUnixTime();
    }
       
    @Override
    public YearMonth shallowCopy() {
        return new YearMonth(this);
    }

    @Override
    public String toString() {
        return String.format("%1$d-%2$02d", getYear(), getMonthOfYear());
    }

}
