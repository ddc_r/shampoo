/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.helper;

import org.joda.time.DateTime;

/**
 * 
 * @author okay_awright
 **/
public class YearMonthWeek extends YearMonth implements YearMonthWeekInterface {
    
    protected YearMonthWeek() {
        //Do not directly use
    }
    
    public YearMonthWeek(long unixTime, String timezone) {
        super(unixTime, timezone);
    }

    public YearMonthWeek(YearMonthInterface yearMonthWithTimeZone) {
        super(yearMonthWithTimeZone);
    }
    public YearMonthWeek(YearInterface yearWithTimeZone) {
        super(yearWithTimeZone);
    }
    
    public YearMonthWeek(YearMonthWeekInterface yearMonthWeekWithTimeZone) {
            this(
                yearMonthWeekWithTimeZone!=null?yearMonthWeekWithTimeZone.getYear():(short)1970,
                yearMonthWeekWithTimeZone!=null?yearMonthWeekWithTimeZone.getMonthOfYear():(byte)1,
                yearMonthWeekWithTimeZone!=null?yearMonthWeekWithTimeZone.getWeekOfMonth():(byte)0,
                yearMonthWeekWithTimeZone!=null?yearMonthWeekWithTimeZone.getTimeZone():null);
    }

    public YearMonthWeek(short year, byte month, byte weekOfMonth, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearWeekOfMonth(year, month, weekOfMonth);
    }

    /**First week of month is numbered 1, it's defined as the first full Monday to Sunday period within a month
     * If one wants to know the first day of week 0 of a month and that month doesn't start on Monday then the result is lower than 1
     */
    private byte getFirstDayOfWeekOfMonth(short year, byte monthOfYear, byte weekOfMonth) {        
        
        //get week day index of the first day of the month
        final DateTime firstDayOfMonth = new DateTime((int)year, (int)monthOfYear, 1, 0, 0, getTZ());
        final int weekDayOfFirstDayOfMonth = firstDayOfMonth.getDayOfWeek();
        
        final int shiftToMondayOfFirstWeek = ((7 - weekDayOfFirstDayOfMonth) + 1) % 7;
                       
        //Allow bleeding by accepting days of month out of range of the actual month
        final int firstDayOfWeekOfMonth = (shiftToMondayOfFirstWeek + 1) + (weekOfMonth - 1) * 7;        
        return NumberUtil.safeIntToByte(firstDayOfWeekOfMonth);
    }            
    
    private byte getWeekOfMonth(short year, byte monthOfYear, byte dayOfMonth) {        

        byte firstDayOfWeekOfMonth = getFirstDayOfWeekOfMonth(year, monthOfYear, (byte)1);
        
        //Allow bleeding by accepting days of month out of range of the actual month
        return NumberUtil.safeLongToByte((long)(Math.floor((dayOfMonth - firstDayOfWeekOfMonth)/7.0)+1));
    }
    
    public void setYearMonthOfYearWeekOfMonth(short year, byte monthOfYear, byte weekOfMonth) {
        final byte dayWithinTheWeekForThatMonth = (byte) Math.max(1, getFirstDayOfWeekOfMonth(year, monthOfYear, weekOfMonth));
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayWithinTheWeekForThatMonth));
    }
    
    @Override
    public void setUnixTime(long unixTime) {
        final DateTime _dt = new DateTime(unixTime, getTZ());
        final short year = NumberUtil.safeIntToShort(_dt.getYear());
        final byte month = NumberUtil.safeIntToByte(_dt.getMonthOfYear());
        final byte day = NumberUtil.safeIntToByte(_dt.getDayOfMonth());
        setYearMonthOfYearWeekOfMonth(//
                year,//
                month,//
                getWeekOfMonth(year, month, day));
    }
    
    @Override
    public long getMillisecondInMonthPeriod() {
        YearMonth periodStart = new YearMonth(this);
        return getUnixTime() - periodStart.getUnixTime();
    }
    
    @Override
    public byte getWeekOfMonth() {
        return getWeekOfMonth(//
                    NumberUtil.safeIntToShort(getDT().getYear()),//
                    NumberUtil.safeIntToByte(getDT().getMonthOfYear()),//
                    NumberUtil.safeIntToByte(getDT().getDayOfMonth())//
                );
    }

    @Override
    public void setWeekOfMonth(byte weekOfMonth) {
        setDT(getDT().withDayOfMonth(//
                getFirstDayOfWeekOfMonth(//
                    NumberUtil.safeIntToShort(getDT().getYear()),//
                    NumberUtil.safeIntToByte(getDT().getMonthOfYear()),//
                    weekOfMonth//
                )//
            ));
    }

    @Override
    public void addWeeks(long weeks) {
        long days = weeks*7;
        setDT(getDT().plusDays(NumberUtil.safeLongToInt(days)));
        /*NumberUtil.foldLongToInt(days, new NumberUtil.Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value!=null) setDT(getDT().plusDays(value));
            }
        });*/
    }

    @Override
    public YearMonthWeek shallowCopy() {
        return new YearMonthWeek(this);
    }

    @Override
    public String toString() {
        return String.format("%1$d-%2$02d-W%3$d", getYear(), getMonthOfYear(), getWeekOfMonth());
    }

    /*public static void main(String [ ] args) {
        YearMonthWeek ymw1 = new YearMonthWeek((short)2014, (byte)5, (byte)3, "Europe/Paris");
        System.out.println("W3 of 2014/5 starts on 19: " + ymw1 + ", first day of that week: " + ymw1.getFirstDayOfWeekOfMonth(ymw1.getYear(), ymw1.getMonthOfYear(), ymw1.getWeekOfMonth()));
        YearMonthWeek ymw3 = new YearMonthWeek((short)2014, (byte)12, (byte)1, "Europe/Paris");
        System.out.println("W1 of 2014/12 starts on 1: " + ymw3 + ", first day of that week: " + ymw3.getFirstDayOfWeekOfMonth(ymw3.getYear(), ymw3.getMonthOfYear(), ymw3.getWeekOfMonth()));
        YearMonthWeek ymw2 = new YearMonthWeek((short)2014, (byte)8, (byte)4, "Europe/Paris");
        System.out.println("W4 of 2014/08 starts on 18: " + ymw2 + ", first day of that week: " + ymw2.getFirstDayOfWeekOfMonth(ymw1.getYear(), ymw2.getMonthOfYear(), ymw2.getWeekOfMonth()));
        YearMonthWeek ymw4 = new YearMonthWeek((short)2014, (byte)2, (byte)1, "Europe/Paris");
        System.out.println("W1 of 2014/2 starts on 3: " + ymw4 + ", first day of that week: " + ymw4.getFirstDayOfWeekOfMonth(ymw1.getYear(), ymw4.getMonthOfYear(), ymw4.getWeekOfMonth()));
    }*/
    
}
