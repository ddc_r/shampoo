/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import org.joda.time.DateTime;

/**
 *
 * @author okay_awright
 *
 */
public class YearMonthWeekDay extends YearMonthWeek implements YearMonthWeekDayInterface {

    protected YearMonthWeekDay() {
        //Do not directly use
    }

    public YearMonthWeekDay(long unixTime, String timezone) {
        super(unixTime, timezone);
    }

    public YearMonthWeekDay(YearMonthWeekInterface yearMonthWeekWithTimeZone) {
        super(yearMonthWeekWithTimeZone);
    }

    public YearMonthWeekDay(YearMonthInterface yearMonthWithTimeZone) {
        super(yearMonthWithTimeZone);
    }

    public YearMonthWeekDay(YearInterface yearWithTimeZone) {
        super(yearWithTimeZone);
    }

    public YearMonthWeekDay(YearMonthWeekDayInterface yearMonthDayWithTimeZone) {
        this(
                yearMonthDayWithTimeZone != null ? yearMonthDayWithTimeZone.getYear() : (short) 1970,
                yearMonthDayWithTimeZone != null ? yearMonthDayWithTimeZone.getMonthOfYear() : (byte) 1,
                yearMonthDayWithTimeZone != null ? yearMonthDayWithTimeZone.getDayOfMonth() : (byte) 1,
                yearMonthDayWithTimeZone != null ? yearMonthDayWithTimeZone.getTimeZone() : null);
    }

    public YearMonthWeekDay(short year, byte month, byte dayOfMonth, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearDayOfMonth(year, month, dayOfMonth);
    }

    public YearMonthWeekDay(short year, byte month, byte weekOfMonth, byte dayOfWeek, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearWeekOfMonthDayOfWeek(year, month, weekOfMonth, dayOfWeek);
    }

    public void setYearMonthOfYearDayOfMonth(short year, byte monthOfYear, byte dayOfMonth) {
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth));
    }

    public void setYearMonthOfYearWeekOfMonthDayOfWeek(short year, byte monthOfYear, byte weekOfMonth, byte dayOfWeek) {
        setYearMonthOfYearWeekOfMonth(year, monthOfYear, weekOfMonth);
        setDT(getDT().withDayOfWeek(dayOfWeek));
    }

    @Override
    public void setUnixTime(long unixTime) {
        final DateTime _dt = new DateTime(unixTime, getTZ());
        setYearMonthOfYearDayOfMonth(NumberUtil.safeIntToShort(_dt.getYear()), NumberUtil.safeIntToByte(_dt.getMonthOfYear()), NumberUtil.safeIntToByte(_dt.getDayOfMonth()));
    }

    @Override
    public long getMillisecondInWeekPeriod() {
        final YearMonthWeek periodStart = new YearMonthWeek(this);
        final long _u = getUnixTime();
        final long _v = periodStart.getUnixTime();
        return _u - _v;
    }

    @Override
    public byte getDayOfWeek() {
        return NumberUtil.safeIntToByte(getDT().getDayOfWeek());
    }

    @Override
    public void setDayOfWeek(byte dayOfWeek) {
        setDT(getDT().withDayOfWeek(dayOfWeek));
    }

    @Override
    public byte getDayOfMonth() {
        return NumberUtil.safeIntToByte(getDT().getDayOfMonth());
    }

    @Override
    public void setDayOfMonth(byte dayOfMonth) {
        setDT(getDT().withDayOfMonth(dayOfMonth));
    }

    @Override
    public void addDays(long days) {
        setDT(getDT().plusDays(NumberUtil.safeLongToInt(days)));
        /*NumberUtil.foldLongToInt(days, new NumberUtil.Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value != null) {
                    setDT(getDT().plusDays(value));
                }
            }
        });*/
    }

    public static Long diffDays(YearMonthWeekDay startDate, YearMonthWeekDay endDate) {
        if (startDate != null && endDate != null) {
            long endCorrectedTimestamp = endDate.getUnixTime() + endDate.getTimeZoneOffsetFromUTC();
            long startCorrectedTimestamp = startDate.getUnixTime() + startDate.getTimeZoneOffsetFromUTC();

            return (endCorrectedTimestamp - startCorrectedTimestamp) / 86400000; /* (1000 * 60 * 60 * 24) **/

        }
        return null;
    }

    @Override
    public long getTimeZoneOffsetFromUTC() {
        return getTZ().getOffset(getDT().getMillis());
    }

    @Override
    public YearMonthWeekDay switchTimeZone(String newTimezone) {
        if (newTimezone != null) {
            //Cheap cloning
            if (!newTimezone.equals(getTimeZone())) {
                return new YearMonthWeekDay(
                        getUnixTime(),
                        newTimezone);
            }
        }
        //Do nothing
        return new YearMonthWeekDayHourMinute(this);
    }

    @Override
    public String toSQLDate() {
        //Use this to get rid of any timezone or DST offsets and the overall severly brain-damaged Date() class
        String yearString = MarshallUtil.pad(getYear(), 4);
        String monthString = MarshallUtil.pad(getMonthOfYear(), 2);
        String dayString = MarshallUtil.pad(getDayOfMonth(), 2);

        return yearString + "-" + monthString + "-" + dayString;
    }

    @Override
    public YearMonthWeekDay shallowCopy() {
        return new YearMonthWeekDay(this);
    }

    @Override
    public String toString() {
        return String.format("%1$d-%2$02d-%3$02d", getYear(), getMonthOfYear(), getDayOfMonth());
    }

}
