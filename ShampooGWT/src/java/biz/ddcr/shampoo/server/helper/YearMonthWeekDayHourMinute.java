/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import org.joda.time.DateTime;

/**
 *
 * @author okay_awright
 *
 */
public class YearMonthWeekDayHourMinute extends YearMonthWeekDay implements YearMonthWeekDayHourMinuteInterface {

    protected YearMonthWeekDayHourMinute() {
        //Do not directly use
    }

    public YearMonthWeekDayHourMinute(long unixTime, String timezone) {
        super(unixTime, timezone);
    }

    public YearMonthWeekDayHourMinute(YearMonthWeekDayInterface yearMonthWeekDayWithTimeZone) {
        super(yearMonthWeekDayWithTimeZone);
    }

    public YearMonthWeekDayHourMinute(YearMonthWeekInterface yearMonthWeekWithTimeZone) {
        super(yearMonthWeekWithTimeZone);
    }

    public YearMonthWeekDayHourMinute(YearMonthInterface yearMonthWithTimeZone) {
        super(yearMonthWithTimeZone);
    }

    public YearMonthWeekDayHourMinute(YearInterface yearWithTimeZone) {
        super(yearWithTimeZone);
    }

    public YearMonthWeekDayHourMinute(YearMonthWeekDayHourMinuteInterface yearMonthDayHourMinuteWithTimeZone) {
        this(
                yearMonthDayHourMinuteWithTimeZone != null ? yearMonthDayHourMinuteWithTimeZone.getYear() : (short) 1970,
                yearMonthDayHourMinuteWithTimeZone != null ? yearMonthDayHourMinuteWithTimeZone.getMonthOfYear() : (byte) 1,
                yearMonthDayHourMinuteWithTimeZone != null ? yearMonthDayHourMinuteWithTimeZone.getDayOfMonth() : (byte) 1,
                yearMonthDayHourMinuteWithTimeZone != null ? yearMonthDayHourMinuteWithTimeZone.getHourOfDay() : (byte) 0,
                yearMonthDayHourMinuteWithTimeZone != null ? yearMonthDayHourMinuteWithTimeZone.getMinuteOfHour() : (byte) 0,
                yearMonthDayHourMinuteWithTimeZone != null ? yearMonthDayHourMinuteWithTimeZone.getTimeZone() : null);
    }

    public YearMonthWeekDayHourMinute(short year, byte month, byte dayOfMonth, byte hour, byte minute, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearDayOfMonthHourOfDayMinuteOfHour(year, month, dayOfMonth, hour, minute);
    }

    public YearMonthWeekDayHourMinute(short year, byte month, byte dayOfMonth, short minutesOfDay, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearDayOfMonthMinuteOfDay(year, month, dayOfMonth, minutesOfDay);
    }

    public void setYearMonthOfYearDayOfMonthHourOfDayMinuteOfHour(short year, byte monthOfYear, byte dayOfMonth, byte hourOfDay, byte minuteOfHour) {
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth).withHourOfDay(hourOfDay).withMinuteOfHour(minuteOfHour));
    }

    public void setYearMonthOfYearDayOfMonthMinuteOfDay(short year, byte monthOfYear, byte dayOfMonth, short minutesOfDay) {
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth).withMillisOfDay(minutesOfDay * 60 * 1000));
    }

    @Override
    public void setUnixTime(long unixTime) {
        final DateTime _dt = new DateTime(unixTime, getTZ());
        setYearMonthOfYearDayOfMonthHourOfDayMinuteOfHour(NumberUtil.safeIntToShort(_dt.getYear()), NumberUtil.safeIntToByte(_dt.getMonthOfYear()), NumberUtil.safeIntToByte(_dt.getDayOfMonth()), NumberUtil.safeIntToByte(_dt.getHourOfDay()), NumberUtil.safeIntToByte(_dt.getMinuteOfHour()));
    }

    @Override
    public long getMillisecondInDayPeriod() {
        YearMonthWeekDay periodStart = new YearMonthWeekDay(this);
        return getUnixTime() - periodStart.getUnixTime();
    }

    @Override
    public byte getHourOfDay() {
        return NumberUtil.safeIntToByte(getDT().getHourOfDay());
    }

    @Override
    public void setHourOfDay(byte hour) {
        setDT(getDT().withHourOfDay(hour));
    }

    @Override
    public byte getMinuteOfHour() {
        return NumberUtil.safeIntToByte(getDT().getMinuteOfHour());
    }

    @Override
    public void setMinuteOfHour(byte minute) {
        setDT(getDT().withMinuteOfHour(minute));
    }

    @Override
    public void addHours(long hours) {
        setDT(getDT().plusHours(NumberUtil.safeLongToInt(hours)));
        /*NumberUtil.foldLongToInt(hours, new NumberUtil.Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value != null) {
                    setDT(getDT().plusHours(value));
                }
            }
        });*/
    }

    @Override
    public void addMinutes(long minutes) {
        setDT(getDT().plusMinutes(NumberUtil.safeLongToInt(minutes)));
        /*NumberUtil.foldLongToInt(minutes, new NumberUtil.Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value != null) {
                    setDT(getDT().plusMinutes(value));
                }
            }
        });*/
    }

    @Override
    public short getMinuteFromStartOfDay() {
        return NumberUtil.safeIntToShort((getHourOfDay() * 60) + getMinuteOfHour());
    }

    /**
     * Uses float, hence the result is approximate, but it should be sufficient
     *
     * @param minuteOfDay
     */
    public void setMinuteFromStartOfDay(short minuteOfDay) {
        setHourOfDay(
                NumberUtil.safeLongToByte((long) ((minuteOfDay > -1 && minuteOfDay < (60 * 24)) ? Math.floor(minuteOfDay / 60.0) : 0)));
        setMinuteOfHour(
                NumberUtil.safeLongToByte((long) ((minuteOfDay > -1 && minuteOfDay < (60 * 24)) ? minuteOfDay % 60 : 0)));
    }

    @Override
    public YearMonthWeekDayHourMinute switchTimeZone(String newTimezone) {
        if (newTimezone != null) {
            //Cheap cloning
            if (!newTimezone.equals(getTimeZone())) {
                return new YearMonthWeekDayHourMinute(
                        getUnixTime(),
                        newTimezone);
            }
        }
        //Do nothing
        return new YearMonthWeekDayHourMinute(this);
    }

    public static Long diffMinutes(YearMonthWeekDayHourMinuteInterface startDate, YearMonthWeekDayHourMinuteInterface endDate) {
        if (startDate != null && endDate != null) {

            long endCorrectedTimestamp = endDate.getUnixTime() + endDate.getTimeZoneOffsetFromUTC();
            long startCorrectedTimestamp = startDate.getUnixTime() + startDate.getTimeZoneOffsetFromUTC();

            return (endCorrectedTimestamp - startCorrectedTimestamp) / 1000 / 60;
        }
        return null;
    }

    @Override
    public YearMonthWeekDayHourMinute shallowCopy() {
        return new YearMonthWeekDayHourMinute(this);
    }  

    @Override
    public String toString() {
        final long minutesOffsetToUTC = getTimeZoneOffsetFromUTC()/1000/60;
        final String signPartOfTimeZoneShift = minutesOffsetToUTC<0?"-":"+";
        final long hourPartOfTimeZoneShift = minutesOffsetToUTC / 60;
        final long minutePartOfTimeZoneShift = minutesOffsetToUTC % 60;
        return String.format("%1$d-%2$02d-%3$02dT%4$02d:%5$02d%6$s%7$02d:%8$02d", getYear(), getMonthOfYear(), getDayOfMonth(), getHourOfDay(), getMinuteOfHour(), signPartOfTimeZoneShift, hourPartOfTimeZoneShift, minutePartOfTimeZoneShift);
    }

    @Override
    public String getFriendlyEnglishCaption() {
        return toString();
    }

    /*public static void main(String [ ] args) {
        System.out.println(new YearMonthWeekDayHourMinute(System.currentTimeMillis(), "Europe/Paris"));       
    }*/
    
}
