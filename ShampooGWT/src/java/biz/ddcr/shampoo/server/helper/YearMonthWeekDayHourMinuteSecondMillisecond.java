/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper;

import biz.ddcr.shampoo.server.helper.NumberUtil.Fold;
import org.joda.time.DateTime;

/**
 *
 * @author okay_awright
 *
 */
public class YearMonthWeekDayHourMinuteSecondMillisecond extends YearMonthWeekDayHourMinute implements YearMonthWeekDayHourMinuteSecondMillisecondInterface {

    protected YearMonthWeekDayHourMinuteSecondMillisecond() {
        //Do not directly use
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(long unixTime, String timezone) {
        super(unixTime, timezone);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(YearMonthWeekDayHourMinuteInterface yearMonthWeekDayHourMinuteWithTimeZone) {
        super(yearMonthWeekDayHourMinuteWithTimeZone);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(YearMonthWeekDayInterface yearMonthWeekDayWithTimeZone) {
        super(yearMonthWeekDayWithTimeZone);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(YearMonthWeekInterface yearMonthWeekWithTimeZone) {
        super(yearMonthWeekWithTimeZone);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(YearMonthInterface yearMonthWithTimeZone) {
        super(yearMonthWithTimeZone);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(YearInterface yearWithTimeZone) {
        super(yearWithTimeZone);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(YearMonthWeekDayHourMinuteSecondMillisecondInterface yearMonthDayHourMinuteSecondMillisecondWithTimeZone) {
        this(
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getYear() : (short) 1970,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getMonthOfYear() : (byte) 1,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getDayOfMonth() : (byte) 1,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getHourOfDay() : (byte) 0,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getMinuteOfHour() : (byte) 0,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getSecondOfMinute() : (byte) 0,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getMillisecondOfSecond() : (short) 0,
                yearMonthDayHourMinuteSecondMillisecondWithTimeZone != null ? yearMonthDayHourMinuteSecondMillisecondWithTimeZone.getTimeZone() : null);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(short year, byte month, byte dayOfMonth, byte hour, byte minute, byte second, short millisecond, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearDayOfMonthHourOfDayMinuteOfHourSecondOfMinuteMillisecondOfSecond(year, month, dayOfMonth, hour, minute, second, millisecond);
    }

    public YearMonthWeekDayHourMinuteSecondMillisecond(short year, byte month, byte dayOfMonth, int millisecondsOfDay, String timezone) {
        setTZ(timezone);
        setYearMonthOfYearDayOfMonthMillisecondOfDay(year, month, dayOfMonth, millisecondsOfDay);
    }

    public void setYearMonthOfYearDayOfMonthHourOfDayMinuteOfHourSecondOfMinuteMillisecondOfSecond(short year, byte monthOfYear, byte dayOfMonth, byte hourOfDay, byte minuteOfHour, byte secondOfMinute, short millisecondOfSecond) {
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth).withHourOfDay(hourOfDay).withMinuteOfHour(minuteOfHour).withSecondOfMinute(secondOfMinute).withMillisOfSecond(millisecondOfSecond));
    }

    public void setYearMonthOfYearDayOfMonthMillisecondOfDay(short year, byte monthOfYear, byte dayOfMonth, int millisecondsOfDay) {
        setDT(getDT().withYear(year).withMonthOfYear(monthOfYear).withDayOfMonth(dayOfMonth).withMillisOfDay(millisecondsOfDay));
    }

    @Override
    public void setUnixTime(long unixTime) {
        setDT(new DateTime(unixTime, getTZ()));
    }

    @Override
    public byte getSecondOfMinute() {
        return NumberUtil.safeIntToByte(getDT().getSecondOfMinute());
    }

    @Override
    public short getMillisecondOfSecond() {
        return NumberUtil.safeIntToShort(getDT().getMillisOfSecond());
    }

    @Override
    public void setSecondOfMinute(byte second) {
        setDT(getDT().withSecondOfMinute(second));
    }

    @Override
    public void setMillisecondOfSecond(short millisecond) {
        setDT(getDT().withMillisOfSecond(millisecond));
    }

    @Override
    public void addSeconds(long seconds) {        
        setDT(getDT().plusSeconds(NumberUtil.safeLongToInt(seconds)));
        /*NumberUtil.foldLongToInt(seconds, new Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value != null) {
                    setDT(getDT().plusSeconds(value));
                }
            }
        });*/
    }

    @Override
    public void addMilliseconds(long milliseconds) {
        setDT(getDT().plusMillis(NumberUtil.safeLongToInt(milliseconds)));
        /*NumberUtil.foldLongToInt(milliseconds, new Fold<Integer>() {
            @Override
            public void each(Integer value) {
                if (value != null) {
                    setDT(getDT().plusMillis(value));
                }
            }
        });*/
    }

    @Override
    public int getMillisecondFromStartOfDay() {
        return (((((getHourOfDay() * 60) + getMinuteOfHour()) * 60) + getSecondOfMinute()) * 1000) + getMillisecondOfSecond();
    }

    public void setMillisecondFromStartOfDay(int millisecondOfDay) {
        //Is the given millisecond properly set within a day long period?
        if ((millisecondOfDay > -1 && millisecondOfDay < (1000 * 60 * 60 * 24))) {

            int rest = millisecondOfDay;

            byte hourOfDay = (byte) (Math.floor(rest / (1000.0 * 60.0 * 60.0)));
            setHourOfDay(
                    hourOfDay
            );
            int hourOfDayInMs = hourOfDay * (1000 * 60 * 60);

            rest -= hourOfDayInMs;
            byte minuteOfHour = (byte) (Math.floor(rest / (1000.0 * 60.0)));
            setMinuteOfHour(
                    minuteOfHour
            );
            int minuteOfHourInMs = minuteOfHour * (1000 * 60);

            rest -= minuteOfHourInMs;
            byte secondOfMinute = (byte) (Math.floor(rest / 1000.0));
            setSecondOfMinute(
                    secondOfMinute
            );
            int secondOfMinuteInMs = secondOfMinute * 1000;

            rest -= secondOfMinuteInMs;
            setMillisecondOfSecond(
                    (short) rest
            );
        } else {
            setHourOfDay((byte) 0);
            setMinuteOfHour((byte) 0);
            setSecondOfMinute((byte) 0);
            setMillisecondOfSecond((short) 0);
        }
    }

    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecond switchTimeZone(String newTimezone) {
        //Cheap cloning
        if (!newTimezone.equals(getTimeZone())) {
            return new YearMonthWeekDayHourMinuteSecondMillisecond(
                    getUnixTime(),
                    newTimezone);
        }
        //Do nothing
        return new YearMonthWeekDayHourMinuteSecondMillisecond(this);
    }

    public static Long diffMilliseconds(YearMonthWeekDayHourMinuteSecondMillisecondInterface startDate, YearMonthWeekDayHourMinuteSecondMillisecondInterface endDate) {
        if (startDate != null && endDate != null) {
            long endCorrectedTimestamp = endDate.getUnixTime() + endDate.getTimeZoneOffsetFromUTC();
            long startCorrectedTimestamp = startDate.getUnixTime() + startDate.getTimeZoneOffsetFromUTC();

            return (endCorrectedTimestamp - startCorrectedTimestamp);
        }
        return null;
    }

    @Override
    public YearMonthWeekDayHourMinuteSecondMillisecond shallowCopy() {
        return new YearMonthWeekDayHourMinuteSecondMillisecond(this);
    }

    @Override
    public String toString() {
        final long minutesOffsetToUTC = getTimeZoneOffsetFromUTC() / 1000 / 60;
        final String signPartOfTimeZoneShift = minutesOffsetToUTC < 0 ? "-" : "+";
        final long hourPartOfTimeZoneShift = minutesOffsetToUTC / 60;
        final long minutePartOfTimeZoneShift = minutesOffsetToUTC % 60;
        return String.format("%1$d-%2$02d-%3$02dT%4$02d:%5$02d:%6$02d.%7$03d%8$s%9$02d:%10$02d", getYear(), getMonthOfYear(), getDayOfMonth(), getHourOfDay(), getMinuteOfHour(), getSecondOfMinute(), getMillisecondOfSecond(), signPartOfTimeZoneShift, hourPartOfTimeZoneShift, minutePartOfTimeZoneShift);
    }

    @Override
    public String getFriendlyEnglishCaption() {
        return toString();
    }

    @Override
    public String toSQLDate() {
        String ymd = super.toSQLDate();

        //Use this to get rid of any timezone or DST offsets and the overall severly brain-damaged Date() class
        String hourString = MarshallUtil.pad(getHourOfDay(), 2);
        String minuteString = MarshallUtil.pad(getMinuteOfHour(), 2);
        String secondString = MarshallUtil.pad(getSecondOfMinute(), 2);
        String millisecondString = MarshallUtil.pad(getMillisecondOfSecond(), 2);

        return ymd + " " + hourString + ":" + minuteString + ":" + secondString + "." + millisecondString;

    }

    /*public static void main(String[] args) {
        System.out.println(new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), "Europe/Paris"));
    }*/

}
