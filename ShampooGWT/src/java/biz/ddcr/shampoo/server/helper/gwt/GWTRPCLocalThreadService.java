/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.helper.gwt;

import biz.ddcr.shampoo.server.helper.ServletUtils2;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class GWTRPCLocalThreadService implements Serializable {

    public HttpServletRequest getRequest() {
        return ServletUtils2.getRequest();
    }

    public HttpServletResponse getResponse() {
        return ServletUtils2.getResponse();
    }

    public void setRequest(HttpServletRequest request) {
        ServletUtils2.setRequest(request);
    }

    public void setResponse(HttpServletResponse response) {
        ServletUtils2.setResponse(response);
    }

}
