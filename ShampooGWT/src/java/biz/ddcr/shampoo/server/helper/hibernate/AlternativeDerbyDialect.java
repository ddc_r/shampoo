/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.helper.hibernate;

import org.hibernate.dialect.DerbyDialect;

/**
 *
 * query.setMaxResults() should specify a size, not an index
 * I don't know to which versions of Derby it actually applies for, it's at least broken in 1.8.10
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class AlternativeDerbyDialect extends DerbyDialect {

    protected boolean hasForUpdateClause(int forUpdateIndex) {
        return forUpdateIndex >= 0;
    }

    protected boolean hasWithClause(String normalizedSelect) {
        return normalizedSelect.startsWith("with ", normalizedSelect.length() - 7);
    }

    protected int getWithIndex(String querySelect) {
        int i = querySelect.lastIndexOf("with ");
        if (i < 0) {
            i = querySelect.lastIndexOf("WITH ");
        }
        return i;
    }

    /**
     * limit should be a size, not an index
     * @param query
     * @param offset
     * @param limit
     * @return
     */
    @Override
    public String getLimitString(String query, final int offset, final int limit) {
        StringBuilder sb = new StringBuilder(query.length() + 50);

        final String normalizedSelect = query.toLowerCase().trim();
        final int forUpdateIndex = normalizedSelect.lastIndexOf("for update");

        if (hasForUpdateClause(forUpdateIndex)) {
            sb.append(query.substring(0, forUpdateIndex - 1));
        } else if (hasWithClause(normalizedSelect)) {
            sb.append(query.substring(0, getWithIndex(query) - 1));
        } else {
            sb.append(query);
        }

        if (offset == 0) {
            sb.append(" fetch first ");
        } else {
            sb.append(" offset ").append(offset).append(" rows fetch next ");
        }

        sb.append(Math.max(1, limit-offset)).append(" rows only");

        if (hasForUpdateClause(forUpdateIndex)) {
            sb.append(' ');
            sb.append(query.substring(forUpdateIndex));
        } else if (hasWithClause(normalizedSelect)) {
            sb.append(' ').append(query.substring(getWithIndex(query)));
        }
        return sb.toString();
    }
}
