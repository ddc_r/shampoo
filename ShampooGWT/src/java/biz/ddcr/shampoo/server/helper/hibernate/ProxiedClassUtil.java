/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.helper.hibernate;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.Hibernate;
import org.hibernate.LazyInitializationException;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

/**
 *
 * Directly using instanceof with proxied Hibernate objects is usually useless.
 * This class is used to retrieve the true class of an object even if proxied by CGLIB or Javassist
 *
 * @author okay_awright
 **/
public class ProxiedClassUtil {

    private ProxiedClassUtil() {
        //Cheap singleton
    }

    /**
     * @deprecated
     * Workaround for Hibernate lazy-loading with objects instantiated outside a DAO
     * Should only be used within LoginManager for objects pulled from an HTTP session
     * Or just before you need to cast a proxy
     *
     * Hydrate the specified entity and all its attached entities (beware of cyclic dead-ends)
     *
     * @param user
     **/
    public static void hydrateRecursive(Object objectToHydrate) throws ClassCastException, InvocationTargetException {
        deepHydration(objectToHydrate);
    }

    /**
     *
     * Workaround for Hibernate lazy-loading with objects instantiated outside a DAO
     * Should only be used within LoginManager for objects pulled from an HTTP session
     * Or just before you need to cast a proxy
     *
     * Hydrate only the specified entity, not its dependencies
     *
     * This entity must be linked to the current Hibernate session first
     *
     * @param user
     **/
    public static <T> T hydrate(T objectToHydrate) {
        if (objectToHydrate!=null && !Hibernate.isInitialized(objectToHydrate)) {
            Hibernate.initialize(objectToHydrate);
        }
        return objectToHydrate;
    }

    /**
     * @deprecated
     * Workaround for Hibernate lazy-loading with objects instantiated outside a DAO
     * Should only be used within LoginManager for objects pulled from an HTTP session
     * Or just before you need to cast a proxy
     *
     * Hydrate the specified entity and all its attached entities (beware of cyclic dead-ends)
     *
     * This entity must be linked to the current Hibernate session first
     *
     * @param user
     **/
    public static void hydrateRecursive(Collection objectsToHydrate) throws ClassCastException, InvocationTargetException {
        deepHydration(objectsToHydrate);
    }

    /**
     *
     * Workaround for Hibernate lazy-loading with objects instantiated outside a DAO
     * Should only be used within LoginManager for objects pulled from an HTTP session
     * Or just before you need to cast a proxy
     *
     * Hydrate only the specified entity, not its dependencies
     *
     * This entity must be linked to the current Hibernate session first
     *
     * @param user
     **/
    public static <T> Collection<? extends T> hydrate(Collection<? extends T> objectsToHydrate) {
        if (objectsToHydrate!=null)
            for (T objectToHydrate : objectsToHydrate) {
                hydrate(objectToHydrate);
            }
        return objectsToHydrate;
    }

    /**
     * @deprecated
     **/
    static Object deepHydration(final Object maybeProxy) throws ClassCastException, InvocationTargetException {
        if (maybeProxy == null) {
            return null;
        }
        return deepHydration(maybeProxy, new HashSet<Object>());
    }

    /**
     * @deprecated
     **/
    private static Object deepHydration(final Object maybeProxy, final HashSet<Object> visited) throws ClassCastException, InvocationTargetException {
        if (maybeProxy == null) {
            return null;
        }
        Class clazz;
        Hibernate.initialize(maybeProxy);
        if (maybeProxy instanceof HibernateProxy) {
            HibernateProxy proxy = (HibernateProxy) maybeProxy;
            LazyInitializer li = proxy.getHibernateLazyInitializer();
            clazz = li.getImplementation().getClass();
        } else {
            clazz = maybeProxy.getClass();
        }
        Object ret = deepHydration(maybeProxy, clazz);
        if (visited.contains(ret)) {
            return ret;
        }
        visited.add(ret);
        for (PropertyDescriptor property : PropertyUtils.getPropertyDescriptors(ret)) {
            try {
                String name = property.getName();
                if (!"owner".equals(name) && property.getWriteMethod() != null) {
                    Object value = PropertyUtils.getProperty(ret, name);
                    boolean needToSetProperty = false;
                    if (value instanceof HibernateProxy) {
                        value = deepHydration(value, visited);
                        needToSetProperty = true;
                    }
                    if (value instanceof Object[]) {
                        Object[] valueArray = (Object[]) value;
                        Object[] result = (Object[]) Array.newInstance(value.getClass(), valueArray.length);
                        for (int i = 0; i < valueArray.length; i++) {
                            result[i] = deepHydration(valueArray[i], visited);
                        }
                        value = result;
                        needToSetProperty = true;
                    }
                    if (value instanceof Set) {
                        Set valueSet = (Set) value;
                        Set result = new HashSet();
                        for (Object o : valueSet) {
                            result.add(deepHydration(o, visited));
                        }
                        value = result;
                        needToSetProperty = true;
                    }
                    if (value instanceof Map) {
                        Map valueMap = (Map) value;
                        Map result = new HashMap();
                        for (Object o : valueMap.keySet()) {
                            //TODO; use entrySet() isntead
                            result.put(deepHydration(o, visited), deepHydration(valueMap.get(o), visited));
                        }
                        value = result;
                        needToSetProperty = true;
                    }
                    if (value instanceof List) {
                        List valueList = (List) value;
                        List result = new ArrayList(valueList.size());
                        for (Object o : valueList) {
                            result.add(deepHydration(o, visited));
                        }
                        value = result;
                        needToSetProperty = true;
                    }
                    if (needToSetProperty) {
                        PropertyUtils.setProperty(ret, name, value);
                    }
                }
            } catch (java.lang.IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * @deprecated
     **/
    private static <T> T deepHydration(Object maybeProxy, Class<T> baseClass) throws ClassCastException {
        if (maybeProxy == null) {
            return null;
        }
        if (maybeProxy instanceof HibernateProxy) {
            return baseClass.cast(((HibernateProxy) maybeProxy).getHibernateLazyInitializer().getImplementation());
        } else {
            return baseClass.cast(maybeProxy);
        }
    }

    /**
     * Hibernate specific class finder
     * @param o
     * @return
     */
    public static Class getProxiedClass(Object o) {
        try {
            return Hibernate.getClass(o);
        } catch (LazyInitializationException e) {
            return o.getClass();
        }
    }

    /**
     *
     * This is NOT a real instanceof equivalent, it returns true iff the object can be cast into the specified class
     * Can be replaced by a Visitor Pattern-type check if I feel like I could spare some time restructuring the app: I do like instanceof
     *
     * @param o
     * @param c
     * @return
     **/
    public static boolean castableAs(Object o, Class c) {
        return c.isAssignableFrom(getProxiedClass(o));
    }

    /**
     * instanceof equivalent for Hibernate proxies
     * Can be replaced by a Visitor Pattern-type check if I feel like I could spare some time restructuring the app: I do like instanceof
     **/
    public static boolean isSameClass(Object o1, Object o2) {
        return getProxiedClass(o1).equals(getProxiedClass(o2));
    }

    /**
    * Utility method that tries to properly initialize the Hibernate CGLIB
    * proxy.
    *
    * @param <T>
    * @param maybeProxy -- the possible Hibernate generated proxy
    * @param baseClass -- the resulting class to be cast to.
    * @return the object of a class <T>
    * @throws ClassCastException
    */
   public static <T> T cast(Object maybeProxy, Class<T> baseClass) throws ClassCastException {
      if (maybeProxy instanceof HibernateProxy) {
         return baseClass.cast(((HibernateProxy) maybeProxy).getHibernateLazyInitializer().getImplementation());
      }
      return baseClass.cast(maybeProxy);
   }
}
