/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.datastore;

import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.FileInfoInterface;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.io.IOException;
import java.util.Collection;

/**
 *
 * A datastore where tracks and cover art pictures are physically stored.
 * There's only one single functional datastore per running instance of the application.
 * The frontend may transfer files to and from the datastore using FTP, HTTP or even through local filesystem I/O, depending on the implementation
 *
 * @author okay_awright
 **/
public interface DataStoreInterface {

    /**
     * Callback interface when an item has been transfered from or to the datastore
     */
    public interface TransferCallbackInterface {
        /**
         * newContainer may point to an updated version of the file metadata if it was modified during the transfer, otherwise, if the header is unchanged, this event is not called
         *
         * @param updatedFileInfo 
         */
        public void onNewHeader(FileInfoInterface updatedFileInfo);
        /**
         * If the underlying business layer wraps the call to the method within an asynchronous thread the value can be used to keep track of the progress of the trasnfer
         * @param percentDone
         **/
        public void onProgress(byte percentDone);
        /**
         * If the underlying business layer wraps the call to the method within an asynchronous thread the value can be used to abort the transfer
         * @return  
         **/
        public boolean onCancellable();
    }

    /**
     * Store a file in the datastore from the TypedStreamInterface stream
     * The file, after the transfer, will be identified by the requestId parameter
     * The move flag specifies if the original stream must be deleted once the file has been fully copied
     * The actual tranfer must be synchronous, it's the responsibility of the underlying business layer to wrap it withint an asynchronous thread if required
     * If the canOverwrite flag is set to false and an existing file is about to be overwritten, an Exception is thrown
     * Returns a transferId that will be used by commit() or rollback()
     * @param requestedId 
     * @param file
     * @param callback 
     * @param canOverwrite 
     * @return
     * @throws Exception
     **/
    public Integer moveTypedStream(final String requestedId, final TypedStreamInterface file, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer moveTypedStream(final String requestedId, final TypedStreamInterface file, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception
     * @param requestedId
     * @param file
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer moveTypedStream(final String requestedId, final TypedStreamInterface file, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer moveTypedStream(final String requestedId, final TypedStreamInterface file, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception
     * @param requestedId
     * @param file
     * @param callback
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copyTypedStream(final String requestedId, final TypedStreamInterface file, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer moveTypedStream(final String requestedId, final TypedStreamInterface file, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception
     * @param requestedId
     * @param file
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copyTypedStream(final String requestedId, final TypedStreamInterface file, final boolean canOverwrite) throws Exception;

    /**
     * Fetch a file as a TypedStreamInterface from the datastore and identified using the requestId identifier
     * The transfer must be synchronous
     * @param requestedFormat 
     * @param requestedId
     * @return
     * @throws Exception
     **/
    public UntypedStreamInterface getRawStream(final FILE_FORMAT requestedFormat, final String requestedId) throws Exception;
    /**
     * See public UntypedStream getRawStream(final FILE_FORMAT requestedFormat, final String requestedId) throws Exception;
     * @param requestedId
     * @return
     * @throws Exception
     */
    public UntypedStreamInterface getRawAudioStream(final String requestedId) throws Exception;
    /**
     * See public UntypedStream getRawStream(final FILE_FORMAT requestedFormat, final String requestedId) throws Exception;
     * @param requestedId
     * @return
     * @throws Exception
     */
    public UntypedStreamInterface getRawCoverArtStream(final String requestedId) throws Exception;
    /**
     * See public UntypedStream getRawStream(final FILE_FORMAT requestedFormat, final String requestedId) throws Exception;
     * @param requestedId
     * @return
     * @throws Exception
     */
    public UntypedStreamInterface getRawTextStream(final String requestedId) throws Exception;

    /**
     * Returns a transferId that will be used by commit() or rollback()
     * The actual tranfer must be synchronous, it's the responsibility of the underlying business layer to wrap it within an asynchronous thread if required
     * @param requestedFormat 
     * @param requestedId
     * @param callback 
     * @return
     * @throws Exception
     **/
    public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId, final TransferCallbackInterface callback) throws Exception;
    /**
     * See public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId, final TransferCallbackInterface callback) throws Exception;
     * @param requestedFormat
     * @param requestedId
     * @return
     * @throws Exception
     */
    public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId) throws Exception;
    /**
     * See public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId, final TransferCallbackInterface callback) throws Exception;
     * @param requestedId
     * @param callback
     * @return
     * @throws Exception
     */
    public Integer removeAudio(final String requestedId, final TransferCallbackInterface callback) throws Exception;
    /**
     * See public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId, final TransferCallbackInterface callback) throws Exception;
     * @param requestedId
     * @return
     * @throws Exception
     */
    public Integer removeAudio(final String requestedId) throws Exception;
    /**
     * See public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId, final TransferCallbackInterface callback) throws Exception;
     * @param requestedId
     * @param callback
     * @return
     * @throws Exception
     */
    public Integer removePicture(final String requestedId, final TransferCallbackInterface callback) throws Exception;
    /**
     * See public Integer remove(final FILE_FORMAT requestedFormat, final String requestedId, final TransferCallbackInterface callback) throws Exception;
     * @param requestedId
     * @return
     * @throws Exception
     */
    public Integer removePicture(final String requestedId) throws Exception;

    /**
     * Copy or move a file internally, i.e. without downloading the file first and then reuploading it
     * Returns a transferId that will be used by commit() or rollback()
     * The actual tranfer must be synchronous, it's the responsibility of the underlying business layer to wrap it within an asynchronous thread if required
     *
     * @param requestedFormat 
     * @param canOverwrite 
     * @param destinationId 
     * @param sourceId 
     * @param callback 
     * @return 
     * @throws Exception 
     */
    public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param requestedFormat
     * @param sourceId
     * @param destinationId
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param callback
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer renameAudio(final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer renameAudio(final String sourceId, final String destinationId, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param callback
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer renamePicture(final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer renamePicture(final String sourceId, final String destinationId, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param requestedFormat
     * @param sourceId
     * @param destinationId
     * @param callback
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copy(final FILE_FORMAT requestedFormat,final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param requestedFormat
     * @param sourceId
     * @param destinationId
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copy(final FILE_FORMAT requestedFormat,final String sourceId, final String destinationId, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param callback
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copyAudio(final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copyAudio(final String sourceId, final String destinationId, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param callback
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copyPicture(final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
    /**
     * See public Integer rename(final FILE_FORMAT requestedFormat, final String sourceId, final String destinationId, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception;
     * @param sourceId
     * @param destinationId
     * @param canOverwrite
     * @return
     * @throws Exception
     */
    public Integer copyPicture(final String sourceId, final String destinationId, final boolean canOverwrite) throws Exception;

    /**
     * Make the actual changes if not previously rollbacked. This doesn't imply that the interface is XA-compliant, it's just an additional safe-guard against corruption.
     * A commit should not fail, nor should it throw an Exception
     * When multiple operations are to be processed within the same commit command, then deleting a file should not make any subsequent copy or move operation on the same file impossible
     **/
    public void commit();
    /**
     * See public void commit();
     * @param transferId
     */
    public void commit(Integer transferId);
    /**
     * See public void commit();
     * @param transferIds
     */
    public void commit(Collection<Integer> transferIds);
    /**
     * Withdraw the changes if not already committed. This doesn't imply that the interface is XA-compliant, it's just an additional safe-guard against corruption.
     * A rollback should not fail, nor should it throw an Exception
     * When multiple operations are to be processed within the same rollback command, then deleting a file should not make any subsequent copy or move operation on the same file impossible
     **/
    public void rollback();
    /**
     * See public void rollback();
     * @param transferId
     */
    public void rollback(Integer transferId);
    /**
     * See public void rollback();
     * @param transferIds
     */
    public void rollback(Collection<Integer> transferIds);

    /**
     * Ask whether the datastore allows direct access to a stream via a free-form URL to third-parties.
     * It's the responsibility of the datastore to make a stream secured and implemnt authentication if direct access is enabled
     * The third-parties are the GWT GUI and the public webservices
     * @param requestedFormat 
     * @param requestedId
     * @return
     */
    public boolean hasDirectSecuredAccessPublicURL(final FILE_FORMAT requestedFormat, final String requestedId);
    /**
     * See public boolean hasDirectSecuredAccessURL(final FILE_FORMAT requestedFormat, final String requestedId);
     * @param requestedId
     * @return
     */
    public boolean hasAudioDirectSecuredAccessPublicURL(final String requestedId);
    /**
     * See public boolean hasDirectSecuredAccessURL(final FILE_FORMAT requestedFormat, final String requestedId);
     * @param requestedId
     * @return
     */
    public boolean hasPictureDirectSecuredAccessPublicURL(final String requestedId);
    /**
     * Get a direct free-form URL to a datastore stream accessible to third-parties.
     * It's the responsibility of the datastore to make a stream secured and implemnt authentication if direct access is enabled
     * The third-parties are the GWT GUI and the public webservices, the protocol SHOULD be http
     * userId is the authenticated user login and md5_password the MD5 hash of his password
     * Throws an IOException if hasDirectSecuredAccessURL() returns false, i.e. there's no direct access to this stream or it's forbidden
     * @param channelId 
     * @param privateKey 
     * @param requestedId
     * @param requestedFormat 
     * @return
     * @throws IOException  
     */
    public String getDirectSecuredAccessPublicURL(final String userId, final String md5_password, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param requestedFormat
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getDirectSecuredAccessPublicURL(final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param channelId
     * @param privateKey
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getAudioDirectSecuredAccessPublicURL(final String userId, final String md5_password, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getAudioDirectSecuredAccessPublicURL(final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param channelId
     * @param privateKey
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getPictureDirectSecuredAccessPublicURL(final String userId, final String md5_password, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getPictureDirectSecuredAccessPublicURL(final String requestedId) throws IOException;

    /**
     * Ask whether the datastore allows direct access to a stream via a free-form URL to third-parties.
     * It's the responsibility of the datastore to make a stream secured and implemnt authentication if direct access is enabled
     * The third-parties are the private webservices (streamer)
     * @param requestedFormat 
     * @param requestedId
     * @return
     */
    public boolean hasDirectSecuredAccessPrivateURL(final FILE_FORMAT requestedFormat, final String requestedId);
    /**
     * See public boolean hasDirectSecuredAccessURL(final FILE_FORMAT requestedFormat, final String requestedId);
     * @param requestedId
     * @return
     */
    public boolean hasAudioDirectSecuredAccessPrivateURL(final String requestedId);
    /**
     * See public boolean hasDirectSecuredAccessURL(final FILE_FORMAT requestedFormat, final String requestedId);
     * @param requestedId
     * @return
     */
    public boolean hasPictureDirectSecuredAccessPrivateURL(final String requestedId);
    /**
     * Get a direct free-form URL to a datastore stream accessible to third-parties.
     * It's the responsibility of the datastore to make a stream secured and implemnt authentication if direct access is enabled
     * The third-parties are the private webservices (streamer), if Liquidsoap is the streamer then the protocol MUST be either http, ftp, smb, or a local file path (doesn't support File URI)
     * Throws an IOException if hasDirectSecuredAccessURL() returns false, i.e. there's no direct access to this stream or it's forbidden
     * @param channelId 
     * @param privateKey 
     * @param requestedId
     * @param requestedFormat 
     * @return
     * @throws IOException  
     */
    public String getDirectSecuredAccessPrivateURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param requestedFormat
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getDirectSecuredAccessPrivateURL(final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param channelId
     * @param privateKey
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getAudioDirectSecuredAccessPrivateURL(final String channelId, final String privateKey, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getAudioDirectSecuredAccessPrivateURL(final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param channelId
     * @param privateKey
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getPictureDirectSecuredAccessPrivateURL(final String channelId, final String privateKey, final String requestedId) throws IOException;
    /**
     * See public String getDirectSecuredAccessURL(final String channelId, final String privateKey, final FILE_FORMAT requestedFormat, final String requestedId) throws IOException;
     * @param requestedId
     * @return
     * @throws IOException
     */
    public String getPictureDirectSecuredAccessPrivateURL(final String requestedId) throws IOException;
    
}
