/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.datastore;

import biz.ddcr.shampoo.server.domain.archive.format.TextFormat.TEXT_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import biz.ddcr.shampoo.server.helper.Retry;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedFTPStream;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * DataStore located on a local or distant FTP server
 * Various restrictions apply compared to LocalFilsystemDataStore:
 * -Direct access to output streams is not possible
 * -Direct access to input streams may be terribly slow (doesn't close connections in a timely manner)
 * -No callbacks/listeners
 *
 * Note:
 *  The FTP backend must comply with any RFC after 939 (must support SIZE and MTDM)
 *
 * @author okay_awright
 **/
public class FTPDataStore extends QueueBasedStore implements DataStoreInterface {

    private class QueueMoveItem implements QueueItemInterface {

        private UntypedFTPStream source;
        private UntypedFTPStream destination;

        QueueMoveItem(UntypedFTPStream source, UntypedFTPStream destination) {
            this.source = source;
            this.destination = destination;
        }

        public UntypedFTPStream getDestination() {
            return destination;
        }

        public UntypedFTPStream getSource() {
            return source;
        }

        @Override
        public int compareTo(QueueItemInterface o) {
            //Move operations must be processed first, Remove opeartions last
            if (o instanceof QueueMoveItem) {
                //sort by destination, then by source
                int diff = this.destination == null || ((QueueMoveItem) o).destination == null ? 0 : this.destination.compareTo(((QueueMoveItem) o).destination);
                if (diff == 0) {
                    diff = this.source == null || ((QueueMoveItem) o).source == null ? 0 : this.source.compareTo(((QueueMoveItem) o).source);
                }
                return diff;
            } else if (o instanceof QueueRemoveItem) {
                return -1;
            } else {
                return 0;
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final QueueMoveItem other = (QueueMoveItem) obj;
            if ((this.destination == null) ? (other.destination != null) : !this.destination.equals(other.destination)) {
                return false;
            }
            if ((this.source == null) ? (other.source != null) : !this.source.equals(other.source)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 53 * hash + (this.destination != null ? this.destination.hashCode() : 0);
            hash = 53 * hash + (this.source != null ? this.source.hashCode() : 0);
            return hash;
        }

        @Override
        public void commit() {
            //Move over the temp file over any existing broadcastble file
            try {
                if (source == null || !source.isWipeable() || destination == null || !destination.isWritable()) {
                    throw new IOException("The stream to persist is either undefined or write-protected");
                }

                new Retry(retriesBeforeFailure) {
                    @Override
                    public boolean retry() throws Exception {
                        return !source.moveTo(destination);
                    }
                };

            } catch (Exception ex) {
                logger.error("Cannot commit", ex);
            }
        }

        @Override
        public void rollback() {
            //Remove the temp file
            try {
                if (source == null || !source.isWipeable()) {
                    throw new IOException("The stream to wipe out is either undefined or write-protected");
                }

                new Retry(retriesBeforeFailure) {
                    @Override
                    public boolean retry() throws Exception {
                        return !source.clean();
                    }
                };
                
            } catch (Exception ex) {
                logger.error("Cannot rollback", ex);
            }
        }
    }

    private class QueueRemoveItem implements QueueItemInterface {

        private UntypedStreamInterface stream;

        QueueRemoveItem(UntypedStreamInterface stream) {
            this.stream = stream;
        }

        public UntypedStreamInterface getStream() {
            return stream;
        }

        @Override
        public int compareTo(QueueItemInterface o) {
            //Move operations must be processed first, Remove opeartions last
            if (o == null) {
                return 0;
            }
            if (o instanceof QueueRemoveItem) {
                return this.stream == null || ((QueueRemoveItem) o).stream == null ? 0 : this.stream.compareTo(((QueueRemoveItem) o).stream);
            } else if (o instanceof QueueMoveItem) {
                return 1;
            } else {
                return 0;
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final QueueRemoveItem other = (QueueRemoveItem) obj;
            if ((this.stream == null) ? (other.stream != null) : !this.stream.equals(other.stream)) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 53 * hash + (this.stream != null ? this.stream.hashCode() : 0);
            return hash;
        }

        @Override
        public void commit() {
            try {
                if (stream == null || !stream.isWipeable()) {
                    throw new IOException("The stream to wipe out is either undefined or write-protected");
                }

                new Retry(retriesBeforeFailure) {
                    @Override
                    public boolean retry() throws Exception {
                        return !stream.clean();
                    }
                };

            } catch (Exception ex) {
                logger.error("Cannot commit", ex);
            }
        }

        @Override
        public void rollback() {
            //there's nothing to do actually, so don't do anything
        }
    }
    protected final Log logger = LogFactory.getLog(getClass());
    /**
     * The full URL(URI) to the FTP server, with protocol, credentials, host, port, and path
     **/
    private URL url;
    /**
     * Number of retries before declaring a problem happens
     * 0 means do not use this feature
     **/
    private int retriesBeforeFailure;
    /**
     * Whether the datastore should expose a File URI when a private webservice asks for an URL
     * BEWARE: The authorization fragment is left unspecified
     * @return 
     */
    private boolean privateFtpURI;

    protected URL getURI() {
        return url;
    }

    public String getUrl() {
        return url.toString();
    }

    public void setUrl(String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public boolean isPrivateFtpURI() {
        return privateFtpURI;
    }

    public void setPrivateFtpURI(boolean privateFtpURI) {
        this.privateFtpURI = privateFtpURI;
    }

    public int getNumberOfRetriesBeforeFailure() {
        if (retriesBeforeFailure < 0) {
            retriesBeforeFailure = 0;
        }
        return retriesBeforeFailure;
    }

    public void setNumberOfRetriesBeforeFailure(int retriesBeforeFailure) {
        this.retriesBeforeFailure = retriesBeforeFailure;
    }

    @Override
    public Integer copyTypedStream(String requestedId, TypedStreamInterface file, TransferCallbackInterface callback, boolean canOverwrite) throws Exception {
        if (file != null && file.getContainer() != null && file.getContainer().getFormat() != null && file.getRawStream() != null) {
            return copyTempFile(requestedId, file.getRawStream(), file.getContainer().getFormat().getFileNameSuffix(), false, callback, canOverwrite);
        }
        return null;
    }

    @Override
    public Integer copyTypedStream(String requestedId, TypedStreamInterface file, boolean canOverwrite) throws Exception {
        return copyTypedStream(requestedId, file, null, canOverwrite);
    }

    @Override
    public Integer moveTypedStream(String requestedId, TypedStreamInterface file, TransferCallbackInterface callback, boolean canOverwrite) throws Exception {
        if (file != null && file.getContainer() != null && file.getContainer().getFormat() != null && file.getRawStream() != null) {
            return copyTempFile(requestedId, file.getRawStream(), file.getContainer().getFormat().getFileNameSuffix(), true, callback, canOverwrite);
        }
        return null;
    }

    @Override
    public Integer moveTypedStream(String requestedId, TypedStreamInterface file, boolean canOverwrite) throws Exception {
        return moveTypedStream(requestedId, file, null, canOverwrite);
    }

    protected Integer copyTempFile(final String destinationId, final String sourceId, final String suffix, final boolean move, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        if (sourceId != null) {
            String sourceFileName = sourceId + suffix;
            URL sourceURL = new URL(MarshallUtil.removeTrailingSlash(getUrl()) + MarshallUtil.addStartingSlash(sourceFileName));
            return copyTempFile(
                    destinationId,
                    new UntypedFTPStream(sourceFileName, sourceURL, false, null),
                    suffix, move, callback, canOverwrite);
        }
        return null;
    }

    protected Integer copyTempFile(final String requestedId, final UntypedStreamInterface stream, final String suffix, final boolean move, final TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        if (requestedId != null && stream != null && suffix != null) {

            String finalFileName = requestedId + suffix;
            String tempFileName = finalFileName + "_";
            URL tempOutputURL = makeURL(tempFileName);
            final UntypedFTPStream tempOutputFileStream = new UntypedFTPStream(tempFileName, tempOutputURL, true);

            //Check if the output stream can be overwritten in case the final file already exists
            URL finalOutputURL = makeURL(finalFileName);
            UntypedFTPStream finalOutputFileStream = new UntypedFTPStream(finalFileName, finalOutputURL, true);
            if (finalOutputFileStream.exists()) {
                if (!canOverwrite) {
                    throw new IOException("The destination stream already exists");
                } else if (canOverwrite && !finalOutputFileStream.isWritable()) {
                    throw new IOException("The destination stream cannot be overwritten");
                }
                //You cannot copy a file over itself; this check is not foolproof though
                if (stream instanceof UntypedFTPStream && stream.equals(finalOutputFileStream)) {
                    throw new IOException("The source stream and the destination stream cannot be the same");
                }
            }
            //If it's a Move operation, check that the original stream can be actually moved, i.e. removed when the copy is done
            //BUG Implement a listening mechanism to continue the thread only when the file has been fully copied and is now available on disk from Apache FileUpload Commons
            //FIX implemented filelocking in UntypedLocalFileStream
            if (move) {
                if (!stream.isWipeable()) {
                    throw new IOException("The source stream cannot be moved");
                }
            }

            try {

                new Retry(retriesBeforeFailure) {
                    @Override
                    public boolean retry() throws Exception {
                        return !tempOutputFileStream.copyFrom(stream, callback);
                    }
                };

                //Add the operation to the commit queue
                int currentOperationId = getNewOperationId();
                addToQueue(currentOperationId, new QueueMoveItem(
                        tempOutputFileStream,
                        finalOutputFileStream));
                if (move) {
                    //If it's a Move operation, the original stream must be removed in the end too
                    addToQueue(currentOperationId, new QueueRemoveItem(stream));
                }
                //Since it's a "bit-perfect" copy, onNewHeader() is not called
                return currentOperationId;
            } catch (Exception e) {
                //Cleanup what could have been partially written on server
                if (tempOutputFileStream != null && tempOutputFileStream.exists()) {
                    tempOutputFileStream.clean();
                }
                //Re-throw the original exception
                throw e;
            }
        }
        return null;
    }

    @Override
    public UntypedStreamInterface getRawStream(FILE_FORMAT requestedFormat, final String requestedId) throws Exception {
        //try to retrieve either the audioTrack and the coverArt
        if (requestedFormat == null) {
            return null;
        }
        if (requestedFormat instanceof AUDIO_FORMAT) {
            return getRawAudioStream(requestedId);
        } else if (requestedFormat instanceof PICTURE_FORMAT) {
            return getRawCoverArtStream(requestedId);
        } else if (requestedFormat instanceof TEXT_FORMAT) {
            return getRawTextStream(requestedId);
        } else {
            return null;
        }
    }

    @Override
    public UntypedFTPStream getRawAudioStream(String requestedId) throws Exception {
        String fileName = requestedId + AUDIO_FORMAT.FILE_NAME_SUFFIX;
        URL audioURL = makeURL(fileName);
        return new UntypedFTPStream(fileName, audioURL, false);
    }

    @Override
    public UntypedFTPStream getRawCoverArtStream(String requestedId) throws Exception {
        String fileName = requestedId + PICTURE_FORMAT.FILE_NAME_SUFFIX;
        URL pictureURL = makeURL(fileName);
        return new UntypedFTPStream(fileName, pictureURL, false);
    }
    
    @Override
    public UntypedFTPStream getRawTextStream(String requestedId) throws Exception {
        String fileName = requestedId + TEXT_FORMAT.FILE_NAME_SUFFIX;
        URL textURL = makeURL(fileName);
        return new UntypedFTPStream(fileName, textURL, false);
    }

    /**
     * Tracking progress of the deletion is not supported
     * @param requestedFormat
     * @param requestedId
     * @return
     * @throws Exception
     **/
    @Override
    public Integer remove(FILE_FORMAT requestedFormat, String requestedId) throws Exception {
        return remove(requestedFormat, requestedId, null);
    }

    /**
     * Tracking progress of the deletion is not supported
     * @param requestedFormat
     * @param requestedId
     * @param callback
     * @return
     * @throws Exception
     **/
    @Override
    public Integer remove(FILE_FORMAT requestedFormat, String requestedId, TransferCallbackInterface callback) throws Exception {
        //try to remove either the audioTrack and the coverArt
        if (requestedFormat == null) {
            return null;
        }
        return dropTempFile(requestedId, requestedFormat.getFileNameSuffix(), callback);
    }

    @Override
    public Integer removePicture(String requestedId) throws Exception {
        return removePicture(requestedId, null);
    }

    @Override
    public Integer removePicture(String requestedId, TransferCallbackInterface callback) throws Exception {
        return dropTempFile(requestedId, PICTURE_FORMAT.FILE_NAME_SUFFIX, callback);
    }

    @Override
    public Integer removeAudio(String requestedId) throws Exception {
        return removeAudio(requestedId, null);
    }

    @Override
    public Integer removeAudio(String requestedId, TransferCallbackInterface callback) throws Exception {
        return dropTempFile(requestedId, AUDIO_FORMAT.FILE_NAME_SUFFIX, callback);
    }

    protected Integer dropTempFile(final String requestedId, final String fileExtension, final TransferCallbackInterface callback) throws Exception {
        String fileName = requestedId + fileExtension;
        URL itemURL = makeURL(fileName);
        UntypedFTPStream itemStream = new UntypedFTPStream(fileName, itemURL, true);

        //Check if the requested file exists first
        if (!itemStream.exists()) {
            throw new IOException("The stream to wipe out does not exist");
        }
        //Then check if it can actually be removed
        //FIX file.canWrite() appears to be buggy on Windows; rely on fileStream.isWipeable() instead, even if it has no real value ATM regarding its implementation
        if (!itemStream.isWipeable()) {
            throw new IOException("The stream to wipe out is write-protected");
        }

        //Add the operation to the commit queue
        int currentOperationId = getNewOperationId();
        //If it's a Move operation, the original stream must be removed in the end too
        addToQueue(currentOperationId, new QueueRemoveItem(
                itemStream));

        //Notify that after deletion the new header is non existent
        if (callback != null) {
            callback.onNewHeader(null);
        }
        return currentOperationId;
    }

    @Override
    public Integer copy(FILE_FORMAT requestedFormat, String sourceId, String destinationId, TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        if (requestedFormat == null) {
            return null;
        }
        return copyTempFile(destinationId, sourceId, requestedFormat.getFileNameSuffix(), false, callback, canOverwrite);
    }

    @Override
    public Integer copy(FILE_FORMAT requestedFormat, String sourceId, String destinationId, final boolean canOverwrite) throws Exception {
        return copy(requestedFormat, sourceId, destinationId, null, canOverwrite);
    }

    @Override
    public Integer copyAudio(String sourceId, String destinationId, TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        return copyTempFile(destinationId, sourceId, AUDIO_FORMAT.FILE_NAME_SUFFIX, false, callback, canOverwrite);
    }

    @Override
    public Integer copyAudio(String sourceId, String destinationId, final boolean canOverwrite) throws Exception {
        return copyAudio(sourceId, destinationId, null, canOverwrite);
    }

    @Override
    public Integer copyPicture(String sourceId, String destinationId, TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        return copyTempFile(destinationId, sourceId, PICTURE_FORMAT.FILE_NAME_SUFFIX, false, callback, canOverwrite);
    }

    @Override
    public Integer copyPicture(String sourceId, String destinationId, final boolean canOverwrite) throws Exception {
        return copyPicture(sourceId, destinationId, null, canOverwrite);
    }

    @Override
    public Integer rename(FILE_FORMAT requestedFormat, String sourceId, String destinationId, TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        if (requestedFormat == null) {
            return null;
        }
        return copyTempFile(destinationId, sourceId, requestedFormat.getFileNameSuffix(), true, callback, canOverwrite);
    }

    @Override
    public Integer rename(FILE_FORMAT requestedFormat, String sourceId, String destinationId, final boolean canOverwrite) throws Exception {
        return rename(requestedFormat, sourceId, destinationId, null, canOverwrite);
    }

    @Override
    public Integer renameAudio(String sourceId, String destinationId, TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        return copyTempFile(destinationId, sourceId, AUDIO_FORMAT.FILE_NAME_SUFFIX, true, callback, canOverwrite);
    }

    @Override
    public Integer renameAudio(String sourceId, String destinationId, final boolean canOverwrite) throws Exception {
        return renameAudio(sourceId, destinationId, null, canOverwrite);
    }

    @Override
    public Integer renamePicture(String sourceId, String destinationId, TransferCallbackInterface callback, final boolean canOverwrite) throws Exception {
        return copyTempFile(destinationId, sourceId, PICTURE_FORMAT.FILE_NAME_SUFFIX, true, callback, canOverwrite);
    }

    @Override
    public Integer renamePicture(String sourceId, String destinationId, final boolean canOverwrite) throws Exception {
        return renamePicture(sourceId, destinationId, null, canOverwrite);
    }

    /** LocalFilesystemDatastore does not uncover its paths to external sources **/
    @Override
    public String getAudioDirectSecuredAccessPublicURL(String requestedId) throws IOException {
        return getAudioDirectSecuredAccessPublicURL(null, null, requestedId);
    }

    @Override
    public String getAudioDirectSecuredAccessPrivateURL(String requestedId) throws IOException {
        return getAudioDirectSecuredAccessPrivateURL(null, null, requestedId);
    }

    /** LocalFilesystemDatastore does not uncover its paths to external sources **/
    @Override
    public String getDirectSecuredAccessPublicURL(FILE_FORMAT requestedFormat, String requestedId) throws IOException {
        return getDirectSecuredAccessPublicURL(null, null, requestedFormat, requestedId);
    }

    @Override
    public String getDirectSecuredAccessPrivateURL(FILE_FORMAT requestedFormat, String requestedId) throws IOException {
        return getDirectSecuredAccessPrivateURL(null, null, requestedFormat, requestedId);
    }

    /** LocalFilesystemDatastore does not uncover its paths to external sources **/
    @Override
    public String getPictureDirectSecuredAccessPublicURL(String requestedId) throws IOException {
        return getPictureDirectSecuredAccessPublicURL(null, null, requestedId);
    }

    @Override
    public String getPictureDirectSecuredAccessPrivateURL(String requestedId) throws IOException {
        return getPictureDirectSecuredAccessPrivateURL(null, null, requestedId);
    }

    @Override
    public String getDirectSecuredAccessPrivateURL(String channelId, String privateKey, FILE_FORMAT requestedFormat, String requestedId) throws IOException {
        if (hasDirectSecuredAccessPrivateURL(requestedFormat, requestedId)) {
            try {
                return makeURL(requestedId, requestedFormat.getFileNameSuffix()).toString();
            } catch (FileNotFoundException ex) {
                throw new IOException("File not found");
            } catch (MalformedURLException ex) {
                throw new IOException("Malformed URI");
            }
        } else {
            throw new IOException("FTPDatastore does not allow direct access to its content");
        }
    }

    @Override
    public String getAudioDirectSecuredAccessPrivateURL(String channelId, String privateKey, String requestedId) throws IOException {
        if (hasAudioDirectSecuredAccessPrivateURL(requestedId)) {
            try {
                return makeURL(requestedId, AUDIO_FORMAT.FILE_NAME_SUFFIX).toString();
            } catch (FileNotFoundException ex) {
                throw new IOException("File not found");
            } catch (MalformedURLException ex) {
                throw new IOException("Malformed URI");
            }
        } else {
            throw new IOException("FTPDatastore does not allow direct access to its content");
        }
    }

    @Override
    public String getPictureDirectSecuredAccessPrivateURL(String channelId, String privateKey, String requestedId) throws IOException {
        if (hasPictureDirectSecuredAccessPrivateURL(requestedId)) {
            try {
                return makeURL(requestedId, PICTURE_FORMAT.FILE_NAME_SUFFIX).toString();
            } catch (FileNotFoundException ex) {
                throw new IOException("File not found");
            } catch (MalformedURLException ex) {
                throw new IOException("Malformed URI");
            }
        } else {
            throw new IOException("FTPDatastore does not allow direct access to its content");
        }
    }

    @Override
    public String getDirectSecuredAccessPublicURL(String userId, String md5_password, FILE_FORMAT requestedFormat, String requestedId) throws IOException {
        throw new IOException("FTPDatastore does not allow direct access to its content");
    }

    @Override
    public String getAudioDirectSecuredAccessPublicURL(String userId, String md5_password, String requestedId) throws IOException {
        throw new IOException("FTPDatastore does not allow direct access to its content");
    }

    @Override
    public String getPictureDirectSecuredAccessPublicURL(String userId, String md5_password, String requestedId) throws IOException {
        throw new IOException("FTPDatastore does not allow direct access to its content");
    }

    /** LocalFilesystemDatastore does not uncover its paths to external sources **/
    @Override
    public boolean hasAudioDirectSecuredAccessPublicURL(String requestedId) {
        return false;
    }

    @Override
    public boolean hasAudioDirectSecuredAccessPrivateURL(String requestedId) {
        return isPrivateFtpURI();
    }

    /** LocalFilesystemDatastore does not uncover its paths to external sources **/
    @Override
    public boolean hasDirectSecuredAccessPublicURL(FILE_FORMAT requestedFormat, String requestedId) {
        return false;
    }

    @Override
    public boolean hasDirectSecuredAccessPrivateURL(FILE_FORMAT requestedFormat, String requestedId) {
        return isPrivateFtpURI();
    }

    /** LocalFilesystemDatastore does not uncover its paths to external sources **/
    @Override
    public boolean hasPictureDirectSecuredAccessPublicURL(String requestedId) {
        return false;
    }

    @Override
    public boolean hasPictureDirectSecuredAccessPrivateURL(String requestedId) {
        return isPrivateFtpURI();
    }

    protected URL makeURL(String filename) throws FileNotFoundException, MalformedURLException {
        return makeURL(filename, null);
    }
    protected URL makeURL(String requestedId, String fileExtension) throws FileNotFoundException, MalformedURLException {
        String fileName = fileExtension!=null ? requestedId + fileExtension : requestedId;
        return new URL(MarshallUtil.removeTrailingSlash(getUrl()) + MarshallUtil.addStartingSlash(fileName));
    }
}
