/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.datastore;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * Impelments the bare minimum mechanisms for datastores based on ordered sequences of commands to process
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 **/
public abstract class QueueBasedStore {

    /** Individual item in the ordered sequence of commands to process**/
    protected interface QueueItemInterface extends Serializable, Comparable<QueueItemInterface> {

        public void commit();

        public void rollback();
    }

   /**
     * What's left to be committed or rollbacked.
     * BEWARE that no locking mechanism is enforced, depending on how the bean is instantiated the queue may be shared system-wide or per-user, results may vary, etc.
     * At the moment any locking mechanism should be implemented in the objects that use this class
     * It is shared among all user-session-instantianted instances of the datastore, since there should be only one per application and other users may commit or rollback items from another user if he's gone before cleaning up
     * TreeMap ensures that keys are sorted because this is mandatory
     * @return
     **/
    private volatile static Map<QueueItemInterface, Collection<Integer>> queue = new TreeMap<QueueItemInterface, Collection<Integer>>();
    /** internal flag */
    private int latestOperationId = 0;

    /**
     * Compute a new transfer id that is guranteed to be unique within the queue
     * @return
     **/
    protected int getNewOperationId() {
        if (latestOperationId == Integer.MAX_VALUE) {
            latestOperationId = Integer.MIN_VALUE;
        }
        return ++latestOperationId;
    }

    /**
     * Append an item to process inside the queue
     * @param id
     * @param newItem
     * @return
     **/
    protected boolean addToQueue(int id, QueueItemInterface newItem) {
        //Check if this item is not already in, in this case just add the new id, otherwise append the item to the queue
        //No null item is accepted
        if (newItem!=null) {
            Collection<Integer> registeredIds = queue.get(newItem);
            if (registeredIds==null) {
                //No previous binding, create a new Collection of ids
                registeredIds = new HashSet<Integer>();
            }
            registeredIds.add(id);
            //Replace the existing entry with the new one, if applicable
            queue.put(newItem, registeredIds);
            return true;
        }
        return false;
    }

    /**
     * 
     */
    public void commit() {
        _commit(null);
    }

    /**
     * Commit the item at index transferId from the sequence
     * @param transferId
     */
    public void commit(Integer transferId) {
        if (transferId != null) {
            Collection<Integer> transferIds = new HashSet<Integer>();
            transferIds.add(transferId);
            _commit(transferIds);
        }
    }

    /**
     * Commit the items at indexs transferIds from the sequence
     * @param transferIds
     */
    public void commit(Collection<Integer> transferIds) {
        if (transferIds != null) {
            _commit(transferIds);
        }
    }

    private boolean isFilteredOut(Collection<Integer> targets, Collection<Integer> probes) {
        if (targets==null || probes==null)
            return false;
        else
            for (Integer probe : probes)
                if (targets.contains(probe)) return false;
        return true;
    }

    private void _commit(Collection<Integer> transferIds) {

        //Browse the sorted queue from top to bottom,
        //Commit each item (filtered or not using transferIds)
        //And finally delete from the queue the items that were processed

        Iterator<Entry<QueueItemInterface, Collection<Integer>>> i = queue.entrySet().iterator();
        while (i.hasNext()) {
            Entry<QueueItemInterface, Collection<Integer>> q = i.next();
            //Filter out the item if applicable
            if (!isFilteredOut(q.getValue(), transferIds)) {
                try {
                    q.getKey().commit();
                } finally {
                    i.remove();
                }
            }
        }
    }

    /**
     * Rollback the list of commands from the sequence
     */
    public void rollback() {
        _rollback(null);
    }

    /**
     * Rollback the specified command at index transferId from the sequence
     * @param transferId
     */
    public void rollback(Integer transferId) {
        if (transferId != null) {
            Collection<Integer> transferIds = new HashSet<Integer>();
            transferIds.add(transferId);
            _rollback(transferIds);
        }
    }

    /**
     * Rollback the specified commands at indexes transferIds from the sequence
     * @param transferIds
     */
    public void rollback(Collection<Integer> transferIds) {
        if (transferIds != null) {
            _rollback(transferIds);
        }
    }

    private void _rollback(Collection<Integer> transferIds) {
        //Browse the sorted queue from top to bottom,
        //Rollback each item (filtered or not using transferIds)
        //And finally delete from the queue the items that were processed

        Iterator<Entry<QueueItemInterface, Collection<Integer>>> i = queue.entrySet().iterator();
        while (i.hasNext()) {
            Entry<QueueItemInterface, Collection<Integer>> q = i.next();
            //Filter out the item if applicable
            if (!isFilteredOut(q.getValue(), transferIds)) {
                try {
                    q.getKey().rollback();
                } finally {
                    i.remove();
                }
            }
        }
    }

}
