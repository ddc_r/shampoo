/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.frontend;

import biz.ddcr.shampoo.client.helper.errors.DownloadingCacheUnchangedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingPreconditionFailedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingWrongRangeForResumeException;
import biz.ddcr.shampoo.server.helper.ServletUtils2;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import biz.ddcr.shampoo.server.io.helper.MIME;
import biz.ddcr.shampoo.server.io.helper.MIMEHelper;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.GenericService;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * Mini-webserver re-implementation with HTTP streaming and NIO via abstraction
 *
 * @author okay_awright
 *
 */
public abstract class DownloadFileHandler<T extends TypedStreamInterface> extends GenericService implements DownloadFileHandlerInterface {

    /**
     * This class represents a byte range.
     *
     */
    protected class DownloadRange {

        long start;
        long end;
        long length;
        long total;

        /**
         * Construct a byte range.
         *
         * @param start Start of the byte range.
         * @param end End of the byte range.
         * @param total Total length of the byte source.
         *
         */
        DownloadRange(long start, long end, long total) {
            this.start = start;
            this.end = end;
            this.length = end - start + 1;
            this.total = total;
        }
    }
    private static final String MULTIPART_BOUNDARY = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
    private SystemConfigurationHelper systemConfigurationHelper;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }    
    
    @Override
    public boolean processFileDownloadRequest(String fileId, HttpServletRequest request, HttpServletResponse response) throws Exception {

        boolean result = false;

        if (beforeAccess(fileId, ServletUtils2.getRemoteAddr(request, getSystemConfigurationHelper().isHttpProxiedIPCheckingEnabled()), request)) {
            T stream = (T) getStream(fileId);
            try {
                if (onCheckFile(stream)) {
                    preProcessFile(stream);
                    try {
                        download(stream, request, response);
                    } catch (NullPointerException e) {
                        //Most likely the connection has been interrupted on the client's end
                        //Just do nothing and pretend it works
                        //e.g. in Grizzly: notifyClosed(final SelectionKey key) in com.sun.enterprise.v3.services.impl.monitor.MonitorableSelectionKeyHandler is not protected against NullPointerExceptions
                        logger.warn("Client abruptly closed connection", e);
                    }
                    result = true;
                }
            } finally {
                //Failsafe; not really sure it's really needed though
                if (stream != null && stream.getRawStream() != null) {
                    stream.getRawStream().closeOutputStream();
                    stream.getRawStream().closeInputStream();
                }
            }
        }
        return result;
    }

    protected void download(T typedStream, HttpServletRequest request, HttpServletResponse response) throws IOException, NullPointerException {

        // Prepare some variables. The ETag is an unique identifier of the typedStream.getFile().
        /*final FILE_FORMAT format = typedStream.getContainer().getFormat();
         final String identifier = typedStream.getRawStream().getIdentifier();*/
        final long length = typedStream.getContainer().getSize();
        final long lastModified = typedStream.getRawStream().getLastModified();
        //Should be enough to be sure of the unicity of the flag per page
        /*String genuineETag = identifier + "-" + format + "/" + length + "@" + lastModified;
         String eTag = DigestUtils.shaHex(genuineETag);*/
        //'W/' prefix is for weak etags
        final int fullHashCode = getSystemConfigurationHelper().isHttpETagGenerationEnabled() ? typedStream.fullHashCode() : 0;
        final String eTag = fullHashCode != 0 ? '"'+String.valueOf(fullHashCode)+'"' : null;

        // Validate request headers for caching ---------------------------------------------------
        // If-None-Match header should contain "*" or ETag. If so, then return 304.        
        String ifNoneMatch = request.getHeader("If-None-Match");
        if (eTag != null) {
            if (ifNoneMatch != null && matches(ifNoneMatch, eTag)) {
                response.setHeader("ETag", eTag); // Required in 304.
                throw new DownloadingCacheUnchangedException(eTag);
            }
        }

        // If-Modified-Since header should be greater than LastModified. If so, then return 304.
        // This header is ignored if any If-None-Match header is specified.
        if (ifNoneMatch == null) {
            long ifModifiedSince = request.getDateHeader("If-Modified-Since");
            if (ifModifiedSince != -1 && ifModifiedSince >= lastModified) {
                if (eTag != null) {
                    response.setHeader("ETag", eTag); // Required in 304.
                }
                throw new DownloadingCacheUnchangedException(eTag);
            }
        }

        // Validate request headers for resume ----------------------------------------------------
        // If-Match header should contain "*" or ETag. If not, then return 412.
        if (eTag != null) {
            String ifMatch = request.getHeader("If-Match");
            if (ifMatch != null && !matches(ifMatch, eTag)) {
                throw new DownloadingPreconditionFailedException(eTag);
            }
        }

        // If-Unmodified-Since header should be greater than LastModified. If not, then return 412.
        long ifUnmodifiedSince = request.getDateHeader("If-Unmodified-Since");
        if (ifUnmodifiedSince != -1 && ifUnmodifiedSince <= lastModified) {
            throw new DownloadingPreconditionFailedException(eTag);
        }

        // Validate and process range -------------------------------------------------------------
        // Prepare some variables. The full Range represents the complete typedStream.getFile().
        DownloadRange full = new DownloadRange(0, length - 1, length);
        List<DownloadRange> ranges = new ArrayList<DownloadRange>();

        // Validate and process Range and If-Range headers.
        String range = request.getHeader("Range");
        if (range != null) {

            // Range header should match format "bytes=n-n,n-n,n-n...". If not, then return 416.
            if (!range.matches("^bytes=\\d*-\\d*(,\\d*-\\d*)*$")) {
                response.setHeader("Content-Range", "bytes */" + length); // Required in 416.
                throw new DownloadingWrongRangeForResumeException(range);
            }

            // If-Range header should either match ETag or be greater then LastModified. If not,
            // then return full typedStream.getFile().
            if (eTag != null) {
                String ifRange = request.getHeader("If-Range");
                if (ifRange != null && !ifRange.equals(eTag)) {
                    try {
                        long ifRangeTime = request.getDateHeader("If-Range"); // Throws IAE if invalid.
                        if (ifRangeTime != -1 && ifRangeTime + 1000 < lastModified) {
                            ranges.add(full);
                        }
                    } catch (IllegalArgumentException ignore) {
                        ranges.add(full);
                    }
                }
            }

            // If any valid If-Range header, then process each part of byte range.
            if (ranges.isEmpty()) {
                for (String part : range.substring(6).split(",")) {
                    // Assuming a file with length of 100, the following examples returns bytes at:
                    // 50-80 (50 to 80), 40- (40 to length=100), -20 (length-20=80 to length=100).
                    long start = sublong(part, 0, part.indexOf('-'));
                    long end = sublong(part, part.indexOf('-') + 1, part.length());

                    if (start == -1) {
                        start = length - end;
                        end = length - 1;
                    } else if (end == -1 || end > length - 1) {
                        end = length - 1;
                    }

                    // Check if Range is syntactically valid. If not, then return 416.
                    if (start > end) {
                        response.setHeader("Content-Range", "bytes */" + length); // Required in 416.
                        throw new DownloadingWrongRangeForResumeException(start + ">" + end);
                    }

                    // Add range.
                    ranges.add(new DownloadRange(start, end, length));
                }
            }
        }

        // Prepare and initialize response --------------------------------------------------------
        // set GZIP support according to the MIME type, right now only text is compressed
        //If the mime type is unknown don't add the field, see: http://www.w3.org/Protocols/rfc2616/rfc2616-sec7.html#sec7.2.1
        boolean acceptsGzip = false;
        boolean acceptsDeflate = false;

        // If content type is text, then determine whether GZIP content encoding is supported by
        // the browser and expand content type with the one and right character encoding.
        /*
         * if (typedStream.getMime() != null &&
         * typedStream.getMime().getMediaType().equals("text")) { String
         * acceptEncoding = request.getHeader("Accept-Encoding"); acceptsGzip =
         * acceptEncoding != null && accepts(acceptEncoding, "gzip");
         * acceptsDeflate = acceptEncoding != null && accepts(acceptEncoding,
         * "deflate");
         }
         */
        // Initialize response.
        response.reset();
        response.setBufferSize(IOUtil.DEFAULT_BUFFER_SIZE);
        response.setHeader("Accept-Ranges", "bytes");
        if (eTag != null) {
            response.setHeader("ETag", eTag);
        }
        response.setDateHeader("Last-Modified", lastModified);
        //UPDATE: removed, not reliable
        //response.setDateHeader("Expires", System.currentTimeMillis() + DEFAULT_EXPIRE_TIME);

        // Send requested file (part(s)) to client ------------------------------------------------
        // Prepare streams.
        OutputStream output = response.getOutputStream();
        if (ranges.isEmpty() || ranges.get(0) == full) {

            // Return full typedStream.getFile().
            final DownloadRange r = full;
            MIME contentType = MIMEHelper.getLikelyMIMEFromFileFormat(typedStream.getContainer().getFormat());
            if (contentType != null) {
                response.setContentType(contentType.toString());
            }
            response.setHeader("Content-Range", "bytes " + r.start + "-" + r.end + "/" + r.total);

            //Transfer file to response
            if (acceptsGzip) {
                // The browser accepts GZIP, so GZIP the content.
                response.setHeader("Content-Encoding", "gzip");
                response.setHeader("Accept-Encoding", "Vary");
                //Sorry, the process now uses blocking calls
                output = new GZIPOutputStream(
                        output,
                        IOUtil.DEFAULT_BUFFER_SIZE);
            } else if (acceptsDeflate) {
                // The browser accepts Deflate, so Deflate the content. (second choice after GZip)
                response.setHeader("Content-Encoding", "deflate");
                response.setHeader("Accept-Encoding", "Vary");
                output = new DeflaterOutputStream(
                        output,
                        new Deflater(Deflater.DEFAULT_STRATEGY),
                        IOUtil.DEFAULT_BUFFER_SIZE);
            } else {
                // Content length is not directly predictable in case of compressed content.
                // So only add the content length header field if the content will be left untouched, otherwise funny things may happen.
                response.setHeader("Content-Length", String.valueOf(r.length));
            }
            //Mark the output stream as OK
            response.setStatus(HttpServletResponse.SC_OK);

            //Transfer file to response
            // Copy full range.
            try {
                typedStream.getRawStream().getInputStream().fastCopyTo(r.start, r.length, output);
            } finally {
                typedStream.getRawStream().closeInputStream();
            }

        } else if (ranges.size() == 1) {

            // Return single part of typedStream.getFile().
            DownloadRange r = ranges.get(0);
            MIME contentType = MIMEHelper.getLikelyMIMEFromFileFormat(typedStream.getContainer().getFormat());
            if (contentType != null) {
                response.setContentType(contentType.toString());
            }
            response.setHeader("Content-Range", "bytes " + r.start + "-" + r.end + "/" + r.total);
            response.setHeader("Content-Length", String.valueOf(r.length));
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT); // 206.

            //Transfer file to response
            // Copy single part range.
            try {
                typedStream.getRawStream().getInputStream().fastCopyTo(r.start, r.length, output);
            } finally {
                typedStream.getRawStream().closeInputStream();
            }

        } else {

            // Return multiple parts of typedStream.getFile().
            response.setContentType("multipart/byteranges; boundary=" + MULTIPART_BOUNDARY);
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT); // 206.

            //Transfer file to response
            // Cast back to ServletOutputStream to get the easy println methods.
            ServletOutputStream sos = (ServletOutputStream) output;

            // Copy multi part range.
            for (DownloadRange r : ranges) {
                // Add multipart boundary and header fields for every range.
                sos.println();
                sos.println("--" + MULTIPART_BOUNDARY);
                MIME contentType = MIMEHelper.getLikelyMIMEFromFileFormat(typedStream.getContainer().getFormat());
                if (contentType != null) {
                    sos.println("Content-Type: " + contentType.toString());
                }
                sos.println("Content-Range: bytes " + r.start + "-" + r.end + "/" + r.total);

                // Copy single part range of multi part range.
                try {
                    typedStream.getRawStream().getInputStream().fastCopyTo(r.start, r.length, output);
                } finally {
                    typedStream.getRawStream().closeInputStream();
                }
            }

            // End with multipart boundary.
            sos.println();
            sos.println("--" + MULTIPART_BOUNDARY + "--");

        }

    }

    /**
     * Returns true if the given accept header accepts the given value.
     *
     * @param acceptHeader The accept header.
     * @param toAccept The value to be accepted.
     * @return True if the given accept header accepts the given value.
     *
     */
    public static boolean accepts(String acceptHeader, String toAccept) {
        String[] acceptValues = acceptHeader.split("\\s*(,|;)\\s*");
        Arrays.sort(acceptValues);
        return Arrays.binarySearch(acceptValues, toAccept) > -1
                || Arrays.binarySearch(acceptValues, toAccept.replaceAll("/.*$", "/*")) > -1
                || Arrays.binarySearch(acceptValues, "*/*") > -1;
    }

    /**
     * Returns true if the given match header matches the given value.
     *
     * @param matchHeader The match header.
     * @param toMatch The value to be matched.
     * @return True if the given match header matches the given value.
     *
     */
    public static boolean matches(String matchHeader, String toMatch) {
        String[] matchValues = matchHeader.split("\\s*,\\s*");
        Arrays.sort(matchValues);
        return Arrays.binarySearch(matchValues, toMatch) > -1
                || Arrays.binarySearch(matchValues, "*") > -1;
    }

    /**
     * Returns a substring of the given string value from the given begin index
     * to the given end index as a long. If the substring is empty, then -1 will
     * be returned
     *
     * @param value The string value to return a substring as long for.
     * @param beginIndex The begin index of the substring to be returned as
     * long.
     * @param endIndex The end index of the substring to be returned as long.
     * @return A substring of the given string value as long or -1 if substring
     * is empty.
     *
     */
    public static long sublong(String value, int beginIndex, int endIndex) {
        String substring = value.substring(beginIndex, endIndex);
        return (substring.length() > 0) ? Long.parseLong(substring) : -1;
    }
}
