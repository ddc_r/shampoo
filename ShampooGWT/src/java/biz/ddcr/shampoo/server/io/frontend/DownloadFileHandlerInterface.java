/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.frontend;

import biz.ddcr.shampoo.server.helper.StaticSessionPoolInterface;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright
 **/
public interface DownloadFileHandlerInterface<T extends TypedStreamInterface> extends StaticSessionPoolInterface {

    public T getStream(String fileId) throws Exception;
    public boolean processFileDownloadRequest(String fileId, HttpServletRequest request, HttpServletResponse response) throws Exception;
    public boolean beforeAccess(String fileId, String remoteIP, HttpServletRequest request) throws Exception;
    public boolean onCheckFile(T file) throws Exception;
    public void preProcessFile(T file) throws Exception;

}
