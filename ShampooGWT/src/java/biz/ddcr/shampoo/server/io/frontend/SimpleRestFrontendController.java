/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.frontend;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionMalformedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionTimedOutException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingCacheUnchangedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingNotFoundException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingPreconditionFailedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingWrongRangeForResumeException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.WebserviceOutOfQuotaException;
import biz.ddcr.shampoo.server.helper.SimpleHTTPController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright
 **/
public abstract class SimpleRestFrontendController extends SimpleHTTPController {

    public abstract boolean processRestQuery(HttpServletRequest request, HttpServletResponse response) throws Exception;

    public abstract boolean isMethodOk(HttpServletRequest request);

    @Override
    public int delegatedHandleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Only get is supported
        //it's then the responsability of the Handler to check if it's really a webservice request or not
        if (isMethodOk(request)) {
            try {

                return processRestQuery(request, response)
                        ? HttpServletResponse.SC_OK
                        : HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            } catch (ConnectionTimedOutException e) {
                logger.warn("processRestQuery(): timed out");
                return HttpServletResponse.SC_REQUEST_TIMEOUT;
            } catch (DownloadingCacheUnchangedException e) {
                logger.info("processRestQuery(): not modified");
                return HttpServletResponse.SC_NOT_MODIFIED;
            } catch (NoEntityException e) {
                logger.warn("processRestQuery(): not found");
                return HttpServletResponse.SC_NOT_FOUND;
            } catch (DownloadingNotFoundException e) {
                logger.warn("processRestQuery(): not found");
                return HttpServletResponse.SC_NOT_FOUND;
            } catch (DownloadingPreconditionFailedException e) {
                logger.warn("processRestQuery(): precondition failed");
                return HttpServletResponse.SC_PRECONDITION_FAILED;
            } catch (DownloadingWrongRangeForResumeException e) {
                logger.warn("processRestQuery(): resume failed, wrong range");
                return HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE;
            } catch (ConnectionMalformedException e) {
                logger.warn("processRestQuery(): malformed");
                return HttpServletResponse.SC_CONFLICT;
            } catch (AccessDeniedException e) {
                logger.warn("processRestQuery(): access denied");
                return HttpServletResponse.SC_FORBIDDEN;
            } catch (BadRequestException e) {
                logger.warn("processRestQuery(): bad request");
                return HttpServletResponse.SC_BAD_REQUEST;
            } catch (WebserviceOutOfQuotaException e) {
                logger.warn("processRestQuery(): ws out of quota");
                return 429; //Too Many Requests (RFC 6585)
            } catch (Exception e) {
                logger.warn("processRestQuery(): unknown error",e);
                return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            }
        }
        return HttpServletResponse.SC_METHOD_NOT_ALLOWED;
    }
}
