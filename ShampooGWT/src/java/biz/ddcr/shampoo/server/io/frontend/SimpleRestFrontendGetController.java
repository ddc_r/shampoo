/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.frontend;

import biz.ddcr.shampoo.server.io.serializer.BasicHTTPDataMarshaller;
import biz.ddcr.shampoo.server.io.serializer.MarshallableInterface;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright
 **/
public abstract class SimpleRestFrontendGetController<T extends MarshallableInterface> extends SimpleRestFrontendController {

    private BasicHTTPDataMarshaller<T> marshaller;

    public BasicHTTPDataMarshaller<T> getMarshaller() {
        return marshaller;
    }

    public void setMarshaller(BasicHTTPDataMarshaller<T> marshaller) {
        this.marshaller = marshaller;
    }

    @Override
    public boolean isMethodOk(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase("GET");
    }

}
