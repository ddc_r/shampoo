/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.helper;

import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import java.util.Collection;
import java.util.HashSet;
import java.util.NoSuchElementException;

/**
 *
 * @author okay_awright
 **/
public class CoverArtConfigurationHelper {

    /** file types currently accepted **/
    private Collection<PICTURE_FORMAT> acceptedFormats = new HashSet<PICTURE_FORMAT>();
    /** -1 or 0 means: no proportional resizing */
    private int rescaleDimension;
    private int maxDimension;
    private int minDimension;
    //Misc options
    private long maxUploadSize; //0 or -1 means unlimited

    public CoverArtConfigurationHelper() {
        //Fallback
        acceptedFormats.add(PICTURE_FORMAT.wbmp);
    }

    public void setAcceptGIF(boolean enabled) {
        if (enabled) {
            this.acceptedFormats.add(PICTURE_FORMAT.gif);
        } else {
            this.acceptedFormats.remove(PICTURE_FORMAT.gif);
        }
    }

    public void setAcceptJPEG(boolean enabled) {
        if (enabled) {
            this.acceptedFormats.add(PICTURE_FORMAT.jpeg);
        } else {
            this.acceptedFormats.remove(PICTURE_FORMAT.jpeg);
        }
    }

    public void setAcceptPNG(boolean enabled) {
        if (enabled) {
            this.acceptedFormats.add(PICTURE_FORMAT.png);
        } else {
            this.acceptedFormats.remove(PICTURE_FORMAT.png);
        }
    }

    public boolean isAcceptFormat(PICTURE_FORMAT format) {
        return acceptedFormats.contains(format);
    }

    public boolean mustResizeIfApplicable() {
        return (maxDimension > 0);
    }

    public int getMaxDimension() {
        //-1 equals do not touch
        if (maxDimension == 0) {
            maxDimension = -1;
        }
        return maxDimension;
    }

    public void setMaxDimension(int maxDimension) {
        this.maxDimension = maxDimension;
    }

    public int getMinDimension() {
        //-1 equals do not touch
        if (minDimension == 0) {
            minDimension = -1;
        }
        return minDimension;
    }

    public void setMinDimension(int minDimension) {
        this.minDimension = minDimension;
    }

    public int getRescaleDimension() {
        //-1 equals do not touch
        if (rescaleDimension == 0) {
            rescaleDimension = -1;
        }
        return rescaleDimension;
    }

    public void setRescaleDimension(int rescaleDimension) {
        this.rescaleDimension = rescaleDimension;
    }

    public long getMaxUploadSize() {
        //-1 equals unlimited
        if (maxUploadSize == 0) {
            maxUploadSize = -1;
        }
        return maxUploadSize;
    }

    public void setMaxUploadSize(long maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }

    public Collection<PICTURE_FORMAT> getAllowedFormats() {
        return acceptedFormats;
    }

    public PICTURE_FORMAT filterOutUnwantedExtensions(String originalFilename) {
        if (originalFilename != null && originalFilename.length()!=0) {
            //Extract extension
            String ext = null;
            int i = originalFilename.lastIndexOf('.');
            if (i > 0 && i < originalFilename.length() - 1) {
                ext = originalFilename.substring(i + 1).toLowerCase();
            }
            //Process extension: transpose a valid extension into a MIME type
            for (PICTURE_FORMAT format : PICTURE_FORMAT.values()) {
                try {
                    if (isAcceptFormat(format)) {
                        if (MIMEHelper.getFileExtensionsForFormat(format).contains(ext)) {
                            return format;
                        }
                    }
                } catch (NoSuchElementException e) {
                    //Do nothing
                }
            }

        }

        return null;
    }

}
