package biz.ddcr.shampoo.server.io.helper;

/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author okay_awright
 * @author Pokidov.Dmitry
 */
public class IOUtil {
    /*Block size that we want to read in one time.*/
    public final static int DEFAULT_BUFFER_SIZE = 256 * 1024; //256Kb

    /*
     * Read all from stream, using nio.
     * @param is source stream.
     * @return result byte array that read from source
     * @throws IOException by {@code Channel.read()}
     */
    public static byte[] readToEndOnePass(InputStream is) throws IOException {
        //create channel for input stream
        ReadableByteChannel bc = Channels.newChannel(is);
        ByteBuffer bb = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);

        while (bc.read(bb) != -1) {
            bb = resizeBuffer(bb); //get new buffer for read
        }
        byte[] result = new byte[bb.position()];
        bb.position(0);
        bb.get(result);

        return result;
    }

    private static ByteBuffer resizeBuffer(ByteBuffer in) {
        ByteBuffer result = in;
        if (in.remaining() < DEFAULT_BUFFER_SIZE) {
            //create new buffer
            result = ByteBuffer.allocate(in.capacity() * 2);
            //set limit to current position in buffer and set position to zero.
            in.flip();
            //put original buffer to new buffer
            result.put(in);
        }

        return result;
    }

    public static int transfer(ByteBuffer src, ByteBuffer dst, boolean needsFlip) {
        int read = 0;
        if (src != null) {
            if (needsFlip) {
                if (src.position() > 0) {
                    src.flip();
                    read = doTransfer(src, dst);
                    if (src.hasRemaining()) {
                        src.compact();
                    } else {
                        src.clear();
                    }
                }
            } else {
                if (src.hasRemaining()) {
                    read = doTransfer(src, dst);
                }
            }
        }

        return read;
    }

    private static int doTransfer(ByteBuffer src, ByteBuffer dst) {
        int read = 0;
        int remaining = src.remaining();
        int toRemaining = dst.remaining();
        if(toRemaining >= remaining) {
            dst.put(src);
            read += remaining;
        } else {
            int limit = src.limit();
            int position = src.position();
            src.limit(position + toRemaining);
            dst.put(src);
            read += toRemaining;
            src.limit(limit);
        }
        return read;
    }
    
    /*
     * Check whether a stream is compressed w\ GZip
     */
    public static boolean isGZipped(UntypedStreamInterface stream) throws IOException {
        boolean result = false;
        InputStream is = stream.getInputStream().getBufferedBackend();
        try {
            is.mark(2);
            try {
                byte byte1 = (byte) is.read();
                if (byte1 != -1) {
                    byte byte2 = (byte) is.read();
                    if (byte2 != -1) {
                        result = ((byte1 == (byte) (GZIPInputStream.GZIP_MAGIC)) && (byte2 == (byte) (GZIPInputStream.GZIP_MAGIC >> 8)));
                    }
                }
            } finally {
                is.reset();
            }
        } finally {
            is.close();
        }
        return result;
    }
    
    public static byte[] gzip(byte[] content) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try{
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(content);
            gzipOutputStream.close();
        } catch(IOException e){
            throw new RuntimeException(e);
        }
        return byteArrayOutputStream.toByteArray();
    }
    public static ByteBuffer gzip(ByteBuffer content) {
        return ByteBuffer.wrap(gzip(content.array()));
    }
    
    public static byte[] ungzip(byte[] contentBytes){
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            InputStream input = new GZIPInputStream(new ByteArrayInputStream(contentBytes));
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int n = 0;
            while ((n = input.read(buffer)) != -1) {
               output.write(buffer, 0, n);
            }
        } catch(IOException e){
            throw new RuntimeException(e);
        }
        return output.toByteArray();
    }
    public static ByteBuffer ungzip(ByteBuffer content) {
        return ByteBuffer.wrap(ungzip(content.array()));
    }

    private IOUtil() {
    }
}
