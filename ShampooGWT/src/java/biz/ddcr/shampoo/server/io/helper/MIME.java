/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.helper;

import java.io.Serializable;
import java.util.regex.Pattern;

/**
 *
 * @author okay_awright
 **/
public class MIME implements Serializable, Comparable<MIME> {

    protected static final Pattern mimeSplitter = Pattern.compile("[/;]++");

    public enum CATEGORY {

        coverArt,
        audioTrack,
        textResource
    }
    private String mediaType;
    private String subType;
    private CATEGORY type;

    public MIME(CATEGORY type, final String mimeType) {
        if (mimeType != null && mimeType.length()!=0) {
            String[] parts = mimeSplitter.split(mimeType.trim());

            if (parts.length > 0) {
                // Treat as the mediaType
                mediaType = parts[0];
            }
            if (parts.length > 1) {
                subType = parts[1];
            }
        }
        this.type = type;
    }

    public MIME(CATEGORY type, String mediaType, String subType) {
        this.type = type;
        this.mediaType = mediaType;
        this.subType = subType;
    }

    public String getMediaType() {
        if (mediaType == null || mediaType.length()==0) {
            mediaType = "*";
        }
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getSubType() {
        if (subType == null || subType.length()==0) {
            subType = "*";
        }
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public CATEGORY getType() {
        return type;
    }

    public void setType(CATEGORY type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return getMediaType() + "/" + getSubType();
    }

    @Override
    public int compareTo(MIME m) {
        //Sort by media, then by subtype
        if (m==null) return 0;
        int diffMedia = this.mediaType.compareTo(m.mediaType);
        return diffMedia == 0 ? this.subType.compareTo(m.subType) : diffMedia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MIME other = (MIME) obj;
        if ((this.mediaType == null) ? (other.mediaType != null) : !this.mediaType.equals(other.mediaType)) {
            return false;
        }
        if ((this.subType == null) ? (other.subType != null) : !this.subType.equals(other.subType)) {
            return false;
        }
        if (this.type != other.type && (this.type == null || !this.type.equals(other.type))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.mediaType != null ? this.mediaType.hashCode() : 0);
        hash = 97 * hash + (this.subType != null ? this.subType.hashCode() : 0);
        hash = 97 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }
}
