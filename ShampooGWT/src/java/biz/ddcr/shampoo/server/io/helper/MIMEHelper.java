/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.helper;

import biz.ddcr.shampoo.server.domain.archive.format.TextFormat;
import biz.ddcr.shampoo.server.domain.archive.format.TextFormat.TEXT_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class MIMEHelper {

    public static MIME getLikelyMIMEFromFileFormat(FILE_FORMAT format) {
        try {
            if (format instanceof AUDIO_FORMAT) {
                return getMIMEsForFormat((AUDIO_FORMAT)format).iterator().next();
            } else if (format instanceof PICTURE_FORMAT) {
                return getMIMEsForFormat((PICTURE_FORMAT)format).iterator().next();
            } else if (format instanceof TEXT_FORMAT) {
                return getMIMEsForFormat((TEXT_FORMAT)format).iterator().next();
            }
        } catch (Exception e) { //includes NoSuchElementException
        }
        return null;
    }

    public static PICTURE_FORMAT getLikelyPictureFormatFromMIME(MIME mime) {
        try {
            for (PICTURE_FORMAT format : PICTURE_FORMAT.values()) {
                if (getMIMEsForFormat(format).contains(mime)) {
                    return format;
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static AUDIO_FORMAT getLikelyAudioFormatFromMIME(MIME mime) {
        try {
            for (AUDIO_FORMAT format : AUDIO_FORMAT.values()) {
                if (getMIMEsForFormat(format).contains(mime)) {
                    return format;
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static TEXT_FORMAT getLikelyTextFormatFromMIME(MIME mime) {
        try {
            for (TEXT_FORMAT format : TEXT_FORMAT.values()) {
                if (getMIMEsForFormat(format).contains(mime)) {
                    return format;
                }
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public static Collection<MIME> getMIMEsForFormat(PICTURE_FORMAT format) {
        return PictureFormat.MIME_PICTURE_FORMAT.get(format);
    }

    public static Collection<MIME> getMIMEsForFormat(AUDIO_FORMAT format) {
        return AudioFormat.MIME_AUDIO_FORMAT.get(format);
    }

    public static Collection<MIME> getMIMEsForFormat(TEXT_FORMAT format) {
        return TextFormat.MIME_TEXT_FORMAT.get(format);
    }
    
    public static Collection<String> getFileExtensionsForFormat(PICTURE_FORMAT format) {
        return PictureFormat.FILE_EXTENSIONS_PICTURE_FORMAT.get(format);
    }

    public static Collection<String> getFileExtensionsForFormat(AUDIO_FORMAT format) {
        return AudioFormat.FILE_EXTENSIONS_AUDIO_FORMAT.get(format);
    }
    
    public static Collection<String> getFileExtensionsForFormat(TEXT_FORMAT format) {
        return TextFormat.FILE_EXTENSIONS_TEXT_FORMAT.get(format);
    }

    private MIMEHelper() {
    }
}
