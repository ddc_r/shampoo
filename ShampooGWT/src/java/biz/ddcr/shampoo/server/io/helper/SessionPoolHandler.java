/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  *
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.helper;

import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface.SessionMetadata;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 *
 * @author okay_awright
 **/
public class SessionPoolHandler {

    protected static final Log logger = LogFactory.getLog(SessionPoolHandler.class);

    public class SessionPoolUploadContainerCollection extends HashMap<String, SessionPoolUploadContainer> implements SessionMetadata {

        @Override
        public void cleanUp() {
            for (SessionPoolUploadContainer container : this.values()) {
                container.cleanUp();
            }
            this.clear();
        }
    }

    public class SessionPoolUploadContainer implements SessionMetadata {

        //private HashSet<MIMEFileItem> files;
        private TypedStreamInterface file;

        public SessionPoolUploadContainer() {
        }

        /*public SessionPoolUploadContainer(ServletFileUpload servlet, HashSet<MIMEFileItem> tempFiles) {
        this.files = tempFiles;
        }**/
        public SessionPoolUploadContainer(TypedStreamInterface tempFile) {
            this.file = tempFile;
        }

        /*public HashSet<MIMEFileItem> getFiles() {
        if (files == null) {
        files = new HashSet<MIMEFileItem>();
        }
        return files;
        }**/
        public TypedStreamInterface getFile() {
            return file;
        }

        /*public void setFiles(HashSet<MIMEFileItem> tempFiles) {
        this.files = tempFiles;
        }**/
        public void setFile(TypedStreamInterface tempFile) {
            this.file = tempFile;
        }

        /*public void cleanUp() {
        for (MIMEFileItem fileItem : files)
        cleanResource(fileItem);
        files.clear();
        }**/
        @Override
        public void cleanUp() {
            cleanResource(file);
        }
    }
    private static final String UPLOAD_POOL_KEY = "SHAMPOO.UPLOAD.POOL";
    private SecurityManagerInterface securityManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public SessionPoolUploadContainerCollection getSessionPoolUploadContainers() {
        SessionPoolUploadContainerCollection sessionPoolObjects = (SessionPoolUploadContainerCollection) securityManager.getCurrentlyAuthenticatedUserMetadataFromSession(UPLOAD_POOL_KEY);
        if (sessionPoolObjects == null) {
            sessionPoolObjects = new SessionPoolUploadContainerCollection();
        }
        return sessionPoolObjects;
    }

    public void setSessionPoolUploadContainers(SessionPoolUploadContainerCollection sessionPoolUpContainers) {
        if (sessionPoolUpContainers != null) {
            logger.info("Session pool data reset");
            securityManager.addCurrentlyAuthenticatedUserMetadataFromSession(UPLOAD_POOL_KEY, sessionPoolUpContainers);
        }
    }

    /**
     * Store in the current session an upload servlet
     *
     * @return
     **/
    public boolean addSessionPoolUploadContainer(String uploadId, SessionPoolUploadContainer sessionPoolUpContainer) {
        SessionPoolUploadContainerCollection servlets = getSessionPoolUploadContainers();
        if (!servlets.containsKey(uploadId)) {
            if (servlets.put(uploadId, sessionPoolUpContainer) == null) {
                logger.info("Session pool data added");
                setSessionPoolUploadContainers(servlets);
                return true;
            }
        }
        logger.warn("Cannot add session pool data");
        return false;
    }

    /*public boolean addSessionPoolUploadContainer(String uploadId, ServletFileUpload uploadServlet, HashSet<MIMEFileItem> tempFiles) throws Exception {
    return addSessionPoolUploadContainer(uploadId, new SessionPoolUploadContainer(uploadServlet, tempFiles));
    }**/
    public boolean addSessionPoolUploadContainer(String uploadId, TypedStreamInterface tempFile) {
        return addSessionPoolUploadContainer(uploadId, new SessionPoolUploadContainer(tempFile));
    }

    public boolean removeSessionPoolUpContainer(String uploadId) throws Exception {
        if (getSessionPoolUploadContainers().remove(uploadId) != null) {
            if (getSessionPoolUploadContainers().remove(uploadId) != null) {
                logger.info("Session pool data removed");
                setSessionPoolUploadContainers(getSessionPoolUploadContainers());
            }
        }
        logger.warn("Cannot remove session pool data");
        return false;
    }

    public SessionPoolUploadContainer getSessionPoolUploadContainer(String uploadId) {
        return getSessionPoolUploadContainers().get(uploadId);
    }

    /*public HashSet<MIMEFileItem> getSessionPoolUploadFile(String uploadId) {
    SessionPoolUploadContainer container = getSessionPoolUploadContainer(uploadId);
    if (container != null) {
    return container.getFiles();
    } else {
    return null;
    }
    }**/
    public TypedStreamInterface getSessionPoolUploadFile(String uploadId) {
        SessionPoolUploadContainer container = getSessionPoolUploadContainer(uploadId);
        if (container != null) {
            return container.getFile();
        } else {
            return null;
        }
    }

    /*public boolean addSessionPoolUploadFile(String uploadId, MIMEFileItem file) {
    SessionPoolUploadContainer container = getSessionPoolUploadContainer(uploadId);
    if (container != null) {
    if (container.getFiles().add(file)) {
    setSessionPoolUploadContainers(getSessionPoolUploadContainers());
    return true;
    }
    }
    return false;
    }**/
    public boolean addSessionPoolUploadFile(String uploadId, TypedStreamInterface file) {
        SessionPoolUploadContainer container = getSessionPoolUploadContainer(uploadId);
        if (container == null) {
            addSessionPoolUploadContainer(uploadId, file);
        } else {
            container.setFile(file);
            setSessionPoolUploadContainers(getSessionPoolUploadContainers());
        }
        return true;
    }

    /*public HashSet<MIMEFileItem> getFileItems(String uploadId) {
    SessionPoolUploadContainer container = getSessionPoolUploadContainer(uploadId);
    if (container != null) {
    return container.getFiles();
    } else {
    return null;
    }
    }**/
    public static void cleanResources(Collection<TypedStreamInterface> items) {
        if (items != null) {
            for (TypedStreamInterface item : items) //Recast since commons-fileupload doesn't properly do it
            {
                cleanResource(item);
            }
        }
    }

    public static void cleanResource(TypedStreamInterface item) {
        if (item != null) {
            if (item.getRawStream() != null && item.getRawStream().isWipeable()) {
                try {
                    item.getRawStream().clean();
                    //WORKAROUND: called too late, the logging will raise an exception by itself if present
                    //logger.warn("Item "+item.toString()+" in pool cleaned up");
                } catch (IOException ex) {
                    //Silently drop the error
                }
            }
            item = null;
        }
    }
}
