/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.helper;

import biz.ddcr.shampoo.client.helper.errors.TrackBitrateTooHighException;
import biz.ddcr.shampoo.client.helper.errors.TrackBitrateTooLowException;
import biz.ddcr.shampoo.client.helper.errors.TrackBrokenException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotCBRException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotStereoException;
import biz.ddcr.shampoo.client.helper.errors.TrackSamplerateTooHighException;
import biz.ddcr.shampoo.client.helper.errors.TrackSamplerateTooLowException;
import biz.ddcr.shampoo.client.helper.errors.TrackTooLongException;
import biz.ddcr.shampoo.client.helper.errors.TrackTooShortException;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFeaturesException;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

/**
 *
 * @author okay_awright
 **/
public class TrackConfigurationHelper {

    private class FormatConfiguration {
        public boolean accepted = false;
        public FormatSpecificQualityControlParameters quality = new FormatSpecificQualityControlParameters();
    }

    private class FormatSpecificQualityControlParameters {
        public long minBitrate = -1;
        public long maxBitrate = -1;
        public boolean acceptVBR = true;
    }

    /** file types currently accepted **/
    private Map<AUDIO_FORMAT, FormatConfiguration> acceptedFormats = new HashMap<AUDIO_FORMAT, FormatConfiguration>();
    /** quality control **/
    private boolean acceptMono;
    private int maxSamplerate;
    private int minSamplerate;
    private int minDuration;
    private int maxDuration;
    //Misc options
    private long maxUploadSize; //0 or -1 means unlimited

    private void setAcceptFormat(AUDIO_FORMAT format, boolean accepted) {
        getOptions(format).accepted = accepted;
    }

    public void setAcceptMP4AAC(boolean acceptMP4AAC) {
        setAcceptFormat(AUDIO_FORMAT.mp4_aac, acceptMP4AAC);
    }

    public void setAcceptNativeFlac(boolean acceptNativeFlac) {
        setAcceptFormat(AUDIO_FORMAT.native_flac, acceptNativeFlac);
    }

    public void setAcceptMP3(boolean acceptMP3) {
        setAcceptFormat(AUDIO_FORMAT.mp3, acceptMP3);
    }

    public void setAcceptOggVorbis(boolean acceptOggVorbis) {
        setAcceptFormat(AUDIO_FORMAT.ogg_vorbis, acceptOggVorbis);
    }

    public boolean isAcceptMono() {
        return acceptMono;
    }

    public void setAcceptMono(boolean acceptMono) {
        this.acceptMono = acceptMono;
    }

    private FormatConfiguration getOptions(AUDIO_FORMAT format) {
        FormatConfiguration options = null;
        if (this.acceptedFormats.containsKey(format)) {
            options = this.acceptedFormats.get(format);
        } else {
            options = new FormatConfiguration();
            this.acceptedFormats.put(format, options);
        }
        return options;
    }

    public boolean isAcceptVBR(AUDIO_FORMAT format) {
        return getOptions(format).quality.acceptVBR;
    }

    public void setAcceptVBR(AUDIO_FORMAT format, boolean acceptVBR) {
        getOptions(format).quality.acceptVBR = acceptVBR;
    }

    public boolean isAcceptMP3VBR() {
        return isAcceptVBR(AUDIO_FORMAT.mp3);
    }

    public void setAcceptMP3VBR(boolean acceptVBR) {
        setAcceptVBR(AUDIO_FORMAT.mp3, acceptVBR);
    }

    public boolean isAcceptNativeFlacVBR() {
        return isAcceptVBR(AUDIO_FORMAT.native_flac);
    }

    public void setAcceptNativeFlacVBR(boolean acceptVBR) {
        setAcceptVBR(AUDIO_FORMAT.native_flac, acceptVBR);
    }

    public boolean isAcceptMP4AACVBR() {
        return isAcceptVBR(AUDIO_FORMAT.mp4_aac);
    }

    public void setAcceptMP4AACVBR(boolean acceptVBR) {
        setAcceptVBR(AUDIO_FORMAT.mp4_aac, acceptVBR);
    }

    public boolean isAcceptOggVorbisVBR() {
        return isAcceptVBR(AUDIO_FORMAT.ogg_vorbis);
    }

    public void setAcceptOggVorbisVBR(boolean acceptVBR) {
        setAcceptVBR(AUDIO_FORMAT.ogg_vorbis, acceptVBR);
    }

    public boolean isAcceptFormat(AUDIO_FORMAT format) {
        return getOptions(format).accepted;
    }

    public long getMaxBitrate(AUDIO_FORMAT format) {
        return getOptions(format).quality.maxBitrate;
    }

    public void setMaxBitrate(AUDIO_FORMAT format, long maxBitrate) {
        getOptions(format).quality.maxBitrate = maxBitrate;
    }

    public long getMaxMP3Bitrate() {
        return getMaxBitrate(AUDIO_FORMAT.mp3);
    }

    public void setMaxMP3Bitrate(long maxBitrate) {
        setMaxBitrate(AUDIO_FORMAT.mp3, maxBitrate);
    }

    public long getMaxNativeFlacBitrate() {
        return getMaxBitrate(AUDIO_FORMAT.native_flac);
    }

    public void setMaxNativeFlacBitrate(long maxBitrate) {
        setMaxBitrate(AUDIO_FORMAT.native_flac, maxBitrate);
    }

    public long getMaxMP4AACBitrate() {
        return getMaxBitrate(AUDIO_FORMAT.mp4_aac);
    }

    public void setMaxMP4AACBitrate(long maxBitrate) {
        setMaxBitrate(AUDIO_FORMAT.mp4_aac, maxBitrate);
    }

    public long getMaxOggVorbisBitrate() {
        return getMaxBitrate(AUDIO_FORMAT.ogg_vorbis);
    }

    public void setMaxOggVorbisBitrate(long maxBitrate) {
        setMaxBitrate(AUDIO_FORMAT.ogg_vorbis, maxBitrate);
    }

    public int getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(int maxDuration) {
        this.maxDuration = maxDuration;
    }

    public int getMaxSamplerate() {
        return maxSamplerate;
    }

    public void setMaxSamplerate(int maxSamplerate) {
        this.maxSamplerate = maxSamplerate;
    }

    public long getMinBitrate(AUDIO_FORMAT format) {
        return getOptions(format).quality.minBitrate;
    }

    public void setMinBitrate(AUDIO_FORMAT format, long minBitrate) {
        getOptions(format).quality.minBitrate = minBitrate;
    }

    public long getMinMP3Bitrate() {
        return getMinBitrate(AUDIO_FORMAT.mp3);
    }

    public void setMinMP3Bitrate(long minBitrate) {
        setMinBitrate(AUDIO_FORMAT.mp3, minBitrate);
    }

    public long getMinNativeFlacBitrate() {
        return getMinBitrate(AUDIO_FORMAT.native_flac);
    }

    public void setMinNativeFlacBitrate(long minBitrate) {
        setMinBitrate(AUDIO_FORMAT.native_flac, minBitrate);
    }

    public long getMinMP4AACBitrate() {
        return getMinBitrate(AUDIO_FORMAT.mp4_aac);
    }

    public void setMinMP4AACBitrate(long minBitrate) {
        setMinBitrate(AUDIO_FORMAT.mp4_aac, minBitrate);
    }

    public long getMinOggVorbisBitrate() {
        return getMinBitrate(AUDIO_FORMAT.ogg_vorbis);
    }

    public void setMinOggVorbisBitrate(long minBitrate) {
        setMinBitrate(AUDIO_FORMAT.ogg_vorbis, minBitrate);
    }

    public int getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(int minDuration) {
        this.minDuration = minDuration;
    }

    public int getMinSamplerate() {
        return minSamplerate;
    }

    public void setMinSamplerate(int minSamplerate) {
        this.minSamplerate = minSamplerate;
    }

    public long getMaxUploadSize() {
        //-1 equals unlimited
        if (maxUploadSize == 0) {
            maxUploadSize = -1;
        }
        return maxUploadSize;
    }

    public void setMaxUploadSize(long maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }

    public Collection<AUDIO_FORMAT> getAllowedFormats() {
        Collection<AUDIO_FORMAT> allowedFormats = new HashSet<AUDIO_FORMAT>();
        for (Entry<AUDIO_FORMAT, FormatConfiguration> entry : acceptedFormats.entrySet()) {
            if (entry.getValue().accepted)
                allowedFormats.add(entry.getKey());
        }
        return allowedFormats;
    }

    public AUDIO_FORMAT filterOutUnwantedExtensions(String originalFilename) {
        if (originalFilename != null && originalFilename.length()!=0) {
            //Extract extension
            String ext = null;
            int i = originalFilename.lastIndexOf('.');
            if (i > 0 && i < originalFilename.length() - 1) {
                ext = originalFilename.substring(i + 1).toLowerCase();
            }
            //Process extension: transpose a valid extension into a MIME type
            for (AUDIO_FORMAT format : AUDIO_FORMAT.values()) {
                try {
                    if (isAcceptFormat(format)) {
                        if (MIMEHelper.getFileExtensionsForFormat(format).contains(ext)) {
                            return format;
                        }
                    }
                } catch (NoSuchElementException e) {
                    //Do nothing
                }
            }

        }

        return null;
    }

    public boolean checkFeatures(AudioFileInfo container) {

        boolean result = true;
        Collection<RuntimeException> subErrors = new ArrayList<RuntimeException>();

        //Bitrates are only meaningful for lossy codecs
        if ((getMinBitrate(container.getFormat()) > 0 && container.getBitrate() < getMinBitrate(container.getFormat())) && (getMaxBitrate(container.getFormat()) > 0 && container.getBitrate() > getMaxBitrate(container.getFormat()))) {
            //This should not happen, unless the file is seriously broken, in this case throw a global exception
            throw new TrackBrokenException(String.valueOf(container.getBitrate()));
        }
        if (getMinBitrate(container.getFormat()) > 0 && container.getBitrate() < getMinBitrate(container.getFormat())) {
            subErrors.add(new TrackBitrateTooLowException(String.valueOf(container.getBitrate())));
            result = false;
        }
        if (getMaxBitrate(container.getFormat()) > 0 && container.getBitrate() > getMaxBitrate(container.getFormat())) {
            subErrors.add(new TrackBitrateTooHighException(String.valueOf(container.getBitrate())));
            result = false;
        }

        if (!isAcceptMono() && container.getChannels() < 2) {
            subErrors.add(new TrackNotStereoException(String.valueOf(container.getChannels())));
            result = false;
        }

        if ((getMinSamplerate() > 0 && container.getSamplerate() < getMinSamplerate()) && (getMaxSamplerate() > 0 && container.getSamplerate() > getMaxSamplerate())) {
            //This should not happen, unless the file is seriously broken, in this case throw a global exception
            throw new TrackBrokenException(String.valueOf(container.getSamplerate()));
        }
        if (getMinSamplerate() > 0 && container.getSamplerate() < getMinSamplerate()) {
            subErrors.add(new TrackSamplerateTooLowException(String.valueOf(container.getSamplerate())));
            result = false;
        }
        if (getMaxSamplerate() > 0 && container.getSamplerate() > getMaxSamplerate()) {
            subErrors.add(new TrackSamplerateTooHighException(String.valueOf(container.getSamplerate())));
            result = false;
        }

        if (!isAcceptVBR(container.getFormat()) && container.isVbr()) {
            subErrors.add(new TrackNotCBRException(String.valueOf(container.isVbr())));
            result = false;
        }

        if ((getMinDuration() > 0 && container.getDuration() < getMinDuration()) && (getMaxDuration() > 0 && container.getDuration() > getMaxDuration())) {
            //This should not happen, unless the file is seriously broken, in this case throw a global exception
            throw new TrackBrokenException(String.valueOf(container.getDuration()));
        }
        if (getMinDuration() > 0 && container.getDuration() < getMinDuration()) {
            subErrors.add(new TrackTooShortException(String.valueOf(container.getDuration())));
            result = false;
        }
        if (getMaxDuration() > 0 && container.getDuration() > getMaxDuration()) {
            subErrors.add(new TrackTooLongException(String.valueOf(container.getDuration())));
            result = false;
        }

        if (!result) {
            throw new UploadingWrongFeaturesException(subErrors);
        }
        return result;
    }

}
