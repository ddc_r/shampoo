/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.parser;

import biz.ddcr.shampoo.server.io.parser.mp3.MP3HeaderParser;
import biz.ddcr.shampoo.client.helper.errors.TrackBrokenException;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.io.parser.flac.FlacHeaderParser;
import biz.ddcr.shampoo.server.io.parser.mp4.MP4HeaderParser;
import biz.ddcr.shampoo.server.io.parser.ogg.OggHeaderParser;
import java.io.IOException;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.tag.TagException;

/**
 *
 * Parse MP3 file headers and tags
 *
 * @author okay_awright
 **/
public class FileHeaderParser {

    public static AudioFileInfo getHeader(AUDIO_FORMAT format, UntypedStreamInterface stream) throws IOException {
        switch (format) {
            case mp3:
                return getMP3Header(stream);
            case mp4_aac:
                return getMP4Header(stream);
            case native_flac:
                return getFlacHeader(stream);
            case ogg_vorbis:
                return getOggHeader(stream);
            default:
                return null;
        }
    }

    public static Tag getTag(final CoverArtConfigurationHelper coverArtConfigurationHelper, AUDIO_FORMAT format, UntypedStreamInterface stream) throws IOException {
        switch (format) {
            case mp3:
                return getMP3Tag(coverArtConfigurationHelper, stream);
            case mp4_aac:
                return getMP4Tag(coverArtConfigurationHelper, stream);
            case native_flac:
                return getFlacTag(coverArtConfigurationHelper, stream);
            case ogg_vorbis:
                return getOggTag(coverArtConfigurationHelper, stream);
            default:
                return null;
        }
    }

    public static AudioFileInfo getMP3Header(UntypedStreamInterface stream) {

        AudioFileInfo container = null;
        if (stream != null) {

            try {
                container = MP3HeaderParser.parseHeader(stream);
            } catch (InvalidAudioFrameException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return container;
    }

    public static AudioFileInfo getMP4Header(UntypedStreamInterface stream) {

        AudioFileInfo container = null;
        if (stream != null) {

            try {
                container = MP4HeaderParser.parseHeader(stream);
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return container;
    }

    public static AudioFileInfo getFlacHeader(UntypedStreamInterface stream) {

        AudioFileInfo container = null;
        if (stream != null) {

            try {
                container = FlacHeaderParser.parseHeader(stream);
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return container;
    }

    public static AudioFileInfo getOggHeader(UntypedStreamInterface stream) {

        AudioFileInfo container = null;
        if (stream != null) {

            try {
                container = OggHeaderParser.parseHeader(stream);
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return container;
    }

    public static Tag getMP3Tag(final CoverArtConfigurationHelper coverArtConfigurationHelper, UntypedStreamInterface stream) {

        Tag tag = null;
        if (stream != null) {

            try {
                tag = MP3HeaderParser.parseTag(coverArtConfigurationHelper, stream);
            } catch (InvalidAudioFrameException ex) {
                //Do nothing; surely not the right format
            } catch (TagException ex) {
                //Do nothing; surely not the right format
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return tag;
    }

    public static Tag getMP4Tag(final CoverArtConfigurationHelper coverArtConfigurationHelper,UntypedStreamInterface stream) {

        Tag tag = null;
        if (stream != null) {

            try {
                tag = MP4HeaderParser.parseTag(coverArtConfigurationHelper, stream);
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return tag;
    }

    public static Tag getFlacTag(final CoverArtConfigurationHelper coverArtConfigurationHelper,UntypedStreamInterface stream) {

        Tag tag = null;
        if (stream != null) {

            try {
                tag = FlacHeaderParser.parseTag(coverArtConfigurationHelper, stream);
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return tag;
    }

    public static Tag getOggTag(final CoverArtConfigurationHelper coverArtConfigurationHelper,UntypedStreamInterface stream) {

        Tag tag = null;
        if (stream != null) {

            try {
                tag = OggHeaderParser.parseTag(coverArtConfigurationHelper, stream);
            } catch (CannotReadException e) {
                //Do nothing; surely not the right format
            } catch (IOException e) {
                //More concerning, the track is likely corrupt
                throw new TrackBrokenException(e.getMessage());
            }
        }
        return tag;
    }

    private FileHeaderParser() {
    }
}
