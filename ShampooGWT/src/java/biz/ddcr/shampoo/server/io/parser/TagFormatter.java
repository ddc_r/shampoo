/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.parser;

import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.MIME;
import biz.ddcr.shampoo.server.io.helper.MIME.CATEGORY;
import biz.ddcr.shampoo.server.io.helper.MIMEHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.UntypedMemoryStream;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import javax.imageio.ImageIO;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.datatype.Artwork;

/**
 *
 * @author okay_awright
 **/
public class TagFormatter {

    public static Tag rebuildTagMetadata(CoverArtConfigurationHelper coverArtConfigurationHelper, org.jaudiotagger.tag.Tag metadata) {
        Tag tag = new Tag();

        //Most popular metadata
        tag.setAuthor(
                metadata.getFirst(FieldKey.ARTIST));
        tag.setAlbum(
                metadata.getFirst(FieldKey.ALBUM));
        tag.setTitle(
                metadata.getFirst(FieldKey.TITLE));
        tag.setDescription(
                metadata.getFirst(FieldKey.COMMENT));
        tag.setDateOfRelease(
                convertDateOfRelease(metadata.getFirst(FieldKey.YEAR)));
        tag.setGenre(
                metadata.getFirst(FieldKey.GENRE));
        //No corresponding metadata in common tag formats
        tag.setRating(null);
        tag.setPublisher(
                metadata.getFirst(FieldKey.RECORD_LABEL));
        tag.setCopyright(
                metadata.getFirst(FieldKey.ISRC));
        //Cover art
        Artwork coverArt = metadata.getFirstArtwork();
        PictureStream pictureStream = null;
        if (coverArt != null) {

            PICTURE_FORMAT format = null;
            //TEMPORARY IMPL.: always stored in memory for simplicity
            //TODO: make it an UntypedLocalMutantStream
            UntypedStreamInterface untypedStream = new UntypedMemoryStream(null, coverArt.getBinaryData());

            try {
                //Resize the picture if applicable
                if (coverArtConfigurationHelper.mustResizeIfApplicable()) {
                    format = TagFormatter.rescalePicture(coverArtConfigurationHelper, untypedStream);
                }
                //if format is null then it is either because something went wrong, or the picture didn't need to be rescaled
                if (format == null) {
                    format = MIMEHelper.getLikelyPictureFormatFromMIME(new MIME(CATEGORY.coverArt, coverArt.getMimeType()));
                    //if the format is still null then exit right away
                    if (format == null) {
                        throw new IOException("The embedded cover art file format is unknown or incompatible");
                    }
                }

                //Then wrap the stream to be stored
                PictureFileInfo container = new PictureFileInfo();
                container.setFormat(format);
                container.setSize(untypedStream.getSize());
                pictureStream = new PictureStream(container, untypedStream);
            } catch (IOException ex) {
                LogFactory.getLog(TagFormatter.class).error("Cannot process and store tag-embedded cover art", ex);
            }
        }
        tag.setCoverArt(pictureStream);

        return tag;
    }

    static Short convertDateOfRelease(String sDateOfRelease) {
        Short eDateOfRelease = null;
        if (sDateOfRelease != null && sDateOfRelease.trim().length()!=0) {
            //trying to get a 4-digit year from free text is too complex, just convert the raw String and see what happens
            try {
                eDateOfRelease = Short.decode(sDateOfRelease);
            } catch (NumberFormatException e) {
                //Do nothing
            }
        }
        return eDateOfRelease;
    }

    /**
     * 
     * Overwrite a stream containing some picture data with a rescaled version if necessary
     * Returns the new PICTURE_FORMAT used to save the new picture, null may indicate that either the stream could not be processed or that the stream has not been processed
     * 
     * @param stream
     * @throws IOException
     **/
    public static PICTURE_FORMAT rescalePicture(CoverArtConfigurationHelper coverArtConfigurationHelper, UntypedStreamInterface stream) throws IOException {
        PICTURE_FORMAT format = null;
        BufferedImage bsrc = null;
        InputStream inputStream = null;
        try {
            //FIX: The ImageIO cache is apparently buggy for whatever reason: it creates a memory leak in the OS heap!
            //Don't cache the transformations
            ImageIO.setUseCache(false);
            inputStream = stream.getInputStream().getBufferedBackend();
            bsrc = ImageIO.read(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        if (bsrc != null) {

            int originalWidth = bsrc.getWidth();
            int originalHeight = bsrc.getHeight();

            //Check if the original dimensions are within acceptable ranges
            if (!((coverArtConfigurationHelper.getMaxDimension()<1 ||
                    (originalWidth <= coverArtConfigurationHelper.getMaxDimension()
                        && originalHeight <= coverArtConfigurationHelper.getMaxDimension()))
                    && originalWidth >= coverArtConfigurationHelper.getMinDimension()
                    && originalHeight >= coverArtConfigurationHelper.getMinDimension()))
            {
                throw new UploadingWrongFormatException(originalWidth+"x"+originalHeight);
            }

            BufferedImage bdest = null;
            boolean hasAlphaChannel = false;
            //Check if resizing is really required
            if (!(
                    (originalWidth == coverArtConfigurationHelper.getRescaleDimension() && originalWidth>=originalHeight)
                    ||
                    (originalHeight == coverArtConfigurationHelper.getRescaleDimension() && originalHeight>=originalWidth))) {

                double newRatio = (double) coverArtConfigurationHelper.getRescaleDimension() / (double) Math.max(originalWidth, originalHeight);
                int newWidth = NumberUtil.safeLongToInt(Math.round(originalWidth * newRatio));
                int newHeight = NumberUtil.safeLongToInt(Math.round(originalHeight * newRatio));

                //FIX: drop support for Graphics2D and roll our own algorithm: no dependency on core AWT anymore, works in pure headless environments like the early OpenJDK7 (2009) for NetBSD
                //Plain simple neearest-neighbour rescaling algorithm
                int intRGBFormat;
                switch (bsrc.getType()) {
                    case BufferedImage.TYPE_4BYTE_ABGR:
                    case BufferedImage.TYPE_INT_ARGB:
                    case BufferedImage.TYPE_CUSTOM:
                    case BufferedImage.TYPE_4BYTE_ABGR_PRE:
                    case BufferedImage.TYPE_INT_ARGB_PRE:
                        intRGBFormat = BufferedImage.TYPE_INT_ARGB;
                        break;
                    default:
                        intRGBFormat = BufferedImage.TYPE_INT_RGB;
                }
                hasAlphaChannel = (intRGBFormat == BufferedImage.TYPE_INT_ARGB);

                bdest = new BufferedImage(newWidth, newHeight, intRGBFormat);
                int[] rgbsdest = new int[newWidth * newHeight];
                int x_ratio = (originalWidth << 16) / newWidth + 1;
                /**quick fix for (1/3)*3==0 with integers**/
                int y_ratio = (originalHeight << 16) / newHeight + 1;
                /**quick fix for (1/3)*3==0 with integers**/
                int x, y;
                int[] rgbsrc = new int[originalWidth * originalHeight];
                bsrc.getRGB(0, 0, originalWidth, originalHeight, rgbsrc, 0, originalWidth);
                for (int i = 0; i < newHeight; i++) {
                    for (int j = 0; j < newWidth; j++) {
                        x = ((j * x_ratio) >> 16);
                        y = ((i * y_ratio) >> 16);
                        //No need to explode channel components since they're just copied, and not modified
                        rgbsdest[(i * newWidth) + j] = rgbsrc[(y * originalWidth) + x];
                    }
                }
                bdest.setRGB(0, 0, newWidth, newHeight, rgbsdest, 0, newWidth);
                /*BufferedImage bdest =
                new BufferedImage(
                newWidth,
                newHeight,
                BufferedImage.TYPE_INT_RGB);
                Graphics2D g = bdest.createGraphics();
                AffineTransform at =
                AffineTransform.getScaleInstance(
                newRatio,
                newRatio);
                g.drawRenderedImage(bsrc, at);*/

                //Choose which format is more suitable for output: enforce a fixed order since I don't like GIF, love PNG, but think JPEG is more adapted to album cover arts in the end
                String informalImagePluginName = "WBMP";
                //Now change the format which may obviously have changed due to the transcoding
                //If an error occured during writing on stream an exception will be raised and the new format will be discarded
                format = PICTURE_FORMAT.wbmp;
                //If the source features an alpha channel try to save it in a format that supports it
                if (hasAlphaChannel) {
                    if (coverArtConfigurationHelper.isAcceptFormat(PICTURE_FORMAT.png) && canWriteImageFormat("PNG")) {
                        informalImagePluginName = "PNG";
                        format = PICTURE_FORMAT.png;
                    } else if (coverArtConfigurationHelper.isAcceptFormat(PICTURE_FORMAT.gif) && canWriteImageFormat("GIF")) {
                        informalImagePluginName = "GIF";
                        format = PICTURE_FORMAT.gif;
                    } else if (coverArtConfigurationHelper.isAcceptFormat(PICTURE_FORMAT.jpeg) && canWriteImageFormat("JPEG")) {
                        informalImagePluginName = "JPEG";
                        format = PICTURE_FORMAT.jpeg;
                    }
                } else {
                    if (coverArtConfigurationHelper.isAcceptFormat(PICTURE_FORMAT.jpeg) && canWriteImageFormat("JPEG")) {
                        informalImagePluginName = "JPEG";
                        format = PICTURE_FORMAT.jpeg;
                    } else if (coverArtConfigurationHelper.isAcceptFormat(PICTURE_FORMAT.png) && canWriteImageFormat("PNG")) {
                        informalImagePluginName = "PNG";
                        format = PICTURE_FORMAT.png;
                    } else if (coverArtConfigurationHelper.isAcceptFormat(PICTURE_FORMAT.gif) && canWriteImageFormat("GIF")) {
                        informalImagePluginName = "GIF";
                        format = PICTURE_FORMAT.gif;
                    }
                }

                OutputStream file = null;
                try {
                    file = stream.getOutputStream().getBufferedBackend();
                    ImageIO.write(bdest, informalImagePluginName, file);
                } finally {
                    if (file != null) {
                        file.close();
                    }
                }
            }
        }
        return format;
    }

    /**
     * Convenience method to check if a given image format is supported by ImageIO
     * @param formatName
     * @return
     **/
    private static boolean canWriteImageFormat(String formatName) {
        Iterator iter = ImageIO.getImageWritersByFormatName(formatName);
        return iter.hasNext();
    }

    private TagFormatter() {
    }
}
