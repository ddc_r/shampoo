/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.parser.flac;

import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.io.parser.TagFormatter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.flac.metadatablock.BlockType;
import org.jaudiotagger.audio.flac.metadatablock.MetadataBlockDataPicture;
import org.jaudiotagger.audio.flac.metadatablock.MetadataBlockHeader;
import org.jaudiotagger.logging.ErrorMessage;
import org.jaudiotagger.tag.InvalidFrameException;
import org.jaudiotagger.tag.flac.FlacTag;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.ID3v22Tag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentReader;
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentTag;

/**
 *
 * @author okay_awright
 **/
public class FlacHeaderParser {

    protected static final Log logger;
    static {
        logger = LogFactory.getLog(FlacHeaderParser.class);
    }

    public static final int FLAC_STREAM_IDENTIFIER_LENGTH = 4;
    public static final String FLAC_STREAM_IDENTIFIER = "fLaC";
    private static final int NO_OF_BITS_IN_BYTE = 8;

    public static AudioFileInfo parseHeader(final UntypedStreamInterface stream) throws IOException, CannotReadException {

        GenericAudioHeader2 infoHeader = lookForHeader(stream);

        if (infoHeader != null) {
            return rebuildHeaderMetadata(infoHeader);
        }

        return null;
    }

    public static Tag parseTag(final CoverArtConfigurationHelper coverArtConfigurationHelper, final UntypedStreamInterface stream) throws IOException, CannotReadException {

        FlacTag infoTag = lookForTag(stream);

        if (infoTag != null) {
            return TagFormatter.rebuildTagMetadata(coverArtConfigurationHelper, infoTag);
        }

        return null;
    }

    protected static GenericAudioHeader2 lookForHeader(final UntypedStreamInterface stream) throws IOException, CannotReadException {
        FastExtendedInputStreamInterface inputStream = null;
        try {
            inputStream = stream.getInputStream();

            //Point the stream to a potential start of header
            findStream(inputStream);

            MetadataBlockDataStreamInfo2 mbdsi = null;
            boolean isLastBlock = false;

            //Search for StreamInfo Block, but even after we found it we still have to continue through all
            //the metadata blocks so that we can find the start of the audio frames which we need to calculate
            //the bitrate
            while (!isLastBlock) {
                MetadataBlockHeader2 mbh = MetadataBlockHeader2.readHeader(inputStream);
                if (mbh.getBlockType() == BlockType.STREAMINFO) {
                    mbdsi = new MetadataBlockDataStreamInfo2(mbh, inputStream);
                    if (!mbdsi.isValid()) {
                        throw new CannotReadException("FLAC StreamInfo not valid");
                    }
                } else {
                    inputStream.seek(inputStream.position() + mbh.getDataLength());
                }

                isLastBlock = mbh.isLastBlock();
                mbh = null; //Free memory
            }

            if (mbdsi == null) {
                throw new CannotReadException("Unable to find Flac StreamInfo");
            }

            GenericAudioHeader2 info = new GenericAudioHeader2(stream.getSize());
            info.setLength(mbdsi.getSongLength());
            info.setPreciseLength(mbdsi.getPreciseLength());
            info.setChannelNumber(mbdsi.getChannelNumber());
            info.setSamplingRate(mbdsi.getSamplingRate());
            info.setEncodingType(mbdsi.getEncodingType());
            info.setExtraEncodingInfos("");
            info.setBitrate(
                    Math.round(/**TODO: beware of bad upcasting**/ ((stream.getSize() - inputStream.position()) * NO_OF_BITS_IN_BYTE / mbdsi.getPreciseLength() / 1000)));
                    //(int) ((mbdsi.getPreciseLength() / 1000) * NO_OF_BITS_IN_BYTE / stream.getSize() - inputStream.position()));
            return info;

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * Reads the stream block to ensure it is a flac file
     *
     * @throws IOException
     * @throws CannotReadException
     **/
    public static int findStream(FastExtendedInputStreamInterface inputStream) throws IOException, CannotReadException {
        //Begins tag parsing
        inputStream.seek(0);

        //FLAC Stream at start
        if (isFlacHeader(inputStream)) {
            return 0;
        }

        //Ok maybe there is an ID3v24tag first, then look for v23, and finally v22
        if (isId3Tag(new ID3v24Tag(), inputStream) || isId3Tag(new ID3v23Tag(), inputStream) || isId3Tag(new ID3v22Tag(), inputStream)) {
            return NumberUtil.safeLongToInt/**TODO: beware of bad upcasting**/(inputStream.position() - FLAC_STREAM_IDENTIFIER_LENGTH);
        }

        throw new CannotReadException(ErrorMessage.FLAC_NO_FLAC_HEADER_FOUND.getMsg());
    }

    private static boolean isId3Tag(AbstractID3v2Tag id3Tag, FastExtendedInputStreamInterface inputStream) throws IOException {
        if (id3Tag != null) {
            int id3tagsize = 0;
            ByteBuffer bb = ByteBuffer.allocate(AbstractID3v2Tag.TAG_HEADER_LENGTH);
            inputStream.seek(0);
            inputStream.read(bb);
            if (id3Tag.seek(bb)) {
                //jaudiotagger didn't advertised this common method in its interface
                if (id3Tag instanceof ID3v22Tag) {
                    id3tagsize = ((ID3v22Tag) id3Tag).readSize(bb);
                } else if (id3Tag instanceof ID3v23Tag) {
                    id3tagsize = ((ID3v23Tag) id3Tag).readSize(bb);
                }
                if (id3Tag instanceof ID3v24Tag) {
                    id3tagsize = ((ID3v24Tag) id3Tag).readSize(bb);
                }
                inputStream.seek(id3tagsize);
                //FLAC Stream immediately after end of id3 tag
                if (isFlacHeader(inputStream)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean isFlacHeader(FastExtendedInputStreamInterface inputStream) throws IOException {
        //FLAC Stream at start
        byte[] b = new byte[FLAC_STREAM_IDENTIFIER_LENGTH];
        inputStream.read(b);
        String flac = new String(b);
        return flac.equals(FLAC_STREAM_IDENTIFIER);
    }

    public static FlacTag lookForTag(final UntypedStreamInterface stream) throws CannotReadException, IOException {
        VorbisCommentReader vorbisCommentReader = new VorbisCommentReader();
        FastExtendedInputStreamInterface inputStream = null;
        try {
            inputStream = stream.getInputStream();

            //Point the stream to a potential start of header
            findStream(inputStream);

            //Hold the metadata
            VorbisCommentTag tag = null;
            List<MetadataBlockDataPicture> images = new ArrayList<MetadataBlockDataPicture>();

            //Seems like we have a valid stream
            boolean isLastBlock = false;
            while (!isLastBlock) {

                //Read the header
                MetadataBlockHeader2 mbh = MetadataBlockHeader2.readHeader(inputStream);

                //Is it one containing some sort of metadata, therefore interested in it?
                switch (mbh.getBlockType()) {
                    //We got a vorbiscomment comment block, parse it
                    case VORBIS_COMMENT:
                        byte[] commentHeaderRawPacket = new byte[mbh.getDataLength()];
                        inputStream.read(commentHeaderRawPacket);
                        tag = vorbisCommentReader.read(commentHeaderRawPacket, false);
                        break;

                    case PICTURE:
                        try {
                            MetadataBlockDataPicture mbdp = getMetadataBlockDataPicture(mbh, inputStream);
                            images.add(mbdp);
                        } catch (IOException ioe) {
                            logger.debug("Unable to read picture metablock, ignoring:" + ioe.getMessage());
                        } catch (InvalidFrameException ive) {
                            logger.debug("Unable to read picture metablock, ignoring" + ive.getMessage());
                        }

                        break;

                    //This is not a metadata block we are interested in so we skip to next block
                    default:
                        inputStream.seek(inputStream.position() + mbh.getDataLength());
                        break;
                }

                isLastBlock = mbh.isLastBlock();
                mbh = null;
            }

            //Note there may not be either a tag or any images, no problem this is valid however to make it easier we
            //just initialize Flac with an empty VorbisTag
            if (tag == null) {
                tag = VorbisCommentTag.createNewTag();
            }
            FlacTag flacTag = new FlacTag(tag, images);
            return flacTag;

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private static MetadataBlockDataPicture getMetadataBlockDataPicture(MetadataBlockHeader header, FastExtendedInputStreamInterface inputStream) throws IOException, InvalidFrameException {
        ByteBuffer rawdata = ByteBuffer.allocate(header.getDataLength());
        long bytesRead = inputStream.read(rawdata);
        if (bytesRead < header.getDataLength()) {
            throw new IOException("Unable to read required number of databytes read:" + bytesRead + ":required:" + header.getDataLength());
        }
        rawdata.rewind();
        return new MetadataBlockDataPicture(rawdata);
    }

    protected static AudioFileInfo rebuildHeaderMetadata(GenericAudioHeader2 header) {
        AudioFileInfo container = new AudioFileInfo();
        //Native FLac
        container.setFormat(AUDIO_FORMAT.native_flac);
        container.setBitrate(header.getBitRateAsNumber() * 1000);
        container.setChannels((byte) header.getChannelNumber());
        container.setDuration(header.getPreciseLength());
        container.setSamplerate(header.getSampleRateAsNumber());
        container.setVbr(header.isVariableBitRate());
        container.setSize(header.getFileSize());
        return container;
    }

    private FlacHeaderParser() {
    }

}
