/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.parser.flac;

import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.jaudiotagger.audio.flac.metadatablock.BlockType;
import org.jaudiotagger.audio.flac.metadatablock.MetadataBlockHeader;

/**
 *
 * @author okay_awright
 **/
public class MetadataBlockHeader2 extends MetadataBlockHeader {

    public MetadataBlockHeader2(boolean isLastBlock, BlockType blockType, int dataLength) {
        super(isLastBlock, blockType, dataLength);
    }

    public MetadataBlockHeader2(ByteBuffer rawdata) {
        super(rawdata);
    }

    /**
     * Create header by reading from file
     *
     * @param raf
     * @return
     * @throws IOException
     **/
    public static MetadataBlockHeader2 readHeader(FastExtendedInputStreamInterface inputStream) throws IOException
    {
        ByteBuffer rawdata = ByteBuffer.allocate(HEADER_LENGTH);
        long bytesRead = inputStream.read(rawdata);
        if (bytesRead < HEADER_LENGTH)
        {
            throw new IOException("Unable to read required number of databytes read:" + bytesRead + ":required:" + HEADER_LENGTH);
        }
        rawdata.rewind();
        return new MetadataBlockHeader2(rawdata);
    }

}
