/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.parser.mp3;

import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.jaudiotagger.tag.id3.ID3v22Tag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;

/**
 *
 * @author okay_awright
 **/
public class ID3v2TagParser {

    public static final int INTEGRAL_SIZE = 4;
    /**
     * Sizes equal or smaller than this are the same whether held as sync safe integer or normal integer so
     * it doesnt matter.
     **/
    public static final int MAX_SAFE_SIZE = 127;
    static final String TYPE_HEADER = "header";
    static final String TYPE_BODY = "body";
    //Tag ID as held in file
    static final byte[] TAG_ID = {'I', 'D', '3'};
    //The tag header is the same for ID3v2 versions
    public static final int TAG_HEADER_LENGTH = 10;
    static final int FIELD_TAGID_LENGTH = 3;
    static final int FIELD_TAG_MAJOR_VERSION_LENGTH = 1;
    static final int FIELD_TAG_MINOR_VERSION_LENGTH = 1;
    static final int FIELD_TAG_FLAG_LENGTH = 1;
    static final int FIELD_TAG_SIZE_LENGTH = 4;
    static final int FIELD_TAGID_POS = 0;
    static final int FIELD_TAG_MAJOR_VERSION_POS = 3;
    static final int FIELD_TAG_MINOR_VERSION_POS = 4;
    static final int FIELD_TAG_FLAG_POS = 5;
    static final int FIELD_TAG_SIZE_POS = 6;
    static final int TAG_SIZE_INCREMENT = 100;
    //The max size we try to write in one go to avoid out of memory errors (10mb)
    //private static final long MAXIMUM_WRITABLE_CHUNK_SIZE = 10000000;

    /**
     * Checks to see if the file contains an ID3tag and if so return its size as reported in
     * the tag header  and return the size of the tag (including header), if no such tag exists return
     * zero.
     *
     * @param file
     * @return the end of the tag in the file or zero if no tag exists.
     * @throws java.io.IOException
     **/
    public static long getV2TagSizeIfExists(UntypedStreamInterface stream) throws IOException {

        FastExtendedInputStreamInterface fis = null;
        ByteBuffer bb = null;
        try {

            fis = stream.getInputStream();
            //Read possible Tag header  Byte Buffer
            bb = ByteBuffer.allocate(TAG_HEADER_LENGTH);
            fis.read(bb);
            bb.flip();
            if (bb.limit() < (TAG_HEADER_LENGTH)) {
                return 0;
            }
        } finally {
            if (fis != null) {
                fis.close();
            }
        }

        //ID3 identifier
        byte[] tagIdentifier = new byte[FIELD_TAGID_LENGTH];
        bb.get(tagIdentifier, 0, FIELD_TAGID_LENGTH);
        if (!(Arrays.equals(tagIdentifier, TAG_ID))) {
            return 0;
        }

        //Is it valid Major Version
        byte majorVersion = bb.get();
        if ((majorVersion != ID3v22Tag.MAJOR_VERSION) && (majorVersion != ID3v23Tag.MAJOR_VERSION) && (majorVersion != ID3v24Tag.MAJOR_VERSION)) {
            return 0;
        }

        //Skip Minor Version
        bb.get();

        //Skip Flags
        bb.get();

        //Get size as recorded in frame header
        int frameSize = bufferToValue(bb);

        //addField header size to frame size
        frameSize += TAG_HEADER_LENGTH;
        return frameSize;
    }

    /**
     * Read syncsafe value from buffer in format specified in spec and convert to int.
     * <p/>
     * The buffers position is moved to just after the location of the syscsafe integer
     *
     * @param buffer syncsafe integer
     * @return decoded int
     **/
    static int bufferToValue(ByteBuffer buffer) {
        byte byteBuffer[] = new byte[INTEGRAL_SIZE];
        buffer.get(byteBuffer, 0, INTEGRAL_SIZE);
        return bufferToValue(byteBuffer);
    }

    /**
     * Read syncsafe value from byteArray in format specified in spec and convert to int.
     *
     * @param buffer syncsafe integer
     * @return decoded int
     **/
    private static int bufferToValue(byte[] buffer) {
        //Note Need to && with 0xff otherwise if value is greater than 128 we get a negative number
        //when cast byte to int
        return ((buffer[0] & 0xff) << 21) + ((buffer[1] & 0xff) << 14) + ((buffer[2] & 0xff) << 7) + ((buffer[3]) & 0xff);
    }

    private ID3v2TagParser() {
    }
}
