/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.parser.mp3;

import org.jaudiotagger.audio.mp3.MPEGFrameHeader;
import org.jaudiotagger.audio.mp3.VbriFrame;
import org.jaudiotagger.audio.mp3.XingFrame;

/**
 *
 * @author okay_awright
 **/
public class MP3FrameHeader {
    /**
     * After testing the average location of the first MP3HeaderParser bit was at 5000 bytes so this is
     * why chosen as a default.
     **/
    public final static int FILE_BUFFER_SIZE = 5000;
    public final static int MIN_BUFFER_REMAINING_REQUIRED = MPEGFrameHeader.HEADER_SIZE + XingFrame.MAX_BUFFER_SIZE_NEEDED_TO_READ_XING;
    public static final int CONVERTS_BYTE_TO_BITS = 8;
    protected MPEGFrameHeader mp3FrameHeader;
    protected XingFrame mp3XingFrame;
    protected VbriFrame mp3VbriFrame;
    private long startByte = -1;
    private long fileSize = -1;

    public MP3FrameHeader(MPEGFrameHeader mp3FrameHeader, XingFrame mp3XingFrame, VbriFrame mp3VbriFrame, long startByte, long fileSize) {
        this(fileSize);
        this.mp3FrameHeader = mp3FrameHeader;
        this.mp3XingFrame = mp3XingFrame;
        this.mp3VbriFrame = mp3VbriFrame;
        this.startByte = startByte;
    }

    public MP3FrameHeader(long fileSize) {
        this.fileSize = fileSize;
    }

    public MPEGFrameHeader getMp3FrameHeader() {
        return mp3FrameHeader;
    }

    public void setMp3FrameHeader(MPEGFrameHeader mp3FrameHeader) {
        this.mp3FrameHeader = mp3FrameHeader;
    }

    public VbriFrame getMp3VbriFrame() {
        return mp3VbriFrame;
    }

    public void setMp3VbriFrame(VbriFrame mp3VbriFrame) {
        this.mp3VbriFrame = mp3VbriFrame;
    }

    public XingFrame getMp3XingFrame() {
        return mp3XingFrame;
    }

    public void setMp3XingFrame(XingFrame mp3XingFrame) {
        this.mp3XingFrame = mp3XingFrame;
    }

    public long getStartByte() {
        return startByte;
    }

    public void setStartByte(long startByte) {
        this.startByte = startByte;
    }

    /**
     * Get number of frames in this file, use Xing if exists otherwise ((File Size - Non Audio Part)/Frame Size)
     **/
    protected long getNumberOfFrames() {
        long numberOfFrames;
        if (mp3XingFrame != null && mp3XingFrame.isFrameCountEnabled()) {
            numberOfFrames = mp3XingFrame.getFrameCount();
        } else if (mp3VbriFrame != null) {
            numberOfFrames = mp3VbriFrame.getFrameCount();
        } else {
            //Estimated
            numberOfFrames = (getFileSize() - startByte) / mp3FrameHeader.getFrameLength();
        }

        return numberOfFrames;
    }

    /**
     * Get the time each frame contributes to the audio in fractions of seconds, the higher
     * the sampling rate the shorter the audio segment provided by the frame,
     * the number of samples is fixed by the MPEG Version and Layer
     **/
    public double getTimePerFrame() {
        double timePerFrame = mp3FrameHeader.getNoOfSamples() / mp3FrameHeader.getSamplingRate().doubleValue();

        //Because when calculating framelength we may have altered the calculation slightly for MPEGVersion2
        //to account for mono/stero we seem to have to make a corresponding modification to get the correct time
        if ((mp3FrameHeader.getVersion() == MPEGFrameHeader.VERSION_2) || (mp3FrameHeader.getVersion() == MPEGFrameHeader.VERSION_2_5)) {
            if ((mp3FrameHeader.getLayer() == MPEGFrameHeader.LAYER_II) || (mp3FrameHeader.getLayer() == MPEGFrameHeader.LAYER_III)) {
                if (mp3FrameHeader.getNumberOfChannels() == 1) {
                    timePerFrame /= 2;
                }
            }
        }

        return timePerFrame;
    }

    /**
     * Set bitrate in bps, if Vbr use Xingheader if possible
     **/
    protected long getAverageBitRate() {

        if (mp3XingFrame != null && mp3XingFrame.isVbr()) {
            if (mp3XingFrame.isAudioSizeEnabled() && mp3XingFrame.getAudioSize() > 0) {
                return (long) ((mp3XingFrame.getAudioSize() * CONVERTS_BYTE_TO_BITS) / (getTimePerFrame() * getNumberOfFrames()));
            } else {
                return (long) (((getFileSize() - startByte) * CONVERTS_BYTE_TO_BITS) / (getTimePerFrame() * getNumberOfFrames()));
            }
        } else if (mp3VbriFrame != null) {
            if (mp3VbriFrame.getAudioSize() > 0) {
                return (long) ((mp3VbriFrame.getAudioSize() * CONVERTS_BYTE_TO_BITS) / (getTimePerFrame() * getNumberOfFrames()));
            } else {
                return (long) (((getFileSize() - startByte) * CONVERTS_BYTE_TO_BITS) / (getTimePerFrame() * getNumberOfFrames()));
            }
        } else {
            return mp3FrameHeader.getBitRate() * 1000;
        }
    }

    public String getEncoder() {
        if (mp3XingFrame != null) {
            if (mp3XingFrame.getLameFrame() != null) {
                return mp3XingFrame.getLameFrame().getEncoder();
            }
        } else if (mp3VbriFrame != null) {
            return mp3VbriFrame.getEncoder();
        }
        return null;
    }

    /**
     * @return MPEG Version (1-3)
     **/
    public String getMpegVersion() {
        return mp3FrameHeader.getVersionAsString();
    }

    /**
     * @return MPEG Layer (1-3)
     **/
    public String getMpegLayer() {
        return mp3FrameHeader.getLayerAsString();
    }

    /**
     * @return the Channel Mode such as Stereo or Mono
     **/
    public String getChannelConstant() {
        return mp3FrameHeader.getChannelModeAsString();
    }

    /**
     * @return Emphasis
     **/
    public String getEmphasis() {
        return mp3FrameHeader.getEmphasisAsString();
    }

    /**
     * @return if the bitrate is variable, Xing header takes precedence if we have one
     **/
    protected boolean checkVariableBitRate() {
        if (mp3XingFrame != null) {
            return mp3XingFrame.isVbr();
        } else if (mp3VbriFrame != null) {
            return mp3VbriFrame.isVbr();
        } else {
            return mp3FrameHeader.isVariableBitRate();
        }
    }

    public boolean isProtected() {
        return mp3FrameHeader.isProtected();
    }

    public boolean isPrivate() {
        return mp3FrameHeader.isPrivate();
    }

    public boolean isCopyrighted() {
        return mp3FrameHeader.isCopyrighted();
    }

    public boolean isOriginal() {
        return mp3FrameHeader.isOriginal();
    }

    public boolean isPadding() {
        return mp3FrameHeader.isPadding();
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

}
