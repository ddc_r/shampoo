/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.parser.mp3;

import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.parser.TagFormatter;
import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.mp3.MPEGFrameHeader;
import org.jaudiotagger.audio.mp3.VbriFrame;
import org.jaudiotagger.audio.mp3.XingFrame;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.TagNotFoundException;
import org.jaudiotagger.tag.id3.AbstractID3v2Tag;
import org.jaudiotagger.tag.id3.ID3v11Tag;
import org.jaudiotagger.tag.id3.ID3v1Tag;
import org.jaudiotagger.tag.id3.ID3v22Tag;
import org.jaudiotagger.tag.id3.ID3v23Tag;
import org.jaudiotagger.tag.id3.ID3v24Tag;

/**
 *
 * @author okay_awright
 **/
public class MP3HeaderParser {

    static final Log logger;
    static {
        logger = LogFactory.getLog(MP3HeaderParser.class);
    }

    /**
     * Look for the first header for the beginning of the stream
     * If you know where it actually starts then use the alternative method
     * @param stream
     **/
    public static AudioFileInfo parseHeader(final UntypedStreamInterface stream) throws IOException, InvalidAudioFrameException {
        MP3FrameHeader header = getStartOfHeader(stream); //Build the container metadata
        return (header!=null?rebuildHeaderMetadata(header):null);
    }

    public static AudioFileInfo parseHeader(final UntypedStreamInterface stream, long startByte) throws IOException, InvalidAudioFrameException {
        MP3FrameHeader header = getStartOfHeader(stream, startByte); //Build the container metadata
        return (header!=null?rebuildHeaderMetadata(header):null);
    }

    static MP3FrameHeader getStartOfHeader(final UntypedStreamInterface stream) throws IOException, InvalidAudioFrameException {
        //Read ID3v2 tag size (if tag exists) to allow audioheader parsing to skip over tag
        long guessedStartByte = ID3v2TagParser.getV2TagSizeIfExists(stream);

        //If exception reading Mpeg then we should give up no point continuing
        MP3FrameHeader header = lookForHeader(stream, guessedStartByte);

        if (header!=null) {
            if (guessedStartByte != header.getStartByte()) {
                logger.debug("First header found after tag");
                header = recheckAudioStart(header, stream);
            }
            return header;
        }

        return null;
    }

    static MP3FrameHeader getStartOfHeader(final UntypedStreamInterface stream, long startByte) throws IOException, InvalidAudioFrameException {
        return lookForHeader(stream, startByte);
    }

    public static Tag parseTag(final CoverArtConfigurationHelper coverArtConfigurationHelper, final UntypedStreamInterface stream) throws IOException, CannotReadException, InvalidAudioFrameException, TagException {
        MP3FrameHeader header = getStartOfHeader(stream);
        return header!=null ? parseTag(coverArtConfigurationHelper, stream, header) : null;
    }

    public static Tag parseTag(final CoverArtConfigurationHelper coverArtConfigurationHelper, final UntypedStreamInterface stream, final MP3FrameHeader header) throws IOException, CannotReadException, TagException {

        org.jaudiotagger.tag.Tag infoTag = lookForTag(header, stream);

        if (infoTag != null) {
            return TagFormatter.rebuildTagMetadata(coverArtConfigurationHelper, infoTag);
        }

        return null;
    }

    /**
     * Returns true if the first MP3 frame can be found for the MP3 file
     * This is the first byte of music data and not the ID3 Tag Frame.
     **/
    static MP3FrameHeader lookForHeader(final UntypedStreamInterface stream, long startByte) throws IOException, InvalidAudioFrameException {

        MP3FrameHeader frameHeader = new MP3FrameHeader(stream.getSize());

        //This is substantially faster than updating the filechannels position
        long filePointerCount = startByte;
        //Read into Byte Buffer in Chunks
        ByteBuffer bb = ByteBuffer.allocateDirect(MP3FrameHeader.FILE_BUFFER_SIZE);

        boolean syncFound = false;

        FastExtendedInputStreamInterface inputStream = null;
        try {
            inputStream = stream.getInputStream();

            //Move FileChannel to the starting position (skipping over tag if any)
            inputStream.seek(startByte);

            //Read from here into the byte buffer , doesn't move location of filepointer
            inputStream.read(bb, startByte);
            bb.flip();

            try {
                do {
                    //TODO remaining() is quite an expensive operation, isn't there a way we can work this out without
                    //interrogating the bytebuffer. Also this is rarely going to be true, and could be made less true
                    //by increasing MAX_MP3_HEADER_LOCATION
                    if (bb.remaining() <= MP3FrameHeader.MIN_BUFFER_REMAINING_REQUIRED) {
                        bb.clear();
                        inputStream.seek(filePointerCount);
                        inputStream.read(bb, inputStream.position());
                        bb.flip();
                        if (bb.limit() <= MP3FrameHeader.MIN_BUFFER_REMAINING_REQUIRED) {
                            //No mp3 exists
                            return null;
                        }
                    }
                    //MP3File.logger.finest("fc:"+fc.position() + "bb"+bb.position());
                    if (MPEGFrameHeader.isMPEGFrame(bb)) {
                        try {
                            logger.debug("Found Possible header at:" + filePointerCount);

                            frameHeader.setMp3FrameHeader(MPEGFrameHeader.parseMPEGHeader(bb));
                            syncFound = true;
                            //if(2==1) use this line when you want to test getting the next frame without using xing
                            if (XingFrame.isXingFrame(bb, frameHeader.getMp3FrameHeader())) {
                                logger.debug("Found Possible XingHeader");
                                try {
                                    //Parses Xing frame without modifying position of main buffer
                                    frameHeader.setMp3XingFrame(XingFrame.parseXingFrame());
                                } catch (InvalidAudioFrameException ex) {
                                    // We Ignore because even if Xing Header is corrupted
                                    //doesn't mean file is corrupted
                                }
                                break;
                            } else if (VbriFrame.isVbriFrame(bb, frameHeader.getMp3FrameHeader())) {
                                logger.debug("Found Possible VbriHeader");
                                try {
                                    //Parses Vbri frame without modifying position of main buffer
                                    frameHeader.setMp3VbriFrame(VbriFrame.parseVBRIFrame());
                                } catch (InvalidAudioFrameException ex) {
                                    // We Ignore because even if Vbri Header is corrupted
                                    //doesn't mean file is corrupted
                                }
                                break;
                            } // There is a small but real chance that an unsynchronised ID3 Frame could fool the MPEG
                            // Parser into thinking it was an MPEG Header. If this happens the chances of the next bytes
                            // forming a Xing frame header are very remote. On the basis that  most files these days have
                            // Xing headers we do an additional check for when an apparent frame header has been found
                            // but is not followed by a Xing Header:We check the next header this wont impose a large
                            // overhead because wont apply to most Mpegs anyway ( Most likely to occur if audio
                            // has an  APIC frame which should have been unsynchronised but has not been) , or if the frame
                            // has been encoded with as Unicode LE because these have a BOM of 0xFF 0xFE
                            else {
                                syncFound = isNextFrameValid(frameHeader, inputStream, filePointerCount, bb);
                                if (syncFound) {
                                    break;
                                }
                            }

                        } catch (InvalidAudioFrameException ex) {
                            // We Ignore because likely to be incorrect sync bits ,
                            // will just continue in loop
                        }
                    }

                    //TODO position() is quite an expensive operation, isn't there a way we can work this out without
                    //interrogating the bytebuffer
                    bb.position(bb.position() + 1);
                    filePointerCount++;

                } while (!syncFound);
            } catch (IndexOutOfBoundsException ex) {
                logger.debug("Reached end of file without finding sync match: " + ex.getMessage());
                syncFound = false;
            } catch (EOFException ex) {
                logger.debug("Reached end of file without finding sync match: " + ex.getMessage());
                syncFound = false;
            } catch (IOException iox) {
                logger.debug("IOException occurred whilst trying to find sync: " + iox.getMessage());
                syncFound = false;
                throw iox;
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

        }

        //Return to start of audio header
        logger.debug("Return found matching mp3 header starting at" + filePointerCount);

        frameHeader.setStartByte(filePointerCount);

        return syncFound ? frameHeader : null;
    }

    /**
     * Called in some circumstances to check the next frame to ensure we have the correct audio header
     *
     * @param seekFile
     * @param filePointerCount
     * @param bb
     * @param fc
     * @return true if frame is valid
     * @throws java.io.IOException
     **/
    private static boolean isNextFrameValid(MP3FrameHeader frameHeader, FastExtendedInputStreamInterface stream, long filePointerCount, ByteBuffer bb) throws IOException {
        logger.debug("Checking next frame :fpc:" + filePointerCount + "skipping to:" + (filePointerCount + frameHeader.getMp3FrameHeader().getFrameLength()));
        boolean result = false;

        int currentPosition = bb.position();

        //Our buffer is not large enough to fit in the whole of this frame, something must
        //have gone wrong because frames are not this large, so just return false
        //bad frame header
        if (frameHeader.getMp3FrameHeader().getFrameLength() > (MP3FrameHeader.FILE_BUFFER_SIZE - MP3FrameHeader.MIN_BUFFER_REMAINING_REQUIRED)) {
            logger.debug("Frame size is too large to be a frame:" + frameHeader.getMp3FrameHeader().getFrameLength());
            return false;
        }

        //Check for end of buffer if not enough room get some more
        if (bb.remaining() <= MP3FrameHeader.MIN_BUFFER_REMAINING_REQUIRED + frameHeader.getMp3FrameHeader().getFrameLength()) {
            logger.debug("Buffer too small, need to reload, buffer size:" + bb.remaining());
            bb.clear();
            stream.seek(filePointerCount);
            byte[] _bb = new byte[MP3FrameHeader.FILE_BUFFER_SIZE];
            //TODO: cannot read MP3s larger than 2gb
            stream.read(_bb, NumberUtil.safeLongToInt(/**TODO: beware of bad upcasting**/ stream.position()), MP3FrameHeader.FILE_BUFFER_SIZE);
            bb = ByteBuffer.wrap(_bb);
            //So now original buffer has been replaced, so set current position to start of buffer
            currentPosition = 0;
            //Not enough left
            if (bb.limit() <= MP3FrameHeader.MIN_BUFFER_REMAINING_REQUIRED) {
                //No mp3 exists
                logger.debug("Nearly at end of file, no header found:");
                return false;
            }

            //Still Not enough left for next alleged frame size so giving up
            if (bb.limit() <= MP3FrameHeader.MIN_BUFFER_REMAINING_REQUIRED + frameHeader.getMp3FrameHeader().getFrameLength()) {
                //No mp3 exists
                logger.debug("Nearly at end of file, no room for next frame, no header found:");
                return false;
            }
        }

        //Position bb to the start of the alleged next frame
        bb.position(bb.position() + frameHeader.getMp3FrameHeader().getFrameLength());
        if (MPEGFrameHeader.isMPEGFrame(bb)) {
            try {
                MPEGFrameHeader.parseMPEGHeader(bb);
                logger.debug("Check next frame confirms is an audio header ");
                result = true;
            } catch (InvalidAudioFrameException ex) {
                logger.debug("Check next frame has identified this is not an audio header");
                result = false;
            }
        } else {
            logger.debug("isMPEGFrame has identified this is not an audio header");
        }
        //Set back to the start of the previous frame
        bb.position(currentPosition);

        return result;
    }

    static AudioFileInfo rebuildHeaderMetadata(MP3FrameHeader header) {
        AudioFileInfo container = new AudioFileInfo();
        //MPEG-1 Layer III == mp3
        container.setFormat(AUDIO_FORMAT.mp3);
        container.setBitrate(header.getAverageBitRate());
        container.setChannels((byte) header.getMp3FrameHeader().getNumberOfChannels());
        container.setDuration((float) (header.getNumberOfFrames() * header.getTimePerFrame()));
        container.setSamplerate(header.getMp3FrameHeader().getSamplingRate());
        container.setVbr(header.checkVariableBitRate());
        container.setSize(header.getFileSize());
        return container;
    }

    /**
     * Regets the audio header starting from start of file, and write appropriate logging to indicate
     * potential problem to user.
     *
     * @param startByte
     * @param currentHeader
     * @return
     * @throws IOException
     * @throws InvalidAudioFrameException
     **/
    private static MP3FrameHeader recheckAudioStart(final MP3FrameHeader currentHeader, final UntypedStreamInterface stream) throws IOException, InvalidAudioFrameException {
        MP3FrameHeader newAudioHeader;
        MP3FrameHeader nextAudioHeader;

        //because we cant agree on start location we reread the audioheader from the start of the file, at least
        //this way we cant overwrite the audio although we might overwrite part of the tag if we write this file
        //back later
        newAudioHeader = lookForHeader(stream, 0);
        logger.debug("Checking from start:" + newAudioHeader);

        //WTF? However I've encountered a few files that can't be parsed from the start, it may happen
        if (newAudioHeader==null)
            throw new InvalidAudioFrameException("Audio header appears to be highly tough to pinpoint, we give up");

        if (currentHeader.getStartByte() == newAudioHeader.getStartByte()) {
            //Although the tag size appears to be incorrect at least we have found the same location for the start
            //of audio whether we start searching from start of file or at the end of the alleged of file so no real
            //problem
            return currentHeader;
        } else {
            //We get a different value if read from start, can't guarantee 100% correct lets do some more checks

            //Frame counts dont match so eiither currentHeader or newAudioHeader isnt really audio header
            if (currentHeader.getNumberOfFrames() != newAudioHeader.getNumberOfFrames()) {
                //Skip to the next header (header 2, counting from start of file)
                nextAudioHeader = lookForHeader(stream, newAudioHeader.getStartByte() + newAudioHeader.mp3FrameHeader.getFrameLength());
                logger.debug("Checking next:" + nextAudioHeader);

                //It matches the header we found when doing the original search from after the ID3Tag therefore it
                //seems that newAudioHeader was a false match and the original header was correct
                if (nextAudioHeader.getStartByte() == currentHeader.getStartByte()) {
                    return currentHeader;
                }
                //it matches the header we just found so lends weight to the fact that the audio does indeed start at new header
                ///Not sure but safer to return earlier audio beause stops jaudiotagger overwriting when writing tag
            }
            //Same frame count so probably both audio headers with newAudioHeader being the firt one
            return newAudioHeader;
        }
    }

    static org.jaudiotagger.tag.Tag lookForTag(final MP3FrameHeader header, final UntypedStreamInterface stream) throws IOException, TagException {
        org.jaudiotagger.tag.Tag currentTag = null;

        //First check if there's an ID3v2 tag, if not present try to retrieve a v1

        //Thanks to header we know where the Actual Audio starts so load all the file from start to that point into
        //a buffer then we can read the IDv2 information without needing any more file I/O
        long startByte = header.getStartByte();
        if (startByte >= AbstractID3v2Tag.TAG_HEADER_LENGTH)
        {
logger.debug("Attempting to read id3v2tags");
            FastExtendedInputStreamInterface inputStream = null;
            ByteBuffer bb = null;
            try {
                inputStream = stream.getInputStream();
                //Read into Byte Buffer
                bb = ByteBuffer.allocate(NumberUtil.safeLongToInt(/**TODO: beware of bad upcasting**/startByte));
                inputStream.read(bb);
            }
            finally
            {
                if (inputStream != null)
                {
                    inputStream.close();
                }
            }

            bb.rewind();

                try
                {
                    currentTag = new ID3v24Tag(bb, stream.getIdentifier());
                }
                catch (TagNotFoundException ex)
                {
logger.debug("No id3v24 tag found");
                }

                try
                {
                    if (currentTag == null)
                    {
                        currentTag = new ID3v23Tag(bb, stream.getIdentifier());
                    }
                }
                catch (TagNotFoundException ex)
                {
logger.debug("No id3v23 tag found");
                }

                try
                {
                    if (currentTag == null)
                    {
                        currentTag = new ID3v22Tag(bb, stream.getIdentifier());
                    }
                }
                catch (TagNotFoundException ex)
                {
logger.debug("No id3v22 tag found");
                }

        }
        else
        {
logger.debug("Not enough room for valid id3v2 tag:" + startByte);
        }

        //Now, as a last resort, try ID3v1
        if (currentTag==null) {

            FastExtendedInputStreamInterface inputStream = null;
            ByteBuffer bb = null;
            try {
                inputStream = stream.getInputStream();
                inputStream.seek(stream.getSize() - AbstractID3v1Tag2.getTagLength());
                //Read into Byte Buffer
                bb = ByteBuffer.allocate(AbstractID3v1Tag2.getTagLength());
                inputStream.read(bb);
            }
            finally
            {
                if (inputStream != null)
                {
                    inputStream.close();
                }
            }

            bb.flip();

            try
            {
                currentTag = new ID3v11Tag();
                ((ID3v11Tag)currentTag).read(bb);
            }
            catch (TagNotFoundException ex)
            {
                currentTag = null;
logger.debug("No ids3v11 tag found");
            }

            try
            {
                if (currentTag == null)
                {
                    currentTag = new ID3v1Tag();
                    ((ID3v1Tag)currentTag).read(bb);
                }
            }
            catch (TagNotFoundException ex)
            {
                currentTag = null;
logger.debug("No id3v1 tag found");
            }

        }

        return currentTag;
    }

    private MP3HeaderParser() {
    }
}
