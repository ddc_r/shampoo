/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.parser.mp4;

import org.jaudiotagger.audio.mp4.Mp4AudioHeader;

/**
 *
 * Extended version of Mp4AudioHeader that only adds the attribute size of file
 *
 * @author okay_awright
 **/
public class MP4AudioHeader2 extends Mp4AudioHeader {

    private long fileSize = -1;

    public MP4AudioHeader2(long fileSize) {
        super();
        this.fileSize = fileSize;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

}
