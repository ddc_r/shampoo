/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.parser.mp4;

import biz.ddcr.shampoo.server.io.parser.ogg.OggHeaderParser;
import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.audio.mp4.atom.Mp4BoxHeader;

/**
 *
 * Extended version of Mp4BoxHeader that allows finding header components within an InputStream, not only a ByteBuffer or a File
 *
 * @author okay_awright
 **/
public class MP4BoxHeader2 extends Mp4BoxHeader {

    protected static final Log logger;
    static {
        logger = LogFactory.getLog(OggHeaderParser.class);
    }

    /**
     *
     * Overloaded method that handles raw stream
     *
     * Seek for box with the specified id starting from the current location of filepointer,
     * <p/>
     * Note it wont find the box if it is contained with a level below the current level, nor if we are
     * at a parent atom that also contains data and we havent yet processed the data. It will work
     * if we are at the start of a child box even if it not the required box as long as the box we are
     * looking for is the same level (or the level above in some cases).
     *
     * @param raf
     * @param id
     * @throws java.io.IOException
     * @return
     **/
    public static Mp4BoxHeader seekWithinLevel(FastExtendedInputStreamInterface stream, String id) throws IOException
    {

        logger.debug("Started searching for:" + id + " in file at:" + stream.position());

        Mp4BoxHeader boxHeader = new Mp4BoxHeader();
        ByteBuffer headerBuffer = ByteBuffer.allocate(HEADER_LENGTH);
        long bytesRead = stream.read(headerBuffer);
        if (bytesRead != HEADER_LENGTH)
        {
            return null;
        }
        headerBuffer.rewind();
        boxHeader.update(headerBuffer);
        while (!boxHeader.getId().equals(id))
        {
            logger.debug("Found:" + boxHeader.getId() + " Still searching for:" + id + " in file at:" + stream.position());

            //Something gone wrong probably not at the start of an atom so return null;
            if (boxHeader.getLength() < Mp4BoxHeader.HEADER_LENGTH)
            {
                return null;
            }
            long noOfBytesSkipped = stream.skip(boxHeader.getDataLength());
            logger.debug("Skipped:" + noOfBytesSkipped);
            if (noOfBytesSkipped < boxHeader.getDataLength())
            {
                return null;
            }
            headerBuffer.rewind();
            bytesRead = stream.read(headerBuffer);
            logger.debug("Header Bytes Read:" + bytesRead);
            headerBuffer.rewind();
            if (bytesRead == Mp4BoxHeader.HEADER_LENGTH)
            {
                boxHeader.update(headerBuffer);
            }
            else
            {
                return null;
            }
        }
        return boxHeader;
    }

}
