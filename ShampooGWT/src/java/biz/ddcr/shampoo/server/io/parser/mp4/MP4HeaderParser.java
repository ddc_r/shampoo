/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.parser.mp4;

import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.parser.TagFormatter;
import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotReadVideoException;
import org.jaudiotagger.audio.generic.Utils;
import org.jaudiotagger.audio.mp4.EncoderType;
import org.jaudiotagger.audio.mp4.Mp4NotMetaFieldKey;
import org.jaudiotagger.audio.mp4.atom.Mp4AlacBox;
import org.jaudiotagger.audio.mp4.atom.Mp4BoxHeader;
import org.jaudiotagger.audio.mp4.atom.Mp4DrmsBox;
import org.jaudiotagger.audio.mp4.atom.Mp4EsdsBox;
import org.jaudiotagger.audio.mp4.atom.Mp4FtypBox;
import org.jaudiotagger.audio.mp4.atom.Mp4MdhdBox;
import org.jaudiotagger.audio.mp4.atom.Mp4MetaBox;
import org.jaudiotagger.audio.mp4.atom.Mp4Mp4aBox;
import org.jaudiotagger.audio.mp4.atom.Mp4MvhdBox;
import org.jaudiotagger.audio.mp4.atom.Mp4StsdBox;
import org.jaudiotagger.logging.ErrorMessage;
import org.jaudiotagger.tag.TagField;
import org.jaudiotagger.tag.mp4.Mp4FieldKey;
import org.jaudiotagger.tag.mp4.Mp4NonStandardFieldKey;
import org.jaudiotagger.tag.mp4.Mp4Tag;
import org.jaudiotagger.tag.mp4.atom.Mp4DataBox;
import org.jaudiotagger.tag.mp4.field.Mp4DiscNoField;
import org.jaudiotagger.tag.mp4.field.Mp4FieldType;
import org.jaudiotagger.tag.mp4.field.Mp4GenreField;
import org.jaudiotagger.tag.mp4.field.Mp4TagBinaryField;
import org.jaudiotagger.tag.mp4.field.Mp4TagByteField;
import org.jaudiotagger.tag.mp4.field.Mp4TagCoverField;
import org.jaudiotagger.tag.mp4.field.Mp4TagRawBinaryField;
import org.jaudiotagger.tag.mp4.field.Mp4TagReverseDnsField;
import org.jaudiotagger.tag.mp4.field.Mp4TagTextField;
import org.jaudiotagger.tag.mp4.field.Mp4TagTextNumberField;
import org.jaudiotagger.tag.mp4.field.Mp4TrackField;

/**
 *
 * @author okay_awright
 **/
public class MP4HeaderParser {

    protected static final Log logger;
    static {
        logger = LogFactory.getLog(MP4HeaderParser.class);
    }

    public static AudioFileInfo parseHeader(final UntypedStreamInterface stream) throws IOException, CannotReadException {

        MP4AudioHeader2 infoHeader = lookForHeader(stream);

        if (infoHeader != null) {
            return rebuildHeaderMetadata(infoHeader);
        }

        return null;
    }

    public static Tag parseTag(final CoverArtConfigurationHelper coverArtConfigurationHelper, final UntypedStreamInterface stream) throws IOException, CannotReadException {

        Mp4Tag infoTag = lookForTag(stream);

        if (infoTag != null) {
            return TagFormatter.rebuildTagMetadata(coverArtConfigurationHelper, infoTag);
        }

        return null;
    }

    protected static MP4AudioHeader2 lookForHeader(final UntypedStreamInterface stream) throws IOException, CannotReadException {
        FastExtendedInputStreamInterface inputStream = null;
        try {
            MP4AudioHeader2 info = new MP4AudioHeader2(stream.getSize());

            inputStream = stream.getInputStream();

            //File Identification
            Mp4BoxHeader ftypHeader = MP4BoxHeader2.seekWithinLevel(inputStream, Mp4NotMetaFieldKey.FTYP.getFieldName());
            if (ftypHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_CONTAINER.getMsg());
            }
            ByteBuffer ftypBuffer = ByteBuffer.allocate(ftypHeader.getLength() - Mp4BoxHeader.HEADER_LENGTH);
            inputStream.read(ftypBuffer);
            ftypBuffer.rewind();
            Mp4FtypBox ftyp = new Mp4FtypBox(ftypHeader, ftypBuffer);
            ftyp.processData();
            info.setBrand(ftyp.getMajorBrand());

            //Get to the facts everything we are interested in is within the moov box, so just load data from file
            //once so no more file I/O needed
            Mp4BoxHeader moovHeader = MP4BoxHeader2.seekWithinLevel(inputStream, Mp4NotMetaFieldKey.MOOV.getFieldName());
            if (moovHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }
            ByteBuffer moovBuffer = ByteBuffer.allocate(moovHeader.getLength() - Mp4BoxHeader.HEADER_LENGTH);
            inputStream.read(moovBuffer);
            moovBuffer.rewind();

            //Level 2-Searching for "mvhd" somewhere within "moov", we make a slice after finding header
            //so all get() methods will be relative to mvdh positions
            Mp4BoxHeader boxHeader = Mp4BoxHeader.seekWithinLevel(moovBuffer, Mp4NotMetaFieldKey.MVHD.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }
            ByteBuffer mvhdBuffer = moovBuffer.slice();
            Mp4MvhdBox mvhd = new Mp4MvhdBox(boxHeader, mvhdBuffer);
            info.setLength(mvhd.getLength());
            //Advance position, TODO should we put this in box code ?
            mvhdBuffer.position(mvhdBuffer.position() + boxHeader.getDataLength());

            //Level 2-Searching for "trak" within "moov"
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.TRAK.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }            
            int endOfFirstTrackInBuffer = mvhdBuffer.position() + boxHeader.getDataLength();
            
            //Level 3-Searching for "mdia" within "trak"
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MDIA.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }
            //Level 4-Searching for "mdhd" within "mdia"
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MDHD.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }
            Mp4MdhdBox mdhd = new Mp4MdhdBox(boxHeader, mvhdBuffer.slice());
            info.setSamplingRate(mdhd.getSampleRate());

            //Level 4-Searching for "hdlr" within "mdia"
        /*We dont currently need to process this because contains nothing we want
            mvhdBuffer.position(mvhdBuffer.position() + boxHeader.getDataLength());
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.HDLR.getFieldName());
            if (boxHeader == null)
            {
            throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }
            Mp4HdlrBox hdlr = new Mp4HdlrBox(boxHeader, mvhdBuffer.slice());
            hdlr.processData();
             */
            //Level 4-Searching for "minf" within "mdia"
            mvhdBuffer.position(mvhdBuffer.position() + boxHeader.getDataLength());
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MINF.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }

            //Level 5-Searching for "smhd" within "minf"
            //Only an audio track would have a smhd frame
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.SMHD.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }
            mvhdBuffer.position(mvhdBuffer.position() + boxHeader.getDataLength());

            //Level 5-Searching for "stbl within "minf"
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.STBL.getFieldName());
            if (boxHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_AUDIO.getMsg());
            }

            //Level 6-Searching for "stsd within "stbl" and process it direct data, dont think these are mandatory so dont throw
            //exception if unable to find
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.STSD.getFieldName());
            if (boxHeader != null) {
                Mp4StsdBox stsd = new Mp4StsdBox(boxHeader, mvhdBuffer);
                stsd.processData();
                int positionAfterStsdHeaderAndData = mvhdBuffer.position();

                ///Level 7-Searching for "mp4a within "stsd"
                boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MP4A.getFieldName());
                if (boxHeader != null) {
                    ByteBuffer mp4aBuffer = mvhdBuffer.slice();
                    Mp4Mp4aBox mp4a = new Mp4Mp4aBox(boxHeader, mp4aBuffer);
                    mp4a.processData();
                    //Level 8-Searching for "esds" within mp4a to get No Of Channels and bitrate
                    boxHeader = Mp4BoxHeader.seekWithinLevel(mp4aBuffer, Mp4NotMetaFieldKey.ESDS.getFieldName());
                    if (boxHeader != null) {
                        Mp4EsdsBox esds = new Mp4EsdsBox(boxHeader, mp4aBuffer.slice());

                        //Set Bitrate in kbps
                        info.setBitrate(esds.getAvgBitrate() / 1000);

                        //Set Number of Channels
                        info.setChannelNumber(esds.getNumberOfChannels());

                        info.setKind(esds.getKind());
                        info.setProfile(esds.getAudioProfile());

                        info.setEncodingType(EncoderType.AAC.getDescription());
                    }
                } else {
                    //Level 7 -Searching for drms within stsd instead (m4p files)
                    mvhdBuffer.position(positionAfterStsdHeaderAndData);
                    boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.DRMS.getFieldName());
                    if (boxHeader != null) {
                        Mp4DrmsBox drms = new Mp4DrmsBox(boxHeader, mvhdBuffer);
                        drms.processData();

                        //Level 8-Searching for "esds" within drms to get No Of Channels and bitrate
                        boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.ESDS.getFieldName());
                        if (boxHeader != null) {
                            Mp4EsdsBox esds = new Mp4EsdsBox(boxHeader, mvhdBuffer.slice());

                            //Set Bitrate in kbps
                            info.setBitrate(esds.getAvgBitrate() / 1000);

                            //Set Number of Channels
                            info.setChannelNumber(esds.getNumberOfChannels());

                            info.setKind(esds.getKind());
                            info.setProfile(esds.getAudioProfile());

                            info.setEncodingType(EncoderType.DRM_AAC.getDescription());
                        }
                    } //Level 7-Searching for alac (Apple Lossless) instead
                    else {
                        mvhdBuffer.position(positionAfterStsdHeaderAndData);
                        boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.ALAC.getFieldName());
                        if (boxHeader != null) {
                            //Process First Alac
                            Mp4AlacBox alac = new Mp4AlacBox(boxHeader, mvhdBuffer);
                            alac.processData();

                            //Level 8-Searching for 2nd "alac" within box that contains the info we really want
                            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.ALAC.getFieldName());
                            if (boxHeader != null) {
                                alac = new Mp4AlacBox(boxHeader, mvhdBuffer);
                                alac.processData();
                                info.setEncodingType(EncoderType.APPLE_LOSSLESS.getDescription());
                                info.setChannelNumber(alac.getChannels());
                                info.setBitrate(alac.getBitRate() / 1000);
                            }
                        }
                    }
                }
            }
            //Set default channels if couldnt calculate it
            if (info.getChannelNumber() == -1) {
                info.setChannelNumber(2);
            }

            //Set default bitrate if couldnt calculate it
            if (info.getBitRateAsNumber() == -1) {
                info.setBitrate(128);
            }

            //This is the most likley option if cant find a match
            if (info.getEncodingType().isEmpty()) {
                info.setEncodingType(EncoderType.AAC.getDescription());
            }

            logger.debug(info.toString());

            //Level 2-Searching for another "trak" within "moov", if more than one track exists then probably
            //video so reject it, unless it fits into certain encoding patterns
            mvhdBuffer.position(endOfFirstTrackInBuffer);
            boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.TRAK.getFieldName());
            if (boxHeader != null) {
                //We only allow multiple tracks as audio if they follow the format used by the winamp encoder or
                //are marked as being audio only and if track contains a nmhd atom rather than smhd.
                //TODO this probably too restrictive but it fixes the test cases we have
                if (ftyp.getMajorBrand().equals(Mp4FtypBox.Brand.ISO14496_1_VERSION_2.getId())
                        || ftyp.getMajorBrand().equals(Mp4FtypBox.Brand.APPLE_AUDIO_ONLY.getId())
                        || ftyp.getMajorBrand().equals(Mp4FtypBox.Brand.APPLE_AUDIO.getId())) {
                    //Ok, need to do further checks on this track to ensure it is a scene descriptor
                    //Level 3-Searching for "mdia" within "trak"
                    boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MDIA.getFieldName());
                    if (boxHeader == null) {
                        throw new CannotReadVideoException(ErrorMessage.MP4_FILE_IS_VIDEO.getMsg());
                    }
                    //Level 4-Searching for "mdhd" within "mdia"
                    boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MDHD.getFieldName());
                    if (boxHeader == null) {
                        throw new CannotReadVideoException(ErrorMessage.MP4_FILE_IS_VIDEO.getMsg());
                    }
                    //Level 4-Searching for "minf" within "mdia"
                    mvhdBuffer.position(mvhdBuffer.position() + boxHeader.getDataLength());
                    boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.MINF.getFieldName());
                    if (boxHeader == null) {
                        throw new CannotReadVideoException(ErrorMessage.MP4_FILE_IS_VIDEO.getMsg());
                    }

                    //Level 5-Searching for "nmhd" within "minf"
                    //Only an audio track would have a nmhd frame
                    boxHeader = Mp4BoxHeader.seekWithinLevel(mvhdBuffer, Mp4NotMetaFieldKey.NMHD.getFieldName());
                    if (boxHeader == null) {
                        throw new CannotReadVideoException(ErrorMessage.MP4_FILE_IS_VIDEO.getMsg());
                    }
                } else {
                    logger.debug(ErrorMessage.MP4_FILE_IS_VIDEO.getMsg() + ":" + ftyp.getMajorBrand());
                    throw new CannotReadVideoException(ErrorMessage.MP4_FILE_IS_VIDEO.getMsg());
                }
            }

            //Build AtomTree to ensure it is valid, this means we can detect any problems early on
            //Mp4AtomTree atomTree = new Mp4AtomTree(raf,false);

            return info;

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    protected static AudioFileInfo rebuildHeaderMetadata(MP4AudioHeader2 header) {
        //Apple MP4 containers may contain AAC, DRMized AAC, and ALAC tracks
        //Only AAC is supported
        if (header.getEncodingType().equals(EncoderType.AAC.getDescription())) {
            AudioFileInfo container = new AudioFileInfo();
            container.setFormat(AUDIO_FORMAT.mp4_aac);
            container.setBitrate(header.getBitRateAsNumber() * 1000);
            container.setChannels((byte) header.getChannelNumber());
            container.setDuration(header.getPreciseLength());
            container.setSamplerate(header.getSampleRateAsNumber());
            container.setVbr(header.isVariableBitRate());
            container.setSize(header.getFileSize());
            return container;
        }
        return null;
    }

    protected static Mp4Tag lookForTag(final UntypedStreamInterface stream) throws IOException, CannotReadException {
        FastExtendedInputStreamInterface inputStream = null;
        try {
            Mp4Tag tag = new Mp4Tag();

            inputStream = stream.getInputStream();

            //Get to the facts everything we are interested in is within the moov box, so just load data from file
            //once so no more file I/O needed
            Mp4BoxHeader moovHeader = MP4BoxHeader2.seekWithinLevel(inputStream, Mp4NotMetaFieldKey.MOOV.getFieldName());
            if (moovHeader == null) {
                throw new CannotReadException(ErrorMessage.MP4_FILE_NOT_CONTAINER.getMsg());
            }
            ByteBuffer moovBuffer = ByteBuffer.allocate(moovHeader.getLength() - Mp4BoxHeader.HEADER_LENGTH);
            inputStream.read(moovBuffer);
            moovBuffer.rewind();

            //Level 2-Searching for "udta" within "moov"
            Mp4BoxHeader boxHeader = Mp4BoxHeader.seekWithinLevel(moovBuffer, Mp4NotMetaFieldKey.UDTA.getFieldName());
            if (boxHeader != null) {
                //Level 3-Searching for "meta" within udta
                boxHeader = Mp4BoxHeader.seekWithinLevel(moovBuffer, Mp4NotMetaFieldKey.META.getFieldName());
                if (boxHeader == null) {
                    logger.debug(ErrorMessage.MP4_FILE_HAS_NO_METADATA.getMsg());
                    return tag;
                }
                Mp4MetaBox meta = new Mp4MetaBox(boxHeader, moovBuffer);
                meta.processData();

                //Level 4- Search for "ilst" within meta
                boxHeader = Mp4BoxHeader.seekWithinLevel(moovBuffer, Mp4NotMetaFieldKey.ILST.getFieldName());
                //This file does not actually contain a tag
                if (boxHeader == null) {
                    logger.debug(ErrorMessage.MP4_FILE_HAS_NO_METADATA.getMsg());
                    return tag;
                }
            } else {
                //Level 2-Searching for "meta" not within udta
                boxHeader = Mp4BoxHeader.seekWithinLevel(moovBuffer, Mp4NotMetaFieldKey.META.getFieldName());
                if (boxHeader == null) {
                    logger.debug(ErrorMessage.MP4_FILE_HAS_NO_METADATA.getMsg());
                    return tag;
                }
                Mp4MetaBox meta = new Mp4MetaBox(boxHeader, moovBuffer);
                meta.processData();


                //Level 3- Search for "ilst" within meta
                boxHeader = Mp4BoxHeader.seekWithinLevel(moovBuffer, Mp4NotMetaFieldKey.ILST.getFieldName());
                //This file does not actually contain a tag
                if (boxHeader == null) {
                    logger.debug(ErrorMessage.MP4_FILE_HAS_NO_METADATA.getMsg());
                    return tag;
                }
            }

            //Size of metadata (exclude the size of the ilst parentHeader), take a slice starting at
            //metadata children to make things safer
            int length = boxHeader.getLength() - Mp4BoxHeader.HEADER_LENGTH;
            ByteBuffer metadataBuffer = moovBuffer.slice();
            //Datalength is longer are there boxes after ilst at this level?
            logger.debug("headerlengthsays:" + length + "datalength:" + metadataBuffer.limit());
            int read = 0;
            logger.debug("Started to read metadata fields at position is in metadata buffer:" + metadataBuffer.position());
            while (read < length) {
                //Read the boxHeader
                boxHeader.update(metadataBuffer);

                //Create the corresponding datafield from the id, and slice the buffer so position of main buffer
                //wont get affected
                logger.debug("Next position is at:" + metadataBuffer.position());
                createMp4Field(tag, boxHeader, metadataBuffer.slice());

                //Move position in buffer to the start of the next parentHeader
                metadataBuffer.position(metadataBuffer.position() + boxHeader.getDataLength());
                read += boxHeader.getLength();
            }
            return tag;

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    /**
     * Process the field and add to the tag
     * <p/>
     * Note:In the case of coverart MP4 holds all the coverart within individual dataitems all within
     * a single covr atom, we will add separate mp4field for each image.
     *
     * @param tag
     * @param header
     * @param raw
     * @return
     * @throws UnsupportedEncodingException
     **/
    private static void createMp4Field(Mp4Tag tag, Mp4BoxHeader header, ByteBuffer raw) throws UnsupportedEncodingException {
        //Reverse Dns Atom
        if (header.getId().equals(Mp4TagReverseDnsField.IDENTIFIER)) {
            
            try {
                TagField field = new Mp4TagReverseDnsField(header, raw);
                tag.addField(field);
            } catch (Exception e) {
                logger.debug(ErrorMessage.MP4_UNABLE_READ_REVERSE_DNS_FIELD.getMsg(e.getMessage()));
                TagField field = new Mp4TagRawBinaryField(header, raw);
                tag.addField(field);
            }
        } //Normal Parent with Data atom
        else {
            int currentPos = raw.position();
            boolean isDataIdentifier = Utils.getString(raw, Mp4BoxHeader.IDENTIFIER_POS, Mp4BoxHeader.IDENTIFIER_LENGTH, "ISO-8859-1").equals(Mp4DataBox.IDENTIFIER);
            raw.position(currentPos);
            if (isDataIdentifier) {
                //Need this to decide what type of Field to create
                int type = Utils.getIntBE(raw, Mp4DataBox.TYPE_POS_INCLUDING_HEADER, Mp4DataBox.TYPE_POS_INCLUDING_HEADER + Mp4DataBox.TYPE_LENGTH - 1);
                Mp4FieldType fieldType = Mp4FieldType.getFieldType(type);
                logger.debug("Box Type id:" + header.getId() + ":type:" + fieldType);

                //Special handling for some specific identifiers otherwise just base on class id
                if (header.getId().equals(Mp4FieldKey.TRACK.getFieldName())) {
                    TagField field = new Mp4TrackField(header.getId(), raw);
                    tag.addField(field);
                } else if (header.getId().equals(Mp4FieldKey.DISCNUMBER.getFieldName())) {
                    TagField field = new Mp4DiscNoField(header.getId(), raw);
                    tag.addField(field);
                } else if (header.getId().equals(Mp4FieldKey.GENRE.getFieldName())) {
                    TagField field = new Mp4GenreField(header.getId(), raw);
                    tag.addField(field);
                } else if (header.getId().equals(Mp4FieldKey.ARTWORK.getFieldName()) || Mp4FieldType.isCoverArtType(fieldType)) {
                    int processedDataSize = 0;
                    int imageCount = 0;
                    //The loop should run for each image (each data atom)
                    while (processedDataSize < header.getDataLength()) {
                        //There maybe a mixture of PNG and JPEG images so have to check type
                        //for each subimage (if there are more than one image)
                        if (imageCount > 0) {
                            type = Utils.getIntBE(raw, processedDataSize + Mp4DataBox.TYPE_POS_INCLUDING_HEADER, processedDataSize + Mp4DataBox.TYPE_POS_INCLUDING_HEADER + Mp4DataBox.TYPE_LENGTH - 1);
                            fieldType = Mp4FieldType.getFieldType(type);
                        }
                        Mp4TagCoverField field = new Mp4TagCoverField(raw, fieldType);
                        tag.addField(field);
                        processedDataSize += field.getDataAndHeaderSize();
                        imageCount++;
                    }
                } else if (fieldType == Mp4FieldType.TEXT) {
                    TagField field = new Mp4TagTextField(header.getId(), raw);
                    tag.addField(field);
                } else if (fieldType == Mp4FieldType.IMPLICIT) {
                    TagField field = new Mp4TagTextNumberField(header.getId(), raw);
                    tag.addField(field);
                } else if (fieldType == Mp4FieldType.INTEGER) {
                    TagField field = new Mp4TagByteField(header.getId(), raw);
                    tag.addField(field);
                } else {
                    boolean existingId = false;
                    for (Mp4FieldKey key : Mp4FieldKey.values()) {
                        if (key.getFieldName().equals(header.getId())) {
                            //The parentHeader is a known id but its field type is not one of the expected types so
                            //this field is invalid. i.e I received a file with the TMPO set to 15 (Oxf) when it should
                            //be 21 (ox15) so looks like somebody got their decimal and hex numbering confused
                            //So in this case best to ignore this field and just write a warning
                            existingId = true;
                            logger.debug("Known Field:" + header.getId() + " with invalid field type of:" + type + " is ignored");
                            break;
                        }
                    }

                    //Unknown field id with unknown type so just create as binary
                    if (!existingId) {
                        logger.debug("UnKnown Field:" + header.getId() + " with invalid field type of:" + type + " created as binary");
                        TagField field = new Mp4TagBinaryField(header.getId(), raw);
                        tag.addField(field);
                    }
                }
            } //Special Cases
            else {
                //MediaMonkey 3 CoverArt Attributes field, does not have data items so just
                //copy parent and child as is without modification
                //if (header.getId().equals(Mp4NonStandardFieldKey.AAPR.getFieldName())) {
                    TagField field = new Mp4TagRawBinaryField(header, raw);
                    tag.addField(field);
                /*} 
                else {
                    //Default case
                    TagField field = new Mp4TagRawBinaryField(header, raw);
                    tag.addField(field);
                }*/
            }
        }

    }

    private MP4HeaderParser() {
    }
}
