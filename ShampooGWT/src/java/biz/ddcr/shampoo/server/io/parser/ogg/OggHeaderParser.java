/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.parser.ogg;

import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.parser.TagFormatter;
import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.io.parser.flac.GenericAudioHeader2;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.generic.Utils;
import org.jaudiotagger.audio.ogg.util.OggPageHeader;
import org.jaudiotagger.audio.ogg.util.VorbisHeader;
import org.jaudiotagger.audio.ogg.util.VorbisIdentificationHeader;
import org.jaudiotagger.audio.ogg.util.VorbisPacketType;
import org.jaudiotagger.logging.ErrorMessage;
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentReader;
import org.jaudiotagger.tag.vorbiscomment.VorbisCommentTag;

/**
 *
 * @author okay_awright
 **/
public class OggHeaderParser {

    protected static final Log logger;
    static {
        logger = LogFactory.getLog(OggHeaderParser.class);
    }

    public static AudioFileInfo parseHeader(final UntypedStreamInterface stream) throws IOException, CannotReadException {

        GenericAudioHeader2 infoHeader = lookForHeader(stream);

        if (infoHeader != null) {
            return rebuildHeaderMetadata(infoHeader);
        }

        return null;
    }

    public static Tag parseTag(final CoverArtConfigurationHelper coverArtConfigurationHelper, final UntypedStreamInterface stream) throws IOException, CannotReadException {

        VorbisCommentTag infoTag = lookForTag(stream);

        if (infoTag != null) {
            return TagFormatter.rebuildTagMetadata(coverArtConfigurationHelper, infoTag);
        }

        return null;
    }

    protected static GenericAudioHeader2 lookForHeader(final UntypedStreamInterface stream) throws IOException, CannotReadException {
        FastExtendedInputStreamInterface inputStream = null;
        try {
            inputStream = stream.getInputStream();

            GenericAudioHeader2 info = new GenericAudioHeader2(stream.getSize());
            long oldPos;

            //Check start of file does it have Ogg pattern
            byte[] b = new byte[OggPageHeader.CAPTURE_PATTERN.length];
            inputStream.read(b);
            if (!(Arrays.equals(b, OggPageHeader.CAPTURE_PATTERN))) {
                throw new CannotReadException(ErrorMessage.OGG_HEADER_CANNOT_BE_FOUND.getMsg(new String(b)));
            }

            //Now work backwards from file looking for the last ogg page, it reads the granule position for this last page
            //which must be set.
            //TODO should do buffering to cut down the number of file reads
            inputStream.seek(0);
            double pcmSamplesNumber = -1;
            inputStream.seek(stream.getSize() - 2);
            while (inputStream.position() >= 4) {
                if (inputStream.read() == OggPageHeader.CAPTURE_PATTERN[3]) {
                    inputStream.seek(inputStream.position() - OggPageHeader.FIELD_CAPTURE_PATTERN_LENGTH);
                    byte[] ogg = new byte[3];
                    inputStream.read(ogg);
                    if (ogg.length == 3 && ogg[0] == OggPageHeader.CAPTURE_PATTERN[0] && ogg[1] == OggPageHeader.CAPTURE_PATTERN[1] && ogg[2] == OggPageHeader.CAPTURE_PATTERN[2]) {
                        inputStream.seek(inputStream.position() - 3);

                        oldPos = inputStream.position();
                        inputStream.seek(inputStream.position() + OggPageHeader.FIELD_PAGE_SEGMENTS_POS);
                        int pageSegments = inputStream.read() & 0xFF; //Unsigned
                        inputStream.seek(oldPos);

                        b = new byte[OggPageHeader.OGG_PAGE_HEADER_FIXED_LENGTH + pageSegments];
                        inputStream.read(b);

                        OggPageHeader pageHeader = new OggPageHeader(b);
                        inputStream.seek(0);
                        pcmSamplesNumber = pageHeader.getAbsoluteGranulePosition();
                        break;
                    }
                }
                inputStream.seek(inputStream.position() - 2);
            }

            if (pcmSamplesNumber == -1) {
                //According to spec a value of -1 indicates no packet finished on this page, this should not occur
                throw new CannotReadException(ErrorMessage.OGG_VORBIS_NO_SETUP_BLOCK.getMsg());
            }

            //1st page = Identification Header
            OggPageHeader2 pageHeader = OggPageHeader2.read(inputStream);
            byte[] vorbisData = new byte[pageHeader.getPageLength()];
            inputStream.read(vorbisData);
            VorbisIdentificationHeader vorbisIdentificationHeader = new VorbisIdentificationHeader(vorbisData);

            //Map to generic encodingInfo
            info.setPreciseLength((float) (pcmSamplesNumber / vorbisIdentificationHeader.getSamplingRate()));
            info.setChannelNumber(vorbisIdentificationHeader.getChannelNumber());
            info.setSamplingRate(vorbisIdentificationHeader.getSamplingRate());
            info.setEncodingType(vorbisIdentificationHeader.getEncodingType());
            info.setExtraEncodingInfos("");

            //TODO this calculation should be done within identification header
            if (vorbisIdentificationHeader.getNominalBitrate() != 0 && vorbisIdentificationHeader.getMaxBitrate() == vorbisIdentificationHeader.getNominalBitrate() && vorbisIdentificationHeader.getMinBitrate() == vorbisIdentificationHeader.getNominalBitrate()) {
                //CBR (in kbps)
                info.setBitrate(vorbisIdentificationHeader.getNominalBitrate() / 1000);
                info.setVariableBitRate(false);
            } else if (vorbisIdentificationHeader.getNominalBitrate() != 0 && vorbisIdentificationHeader.getMaxBitrate() == 0 && vorbisIdentificationHeader.getMinBitrate() == 0) {
                //Average vbr (in kpbs)
                info.setBitrate(vorbisIdentificationHeader.getNominalBitrate() / 1000);
                info.setVariableBitRate(true);
            } else {
                //TODO need to remove comment from raf.getLength()
                info.setBitrate(
                        NumberUtil.safeLongToInt(Math.round(/**TODO: Beware of bad upcasting**/ ((stream.getSize() / 1000.0) * 8.0 / info.getTrackLength()))));
                info.setVariableBitRate(true);
            }


            return info;

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    protected static AudioFileInfo rebuildHeaderMetadata(GenericAudioHeader2 header) {
        AudioFileInfo container = new AudioFileInfo();
        //Ogg with Vorbis
        container.setFormat(AUDIO_FORMAT.ogg_vorbis);
        container.setBitrate(header.getBitRateAsNumber() * 1000);
        container.setChannels((byte) header.getChannelNumber());
        container.setDuration(header.getPreciseLength());
        container.setSamplerate(header.getSampleRateAsNumber());
        container.setVbr(header.isVariableBitRate());
        container.setSize(header.getFileSize());
        return container;
    }

    public static VorbisCommentTag lookForTag(final UntypedStreamInterface stream) throws IOException, CannotReadException {
        FastExtendedInputStreamInterface inputStream = null;
        try {
            inputStream = stream.getInputStream();

            logger.debug("Read 1st page");
            //1st page = codec infos
            OggPageHeader pageHeader = OggPageHeader2.read(inputStream);
            //Skip over data to end of page header 1
            inputStream.seek(inputStream.position() + pageHeader.getPageLength());

            logger.debug("Read 2nd page");
            //2nd page = comment, may extend to additional pages or not , may also have setup header
            pageHeader = OggPageHeader2.read(inputStream);

            //Now at start of packets on page 2 , check this is the vorbis comment header
            byte[] b = new byte[VorbisHeader.FIELD_PACKET_TYPE_LENGTH + VorbisHeader.FIELD_CAPTURE_PATTERN_LENGTH];
            inputStream.read(b);
            if (!isVorbisCommentHeader(b)) {
                throw new CannotReadException("Cannot find comment block (no vorbiscomment header)");
            }

            //Convert the comment raw data which maybe over many pages back into raw packet
            byte[] rawVorbisCommentData = convertToVorbisCommentPacket(pageHeader, inputStream);

            //Begin tag reading
            return (new VorbisCommentReader()).read(rawVorbisCommentData, true);

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    protected static boolean isVorbisCommentHeader(byte[] headerData) {
        String vorbis = Utils.getString(headerData, VorbisHeader.FIELD_CAPTURE_PATTERN_POS, VorbisHeader.FIELD_CAPTURE_PATTERN_LENGTH, "ISO-8859-1");
        return !(headerData[VorbisHeader.FIELD_PACKET_TYPE_POS] != VorbisPacketType.COMMENT_HEADER.getType() || !vorbis.equals(VorbisHeader.CAPTURE_PATTERN));
    }

    private static byte[] convertToVorbisCommentPacket(OggPageHeader startVorbisCommentPage, FastExtendedInputStreamInterface inputStream) throws IOException, CannotReadException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] b = new byte[startVorbisCommentPage.getPacketList().get(0).getLength() - (VorbisHeader.FIELD_PACKET_TYPE_LENGTH + VorbisHeader.FIELD_CAPTURE_PATTERN_LENGTH)];
        inputStream.read(b);
        baos.write(b);

        //Because there is at least one other packet (SetupHeaderPacket) this means the Comment Packet has finished
        //on this page so thats all we need and we can return
        if (startVorbisCommentPage.getPacketList().size() > 1) {
            logger.debug("Comments finish on 2nd Page because there is another packet on this page");
            return baos.toByteArray();
        }

        //There is only the VorbisComment packet on page if it has completed on this page we can return
        if (!startVorbisCommentPage.isLastPacketIncomplete()) {
            logger.debug("Comments finish on 2nd Page because this packet is complete");
            return baos.toByteArray();
        }

        //The VorbisComment extends to the next page, so should be at end of page already
        //so carry on reading pages until we get to the end of comment
        while (true) {
            logger.debug("Reading next page");
            OggPageHeader nextPageHeader = OggPageHeader2.read(inputStream);
            b = new byte[nextPageHeader.getPacketList().get(0).getLength()];
            inputStream.read(b);
            baos.write(b);

            //Because there is at least one other packet (SetupHeaderPacket) this means the Comment Packet has finished
            //on this page so thats all we need and we can return
            if (nextPageHeader.getPacketList().size() > 1) {
                logger.debug("Comments finish on Page because there is another packet on this page");
                return baos.toByteArray();
            }

            //There is only the VorbisComment packet on page if it has completed on this page we can return
            if (!nextPageHeader.isLastPacketIncomplete()) {
                logger.debug("Comments finish on Page because this packet is complete");
                return baos.toByteArray();
            }
        }
    }

    private OggHeaderParser() {
    }
}
