/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.parser.ogg;

import biz.ddcr.shampoo.server.io.util.FastExtendedInputStreamInterface;
import java.io.IOException;
import java.util.Arrays;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.ogg.util.OggPageHeader;
import org.jaudiotagger.logging.ErrorMessage;

/**
 *
 * @author okay_awright
 **/
public class OggPageHeader2 extends OggPageHeader {

    public OggPageHeader2(byte[] b) {
        super(b);
    }

     /**
     * Read next PageHeader from file
     * @param raf
     * @return
     * @throws IOException
     * @throws CannotReadException
     **/
    public static OggPageHeader2 read(FastExtendedInputStreamInterface inputStream) throws IOException, CannotReadException
    {
        long start = inputStream.position();
        logger.fine("Trying to read OggPage at:" + start);

        byte[] b = new byte[OggPageHeader.CAPTURE_PATTERN.length];
        inputStream.read(b);
        if (!(Arrays.equals(b, OggPageHeader.CAPTURE_PATTERN)))
        {
            throw new CannotReadException(ErrorMessage.OGG_HEADER_CANNOT_BE_FOUND.getMsg(new String(b)));
        }

        inputStream.seek(start + OggPageHeader.FIELD_PAGE_SEGMENTS_POS);
        int pageSegments = inputStream.read() & 0xFF; //unsigned
        inputStream.seek(start);

        b = new byte[OggPageHeader.OGG_PAGE_HEADER_FIXED_LENGTH + pageSegments];
        inputStream.read(b);


        OggPageHeader2 pageHeader = new OggPageHeader2(b);

        //Now just after PageHeader, ready for Packet Data
        return pageHeader;
    }

}
