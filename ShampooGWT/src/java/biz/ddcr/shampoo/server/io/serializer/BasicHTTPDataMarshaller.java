/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import biz.ddcr.shampoo.server.io.serializer.MarshallableInterface.MarshalledChunk;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class BasicHTTPDataMarshaller<T extends MarshallableInterface> extends GenericHTTPMarshaller<T> {

    public interface BasicDataMarshalledChunk extends MarshalledChunk {

        public StringBuilder toCSV();

        public StringBuilder toJSON();

        public StringBuilder toXML();

        /**
         * Supports Liquidsoap 0.91 abstract "Requests" Write metadata through
         * the Annotate text protocol. See
         * http://savonet.sourceforge.net/doc-svn/requests.html
         */
        public StringBuilder toLiquidsoapAnnotate();

        public StringBuilder toLiquidsoapJSON();
    }
    protected ExtendedHttpMessageConverter<T> csvSerializer;
    protected ExtendedHttpMessageConverter<T> jsonSerializer;
    protected ExtendedHttpMessageConverter<T> xmlSerializer;
    protected ExtendedHttpMessageConverter<T> liquidsoapAnnotateSerializer;
    protected ExtendedHttpMessageConverter<T> liquidsoapJSONSerializer;

    public ExtendedHttpMessageConverter<T> getCsvSerializer() {
        return csvSerializer;
    }

    public void setCsvSerializer(ExtendedHttpMessageConverter<T> csvSerializer) {
        this.csvSerializer = csvSerializer;
    }

    public ExtendedHttpMessageConverter<T> getJsonSerializer() {
        return jsonSerializer;
    }

    public void setJsonSerializer(ExtendedHttpMessageConverter<T> jsonSerializer) {
        this.jsonSerializer = jsonSerializer;
    }

    public ExtendedHttpMessageConverter<T> getXmlSerializer() {
        return xmlSerializer;
    }

    public void setXmlSerializer(ExtendedHttpMessageConverter<T> xmlSerializer) {
        this.xmlSerializer = xmlSerializer;
    }

    public ExtendedHttpMessageConverter<T> getLiquidsoapAnnotateSerializer() {
        return liquidsoapAnnotateSerializer;
    }

    public void setLiquidsoapAnnotateSerializer(ExtendedHttpMessageConverter<T> liquidsoapAnnotateSerializer) {
        this.liquidsoapAnnotateSerializer = liquidsoapAnnotateSerializer;
    }

    public ExtendedHttpMessageConverter<T> getLiquidsoapJSONSerializer() {
        return liquidsoapJSONSerializer;
    }

    public void setLiquidsoapJSONSerializer(ExtendedHttpMessageConverter<T> liquidsoapJSONSerializer) {
        this.liquidsoapJSONSerializer = liquidsoapJSONSerializer;
    }

    @Override
    protected ExtendedHttpMessageConverter<T> findConverter(GenericStreamableMetadataContainerInterface<T> object) {
        ExtendedHttpMessageConverter<T> currentSerializer = null;
        if (object.getFormat() != null) {
            switch (object.getFormat()) {
                case liquidsoapJSON:
                    currentSerializer = liquidsoapJSONSerializer;
                    break;
                case liquidsoapAnnotate:
                    currentSerializer = liquidsoapAnnotateSerializer;
                    break;
                case genericJSON:
                    currentSerializer = jsonSerializer;
                    break;
                case genericCSV:
                    currentSerializer = csvSerializer;
                    break;
                default:
                    currentSerializer = xmlSerializer;
                    break;
            }
        }
        if (currentSerializer == null && xmlSerializer != null) {
            currentSerializer = xmlSerializer;
        }
        return currentSerializer;
    }
}
