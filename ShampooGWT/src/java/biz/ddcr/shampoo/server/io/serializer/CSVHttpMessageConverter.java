/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import biz.ddcr.shampoo.server.domain.streamer.metadata.*;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class CSVHttpMessageConverter<T extends MarshallableInterface> extends AbstractHttpMessageConverter<T> {

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private final List<Charset> availableCharsets;
    //private boolean showHeader = true;

    public CSVHttpMessageConverter() {
        super();
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(new MediaType("text", "csv"));
        setSupportedMediaTypes(mediaTypes);
        (this.availableCharsets = new ArrayList<Charset>()).add(DEFAULT_CHARSET);
    }

    public CSVHttpMessageConverter(boolean showHeader) {
        this();
        //this.showHeader = showHeader;
    }

    @Override
    public boolean supports(Class clazz) {
        return StreamableChannelMetadataInterface.class.isAssignableFrom(clazz);
    }

    @Override
    public boolean canRead(Class type, MediaType mt) {
        //Not supported yet
        return false;
    }

    @Override
    public boolean canWrite(Class type, MediaType mt) {
        //We don't care about the media type
        return supports(type);
    }

    @Override
    protected T readInternal(Class<? extends T> type, HttpInputMessage him) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public void writeInternal(T t, final HttpOutputMessage hom) throws IOException, HttpMessageNotWritableException {
        if (t != null) {

            hom.getHeaders().setAcceptCharset(getAcceptedCharsets());
            MediaType contentType = hom.getHeaders().getContentType();
            final Charset charset = contentType.getCharSet() != null ? contentType.getCharSet() : DEFAULT_CHARSET;
            final OutputStreamWriter out = new OutputStreamWriter(hom.getBody(), charset);

            MarshallableSupporter supporter = new MarshallableSupporter() {

                @Override
                public void support(SerializableIterableMetadataInterface marshallableMetadata) throws Exception {
                    //Individually append each item for this collection by calling the other support() method
                    final Iterator it = marshallableMetadata.iterator();
                    if (it.hasNext()) {
                        support((SerializableMetadataInterface) it.next());
                        while (it.hasNext()) {
                            //Carriage return + line feed (HTTP 1.1 compliant) in order to separate entries
                            out.write("\r\n");
                            support((SerializableMetadataInterface) it.next());
                        }                        
                    }
                }

                @Override
                public void support(SerializableMetadataInterface marshallableMetadata) throws Exception {
                    out.write(marshallableMetadata.marshall(true).toCSV().toString());
                }
            };            
            
            try {
                
                t.supports(supporter);
                
            } catch (IOException ex) {
                throw ex;
            } catch (Exception ex) {
                throw new HttpMessageNotWritableException("Stream cannot be written", ex);
            } finally {
                out.close();
            }
        }
    }

    protected List<Charset> getAcceptedCharsets() {
        return this.availableCharsets;
    }
}
