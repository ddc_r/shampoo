/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

/**
 * Provides file extension for Content-Disposition to HttpMessageConverter
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ExtendedHttpMessageConverter<T> implements HttpMessageConverter<T>, Serializable { 
    private HttpMessageConverter<T> converter;
    private String fileExtension;

    public ExtendedHttpMessageConverter(HttpMessageConverter<T> converter) {
        this.converter = converter;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return converter.getSupportedMediaTypes();
    }

    @Override
    public boolean canWrite(Class<?> type, MediaType mt) {
        return converter.canWrite(type, mt);
    }

    @Override
    public boolean canRead(Class<?> type, MediaType mt) {
        return converter.canRead(type, mt);
    }

    @Override
    public void write(T t, MediaType mt, HttpOutputMessage hom) throws IOException, HttpMessageNotWritableException {
        converter.write(t, mt, hom);
    }

    @Override
    public T read(Class<? extends T> type, HttpInputMessage him) throws IOException, HttpMessageNotReadableException {
        return converter.read(type, him);
    }

}
