/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import biz.ddcr.shampoo.client.helper.errors.DownloadingCacheUnchangedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingPreconditionFailedException;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.frontend.DownloadFileHandler;
import static biz.ddcr.shampoo.server.io.frontend.DownloadFileHandler.matches;
import biz.ddcr.shampoo.server.service.GenericService;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.server.ServletServerHttpResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class GenericHTTPMarshaller<T extends MarshallableInterface> extends GenericService {

    protected abstract ExtendedHttpMessageConverter<T> findConverter(GenericStreamableMetadataContainerInterface<T> object);

    private SystemConfigurationHelper systemConfigurationHelper;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    private String extractLastFragment(String urlString) {
        int i = urlString != null ? urlString.lastIndexOf('/') : -1;
        return (i > -1) ? urlString.substring(i + 1) : UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
    }

    public void marshallAsAttachment(GenericStreamableMetadataContainerInterface<T> object, HttpServletRequest request, HttpServletResponse response) throws Exception {
        _marshall(object, request, response, true);
    }

    public void marshall(GenericStreamableMetadataContainerInterface<T> object, HttpServletRequest request, HttpServletResponse response) throws Exception {
        _marshall(object, request, response, false);
    }

    protected void _marshall(GenericStreamableMetadataContainerInterface<T> object, HttpServletRequest request, HttpServletResponse response, boolean asAttachment) throws Exception {
        if (object != null && response != null) {

            ExtendedHttpMessageConverter<T> currentSerializer = findConverter(object);
            if (currentSerializer == null) {
                throw new IOException("No default marshaller is available");
            }

            final T item = object.getItem();

            //Check whether we should use HTTP caching
            if (getSystemConfigurationHelper().isHttpETagGenerationEnabled()) {
                checkHTTPCaching(item, request, response);
            }

            //Make it an attachment if needed
            if (asAttachment) {
                response.setHeader("Content-Disposition", "attachment; filename=" + extractLastFragment(request.getServletPath()) + (currentSerializer.getFileExtension() != null && currentSerializer.getFileExtension().length() != 0 ? "." + currentSerializer.getFileExtension() : ""));
            }

            //Check whether we should compress the output and marshall it
            String acceptEncoding = request.getHeader("Accept-Encoding");
            if (acceptEncoding != null && DownloadFileHandler.accepts(acceptEncoding, "gzip")) {
                (new MarshallerGzipper<T>(currentSerializer)).write(item, null, new ServletServerHttpResponse(response));
            } else if (acceptEncoding != null && DownloadFileHandler.accepts(acceptEncoding, "deflate")) {
                (new MarshallerDeflater<T>(currentSerializer)).write(item, null, new ServletServerHttpResponse(response));
            } else {
                currentSerializer.write(item, null, new ServletServerHttpResponse(response));
            }

        }
    }

    /**
     * throws exception if the HTTP caching detection mechanism sees that
     * delivering the content is not needed
     *
     * @param item
     * @param request
     * @param response
     * @throws Exception
     */
    protected void checkHTTPCaching(final T item, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

        if (item!=null && request!=null && response!=null) {
            
            //ONly ETag is implemented
            final int fullHashCode = item.fullHashCode();
            final String eTag = fullHashCode != 0 ? '"' + String.valueOf(fullHashCode) + '"' : null;
            if (eTag != null) {

            // Validate request headers for caching ---------------------------------------------------
                // If-None-Match header should contain "*" or ETag. If so, then return 304.        
                final String ifNoneMatch = request.getHeader("If-None-Match");
                if (ifNoneMatch != null && matches(ifNoneMatch, eTag)) {
                    //eTag header required in response during 304s
                    response.setHeader("ETag", eTag);
                    throw new DownloadingCacheUnchangedException(eTag);
                }
                // Validate request headers for resume ----------------------------------------------------
                // If-Match header should contain "*" or ETag. If not, then return 412.
                if (eTag != null) {
                    String ifMatch = request.getHeader("If-Match");
                    if (ifMatch != null && !matches(ifMatch, eTag)) {
                        throw new DownloadingPreconditionFailedException(eTag);
                    }
                }

                response.setHeader("ETag", eTag);
            }
        }
    }
}
