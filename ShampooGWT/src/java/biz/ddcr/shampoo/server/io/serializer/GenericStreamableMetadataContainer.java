/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class GenericStreamableMetadataContainer<T extends MarshallableInterface> implements GenericStreamableMetadataContainerInterface<T> {

    private SERIALIZATION_METADATA_FORMAT format;
    private T item;

    public GenericStreamableMetadataContainer(SERIALIZATION_METADATA_FORMAT format, T item) {
        this.format = format;
        this.item = item;
    }

    @Override
    public SERIALIZATION_METADATA_FORMAT getFormat() {
        return format;
    }

    @Override
    public T getItem() {
        return item;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GenericStreamableMetadataContainer<T> other = (GenericStreamableMetadataContainer<T>) obj;
        if (this.item != other.item && (this.item == null || !this.item.equals(other.item))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.item != null ? this.item.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean isCacheable() {
        return true;
    }       
}
