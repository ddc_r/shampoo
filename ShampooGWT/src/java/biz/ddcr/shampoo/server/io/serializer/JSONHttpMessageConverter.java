/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import biz.ddcr.shampoo.server.domain.streamer.metadata.*;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class JSONHttpMessageConverter<T extends MarshallableInterface> extends AbstractHttpMessageConverter<T> {

    public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private final List<Charset> availableCharsets;

    public JSONHttpMessageConverter() {
        super();
        List<MediaType> mediaTypes = new ArrayList<MediaType>();
        mediaTypes.add(new MediaType("application", "json"));
        setSupportedMediaTypes(mediaTypes);
        (this.availableCharsets = new ArrayList<Charset>()).add(DEFAULT_CHARSET);
    }

    @Override
    public boolean supports(Class clazz) {
        return StreamableChannelMetadataInterface.class.isAssignableFrom(clazz);
    }

    @Override
    public boolean canRead(Class type, MediaType mt) {
        //Not supported yet
        return false;
    }

    @Override
    public boolean canWrite(Class type, MediaType mt) {
        //We don't care about the media type
        return supports(type);
    }

    @Override
    protected T readInternal(Class<? extends T> type, HttpInputMessage him) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException("Not supported yet");
    }

    /**
     * Supports a special brand of JSON, especially tuned for Liquidsoap 1.0
     * beta 3 All values are Strings, it's then Liquidsoap's responsibility to
     * cast them (limitation in Liquidsoap's implementation of lists in scripts)
     * No child objects, or arrays are allowed By default the output is minified
     *
     * @param t
     * @param mt
     * @param hom
     * @throws IOException
     * @throws HttpMessageNotWritableException
     */
    @Override
    public void writeInternal(T t, final HttpOutputMessage hom) throws IOException, HttpMessageNotWritableException {
        if (t != null) {

            hom.getHeaders().setAcceptCharset(getAcceptedCharsets());
            MediaType contentType = hom.getHeaders().getContentType();
            final Charset charset = contentType.getCharSet() != null ? contentType.getCharSet() : DEFAULT_CHARSET;
            final OutputStreamWriter out = new OutputStreamWriter(hom.getBody(), charset);

            MarshallableSupporter supporter = new MarshallableSupporter() {
                @Override
                public void support(SerializableIterableMetadataInterface marshallableMetadata) throws Exception {
                    //SerializableCollection opening bracket
                    out.write("[");
                    //Individually append each item for this collection by calling the other support() method
                    final Iterator it = marshallableMetadata.iterator();
                    if (it.hasNext()) {
                        support((SerializableMetadataInterface) it.next());
                        while (it.hasNext()) {
                            //Next item separator
                            out.write(",");
                            support((SerializableMetadataInterface) it.next());
                        }                        
                    }
                    //SerializableCollection closing bracket
                    out.write("]");
                }

                @Override
                public void support(SerializableMetadataInterface marshallableMetadata) throws Exception {                    
                    out.write(marshallableMetadata.marshall(false).toJSON().toString());
                }
            };

            try {

                t.supports(supporter);

            } catch (IOException ex) {
                throw ex;
            } catch (Exception ex) {
                throw new HttpMessageNotWritableException("Stream cannot be written", ex);
            } finally {
                out.close();
            }
        }
    }

    protected List<Charset> getAcceptedCharsets() {
        return this.availableCharsets;
    }
}
