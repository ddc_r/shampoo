/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;

public abstract class MarshallerCompressor<T extends MarshallableInterface> implements HttpMessageConverter<T> {

    protected class SimpleHttpOutputMessage implements HttpOutputMessage {

        private OutputStream stream;
        private HttpHeaders headers;

        SimpleHttpOutputMessage(OutputStream stream, HttpHeaders headers) {
            super();
            this.stream = stream;
            this.headers = headers;
        }

        @Override
        public OutputStream getBody() throws IOException {
            return stream;
        }

        @Override
        public HttpHeaders getHeaders() {
            return headers;
        }
    }
    private HttpMessageConverter<T> wrappedConverter;

    public void setWrappedConverter(HttpMessageConverter<T> wrappedConverter) {
        this.wrappedConverter = wrappedConverter;
    }

    public HttpMessageConverter<T> getWrappedConverter() {
        return wrappedConverter;
    }

    public MarshallerCompressor(HttpMessageConverter<T> wrappedConverter) {
        super();
        this.wrappedConverter = wrappedConverter;
    }

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return wrappedConverter.canRead(clazz, mediaType);
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return wrappedConverter.canWrite(clazz, mediaType);
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return wrappedConverter.getSupportedMediaTypes();
    }

    @Override
    public T read(Class<? extends T> type, HttpInputMessage him) throws IOException, HttpMessageNotReadableException {
        throw new UnsupportedOperationException("Not supported yet");
    }

}
