/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.io.serializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class MarshallerDeflater<T extends MarshallableInterface> extends MarshallerCompressor<T> {

    private static final String DEFLATE = "deflate";

    public MarshallerDeflater(HttpMessageConverter<T> wrappedConverter) {
        super(wrappedConverter);
    }

    @Override
    public void write(T t, MediaType contentType, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        final ByteArrayOutputStream compressed = new ByteArrayOutputStream();
        final OutputStream zout = new DeflaterOutputStream(compressed, new Deflater(Deflater.DEFAULT_STRATEGY));

        HttpHeaders headers = outputMessage.getHeaders();
        headers.add("Content-Encoding", DEFLATE);
        headers.add("Vary", "Accept-Encoding");

        try {
            HttpOutputMessage reoutputMsg = new SimpleHttpOutputMessage(zout, headers);

            getWrappedConverter().write(t, contentType, reoutputMsg);
        } finally {
            zout.close();
        }

        try {
            outputMessage.getBody().write(compressed.toByteArray());
        } finally {
            compressed.close();
        }
    }

}
