/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

/**
 *
 * Marshalling formats that Shampoo handles
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public enum SERIALIZATION_METADATA_FORMAT {

    liquidsoapJSON("LiquidsoapJSON"),
    liquidsoapAnnotate("liquidsoapAnnotate"),
    genericJSON("JSON"),
    genericXML("XML"),
    genericCSV("CSV");
    
    protected final String friendlyName;

    SERIALIZATION_METADATA_FORMAT(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getRawName() {
        return friendlyName;
    }

    /**
     * Map a SERIALIZATION_METADATA_FORMAT from an HTTP parameter value-like
     *
     * @param rawValue
     * @return
     */
    public static SERIALIZATION_METADATA_FORMAT mapFromFriendlyName(String rawValue) {
        if (rawValue != null && rawValue.length() != 0) {
            try {
                if (liquidsoapJSON.getRawName().equalsIgnoreCase(rawValue)) {
                    return liquidsoapJSON;
                } else if (liquidsoapAnnotate.getRawName().equalsIgnoreCase(rawValue)) {
                    return liquidsoapAnnotate;
                } else if (genericJSON.getRawName().equalsIgnoreCase(rawValue)) {
                    return genericJSON;
                } else if (genericXML.getRawName().equalsIgnoreCase(rawValue)) {
                    return genericXML;
                } else if (genericCSV.getRawName().equalsIgnoreCase(rawValue)) {
                    return genericCSV;
                }
            } catch (IllegalArgumentException e) {
                //Silently fails
            }
        }
        return null;
    }
}
