/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import java.util.*;

/**
 * A collection that is guaranteed to be serializable
 * Use a real implementation that is serializable via composition and avoid copying large datasets as a result
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SerializableMetadataCollection<T extends SerializableMetadataInterface> implements SerializableIterableMetadataInterface<T> {
    private Collection<T> internalCollection;

    protected SerializableMetadataCollection() {
        //For serialization purposes; Castor wants the c'tor to remain public, unless verify-constructable is set to false
    }

    /** known serializable collection */
    protected SerializableMetadataCollection(ArrayList<T> internalCollection) {
        setInternalCollection(internalCollection);
    }
    /** known serializable collection */
    protected SerializableMetadataCollection(HashSet<T> internalCollection) {
        setInternalCollection(internalCollection);
    }
    /** known serializable collection */
    protected SerializableMetadataCollection(TreeSet<T> internalCollection) {
        setInternalCollection(internalCollection);
    }

    /** known serializable collection */
    void setInternalCollection(ArrayList<T> internalCollection) {
        this.internalCollection = internalCollection;
    }
    /** known serializable collection */
    void setInternalCollection(HashSet<T> internalCollection) {
        this.internalCollection = internalCollection;
    }
    /** known serializable collection */
    void setInternalCollection(TreeSet<T> internalCollection) {
        this.internalCollection = internalCollection;
    }

    /** Castor requires it to be public, that's a shame*/
    public Collection<T> getItems() {
        if (internalCollection==null) internalCollection = new HashSet<T>();
        return internalCollection;
    }
    
    public int getSize() {
        return getItems().size();
    }   
    
    @Override
    public String toString() {
        return getItems().toString();
    }

    public <T> T[] toArray(T[] a) {
        return getItems().toArray(a);
    }

    public Object[] toArray() {
        return getItems().toArray();
    }

    public int size() {
        return getItems().size();
    }

    public boolean retainAll(Collection<?> c) {
        return getItems().retainAll(c);
    }

    public boolean removeAll(Collection<?> c) {
        return getItems().removeAll(c);
    }

    public boolean remove(Object o) {
        return getItems().remove(o);
    }

    @Override
    public Iterator<T> iterator() {
        return getItems().iterator();
    }

    public boolean isEmpty() {
        return getItems().isEmpty();
    }

    @Override
    public int hashCode() {
        return getItems().hashCode();
    }

    @Override
    public int fullHashCode() {
        int hash = 7;
        for (T t : this)
            hash = 97 * hash + (t != null ? t.fullHashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        return getItems().equals(o);
    }

    public boolean containsAll(Collection<?> c) {
        return getItems().containsAll(c);
    }

    public boolean contains(Object o) {
        return getItems().contains(o);
    }

    public void clear() {
        getItems().clear();
    }

    public boolean addAll(Collection<? extends T> c) {
        return getItems().addAll(c);
    }

    public boolean add(T e) {
        return getItems().add(e);
    }
    
    public static <U extends SerializableMetadataInterface> SerializableMetadataCollection<U> wrap(Collection<U> implementationCollection) {
        if (implementationCollection!=null) {
            if (implementationCollection instanceof HashSet) {
                return new SerializableMetadataCollection<U>((HashSet<U>)implementationCollection);
            } else if (implementationCollection instanceof ArrayList) {
                return new SerializableMetadataCollection<U>((ArrayList<U>)implementationCollection);
            } else if (implementationCollection instanceof TreeSet) {
                return new SerializableMetadataCollection<U>((TreeSet<U>)implementationCollection);
            } else if (implementationCollection instanceof LinkedHashSet) {
                return new SerializableMetadataCollection<U>((LinkedHashSet<U>)implementationCollection);
            }
            //No other collection is handled, yet            
            throw new UnsupportedOperationException("Serializable collection wrapping not supported yet");
        }
        return null;
    }

    @Override
    public void supports(MarshallableSupporter otherObject) throws Exception {
        otherObject.support(this);
    }

}
