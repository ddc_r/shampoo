/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.io.serializer;

import biz.ddcr.shampoo.server.domain.VisitorPatternInterface.Visitor;
import biz.ddcr.shampoo.server.domain.archive.metadata.SimpleStreamableArchiveMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableBlankQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableChannelGenericMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableLiveGenericItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableLiveQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableTrackQueueItemMetadata;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface SerializableMetadataVisitor extends Visitor {

    public void visit(SimpleStreamableLiveQueueItemMetadata streamableItemMetadata) throws Exception;

    public void visit(SimpleStreamableTrackQueueItemMetadata streamableItemMetadata) throws Exception;
    
    public void visit(SimpleStreamableLiveGenericItemMetadata streamableItemMetadata) throws Exception;
    
    public void visit(SimpleStreamableBlankQueueItemMetadata streamableItemMetadata) throws Exception;
    
    public void visit(SimpleStreamableChannelGenericMetadata streamableItemMetadata) throws Exception;
    
    public void visit(SimpleStreamableArchiveMetadata streamableItemMetadata) throws Exception;

}
