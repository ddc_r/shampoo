/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.serializer;

import biz.ddcr.shampoo.server.io.serializer.SupportInterface.Supported;

/**
 * Alternative visitor pattern. Can be safely used along with VisitorPatternInterface.
 * Currently only reserved to marshallable items.
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface SupportInterface<T extends Supported> {
    /**
     * Alternative visitor pattern
     */
    public void supports(T otherObject) throws Exception;
    
    public interface Supported{}
    
}
