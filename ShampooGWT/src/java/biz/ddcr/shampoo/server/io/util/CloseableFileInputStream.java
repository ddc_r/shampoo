/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author okay_awright
 **/
public class CloseableFileInputStream extends InputStream implements FastExtendedInputStreamInterface {

    protected final Log logger = LogFactory.getLog(getClass());
    private final FileChannel _channel;
    private transient long markPosition = -1;
    private transient final ByteBuffer _byteBuffer;

    public CloseableFileInputStream(File file) throws IOException {
        this._channel = (new RandomAccessFile(file, "r")).getChannel();
        //Try to acquire a shared lock on the file, otherwise throw an exception
        this._byteBuffer = ByteBuffer.allocateDirect(IOUtil.DEFAULT_BUFFER_SIZE);
    }

    @Override
    public void close() throws IOException {
        _channel.close();
    }

    @Override
    public boolean isClosed() {
        return _channel == null || !_channel.isOpen();
    }

    @Override
    public long fastCopyTo(long start, long length, OutputStream output) throws IOException {
        //return _file.getChannel().transferTo(length, length, Channels.newChannel(output));
        return fastCopyTo(start, length, Channels.newChannel(output));
    }

    @Override
    public synchronized long fastCopyTo(long start, long length, WritableByteChannel output) throws IOException {

        //WORKAROUND: copying large files on some OSes, including Windows and old SunOS
        //Magic number for Windows = 64Mb - 32Kb
        int maxBufferSize = (64 * 1024 * 1024) - (32 * 1024);
        int bufferSize = NumberUtil.safeLongToInt( Math.min(
                length,
                Math.min(IOUtil.DEFAULT_BUFFER_SIZE, maxBufferSize)) );
        long totalBytesCopied = 0;
        long bytesCopiedInPass = 0;

        //Do the copy via native transferTo()
        do {
            bytesCopiedInPass = _channel.transferTo(
                    start + totalBytesCopied,
                    bufferSize,
                    output);
            totalBytesCopied += bytesCopiedInPass;
        } while (totalBytesCopied < length && bytesCopiedInPass > 0);

        return totalBytesCopied;
    }

    @Override
    public int available() throws IOException {
        return NumberUtil.safeLongToInt(_channel.size() - position());
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public int read(byte[] bytes) throws IOException {
        //return _file.read(b);
        return read(bytes, NumberUtil.safeLongToInt(position()), bytes.length);
    }

    @Override
    public int read(byte[] bytes, int offset, int length) throws IOException {

        if (offset != _channel.position()) {
            _channel.position(offset);
        }

        synchronized (_byteBuffer) {
            _byteBuffer.position(0);
            _byteBuffer.limit(Math.min(_byteBuffer.capacity(), length));

            int nbRead = _channel.read(_byteBuffer);
            if (nbRead <= 0) {
                return nbRead;
            }

            _byteBuffer.position(0);
            _byteBuffer.limit(nbRead);
            _byteBuffer.get(bytes);

            return nbRead;
        }
    }

    @Override
    public int read() throws IOException {
        //return _file.read();
        synchronized (_byteBuffer) {
            _byteBuffer.position(0);
            _byteBuffer.limit(1);

            int nbRead = _channel.read(_byteBuffer);
            if (nbRead == -1) {
                return nbRead;
            }

            return 0xFF & _byteBuffer.get(0);
        }
    }

    @Override
    public long read(ByteBuffer bytes) throws IOException {
        //return _file.getChannel().read(bytes);
        return _channel.read(bytes);
    }

    @Override
    public long read(ByteBuffer bytes, long position) throws IOException {
        //return _file.getChannel().read(bytes, position);
        return _channel.read(bytes, position);
    }

    @Override
    public void seek(final long offset) throws IOException {
        if (position() == offset) {
            return;
        }
        _channel.position(offset);
    }

    @Override
    public void mark(int readlimit) {
        try {
            markPosition = position();
        } catch (IOException ex) {
            markPosition = -1;
        }
    }

    @Override
    public void reset() throws IOException {
        if (isMarked()) {
            seek(markPosition);
            // Clear any mark.
            markPosition = -1;
        }
    }

    protected boolean isMarked() {
        return markPosition != -1;
    }

    @Override
    public long position() throws IOException {
        //return _file.getFilePointer();
        return _channel.position();
    }

    @Override
    public void rewind() throws IOException {
        //_file.seek(0);
        _channel.position(0);
    }

    @Override
    public long skip(long n) throws IOException {
        _channel.position(position() + n);
        return n;
    }

    @Override
    public InputStream getBufferedBackend() {
        return new BufferedInputStream(Channels.newInputStream(_channel));
    }
}
