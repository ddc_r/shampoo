/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author okay_awright
 **/
public class CloseableFileOutputStream extends OutputStream implements FastExtendedOutputStreamInterface {

    protected final Log logger = LogFactory.getLog(getClass());
    private final FileChannel _channel;
    private transient FileLock _lock;
    private transient final ByteBuffer _byteBuffer;

    public CloseableFileOutputStream(File file) throws IOException {
        this._channel = (new RandomAccessFile(file, "rw")).getChannel();
        //Try to acquire an exclusive lock on the file, otherwise throw an exception
        this._lock = _channel/*.getChannel()*/.lock();
        if (_lock == null || !_lock.isValid()) {
            throw new IOException("The stream is already locked by another process and cannot be written");
        }
        this._byteBuffer = ByteBuffer.allocateDirect(IOUtil.DEFAULT_BUFFER_SIZE);
    }

    @Override
    public void close() throws IOException {
        //Release the lock
        if (_lock != null && _lock.isValid()) {
            _lock.release();
        }
        //Then close the stream
        _channel.close();
    }

    @Override
    public boolean isClosed() {
        return _channel == null || !_channel.isOpen();
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public void seek(long offset) throws IOException {
        if (position() == offset) {
            return;
        }
        _channel.position(offset);
    }

    @Override
    public void write(int i) throws IOException {
        //_file.write(i);
        synchronized (_byteBuffer) {
            _byteBuffer.position(0);
            _byteBuffer.limit(1);
            _byteBuffer.put((byte) i);
            _byteBuffer.position(0);
            _channel.write(_byteBuffer);
        }
    }

    @Override
    public long fastCopyFrom(FastCopyInputStreamInterface input, long start, long length) throws IOException {
        return input.fastCopyTo(start, length, _channel/*.getChannel()*/);
    }

    @Override
    public synchronized long fastCopyFrom(FileChannel input, long start, long length) throws IOException {

        //WORKAROUND: copying large files on some OSes, including Windows and old SunOS
        //Magic number for Windows = 64Mb - 32Kb
        int maxBufferSize = (64 * 1024 * 1024) - (32 * 1024);
        int bufferSize = NumberUtil.safeLongToInt( Math.min(
                length,
                Math.min(IOUtil.DEFAULT_BUFFER_SIZE, maxBufferSize)) );
        long totalBytesCopied = 0;

        boolean immutableIsClosed = isClosed();
        //Relock the _channel if the stream is closed, i.e. the lock has already been released
        try {

            if (immutableIsClosed) {
                _lock = _channel.lock();
                if (_lock == null || !_lock.isValid()) {
                    throw new IOException("The stream is already locked by another process and cannot be written");
                }
            }

            long bytesCopiedInPass = 0;

            //Do the copy via native transferTo()
            do {
                bytesCopiedInPass = input.transferTo(
                    start + totalBytesCopied,
                    bufferSize,
                    _channel);
                totalBytesCopied += bytesCopiedInPass;
            } while (totalBytesCopied < length && bytesCopiedInPass > 0);

        } finally {
            if (immutableIsClosed) {
                if (_lock != null && _lock.isValid()) {
                    _lock.release();
                }
            }
        }

        return totalBytesCopied;
    }

    @Override
    public void flush() throws IOException {
        //_channel.getFD().sync();
        _channel.force(true);
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        //_file.write(bytes);
        write(bytes, 0, bytes.length);
    }

    @Override
    public void write(byte[] bytes, int offset, int length) throws IOException {
        //_file.write(bytes, i, i1);
        int nbToWrite;

        if (offset != _channel.position()) {
            _channel.position(offset);
        }

        synchronized (_byteBuffer) {
            do {
                _byteBuffer.position(0);
                _byteBuffer.limit(Math.min(_byteBuffer.capacity(), length));

                _byteBuffer.put(bytes);
                _byteBuffer.rewind();

                nbToWrite = _channel.write(_byteBuffer);

                length -= nbToWrite;
            } while (length > 0 || nbToWrite <= 0);
        }
    }

    @Override
    public long position() throws IOException {
        //return _file.getFilePointer();
        return _channel.position();
    }

    @Override
    public void rewind() throws IOException {
        //_file.seek(0);
        _channel.position(0);
    }

    @Override
    public OutputStream getBufferedBackend() {
        return new BufferedOutputStream(Channels.newOutputStream(_channel));
    }
}
