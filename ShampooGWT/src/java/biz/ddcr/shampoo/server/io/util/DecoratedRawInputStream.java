/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * It's just a phony wrapper for plain InputStreams where bytebuffers are always copied (which defaet their purpose) and no optimization and implementation-guessing are performed
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DecoratedRawInputStream extends InputStream implements FastExtendedInputStreamInterface {

    protected final Log logger = LogFactory.getLog(getClass());

    protected InputStream stream;
    protected transient long position = 0;
    protected transient boolean closed = false; //Let's assume it's open by default

    public DecoratedRawInputStream(InputStream stream) {
        super();
        setInputStream(stream);
    }
    public DecoratedRawInputStream() {
        super();
    }

    protected void setInputStream(InputStream stream) {
        this.stream = stream;
    }

    @Override
    public int read() throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        try {
            int ret = stream.read();
            ++position;
            return ret;
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public int available() throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        try {
            return stream.available();
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public void close() throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        try {
            stream.close();
        } finally {
            closed = true;
        }

    }

    @Override
    public boolean canSeekBackward() {
        //Can only seek forward
        return false;
    }

    @Override
    public synchronized void mark(int readlimit) {
        if (stream!=null)
            stream.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
        return (stream!=null) ? stream.markSupported() : false;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        if (off<0 || len<1) throw new IOException("Out of range");
        try {
            int read = super.read(b, off, len);
            position += read;
            return read;
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public int read(byte[] b) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        try {
            int read = stream.read(b);
            position += read;
            return read;
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public synchronized void reset() throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        try {
        stream.reset();
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public long skip(long n) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        try {
            long skipped = stream.skip(n);
            position += skipped;
            return skipped;
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public void seek(long n) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        long diff = n - position;

        if (diff < 0) {
            throw new UnsupportedOperationException("Can't seek backward");
        }

        if (diff == 0) {
            return;
        }

        long realDiff = 0;
        try {
            realDiff = skip(diff);
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
        if (realDiff<diff) {
            logger.warn(new IOException("Failed to seek " + diff + " bytes"));
            //UPDATE: don't quit, just log a warning
            //throw new IOException("Failed to seek " + diff + " bytes");
        }
    }

    @Override
    public long read(ByteBuffer bytes) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        if (bytes==null) throw new IOException("Null ByteBuffer");
        if (bytes.hasArray() && !bytes.isReadOnly()) {
            return read(bytes.array());
        } else {
            byte[] b = new byte[bytes.remaining()];
            bytes.get(b);
            return read(b);
        }
    }

    @Override
    public long read(ByteBuffer bytes, long position) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        if (bytes==null) throw new IOException("Null ByteBuffer");
        if (position<0) throw new IOException("Out of range");
        if (position()!=position) seek(position);
        return read(bytes);
        //Don't bother resuming the original position: you cannot
    }

    @Override
    public long position() throws IOException {
        return position;
    }

    @Override
    public void rewind() throws IOException {
        throw new UnsupportedOperationException("Can't seek backward");
    }

    @Override
    public long fastCopyTo(long start, long length, OutputStream output) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        if (output==null) throw new IOException("Null OutputStream");
        if (start<0 || length<1) throw new IOException("Out of range");
        return fastCopyTo(start, length, Channels.newChannel(output));
    }

    /** It's absolutely not fast, it's just a phony facade method where byte arrays are copied all over the place **/
    @Override
    public long fastCopyTo(long start, long length, WritableByteChannel output) throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        if (output==null) throw new IOException("Null WritableByteChannel");
        if (start<0 || length<1) throw new IOException("Out of range");

        long totalBytesCopied = 0;
        long bytesCopiedInPass = 0;
        int bufferSize = 0;

        if (position()!=start) seek(start);

        bufferSize = NumberUtil.safeLongToInt( Math.min(length, IOUtil.DEFAULT_BUFFER_SIZE) );
        ByteBuffer _byteBuffer = ByteBuffer.allocate(bufferSize);

        long nbRead = read(_byteBuffer);
        while (nbRead > 0) {

            _byteBuffer.position(0);
            _byteBuffer.limit(NumberUtil.safeLongToInt( Math.min(nbRead, bufferSize)) );
            bytesCopiedInPass = output.write(_byteBuffer);

            totalBytesCopied += bytesCopiedInPass;

            if (_byteBuffer.hasRemaining()) _byteBuffer.compact(); else _byteBuffer.clear();
            nbRead = read(_byteBuffer);
        }

        return totalBytesCopied;

    }

    @Override
    public boolean isClosed() {
        return (stream!=null) ? closed : true;
    }

    @Override
    public InputStream getBufferedBackend() throws IOException {
        if (stream==null) throw new IOException("Stream not ready");
        return new BufferedInputStream(stream);
    }

}
