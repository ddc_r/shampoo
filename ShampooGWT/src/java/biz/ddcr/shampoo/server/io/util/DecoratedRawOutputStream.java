/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;

/**
 *
 * It's just a phony wrapper for plain OutputStreams where bytebuffers are always copied (which defaet their purpose) and no optimization and implementation-guessing are performed
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DecoratedRawOutputStream extends OutputStream implements FastExtendedOutputStreamInterface {

    protected final OutputStream stream;
    protected transient long position = 0;
    protected transient boolean closed = false; //Let's assume it's open by default

    public DecoratedRawOutputStream(OutputStream stream) {
        this.stream = stream;
    }

    @Override
    public void write(int i) throws IOException {
        try {
            stream.write(i);
            ++position;
        } catch (IOException e) {
            //Maybe it's close
            closed = true;
            throw e;
        }
    }

    @Override
    public void close() throws IOException {
        try {
            stream.close();
        } finally {
            closed = true;
        }

    }

    @Override
    public boolean canSeekBackward() {
        //Can only seek forward
        return false;
    }

    @Override
    public void seek(long n) throws IOException {
        throw new UnsupportedOperationException("Can't seek");
    }

    @Override
    public long position() throws IOException {
        return position;
    }

    @Override
    public void rewind() throws IOException {
        throw new UnsupportedOperationException("Can't seek");
    }

    @Override
    public long fastCopyFrom(FastCopyInputStreamInterface input, long start, long length) throws IOException {
        return input.fastCopyTo(start, length, Channels.newChannel(this));
    }

    /** It's absolutely not fast, it's just a phony facade method where byte arrays are copied all over the place **/
    @Override
    public long fastCopyFrom(FileChannel input, long start, long length) throws IOException {
        if (input==null) throw new IOException("Null FileChannel");
        if (start<0 || length<1) throw new IOException("Out of range");
        return input.transferTo(length, length, input);
    }

    @Override
    public boolean isClosed() {
        return closed;
    }

    @Override
    public OutputStream getBufferedBackend() {
        return new BufferedOutputStream(stream);
    }

}
