/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.util;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 *
 * @author okay_awright
 **/
public interface ExtendedByteInputStreamInterface extends InputStreamInterface {

    /**
     * Transfer stream data from the current position to the ByteBuffer buffer
     * The stream position is modified
     * See FileChannel.read(ByteBuffer bytes) for reference
     * @param bytes
     * @return
     * @throws IOException
     **/
    public long read(ByteBuffer bytes) throws IOException;
    /**
     * Transfer stream data from the specified position to the ByteBuffer buffer
     * The original stream position is unmodified
     * See FileChannel.read(ByteBuffer bytes, int position) for reference
     * @param bytes
     * @param position
     * @return
     * @throws IOException
     **/
    public long read(ByteBuffer bytes, long position) throws IOException;

}
