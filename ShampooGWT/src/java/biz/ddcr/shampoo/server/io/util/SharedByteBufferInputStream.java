/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

public class SharedByteBufferInputStream extends InputStream implements FastExtendedInputStreamInterface {

    protected SharedBytePool _sharedBuffer;

    public SharedByteBufferInputStream(SharedBytePool sharedBuffer) {
        if (sharedBuffer == null) {
            throw new IllegalArgumentException("Cannot instantiate a ByteBufferInputStream from a null list of buffers");
        }
        setSharedBytePool(sharedBuffer);
        clear();
    }

    /** useful for c'tor overridings */
    protected void setSharedBytePool(SharedBytePool sharedBuffer) {
        _sharedBuffer = sharedBuffer;
    }
    
    public void clear() {
        _sharedBuffer.getMemoryBuffer().clear();
    }

    @Override
    public int available() throws IOException {
        return _sharedBuffer.getMemoryBuffer().remaining();
    }

    @Override
    public void close() {
        clear();
        //Do nothing else
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public void mark(int readLimit) {
        //Don't use readLimit since ByteBuffer allows an unlimited number of readings
        _sharedBuffer.getMemoryBuffer().mark();
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public int read(byte[] bytes) throws IOException {
        return read(bytes, 0, bytes.length);
    }

    @Override
    public void reset() throws IOException {
        _sharedBuffer.getMemoryBuffer().reset();
    }

    @Override
    public long skip(long l) throws IOException {
        //A ByteBuffer cannot store a long-number of byte, downcast to int
        int previousPosition = _sharedBuffer.getMemoryBuffer().position();
        _sharedBuffer.getMemoryBuffer().position(NumberUtil.safeLongToInt(l));
        int diff = _sharedBuffer.getMemoryBuffer().position() - previousPosition;
        return diff > 0 ? diff : -1;
    }

    @Override
    public int read() throws IOException {
        return (_sharedBuffer.getMemoryBuffer().hasRemaining() ? _sharedBuffer.getMemoryBuffer().get() & 0xFF : -1);
    }

    @Override
    public int read(byte[] bytes, int off, int len) throws IOException {

        if (bytes == null) {
            throw new NullPointerException("No data to read");
        } else if ((off < 0) || (off > bytes.length) || (len < 0)
                || ((off + len) > bytes.length) || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException("Offset "+off+"/length "+len);
        }

        if (!_sharedBuffer.getMemoryBuffer().hasRemaining()) {
            return -1;
        } else if (len == 0 || bytes.length == 0) {
            return (_sharedBuffer.getMemoryBuffer().remaining() > 0 ? 0 : -1);
        }

        len = Math.min(len, _sharedBuffer.getMemoryBuffer().remaining());
        _sharedBuffer.getMemoryBuffer().get(bytes, off, len);

        //Nothing else to read? returns the appropriate warning; otherwise just retruns the number of bytes read so far
        return (len > 0 ? len : -1);

    }

    @Override
    public long read(ByteBuffer bytes) throws IOException {
        long read = 0L;
        if (_sharedBuffer.getMemoryBuffer().hasRemaining()) {
            read = IOUtil.transfer(_sharedBuffer.getMemoryBuffer(), bytes, false);
        }
        return read;
    }

    @Override
    public long read(ByteBuffer bytes, long position) throws IOException {
        if (position < 0) {
            throw new IllegalArgumentException("Position "+position);
        }
        long result = 0L;
        int prevPosition = _sharedBuffer.getMemoryBuffer().position();
        try {
            //Throws an exception if position is larger than capacity (or limit) and return -1; as FileChannel API tells us to do so
            _sharedBuffer.getMemoryBuffer().position(NumberUtil.safeLongToInt(position));
            result = read(bytes);
        } catch (IllegalArgumentException e) {
            result = -1;
        } finally {
            _sharedBuffer.getMemoryBuffer().position(prevPosition);
        }
        return result;
    }

    @Override
    public long fastCopyTo(long position, long count, OutputStream output) throws IOException {
        return fastCopyTo(position, count, Channels.newChannel(output));
    }

    @Override
    public long fastCopyTo(long position, long count, WritableByteChannel output) throws IOException {

        int result = 0;

        if (position < 0 || count < 0) {
            throw new IllegalArgumentException("Position "+position+"/count "+count);
        }

        //A ByteBuffer cannot store a long-number of byte, downcast to int
        int prevPosition = _sharedBuffer.getMemoryBuffer().position(); //could use mark() and reset() too
        int prevLimit = _sharedBuffer.getMemoryBuffer().limit();
        try {
            _sharedBuffer.getMemoryBuffer().limit(NumberUtil.safeLongToInt(Math.min(_sharedBuffer.getMemoryBuffer().capacity(), position + count)));
            _sharedBuffer.getMemoryBuffer().position(NumberUtil.safeLongToInt(Math.min(_sharedBuffer.getMemoryBuffer().limit(), position)));

            result = output.write(_sharedBuffer.getMemoryBuffer());
        } finally {
            _sharedBuffer.getMemoryBuffer().position(prevPosition);
            _sharedBuffer.getMemoryBuffer().limit(prevLimit);
        }
        return result;
    }

    @Override
    public void seek(long position) {
        _sharedBuffer.getMemoryBuffer().position(NumberUtil.safeLongToInt(position));
    }

    @Override
    public boolean isClosed() {
        //This stream remains open
        return false;
    }

    @Override
    public void rewind() {
        _sharedBuffer.getMemoryBuffer().rewind();
    }

    @Override
    public long position() throws IOException {
        return _sharedBuffer.getMemoryBuffer().position();
    }

    @Override
    public InputStream getBufferedBackend() {
        return this;
    }

}

