/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.NumberUtil;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;

/**
 * A stream of bytes directly stored into memory, backed up by a ByteBuffer but still compatible with the reference java.io
 * TODO: implement the idea of incremental array of buffers
 *
 * @author okay_awright
 **/
public class SharedByteBufferOutputStream extends OutputStream implements FastExtendedOutputStreamInterface {

    private final SharedBytePool _sharedBuffer;

    public SharedByteBufferOutputStream(SharedBytePool sharedBuffer) {
        if (sharedBuffer == null) {
            throw new IllegalArgumentException("Cannot instantiate a ByteBufferOutputStream from a null list of buffers");
        }
        _sharedBuffer = sharedBuffer;
        clear();
    }

    @Override
    public void close() throws IOException {
        flush();
        //Do nothing else
    }

    public void clear() {
        _sharedBuffer.getMemoryBuffer().clear();
    }

    @Override
    public void flush() throws IOException {
        clear();
        //Do nothing else
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        write(bytes, 0, bytes.length);
    }

    /**
     * override if you wish to update lastModified for instance
     * DO NOT use this callback for anything other very simple computations, otherwise it will withdraw the benefits of memory manipulation speed
     **/
    public void isContentModified() {
        //Do nothing
    }

    /**
     *
     * Make a new ByteBuffer with capacity extended by growBy
     *
     * @param growBy
     **/
    protected void extendCapacity(int growBy) {
        ByteBuffer biggerBuffer = ByteBuffer.allocate(_sharedBuffer.getMemoryBuffer().capacity() + growBy);

        _sharedBuffer.getMemoryBuffer().flip();
        biggerBuffer.put(_sharedBuffer.getMemoryBuffer());
        _sharedBuffer.setMemoryBuffer(biggerBuffer);
    }

    @Override
    public void write(int b) throws IOException {

        //Check if we need to grow the bytebuffer in order to store the new data
        if (!_sharedBuffer.getMemoryBuffer().hasRemaining()) extendCapacity(1);
        _sharedBuffer.getMemoryBuffer().put((byte) b);

        isContentModified();
    }

    @Override
    public void write(byte[] bytes, int off, int len) throws IOException {

        //Check if we need to grow the bytebuffer in order to store the new data
        if (len > _sharedBuffer.getMemoryBuffer().remaining()) extendCapacity(len - _sharedBuffer.getMemoryBuffer().remaining());
        _sharedBuffer.getMemoryBuffer().put(bytes, off, len);

        isContentModified();
    }

    @Override
    public void seek(long position) {
        _sharedBuffer.getMemoryBuffer().position(NumberUtil.safeLongToInt(position));
    }

    @Override
    public long fastCopyFrom(FastCopyInputStreamInterface input, long start, long length) throws IOException {
        return input.fastCopyTo(start, length, Channels.newChannel(this));
    }

    @Override
    public long fastCopyFrom(FileChannel input, long start, long length) throws IOException {
        return input.transferTo(start, length, Channels.newChannel(this));
    }

    @Override
    public boolean isClosed() {
        //This stream is always open
        return false;
    }

    /*@Override
    public String toString() {
    return new String(_buffer.array(), _buffer.arrayOffset(), _buffer.position());
    }*/
    @Override
    public void rewind() {
        _sharedBuffer.getMemoryBuffer().rewind();
    }

    @Override
    public long position() throws IOException {
        return _sharedBuffer.getMemoryBuffer().position();
    }

    @Override
    public OutputStream getBufferedBackend() {
        return this;
    }

}

