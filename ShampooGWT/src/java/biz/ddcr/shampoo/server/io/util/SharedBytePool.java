/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.io.HasFullHashCode;
import java.nio.ByteBuffer;

/**
 *
 * Thread-safe pool of available ByteBuffers
 * TODO: make it a real pool of thread-safe ByteBuffers not just a single shareable item
 *
 * @author okay_awright
 **/
public class SharedBytePool {

    protected volatile ByteBuffer memoryBuffer;

    public SharedBytePool(ByteBuffer memoryBuffer) {
        setMemoryBuffer(memoryBuffer);
    }

    public ByteBuffer getMemoryBuffer() {
        return memoryBuffer;
    }

    public void setMemoryBuffer(ByteBuffer memoryBuffer) {
        this.memoryBuffer = memoryBuffer;
    }

    public int shallowStreamHashCode() {
        int hash = 3;
        hash = 43 * hash + (this.memoryBuffer != null ? this.memoryBuffer.hashCode() : 0);
        return hash;
    }

}
