/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.io.helper.Tag;

/**
 *
 * @author okay_awright
 **/
public class TrackStream implements TypedStreamInterface<AudioFileInfo> {

    private UntypedStreamInterface stream;
    private AudioFileInfo container;
    private Tag tag;

    /**
     * Wraps, not clone
     * @param type
     * @param stream
     **/
    public TrackStream(AudioFileInfo container, UntypedStreamInterface stream) {
        this.container = container;
        this.stream = stream;
    }

    public TrackStream(Tag tag, AudioFileInfo container, UntypedStreamInterface stream) {
        this(container, stream);
        this.tag = tag;
    }

    @Override
    public AudioFileInfo getContainer() {
        return container;
    }

    @Override
    public UntypedStreamInterface getRawStream() {
        return stream;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    @Override
    public boolean isCacheable() {
        //Not implemented
        return false;
    }    
    
    @Override
    public int fullHashCode() {
        int hash = 7;
        hash = 71 * hash + (this.container != null ? this.container.fullHashCode() : 0);
        hash = 71 * hash + (this.stream != null ? this.stream.shallowStreamHashCode() : 0);
        return hash;
    }
}
