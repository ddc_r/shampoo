/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.FinalBoolean;
import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import biz.ddcr.shampoo.server.io.util.ftp.FTPConnection2;
import biz.ddcr.shampoo.server.io.util.ftp.NormalFTPConnection2;
import biz.ddcr.shampoo.server.io.util.ftp.SecureFTPConnection2;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ftp4che.FTPConnection;
import org.ftp4che.exception.AuthenticationNotSupportedException;
import org.ftp4che.exception.FtpIOException;
import org.ftp4che.exception.FtpWorkflowException;
import org.ftp4che.exception.NotConnectedException;
import org.ftp4che.util.ftpfile.FTPFile;

/**
 * FTP-implementation of a stream
 * Try to utilize one single connection for the lifetime of this object
 * @author okay_awright
 **/
public class UntypedFTPStream extends UntypedStream {

    private int secured;
    private int port;
    private String host, user, password, path, filename;
    protected final Log logger = LogFactory.getLog(getClass());
    private transient DecoratedRawInputStream inputStream;
    private boolean isWritable;
    private transient FTPConnection2 client;
    private transient FTPFile ftpFile;

    public UntypedFTPStream(String identifier, URL url, boolean autoConnect, FinalBoolean success) throws FileNotFoundException, IOException {
        this(identifier, url, true, autoConnect, success);
    }
    public UntypedFTPStream(String identifier, URL url) throws FileNotFoundException, IOException {
        this(identifier, url, true, false);
    }

    public UntypedFTPStream(String identifier, URL url, boolean canWrite, boolean autoConnect) throws FileNotFoundException, IOException {
        this(identifier, url, canWrite, autoConnect, null);
    }
    public UntypedFTPStream(String identifier, URL url, boolean canWrite) throws FileNotFoundException, IOException {
        this(identifier, url, canWrite, false, null);
    }
    public UntypedFTPStream(String identifier, URL url, boolean canWrite, boolean autoConnect, FinalBoolean success) throws FileNotFoundException, IOException {
        super(identifier);
        parseURL(url);
        this.isWritable = canWrite;
        boolean ready = autoConnect ? doAutoConnect() : true;
        if (success != null) {
            success.value = ready;
        }
    }

    protected boolean checkIfRecoverable(FtpIOException e) {
        //See http://en.wikipedia.org/wiki/List_of_FTP_server_return_codes
        return (e != null && e.getCode() != null && e.getCode().startsWith("4"));
    }

    protected boolean checkIfRecoverable(FtpWorkflowException e) {
        //See http://en.wikipedia.org/wiki/List_of_FTP_server_return_codes
        return (e != null && e.getCode() != null && e.getCode().startsWith("4"));
    }
  
    protected boolean doAutoConnect() throws IOException {
        try {
            getClient().singleConnect();
            getClient().noOperation();
            return true;
        } catch (NotConnectedException ex) {
            throw new IOException(ex.getMessage());
        } catch (AuthenticationNotSupportedException ex) {
            throw new IOException(ex.getMessage());
        } catch (FtpIOException ex) {
            if (checkIfRecoverable(ex)) {
                return false;
            } else {
                throw new IOException(ex.getMessage());
            }
        } catch (FtpWorkflowException ex) {
            if (checkIfRecoverable(ex)) {
                return false;
            } else {
                throw new IOException(ex.getMessage());
            }
        }
    }

    public FTPFile getFTPFile() {
        if (ftpFile == null) {
            ftpFile = new FTPFile(path, filename);
        }
        return ftpFile;
    }

    protected void parseURL(URL url) throws FileNotFoundException {
        if (url != null && url.getProtocol().startsWith("ftp")) {

            //FTPS?
            if (url.getProtocol().equals("ftps")) {
                if (url.getPort() == -1 || url.getPort() == 21) {
                    port = 21;
                    //FTPS (explicit)
                    secured = FTPConnection.AUTH_TLS_FTP_CONNECTION;
                } else {
                    port = url.getPort();
                    //FTPS (implicit)
                    secured = FTPConnection.IMPLICIT_TLS_FTP_CONNECTION;
                }
            } else {
                if (url.getPort() == -1) {
                    port = 21;
                } else {
                    port = url.getPort();
                }
                //Plain FTP
                secured = FTPConnection.FTP_CONNECTION;
            }

            //Credentials
            user = "anonymous";
            password = "UntypedFTPStream@shampoo.ddcr.biz";
            if (url.getUserInfo() != null && url.getUserInfo().contains(":")) {
                String[] userInfo = url.getUserInfo().split(":");
                user = userInfo[0];
                password = userInfo[1];
            }

            host = url.getHost();

            try {
                int i = url.getPath().lastIndexOf('/');
                if (i > -1) {
                    path = url.getPath().substring(0, i + 1);
                    filename = url.getPath().substring(i + 1);
                }
            } catch (IndexOutOfBoundsException e) {
                throw new FileNotFoundException("Invalid path or file");
            }

        } else {
            throw new FileNotFoundException("Not FTP");
        }
    }

    protected FTPConnection2 getClient() throws IOException {
        //Lazy instantiation
        if (client == null) {
            client = secured == FTPConnection.FTP_CONNECTION ? new NormalFTPConnection2() : new SecureFTPConnection2();
            client.setConnectionType(secured);
            client.setAddress(new InetSocketAddress(host, port));
            client.setUser(user);
            client.setPassword(password);
            client.setAccount(null);
            client.setTimeout(10000);
            client.setPassiveMode(true);
            client.setDownloadBandwidth(FTPConnection.MAX_DOWNLOAD_BANDWIDTH);
            client.setUploadBandwidth(FTPConnection.MAX_UPLOAD_BANDWIDTH);
            client.setTryResume(true);
            client.setProxy(null);
        }
        return client;
    }

    @Override
    public boolean isInMemory() {
        return false;
    }

    public boolean exists() throws IOException {
        return getSize() > -1;
    }

    /**
     * Returns the size of the stream in bytes, or -1 if it cannot be computed
     * @return
     */
    @Override
    public long getSize() throws IOException {

        //favour cached data
        if (getFTPFile().getSize() > -1) {
            return getFTPFile().getSize();
        }

        long result = -1L;
        try {
            getClient().singleConnect();
            getClient().noOperation();
            if (isReadable()) {
                result = getClient().getSize(getFTPFile());
                getFTPFile().setSize(result);
            } else {
                forceDisconnection();
                throw new IOException("this stream cannot be polled");
            }
        } catch (FtpIOException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (FtpWorkflowException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (Exception ex) {
            logger.error(ex);
            forceDisconnection();
        }
        return result;
    }

    @Override
    public boolean clean() throws IOException {
        boolean result = false;
        try {
            getClient().singleConnect();
            getClient().noOperation();
            if (isWipeable()) {
                getClient().deleteFile(getFTPFile());
                result = true;
            } else {
                forceDisconnection();
                throw new IOException("this stream cannot be deleted");
            }
        } catch (FtpIOException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (FtpWorkflowException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (Exception ex) {
            logger.error(ex);
            forceDisconnection();
        }
        return result;
    }

    /**
     * Try to use OS file-specific mechanisms first, then fall back to the original method
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean moveTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {
        //Use FTP-specific methods first if available
        if (destinationStream instanceof UntypedFTPStream) //Favour work on the FTP server only
        {
            boolean result = false;
            try {
                getClient().singleConnect();
                getClient().noOperation();
                if (isWipeable()) {
                    getClient().renameFile(getFTPFile(), ((UntypedFTPStream) destinationStream).getFTPFile());
                    result = true;
                } else {
                    forceDisconnection();
                    throw new IOException("this stream cannot be deleted");
                }
            } catch (FtpIOException ex) {
                if (!checkIfRecoverable(ex)) {
                    forceDisconnection();
                    throw new IOException(ex.getMessage());
                }
            } catch (FtpWorkflowException ex) {
                if (!checkIfRecoverable(ex)) {
                    forceDisconnection();
                    throw new IOException(ex.getMessage());
                }
            } catch (Exception ex) {
                logger.error(ex);
                forceDisconnection();
            }
            return result;
        } else {
            //then try to copy the source to the destination, and overwrite the destination if it already exists
            //then delete the source
            return (copyTo(destinationStream, eventHandler) && clean());
        }
    }

    @Override
    public DecoratedRawInputStream getInputStream() throws IOException {
        try {
            getClient().singleConnect();
            getClient().noOperation();
            if (isReadable()) {
                if (inputStream == null || inputStream.isClosed()) {
                    inputStream = new DecoratedRawInputStream(getClient().downloadStream(getFTPFile()));
                }
                return inputStream;
            } else {
                forceDisconnection();
                throw new IOException("this stream is not readable");
            }
        } catch (FtpIOException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (FtpWorkflowException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (Exception ex) {
            logger.error(ex);
            forceDisconnection();
        }
        return null;
    }

    @Override
    public void closeInputStream() throws IOException {
        if (inputStream != null && !inputStream.isClosed()) {
            inputStream.close();
        }
    }

    @Override
    public synchronized FastExtendedOutputStreamInterface getOutputStream() throws IOException {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public void closeOutputStream() throws IOException {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public long getLastModified() throws IOException {
        //favour cached data
        if (getFTPFile().getDate() != null) {
            return getFTPFile().getDate().getTime();
        }

        long result = -1L;
        try {
            getClient().singleConnect();
            getClient().noOperation();
            if (isReadable()) {
                Date date = getClient().getModificationTime(getFTPFile());
                if (date != null) {
                    result = date.getTime();
                    getFTPFile().setDate(date);
                }
            } else {
                forceDisconnection();
                throw new IOException("this stream cannot be polled");
            }
        } catch (FtpIOException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (FtpWorkflowException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (Exception ex) {
            logger.error(ex);
            forceDisconnection();
        }
        return result;
    }

    @Override
    public boolean setLastModified(long timestamp) throws IOException {
        boolean result = false;
        try {
            getClient().singleConnect();
            getClient().noOperation();
            if (isWritable()) {
                Date date = new Date(timestamp);
                getClient().setModificationTime(getFTPFile(), date);
                getFTPFile().setDate(date);
                result = true;
            } else {
                forceDisconnection();
                throw new IOException("this stream cannot be polled");
            }
        } catch (FtpIOException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (FtpWorkflowException ex) {
            if (!checkIfRecoverable(ex)) {
                forceDisconnection();
                throw new IOException(ex.getMessage());
            }
        } catch (Exception ex) {
            logger.error(ex);
            forceDisconnection();
        }
        return result;
    }

    protected boolean isReadable() {
        //Assume yes, actually we don't know yet
        return true;
    }

    @Override
    public boolean isWritable() {
        //Assume yes, actually we don't know yet
        return isWritable;
    }

    @Override
    public boolean isWipeable() {
        return isWritable();
    }

    /** Copy the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean copyTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {

        boolean result = false;
        //Use FTP-specific methods first if available
        if (destinationStream instanceof UntypedFTPStream) //Favour work on the FTP server only
        {
            try {
                getClient().singleConnect();
                getClient().noOperation();
                if (isReadable()) {
                    //FTP kludge no distant copy (except with specific SITE commands) so download and then reupload
                    getClient().uploadStream(getClient().downloadStream(getFTPFile()), ((UntypedFTPStream) destinationStream).getFTPFile());
                    result = true;
                } else {
                    forceDisconnection();
                    throw new IOException("this stream cannot be read");
                }
            } catch (FtpIOException ex) {
                if (!checkIfRecoverable(ex)) {
                    forceDisconnection();
                    throw new IOException(ex.getMessage());
                }
            } catch (FtpWorkflowException ex) {
                if (!checkIfRecoverable(ex)) {
                    forceDisconnection();
                    throw new IOException(ex.getMessage());
                }
            } catch (Exception ex) {
                logger.error(ex);
                forceDisconnection();
            }
            return result;
        } else {

            long totalBytesCopied = 0;

            //Open the streams
            DecoratedRawInputStream _inputStream = getInputStream();
            WritableByteChannel _outputStream = Channels.newChannel(destinationStream.getOutputStream().getBufferedBackend());

            //Initial progress set to 0
            if (eventHandler != null) {
                eventHandler.onProgress((byte) 0);
            }
            try {
                //Split the actual upload into smaller chunks
                while (totalBytesCopied < getSize()) {
                    //Check if the transfer should be cancelled
                    if (eventHandler != null && eventHandler.onCancellable()) {
                        //break it
                        throw new InterruptedIOException();
                    }
                    //Transfer the stream to the local file
                    totalBytesCopied += _inputStream.fastCopyTo(
                            totalBytesCopied,
                            getMaxTransferWindowSize(),
                            _outputStream);
                    //Current progress notification
                    if (eventHandler != null) {
                        eventHandler.onProgress((byte) ((totalBytesCopied / getSize()) * 100));
                    }
                }
                result = true;
            } finally {
                //Close the streams
                if (_inputStream != null) {
                    _inputStream.close();
                }
                if (_outputStream != null) {
                    _outputStream.close();
                }
            }
        }
        return result;
    }

    public boolean copyFrom(UntypedStreamInterface sourceStream, TransferCallbackInterface eventHandler) throws IOException {

        boolean result = false;
        //Use FTP-specific methods first if available
        if (sourceStream instanceof UntypedFTPStream) {
            //Favour work on the FTP server only
            ((UntypedFTPStream) sourceStream).copyTo(this, eventHandler);
        } else {
            InputStream _inputStream = null;
            try {
                _inputStream = sourceStream.getInputStream().getBufferedBackend();
                getClient().singleConnect();
                getClient().noOperation();
                if (isWritable()) {
                    getClient().uploadStream(_inputStream, sourceStream.getSize(), getFTPFile(), false);
                    result = true;
                } else {
                    forceDisconnection();
                    throw new IOException("this stream cannot be written");
                }
            } catch (FtpIOException ex) {
                if (!checkIfRecoverable(ex)) {
                    forceDisconnection();
                    throw new IOException(ex.getMessage());
                }
            } catch (FtpWorkflowException ex) {
                if (!checkIfRecoverable(ex)) {
                    forceDisconnection();
                    throw new IOException(ex.getMessage());
                }
            } catch (Exception ex) {
                logger.error(ex);
                forceDisconnection();
            }
        }
        return result;
    }

    public void forceDisconnection() {
        try {
            getClient().disconnect();
        } catch (IOException ex1) {
            logger.error("Cannot manually disconnect from server", ex1);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            getClient().disconnect();
        } finally {
            super.finalize();
        }
    }
    
    @Override
    public int shallowStreamHashCode() {
        int hash = 5;
        hash = 29 * hash + (this.ftpFile != null && this.ftpFile.getFile() != null ? this.ftpFile.getFile().hashCode() : 0);
        return hash;
    }
}
