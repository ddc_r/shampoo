/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

/**
 *
 * @author okay_awright
 **/
public class UntypedLocalFileStream extends UntypedStream {

    private File localFile;
    private transient CloseableFileInputStream inputStream;
    private transient CloseableFileOutputStream outputStream;
    private boolean isWritable;

    public UntypedLocalFileStream(String identifier, File file) throws FileNotFoundException {
        this(identifier, file, true);
    }

    public UntypedLocalFileStream(String identifier, File file, boolean canWrite) throws FileNotFoundException {
        super(identifier);
        this.localFile = file;
        this.isWritable = canWrite;
    }

    @Override
    public boolean isInMemory() {
        return false;
    }

    protected File getFile() {
        return localFile;
    }

    public String getNativeFilePath() {
        return getFile().getPath();
    }

    /**
     * Returns the size of the stream in bytes, or -1 if it cannot be computed
     * @return
     */
    @Override
    public long getSize() {
        /*long size = -1;
        FileLock _lock = null;
        FileChannel localFileChannel = null;
        try {
            localFileChannel = (new FileInputStream(localFile)).getChannel();
            _lock = localFileChannel.lock(0, localFileChannel.size(), true);

            //Return the file channel dimension
            size = localFileChannel.size();
        } catch (IOException e) {
            logger.error(e);
        } finally {
            try {
                if (_lock!=null && _lock.isValid()) _lock.release();
            } catch (IOException e) {
                logger.error(e);
            }
            try {
                if (localFileChannel!=null) localFileChannel.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return size;*/
        return localFile!=null ? localFile.length() : -1;
    }

    @Override
    public boolean clean() throws IOException {
        if (isWipeable()) {
            return localFile!=null ? localFile.delete() : false;
        } else {
            throw new IOException("this stream cannot be deleted");
        }
    }

    /**
     * Try to use OS file-specific mechanisms first, then fall back to the original method
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean moveTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {
        //Use file-specific methods first if available
        if (destinationStream instanceof UntypedLocalFileStream) //Don't try to use any OS-specific mechanism to move the temp file over any existing one only, with anything less than java 7 (and the aim is to make the application Java 5 compliant),  it will most likely silently fail on Windows and NFS-mounted drives, which is a pity, prepare for a backup plan such as a straight copy followed by a delete
        //Compared to other implementations, there's no retry mechanism in case of failure, if an I/O Exception on a local filesystem occurs there's usually no point in automatically retrying
        {
            //Vanilla JDK API
            if (getFile().renameTo(((UntypedLocalFileStream) destinationStream).getFile())) {
                //Make sure the timestamp of the file is modified, it appears it's not automatically performed on some platforms
                //Don't use DateHelper for this, keep the dependencies to the minimum in this code section
                ((UntypedLocalFileStream) destinationStream).setLastModified(System.currentTimeMillis());
                return true;
            }
        }

        //There's been a problem with renameTo or it's not available
        //then try to copy the source to the destination, and overwrite the destination if it already exists
        //then delete the source
        return (copyTo(destinationStream, eventHandler) && clean());
    }

    @Override
    public CloseableFileInputStream getInputStream() throws IOException {
        if (isReadable()) {
            if (inputStream == null || inputStream.isClosed()) {
                inputStream = new CloseableFileInputStream(localFile);
            }
            return inputStream;
        } else {
            throw new IOException("this stream is not readable");
        }
    }
    @Override
    public void closeInputStream() throws IOException {
        if (inputStream != null && !inputStream.isClosed()) {
            inputStream.close();
        }
    }

    @Override
    public synchronized CloseableFileOutputStream getOutputStream() throws IOException {
        if (isWritable()) {
            if (outputStream == null || outputStream.isClosed()) {
                outputStream = new CloseableFileOutputStream(localFile);
            }
            return outputStream;
        } else {
            throw new IOException("this stream is not writable");
        }
    }
    @Override
    public synchronized void closeOutputStream() throws IOException {
        if (outputStream != null && !outputStream.isClosed()) {
            outputStream.close();
        }
    }

    @Override
    public long getLastModified() {
        return localFile != null ? localFile.lastModified() : -1;
    }

    @Override
    public boolean setLastModified(long timestamp) {
        if (localFile != null) {
            return localFile.setLastModified(timestamp);
        }
        return false;
    }

    /**
     * BUG canWrite() is buggy on Windows NT/XP? or is it just a concurrency problem with external processes?
     * FIX? crude filelocking appears to be enough to fix those odd and random bugs
     * @return
     */
    @Override
    public boolean isWritable() {
        /*return FileLocking.lock(
        this,
        new FileLockingCallback() {
        public boolean run(FileChannel lockedChannel) throws IOException {
        return isWritable && (lockedChannel != null *//*&& lockedChannel.canWrite()*//*);
        }
        },
        true);*/
        if (localFile != null && isWritable) {
            if (localFile.exists() && localFile.isFile())
                return localFile.canWrite();
            else {
                File parent = localFile.getParentFile();
                if (parent!=null && parent.isDirectory())
                    return parent.canWrite();
            }
        }
        return false;
    }

    protected boolean isReadable() {
        if (localFile != null) {
            if (localFile.exists() && localFile.isFile())
                return localFile.canRead();
            else {
                File parent = localFile.getParentFile();
                if (parent!=null && parent.isDirectory())
                    return parent.canRead();
            }
        }
        return false;
    }

    @Override
    public boolean isWipeable() {
        return isWritable();
    }

    /** Copy the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean copyTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {

        long totalBytesCopied = 0;
        boolean result = false;

        //Open the streams
        CloseableFileInputStream _inputStream = getInputStream();
        WritableByteChannel _outputStream = Channels.newChannel(destinationStream.getOutputStream().getBufferedBackend());

        //Initial progress set to 0
        if (eventHandler != null) {
            eventHandler.onProgress((byte) 0);
        }
        try {
            //Split the actual upload into smaller chunks
            while (totalBytesCopied < getSize()) {
                //Check if the transfer should be cancelled
                if (eventHandler != null && eventHandler.onCancellable()) {
                    //break it
                    throw new InterruptedIOException();
                }
                //Transfer the stream to the local file
                totalBytesCopied += _inputStream.fastCopyTo(
                        totalBytesCopied,
                        getMaxTransferWindowSize(),
                        _outputStream);
                //Current progress notification
                if (eventHandler != null) {
                    eventHandler.onProgress((byte) ((totalBytesCopied / getSize()) * 100));
                }
            }
            result = true;
        } finally {
            //Close the streams
            if (_inputStream != null) {
                _inputStream.close();
            }
            if (_outputStream != null) {
                _outputStream.close();
            }
        }
        return result;
    }

    @Override
    public int shallowStreamHashCode() {
        int hash = 5;
        hash = 29 * hash + (this.localFile != null ? this.localFile.hashCode() : 0);
        return hash;
    }    
    
}
