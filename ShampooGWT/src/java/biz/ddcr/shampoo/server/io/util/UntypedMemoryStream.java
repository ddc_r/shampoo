/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

/**
 *
 * A stream of bytes stored in memory; most likely in the heap regarding the current implementation of ByteBuffers
 *
 * @author okay_awright
 **/
public class UntypedMemoryStream extends UntypedStream {

    protected SharedBytePool sharedBytes;
    protected long lastModificationTimestamp;

    protected transient SharedByteBufferInputStream inputStream;
    protected transient SharedByteBufferOutputStream outputStream;

    public UntypedMemoryStream(String identifier) {
        super(identifier);
    }

    public UntypedMemoryStream(String identifier, byte[] buffer) {
        super(identifier);

        ByteBuffer memoryBuffer = ByteBuffer.allocate(buffer.length);
        memoryBuffer.put(buffer);
        setSharedBytePool(memoryBuffer);

        //Make a wild guess and mark the whole content as brand new and then notify contentModified()
        setLastModified(System.currentTimeMillis());
    }
    
    /**
     * 
     * Wrap, not clone
     * 
     * @param identifier
     * @param mime
     * @param buffer
     **/
    public UntypedMemoryStream(String identifier, ByteBuffer buffer) {
        super(identifier);

        setSharedBytePool(buffer);

        //Make a wild guess and mark the whole content as brand new and then notify contentModified()
        setLastModified(System.currentTimeMillis());
    }
    
    protected void setSharedBytePool(ByteBuffer buffer) {
        sharedBytes = new SharedBytePool(buffer);
    }
    
    @Override
    public boolean isInMemory() {
        return true;
    }

    @Override
    public boolean clean() throws IOException {
        sharedBytes.getMemoryBuffer().clear();
        return true;
    }

    @Override
    public long getSize() {
        return sharedBytes.getMemoryBuffer().capacity();
    }

    @Override
    public SharedByteBufferInputStream getInputStream() throws IOException {
        if (inputStream==null)
            inputStream = new SharedByteBufferInputStream(sharedBytes);
        return inputStream;
    }
    @Override
    public void closeInputStream() throws IOException {
        if (inputStream != null && !inputStream.isClosed()) {
            inputStream.close();
        }
    }

    /** Synchronize this method, just to be on the safe side; I'm not sure it should be necessary though **/
    @Override
    public synchronized SharedByteBufferOutputStream getOutputStream() throws IOException {
        if (outputStream==null)
            outputStream = new SharedByteBufferOutputStream(sharedBytes);
        return outputStream;
    }
    @Override
    public synchronized void closeOutputStream() throws IOException {
        if (outputStream != null && !outputStream.isClosed()) {
            outputStream.close();
        }
    }

    @Override
    public long getLastModified() {
        return lastModificationTimestamp;
    }

    @Override
    public boolean setLastModified(long timestamp) {
        lastModificationTimestamp = timestamp;
        return true;
    }

    @Override
    public boolean isWritable() {
        //Always writable
        return true;
    }

    @Override
    public boolean isWipeable() {
        //Always wipeable
        return true;
    }

    /** Copy the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean copyTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {
        return copyTo(getInputStream(), destinationStream, eventHandler);
    }

    /** Copy the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    public boolean copyTo(SharedByteBufferInputStream _inputStream, UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {

        long totalBytesCopied = 0;
        boolean result = false;

        //Open the streams
        WritableByteChannel _outputStream = Channels.newChannel(destinationStream.getOutputStream().getBufferedBackend());

        //Initial progress set to 0
        if (eventHandler != null) {
            eventHandler.onProgress((byte) 0);
        }
        try {
            //Split the actual upload into smaller chunks
            while (totalBytesCopied < getSize()) {
                //Check if the transfer should be cancelled
                if (eventHandler != null && eventHandler.onCancellable()) {
                    //break it
                    throw new InterruptedIOException();
                }
                //Transfer the stream to the local file
                totalBytesCopied += _inputStream.fastCopyTo(
                        totalBytesCopied,
                        getMaxTransferWindowSize(),
                        _outputStream);
                //Current progress notification
                if (eventHandler != null) {
                    eventHandler.onProgress((byte) ((totalBytesCopied / getSize()) * 100));
                }
            }
            result = true;
        } finally {
            //Close the streams
            if (_inputStream != null) {
                _inputStream.close();
            }
            if (_outputStream != null) {
                _outputStream.close();
            }
        }
        return result;
    }
    
    @Override
    public int shallowStreamHashCode() {
        int hash = 5;
        hash = 29 * hash + (this.sharedBytes != null ? this.sharedBytes.shallowStreamHashCode() : 0);
        return hash;
    }
}
