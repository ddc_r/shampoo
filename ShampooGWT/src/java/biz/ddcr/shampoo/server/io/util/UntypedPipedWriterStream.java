/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.MicroTask;
import biz.ddcr.shampoo.server.helper.TaskCallback;
import biz.ddcr.shampoo.server.helper.TaskPerformer;
import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;

/**
 *
 * A generic stream that can only be read
 *
 * @author okay_awright
 **/
public abstract class UntypedPipedWriterStream extends UntypedStream {

    final private transient TaskPerformer taskPerformer;
    protected long lastModificationTimestamp;
    
    final private transient PipedOutputStream pipedWriter;
    final private transient PipedInputStream pipedReader;
    
    private transient DecoratedRawInputStream inputStream;

    public UntypedPipedWriterStream(String identifier, TaskPerformer taskPerformer) throws IOException, Exception {
        super(identifier);

        this.taskPerformer = taskPerformer;
        
        pipedWriter = new PipedOutputStream();
        pipedReader = new PipedInputStream(pipedWriter);

        //Make a wild guess and mark the whole content as brand new and then notify contentModified()
        setLastModified(System.currentTimeMillis());
        
        start();
    }
    
    public abstract void feed(final OutputStream outputStream) throws Exception;
    
    protected void start() throws Exception {
        //Second step: actual processing
            MicroTask loop = new MicroTask() {

                @Override
                public boolean loop() throws Exception {
                    try {
                        feed(pipedWriter);
                        pipedWriter.flush();
                    } finally {
                        pipedWriter.close();
                    }
                    return false;
                }
            };
            TaskCallback loopCallback = new TaskCallback() {
                @Override
                public void onFailure(Exception e) throws Exception {
                    throw e;
                }

                @Override
                public void onSuccess() throws Exception {
                    //Do nothing
                }
            };
            taskPerformer.runTask(loop, loopCallback);
    }
    
    @Override
    public boolean isInMemory() {
        return true;
    }
    
    /**
     * Cannot be cleaned but pretends it works
     */
    @Override
    public boolean clean() throws IOException {
        return true;
    }

    /** Cannot guess*/
    @Override
    public long getSize() {
        return -1;
    }

    @Override
    public DecoratedRawInputStream getInputStream() throws IOException {
        if (inputStream==null)
            inputStream = new DecoratedRawInputStream(pipedReader);        
        return inputStream;
    }
    @Override
    public void closeInputStream() throws IOException {
        if (inputStream != null && !inputStream.isClosed()) {
            inputStream.close();
        }
    }

    @Override
    public FastExtendedOutputStreamInterface getOutputStream() throws IOException {
        throw new UnsupportedOperationException("UntypedPipedWriterStream does not provide direct access to its OutputStream");        
    }
    @Override
    public void closeOutputStream() throws IOException {
        throw new UnsupportedOperationException("UntypedPipedWriterStream does not provide direct access to its OutputStream");
    }

    @Override
    public long getLastModified() {
        return lastModificationTimestamp;
    }

    @Override
    public boolean setLastModified(long timestamp) {
        lastModificationTimestamp = timestamp;
        return true;
    }

    @Override
    public boolean isWritable() {
        //Always writable
        return true;
    }

    @Override
    public boolean isWipeable() {
        //Not wipeable by default
        return false;
    }

    /** Copy the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean copyTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {
        return copyTo(getInputStream(), destinationStream, eventHandler);
    }

    /** Copy the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    public boolean copyTo(DecoratedRawInputStream _inputStream, UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {

        long totalBytesCopied = 0;
        boolean result = false;

        //Open the streams
        WritableByteChannel _outputStream = Channels.newChannel(destinationStream.getOutputStream().getBufferedBackend());

        //Initial progress set to 0
        if (eventHandler != null) {
            eventHandler.onProgress((byte) 0);
        }
        try {
            //Split the actual upload into smaller chunks
            long bytesRead = 0;
            do {
                //Check if the transfer should be cancelled
                if (eventHandler != null && eventHandler.onCancellable()) {
                    //break it
                    throw new InterruptedIOException();
                }
                //Transfer the stream to the local file
                bytesRead = _inputStream.fastCopyTo(
                        totalBytesCopied,
                        getMaxTransferWindowSize(),
                        _outputStream);
                totalBytesCopied += bytesRead;
                //We cannot call the current progress notification callback since we don't know the final size of the stream
            } while (bytesRead>0);
            result = true;
        } finally {
            //Close the streams
            if (_inputStream != null) {
                _inputStream.close();
            }
            if (_outputStream != null) {
                _outputStream.close();
            }
        }
        return result;
    }
    
}
