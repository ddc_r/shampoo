/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import biz.ddcr.shampoo.server.io.helper.IOUtil;
import java.io.IOException;

/**
 *
 * Basically a File wrapped up with some MIME info. Since File has no viable interface, it will remain that way, i.e. a composite object.
 *
 * @author okay_awright
 **/
public abstract class UntypedStream implements UntypedStreamInterface {

    private String identifier;

    public UntypedStream(String identifier) {
        this.setIdentifier(identifier);
    }

    protected int getMaxTransferWindowSize() {
        return IOUtil.DEFAULT_BUFFER_SIZE;
    }

    /**
     * Move the content of the stream to another
     * @return
     * @throws IOException
     */
    @Override
    public boolean moveTo(UntypedStreamInterface destinationStream) throws IOException {
        return moveTo(destinationStream, null);
    }
    /** Move the content from one stream to another, with a progress callback if the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    @Override
    public boolean moveTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException {
        //Try to copy the source to the destination, and overwrite the destination if it already exists
        //then delete the source
        return (copyTo(destinationStream, eventHandler) && clean());
    }
    /**
     * Copy the content of the stream to another
     * @return
     * @throws IOException
     */
    @Override
    public boolean copyTo(UntypedStreamInterface destinationStream) throws IOException {
        return copyTo(destinationStream, null);
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public void setIdentifier(String identifier) {
        //identifier should always be specified
        if (identifier==null) identifier = UUIDHelper.toShortenString(UUIDHelper.getRandomUUID());
        this.identifier = identifier;
    }

    /**
     * Beware: actual contents are not compared, only stream identifiers! Be sure to tag different streams with different ids beforehand
     * @param o
     * @return
     */
    @Override
    public int compareTo(UntypedStreamInterface o) {
        if (o!=null)
            return (this.getIdentifier() == null ? 0 : (o.getIdentifier() == null ? 0 : this.getIdentifier().compareTo(o.getIdentifier())));
        else
            return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UntypedStream other = (UntypedStream) obj;
        if ((this.identifier == null) ? (other.identifier != null) : !this.identifier.equals(other.identifier)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.identifier != null ? this.identifier.hashCode() : 0);
        return hash;
    }

}
