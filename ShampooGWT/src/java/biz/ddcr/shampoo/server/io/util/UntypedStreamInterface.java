/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util;

import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import java.io.IOException;
import java.io.Serializable;

/**
 *
 * All Datastore streams must extend this interface
 *
 * @author okay_awright
 *
 */
public interface UntypedStreamInterface extends Serializable, Comparable<UntypedStreamInterface> {

    /**
     * Check if the stream is actually stored in memory The result depends on
     * the implementing class, e.g. MIMEMemoryStream always returns true while
     * MIMELocalFileStream always returns false
     *
     * @return
     *
     */
    public boolean isInMemory();

    /**
     * can getOutputStream() be used without raising an Exception?
     *
     * @return
     *
     */
    public boolean isWritable();

    /**
     * can we use clean() without raising an Exception?
     *
     * @return
     *
     */
    public boolean isWipeable();

    public long getSize() throws IOException;

    /**
     * Remove the content of the underlying byte storage This object can still
     * be queried as usual afterwards
     *
     * @return
     *
     */
    public boolean clean() throws IOException;

    /**
     * Move the content of the stream to another
     *
     * @return
     * @throws IOException
     */
    public boolean moveTo(UntypedStreamInterface destinationStream) throws IOException;

    /**
     * Move the content from one stream to another, with a progress callback if
     * the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    public boolean moveTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException;

    /**
     * Copy the content of the stream to another
     *
     * @return
     * @throws IOException
     */
    public boolean copyTo(UntypedStreamInterface destinationStream) throws IOException;

    /**
     * Copy the content from one stream to another, with a progress callback if
     * the underlying transfer mechanism supports it
     *
     * @param destinationStream
     * @param eventHandler
     * @return
     * @throws IOException
     */
    public boolean copyTo(UntypedStreamInterface destinationStream, TransferCallbackInterface eventHandler) throws IOException;

    public FastExtendedInputStreamInterface getInputStream() throws IOException;

    public void closeInputStream() throws IOException;

    public FastExtendedOutputStreamInterface getOutputStream() throws IOException;

    public void closeOutputStream() throws IOException;

    public String getIdentifier();

    public void setIdentifier(String identifier);

    public long getLastModified() throws IOException;

    public boolean setLastModified(long timestamp) throws IOException;
    
    /**
     * Shallow hashcode of the underlying stream
     * Return 0 if unknown
     * @return 
     */
    public int shallowStreamHashCode();
}
