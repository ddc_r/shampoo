/**
 *  This file is part of ftp4che.
 *  This library is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
package biz.ddcr.shampoo.server.io.util.ftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.net.InetSocketAddress;
import java.text.ParseException;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ftp4che.FTPConnection;
import org.ftp4che.commands.Command;
import org.ftp4che.commands.RetrieveCommand;
import org.ftp4che.commands.StoreCommand;
import org.ftp4che.exception.AuthenticationNotSupportedException;
import org.ftp4che.exception.ConfigurationException;
import org.ftp4che.exception.FtpIOException;
import org.ftp4che.exception.FtpWorkflowException;
import org.ftp4che.exception.NotConnectedException;
import org.ftp4che.io.ReplyWorker;
import org.ftp4che.io.SocketProvider;
import org.ftp4che.reply.Reply;
import org.ftp4che.util.ftpfile.FTPFile;

/**
 * @author arnold,kurt
 * @author okay_awright
 *
 * bug fixing for raw inputStreams (NullPointerException)
 * + extra commands compared to FTPConnection
 * UPDATE: removed extra commands, too extensive mod. = tough to catch up with the official version if I need to updgrade
 *
 */
public abstract class FTPConnection2 extends FTPConnection {

    protected final Log logger = LogFactory.getLog(getClass());

    public final static String SIZE = "SIZE";
    public final static String ABOR = "ABOR";

    /**
     * @author arnold,kurt
     * @param password
     *            Get method for the password the FTPConnection will use if
     *            connect() is called
     */
    @Override
    public String getPassword() {
        return super.getPassword();
    }

    /**
     * @author arnold,kurt
     * @param password
     *            Set method for the password the FTPConnection will use if
     *            connect() is called
     */
    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    /**
     * @author arnold,kurt
     * @param user
     *            Set method for the user the FTPConnection will use if
     *            connect() is called
     * @throws ConfigurationException
     *             will be thrown if a parameter is missing or invalid
     */
    @Override
    public void setUser(String user) {
        try {
            super.setUser(user);
        } catch (ConfigurationException ex) {
            LogFactory.getLog(this.getClass()).error("FTP Username is null", ex);
        }
    }

    /**
     * This method is used initaly to set the connection timeout. normal you
     * would set it to 10000 (10 sec.). if you have very slow servers try to set
     * it higher.
     *
     * @param millis
     *            the milliseconds before a timeout will close the connection
     * @author arnold,kurt
     */
    @Override
    public void setTimeout(int millis) {
        super.setTimeout(millis);
    }

    /**
     * This method is used initaly to get the connection timeout. normal you
     * would set it to 10000 (10 sec.). if you have very slow servers try to set
     * it higher.
     *
     * @param millis
     *            the milliseconds before a timeout will close the connection
     * @author arnold,kurt
     */
    @Override
    public int getTimeout() {
        return super.getTimeout();
    }

    /**
     * @param connectionType
     *            The connectionType to set (FTP_CONNECTION,AUTH_SSL_FTP_CONNETION,...)
     */
    @Override
    public void setConnectionType(int connectionType) {
        super.setConnectionType(connectionType);
    }

    /**
     * @return Returns the downloadBandwidth.
     */
    @Override
    public int getDownloadBandwidth() {
        return super.getDownloadBandwidth();
    }

    /**
     * @param downloadBandwidth
     *            The downloadBandwidth to set.
     */
    @Override
    public void setDownloadBandwidth(int maxDownloadBandwidth) {
        super.setDownloadBandwidth(maxDownloadBandwidth);
    }

    /**
     * @return Returns the uploadBandwidth.
     */
    @Override
    public int getUploadBandwidth() {
        return super.getUploadBandwidth();
    }

    /**
     * @param uploadBandwidth
     *            The uploadBandwidth to set.
     */
    @Override
    public void setUploadBandwidth(int maxUploadBandwidth) {
        super.setUploadBandwidth(maxUploadBandwidth);
    }

    /** connect() only if no previous connection was established
     * returns true if a new connection is established */
    public boolean singleConnect() throws NotConnectedException, IOException, AuthenticationNotSupportedException, FtpIOException, FtpWorkflowException {
        if (socketProvider == null || !socketProvider.isConnected()) {
            connect();
            return true;
        }
        return false;
    }

    /**
     * This method is used to init the data socket
     * 
     * @param command
     *            the command you want to send (LIST,RETR,....)
     * @param commmandReply
     *            the reply from the server will be stored in this object
     * @return the new established socket provider
     * @throws IOException
     *             will be thrown if there was a communication problem with the
     *             server
     * @throws FtpWorkflowException
     *             will be thrown if there was a ftp reply class 5xx. in most
     *             cases wrong commands where send
     * @throws FtpIOException
     *             will be thrown if there was a ftp reply class 4xx. this
     *             should indicate some secific problems on the server
     */
    protected SocketProvider initDataSocket(Command command, Reply commandReply)
            throws IOException, FtpIOException, FtpWorkflowException {

        setConnectionStatusLock(CSL_INDIRECT_CALL);
        setConnectionStatus(FTPConnection.BUSY);

        if (isPretSupport()) {
            Command pretCommand = new Command(Command.PRET, command.toString());
            sendCommand(pretCommand).dumpReply();
        }
        InetSocketAddress dataSocket = sendPassiveMode();
        SocketProvider provider = new SocketProvider(false);
        provider.connect(dataSocket, getProxy(), getDownloadBandwidth(),
                getUploadBandwidth());
        provider.setSSLMode(getConnectionType());

        commandReply.setLines(sendCommand(command).getLines());
        commandReply.dumpReply();
        commandReply.validate();

        if (getConnectionType() == FTPConnection.AUTH_TLS_FTP_CONNECTION
                || getConnectionType() == FTPConnection.AUTH_SSL_FTP_CONNECTION
                || getConnectionType() == FTPConnection.IMPLICIT_SSL_WITH_CRYPTED_DATA_FTP_CONNECTION
                || getConnectionType() == FTPConnection.IMPLICIT_TLS_WITH_CRYPTED_DATA_FTP_CONNECTION) {
            provider.negotiate(this.getTrustManagers(), this.getKeyManagers());
        }

        setConnectionStatus(FTPConnection.IDLE);
        setConnectionStatusLock(CSL_DIRECT_CALL);

        return provider;
    }

    public void uploadStream(InputStream upSrc, long upSrcProjectedSize, FTPFile toFile, boolean resume)
            throws IOException, FtpWorkflowException, FtpIOException {

        FTPFile srcInfo = new FTPFile(null, upSrc.toString());
        setConnectionStatusLock(CSL_INDIRECT_CALL);
        setConnectionStatus(SENDING_FILE_STARTED, srcInfo, toFile);
        setConnectionStatus(SENDING_FILE);

        StoreCommand command = new StoreCommand(Command.STOR, upSrc, toFile);
        SocketProvider provider = null;

        if (getConnectionType() == FTPConnection.AUTH_SSL_FTP_CONNECTION
                || getConnectionType() == FTPConnection.AUTH_TLS_FTP_CONNECTION) {
            Command pbsz = new Command(Command.PBSZ, "0");
            (sendCommand(pbsz)).dumpReply();
            Command prot = new Command(Command.PROT, "P");
            (sendCommand(prot)).dumpReply();
        }
        if (upSrcProjectedSize > 0 && (resume || isTryResume())) {
            if (toFile.getSize() <= 0) {
                List<FTPFile> files = getDirectoryListing(toFile.getPath() + toFile.getName());
                if (files != null && !files.isEmpty()) {
                    toFile.setSize(files.get(0).getSize());
                }
            }
            if (toFile.getSize() > 0) {
                if ((upSrcProjectedSize != toFile.getSize())) {
                    command.setResumePosition(toFile.getSize());
                    Command resumeCommand = new Command(Command.REST, "" + toFile.getSize());
                    Reply resumeReply = sendCommand(resumeCommand);
                    resumeReply.dumpReply();
                    try {
                        resumeReply.validate();
                    } catch (FtpWorkflowException fwe) {
                        logger.error("Couldn't resume file, error was: " + fwe.getMessage());
                    } catch (FtpIOException fioe) {
                        logger.error("Couldn't resume file, error was: " + fioe.getMessage());
                    }
                } else {
                    return;
                }
            }
        }
        //Send TYPE I
        /*Command commandType = new Command(connectionTransferType);
        (sendCommand(commandType)).dumpReply();*/
        setTransferType(true); //Force binary

        Reply commandReply = new Reply();
        if (isPassiveMode()) {
            provider = initDataSocket(command, commandReply);
        } else {
            provider = sendPortCommand(command, commandReply);
        }

        command.setDataSocket(provider);
        // INFO response from ControllConnection is ignored
        try {
            command.fetchDataConnectionReply();
        } catch (IOException ioe) {
            setConnectionStatus(ERROR);
            disconnect();
            throw ioe;
        }
        if (commandReply.getLines().size() == 1) {
            try {
                (ReplyWorker.readReply(socketProvider)).dumpReply();
            } catch (IOException ioe) {
                setConnectionStatus(ERROR);
                disconnect();
                throw ioe;
            }
        }

        setConnectionStatus(SENDING_FILE_ENDED, srcInfo, toFile);
        setConnectionStatus(IDLE);
        setConnectionStatusLock(CSL_DIRECT_CALL);
    }

    @Override
    public InputStream downloadStream(FTPFile fromFile) throws IOException, FtpWorkflowException, FtpIOException {

        //JRE6 Only, too bad
        //PipedInputStream pis = new PipedInputStream(IOUtil.DEFAULT_BUFFER_SIZE);
        PipedInputStream pis = new PipedInputStream();

        class DownStreamingThread extends Thread {

            FTPConnection2 connection;
            FTPFile fromFile;
            PipedInputStream pis;

            DownStreamingThread(FTPConnection2 connection, FTPFile fromFile, PipedInputStream pis) {
                super();
                this.connection = connection;
                this.fromFile = fromFile;
                this.pis = pis;
            }

            @Override
            public void run() {
                try {
                    connection.streamFile(fromFile, pis);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }

        (new DownStreamingThread(this, fromFile, pis)).start();

        // ensure that in/out pipes are connected already
        try {
            for (int i = 0; i < 60 && pis.available() <= 0; i++) {
                Thread.sleep(1000);

            }
        } catch (Exception e) {
        }

        return pis;
    }

    protected void streamFile(FTPFile fromFile, PipedInputStream pis) throws IOException, FtpWorkflowException, FtpIOException {

        setConnectionStatusLock(CSL_INDIRECT_CALL);
        setConnectionStatus(RECEIVING_FILE_STARTED, fromFile, null);
        setConnectionStatus(RECEIVING_FILE);

        RetrieveCommand command = new RetrieveCommand(Command.RETR, fromFile, pis);
        SocketProvider provider = null;

        if (getConnectionType() == FTPConnection.AUTH_SSL_FTP_CONNECTION
                || getConnectionType() == FTPConnection.AUTH_TLS_FTP_CONNECTION) {
            Command pbsz = new Command(Command.PBSZ, "0");
            (sendCommand(pbsz)).dumpReply();
            Command prot = new Command(Command.PROT, "P");
            (sendCommand(prot)).dumpReply();
        }

        // Send TYPE I
        /*Command commandType = new Command(connectionTransferType);
        (sendCommand(commandType)).dumpReply();*/
        setTransferType(true); //Force binary

        Reply commandReply = new Reply();
        if (isPassiveMode()) {
            provider = initDataSocket(command, commandReply);
        } else {
            provider = sendPortCommand(command, commandReply);
        }

        command.setDataSocket(provider);
        // INFO response from ControllConnection is ignored
        try {
            command.fetchDataConnectionReply(RetrieveCommand.STREAM_BASED);
        } catch (IOException ioe) {

            //Should we?
            Command abor = new Command(ABOR);
            (sendCommand(abor)).dumpReply();

            setConnectionStatus(ERROR);
            disconnect();
            throw ioe;
        }
        if (commandReply.getLines().size() == 1) {
            try {
                (ReplyWorker.readReply(socketProvider)).dumpReply();
            } catch (IOException ioe) {
                setConnectionStatus(ERROR);
                disconnect();
                throw ioe;
            }
        }

        setConnectionStatus(RECEIVING_FILE_ENDED, fromFile, null);
        setConnectionStatus(IDLE);
        setConnectionStatusLock(CSL_DIRECT_CALL);
    }

    /**
     * This method is used to disconnect from the specified server.
     *
     * @author arnold,kurt
     */
    @Override
    public void disconnect() {
        if (socketProvider != null && socketProvider.isConnected()) {
            super.disconnect();
        }
    }

    public long getSize(FTPFile file) throws IOException, FtpIOException, FtpWorkflowException, ParseException {
        Command size = new Command(SIZE, file.toString());
        Reply reply = sendCommand(size);
        reply.dumpReply();
        reply.validate();
        return ReplyFormattter2.parseSIZEReply(reply);
    }

}
