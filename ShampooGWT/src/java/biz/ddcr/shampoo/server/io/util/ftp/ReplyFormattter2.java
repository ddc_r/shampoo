/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.io.util.ftp;

import java.text.ParseException;
import java.util.List;
import org.ftp4che.exception.UnkownReplyStateException;
import org.ftp4che.reply.Reply;
import org.ftp4che.util.ReplyFormatter;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ReplyFormattter2 extends ReplyFormatter {

    public static long parseSIZEReply(Reply sizeReply) throws ParseException {
        List<String> lines = sizeReply.getLines();
        if (lines.size() != 1) {
            throw new UnkownReplyStateException(
                    "SIZE Reply has to have a size of 1 entry but it has: "
                    + lines.size());
        }
        String line = lines.get(0);
        return Long.decode(line.substring(line.indexOf(' ') + 1).replaceAll("\\s+", ""));
    }
}
