/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.archive;

import biz.ddcr.shampoo.client.form.archive.*;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.archive.*;
import biz.ddcr.shampoo.server.domain.archive.metadata.SimpleStreamableArchiveMetadata;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.helper.TaskPerformer;
import biz.ddcr.shampoo.server.io.serializer.MarshallableSupporter;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataCollection;
import biz.ddcr.shampoo.server.io.util.TextStream;
import biz.ddcr.shampoo.server.io.util.UntypedPipedWriterStream;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author okay_awright
 *
 */
public class ArchiveDTOAssembler extends GenericService implements ArchiveDTOAssemblerInterface {

    /**
     * dummy marker class for form export *
     */
    private interface ArchiveVisitorDTO extends ArchiveVisitor {

        public ArchiveForm getForm();
    };
    private ChannelManagerInterface channelManager;

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public static ArchiveFormID toIdDTO(Archive archive, String localTimeZone) throws Exception {
        ArchiveFormID newId = new ArchiveFormID();

        newId.setRefID(archive.getRefID());
        newId.setChannelId(archive.getChannel().getLabel());
        if (localTimeZone != null && localTimeZone.length() != 0) {
            newId.setSchedulingTime(DateDTOAssembler.toJS(archive.getStartTime().switchTimeZone(localTimeZone)));
        } else {
            newId.setSchedulingTime(DateDTOAssembler.toJS(archive.getStartTime()));
        }

        return newId;
    }

    @Override
    public ActionCollectionEntry<? extends ArchiveForm> toDTO(Archive archive) throws Exception {
        return toDTO(archive, null);
    }

    @Override
    public ActionCollectionEntry<? extends ArchiveForm> toDTO(Archive archive, String localTimeZone) throws Exception {
        if (archive == null) {
            return null;
        }

        ArchiveVisitorDTO archiveVisitor = new ArchiveVisitorDTO() {
            ArchiveForm archiveModule = null;

            @Override
            public ArchiveForm getForm() {
                return archiveModule;
            }

            @Override
            public void visit(NonQueuedLiveArchive entry) throws Exception {
                archiveModule = new LiveArchiveForm();
                ((LiveArchiveForm) archiveModule).setBroadcasterCaption(entry.getBroadcasterCaption());
            }

            @Override
            public void visit(QueuedLiveArchive entry) throws Exception {
                archiveModule = new LiveArchiveForm();
                ((LiveArchiveForm) archiveModule).setBroadcasterCaption(entry.getBroadcasterCaption());
            }

            @Override
            public void visit(BroadcastableTrackArchive entry) throws Exception {
                archiveModule = new BroadcastableTrackArchiveForm();
                ((BroadcastableTrackArchiveForm) archiveModule).setTrackAuthorCaption(entry.getTrackAuthorCaption());
                ((BroadcastableTrackArchiveForm) archiveModule).setTrackTitleCaption(entry.getTrackTitleCaption());
            }

            @Override
            public void visit(RequestArchive entry) throws Exception {
                archiveModule = new RequestArchiveForm();
                ((RequestArchiveForm) archiveModule).setTrackAuthorCaption(entry.getTrackAuthorCaption());
                ((RequestArchiveForm) archiveModule).setTrackTitleCaption(entry.getTrackTitleCaption());
            }
        };
        archive.acceptVisit(archiveVisitor);

        if (archiveVisitor.getForm() != null) {
            //Fill in common properties
            archiveVisitor.getForm().setRefId(toIdDTO(archive, localTimeZone));
            archiveVisitor.getForm().setDuration(new JSDuration(archive.getDuration()));
            archiveVisitor.getForm().setPlaylistFriendlyId(archive.getPlaylistCaption());
            archiveVisitor.getForm().setProgrammeFriendlyId(archive.getProgrammeCaption());

            return new ActionMapEntry<ArchiveForm>(archiveVisitor.getForm(), decorateEntryWithActions(archive));
        } else {
            return null;
        }
    }

    protected Collection<ACTION> decorateEntryWithActions(Archive archive) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewArchive(archive.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteArchive(archive.getChannel())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    @Override
    public ActionCollection<? extends ArchiveForm> toDTO(Collection<Archive> archives, boolean doSort) throws Exception {
        return toDTO(archives, null, doSort);
    }

    @Override
    public ActionCollection<? extends ArchiveForm> toDTO(Collection<Archive> archives, String localTimeZone, boolean doSort) throws Exception {
        ActionMap<ArchiveForm> archiveModules = doSort ? new ActionSortedMap<ArchiveForm>() : new ActionUnsortedMap<ArchiveForm>();
        for (Archive archive : archives) {
            archiveModules.set(toDTO(archive, localTimeZone));
        }
        return archiveModules;
    }

    public static StreamableArchiveMetadataInterface toSerializableDTO(Archive archive) throws Exception {
        if (archive == null) {
            return null;
        }
        final SimpleStreamableArchiveMetadata archiveMetadata = new SimpleStreamableArchiveMetadata();

        archiveMetadata.setChannelLabel(archive.getChannel() != null ? archive.getChannel().getLabel() : null);
        archiveMetadata.setDuration(archive.getDuration() >= 0 ? archive.getDuration() : null);
        archiveMetadata.setPlaylistLabel(archive.getPlaylistCaption());
        archiveMetadata.setProgrammeLabel(archive.getProgrammeCaption());
        archiveMetadata.setStartTime(archive.getStartTime() != null ? archive.getStartTime().getUnixTime() : null);
        archiveMetadata.setTimezone(archive.getStartTime() != null ? archive.getStartTime().getTimeZone() : null);

        //Fill in the main attributes
        ArchiveVisitor archiveVisitor = new ArchiveVisitor() {
            @Override
            public void visit(NonQueuedLiveArchive entry) throws Exception {
                archiveMetadata.setAuthor(entry.getBroadcasterCaption());
                archiveMetadata.setTypeCaption("live");
            }

            @Override
            public void visit(QueuedLiveArchive entry) throws Exception {
                archiveMetadata.setAuthor(entry.getBroadcasterCaption());
                archiveMetadata.setTypeCaption("live");
            }

            @Override
            public void visit(BroadcastableTrackArchive entry) throws Exception {
                archiveMetadata.setAuthor(entry.getTrackAuthorCaption());
                archiveMetadata.setCopyright(entry.getTrackCopyrightCaption());
                archiveMetadata.setPublisher(entry.getTrackPublisherCaption());
                archiveMetadata.setTitle(entry.getTrackTitleCaption());
                archiveMetadata.setTypeCaption("track");
            }

            @Override
            public void visit(RequestArchive entry) throws Exception {
                archiveMetadata.setAuthor(entry.getTrackAuthorCaption());
                archiveMetadata.setCopyright(entry.getTrackCopyrightCaption());
                archiveMetadata.setPublisher(entry.getTrackPublisherCaption());
                archiveMetadata.setTitle(entry.getTrackTitleCaption());
                archiveMetadata.setTypeCaption("track");
            }
        };
        archive.acceptVisit(archiveVisitor);

        return archiveMetadata;
    }

    /**
     * Non-streaming version
     * Don't use it for high-volume data
     */
    public static SerializableMetadataCollection<StreamableArchiveMetadataInterface> toSerializableDTO(Collection<Archive> archives) throws Exception {
        //LikedHashSet gathers the best of Set features (no null, no duplicates) and the best of List features (fixed order) at the cost of some memory overhead
        LinkedHashSet<StreamableArchiveMetadataInterface> archiveMetadata = new LinkedHashSet<StreamableArchiveMetadataInterface>();
        for (Archive archive : archives) {
            archiveMetadata.add(toSerializableDTO(archive));
        }
        return SerializableMetadataCollection.wrap(archiveMetadata);
    }

    /**
     * Streaming version
     * Use that for high-volume data
     */
    public static UntypedPipedWriterStream toSerializableDTO(String id, final Collection<Archive> archives, final TaskPerformer taskPerformer) throws Exception {
        return new UntypedPipedWriterStream(id, taskPerformer) {
            @Override
            public void feed(OutputStream outputStream) throws Exception {
                ObjectOutputStream objectStream = new ObjectOutputStream(new DeflaterOutputStream(outputStream));
                try {
                    //First write number of items that will be written
                    objectStream.writeLong(archives.size());
                    //Then each individual item
                    for (Archive archive : archives) {
                        StreamableArchiveMetadataInterface object = toSerializableDTO(archive);
                        //if (object != null) {
                            objectStream.writeObject(object);
                        //}
                    }
                    objectStream.flush();
                } finally {
                    objectStream.close();
                }
            }

            @Override
            public int shallowStreamHashCode() {
                //Cannot be computed
                return 0;
            }
        };

    }
    /**
     * Streaming version
     * Use that for high-volume data
     */
    public static SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface> fromSerializableDTO(String id, final TextStream stream, final TaskPerformer taskPerformer) throws Exception {

        final Iterator<StreamableArchiveMetadataInterface> streamingIterator = new Iterator<StreamableArchiveMetadataInterface>() {

            private ObjectInputStream objectStream;
            //First read the number of items that are contained in this stream
            private long remainingItems = -1;
            
            private void prepareStream() throws IOException {
                if (objectStream==null) {
                    objectStream = new ObjectInputStream(new InflaterInputStream(stream.getRawStream().getInputStream().getBufferedBackend()));
                    remainingItems = objectStream.readLong();
                }
            }
            
            @Override
            public boolean hasNext() {
                try {
                    prepareStream();
                    return remainingItems>0;
                } catch (IOException ex) {
                    LogFactory.getLog(getClass()).error("Cannot deserialize object", ex);
                    throw new RuntimeException(ex);
                }
            }

            @Override
            public StreamableArchiveMetadataInterface next() {
                try {
                    prepareStream();
                    remainingItems--;
                    StreamableArchiveMetadataInterface s = (StreamableArchiveMetadataInterface)objectStream.readObject();
                    //Try to guess when to clean up: should be safe since everything is always read in one go
                    if (!hasNext()) objectStream.close();
                    return s;
                } catch (IOException ex) {
                    LogFactory.getLog(getClass()).error("Cannot deserialize object", ex);
                    throw new RuntimeException(ex);
                } catch (ClassNotFoundException ex) {
                    LogFactory.getLog(getClass()).error("Cannot deserialize object", ex);
                    throw new RuntimeException(ex);
                }
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported yet");
            }
        };
        
        return new SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>() {

                @Override
                public Iterator<StreamableArchiveMetadataInterface> iterator() {
                    return streamingIterator;
                }

                @Override
                public void supports(MarshallableSupporter otherObject) throws Exception {
                    otherObject.support(this);
                }

                @Override
                public int fullHashCode() {
                    //Returns 0 because it is streaming, we cannot compute it
                    return 0;
                }
                
            };
    }
}
