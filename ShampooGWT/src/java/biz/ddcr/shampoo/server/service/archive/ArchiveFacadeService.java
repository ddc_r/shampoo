/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.archive;

import biz.ddcr.shampoo.client.form.archive.ArchiveForm;
import biz.ddcr.shampoo.client.form.archive.ArchiveForm.ARCHIVE_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.ArchiveInterface;
import biz.ddcr.shampoo.server.domain.archive.log.ArchiveLog;
import biz.ddcr.shampoo.server.domain.archive.log.ArchiveLog.ARCHIVE_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelArchiveLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelArchiveLinkNotification.CHANNELARCHIVE_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 *
 */
public class ArchiveFacadeService extends GenericService implements ArchiveFacadeServiceInterface {
    
    private ArchiveDTOAssemblerInterface archiveDTOAssembler;
    private ChannelManagerInterface channelManager;
    private SecurityManagerInterface securityManager;
    
    public ArchiveDTOAssemblerInterface getArchiveDTOAssembler() {
        return archiveDTOAssembler;
    }

    public void setArchiveDTOAssembler(ArchiveDTOAssemblerInterface archiveDTOAssembler) {
        this.archiveDTOAssembler = archiveDTOAssembler;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }
    
    @Override
    public void deleteSecuredArchive(String id) throws Exception {

        if (id != null) {

            Archive archive = getChannelManager().loadArchive(id);
            //Then check if it can be accessed

            if (getChannelManager().checkAccessDeleteArchive(archive.getChannel())) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = ARCHIVE_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), archive);
                //Save original links before modification (clones)
                Channel originallyLinkedChannel = archive.getChannel();

                //Whatever happens, someone who can remove a archive can unlink other stuff bound to it: no need for extra check
                //Cascade delete will take care of their unlinking

                //Everything's fine, now make it persistent in the database
                getChannelManager().deleteArchive(archive);
                //Log it
                log(ARCHIVE_LOG_OPERATION.delete, archive, originallyBoundChannels);
                //Notify responsible users
                notify(CHANNELARCHIVE_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, archive);

            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredArchives(Collection<String> ids) throws Exception {
        if (ids != null) {

            Collection archivesToDelete = new HashSet<ArchiveInterface>();
            for (String id : ids) {
                //First get a copy of the entity from a DTO
                ArchiveInterface archive = getChannelManager().loadArchive(id);

                if (getChannelManager().checkAccessDeleteArchive(archive.getChannel())) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = ARCHIVE_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), archive);
                    //Save original links before modification (clones)
                    Channel originallyLinkedChannel = archive.getChannel();

                    archivesToDelete.add(archive);
                    //Log it
                    log(ARCHIVE_LOG_OPERATION.delete, archive, originallyBoundChannels);
                    //Notify responsible users
                    notify(CHANNELARCHIVE_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, archive);

                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!archivesToDelete.isEmpty()) {

                getChannelManager().deleteArchives(archivesToDelete);

            }
        }
    }

    @Override
    public ActionCollectionEntry<? extends ArchiveForm> getSecuredArchive(String id) throws Exception {
        Archive archive = getChannelManager().loadArchive(id);
        ActionCollectionEntry<? extends ArchiveForm> archiveModuleEntry = archiveDTOAssembler.toDTO(archive);
        if (archiveModuleEntry == null)
            throw new NoEntityException();
        else if (archiveModuleEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(ARCHIVE_LOG_OPERATION.read, archive);
            return archiveModuleEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<? extends ArchiveForm> getSecuredArchives(GroupAndSort<ARCHIVE_TAGS> constraint, String localTimeZone) throws Exception {
        Collection<Archive> archives = getChannelManager().getFilteredArchives(constraint);
        ActionCollection<? extends ArchiveForm> forms = archiveDTOAssembler.toDTO(archives, localTimeZone, constraint==null || constraint.getSortByFeature()==null);

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(ARCHIVE_LOG_OPERATION.read, archives);

        return forms;
    }

    @Override
    public ActionCollection<? extends ArchiveForm> getSecuredArchivesForChannelId(GroupAndSort<ARCHIVE_TAGS> constraint, String channelId, String localTimeZone) throws Exception {
        Collection<Archive> archives = getChannelManager().getFilteredArchivesForChannelId(constraint, channelId);
        ActionCollection<? extends ArchiveForm> forms = archiveDTOAssembler.toDTO(archives, localTimeZone, constraint==null || constraint.getSortByFeature()==null);

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(ARCHIVE_LOG_OPERATION.read, archives);
        return forms;
    }

    protected void log(ARCHIVE_LOG_OPERATION operation, Collection<ArchiveInterface> archives) throws Exception {
        if (archives != null && !archives.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<ArchiveLog> logs = new HashSet<ArchiveLog>();
            for (ArchiveInterface archive : archives) {
                logs.add(
                        new ArchiveLog(
                        operation,
                        archive.getRefID(),
                        archive.getStartTime(),
                        archive.getChannel() != null ? archive.getChannel().getLabel() : null,
                        operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), archive)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(ARCHIVE_LOG_OPERATION operation, ArchiveInterface archive) throws Exception {
        log(operation, archive, null);
    }

    protected void log(ARCHIVE_LOG_OPERATION operation, ArchiveInterface archive, Collection<Channel> supplementaryChannels) throws Exception {
        if (archive != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), archive);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(
                    new ArchiveLog(
                    operation,
                    archive.getRefID(),
                    archive.getStartTime(),
                    archive.getChannel() != null ? archive.getChannel().getLabel() : null,
                    allBoundChannels));
        }
    }

    protected void notify(CHANNELARCHIVE_NOTIFICATION_OPERATION operation, Channel channel, ArchiveInterface archive) throws Exception {
        notify(operation, channel, archive, operation.getRecipients(getSecurityManager(), channel, archive));
    }

    protected void notify(CHANNELARCHIVE_NOTIFICATION_OPERATION operation, Channel channel, ArchiveInterface archive, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && archive != null && recipients != null) {
            Collection<ChannelArchiveLinkNotification> notifications = new HashSet<ChannelArchiveLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelArchiveLinkNotification(operation, channel, archive, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
    
}
