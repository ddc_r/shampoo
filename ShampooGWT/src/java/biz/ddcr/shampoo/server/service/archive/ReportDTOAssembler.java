/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.archive;

import biz.ddcr.shampoo.client.form.archive.*;
import biz.ddcr.shampoo.client.form.archive.format.TEXT_TYPE;
import biz.ddcr.shampoo.client.form.archive.format.TextModule;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.archive.*;
import biz.ddcr.shampoo.server.domain.archive.format.TextFileInfo;
import biz.ddcr.shampoo.server.domain.archive.format.TextFormat.TEXT_FORMAT;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 *
 */
public class ReportDTOAssembler extends GenericService implements ReportDTOAssemblerInterface {

    private ChannelManagerInterface channelManager;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public static TEXT_FORMAT fromDTO(TEXT_TYPE textType) {
        if (textType == null) {
            return null;
        }
        switch (textType) {
            case structured:
                return TEXT_FORMAT.structured;
            default:
                return null;
        }
    }

    public static TEXT_TYPE toDTO(TEXT_FORMAT textType) {
        if (textType == null) {
            return null;
        }
        switch (textType) {
            case structured:
                return TEXT_TYPE.structured;
            default:
                return null;
        }
    }
    
    public static TextFileInfo fromDTO(TextModule file) {
        if (file != null) {
            TextFileInfo container = new TextFileInfo();

            //Format
            container.setFormat(fromDTO(file.getFormat()));

            //metadata
            container.setSize(file.getSize());
            return container;
        }
        return null;
    }
    
    public TextModule toTextDTO(Report report) throws Exception {
        if (report != null && report.getTextContainer() != null) {
            TextFileInfo info = report.getTextContainer();
            TextModule container = new TextModule();

            //Format
            container.setFormat(toDTO(info.getFormat()));

            //metadata
            container.setSize(info.getSize());

            //Download info
            container.setURL(
                    getDatastoreService().getGWTTextDownloadURLEndpointForUser(report.getRefID()));

            return container;
        }
        return null;
    }
    
    @Override
    public Report fromDTO(ReportForm reportForm) throws Exception {
        if (reportForm == null) {
            return null;
        }

        Report report = new Report(//
                getChannelManager().getChannel(reportForm.getLocation().getChannelId()),//
                DateDTOAssembler.fromJS(reportForm.getLocation().getFromTime()),//
                DateDTOAssembler.fromJS(reportForm.getLocation().getToTime())
        );
        //setReady() handled elsewhere
        report.setTextContainer(fromDTO(reportForm.getTextFile()));

        return report;
    }

    @Override
    public boolean augmentWithDTO(Report report, ReportForm form) throws Exception {
        boolean contentUpdated = false;

        if (form != null && report != null) {

            //Move over all regular properties and the identifier

            //Don't check if the programme bound to the playlist can be updated; this right is implied with the updatePlaylist check
            if (getChannelManager().checkAccessUpdateReport(report.getChannel())) {
                report.removeChannel();
                Channel c = getChannelManager().getChannel(form.getLocation().getChannelId());
                if (getChannelManager().checkAccessUpdateReport(c)) {
                    report.addChannel(c);
                } else {
                    throw new AccessDeniedException();
                }
            }

            //Entries
            //There's no permission issue with entries: even if a user can't read a Track attached to an entry he may remove or edit the whole row if he's allowed to touch the playlist

            //Hibernate is Hell on Earth! Lots of tweaks and non-portable tricks must be carried out to programmatically detect and intercept changes between two collections

            //1) Generate the new list of entries from the DTO
            //2) for each entry in the old list check if it's included in the new list
            //2.1) if it is, then drop the matching entry in the new list
            //2.2) if it's not, drop the unmatched entry from the old list + mark the playlist as tainted
            //3) add remaining entries from the new list into the old one + mark the playlist as tainted

        }
        return contentUpdated;
    }
    
    public static ReportFormID toIdDTO(Report report, String localTimeZone) throws Exception {
        ReportFormID newId = new ReportFormID();

        newId.setRefID(report.getRefID());
        newId.setChannelId(report.getChannel().getLabel());
        if (localTimeZone != null && localTimeZone.length() != 0) {
            newId.setFromTime(DateDTOAssembler.toJS(report.getStartTime().switchTimeZone(localTimeZone)));
            newId.setToTime(DateDTOAssembler.toJS(report.getEndTime().switchTimeZone(localTimeZone)));
        } else {
            newId.setFromTime(DateDTOAssembler.toJS(report.getStartTime()));
            newId.setToTime(DateDTOAssembler.toJS(report.getEndTime()));
        }

        return newId;
    }
    
    @Override
    public ActionCollectionEntry<ReportForm> toDTO(Report report) throws Exception {
        return toDTO(report, null);
    }

    @Override
    public ActionCollectionEntry<ReportForm> toDTO(Report report, String localTimeZone) throws Exception {
        if (report == null) {
            return null;
        }

        ReportForm newForm = new ReportForm();
        
        newForm.setReady(report.isReady());
        newForm.setTextFile(toTextDTO(report));
        
        newForm.setRefId(toIdDTO(report, localTimeZone));
        if (localTimeZone != null && localTimeZone.length() != 0) {
            newForm.getLocation().setFromTime(DateDTOAssembler.toJS(report.getStartTime().switchTimeZone(localTimeZone)));
            newForm.getLocation().setToTime(DateDTOAssembler.toJS(report.getEndTime().switchTimeZone(localTimeZone)));
        } else {
            newForm.getLocation().setFromTime(DateDTOAssembler.toJS(report.getStartTime()));
            newForm.getLocation().setToTime(DateDTOAssembler.toJS(report.getEndTime()));
        }
        newForm.setDataDownloadURL(getDatastoreService().getGWTTextDownloadURLEndpointForUser(report.getRefID()));

        return new ActionMapEntry<ReportForm>(newForm, decorateEntryWithActions(report));
    }

    protected Collection<ACTION> decorateEntryWithActions(Report report) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewReport(report.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteReport(report.getChannel())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    @Override
    public ActionCollection<ReportForm> toDTO(Collection<Report> reports, boolean doSort) throws Exception {
        return toDTO(reports, null, doSort);
    }

    @Override
    public ActionCollection<ReportForm> toDTO(Collection<Report> reports, String localTimeZone, boolean doSort) throws Exception {
        ActionMap<ReportForm> reportModules = doSort ? new ActionSortedMap<ReportForm>() : new ActionUnsortedMap<ReportForm>();
        for (Report report : reports) {
            reportModules.set(toDTO(report, localTimeZone));
        }
        return reportModules;
    }

    @Override
    public Channel getImmutableChannel(ReportForm report) throws Exception {
        if (report != null) {
            return getChannelManager().getChannel(report.getLocation().getChannelId());
        }
        return null;
    }

}
