/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.archive;

import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ReportDTOAssemblerInterface {
    
    //Domain to DTO conversion
    public ActionCollectionEntry<ReportForm> toDTO(Report report, String localTimeZone) throws Exception;
    public ActionCollectionEntry<ReportForm> toDTO(Report report) throws Exception;
    public boolean augmentWithDTO(Report report, ReportForm reportForm) throws Exception;

    public Report fromDTO(ReportForm reportForm) throws Exception;
    
    //Helpers
    public Channel getImmutableChannel(ReportForm report) throws Exception;
    public ActionCollection<ReportForm> toDTO(Collection<Report> reports, String localTimeZone, boolean doSort) throws Exception;
    public ActionCollection<ReportForm> toDTO(Collection<Report> reports, boolean doSort) throws Exception;

}