/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.archive;

import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.form.archive.ReportForm.REPORT_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DateFormatException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.ReportNotReadyException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.archive.log.ReportLog;
import biz.ddcr.shampoo.server.domain.archive.log.ReportLog.REPORT_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelReportLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelReportLinkNotification.CHANNELREPORT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.archive.format.TextFileInfo;
import biz.ddcr.shampoo.server.domain.archive.format.TextFormat.TEXT_FORMAT;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnit;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import biz.ddcr.shampoo.server.helper.MicroTask;
import biz.ddcr.shampoo.server.helper.TaskCallback;
import biz.ddcr.shampoo.server.helper.TaskPerformer;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainer;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.io.util.TextStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedPipedWriterStream;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.archive.ReportFacadeServiceInterface.AsynchronousAddReportTransactionInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface.ReportFiles;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.core.task.TaskExecutor;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 *
 */
public class ReportFacadeService extends GenericService implements ReportFacadeServiceInterface {

    /**
     * Internal interim struct
     *
     */
    protected class AsynchronousDeleteOperation extends AsynchronousUnmanagedTransactionUnit<Report> implements AsynchronousDeleteReportTransactionInterface {

        AsynchronousDeleteOperation() throws Exception {
            super();
            //Init
            idsAndBackups = new HashMap<String, ReportForm>();
        }
        protected Map<String, ReportForm> idsAndBackups;

        @Override
        public void add(String id) {
            if (id != null) {
                idsAndBackups.put(id, null);
            }
        }

        @Override
        public void init() throws Exception {
            //Mark all tracks to delete as unavailable
            clearTransactions();
            synchronousSafePreDeleteReports(idsAndBackups);
        }

        @Override
        public void processing() throws Exception {
            //Remove all files attached to the tracks that have no remaining programme attached to them
            //otherwise just reset their activation state
            addTransactions(synchronousSafePostDeleteReports(idsAndBackups));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to do, really
        }

        @Override
        public void rollback(Exception originalException) {
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Rollback any modification performed during initialization() and mark them as ready
                Collection<Report> cleansedReports = new HashSet<Report>();
                for (ReportForm backup : idsAndBackups.values()) {
                    Report contaminatedReport = getChannelManager().loadReport(backup.getRefId().getRefID());
                    getReportDTOAssembler().augmentWithDTO(contaminatedReport, backup);
                    contaminatedReport.setReady(true);
                    cleansedReports.add(contaminatedReport);
                }
                getChannelManager().updateReports(cleansedReports);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    protected class AsynchronousAddOperation extends AsynchronousUnmanagedTransactionUnit<Report> implements AsynchronousAddReportTransactionInterface {

        AsynchronousAddOperation() throws Exception {
            super();
            //Init
            files = new HashMap<String, ReportFiles>();
            reports = new HashSet<ReportForm>();
        }
        protected Map<String, ReportFiles> files;
        protected Collection<ReportForm> reports;

        public void add(ReportForm report, String textUploadId) {
            //if trackUploadId or textUploadId is null, it means the corresponding entry for the track has to be removed
            //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
            if (report != null) {
                if (report.getRefId().getRefID() == null) {
                    report.getRefId().setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }                
                ReportFiles newReportFile = new ReportFiles(
                        true, textUploadId);
                files.put(report.getRefId().getRefID(), newReportFile);
                reports.add(report);
            }
        }

        @Override
        public void add(ReportForm report) {
            //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
            if (report != null) {
                if (report.getRefId().getRefID() == null) {
                    report.getRefId().setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }            
                files.put(report.getRefId().getRefID(), null);
                reports.add(report);
            }
        }

        @Override
        public void init() throws Exception {
            clearTransactions();
            synchronousSafePreAddReports(reports);
        }

        @Override
        public void processing() throws Exception {
            //Add and bind all files to the tracks that have just been inserted
            addTransactions(synchronousSafePostAddReports(files));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to perform
        }

        @Override
        public void rollback(Exception originalException) {
            //Cancel the persistance of the changes in the datastore
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Remove all files that have been inserted during initialization()
                Collection<Report> reportsToUpdate = getChannelManager().loadReports(files.keySet());
                getChannelManager().deleteReports(reportsToUpdate);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };
    private ReportDTOAssemblerInterface reportDTOAssembler;
    private ArchiveDTOAssemblerInterface archiveDTOAssembler;
    private ChannelManagerInterface channelManager;
    private SecurityManagerInterface securityManager;
    //Run sequential steps in an asynchronous thread
    private transient TaskPerformer taskPerformer;
    private AsynchronousReportFacadeServiceHelperInterface asynchronousReportFacadeServiceHelper;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public ArchiveDTOAssemblerInterface getArchiveDTOAssembler() {
        return archiveDTOAssembler;
    }

    public void setArchiveDTOAssembler(ArchiveDTOAssemblerInterface archiveDTOAssembler) {
        this.archiveDTOAssembler = archiveDTOAssembler;
    }

    public ReportDTOAssemblerInterface getReportDTOAssembler() {
        return reportDTOAssembler;
    }

    public void setReportDTOAssembler(ReportDTOAssemblerInterface reportDTOAssembler) {
        this.reportDTOAssembler = reportDTOAssembler;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskPerformer = new TaskPerformer(taskExecutor);
    }

    public AsynchronousReportFacadeServiceHelperInterface getAsynchronousReportFacadeServiceHelper() {
        return asynchronousReportFacadeServiceHelper;
    }

    public void setAsynchronousReportFacadeServiceHelper(AsynchronousReportFacadeServiceHelperInterface asynchronousReportFacadeServiceHelper) {
        this.asynchronousReportFacadeServiceHelper = asynchronousReportFacadeServiceHelper;
    }

    protected void startAsynchronousTask(AsynchronousUnmanagedTransactionUnitInterface<Report> asynchronousReportOps) throws Exception {
        Collection<AsynchronousUnmanagedTransactionUnitInterface<Report>> asynchronousReportOpsCollection = new HashSet<AsynchronousUnmanagedTransactionUnitInterface<Report>>();
        asynchronousReportOpsCollection.add(asynchronousReportOps);
        startAsynchronousTask(asynchronousReportOpsCollection);
    }

    /**
     * MUST be called outside any transactionnal scope. Will use transactional
     * methods once embedded within an asynchronous workers.
     *
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     *
     */
    protected void startAsynchronousTask(final Collection<AsynchronousUnmanagedTransactionUnitInterface<Report>> asynchronousReportOps) throws Exception {
        //No batch version of this method exists, process those tracks one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (asynchronousReportOps != null) {

            //First step: initialization of units of work
            getAsynchronousReportFacadeServiceHelper().synchronousInitialization(asynchronousReportOps);

            //Second step: actual processing
            MicroTask loop = new MicroTask() {
                @Override
                public boolean loop() throws Exception {
                    getAsynchronousReportFacadeServiceHelper().synchronousProcessing(asynchronousReportOps);
                    return false;
                }
            };
            TaskCallback loopCallback = new TaskCallback() {
                @Override
                public void onSuccess() throws Exception {
                    getAsynchronousReportFacadeServiceHelper().synchronousCommit(asynchronousReportOps);
                }

                @Override
                public void onFailure(Exception e) throws Exception {
                    getAsynchronousReportFacadeServiceHelper().synchronousRollback(asynchronousReportOps, e);
                }
            };
            taskPerformer.runTask(loop, loopCallback);

        }
    }

    /**
     *
     * Validation process for reports Incorrect values are replaced with default
     * ones unless the problem is not recoverable, then an Exception is thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     *
     */
    private boolean checkReport(Report report) throws Exception {
        boolean result = false;

        if (report != null) {

            //start time must be greater than end time
            if (report.getStartTime() != null) {
                if (report.getStartTime().compareTo(report.getEndTime()) > 0) {
                    YearMonthWeekDayHourMinuteSecondMillisecondInterface _tmp = report.getEndTime();
                    report.getLocation().setRawEndTime(report.getStartTime());
                    report.getLocation().setRawStartTime(_tmp);
                }

                if (report.getEndTime() != null) {
                    //fromDate must not be set more than one year ahead of toDate, use a safe guard and add one day to a year
                    if (report.getEndTime().getUnixTime() - report.getStartTime().getUnixTime() > (31558464000L/*
                             * One year
                             */ + 86400000L/*
                             * one day
                             */)) {
                        throw new DateFormatException("Cannot set date intervals greater than one year");
                    }

                    result = true;
                }
            }
        }

        return result;
    }

    @Override
    public void asynchronousAddSecuredReport(final ReportForm reportForm) throws Exception {
        if (reportForm != null) {

            AsynchronousAddReportTransactionInterface op = new AsynchronousAddOperation();
            op.add(reportForm);
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    @Override
    public void asynchronousDeleteSecuredReport(String reportId) throws Exception {
        if (reportId != null) {
            Collection<String> reportIds = new HashSet<String>();
            reportIds.add(reportId);
            asynchronousDeleteSecuredReports(reportIds);
        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once
     * embedded within an asynchronous workers.
     *
     * @param playlistForm
     * @param playlistUploadId
     * @throws Exception
     *
     */
    @Override
    public void asynchronousDeleteSecuredReports(Collection<String> reportIds) throws Exception {
        //No batch version of this method exists, process those playlists one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (reportIds != null) {

            AsynchronousDeleteReportTransactionInterface op = new AsynchronousDeleteOperation();
            for (String reportId : reportIds) {
                op.add(reportId);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    /*public void addSecuredReport(ReportForm reportForm) throws Exception {
     if (reportForm != null) {

     //hydrate the entity from the DTO
     Report report = reportDTOAssembler.fromDTO(reportForm);
     //Then check if it can be accessed
     if (getChannelManager().checkAccessAddReport(report.getChannel())) {

     //Check and adjust its dependencies before insertion
     if (checkReport(report)) {
     //Everything's fine, now make it persistent in the database
     try {
     getChannelManager().addReport(report);
     //Log it
     log(REPORT_LOG_OPERATION.add, report);
     //Notify responsible users of any changes
     //All linked entities are then new links
     if (report.getChannel() != null) {
     notify(CHANNELREPORT_NOTIFICATION_OPERATION.add, report.getChannel(), report);
     }

     } catch (DataIntegrityViolationException e) {
     throw new DuplicatedEntityException(e.getClass().getCanonicalName());
     }
     }
     } else {
     throw new AccessDeniedException();
     }
     }
     }*/
    protected void synchronousSafePreAddReports(Collection<ReportForm> reportForms) throws Exception {
        if (reportForms != null) {

            //Check if it can be accessed
            if (getChannelManager().checkAccessAddReports()) {

                Collection<Report> addedReports = new HashSet<Report>();
                for (ReportForm reportForm : reportForms) {
                    //hydrate the entity from the DTO
                    Report report = reportDTOAssembler.fromDTO(reportForm);
                    if (checkReport(report)) {
                        //Mark it as not ready
                        report.setReady(false);
                        //IMPORTANT: update the original form with the new id!
                        report.setRefID(reportForm.getRefId().getRefID());
                        addedReports.add(report);
                    }
                }
                //Everything's fine, now make it persistent in the database
                try {
                    //Add all reports to insert
                    getChannelManager().addReports(addedReports);
                    //Log and notify only when the actual file copy has been performed
                } catch (DataIntegrityViolationException e) {
                    throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                }
            } else {
                throw new AccessDeniedException();
            }

        }

    }

    protected Collection<Integer> synchronousSafePostAddReports(
            Map<String, ReportFiles> reportIdsAndFiles) throws Exception {

        if (reportIdsAndFiles != null) {
            Collection<Integer> transactions = null;
            Map<Report, ReportFiles> reportsToReset = new HashMap<Report, ReportFiles>();

            final Collection<Report> reports = getChannelManager().loadReports(reportIdsAndFiles.keySet());
            if (reports != null) {
                for (Report report : reports) {

                    //reset its activation state
                    report.setReady(true);
                    reportsToReset.put(report, reportIdsAndFiles.get(report.getRefID()));
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    log(REPORT_LOG_OPERATION.add, report);
                    //Notify responsible users of any changes
                    //All linked entities are then new links
                    if (report.getChannel() != null) {
                        notify(CHANNELREPORT_NOTIFICATION_OPERATION.add, report.getChannel(), report);
                    }

                }
            }
            if (!reportsToReset.isEmpty()) {

                //Fetch the structures
                for (Map.Entry<Report, ReportFiles> reportToReset : reportsToReset.entrySet()) {
                    Report report = reportToReset.getKey();
                    if (report != null) {

                        //Stream the result
                        UntypedPipedWriterStream outputStream = ArchiveDTOAssembler.toSerializableDTO(//
                                report.getRefID(),//
                                getChannelManager().getArchivesForChannelId(report.getChannel(), report.getStartTime(), report.getEndTime()),//
                                taskPerformer);

                        //Reinject it to the datastore
                        TextFileInfo textFileInfo = new TextFileInfo();
                        textFileInfo.setSize(-1);
                        textFileInfo.setFormat(TEXT_FORMAT.structured);
                        TextStream objectStream = new TextStream(textFileInfo, outputStream);
                        if (objectStream != null) {
                            setSecuredStreamToSessionPool(report.getRefID(), objectStream);
                            reportsToReset.put(report, new ReportFiles(true, report.getRefID()));
                        }
                    }

                }

                //Copy, do not move, since sources of pipedstreams do not accept this kind of opeartion
                transactions = getDatastoreService().copyReportFilesFromPoolToDataStore(reportsToReset);
                getChannelManager().updateReports(reports);
            }
            return transactions;
        }
        return null;
    }

    public boolean setSecuredStreamToSessionPool(String uploadId, TypedStreamInterface file) throws Exception {
        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().setStreamToSessionPool(uploadId, file);
    }

    private void processReportForDeletion(Report report) throws Exception {

        //BUGFIX: cannot dereference channel since it's part of the business key
        /*if (report != null) {

            report.removeChannel();

        }*/

    }

    /*@Override
     public void deleteSecuredReport(String id) throws Exception {
     if (id != null) {

     Report report = getChannelManager().loadReport(id);
     //Then check if it can be accessed

     if (getChannelManager().checkAccessDeleteReport(report.getChannel())) {

     //Copy all originally bound channels before touching them
     Collection originallyBoundChannels = REPORT_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), report);
     //Save original links before modification (clones)
     Channel originallyLinkedChannel = report.getChannel();

     //Whatever happens, someone who can remove a archive can unlink other stuff bound to it: no need for extra check
     //Cascade delete will take care of their unlinking
     processReportForDeletion(report);

     //Everything's fine, now make it persistent in the database
     getChannelManager().deleteReport(report);
     //Log it
     log(REPORT_LOG_OPERATION.delete, report, originallyBoundChannels);
     //Notify responsible users
     notify(CHANNELREPORT_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, report);

     } else {
     throw new AccessDeniedException();
     }

     }
     }

     @Override
     public void deleteSecuredReports(Collection<String> ids) throws Exception {
     if (ids != null) {

     Collection<Report> reportsToDelete = new HashSet<Report>();
     for (String id : ids) {
     //First get a copy of the entity from a DTO
     Report report = getChannelManager().loadReport(id);

     if (getChannelManager().checkAccessDeleteReport(report.getChannel())) {

     //Copy all originally bound channels before touching them
     Collection originallyBoundChannels = REPORT_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), report);
     //Save original links before modification (clones)
     Channel originallyLinkedChannel = report.getChannel();

     processReportForDeletion(report);
                    
     reportsToDelete.add(report);
     //Log it
     log(REPORT_LOG_OPERATION.delete, report, originallyBoundChannels);
     //Notify responsible users
     notify(CHANNELREPORT_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, report);

     } else {
     throw new AccessDeniedException();
     }

     }

     //Everything's fine, now make it persistent in the database
     if (!reportsToDelete.isEmpty()) {

     getChannelManager().deleteReports(reportsToDelete);

     }
     }
     }*/
    protected void synchronousSafePreDeleteReports(Map<String, ReportForm> reportIdsAndBackups) throws Exception {
        if (reportIdsAndBackups != null) {

            //Get the original version of this report
            //It will be this version that will be modified with data coming from the DTO
            Collection<Report> reports = getChannelManager().loadReports(reportIdsAndBackups.keySet());
            Collection<Report> deletedReports = new HashSet<Report>();
            for (Report report : reports) {

                if (getChannelManager().checkAccessDeleteReport(report.getChannel()) && isReportTouchable(report)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<? extends ReportForm> rollbackCopyEntry = reportDTOAssembler.toDTO(report);
                    ReportForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;
                    //Check if it can be accessed

                    //Then remove dependencies
                    processReportForDeletion(report);

                    report.setReady(false);
                    deletedReports.add(report);
                    //Update the referenced id list with rollback copies
                    reportIdsAndBackups.put(report.getRefID(), rollbackCopy);

                } else {
                    throw new AccessDeniedException();
                }

            }
            getChannelManager().updateReports(deletedReports);
        }

    }

    protected Collection<Integer> synchronousSafePostDeleteReports(
            Map<String, ReportForm> reportIdsAndBackups) throws Exception {

        if (reportIdsAndBackups != null) {
            Collection<Integer> transactions = null;
            Collection<Report> reportsToDelete = new HashSet<Report>();

            final Collection<? extends Report> reports = getChannelManager().loadReports(reportIdsAndBackups.keySet());
            if (reports != null) {
                for (Report report : reports) {

                    reportsToDelete.add(report);
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    ReportForm rollbackCopy = reportIdsAndBackups.get(report.getRefID());
                    //Save original links before modification (clones)
                    Channel originallyLinkedChannel = reportDTOAssembler.getImmutableChannel(rollbackCopy);
                    log(REPORT_LOG_OPERATION.delete, report, Collections.singleton(originallyLinkedChannel));
                    //Notify responsible users of any changes
                    //All links have been successfully removed
                    //use the rollback copy to know which one was originally linked to the report
                    notify(CHANNELREPORT_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, report);
                }
            }
            if (!reportsToDelete.isEmpty()) {
                transactions = getDatastoreService().unsetReportFilesFromDatastore(reportsToDelete);
                getChannelManager().deleteReports(reportsToDelete);
            }
            return transactions;
        }
        return null;
    }

    @Override
    public ActionCollection<? extends ReportForm> getSecuredReports(GroupAndSort<REPORT_TAGS> constraint) throws Exception {
        Collection<Report> reports = getChannelManager().getFilteredReports(constraint);
        ActionCollection<? extends ReportForm> forms = reportDTOAssembler.toDTO(reports, constraint == null || constraint.getSortByFeature() == null);

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(ARCHIVE_LOG_OPERATION.read, archives);

        return forms;
    }

    @Override
    public ActionCollection<? extends ReportForm> getSecuredReportsForChannelId(GroupAndSort<REPORT_TAGS> constraint, String channelId) throws Exception {
        Collection<Report> reports = getChannelManager().getFilteredReportsForChannelId(constraint, channelId);
        ActionCollection<? extends ReportForm> forms = reportDTOAssembler.toDTO(reports, constraint == null || constraint.getSortByFeature() == null);

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(ARCHIVE_LOG_OPERATION.read, archives);
        return forms;
    }

    protected void log(REPORT_LOG_OPERATION operation, Collection<Report> reports) throws Exception {
        if (reports != null && !reports.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<ReportLog> logs = new HashSet<ReportLog>();
            for (Report report : reports) {
                logs.add(
                        new ReportLog(
                        operation,
                        report.getRefID(),
                        report.getStartTime(),
                        report.getEndTime(),
                        report.getChannel() != null ? report.getChannel().getLabel() : null,
                        operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), report)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(REPORT_LOG_OPERATION operation, Report report) throws Exception {
        log(operation, report, null);
    }

    protected void log(REPORT_LOG_OPERATION operation, Report report, Collection<Channel> supplementaryChannels) throws Exception {
        if (report != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), report);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(
                    new ReportLog(
                    operation,
                    report.getRefID(),
                    report.getStartTime(),
                    report.getEndTime(),
                    report.getChannel() != null ? report.getChannel().getLabel() : null,
                    allBoundChannels));
        }
    }

    protected void notify(CHANNELREPORT_NOTIFICATION_OPERATION operation, Channel channel, Report report) throws Exception {
        notify(operation, channel, report, operation.getRecipients(getSecurityManager(), channel, report));
    }

    protected void notify(CHANNELREPORT_NOTIFICATION_OPERATION operation, Channel channel, Report report, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && report != null && recipients != null) {
            Collection<ChannelReportLinkNotification> notifications = new HashSet<ChannelReportLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelReportLinkNotification(operation, channel, report, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected boolean isReportTouchable(Report report) throws Exception {

        if (report == null) {
            throw new NoEntityException();
        }

        if (!report.isReady()) {
            throw new ReportNotReadyException();
        }

        return true;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> fetchReport(String reportId, SERIALIZATION_METADATA_FORMAT metadataFormat) throws Exception {
        
        //Then check if the call is legitimate
        Report report = getChannelManager().getReport(reportId);
        if (getChannelManager().checkAccessViewReport(report!=null?report.getChannel():null)) {

            TextStream objectStream = getDatastoreService().getReportTextStreamFromDataStore(report.getRefID());
                          
            return new GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>>(
                    metadataFormat,
                    ArchiveDTOAssembler.fromSerializableDTO(report.getRefID(), objectStream, taskPerformer));

        } else {
            throw new AccessDeniedException();
        }
        
    }
    
    
}
