/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.archive;

import biz.ddcr.shampoo.client.form.archive.ReportForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/

public interface ReportFacadeServiceInterface {
    
    public interface AsynchronousDeleteReportTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Report> {
        public void add(String id);
    }

    public interface AsynchronousAddReportTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Report> {
        public void add(ReportForm report);
    }
    
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousAddSecuredReport(final ReportForm reportForm) throws Exception;
    
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousDeleteSecuredReport(final String id) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousDeleteSecuredReports(final Collection<String> ids) throws Exception;
    
    public ActionCollection<? extends ReportForm> getSecuredReports(GroupAndSort<ReportForm.REPORT_TAGS> constraint) throws Exception;
    public ActionCollection<? extends ReportForm> getSecuredReportsForChannelId(GroupAndSort<ReportForm.REPORT_TAGS> constraint, String channelId) throws Exception;

    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> fetchReport(String reportId, SERIALIZATION_METADATA_FORMAT metadataFormat) throws Exception;
    
}
