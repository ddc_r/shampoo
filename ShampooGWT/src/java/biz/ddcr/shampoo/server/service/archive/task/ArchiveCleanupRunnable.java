/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.archive.task;

import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;

/**
 *
 * @author okay_awright
 **/
public class ArchiveCleanupRunnable extends GenericService implements ArchiveCleanupRunnableInterface {

    private ChannelManagerInterface channelManager;

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    /**
     * Purge old unwanted archives
     * @throws Exception
     */
    @Override
    public void deleteOldArchives() throws Exception {
        getChannelManager().purgeArchives();
    }

    @Override
    public void run() {
        try {

            deleteOldArchives();
        } catch (Exception e) {
            logger.error("Archive runnable failed", e);
        }
    }

}
