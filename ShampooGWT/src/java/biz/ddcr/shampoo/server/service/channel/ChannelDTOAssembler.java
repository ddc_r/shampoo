/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.channel.CHANNEL_ACTION;
import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.STREAMER_ACTION;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;
import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.form.user.RESTRICTEDUSER_ACTION;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author okay_awright
 *
 */
public class ChannelDTOAssembler extends GenericService implements ChannelDTOAssemblerInterface {

    private UserManagerInterface userManager;
    private ProgrammeManagerInterface programmeManager;
    private ChannelManagerInterface channelManager;

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public static biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT fromDTO(biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT format) {
        if (format == null) {
            return null;
        }
        switch (format) {
            case liquidsoapJSON:
                return biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT.liquidsoapJSON;
            case liquidsoapAnnotate:
                return biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT.liquidsoapAnnotate;
            case genericJSON:
                return biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT.genericJSON;
            case genericXML:
                return biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT.genericXML;
            case genericCSV:
                return biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT.genericCSV;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT toDTO(biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT format) {
        if (format == null) {
            return null;
        }
        switch (format) {
            case liquidsoapJSON:
                return biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT.liquidsoapJSON;
            case liquidsoapAnnotate:
                return biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT.liquidsoapAnnotate;
            case genericJSON:
                return biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT.genericJSON;
            case genericXML:
                return biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT.genericXML;
            case genericCSV:
                return biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT.genericCSV;
            default:
                return null;
        }
    }

    /*protected StreamerModuleWithPassword toStaticDTOWithPassword(Streamer streamer) throws Exception {
        if (streamer != null) {
            StreamerModuleWithPassword streamerModule = new StreamerModuleWithPassword();
            streamerModule.setPrivateKey(streamer.getPrivateKey());
            streamerModule.setEnableEmergencyItems(streamer.isEmergencyItemsEnabled());
            streamerModule.setSeatNumber(streamer.getSeatNumber());

            return streamerModule;
        }
        return null;
    }*/

    protected StreamerModule toDTO(Long seatNumber, DynamicStreamerData streamerConfiguration) throws Exception {
        if (seatNumber!=null && streamerConfiguration != null) {
            StreamerModule streamerModule = new StreamerModule(//
                    toDTO(streamerConfiguration.getMetadataFormat()),//
                    streamerConfiguration.getQueueMinItems(),//
                    //queueing Lives
                    streamerConfiguration.isQueueableLive(),//
                    //queuing Blanks
                    streamerConfiguration.isQueueableBlank(),//
                    //Lives and blanks options
                    streamerConfiguration.getQueueableLiveURI(),//
                    streamerConfiguration.getQueueableLiveChunkSize(),//
                    streamerConfiguration.getQueueableBlankURI(),//
                    streamerConfiguration.getQueueableBlankChunkSize(),//
                    //M3U URL
                    streamerConfiguration.getStreamURI(),//
                    //Misc.
                    streamerConfiguration.getUserAgentID(),//
                    streamerConfiguration.getLatestHeartbeat(),//
                    //Beware TTL for the form is in seconds!
                    streamerConfiguration.getTtl()!=null ? streamerConfiguration.getTtl()/1000 : null,
                    streamerConfiguration.isStreaming()
                    );

            streamerModule.setSeatNumber(seatNumber);
            streamerModule.setEnableEmergencyItems(streamerConfiguration.isEnableEmergencyItems());

            return streamerModule;
        }
        return null;
    }

    @Override
    public Channel fromDTO(ChannelForm channelForm) throws Exception {
        if (channelForm != null) {
            Channel newChannel = new Channel();

            //Fill in all attributes
            newChannel.setDescription(channelForm.getDescription());
            newChannel.setLabel(channelForm.getLabel());
            newChannel.setTimezone(channelForm.getTimezone());
            newChannel.setMaxDailyRequestLimitPerUser(channelForm.getMaxDailyRequestLimitPerUser());

            //Streamer
            //If you can add a channel, you can add a streamer
            newChannel.setSeatNumber(channelForm.getSeatNumber()!=null?channelForm.getSeatNumber().getItem():null);
            //Public webservices are not handled here

            //Programmes
            //Don't throw an exception if one cannot be accessed, just don't add it
            for (ActionCollectionEntry<String> programmeEntry : channelForm.getProgrammes()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p)) {
                    newChannel.addProgramme(p);
                }
            }

            //Rights
            //Don't throw an exception if one cannot be accessed, just don't add it
            for (ActionCollectionEntry<String> userEntry : channelForm.getChannelAdministratorRights()) {
                RestrictedUser u = getUserManager().getRestrictedUser(userEntry.getKey());
                if (getUserManager().checkAccessUpdateRestrictedUser(u)) {
                    newChannel.addChannelAdministratorRight(u);
                }
            }

            for (ActionCollectionEntry<String> userEntry : channelForm.getProgrammeManagerRights()) {
                RestrictedUser u = getUserManager().getRestrictedUser(userEntry.getKey());
                if (getUserManager().checkAccessUpdateRestrictedUser(u)) {
                    newChannel.addProgrammeManagerRight(u);
                }
            }

            for (ActionCollectionEntry<String> userEntry : channelForm.getListenerRights()) {
                RestrictedUser u = getUserManager().getRestrictedUser(userEntry.getKey());
                if (getUserManager().checkAccessUpdateRestrictedUser(u)) {
                    newChannel.addListenerRight(u);
                }
            }

            //misc data
            newChannel.setUrl(MarshallUtil.trimText(channelForm.getUrl()));
            newChannel.setTag(channelForm.getTag());
            newChannel.setOpenRegistration(channelForm.isOpen());

            return newChannel;
        }
        return null;
    }

    /**
     * update an existing channel with data from a DTO. Returns true if
     * attributes of the original channel have been changed.
     *
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(Channel channel, ChannelForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && channel != null) {

            if ((form.getDescription() != null && !form.getDescription().equals(channel.getDescription())
                    || (form.getDescription() == null && channel.getDescription() != null))) {
                channel.setDescription(form.getDescription());
                contentUpdated = true;
            }
            if ((form.getLabel() != null && !form.getLabel().equals(channel.getLabel())
                    || (form.getLabel() == null && channel.getLabel() != null))) {
                channel.setLabel(form.getLabel());
                contentUpdated = true;
            }
            if ((form.getTimezone() != null && !form.getTimezone().equals(channel.getTimezone())
                    || (form.getTimezone() == null && channel.getTimezone() != null))) {
                channel.setTimezone(form.getTimezone());
                contentUpdated = true;
            }
            if ((form.getMaxDailyRequestLimitPerUser() != null && !form.getMaxDailyRequestLimitPerUser().equals(channel.getMaxDailyRequestLimitPerUser())
                    || (form.getMaxDailyRequestLimitPerUser() == null && channel.getMaxDailyRequestLimitPerUser() != null))) {
                channel.setMaxDailyRequestLimitPerUser(form.getMaxDailyRequestLimitPerUser());
                contentUpdated = true;
            }
            //misc data
            String url = MarshallUtil.trimText(form.getUrl());
            if ((url != null && !url.equals(channel.getUrl())
                    || (url == null && channel.getUrl() != null))) {
                channel.setUrl(url);
                contentUpdated = true;
            }
            if ((form.getTag() != null && !form.getTag().equals(channel.getTag())
                    || (form.getTag() == null && channel.getTag() != null))) {
                channel.setTag(form.getTag());
                contentUpdated = true;
            }
            if (form.isOpen() != channel.isOpenRegistration()) {
                channel.setOpenRegistration(form.isOpen());
                contentUpdated = true;
            }

            //Streamer
            if (getChannelManager().checkAccessAddChannels()) {
                if ((channel.getSeatNumber() != null && form.getSeatNumber() != null && !channel.getSeatNumber().equals(form.getSeatNumber().getItem()))
                    || ((form.getSeatNumber() == null || form.getSeatNumber().getItem()==null) && channel.getSeatNumber() != null)
                    || ((form.getSeatNumber() != null && form.getSeatNumber().getItem()!=null) && channel.getSeatNumber() == null)) {
                    channel.setSeatNumber(form.getSeatNumber().getItem());
                    contentUpdated = true;
                }
            }
            
            //Public webservices are not handled here

            //First remove all programmes from the original channel that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            Collection<Programme> programmeEntries = new HashSet<Programme>();
            for (Programme p : getProgrammeManager().getProgrammes(form.getProgrammes().keySet())) {
                if (getProgrammeManager().checkAccessUpdateProgramme(p)) {
                    programmeEntries.add(p);
                }
            }

            for (Iterator<Programme> i = channel.getProgrammes().iterator(); i.hasNext();) {
                Programme boundProgramme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(boundProgramme)) {
                    i.remove();
                    channel.removeProgramme(boundProgramme);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (Programme p : programmeEntries) {
                channel.addProgramme(p);
            }

            //First remove all the rights from the original user that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            Collection<RestrictedUser> formUserEntries = new HashSet<RestrictedUser>();
            for (RestrictedUser u : getUserManager().getRestrictedUsers(form.getChannelAdministratorRights().keySet())) {
                if (getUserManager().checkAccessUpdateRestrictedUser(u)) {
                    formUserEntries.add(u);
                }
            }

            for (Iterator<RestrictedUser> i = channel.getChannelAdministratorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    channel.removeChannelAdministratorRight(boundUser);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (RestrictedUser u : formUserEntries) {
                channel.addChannelAdministratorRight(u);
            }

            //First remove all the rights from the original user that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            formUserEntries = new HashSet<RestrictedUser>();
            for (RestrictedUser u : getUserManager().getRestrictedUsers(form.getListenerRights().keySet())) {
                if (getUserManager().checkAccessUpdateRestrictedUser(u)) {
                    formUserEntries.add(u);
                }
            }

            for (Iterator<RestrictedUser> i = channel.getListenerRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    channel.removeListenerRight(boundUser);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (RestrictedUser u : formUserEntries) {
                channel.addListenerRight(u);
            }

            //First remove all the rights from the original user that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            formUserEntries = new HashSet<RestrictedUser>();
            for (RestrictedUser u : getUserManager().getRestrictedUsers(form.getProgrammeManagerRights().keySet())) {
                if (getUserManager().checkAccessUpdateRestrictedUser(u)) {
                    formUserEntries.add(u);
                }
            }

            for (Iterator<RestrictedUser> i = channel.getProgrammeManagerRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    channel.removeProgrammeManagerRight(boundUser);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (RestrictedUser u : formUserEntries) {
                channel.addProgrammeManagerRight(u);
            }

        }
        return contentUpdated;
    }

    @Override
    public ActionCollectionEntry<ChannelForm> toDTO(Channel channel) throws Exception {
        if (channel == null) {
            return null;
        }

        ChannelForm channelForm = new ChannelForm();

        //Fill in common properties
        channelForm.setCreatorId(channel.getCreator() != null ? channel.getCreator().getUsername() : null);
        channelForm.setLatestEditorId(channel.getLatestEditor() != null ? channel.getLatestEditor().getUsername() : null);
        channelForm.setCreationDate(channel.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(channel.getCreationDate().getTime(), null)) : null);
        channelForm.setLatestModificationDate(channel.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(channel.getLatestModificationDate().getTime(), null)) : null);

        //Fill in all attributes
        channelForm.setDescription(channel.getDescription());
        channelForm.setLabel(channel.getLabel());
        channelForm.setTimezone(channel.getTimezone());
        channelForm.setMaxDailyRequestLimitPerUser(channel.getMaxDailyRequestLimitPerUser());

        //Misc data
        channelForm.setUrl(channel.getUrl());
        channelForm.setTag(channel.getTag());
        channelForm.setOpen(channel.isOpenRegistration());

        //Streamer
        channelForm.setSeatNumber(
                    new ActionMapEntry<Long>(
                    channel.getSeatNumber(),
                    decorateStreamerEntryWithActions(channel)));
        //Public webservices are not handled here

        //Programmes
        ActionMap<String> programmeIds = new ActionSortedMap<String>();
        for (Programme programme : channel.getProgrammes()) {
            programmeIds.set(programme.getLabel(), decorateEntryWithActions(programme));
        }
        channelForm.setProgrammes(programmeIds);

        //Rights
        ActionMap<String> newChannelAdministratorRights = new ActionSortedMap<String>();
        for (RestrictedUser user : channel.getChannelAdministratorRights()) {
            newChannelAdministratorRights.set(user.getUsername(), decorateEntryWithActions(user));
        }
        channelForm.setChannelAdministratorRights(newChannelAdministratorRights);
        ActionMap<String> newProgrammeManagerRights = new ActionSortedMap<String>();
        for (RestrictedUser user : channel.getProgrammeManagerRights()) {
            newProgrammeManagerRights.set(user.getUsername(), decorateEntryWithActions(user));
        }
        channelForm.setProgrammeManagerRights(newProgrammeManagerRights);
        ActionMap<String> newListenerRights = new ActionSortedMap<String>();
        for (RestrictedUser user : channel.getListenerRights()) {
            newListenerRights.set(user.getUsername(), decorateEntryWithActions(user));
        }
        channelForm.setListenerRights(newListenerRights);

        return new ActionMapEntry<ChannelForm>(channelForm, decorateEntryWithActions(channel));
    }

    @Override
    public ActionCollectionEntry<StreamerModule> toStreamerMetadataDTO(Channel channel) throws Exception {
        if (channel == null) {
            return null;
        }
        DynamicStreamerData data = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
        Long seatNumber = ChannelStreamerConnectionStatusHelper.getSeatNumber(channel.getLabel());
        return new ActionMapEntry<StreamerModule>(toDTO(seatNumber, data), decorateStreamerEntryWithActions(channel));
    }

    @Override
    public ActionCollectionEntry<String> toStreamerKeyDTO(Channel channel) throws Exception {
        if (channel == null) {
            return null;
        }
        String streamerKey = ChannelStreamerConnectionStatusHelper.getProtectionKey(channel.getLabel());
        return new ActionMapEntry<String>(streamerKey, decorateStreamerEntryWithActions(channel));
    }
    
    @Override
    public ActionCollection<String> toStringDTO(Collection<Channel> channels, boolean doSort) throws Exception {
        ActionMap<String> channelStrings = doSort ? new ActionSortedMap<String>() : new ActionUnsortedMap<String>();
        for (Channel channel : channels) {
            channelStrings.set(channel.getLabel(), decorateEntryWithActions(channel));
        }
        return channelStrings;
    }

    @Override
    public ActionCollection<ChannelForm> toDTO(Collection<Channel> channels, boolean doSort) throws Exception {
        ActionMap<ChannelForm> channelForms = doSort ? new ActionSortedMap<ChannelForm>() : new ActionUnsortedMap<ChannelForm>();
        for (Channel channel : channels) {
            channelForms.set(toDTO(channel));
        }
        return channelForms;
    }

    protected Collection<ACTION> decorateEntryWithActions(RestrictedUser user) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getUserManager().checkAccessViewRestrictedUser(user)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessUpdateRestrictedUser(user)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(user)) {
                actions.add(RESTRICTEDUSER_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessDeleteRestrictedUser(user)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Channel channel) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewChannel(channel)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(channel)) {
                actions.add(CHANNEL_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateChannel(channel)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteChannel(channel)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }  
    
    protected Collection<ACTION> decorateEntryWithActions(Programme programme) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewProgramme(programme)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Curators and Contributors for linking programmes to tracks, they cannot edit a programme yet they can bind tracks
        try {
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programme)) {
                actions.add(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateStreamerEntryWithActions(Channel channel) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewStreamer(channel)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateStreamer(channel)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessListenStreamer(channel)) {
                actions.add(STREAMER_ACTION.LISTEN);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }
}
