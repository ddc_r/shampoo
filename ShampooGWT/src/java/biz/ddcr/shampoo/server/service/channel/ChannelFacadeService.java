/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
  *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.ActionMapEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.channel.log.ChannelLog.CHANNEL_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelContentNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelContentNotification.CHANNEL_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.log.ChannelLog;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelChannelAdministratorLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelChannelAdministratorLinkNotification.CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification.CHANNELLISTENER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeLinkNotification.CHANNELPROGRAMME_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeManagerLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeManagerLinkNotification.CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelTimetableSlotLinkNotification.CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelWebserviceLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelWebserviceLinkNotification.CHANNELWEBSERVICE_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 **/
public class ChannelFacadeService extends GenericService implements ChannelFacadeServiceInterface {

    private ChannelDTOAssemblerInterface channelDTOAssembler;
    private TimetableDTOAssemblerInterface timetableDTOAssembler;
    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private SecurityManagerInterface securityManager;

    public ChannelDTOAssemblerInterface getChannelDTOAssembler() {
        return channelDTOAssembler;
    }

    public void setChannelDTOAssembler(ChannelDTOAssemblerInterface channelDTOAssembler) {
        this.channelDTOAssembler = channelDTOAssembler;
    }

    public TimetableDTOAssemblerInterface getTimetableDTOAssembler() {
        return timetableDTOAssembler;
    }

    public void setTimetableDTOAssembler(TimetableDTOAssemblerInterface timetableDTOAssembler) {
        this.timetableDTOAssembler = timetableDTOAssembler;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /**
     *
     * Validation process for channels
     * Incorrect values are replaced with default ones unless the problem is not recoverable, then an Exception is thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean checkChannel(Channel channel) throws Exception {
        boolean result = false;

        if (channel != null) {

            /*Streamer streamer = channel.getStreamer();
            if (streamer!=null) {
                //All those values cannot be <=0, it makes no sense otherwise
                if (streamer.getMinPlayableDuration()<1) streamer.setMinPlayableDuration(1l);
                if (streamer.getQueueMinItems()<1) streamer.setQueueMinItems(1);
                //Check that all URIs are actually valid URIs
                try {
                    new URI(streamer.getStreamURI());
                } catch (URISyntaxException ex) {
                    throw new MalformedURIException(streamer.getStreamURI());
                }
                //We must check the Apache Strsubstitutor variable as soon as possible, otherwise it will likely mess up with the whole queue
                try {
                    URISchemeMetadataPatternResolver.check(streamer.getQueueableBlankURIPattern());
                } catch (IllegalArgumentException e) {
                    throw new MalformedTextPatternExpressionException(streamer.getQueueableBlankURIPattern());
                }
                try {
                    URISchemeMetadataPatternResolver.check(streamer.getQueueableLiveURIPattern());
                } catch (IllegalArgumentException e) {
                    throw new MalformedTextPatternExpressionException(streamer.getQueueableLiveURIPattern());
                }
            }*/
            result = true;
        }
        return result;
    }

    /**
     * Add a channel to the database if the authenticated user can access it
     * Check every rights and programmes belonging to the user to add and strip them down if the authenticated user cannot access them
     * It will not manage timetable slots
     *
     * @param channelForm
     * @throws Exception
     **/
    @Override
    public void addSecuredChannel(ChannelForm channelForm) throws Exception {

        if (channelForm != null) {

            //Then check if it can be accessed
            if (getChannelManager().checkAccessAddChannels()) {

                //hydrate the entity from the DTO
                Channel channel = channelDTOAssembler.fromDTO(channelForm);

                if (checkChannel(channel)) {
                    //Everything's fine, now make it persistent in the database
                    try {

                        getChannelManager().addChannel(channel);
                        //Log it
                        log(CHANNEL_LOG_OPERATION.add, channel);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        for (Programme boundProgramme : channel.getProgrammes())
                            notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.add, channel, boundProgramme);
                        for (RestrictedUser boundUser : channel.getChannelAdministratorRights())
                            notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.add, channel, boundUser);
                        for (RestrictedUser boundUser : channel.getListenerRights())
                            notify(CHANNELLISTENER_NOTIFICATION_OPERATION.add, channel, boundUser);
                        for (RestrictedUser boundUser : channel.getProgrammeManagerRights())
                            notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.add, channel, boundUser);
                        for (TimetableSlot boundSlot : channel.getTimetableSlots())
                            notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.add, channel, boundSlot);
                        for (Webservice boundWS : channel.getWebservices())
                            notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.add, channel, boundWS);

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    /**
     *
     * Update a channel if the authenticated user can access it
     * Do only add or remove rights and programmes bound to the channel that the authenticated user can acccess
     * It will not manage timetable slots
     *
     * @param channelForm
     * @throws Exception
     **/
    @Override
    public void updateSecuredChannel(ChannelForm channelForm) throws Exception {

        if (channelForm != null) {

            //Get the original version of this programme
            //It will be this version that will be modified with data coming from the DTO
            Channel originalChannel = getChannelManager().loadChannel(channelForm.getLabel());
            //Check if it can be accessed

            if (getChannelManager().checkAccessUpdateChannel(originalChannel)) {

                //Save original links before modification (clones)
                Set<Programme> originallyLinkedProgrammes = new HashSet<Programme>(originalChannel.getProgrammes());
                Set<RestrictedUser> originallyLinkedChannelAdministrators = new HashSet<RestrictedUser>(originalChannel.getChannelAdministratorRights());
                Set<RestrictedUser> originallyLinkedListeners = new HashSet<RestrictedUser>(originalChannel.getListenerRights());
                Set<RestrictedUser> originallyLinkedProgrammeManagers = new HashSet<RestrictedUser>(originalChannel.getProgrammeManagerRights());
                Set<TimetableSlot> originallyLinkedSlots = new HashSet<TimetableSlot>(originalChannel.getTimetableSlots());
                Set<Webservice> originallyLinkedWSes = new HashSet<Webservice>(originalChannel.getWebservices());

                //Update the existing entity from the DTO
                boolean areAttributesUpdated = channelDTOAssembler.augmentWithDTO(originalChannel, channelForm);

                //Check and adjust its dependencies before updating
                if (checkChannel(originalChannel)) {

                    //Everything's fine, now make it persistent in the database
                    getChannelManager().updateChannel(originalChannel);
                    //Log it
                    log(CHANNEL_LOG_OPERATION.edit, originalChannel);
                    //Notify responsible users of any changes
                    if (areAttributesUpdated)
                        //Only users still linked to the channel after update will receive this notification
                        notify(CHANNEL_NOTIFICATION_OPERATION.edit, originalChannel);
                    //New linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedProgrammes, originalChannel.getProgrammes()))
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.add, originalChannel, boundProgramme);
                    //Old linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(originalChannel.getProgrammes(), originallyLinkedProgrammes))
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, originalChannel, boundProgramme);
                    //New linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originallyLinkedChannelAdministrators, originalChannel.getChannelAdministratorRights()))
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.add, originalChannel, boundUser);
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originalChannel.getChannelAdministratorRights(), originallyLinkedChannelAdministrators))
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, originalChannel, boundUser);
                    //New linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originallyLinkedListeners, originalChannel.getListenerRights()))
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.add, originalChannel, boundUser);
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originalChannel.getListenerRights(), originallyLinkedListeners))
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, originalChannel, boundUser);
                    //New linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originallyLinkedProgrammeManagers, originalChannel.getProgrammeManagerRights()))
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.add, originalChannel, boundUser);
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originalChannel.getProgrammeManagerRights(), originallyLinkedProgrammeManagers))
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, originalChannel, boundUser);
                    //New linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(originallyLinkedSlots, originalChannel.getTimetableSlots()))
                        notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.add, originalChannel, boundSlot);
                    //Old linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(originalChannel.getTimetableSlots(), originallyLinkedSlots))
                        notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originalChannel, boundSlot);
                    //New linked WSes
                    for (Webservice boundWS : CollectionUtil.exclusion(originallyLinkedWSes, originalChannel.getWebservices()))
                        notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.add, originalChannel, boundWS);
                    //Old linked WSes
                    for (Webservice boundWS : CollectionUtil.exclusion(originalChannel.getWebservices(), originallyLinkedWSes))
                        notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, originalChannel, boundWS);

                }
            } else {
                throw new AccessDeniedException();
            }

        }

    }

    /**
     *
     * Pre-process a channel for removal
     * Outputs true if this entity must be removed, false for updating it instead
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean processChannelForDeletion(Channel channel) throws Exception {
        //Flag to know whether to update or delete the channel regarding how the checks end up
        boolean mustBeRemoved = true;

        if (channel != null) {

            //And every programme of its
            //Don't throw an exception if a programme cannot be accessed, just remove it from the list and make sure the channel is updated, not removed actually
            //for (Programme boundProgramme : channel.getProgrammes()) {
            for (Iterator<Programme> i = channel.getProgrammes().iterator(); i.hasNext();) {
                Programme boundProgramme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(boundProgramme)) {
                    i.remove();
                    channel.removeProgramme(boundProgramme);
                } else {
                    mustBeRemoved = false;
                }
            }

            //And every rights of its
            //Don't throw an exception if a right cannot be accessed, just remove it from the list and make sure the channel is updated, not removed actually
            //for (RestrictedUser boundUser : channel.getChannelAdministratorRights()) {
            for (Iterator<RestrictedUser> i = channel.getChannelAdministratorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    channel.removeChannelAdministratorRight(boundUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (RestrictedUser boundUser : channel.getProgrammeManagerRights()) {
            for (Iterator<RestrictedUser> i = channel.getProgrammeManagerRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    channel.removeProgrammeManagerRight(boundUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (RestrictedUser boundUser : channel.getListenerRights()) {
            for (Iterator<RestrictedUser> i = channel.getListenerRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    channel.removeListenerRight(boundUser);
                } else {
                    mustBeRemoved = false;
                }
            }

            //Whatever happens, someone who can remove a programme can unlink a timetable slot and other stuff: no need for extra check
            //Cascade delete will take care of their unlinking
            //Don't use clearTimetableSlots() since channel is part of a not null, unique component in timetable slots, just issue a clear() on the collection, it's enough with the appropriate delete cascade parameter
            channel.getTimetableSlots().clear();
            channel.clearArchives();
            channel.clearReports();
            channel.clearQueueItems();
            channel.clearLogs();
            channel.removeStreamItem();
            channel.getRequests().clear();
            channel.clearWebservices();

        }

        return mustBeRemoved;
    }

    @Override
    public void deleteSecuredChannel(String id) throws Exception {

        if (id != null) {

            Channel channel = getChannelManager().loadChannel(id);
            //Then check if it can be accessed

            if (getChannelManager().checkAccessDeleteChannel(channel)) {

                //Whatever happens, someone who can remove a timetable slot can unlink other stuff bound to it: no need for extra check
                //Cascade delete will take care of their unlinking

                //Save original links before modification (clones)
                Set<Programme> originallyLinkedProgrammes = new HashSet<Programme>(channel.getProgrammes());
                Set<RestrictedUser> originallyLinkedChannelAdministrators = new HashSet<RestrictedUser>(channel.getChannelAdministratorRights());
                Set<RestrictedUser> originallyLinkedListeners = new HashSet<RestrictedUser>(channel.getListenerRights());
                Set<RestrictedUser> originallyLinkedProgrammeManagers = new HashSet<RestrictedUser>(channel.getProgrammeManagerRights());
                Set<TimetableSlot> originallyLinkedSlots = new HashSet<TimetableSlot>(channel.getTimetableSlots());
                Set<Webservice> originallyLinkedWSes = new HashSet<Webservice>(channel.getWebservices());

                //Flag to know whether to update or delete the channel regarding how the checks end up
                if (processChannelForDeletion(channel)) {

                    //Everything's fine, now make it persistent in the database
                    getChannelManager().deleteChannel(channel);
                    //Log it
                    log(CHANNEL_LOG_OPERATION.delete, channel);
                    //Notify responsible users of any changes
                    //All links have been successfully removed
                    for (Programme boundProgramme : originallyLinkedProgrammes)
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, channel, boundProgramme);
                    for (RestrictedUser boundUser : originallyLinkedChannelAdministrators)
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, channel, boundUser);
                    for (RestrictedUser boundUser : originallyLinkedListeners)
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                    for (RestrictedUser boundUser : originallyLinkedProgrammeManagers)
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                    for (TimetableSlot boundSlot : originallyLinkedSlots)
                        notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, channel, boundSlot);
                    for (Webservice boundWS : originallyLinkedWSes)
                        notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, channel, boundWS);

                } else {

                    //Everything's fine, now make it persistent in the database
                    getChannelManager().updateChannel(channel);
                    //Log it
                    log(CHANNEL_LOG_OPERATION.edit, channel);
                    //Notify responsible users of any changes
                    //Old linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(channel.getProgrammes(), originallyLinkedProgrammes))
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, channel, boundProgramme);
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(channel.getChannelAdministratorRights(), originallyLinkedChannelAdministrators))
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, channel, boundUser);
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(channel.getListenerRights(), originallyLinkedListeners))
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(channel.getProgrammeManagerRights(), originallyLinkedProgrammeManagers))
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                    //Old linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(channel.getTimetableSlots(), originallyLinkedSlots))
                        notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, channel, boundSlot);
                    //Old linked webservices
                    for (Webservice boundWS : CollectionUtil.exclusion(channel.getWebservices(), originallyLinkedWSes))
                        notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, channel, boundWS);
                }

            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredChannels(Collection<String> ids) throws Exception {
        if (ids != null) {

            Collection<Channel> channelsToUpdate = new HashSet<Channel>();
            Collection<Channel> channelsToDelete = new HashSet<Channel>();
            for (String id : ids) {
                //First get a copy of the entity from a DTO
                //Channel channel = channelDTOAssembler.fromDTO(channelForm);
                Channel channel = getChannelManager().loadChannel(id);

                if (getChannelManager().checkAccessDeleteChannel(channel)) {

                    //Save original links before modification (clones)
                    Set<Programme> originallyLinkedProgrammes = new HashSet<Programme>(channel.getProgrammes());
                    Set<RestrictedUser> originallyLinkedChannelAdministrators = new HashSet<RestrictedUser>(channel.getChannelAdministratorRights());
                    Set<RestrictedUser> originallyLinkedListeners = new HashSet<RestrictedUser>(channel.getListenerRights());
                    Set<RestrictedUser> originallyLinkedProgrammeManagers = new HashSet<RestrictedUser>(channel.getProgrammeManagerRights());
                    Set<TimetableSlot> originallyLinkedSlots = new HashSet<TimetableSlot>(channel.getTimetableSlots());
                    Set<Webservice> originallyLinkedWSes = new HashSet<Webservice>(channel.getWebservices());

                    //Flag to know whether to update or delete the channel regarding how the checks end up
                    if (processChannelForDeletion(channel)) {

                        channelsToDelete.add(channel);
                        //Notify responsible users of any changes
                        //All links have been successfully removed
                        for (Programme boundProgramme : originallyLinkedProgrammes)
                            notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, channel, boundProgramme);
                        for (RestrictedUser boundUser : originallyLinkedChannelAdministrators)
                            notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, channel, boundUser);
                        for (RestrictedUser boundUser : originallyLinkedListeners)
                            notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                        for (RestrictedUser boundUser : originallyLinkedProgrammeManagers)
                            notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                        for (TimetableSlot boundSlot : originallyLinkedSlots)
                            notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, channel, boundSlot);
                        for (Webservice boundWS : originallyLinkedWSes)
                            notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, channel, boundWS);

                    } else {

                        channelsToUpdate.add(channel);
                        //Notify responsible users of any changes
                        //Old linked programmes
                        for (Programme boundProgramme : CollectionUtil.exclusion(channel.getProgrammes(), originallyLinkedProgrammes))
                            notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, channel, boundProgramme);
                        //Old linked users
                        for (RestrictedUser boundUser : CollectionUtil.exclusion(channel.getChannelAdministratorRights(), originallyLinkedChannelAdministrators))
                            notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, channel, boundUser);
                        //Old linked users
                        for (RestrictedUser boundUser : CollectionUtil.exclusion(channel.getListenerRights(), originallyLinkedListeners))
                            notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                        //Old linked users
                        for (RestrictedUser boundUser : CollectionUtil.exclusion(channel.getProgrammeManagerRights(), originallyLinkedProgrammeManagers))
                            notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, channel, boundUser);
                        //Old linked slots
                        for (TimetableSlot boundSlot : CollectionUtil.exclusion(channel.getTimetableSlots(), originallyLinkedSlots))
                            notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, channel, boundSlot);
                        //Old linked webservices
                        for (Webservice boundWS : CollectionUtil.exclusion(channel.getWebservices(), originallyLinkedWSes))
                            notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, channel, boundWS);

                    }
                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!channelsToUpdate.isEmpty()) {

                getChannelManager().updateChannels(channelsToUpdate);
                //Log it
                log(CHANNEL_LOG_OPERATION.edit, channelsToUpdate);

            }
            if (!channelsToDelete.isEmpty()) {

                getChannelManager().deleteChannels(channelsToDelete);
                //Log it
                log(CHANNEL_LOG_OPERATION.delete, channelsToDelete);

            }
        }
    }

    /**
     *
     * Fetch a channel from the database if the authenticated user can access it
     *
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public ActionCollectionEntry<ChannelForm> getSecuredChannel(String id) throws Exception {

        Channel channel = getChannelManager().loadChannel(id);
        ActionCollectionEntry<ChannelForm> channelFormEntry = channelDTOAssembler.toDTO(channel);
        if (channelFormEntry == null || channelFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(CHANNEL_LOG_OPERATION.read, channel);
            return channelFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * Returns the time zone of the selected channel. This method call is unprotected.
     *
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public String getChannelTimeZone(String id) throws Exception {
        return getChannelManager().getChannelTimeZone(id);
    }

    /**
     *
     * Fetch the current metadata of a channel-bound streamer if the authenticated user can access it
     *
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public ActionCollectionEntry<StreamerModule> getSecuredDynamicChannelStreamerMetadata(String id) throws Exception {

        Channel channel = getChannelManager().getChannel(id);
        return channelDTOAssembler.toStreamerMetadataDTO(channel);
    }

    /**
     * 
     * Get protection key for the current seat, allocated by the running streamer. Null if still unallocated
     * @param id
     * @return
     * @throws Exception
     */
    @Override
    public ActionCollectionEntry<String> getSecuredDynamicChannelStreamerKey(String id) throws Exception {
        Channel channel = getChannelManager().getChannel(id);
        return channelDTOAssembler.toStreamerKeyDTO(channel);
    }
    
    @Override
    public Long getUnsecuredNextAvailableStreamerSeatNumber() throws Exception {
        Long i = 1L;
        
        while (i<=Long.MAX_VALUE) {
            //Find a free seat not statically allocated starting from i included
            i = getChannelManager().getNextFreeSeatNumber(i);
            if (i==null) break;
            //See if this seat is already dynamically allocated
            if (ChannelStreamerConnectionStatusHelper.getStreamerData(i)==null)
                return i;
            i++;
            //Try another number
        }
        
        return null;
    }
    
    @Override
    public ActionCollection<ChannelForm> getSecuredChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception {
        Collection<Channel> channels = getChannelManager().getFilteredChannels(constraint);
        ActionCollection<ChannelForm> forms = channelDTOAssembler.toDTO(channels, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(CHANNEL_LOG_OPERATION.read, channels);
        return forms;
    }

    @Override
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception {
        Collection<Channel> channels = getChannelManager().getFilteredChannelsForProgrammeId(constraint, programmeId);
        ActionCollection<ChannelForm> forms = channelDTOAssembler.toDTO(channels, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(CHANNEL_LOG_OPERATION.read, channels);
        return forms;
    }

    @Override
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception {
        Collection<Channel> channels = getChannelManager().getFilteredChannelsForProgrammeIds(constraint, programmeIds);
        ActionCollection<ChannelForm> forms = channelDTOAssembler.toDTO(channels, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(CHANNEL_LOG_OPERATION.read, channels);
        return forms;
    }

    @Override
    public Collection<String> getUnsecuredOpenChannelLabels() throws Exception {
        return getChannelManager().getOpenChannelLabels();
    }

    protected void log(CHANNEL_LOG_OPERATION operation, Collection<Channel> channels) throws Exception {
        if (channels != null && !channels.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<ChannelLog> logs = new HashSet<ChannelLog>();
            for (Channel channel : channels) {
                logs.add(new ChannelLog(operation, channel));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(CHANNEL_LOG_OPERATION operation, Channel channel) throws Exception {
        if (channel != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            getSecurityManager().addJournal(new ChannelLog(operation, channel));
        }
    }

    protected void notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION operation, Channel channel, Programme programme) throws Exception {
        notify(operation, channel, programme, operation.getRecipients(getSecurityManager(), channel, programme));
    }
    protected void notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION operation, Channel channel, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && programme != null && recipients != null) {
            Collection<ChannelProgrammeLinkNotification> notifications = new HashSet<ChannelProgrammeLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelProgrammeLinkNotification(operation, channel, programme, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION operation, Channel channel, Webservice webservice) throws Exception {
        notify(operation, channel, webservice, operation.getRecipients(getSecurityManager(), channel, webservice));
    }
    protected void notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION operation, Channel channel, Webservice webservice, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && webservice != null && recipients != null) {
            Collection<ChannelWebserviceLinkNotification> notifications = new HashSet<ChannelWebserviceLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelWebserviceLinkNotification(operation, channel, webservice, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }    
    protected void notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelChannelAdministratorLinkNotification> notifications = new HashSet<ChannelChannelAdministratorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelChannelAdministratorLinkNotification(operation, channel, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
    protected void notify(CHANNELLISTENER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }
    protected void notify(CHANNELLISTENER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelListenerLinkNotification> notifications = new HashSet<ChannelListenerLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelListenerLinkNotification(operation, channel, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
    protected void notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }
    protected void notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelProgrammeManagerLinkNotification> notifications = new HashSet<ChannelProgrammeManagerLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelProgrammeManagerLinkNotification(operation, channel, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION operation, Channel channel, TimetableSlot timetableSlot) throws Exception {
        notify(operation, channel, timetableSlot, operation.getRecipients(getSecurityManager(), channel, timetableSlot));
    }
    protected void notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION operation, Channel channel, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && timetableSlot != null && recipients != null) {
            Collection<ChannelTimetableSlotLinkNotification> notifications = new HashSet<ChannelTimetableSlotLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelTimetableSlotLinkNotification(operation, channel, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNEL_NOTIFICATION_OPERATION operation, Channel channel) throws Exception {
        notify(operation, channel, operation.getRecipients(getSecurityManager(), channel));
    }
    protected void notify(CHANNEL_NOTIFICATION_OPERATION operation, Channel channel, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && recipients != null) {
            Collection<ChannelContentNotification> notifications = new HashSet<ChannelContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelContentNotification(operation, channel, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

}
