/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;

import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import java.util.Collection;



/**
 *
 * @author okay_awright
 **/
public interface ChannelFacadeServiceInterface {

    
    public void addSecuredChannel(ChannelForm channelForm) throws Exception;

    
    public void deleteSecuredChannel(String id) throws Exception;
    
    public void deleteSecuredChannels(Collection<String> ids) throws Exception;

    
    public void updateSecuredChannel(ChannelForm channelForm) throws Exception;

    
    public ActionCollectionEntry<ChannelForm> getSecuredChannel(String id) throws Exception;
    //Helper
    
    public String getChannelTimeZone(String id) throws Exception;

    
    public ActionCollection<ChannelForm> getSecuredChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception;
    
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception;

    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception;

    public ActionCollectionEntry<StreamerModule> getSecuredDynamicChannelStreamerMetadata(String id) throws Exception;
    
    public ActionCollectionEntry<String> getSecuredDynamicChannelStreamerKey(String id) throws Exception;

    /** Fetch all channel labels that are opn to listeners self-registration
     * This method is not secured, any authenticated and non-authenticated user can access it
     * @return
     * @throws Exception
     */
    public Collection<String> getUnsecuredOpenChannelLabels() throws Exception;
    
    /**
     * Find the next free available seat to allocate to a streamer
     * This method is not secured, any authenticated and non-authenticated user can access it
     * Returns null if none is available
     * @return
     * @throws Exception 
     */
    public Long getUnsecuredNextAvailableStreamerSeatNumber() throws Exception;

}
