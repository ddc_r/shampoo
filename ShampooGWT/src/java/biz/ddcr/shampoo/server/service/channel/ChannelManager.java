/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.archive.ArchiveForm.ARCHIVE_TAGS;
import biz.ddcr.shampoo.client.form.archive.ReportForm.REPORT_TAGS;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.queue.QueueForm.QUEUE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.server.dao.channel.ChannelDao;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.archive.ArchiveDao;
import biz.ddcr.shampoo.server.dao.programme.ProgrammeDao;
import biz.ddcr.shampoo.server.domain.TimeBasedNote;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.webservice.Hit;
import biz.ddcr.shampoo.server.helper.*;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool;

import java.util.Collection;
import java.util.HashSet;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 *
 */
public class ChannelManager extends GenericService implements ChannelManagerInterface {

    private SecurityManagerInterface securityManager;
    private transient ChannelDao channelDao;
    private transient ProgrammeDao programmeDao;
    private transient ArchiveDao archiveDao;
    //System configuration
    private SystemConfigurationHelper systemConfigurationHelper;
    //Streamer event pile pool
    private StreamerEventPilePool streamerEventPilePoolHelper;

    public StreamerEventPilePool getStreamerEventPilePoolHelper() {
        return streamerEventPilePoolHelper;
    }

    public void setStreamerEventPilePoolHelper(StreamerEventPilePool streamerEventPilePoolHelper) {
        this.streamerEventPilePoolHelper = streamerEventPilePoolHelper;
    }

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public ChannelDao getChannelDao() {
        return channelDao;
    }

    public void setChannelDao(ChannelDao channelDao) {
        this.channelDao = channelDao;
    }

    public ArchiveDao getArchiveDao() {
        return archiveDao;
    }

    public void setArchiveDao(ArchiveDao archiveDao) {
        this.archiveDao = archiveDao;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public ProgrammeDao getProgrammeDao() {
        return programmeDao;
    }

    public void setProgrammeDao(ProgrammeDao programmeDao) {
        this.programmeDao = programmeDao;
    }

    @Override
    public void addChannel(Channel channel) throws Exception {
        try {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(channel);
            channelDao.addChannel(channel);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        }
    }

    @Override
    public void updateChannel(Channel channel) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(channel);
        channelDao.updateChannel(channel);
    }

    @Override
    public void updateChannels(Collection<Channel> channels) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(channels);
        channelDao.updateBatchChannels(channels);
    }

    @Override
    public Channel getChannel(String id) throws Exception {
        Channel channel = channelDao.getChannel(id);
        if (channel == null) {
            throw new NoEntityException();
        }
        return channel;
    }

    @Override
    public Channel loadChannel(String id) throws Exception {
        Channel channel = channelDao.loadChannel(id);
        if (channel == null) {
            throw new NoEntityException();
        }
        return channel;
    }

    @Override
    public Collection<Channel> getChannels(Collection<String> ids) throws Exception {
        Collection<Channel> channels = channelDao.getChannels(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (channels == null || ids.size() != channels.size()) {
            throw new NoEntityException();
        }
        return channels;
    }

    @Override
    public Collection<Channel> loadChannels(Collection<String> ids) throws Exception {
        Collection<Channel> channels = channelDao.loadChannels(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (channels == null || ids.size() != channels.size()) {
            throw new NoEntityException();
        }
        return channels;
    }

    @Override
    public Archive loadArchive(String id) throws Exception {
        Archive archive = archiveDao.loadArchive(id);
        if (archive == null) {
            throw new NoEntityException();
        }
        return archive;
    }

    @Override
    public Archive getArchive(String id) throws Exception {
        Archive slot = archiveDao.getArchive(id);
        if (slot == null) {
            throw new NoEntityException();
        }
        return slot;
    }

    @Override
    public Collection<Archive> getArchives(Collection<String> ids) throws Exception {
        Collection<Archive> archives = archiveDao.getArchives(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (archives == null || ids.size() != archives.size()) {
            throw new NoEntityException();
        }
        return archives;
    }

    @Override
    public Collection<Archive> getFreshestArchives(Channel channel, int maxNumberOfItems) throws Exception {
        if (channel != null) {
            //The lack of backward iterator for sets is a real burden,
            //use a custom HQL request for this specific task
            long selectionIndex = 0;
            if (maxNumberOfItems<1) maxNumberOfItems=1;
            Collection<Archive> archives =
                    (Collection<Archive>) archiveDao.getSelectedArchives(
                    channel.getLabel(),
                    selectionIndex,
                    selectionIndex + maxNumberOfItems - 1);
            //The collection should be sorted with freshest archives first
            return archives;
        }
        return null;
    }    
    
    @Override
    public TimetableSlot getTimetableSlot(String id) throws Exception {
        TimetableSlot slot = channelDao.getTimetableSlot(id);
        if (slot == null) {
            throw new NoEntityException();
        }
        return slot;
    }

    @Override
    public Collection<TimetableSlot> getTimetableSlots(Collection<String> ids) throws Exception {
        Collection<TimetableSlot> timetableSlots = channelDao.getTimetableSlots(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (timetableSlots == null || ids.size() != timetableSlots.size()) {
            throw new NoEntityException();
        }
        return timetableSlots;
    }

    @Override
    public Webservice loadWebservice(String id) throws Exception {
        Webservice webservice = channelDao.loadWebservice(id);
        if (webservice == null) {
            throw new NoEntityException();
        }
        return webservice;
    }

    @Override
    public Webservice getWebservice(String id) throws Exception {
        Webservice slot = channelDao.getWebservice(id);
        if (slot == null) {
            throw new NoEntityException();
        }
        return slot;
    }
    
    @Override
    public Hit getLatestHit(Webservice webservice) throws Exception {
        if (webservice!=null) {
            Hit hit = channelDao.getLatestWebserviceHit(webservice.getApiKey());
            if (hit == null) {
                throw new NoEntityException();
            }
            return hit;
        }
        throw new NoEntityException();
    }

    @Override
    public Collection<Webservice> getWebservices(Collection<String> ids) throws Exception {
        Collection<Webservice> webservices = channelDao.getWebservices(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (webservices == null || ids.size() != webservices.size()) {
            throw new NoEntityException();
        }
        return webservices;
    }

    /*
     * public TimetableSlot getTimetableSlot(TimetableSlotLocation location)
     * throws Exception { TimetableSlot slot =
     * channelDao.loadTimetableSlot(location); if (slot == null) { throw new
     * NoEntityException(); } return slot;
     }
     */
    @Override
    public TimetableSlot getTimetableSlot(String channelId, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        TimetableSlot slot = channelDao.getTimetableSlot(channelId, startTime);
        if (slot == null) {
            throw new NoEntityException();
        }
        return slot;
    }

    @Override
    public TimetableSlot loadTimetableSlot(String timetableSlotId) throws Exception {
        TimetableSlot slot = channelDao.loadTimetableSlot(timetableSlotId);
        if (slot == null) {
            throw new NoEntityException();
        }
        return slot;
    }

    @Override
    public void deleteChannel(Channel channel) throws Exception {
        channelDao.deleteChannel(channel);
        //Drop the streamer event pile linked to this channel as well
        if (channel != null) {
            final Long seatNumber = ChannelStreamerConnectionStatusHelper.getSeatNumber(channel.getLabel());
            if (seatNumber != null) {
                getStreamerEventPilePoolHelper().clearPile(seatNumber);
                //Do NOT forget to manually unregister the channel/streamer connection from the pool
                ChannelStreamerConnectionStatusHelper.deleteStreamerChannelStateConnection(seatNumber);
            }
        }
    }

    @Override
    public void deleteChannels(Collection<Channel> channels) throws Exception {
        channelDao.deleteBatchChannels(channels);
        //Drop the streamer event piles linked to these channels as well
        if (channels != null) {
            for (Channel channel : channels) {
                final Long seatNumber = ChannelStreamerConnectionStatusHelper.getSeatNumber(channel.getLabel());
                if (seatNumber != null) {
                    getStreamerEventPilePoolHelper().clearPile(seatNumber);
                    //Do NOT forget to manually unregister the channel/streamer connection from the pool
                    ChannelStreamerConnectionStatusHelper.deleteStreamerChannelStateConnection(seatNumber);
                }
            }
        }
    }

    @Override
    public void deleteChannel(String id) throws Exception {
        Channel channel = getChannel(id);
        deleteChannel(channel);
    }

    @Override
    public boolean checkAccessViewChannels() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Channel.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewTimetableSlots() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = TimetableSlot.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewArchives() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Archive.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewQueues() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = QueueItem.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewWebservices() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Webservice.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    protected boolean checkAccessChannelCreator(Channel channelToCheck, RightsBundle rightsBundle) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //Check first if the authenticated user has the minimal rights to touch the entity
                if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle)) //Then see if:
                //-no right is bound to the entity
                //-and either he's its creator or if it has none
                {
                    return (channelToCheck != null
                            && channelToCheck.getListenerRights().isEmpty()
                            && channelToCheck.getProgrammeManagerRights().isEmpty()
                            && channelToCheck.getChannelAdministratorRights().isEmpty()
                            && (channelToCheck.getLatestEditor() == null
                            || getSecurityManager().isCurrentlyAuthenticatedUser(channelToCheck.getLatestEditor())));
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewChannel(String channelIdToCheck) throws Exception {
        return checkAccessViewChannel(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessViewChannel(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Channel.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewStreamer(String channelIdToCheck) throws Exception {
        return checkAccessViewStreamer(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessViewStreamer(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Channel.getAddRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessQueryStatusStreamer(String channelIdToCheck) throws Exception {
        return checkAccessQueryStatusStreamer(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessQueryStatusStreamer(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Channel.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessListenStreamers() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Channel.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessListenStreamer(String channelToCheckId) throws Exception {
        return checkAccessListenStreamer(getChannel(channelToCheckId));
    }

    @Override
    public boolean checkAccessListenStreamer(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Channel.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewReports() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Report.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewReport(String channelToCheckId) throws Exception {
        return checkAccessViewReport(getChannel(channelToCheckId));
    }

    @Override
    public boolean checkAccessViewReport(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Report.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAddReports() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Report.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddReport(String channelToCheckId) throws Exception {
        return checkAccessAddReport(getChannel(channelToCheckId));
    }

    @Override
    public boolean checkAccessAddReport(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Report.getAddRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteReports() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Report.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteReport(String channelToCheckId) throws Exception {
        return checkAccessDeleteReport(getChannel(channelToCheckId));
    }

    @Override
    public boolean checkAccessDeleteReport(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Report.getDeleteRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewWebservice(String channelIdToCheck) throws Exception {
        return checkAccessViewWebservice(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessViewWebservice(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Webservice.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    /**
     * Special rights a bit broader than a simple View but less than Edit Used
     * by Programme Managers for linking programmes to channels
     *
     * @param programmeToCheckId
     * @return
     * @throws Exception
     *
     */
    @Override
    public boolean checkAccessLimitedProgrammeDependencyUpdateChannel(String channelIdToCheck) throws Exception {
        return checkAccessLimitedProgrammeDependencyUpdateChannel(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessLimitedProgrammeDependencyUpdateChannel(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Channel.getLimitedProgrammeBindingRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewTimetableSlot(String channelIdToCheck) throws Exception {
        return checkAccessViewTimetableSlot(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessViewTimetableSlot(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = TimetableSlot.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewArchive(String channelIdToCheck) throws Exception {
        return checkAccessViewArchive(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessViewArchive(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Archive.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewQueue(String channelIdToCheck) throws Exception {
        return checkAccessViewQueue(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessViewQueue(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound channels to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = QueueItem.getViewRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAddChannels() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Channel.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddTimetableSlots() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = TimetableSlot.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddTimetableSlot(String channelIdToCheck) throws Exception {
        return checkAccessAddTimetableSlot(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessAddTimetableSlot(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = TimetableSlot.getAddRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAddWebservices() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Webservice.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddWebservice(String channelIdToCheck) throws Exception {
        return checkAccessAddWebservice(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessAddWebservice(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Webservice.getAddRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateChannels() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Channel.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdateTimetableSlots() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = TimetableSlot.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdateWebservices() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Webservice.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdateChannel(String channelIdToCheck) throws Exception {
        return checkAccessUpdateChannel(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateChannel(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Channel.getUpdateRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateStreamer(String channelIdToCheck) throws Exception {
        return checkAccessUpdateStreamer(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateStreamer(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Channel.getAddRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateWebservice(String channelIdToCheck) throws Exception {
        return checkAccessUpdateWebservice(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateWebservice(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Webservice.getUpdateRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateTimetableSlot(String channelIdToCheck) throws Exception {
        return checkAccessUpdateTimetableSlot(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateTimetableSlot(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = TimetableSlot.getUpdateRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateReport(String channelIdToCheck) throws Exception {
        return checkAccessUpdateReport(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateReport(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Report.getUpdateRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteChannels() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Channel.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteTimetableSlots() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = TimetableSlot.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteArchives() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Archive.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteWebservices() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Webservice.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteChannel(String channelIdToCheck) throws Exception {
        return checkAccessDeleteChannel(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessDeleteChannel(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Channel.getDeleteRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteTimetableSlot(String channelIdToCheck) throws Exception {
        return checkAccessDeleteTimetableSlot(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessDeleteTimetableSlot(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = TimetableSlot.getDeleteRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteArchive(String channelIdToCheck) throws Exception {
        return checkAccessDeleteArchive(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessDeleteArchive(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Archive.getDeleteRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteWebservice(String channelIdToCheck) throws Exception {
        return checkAccessDeleteWebservice(getChannel(channelIdToCheck));
    }

    @Override
    public boolean checkAccessDeleteWebservice(Channel channelToCheck) throws Exception {
        if (channelToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Webservice.getDeleteRights();

                return (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(channelToCheck, rightsBundle)
                        || checkAccessChannelCreator(channelToCheck, rightsBundle));
            }
        }
        return false;
    }

    @Override
    public Collection<Channel> getAllChannels() throws Exception {
        return channelDao.getAllChannels();
    }

    @Override
    public Collection<Channel> getFilteredChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception {
        return getFilteredChannels(constraint, (String) null);
    }

    @Override
    public Collection<Channel> getFilteredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception {
        return getFilteredChannels(constraint, programmeId);
    }

    protected Collection<Channel> getFilteredChannels(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception {
        Collection<Channel> channels;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (programmeId == null) {
                channels = channelDao.getAllChannels(constraint);
            } else {
                channels = channelDao.getChannelsForProgramme(constraint, programmeId);
            }
        } else {

            //Gather all channels visible by the authenticated user

            RightsBundle rightsBundle = Channel.getViewRights();

            channels = channelDao.getChannelsWithRightsByChannelsForProgramme(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    programmeId);
        }
        return channels;
    }

    @Override
    public Collection<Channel> getFilteredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception {
        return getFilteredChannels(constraint, programmeIds);
    }

    protected Collection<Channel> getFilteredChannels(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception {
        Collection<Channel> channels;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (programmeIds == null || programmeIds.isEmpty()) {
                channels = channelDao.getAllChannels(constraint);
            } else {
                channels = channelDao.getChannelsForProgrammes(constraint, programmeIds);
            }
        } else {

            //Gather all channels visible by the authenticated user

            RightsBundle rightsBundle = Channel.getViewRights();

            channels = channelDao.getChannelsWithRightsByChannelsForProgrammes(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    programmeIds);
        }
        return channels;
    }

    public Collection<Channel> getFilteredUnlinkedChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception {
        Collection<Channel> channels;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            channels = channelDao.getAllChannelsWithNoRight(constraint);
        } else {
            channels = channelDao.getAllChannelsWithNoRightForCreator(getSecurityManager().getCurrentlyAuthenticatedUser().getUsername(), constraint);
        }
        return channels;
    }

    @Override
    public Collection<TimetableSlot> getFilteredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId) throws Exception {
        Collection<TimetableSlot> slots;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            slots = channelDao.getTimetableSlotsForChannel(constraint, channelId);
        } else {

            //Gather all channels visible by the authenticated user
            RightsBundle rightsBundle = TimetableSlot.getViewRights();

            slots = channelDao.getTimetableSlotsWithRightsByChannelsForChannel(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    channelId);
        }
        return slots;
    }

    @Override
    public Collection<Webservice> getFilteredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint) throws Exception {
        Collection<Webservice> wses;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            wses = channelDao.getAllWebservices(constraint);
        } else {

            //Gather all channels visible by the authenticated user
            RightsBundle rightsBundle = Webservice.getViewRights();

            wses = channelDao.getWebservicesWithRightsByChannels(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint);
        }
        return wses;
    }

    @Override
    public Collection<TimetableSlot> getFilteredTimetableSlotsForChannelIdAndWeek(
            String channelId,
            YearMonthWeekDayHourMinuteInterface startOfWeek) throws Exception {

        if (channelId == null || startOfWeek == null) {
            return new HashSet<TimetableSlot>();
        }

        Channel channel = getChannel(channelId);

        Collection<TimetableSlot> slots;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            slots = channelDao.getWeekTimetableSlotsForChannel(
                    channel.getLabel(), channel.getTimezone(),
                    startOfWeek);
        } else {

            //Gather all channels visible by the authenticated user
            RightsBundle rightsBundle = TimetableSlot.getViewRights();

            slots = channelDao.getWeekTimetableSlotsWithRightsByChannelsForChannel(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    channel.getLabel(), channel.getTimezone(),
                    startOfWeek);
        }
        return slots;
    }

    @Override
    public Collection<TimetableSlot> getFilteredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId) throws Exception {
        Collection<TimetableSlot> slots;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            slots = channelDao.getTimetableSlotsForProgramme(constraint, programmeId);
        } else {

            //Gather all channels visible by the authenticated user
            RightsBundle rightsBundle = TimetableSlot.getViewRights();

            slots = channelDao.getTimetableSlotsWithRightsByChannelsForProgramme(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    programmeId);
        }
        return slots;
    }

    @Override
    public Collection<TimetableSlot> getFilteredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId) throws Exception {
        Collection<TimetableSlot> slots;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            slots = channelDao.getTimetableSlotsForPlaylist(constraint, playlistId);
        } else {

            //Gather all channels visible by the authenticated user
            RightsBundle rightsBundle = TimetableSlot.getViewRights();

            slots = channelDao.getTimetableSlotsWithRightsByChannelsForPlaylist(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    playlistId);
        }
        return slots;
    }

    @Override
    public Collection<Archive> getFilteredArchives(GroupAndSort<ARCHIVE_TAGS> constraint) throws Exception {
        return getFilteredArchivesForChannelId(constraint, null);
    }

    @Override
    public Collection<Archive> getFilteredArchivesForChannelId(GroupAndSort<ARCHIVE_TAGS> constraint, String channelId) throws Exception {
        Collection<Archive> archives;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (channelId == null) {
                archives = archiveDao.getAllArchives(constraint);
            } else {
                archives = archiveDao.getArchivesForChannel(constraint, channelId);
            }
        } else {

            //Gather all channels visible by the authenticated user

            RightsBundle rightsBundle = Archive.getViewRights();

            if (channelId == null) {
                archives = archiveDao.getArchivesWithRightsByChannels(
                        getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint);
            } else {
                archives = archiveDao.getArchivesWithRightsByChannelsForChannel(
                        getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint,
                        channelId);
            }
        }
        return archives;
    }

    @Override
    public Collection<QueueItem> getFilteredQueueItemsForChannelId(GroupAndSort<QUEUE_TAGS> constraint, String channelId) throws Exception {
        Collection<QueueItem> queueItems;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (channelId == null) {
                queueItems = channelDao.getAllQueueItems(constraint);
            } else {
                queueItems = channelDao.getQueueItemsForChannel(constraint, channelId);
            }
        } else {

            //Gather all channels visible by the authenticated user

            RightsBundle rightsBundle = QueueItem.getViewRights();

            if (channelId == null) {
                queueItems = channelDao.getQueueItemsWithRightsByChannels(
                        getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint);
            } else {
                queueItems = channelDao.getQueueItemsWithRightsByChannelsForChannel(
                        getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint,
                        channelId);
            }
        }
        return queueItems;
    }

    @Override
    public void addTimetableSlot(TimetableSlot timetableSlot) throws Exception {
        //Timestamp the creation
        getSecurityManager().setCreationTimestamp(timetableSlot);
        channelDao.addTimetableSlot(timetableSlot);
    }

    @Override
    public void addTimetableSlots(Collection<TimetableSlot> timetableSlots) throws Exception {
        //Timestamp the creation
        getSecurityManager().setCreationTimestamp(timetableSlots);
        channelDao.addBatchTimetableSlots(timetableSlots);
    }

    @Override
    public void addWebservice(Webservice webservice) throws Exception {
        //Timestamp the creation
        getSecurityManager().setCreationTimestamp(webservice);
        channelDao.addWebservice(webservice);
    }

    @Override
    public void addWebservices(Collection<Webservice> webservices) throws Exception {
        //Timestamp the creation
        getSecurityManager().setCreationTimestamp(webservices);
        channelDao.addBatchWebservices(webservices);
    }

    @Override
    public void addArchive(Archive archive) throws Exception {
        //Timestamp the creation
        getSecurityManager().setCreationTimestamp(archive);
        archiveDao.addArchive(archive);
    }

    @Override
    public void addArchives(Collection<? extends Archive> archives) throws Exception {
        //Timestamp the creation
        getSecurityManager().setCreationTimestamp(archives);
        archiveDao.addBatchArchives(archives);
    }

    @Override
    public void deleteTimetableSlot(TimetableSlot timetableSlot) throws Exception {
        channelDao.deleteTimetableSlot(timetableSlot);
    }

    @Override
    public void deleteTimetableSlots(Collection<TimetableSlot> timetableSlots) throws Exception {
        channelDao.deleteBatchTimetableSlots(timetableSlots);
    }

    @Override
    public void deleteWebservice(Webservice webservice) throws Exception {
        channelDao.deleteWebservice(webservice);
    }

    @Override
    public void deleteWebservices(Collection<Webservice> webservices) throws Exception {
        channelDao.deleteBatchWebservices(webservices);
    }

    @Override
    public void deleteArchive(Archive archive) throws Exception {
        archiveDao.deleteArchive(archive);
    }

    @Override
    public void deleteArchives(Collection<Archive> archives) throws Exception {
        archiveDao.deleteBatchArchives(archives);
    }

    @Override
    public void deleteTimetableSlot(String channelId, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        TimetableSlot timetableSlot = getTimetableSlot(channelId, startTime);
        deleteTimetableSlot(timetableSlot);
    }

    @Override
    public void updateTimetableSlot(TimetableSlot timetableSlot) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(timetableSlot);
        channelDao.updateTimetableSlot(timetableSlot);
    }

    @Override
    public void updateWebservice(Webservice webservice) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(webservice);
        channelDao.updateWebservice(webservice);
    }

    /**
     * @deprecated *
     */
    @Override
    public String getChannelTimeZone(String id) throws Exception {
        String timezone = channelDao.getChannelTimezone(id);
        if (timezone == null) {
            throw new NoEntityException();
        }
        //Don't log
        return timezone;
    }

    @Override
    public Collection<String> getOpenChannelLabels() throws Exception {
        return channelDao.getOpenChannelLabels();
    }

    @Override
    public void purgeArchives() throws Exception {
        if (getSystemConfigurationHelper().getArchiveMaxAge() > 0) {

            //Compute the expiration date from now
            YearMonthWeekDayHourMinuteSecondMillisecond expirationDate = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), TimeBasedNote.getFixedTimezone());
            expirationDate.addDays(-getSystemConfigurationHelper().getArchiveMaxAge());

            archiveDao.purgeArchives(expirationDate);
        }
    }

    @Override
    public void purgeHits() throws Exception {
        //Compute the expiration date from now
        YearMonthWeekDayHourMinuteSecondMillisecond expirationDate = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), TimeBasedNote.getFixedTimezone());
        expirationDate.addDays(-1);

        channelDao.purgeHits(expirationDate);
    }

    @Override
    public void addHit(Webservice webservice) throws Exception {
        if (webservice != null) {
            channelDao.addHit(new Hit(webservice, DateHelper.getCurrentTime()));
        }
    }

    @Override
    public Long getDailyNumberOfHits(Webservice webservice) {
        //Compute the expiration date from now
        YearMonthWeekDayHourMinuteSecondMillisecond thresholdDate = new YearMonthWeekDayHourMinuteSecondMillisecond(DateHelper.getCurrentTime(), TimeBasedNote.getFixedTimezone());
        thresholdDate.addDays(-1);
        return getNumberOfHitsAfterThreshold(thresholdDate, webservice);
    }

    @Override
    public Long getNumberOfHitsIn5Minutes(Webservice webservice) {
        //Compute the expiration date from now
        YearMonthWeekDayHourMinuteSecondMillisecond thresholdDate = new YearMonthWeekDayHourMinuteSecondMillisecond(DateHelper.getCurrentTime(), TimeBasedNote.getFixedTimezone());
        thresholdDate.addMinutes(-5);
        return getNumberOfHitsAfterThreshold(thresholdDate, webservice);
    }
    
    protected Long getNumberOfHitsAfterThreshold(YearMonthWeekDayHourMinuteSecondMillisecond thresholdTime, Webservice webservice) {
        if (webservice != null && thresholdTime != null) {

            return channelDao.getNumberOfHitsAfterTimeThreshold(webservice.getApiKey(), thresholdTime);
        }
        throw new NoEntityException();
    }

    @Override
    public Collection<Archive> getArchivesForChannelId(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface fromDate, YearMonthWeekDayHourMinuteSecondMillisecondInterface toDate) throws Exception {
        if (channel != null) {

            if (fromDate != null && toDate != null) {
                //Revert times if necessary
                if (fromDate.compareTo(toDate) > 0) {
                    YearMonthWeekDayHourMinuteSecondMillisecondInterface _temp = toDate;
                    toDate = fromDate;
                    fromDate = _temp;
                }

                //fromDate and toDate must not be set in the future            
                YearMonthWeekDayHourMinuteSecondMillisecondInterface now = new YearMonthWeekDayHourMinuteSecondMillisecond(DateHelper.getCurrentTime(), channel.getTimezone());
                if (fromDate.compareTo(now) > 0) {
                    fromDate = now;
                }
                if (toDate.compareTo(now) > 0) {
                    toDate = now;
                }
            }

            return archiveDao.getArchivesForChannel(channel.getLabel(), fromDate, toDate);
        }
        throw new NoEntityException();
    }

    @Override
    public void addReport(final Report report) throws Exception {
        try {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(report);
            archiveDao.addReport(report);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        }
    }

    @Override
    public void deleteReport(Report report) throws Exception {
        archiveDao.deleteReport(report);
    }

    @Override
    public void deleteReports(Collection<Report> reports) throws Exception {
        archiveDao.deleteBatchReports(reports);
    }

    @Override
    public void deleteReport(String id) throws Exception {
        Report report = getReport(id);
        if (report == null) {
            throw new NoEntityException();
        }
        deleteReport(report);
    }

    @Override
    public void purgeReports() throws Exception {
        if (getSystemConfigurationHelper().getReportMaxAge() > 0) {

            //Compute the expiration date from now
            YearMonthWeekDayHourMinuteSecondMillisecond expirationDate = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), TimeBasedNote.getFixedTimezone());
            expirationDate.addDays(-getSystemConfigurationHelper().getArchiveMaxAge());

            archiveDao.purgeReports(expirationDate);
        }
    }

    @Override
    public Report getReport(String id) throws Exception {
        Report report = archiveDao.getReport(id);
        if (report == null) {
            throw new NoEntityException();
        }
        return report;
    }

    @Override
    public Report loadReport(String id) throws Exception {
        Report report = archiveDao.loadReport(id);
        if (report == null) {
            throw new NoEntityException();
        }
        return report;
    }

    @Override
    public Collection<Report> getReports(Collection<String> ids) throws Exception {
        Collection<Report> reports = archiveDao.getReports(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (reports == null || ids.size() != reports.size()) {
            throw new NoEntityException();
        }
        return reports;
    }

    @Override
    public Collection<Report> loadReports(Collection<String> ids) throws Exception {
        Collection<Report> reports = archiveDao.loadReports(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (reports == null || ids.size() != reports.size()) {
            throw new NoEntityException();
        }
        return reports;
    }

    @Override
    public void addReports(Collection<Report> reports) throws Exception {
        if (reports != null) {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(reports);
            archiveDao.addBatchReports(reports);
            //No file from the datastore to update
        }
    }

    @Override
    public void updateReports(Collection<Report> reports) throws Exception {
        if (reports != null) {
            //Timestamp the edition
            getSecurityManager().setEditionTimestamp(reports);
            archiveDao.updateBatchReports(reports);
        }
    }

    @Override
    public Collection<Report> getFilteredReports(GroupAndSort<REPORT_TAGS> constraint) throws Exception {
        return getFilteredReports(constraint, null);
    }

    @Override
    public Collection<Report> getFilteredReportsForChannelId(GroupAndSort<REPORT_TAGS> constraint, String channelId) throws Exception {
        return getFilteredReports(constraint, channelId);
    }

    protected Collection<Report> getFilteredReports(GroupAndSort<REPORT_TAGS> constraint, String channelId) throws Exception {
        Collection<Report> reports;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (channelId == null) {
                reports = archiveDao.getAllReports(constraint);
            } else {
                reports = archiveDao.getReportsForChannel(constraint, channelId);
            }
        } else {

            //Gather all playlists visible by the authenticated user
            RightsBundle rightsBundle = Report.getViewRights();

            if (channelId == null) {
                reports = archiveDao.getReportsWithRightsByChannels(
                        getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint);
            } else {
                reports = archiveDao.getReportsWithRightsByChannelsForChannel(
                        getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint,
                        channelId);
            }

        }
        return reports;
    }

    @Override
    public Collection<Channel> getChannelsForSeatNumber(long seatNumber) throws Exception {
        Collection<Channel> channels = channelDao.getChannelsForSeatNumber(seatNumber);
        if (channels == null) {
            throw new NoEntityException();
        }
        return channels;
    }

    @Override
    public Collection<Channel> getChannelsForDynamicSeats() throws Exception {
        Collection<Channel> channels = channelDao.getChannelsForDynamicSeats();
        if (channels == null) {
            throw new NoEntityException();
        }
        return channels;
    }

    @Override
    public Long getNextFreeSeatNumber(long seatNumberStart) throws Exception {
        return channelDao.getNextFreeSeatNumber(seatNumberStart-1);
    }
    
}
