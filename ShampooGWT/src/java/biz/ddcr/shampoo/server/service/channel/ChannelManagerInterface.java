/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.archive.ArchiveForm.ARCHIVE_TAGS;
import biz.ddcr.shampoo.client.form.archive.ReportForm.REPORT_TAGS;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.queue.QueueForm.QUEUE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.webservice.Hit;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ChannelManagerInterface {

    /** Internal struct **/
    public class ReportFiles {

        public ReportFiles(boolean doUpdateText, String textUploadId) {
            this.doUpdateText = doUpdateText;
            this.textUploadId = textUploadId;
        }
        private boolean doUpdateText;
        private String textUploadId;

        public String getTextUploadId() {
            return textUploadId;
        }

        public void setTextUploadId(String textUploadId) {
            this.textUploadId = textUploadId;
        }

        public boolean doUpdateText() {
            return doUpdateText;
        }

        public void doUpdateText(boolean doUpdateText) {
            this.doUpdateText = doUpdateText;
        }

    }
    
    //Check access to resource
    public boolean checkAccessViewChannels() throws Exception;
    public boolean checkAccessViewChannel(String channelToCheckId) throws Exception;
    public boolean checkAccessViewChannel(Channel channelToCheck) throws Exception;
    public boolean checkAccessAddChannels() throws Exception;
    public boolean checkAccessUpdateChannels() throws Exception;
    public boolean checkAccessUpdateChannel(String channelToCheckId) throws Exception;
    public boolean checkAccessUpdateChannel(Channel channelToCheck) throws Exception;
    public boolean checkAccessLimitedProgrammeDependencyUpdateChannel(String channelToCheckId) throws Exception;
    public boolean checkAccessLimitedProgrammeDependencyUpdateChannel(Channel channelToCheck) throws Exception;
    public boolean checkAccessDeleteChannels() throws Exception;
    public boolean checkAccessDeleteChannel(String channelToCheckId) throws Exception;
    public boolean checkAccessDeleteChannel(Channel channelToCheck) throws Exception;
    public boolean checkAccessViewStreamer(String channelToCheckId) throws Exception;
    public boolean checkAccessViewStreamer(Channel channelToCheck) throws Exception;
    public boolean checkAccessQueryStatusStreamer(String channelToCheckId) throws Exception;
    public boolean checkAccessQueryStatusStreamer(Channel channelToCheck) throws Exception;
    public boolean checkAccessListenStreamers() throws Exception;
    public boolean checkAccessListenStreamer(Channel channelToCheck) throws Exception;
    public boolean checkAccessListenStreamer(String channelToCheckId) throws Exception;
    public boolean checkAccessUpdateStreamer(String channelToCheckId) throws Exception;
    public boolean checkAccessUpdateStreamer(Channel channelToCheck) throws Exception;
    public boolean checkAccessViewTimetableSlots() throws Exception;
    public boolean checkAccessViewTimetableSlot(String channelToCheckId) throws Exception;
    public boolean checkAccessViewTimetableSlot(Channel channelToCheck) throws Exception;
    public boolean checkAccessAddTimetableSlots() throws Exception;
    public boolean checkAccessAddTimetableSlot(String channelToCheckId) throws Exception;
    public boolean checkAccessAddTimetableSlot(Channel channelToCheck) throws Exception;
    public boolean checkAccessUpdateTimetableSlots() throws Exception;
    public boolean checkAccessUpdateTimetableSlot(String channelToCheckId) throws Exception;
    public boolean checkAccessUpdateTimetableSlot(Channel channelToCheck) throws Exception;
    public boolean checkAccessDeleteTimetableSlots() throws Exception;
    public boolean checkAccessDeleteTimetableSlot(String channelToCheckId) throws Exception;
    public boolean checkAccessDeleteTimetableSlot(Channel channelToCheck) throws Exception;
    public boolean checkAccessViewQueues() throws Exception;
    public boolean checkAccessViewQueue(String channelIdToCheck) throws Exception;
    public boolean checkAccessViewQueue(Channel channelToCheck) throws Exception;
    public boolean checkAccessViewReports() throws Exception;
    public boolean checkAccessViewReport(String channelToCheckId) throws Exception;
    public boolean checkAccessViewReport(Channel channelToCheck) throws Exception;
    public boolean checkAccessAddReports() throws Exception;
    public boolean checkAccessAddReport(String channelToCheckId) throws Exception;
    public boolean checkAccessAddReport(Channel channelToCheck) throws Exception;
    public boolean checkAccessUpdateReport(String channelToCheckId) throws Exception;
    public boolean checkAccessUpdateReport(Channel channelToCheck) throws Exception;
    public boolean checkAccessDeleteReports() throws Exception;
    public boolean checkAccessDeleteReport(String channelToCheckId) throws Exception;
    public boolean checkAccessDeleteReport(Channel channelToCheck) throws Exception;

    //Editing objects
    public void addChannel(Channel channel) throws Exception;

    public void updateChannel(Channel channel) throws Exception;
    public void updateChannels(Collection<Channel> channels) throws Exception;
    
    public void deleteChannel(Channel channel) throws Exception;
    public void deleteChannels(Collection<Channel> channels) throws Exception;
    public void deleteChannel(String id) throws Exception;

    public void addTimetableSlot(TimetableSlot timetableSlot) throws Exception;
    public void addTimetableSlots(Collection<TimetableSlot> timetableSlots) throws Exception;

    public void updateTimetableSlot(TimetableSlot timetableSlot) throws Exception;

    public void deleteTimetableSlot(TimetableSlot timetableSlot) throws Exception;
    public void deleteTimetableSlots(Collection<TimetableSlot> timetableSlots) throws Exception;
    public void deleteTimetableSlot(String channelId, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception;
    
    public void addReport(Report report) throws Exception;
    public void addReports(Collection<Report> reports) throws Exception;

    public void updateReports(Collection<Report> reports) throws Exception;
    
    public void deleteReport(Report report) throws Exception;
    public void deleteReports(Collection<Report> reports) throws Exception;
    public void deleteReport(String id) throws Exception;
    public void purgeReports() throws Exception;
    
    //Fetch objects
    public Channel getChannel(String id) throws Exception;
    public Channel loadChannel(String id) throws Exception;
    public Collection<Channel> getChannels(Collection<String> ids) throws Exception;
    public Collection<Channel> loadChannels(Collection<String> ids) throws Exception;
    public String getChannelTimeZone(String id) throws Exception;
    public TimetableSlot getTimetableSlot(String id) throws Exception;
    public Collection<TimetableSlot> getTimetableSlots(Collection<String> ids) throws Exception;
    /**
     * Get the timetable slot that starts at the given time exactly
     * @param channelId
     * @param startTime
     * @return
     * @throws Exception
     */
    public TimetableSlot getTimetableSlot(String channelId, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception;
    public TimetableSlot loadTimetableSlot(String timetableSlotId) throws Exception;
    public Report getReport(String id) throws Exception;
    public Report loadReport(String id) throws Exception;
    public Collection<Report> getReports(Collection<String> ids) throws Exception;
    public Collection<Report> loadReports(Collection<String> ids) throws Exception;       
    //Helper

    public Collection<Channel> getAllChannels() throws Exception;

    //Fetching objects with associated access policy flags
    public Collection<Channel> getFilteredChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception;
    public Collection<Channel> getFilteredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception;
    public Collection<Channel> getFilteredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception;

    public Collection<Report> getFilteredReports(GroupAndSort<REPORT_TAGS> constraint) throws Exception;
    public Collection<Report> getFilteredReportsForChannelId(GroupAndSort<REPORT_TAGS> constraint, String channelId) throws Exception;
    
    public Collection<TimetableSlot> getFilteredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId) throws Exception;
    public Collection<TimetableSlot> getFilteredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId) throws Exception;
    public Collection<TimetableSlot> getFilteredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId) throws Exception;
    public Collection<TimetableSlot> getFilteredTimetableSlotsForChannelIdAndWeek(
            String channelId,
            YearMonthWeekDayHourMinuteInterface weekStart) throws Exception;

    //Check access to resource
    public boolean checkAccessViewArchives() throws Exception;
    public boolean checkAccessViewArchive(String channelToCheckId) throws Exception;
    public boolean checkAccessViewArchive(Channel channelToCheck) throws Exception;
    public boolean checkAccessDeleteArchives() throws Exception;
    public boolean checkAccessDeleteArchive(String channelToCheckId) throws Exception;
    public boolean checkAccessDeleteArchive(Channel channelToCheck) throws Exception;
    
    //Archiveization
    public void addArchive(Archive archive) throws Exception;
    public void addArchives(Collection<? extends Archive> archives) throws Exception;

    public void deleteArchive(Archive archive) throws Exception;
    public void deleteArchives(Collection<Archive> archives) throws Exception;
    public void purgeArchives() throws Exception;

    public Archive getArchive(String id) throws Exception;
    public Archive loadArchive(String id) throws Exception;
    public Collection<Archive> getArchives(Collection<String> ids) throws Exception;
    /* HQL optimized version */
    public Collection<Archive> getFreshestArchives(Channel channel, int maxNumberOfItems) throws Exception;

    public Collection<Archive> getFilteredArchives(GroupAndSort<ARCHIVE_TAGS> constraint) throws Exception;
    public Collection<Archive> getFilteredArchivesForChannelId(GroupAndSort<ARCHIVE_TAGS> constraint, String channelId) throws Exception;

    public Collection<Archive> getArchivesForChannelId(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface fromDate, YearMonthWeekDayHourMinuteSecondMillisecondInterface toDate) throws Exception;
    
    public Collection<Channel> getChannelsForSeatNumber(long seatNumber) throws Exception;
    public Collection<Channel> getChannelsForDynamicSeats() throws Exception;
    public Long getNextFreeSeatNumber(long seatNumberStart) throws Exception;
    
    //Check access to resource
    public boolean checkAccessViewWebservices() throws Exception;
    public boolean checkAccessViewWebservice(String channelToCheckId) throws Exception;
    public boolean checkAccessViewWebservice(Channel channelToCheck) throws Exception;
    public boolean checkAccessAddWebservices() throws Exception;
    public boolean checkAccessAddWebservice(String channelToCheckId) throws Exception;
    public boolean checkAccessAddWebservice(Channel channelToCheck) throws Exception;
    public boolean checkAccessUpdateWebservices() throws Exception;
    public boolean checkAccessUpdateWebservice(String channelToCheckId) throws Exception;
    public boolean checkAccessUpdateWebservice(Channel channelToCheck) throws Exception;
    public boolean checkAccessDeleteWebservices() throws Exception;
    public boolean checkAccessDeleteWebservice(String channelToCheckId) throws Exception;
    public boolean checkAccessDeleteWebservice(Channel channelToCheck) throws Exception;

    //Webserviceization
    public void addWebservice(Webservice webservice) throws Exception;
    public void addWebservices(Collection<Webservice> webservices) throws Exception;
    public void addHit(Webservice webservice) throws Exception;

    public void updateWebservice(Webservice webservice) throws Exception;

    public void deleteWebservice(Webservice webservice) throws Exception;
    public void deleteWebservices(Collection<Webservice> webservices) throws Exception;
    public void purgeHits() throws Exception;

    public Webservice getWebservice(String id) throws Exception;
    public Webservice loadWebservice(String id) throws Exception;
    public Collection<Webservice> getWebservices(Collection<String> ids) throws Exception;

    public Collection<Webservice> getFilteredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint) throws Exception;

    //Helper
    public Hit getLatestHit(Webservice webservice) throws Exception;
    public Long getDailyNumberOfHits(Webservice webservice);
    public Long getNumberOfHitsIn5Minutes(Webservice webservice);

    //Queue items
    public Collection<QueueItem> getFilteredQueueItemsForChannelId(GroupAndSort<QUEUE_TAGS> constraint, String channelId) throws Exception;

    public Collection<String> getOpenChannelLabels() throws Exception;

}
