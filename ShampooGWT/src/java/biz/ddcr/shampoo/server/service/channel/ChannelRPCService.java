/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.channel.ChannelForm;
import biz.ddcr.shampoo.client.form.channel.ChannelForm.CHANNEL_TAGS;
import biz.ddcr.shampoo.client.form.channel.StreamerModule;

import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.serviceAsync.channel.ChannelRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ChannelRPCService extends GWTRPCLocalThreadService implements ChannelRPCServiceInterface {

    private ChannelFacadeServiceInterface channelFacadeService;
    private TimetableFacadeServiceInterface timetableFacadeService;
    private WebserviceFacadeServiceInterface webserviceFacadeService;

    public ChannelFacadeServiceInterface getChannelFacadeService() {
        return channelFacadeService;
    }

    public void setChannelFacadeService(ChannelFacadeServiceInterface channelFacadeService) {
        this.channelFacadeService = channelFacadeService;
    }

    public TimetableFacadeServiceInterface getTimetableFacadeService() {
        return timetableFacadeService;
    }

    public void setTimetableFacadeService(TimetableFacadeServiceInterface timetableFacadeService) {
        this.timetableFacadeService = timetableFacadeService;
    }

    public WebserviceFacadeServiceInterface getWebserviceFacadeService() {
        return webserviceFacadeService;
    }

    public void setWebserviceFacadeService(WebserviceFacadeServiceInterface webserviceFacadeService) {
        this.webserviceFacadeService = webserviceFacadeService;
    }

    @Override
    public void addSecuredChannel(ChannelForm channelForm) throws Exception {
        channelFacadeService.addSecuredChannel(channelForm);
    }

    //Specializations of inserts for timetable slots
    @Override
    public void cloneSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime, Collection<YearMonthDayHourMinuteSecondMillisecondInterface> newDates) throws Exception {
        timetableFacadeService.addSecuredTimetableSlotClones(channelId, originalStartTime, newDates);
    }

    @Override
    public void updateSecuredChannel(ChannelForm channelForm) throws Exception {
        channelFacadeService.updateSecuredChannel(channelForm);
    }

    @Override
    public void deleteSecuredChannel(String id) throws Exception {
        channelFacadeService.deleteSecuredChannel(id);
    }
    @Override
    public void deleteSecuredChannels(Collection<String> ids) throws Exception {
        channelFacadeService.deleteSecuredChannels(ids);
    }

    @Override
    public ActionCollectionEntry<ChannelForm> getSecuredChannel(String id) throws Exception {
        return channelFacadeService.getSecuredChannel(id);
    }

    @Override
    public ActionCollection<ChannelForm> getSecuredChannels(GroupAndSort<CHANNEL_TAGS> constraint) throws Exception {
        return channelFacadeService.getSecuredChannels(constraint);
    }

    @Override
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeId(GroupAndSort<CHANNEL_TAGS> constraint, String programmeId) throws Exception {
        return channelFacadeService.getSecuredChannelsForProgrammeId(constraint, programmeId);
    }

    @Override
    public ActionCollection<ChannelForm> getSecuredChannelsForProgrammeIds(GroupAndSort<CHANNEL_TAGS> constraint, Collection<String> programmeIds) throws Exception {
        return channelFacadeService.getSecuredChannelsForProgrammeIds(constraint, programmeIds);
    }

    @Override
    public void addSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception {
        timetableFacadeService.addSecuredTimetableSlot(timetableSlotForm);
    }

    @Override
    public void updateSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception {
        timetableFacadeService.updateSecuredTimetableSlot(timetableSlotForm);
    }
    @Override
    public void shiftSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long shift) throws Exception {
        timetableFacadeService.updateShiftSecuredTimetableSlot(channelId, startTime, shift);
    }
    @Override
    public void resizeSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long size) throws Exception {
        timetableFacadeService.updateResizeSecuredTimetableSlot(channelId, startTime, size);
    }
    @Override
    public void setDecommissioningSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, YearMonthDayInterface decommissioningDate) throws Exception {
        timetableFacadeService.updateDecommissioningDateSecuredTimetableSlot(channelId, startTime, decommissioningDate);
    }
    @Override
    public void enableSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, boolean enabled) throws Exception {
        timetableFacadeService.updateEnableSecuredTimetableSlot(channelId, startTime, enabled);
    }
    @Override
    public void setPlaylistSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, String playlistId) throws Exception {
        timetableFacadeService.updatePlaylistSecuredTimetableSlot(channelId, startTime, playlistId);
    }

    @Override
    public void deleteSecuredTimetableSlot(TimetableSlotFormID id) throws Exception {
        timetableFacadeService.deleteSecuredTimetableSlot(id);
    }
    @Override
    public void deleteSecuredTimetableSlots(Collection<TimetableSlotFormID> ids) throws Exception {
        timetableFacadeService.deleteSecuredTimetableSlots(ids);
    }

    @Override
    public ActionCollectionEntry<TimetableSlotForm> getSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlot(channelId, startTime);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForChannelId(constraint, channelId, null);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId, String localTimeZone) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForChannelId(constraint, channelId, localTimeZone);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForProgrammeId(constraint, programmeId, null);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId, String localTimeZone) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForProgrammeId(constraint, programmeId, localTimeZone);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForPlaylistId(constraint, playlistId, null);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId, String localTimeZone) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForPlaylistId(constraint, playlistId, localTimeZone);
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelIdAndWeek(String channelId, YearMonthDayHourMinuteInterface from) throws Exception {
        return timetableFacadeService.getSecuredTimetableSlotsForChannelIdAndWeek(channelId, from);
    }

    @Override
    public String getChannelTimeZone(String id) throws Exception {
        return channelFacadeService.getChannelTimeZone(id);
    }

    @Override
    public ActionCollectionEntry<StreamerModule> getSecuredDynamicChannelStreamerMetadata(String id) throws Exception {
        return channelFacadeService.getSecuredDynamicChannelStreamerMetadata(id);
    }

    @Override
    public ActionCollectionEntry<String> getSecuredChannelStreamerKey(String id) throws Exception {
        return channelFacadeService.getSecuredDynamicChannelStreamerKey(id);
    }
    
    @Override
    public Long getUnsecuredNextAvailableStreamerSeatNumber() throws Exception {
        return channelFacadeService.getUnsecuredNextAvailableStreamerSeatNumber();
    }
    
    @Override
    public Collection<String> getUnsecuredOpenChannelLabels() throws Exception {
        return channelFacadeService.getUnsecuredOpenChannelLabels();
    }

    @Override
    public void addSecuredWebservice(WebserviceForm webserviceForm) throws Exception {
        webserviceFacadeService.addSecuredWebservice(webserviceForm);
    }

    @Override
    public void updateSecuredWebservice(WebserviceForm webserviceForm) throws Exception {
        webserviceFacadeService.updateSecuredWebservice(webserviceForm);
    }

    @Override
    public void deleteSecuredWebservice(String id) throws Exception {
        webserviceFacadeService.deleteSecuredWebservice(id);
    }

    @Override
    public void deleteSecuredWebservices(Collection<String> ids) throws Exception {
        webserviceFacadeService.deleteSecuredWebservices(ids);
    }

    @Override
    public ActionCollectionEntry<WebserviceForm> getSecuredWebservice(String id) throws Exception {
        return webserviceFacadeService.getSecuredWebservice(id);
    }

    @Override
    public ActionCollection<WebserviceForm> getSecuredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint) throws Exception {
        return webserviceFacadeService.getSecuredWebservices(constraint);
    }

    @Override
    public ActionCollectionEntry<WebserviceModule> getSecuredDynamicPublicWebserviceMetadata(String id) throws Exception {
        return webserviceFacadeService.getSecuredDynamicPublicWebserviceMetadata(id);
    }

}
