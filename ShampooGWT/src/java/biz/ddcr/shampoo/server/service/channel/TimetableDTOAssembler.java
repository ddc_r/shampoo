/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
  *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistFormID;
import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm.RECURRING_EVENT;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.timetable.UniqueTimetableSlotForm;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.timetable.DailyTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.MonthlyTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.RecurringTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.UniqueTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.WeeklyTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.YearlyTimetableSlot;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class TimetableDTOAssembler extends GenericService implements TimetableDTOAssemblerInterface {

    private ProgrammeManagerInterface programmeManager;
    private ChannelManagerInterface channelManager;

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    protected TimetableSlot fromCachedDTO(TimetableSlotForm timetableSlotForm, HashMap<String, Playlist> cachedPlaylists, HashMap<String, Channel> cachedChannels, HashMap<String, Programme> cachedProgrammes) throws Exception {

        if (timetableSlotForm != null) {

            //Check if the fully-fledged channel has not already been cached
            Channel cachedChannel = null;
            if (cachedChannels != null) {
                cachedChannel = cachedChannels.get(timetableSlotForm.getRefId().getChannelId());
            }
            if (cachedChannel == null) {
                cachedChannel = getChannelManager().getChannel(timetableSlotForm.getRefId().getChannelId());
                if (cachedChannels != null) {
                    cachedChannels.put(timetableSlotForm.getRefId().getChannelId(), cachedChannel);
                }
            }

            //Check if the fully-fledged playlist has not already been cached
            Playlist cachedPlaylist = null;
            if (timetableSlotForm.getPlaylistId() != null) {
                if (cachedPlaylists != null) {
                    cachedPlaylist = cachedPlaylists.get(timetableSlotForm.getPlaylistId().getItem());
                }
                if (cachedPlaylist == null) {
                    cachedPlaylist = getProgrammeManager().getPlaylist(timetableSlotForm.getPlaylistId().getItem());
                    if (cachedPlaylists != null) {
                        cachedPlaylists.put(timetableSlotForm.getPlaylistId().getKey(), cachedPlaylist);
                    }
                }
            }

            //Check if the fully-fledged programme has not already been cached
            Programme cachedProgramme = null;
            if (cachedProgrammes != null) {
                cachedProgramme = cachedProgrammes.get(timetableSlotForm.getProgrammeId().getItem());
            }
            if (cachedProgramme == null) {
                cachedProgramme = getProgrammeManager().getProgramme(timetableSlotForm.getProgrammeId().getItem());
                if (cachedProgrammes != null) {
                    cachedProgrammes.put(timetableSlotForm.getProgrammeId().getItem(), cachedProgramme);
                }
            }

            TimetableSlot newSlot = null;
            biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface localStartTime = DateDTOAssembler.fromJS(
                    timetableSlotForm.getRefId().getSchedulingTime());
            if (ProxiedClassUtil.castableAs(timetableSlotForm, UniqueTimetableSlotForm.class)) {
                newSlot = new UniqueTimetableSlot(cachedChannel, localStartTime);
            } else {
                switch (((RecurringTimetableSlotForm) timetableSlotForm).getRecurringEvent()) {
                    case daily:
                        newSlot = new DailyTimetableSlot(cachedChannel, localStartTime);
                        break;
                    case weekly:
                        newSlot = new WeeklyTimetableSlot(cachedChannel, localStartTime);
                        break;
                    case monthly:
                        newSlot = new MonthlyTimetableSlot(cachedChannel, localStartTime);
                        break;
                    case yearly:
                        newSlot = new YearlyTimetableSlot(cachedChannel, localStartTime);
                        break;
                }
            }

            //Don't link a playlist if it cannot be accessed
            if (cachedPlaylist != null) {
                if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(cachedPlaylist.getProgramme())) {
                    newSlot.addPlaylist(cachedPlaylist);
                }
            }
            //Because it's a mandatory field, throw an exception if it cannot be stored
            if (getProgrammeManager().checkAccessUpdateProgramme(cachedProgramme)) {
                newSlot.addProgramme(cachedProgramme);
            } else {
                throw new AccessDeniedException();
            }
            biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface localEndTime = DateDTOAssembler.fromJS(
                    timetableSlotForm.getEndTime());
            newSlot.setEndTimeWithCorrection(localEndTime);
            newSlot.setEnabled(timetableSlotForm.getRefId().isEnabled());
            newSlot.setRefID(timetableSlotForm.getRefId().getRefID());

            if (ProxiedClassUtil.castableAs(timetableSlotForm, RecurringTimetableSlotForm.class)) {
                biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface decommissioningDate = ((RecurringTimetableSlotForm) timetableSlotForm).getDecommissioningTime();
                biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface localDecomissioningDate = null;
                if (decommissioningDate != null) {
                    localDecomissioningDate = DateDTOAssembler.fromJS(decommissioningDate);
                }
                ((RecurringTimetableSlot) newSlot).setDecommissioningTimeWithCorrection(localDecomissioningDate);
            }

            return newSlot;

        } else {
            return null;
        }
    }

    @Override
    public Collection<TimetableSlot> fromDTO(Collection<TimetableSlotForm> timetableSlotForms) throws Exception {
        HashSet<TimetableSlot> timetableSlots = new HashSet<TimetableSlot>();
        //Avoid doing too many unnecessary calls to the DTO: use a cache version of the entity to attach if the ID has already been parsed
        HashMap<String, Playlist> cachedPlaylists = new HashMap<String, Playlist>();
        HashMap<String, Channel> cachedChannels = new HashMap<String, Channel>();
        HashMap<String, Programme> cachedProgrammes = new HashMap<String, Programme>();
        for (TimetableSlotForm timetableSlotForm : timetableSlotForms) {
            timetableSlots.add(
                    fromCachedDTO(timetableSlotForm, cachedPlaylists, cachedChannels, cachedProgrammes));
        }
        return timetableSlots;
    }

    @Override
    public TimetableSlot fromDTO(TimetableSlotForm timetableForm) throws Exception {
        return fromCachedDTO(timetableForm, null, null, null);
    }

    /**
     * update an existing playlist with data from a DTO.
     * Returns true if attributes of the original playlist have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(TimetableSlot slot, TimetableSlotForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && slot != null) {

            //Move over all regular properties and the identifier
            biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface newLocalStartTime = DateDTOAssembler.fromJS(
                    form.getRefId().getSchedulingTime());
            if ((newLocalStartTime!=null && !newLocalStartTime.equals(slot.getStartTime())
                    || (newLocalStartTime==null && slot.getStartTime()!=null))) {
                slot.setStartTimeWithCorrection(newLocalStartTime);
                contentUpdated=true;
            }
            biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface newLocalEndTime = DateDTOAssembler.fromJS(
                    form.getEndTime());
            if ((newLocalEndTime!=null && !newLocalEndTime.equals(slot.getEndTime())
                    || (newLocalEndTime==null && slot.getEndTime()!=null))) {
                slot.setEndTimeWithCorrection(newLocalEndTime);
                contentUpdated=true;
            }
            if (form.getRefId().isEnabled()!=slot.isEnabled()) {
                slot.setEnabled(form.getRefId().isEnabled());
                contentUpdated=true;
            }

            //Leave the previous one if we cannot access it
            //or an exception if it can be properly updated
            if (getChannelManager().checkAccessUpdateChannel(slot.getChannel())) {
                slot.removeChannel();
                Channel c = getChannelManager().getChannel(form.getRefId().getChannelId());
                if (getChannelManager().checkAccessUpdateChannel(c)) {
                    slot.addChannel(c);
                } else {
                    throw new AccessDeniedException();
                }
            }

            if (ProxiedClassUtil.castableAs(slot, RecurringTimetableSlot.class) && ProxiedClassUtil.castableAs(form, RecurringTimetableSlotForm.class)) {
                biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface decommissioningDate = ((RecurringTimetableSlotForm) form).getDecommissioningTime();
                biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface localDecomissioningDate = null;
                if (decommissioningDate != null) {
                    localDecomissioningDate = DateDTOAssembler.fromJS(decommissioningDate);
                }
                if ((localDecomissioningDate!=null && localDecomissioningDate.equals(((RecurringTimetableSlot) slot).getDecommissioningTime())
                        || (localDecomissioningDate==null && ((RecurringTimetableSlot) slot).getDecommissioningTime()!=null))) {
                    ((RecurringTimetableSlot) slot).setDecommissioningTimeWithCorrection(localDecomissioningDate);
                    contentUpdated=true;
                }
            }

            //Don't link a playlist if it cannot be accessed
            if (slot.getPlaylist() != null) {
                if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(slot.getPlaylist().getProgramme())) {
                    slot.removePlaylist();
                    //If the playlist within the timetable slot form is null, let's assume the user just wants to remove it
                    if (form.getPlaylistId() != null) {
                        //Otherwise add a new one, suplementary checks such as access control and programme compatibility are performed elsewhere
                        slot.addPlaylist(
                                getProgrammeManager().getPlaylist(form.getPlaylistId().getItem()));
                    }
                }
            }
            //Because it's a mandatory field, throw an exception if it cannot be stored
            if (getProgrammeManager().checkAccessUpdateProgramme(slot.getProgramme())) {
                slot.removeProgramme();
                Programme p = getProgrammeManager().getProgramme(form.getProgrammeId().getItem());
                if (getProgrammeManager().checkAccessUpdateProgramme(p)) {
                    slot.addProgramme(p);
                } else {
                    throw new AccessDeniedException();
                }
            }

        }
        return contentUpdated;

    }

    @Override
    public ActionCollectionEntry<? extends TimetableSlotForm> toDTO(TimetableSlot timetableSlot) throws Exception {
        return toDTO(timetableSlot, null);
    }

    public static TimetableSlotFormID toIdDTO(TimetableSlot timetableSlot, String timeZone) throws Exception {
        TimetableSlotFormID newId = new TimetableSlotFormID();

        newId.setRefID(timetableSlot.getRefID());
        newId.setChannelId(timetableSlot.getChannel().getLabel());
        if (timeZone != null && timeZone.length()!=0) {
            newId.setSchedulingTime(DateDTOAssembler.toJS(timetableSlot.getStartTime().switchTimeZone(timeZone)));
        } else {
            newId.setSchedulingTime(DateDTOAssembler.toJS(timetableSlot.getStartTime()));
        }
        newId.setEnabled(timetableSlot.isEnabled());

        return newId;
    }

    @Override
    public ActionCollectionEntry<? extends TimetableSlotForm> toDTO(TimetableSlot timetableSlot, String timeZone) throws Exception {
        if (timetableSlot != null) {
            TimetableSlotForm newTimetableSlotForm = null;

            if (ProxiedClassUtil.castableAs(timetableSlot, UniqueTimetableSlot.class)) {
                newTimetableSlotForm = new UniqueTimetableSlotForm();

            } else {
                //Then it must be a recurring timetable slot
                newTimetableSlotForm = new RecurringTimetableSlotForm();
                //castableas() checks must be performed from the most distant child to the parent and not the other way round since castableas() is not identical to instanceof
                if (ProxiedClassUtil.castableAs(timetableSlot, YearlyTimetableSlot.class)) {
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.yearly);
                } else if (ProxiedClassUtil.castableAs(timetableSlot, MonthlyTimetableSlot.class)) {
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.monthly);
                } else if (ProxiedClassUtil.castableAs(timetableSlot, WeeklyTimetableSlot.class)) {
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.weekly);
                } else {
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setRecurringEvent(RECURRING_EVENT.daily);
                }

            }

            //Fill in common properties
            newTimetableSlotForm.setCreatorId(timetableSlot.getCreator() != null ? timetableSlot.getCreator().getUsername() : null);
            newTimetableSlotForm.setLatestEditorId(timetableSlot.getLatestEditor() != null ? timetableSlot.getLatestEditor().getUsername() : null);
            newTimetableSlotForm.setCreationDate(timetableSlot.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(timetableSlot.getCreationDate().getTime(), null)) : null);
            newTimetableSlotForm.setLatestModificationDate(timetableSlot.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(timetableSlot.getLatestModificationDate().getTime(), null)) : null);

            //Fill in all attributess
            newTimetableSlotForm.setRefId(toIdDTO(timetableSlot, timeZone));
            if (timeZone != null && timeZone.length()!=0) {
                newTimetableSlotForm.setEndTime(DateDTOAssembler.toJS(timetableSlot.getEndTime().switchTimeZone(timeZone)));
            } else {
                newTimetableSlotForm.setEndTime(DateDTOAssembler.toJS(timetableSlot.getEndTime()));
            }

            if (timetableSlot.getPlaylist() != null) {
                PlaylistFormID newPlaylistID = new PlaylistFormID();

                newPlaylistID.setLabel(timetableSlot.getPlaylist().getLabel());
                newPlaylistID.setRefID(timetableSlot.getPlaylist().getRefID());
                newTimetableSlotForm.setPlaylistId(
                        new ActionMapEntry<PlaylistFormID>(
                        newPlaylistID,
                        decorateEntryWithActions(timetableSlot.getPlaylist())));
            }

            newTimetableSlotForm.setProgrammeId(
                    new ActionMapEntry<String>(
                    timetableSlot.getProgramme().getLabel(),
                    decorateEntryWithActions(timetableSlot.getProgramme())));

            if (ProxiedClassUtil.castableAs(timetableSlot, RecurringTimetableSlot.class)) {
                biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface decommissioningDate = ((RecurringTimetableSlot) timetableSlot).getDecommissioningTime();
                if (decommissioningDate != null && timeZone != null && timeZone.length()!=0) {
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setDecommissioningTime(DateDTOAssembler.toJS(decommissioningDate.switchTimeZone(timeZone)));
                } else {
                    ((RecurringTimetableSlotForm) newTimetableSlotForm).setDecommissioningTime(null);
                }
            }

            //Read-only setting
            newTimetableSlotForm.setNowPlaying(timetableSlot.getStreamItem()!=null);

            return new ActionMapEntry<TimetableSlotForm>(newTimetableSlotForm, decorateEntryWithActions(timetableSlot));
        }
        return null;
    }

    @Override
    public ActionCollection<TimetableSlotForm> toDTO(Collection<TimetableSlot> timetableSlots, boolean doSort) throws Exception {
        return toDTO(timetableSlots, null, doSort);
    }

    @Override
    public ActionCollection<TimetableSlotForm> toDTO(Collection<TimetableSlot> timetableSlots, String timeZone, boolean doSort) throws Exception {
        ActionMap<TimetableSlotForm> timetableSlotForms = doSort ? new ActionSortedMap<TimetableSlotForm>() : new ActionUnsortedMap<TimetableSlotForm>();
        for (TimetableSlot timetableSlot : timetableSlots) {
            timetableSlotForms.set(toDTO(timetableSlot, timeZone));
        }
        return timetableSlotForms;
    }

    protected Collection<ACTION> decorateEntryWithActions(TimetableSlot timetableSlot) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewTimetableSlot(timetableSlot.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Animators for linking playlists to timetable slots, they cannot edit a timetable yet but they can bind playlists
        try {
            if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(timetableSlot.getProgramme())) {
                actions.add(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateTimetableSlot(timetableSlot.getChannel())) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteTimetableSlot(timetableSlot.getChannel())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Playlist playlist) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewPlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Animators for linking playlists to timetable slots, they cannot edit a timetable yet but they can bind playlists
        try {
            if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(playlist.getProgramme())) {
                actions.add(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdatePlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeletePlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Programme programme) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewProgramme(programme)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Curators and Contributors for linking programmes to tracks, they cannot edit a programme yet they can bind tracks
        try {
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programme)) {
                actions.add(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }
}
