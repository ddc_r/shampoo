/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface TimetableDTOAssemblerInterface {

    //DTO to domain conversion
    public TimetableSlot fromDTO(TimetableSlotForm timetableSlotForm) throws Exception;
    public boolean augmentWithDTO(TimetableSlot timetableSlot, TimetableSlotForm timetableSlotForm) throws Exception;

    //Domain to DTO conversion
    public ActionCollectionEntry<? extends TimetableSlotForm> toDTO(TimetableSlot timetableSlot, String timeZone) throws Exception;
    public ActionCollectionEntry<? extends TimetableSlotForm> toDTO(TimetableSlot timetableSlot) throws Exception;

    //Helpers
    public ActionCollection<TimetableSlotForm> toDTO(Collection<TimetableSlot> timetableSlots, String timeZone, boolean doSort) throws Exception;
    public ActionCollection<TimetableSlotForm> toDTO(Collection<TimetableSlot> timetableSlots, boolean doSort) throws Exception;
    public Collection<TimetableSlot> fromDTO(Collection<TimetableSlotForm> timetableSlotForms) throws Exception;

}
