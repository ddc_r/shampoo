/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.TimetableSlotMalformedException;
import biz.ddcr.shampoo.client.helper.errors.TimetableSlotNowPlayingException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelTimetableSlotLinkNotification.CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification.PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification.PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.timetable.RecurringTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.log.TimetableLog.TIMETABLE_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.timetable.log.TimetableLog;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.UniqueTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.notification.TimetableContentNotification;
import biz.ddcr.shampoo.server.domain.timetable.notification.TimetableContentNotification.TIMETABLE_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.HashSet;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 *
 */
public class TimetableFacadeService extends GenericService implements TimetableFacadeServiceInterface {

    private ChannelDTOAssemblerInterface channelDTOAssembler;
    private TimetableDTOAssemblerInterface timetableDTOAssembler;
    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private SecurityManagerInterface securityManager;

    public ChannelDTOAssemblerInterface getChannelDTOAssembler() {
        return channelDTOAssembler;
    }

    public void setChannelDTOAssembler(ChannelDTOAssemblerInterface channelDTOAssembler) {
        this.channelDTOAssembler = channelDTOAssembler;
    }

    public TimetableDTOAssemblerInterface getTimetableDTOAssembler() {
        return timetableDTOAssembler;
    }

    public void setTimetableDTOAssembler(TimetableDTOAssemblerInterface timetableDTOAssembler) {
        this.timetableDTOAssembler = timetableDTOAssembler;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /**
     *
     * Validation process for timetable slots Incorrect values are replaced with
     * default ones unless the problem is not recoverable, then an Exception is
     * thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     *
     */
    private boolean checkTimetableSlot(TimetableSlot timetableSlot) throws Exception {
        boolean result = false;

        if (timetableSlot != null) {

            //A slot can only be active if there's a playlist
            timetableSlot.setEnabled(timetableSlot.getPlaylist() != null ? timetableSlot.isEnabled() : false);

            //Because it's a composite object that connect a channel to a playlist, one must be sure that the given playlist belongs to a programme that is, in turn, part of a compatible channel
            if (!timetableSlot.getProgramme().getChannels().contains(timetableSlot.getChannel())) {
                throw new TimetableSlotMalformedException();
            }
            if (timetableSlot.getPlaylist() != null) {
                if (!timetableSlot.getPlaylist().getProgramme().equals(timetableSlot.getProgramme())) {
                    throw new TimetableSlotMalformedException();
                }
            }

            //The length of the slot cannot be lower than 1000 ms, i.e. 1 minute
            if (timetableSlot.getLengthInMilliseconds() < 1000) {
                timetableSlot.setLengthInMilliseconds(1000);
            }

            //TODO: failsafe check: too lazy to see if endDate is actually greater than decommissioningDate when dealing with recurring slots

            result = true;
        }

        return result;
    }

    /**
     *
     * Add copies of the given timetable slot at the specified moments in time
     *
     * @param id
     * @param newDates
     * @throws Exception
     *
     */
    @Override
    public void addSecuredTimetableSlotClones(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime, Collection<biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface> newJSDates) throws Exception {

        if (newJSDates != null) {

            //Get the original version of this slot
            //It will be this version that will be modified with data coming from the DTO
            TimetableSlot originalSlot = getChannelManager().getTimetableSlot(channelId, DateDTOAssembler.fromJS(originalStartTime));

            //Only unique slots can be cloned
            if (!ProxiedClassUtil.castableAs(originalSlot, UniqueTimetableSlot.class)) {
                throw new NoEntityException();
            }

            Collection<TimetableSlot> clonedSlots = new HashSet<TimetableSlot>();
            for (biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface newJSDate : newJSDates) {
                biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface newDate = DateDTOAssembler.fromJS(newJSDate);

                //Clone the original slot and update both its starting and ending times
                //The clone Id will be auto-incremented when inserted
                TimetableSlot clonedSlot = (TimetableSlot) originalSlot.shallowCopy();
                clonedSlot.setStartTimeWithCorrection(newDate);
                clonedSlot.setLengthInMilliseconds(originalSlot.getLengthInMilliseconds());

                //Check and adjust its dependencies before insertion
                if (checkTimetableSlot(clonedSlot)) {
                    clonedSlots.add(clonedSlot);

                    //Notify responsible users of any changes
                    //All linked entities are then new links
                    if (clonedSlot.getChannel() != null) {
                        notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.add, clonedSlot.getChannel(), clonedSlot);
                    }

                }
            }

            //Everything's fine, now make it persistent in the database
            if (!clonedSlots.isEmpty()) {
                try {
                    getChannelManager().addTimetableSlots(clonedSlots);
                    //Log it
                    log(TIMETABLE_LOG_OPERATION.clone, clonedSlots);
                } catch (DataIntegrityViolationException e) {
                    throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                }
            }

        }

    }

    /**
     * Add a timetable slot and link it to other dependent objects
     *
     * @param timetableSlotForm
     * @throws Exception
     *
     */
    @Override
    public void addSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception {

        if (timetableSlotForm != null) {

            //hydrate the entity from the DTO
            TimetableSlot timetableSlot = timetableDTOAssembler.fromDTO(timetableSlotForm);
            //Then check if it can be accessed
            if (getChannelManager().checkAccessAddTimetableSlot(timetableSlot.getChannel())) {

                //Check and adjust its dependencies before insertion
                if (checkTimetableSlot(timetableSlot)) {
                    //Everything's fine, now make it persistent in the database
                    try {
                        getChannelManager().addTimetableSlot(timetableSlot);
                        //Log it
                        log(TIMETABLE_LOG_OPERATION.add, timetableSlot);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        if (timetableSlot.getChannel() != null) {
                            notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.add, timetableSlot.getChannel(), timetableSlot);
                        }
                        if (timetableSlot.getProgramme() != null) {
                            notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.add, timetableSlot.getProgramme(), timetableSlot);
                        }
                        if (timetableSlot.getPlaylist() != null) {
                            notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, timetableSlot.getPlaylist(), timetableSlot);
                        }

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    public ActionCollectionEntry<TimetableSlotForm> getSecuredTimetableSlot(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface startTime) throws Exception {

        biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface cStartTime = DateDTOAssembler.fromJS(startTime);

        TimetableSlot tts = getChannelManager().getTimetableSlot(channelId, cStartTime);
        ActionCollectionEntry<TimetableSlotForm> ttsFormEntry = (ActionCollectionEntry<TimetableSlotForm>) timetableDTOAssembler.toDTO(tts);
        if (ttsFormEntry == null || ttsFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(TIMETABLE_LOG_OPERATION.read, tts);
            return ttsFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelIdAndWeek(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface from) throws Exception {

        biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteInterface startOfWeek = DateDTOAssembler.fromJS(from);

        Collection<TimetableSlot> timetableSlots = getChannelManager().getFilteredTimetableSlotsForChannelIdAndWeek(channelId, startOfWeek);
        //Shift the fetched timetable slots from the channel timezone back to the one specified in parameter
        ActionCollection<TimetableSlotForm> forms = timetableDTOAssembler.toDTO(timetableSlots, from.getTimeZone(), true);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(TIMETABLE_LOG_OPERATION.read, timetableSlots);
        return forms;
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId, String localTimezone) throws Exception {

        Collection<TimetableSlot> timetableSlots = getChannelManager().getFilteredTimetableSlotsForChannelId(constraint, channelId);
        //Shift the fetched timetable slots from the channel timezone back to the one specified in parameter
        ActionCollection<TimetableSlotForm> forms = timetableDTOAssembler.toDTO(timetableSlots, localTimezone, constraint == null || constraint.getSortByFeature() == null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(TIMETABLE_LOG_OPERATION.read, timetableSlots);
        return forms;
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId, String localTimezone) throws Exception {

        Collection<TimetableSlot> timetableSlots = getChannelManager().getFilteredTimetableSlotsForProgrammeId(constraint, programmeId);
        //Shift the fetched timetable slots from the channel timezone back to the one specified in parameter
        ActionCollection<TimetableSlotForm> forms = timetableDTOAssembler.toDTO(timetableSlots, localTimezone, constraint == null || constraint.getSortByFeature() == null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(TIMETABLE_LOG_OPERATION.read, timetableSlots);
        return forms;
    }

    @Override
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId, String localTimezone) throws Exception {

        Collection<TimetableSlot> timetableSlots = getChannelManager().getFilteredTimetableSlotsForPlaylistId(constraint, playlistId);
        //Shift the fetched timetable slots from the channel timezone back to the one specified in parameter
        ActionCollection<TimetableSlotForm> forms = timetableDTOAssembler.toDTO(timetableSlots, localTimezone, constraint == null || constraint.getSortByFeature() == null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(TIMETABLE_LOG_OPERATION.read, timetableSlots);
        return forms;
    }

    @Override
    public void updateSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception {
        if (timetableSlotForm != null) {

            //Get the original version of this slot
            //It will be this version that will be modified with data coming from the DTO
            TimetableSlot originalSlot = getChannelManager().getTimetableSlot(timetableSlotForm.getRefId().getChannelId(), DateDTOAssembler.fromJS(timetableSlotForm.getRefId().getSchedulingTime()));
            //Check if it can be accessed

            if (getChannelManager().checkAccessUpdateTimetableSlot(timetableSlotForm.getRefId().getChannelId()) && isSlotTouchable(originalSlot)) {
                if (originalSlot != null) {
                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = TIMETABLE_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalSlot);
                    //Save original links before modification (clones)
                    Programme originallyLinkedProgramme = originalSlot.getProgramme();
                    Playlist originallyLinkedPlaylist = originalSlot.getPlaylist();
                    Channel originallyLinkedChannel = originalSlot.getChannel();

                    //Update the existing entity from the DTO
                    boolean areAttributesUpdated = timetableDTOAssembler.augmentWithDTO(originalSlot, timetableSlotForm);



                    //Check and adjust its dependencies before updating
                    if (checkTimetableSlot(originalSlot)) {

                        //Everything's fine, now make it persistent in the database
                        getChannelManager().updateTimetableSlot(originalSlot);
                        //Log it
                        log(TIMETABLE_LOG_OPERATION.edit, originalSlot, originallyBoundChannels);
                        //Notify responsible users of any changes
                        if (areAttributesUpdated) //Only users still linked to the playlist after update will receive this notification
                        {
                            notify(TIMETABLE_NOTIFICATION_OPERATION.edit, originalSlot);
                        }
                        //All linked entities are then new links
                        //Linked programme changed
                        if (!originallyLinkedProgramme.equals(originalSlot.getProgramme())) {
                            notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.add, originalSlot.getProgramme(), originalSlot);
                            notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedProgramme, originalSlot);
                        }
                        if (!originallyLinkedPlaylist.equals(originalSlot.getPlaylist())) {
                            notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, originalSlot.getPlaylist(), originalSlot);
                            notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedPlaylist, originalSlot);
                        }
                        if (!originallyLinkedChannel.equals(originalSlot.getChannel())) {
                            notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.add, originalSlot.getChannel(), originalSlot);
                            notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, originalSlot);
                        }
                    }
                }
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    /**
     * Shift timetableSlot by shift minutes (forward, or backward if negative)
     *
     * @param channelId
     * @param startTime
     * @param shift
     * @throws Exception
     */
    @Override
    public void updateShiftSecuredTimetableSlot(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface startTime, long shift) throws Exception {

        //No need to go any further if no timetable will be shifted in the end
        if (shift != 0) {

            //Get the original version of this slot
            //It will be this version that will be modified with data coming from the DTO
            TimetableSlot originalSlot = getChannelManager().getTimetableSlot(channelId, DateDTOAssembler.fromJS(startTime));

            //Check if it can be accessed
            if (getChannelManager().checkAccessUpdateTimetableSlot(originalSlot.getChannel()) && isSlotTouchable(originalSlot)) {

                //Do only change its start and end time accordingly
                YearMonthWeekDayHourMinuteSecondMillisecond newStart = originalSlot.getStartTime();
                newStart.addSeconds(shift);
                YearMonthWeekDayHourMinuteSecondMillisecond newEnd = originalSlot.getEndTime();
                newEnd.addSeconds(shift);
                originalSlot.setStartTimeWithCorrection(newStart);
                originalSlot.setEndTimeWithCorrection(newEnd);

                //Check and adjust its dependencies before updating
                if (checkTimetableSlot(originalSlot)) {

                    //Everything's fine, now make it persistent in the database
                    getChannelManager().updateTimetableSlot(originalSlot);
                    //Log it
                    log(TIMETABLE_LOG_OPERATION.shift, originalSlot);
                    //Notify responsible users of any changes
                    notify(TIMETABLE_NOTIFICATION_OPERATION.edit, originalSlot);
                }
            } else {
                throw new AccessDeniedException();
            }

        }

    }

    /**
     * Resize the timetableSlot by size seconds (shrink if size is negative, or
     * extend)
     *
     * @param channelId
     * @param startTime
     * @param size
     * @throws Exception
     */
    @Override
    public void updateResizeSecuredTimetableSlot(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface startTime, long size) throws Exception {

        //No need to go any further if no timetable will be resized in the end
        if (size != 0) {

            //Get the original version of this slot
            //It will be this version that will be modified with data coming from the DTO
            TimetableSlot originalSlot = getChannelManager().getTimetableSlot(channelId, DateDTOAssembler.fromJS(startTime));

            //Check if it can be accessed
            if (getChannelManager().checkAccessUpdateTimetableSlot(originalSlot.getChannel()) && isSlotTouchable(originalSlot)) {

                //Do only change the length of the slot, nothing more
                YearMonthWeekDayHourMinuteSecondMillisecond newEnd = originalSlot.getStartTime();
                newEnd.addSeconds(size);
                originalSlot.setEndTimeWithCorrection(newEnd);

                //Check and adjust its dependencies before updating
                if (checkTimetableSlot(originalSlot)) {

                    //Everything's fine, now make it persistent in the database
                    getChannelManager().updateTimetableSlot(originalSlot);
                    //Log it
                    log(TIMETABLE_LOG_OPERATION.resize, originalSlot);
                    //Notify responsible users of any changes
                    notify(TIMETABLE_NOTIFICATION_OPERATION.edit, originalSlot);
                }

            } else {
                throw new AccessDeniedException();
            }
        }

    }

    @Override
    public void updateDecommissioningDateSecuredTimetableSlot(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface startTime, biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface decommissioningDate) throws Exception {
        //Get the original version of this slot
        //It will be this version that will be modified with data coming from the DTO
        TimetableSlot originalSlot = getChannelManager().getTimetableSlot(channelId, DateDTOAssembler.fromJS(startTime));

        //Only recurring slots have a decommissioning date
        if (!ProxiedClassUtil.castableAs(originalSlot, RecurringTimetableSlot.class)) {
            throw new NoEntityException();
        }

        RecurringTimetableSlot recurringSlot = (RecurringTimetableSlot) originalSlot;

        //Check if it can be accessed
        if (getChannelManager().checkAccessUpdateTimetableSlot(recurringSlot.getChannel()) && isSlotTouchable(originalSlot)) {

            boolean isContentChanged = false;
            //Do only change the decommisioning date of the slot, nothing more
            if (decommissioningDate == null) {
                if (recurringSlot.getDecommissioningTime() != null) {
                    recurringSlot.unsetDecommissioningTime();
                    isContentChanged = true;
                }
            } else {
                biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface newDecommissioningDate = DateDTOAssembler.fromJS(decommissioningDate);
                if (!newDecommissioningDate.equals(recurringSlot.getDecommissioningTime())) {
                    recurringSlot.setDecommissioningTimeWithCorrection(newDecommissioningDate);
                    isContentChanged = true;
                }
            }

            //Check and adjust its dependencies before updating
            if (checkTimetableSlot(recurringSlot)) {
                //Everything's fine, now make it persistent in the database
                getChannelManager().updateTimetableSlot(recurringSlot);
                //Log it
                log(TIMETABLE_LOG_OPERATION.edit, recurringSlot);
                //Notify responsible users
                if (isContentChanged) {
                    notify(TIMETABLE_NOTIFICATION_OPERATION.edit, recurringSlot);
                }
            }

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void updateEnableSecuredTimetableSlot(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface startTime, boolean enabled) throws Exception {
        //Get the original version of this slot
        //It will be this version that will be modified with data coming from the DTO
        TimetableSlot originalSlot = getChannelManager().getTimetableSlot(channelId, DateDTOAssembler.fromJS(startTime));

        //Check if it can be accessed
        if (getChannelManager().checkAccessUpdateTimetableSlot(originalSlot.getChannel()) || getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(originalSlot.getProgramme())) {

            boolean isContentChanged = false;
            //Do only change the activation state the slot, nothing more
            //If there's no playlist then it can't be activated anyway
            if (originalSlot.isEnabled() != enabled) {
                originalSlot.setEnabled(enabled);
                isContentChanged = true;
            }

            //Check and adjust its dependencies before updating
            if (checkTimetableSlot(originalSlot)) {
                //Everything's fine, now make it persistent in the database
                getChannelManager().updateTimetableSlot(originalSlot);
                //Log it
                log(
                        originalSlot.isEnabled()
                        ? TIMETABLE_LOG_OPERATION.confirm
                        : TIMETABLE_LOG_OPERATION.cancel,
                        originalSlot);
                //Notify responsible users
                if (isContentChanged) {
                    notify(TIMETABLE_NOTIFICATION_OPERATION.edit, originalSlot);
                }
            }

        } else {
            throw new AccessDeniedException();
        }

    }

    @Override
    public void updatePlaylistSecuredTimetableSlot(String channelId, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface startTime, String playlistId) throws Exception {
        //Get the original version of this slot
        //It will be this version that will be modified with data coming from the DTO
        TimetableSlot originalSlot = getChannelManager().getTimetableSlot(channelId, DateDTOAssembler.fromJS(startTime));

        //Check if it can be accessed
        if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(originalSlot.getProgramme()) && isSlotTouchable(originalSlot)) {

            //Do only switch the playlist, nothing more
            //Don't check if the playlist programme is the same as the one from the timetable slot, it's the responsibility of checkTimetableSlot()
            //Don't bother to check if the user has access to the playlist because if it shares the same programme as the timetable slot then the check has already been performed
            //Assume that if the original playlist is null the user wantedto remove it from the timetable slot
            Playlist originallyLinkedPlaylist = originalSlot.getPlaylist();
            if (playlistId == null) {
                originalSlot.removePlaylist();
            } else {
                originalSlot.addPlaylist(
                        getProgrammeManager().loadPlaylist(playlistId));
            }

            //Check and adjust its dependencies before updating
            if (checkTimetableSlot(originalSlot)) {
                //Everything's fine, now make it persistent in the database
                getChannelManager().updateTimetableSlot(originalSlot);
                //Log it
                log(TIMETABLE_LOG_OPERATION.edit, originalSlot);
                //Notify responsible users
                if (originallyLinkedPlaylist == null) {
                    notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, originalSlot.getPlaylist(), originalSlot);
                } else if (!originallyLinkedPlaylist.equals(originalSlot.getPlaylist())) {
                    notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, originalSlot.getPlaylist(), originalSlot);
                    notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedPlaylist, originalSlot);
                }
            }

        } else {
            throw new AccessDeniedException();
        }

    }

    private void processTimetableSlotForDeletion(TimetableSlot timetableSlot) throws Exception {

        if (timetableSlot != null) {

            timetableSlot.removeStreamItem();
            timetableSlot.clearQueueItems();

        }

    }

    @Override
    public void deleteSecuredTimetableSlot(TimetableSlotFormID id) throws Exception {
        if (id != null) {

            TimetableSlot timetableSlot = getChannelManager().getTimetableSlot(id.getChannelId(), DateDTOAssembler.fromJS(id.getSchedulingTime()));

            //Then check if it can be accessed
            if (getChannelManager().checkAccessDeleteTimetableSlot(timetableSlot.getChannel()) && isSlotTouchable(timetableSlot)) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = TIMETABLE_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), timetableSlot);
                //Save original links before modification (clones)
                Programme originallyLinkedProgramme = timetableSlot.getProgramme();
                Playlist originallyLinkedPlaylist = timetableSlot.getPlaylist();
                Channel originallyLinkedChannel = timetableSlot.getChannel();
                //Whatever happens, someone who can remove a timetable slot can unlink other stuff bound to it: no need for extra check
                //Cascade delete will take care of their unlinking
                processTimetableSlotForDeletion(timetableSlot);

                //Everything's fine, now make it persistent in the database
                getChannelManager().deleteTimetableSlot(timetableSlot);
                //Log it
                log(TIMETABLE_LOG_OPERATION.delete, timetableSlot, originallyBoundChannels);
                //Notify responsible users
                notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedProgramme, timetableSlot);
                notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedPlaylist, timetableSlot);
                notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, timetableSlot);
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredTimetableSlots(Collection<TimetableSlotFormID> ids) throws Exception {
        if (ids != null) {

            Collection<TimetableSlot> slotsToDelete = new HashSet<TimetableSlot>();

            for (TimetableSlotFormID id : ids) {

                TimetableSlot timetableSlot = getChannelManager().getTimetableSlot(id.getChannelId(), DateDTOAssembler.fromJS(id.getSchedulingTime()));

                //Then check if it can be accessed
                if (getChannelManager().checkAccessDeleteTimetableSlot(timetableSlot.getChannel()) && isSlotTouchable(timetableSlot)) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = TIMETABLE_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), timetableSlot);
                    //Save original links before modification (clones)
                    Programme originallyLinkedProgramme = timetableSlot.getProgramme();
                    Playlist originallyLinkedPlaylist = timetableSlot.getPlaylist();
                    Channel originallyLinkedChannel = timetableSlot.getChannel();
                    //Whatever happens, someone who can remove a timetable slot can unlink other stuff bound to it: no need for extra check
                    //Cascade delete will take care of their unlinking
                    processTimetableSlotForDeletion(timetableSlot);

                    slotsToDelete.add(timetableSlot);
                    //Log it
                    log(TIMETABLE_LOG_OPERATION.delete, timetableSlot, originallyBoundChannels);
                    //Notify responsible users
                    notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedProgramme, timetableSlot);
                    notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedPlaylist, timetableSlot);
                    notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, timetableSlot);

                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!slotsToDelete.isEmpty()) {
                getChannelManager().deleteTimetableSlots(slotsToDelete);
            }

        }
    }

    protected boolean isSlotTouchable(TimetableSlot slot) throws Exception {

        if (slot == null) {
            throw new NoEntityException();
        }

        //If it's no playing then don't remove it
        if (slot.getStreamItem() != null) {
            throw new TimetableSlotNowPlayingException();
        }

        return true;
    }

    protected void log(TIMETABLE_LOG_OPERATION operation, Collection<TimetableSlot> timetableSlots) throws Exception {
        if (timetableSlots != null && !timetableSlots.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<TimetableLog> logs = new HashSet<TimetableLog>();
            for (TimetableSlot timetableSlot : timetableSlots) {
                logs.add(
                        new TimetableLog(
                        operation,
                        timetableSlot.getRefID(),
                        timetableSlot.getStartTime(),
                        timetableSlot.getChannel() != null ? timetableSlot.getChannel().getLabel() : null,
                        operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), timetableSlot)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(TIMETABLE_LOG_OPERATION operation, TimetableSlot timetableSlot) throws Exception {
        log(operation, timetableSlot, null);
    }

    protected void log(TIMETABLE_LOG_OPERATION operation, TimetableSlot timetableSlot, Collection<Channel> supplementaryChannels) throws Exception {
        if (timetableSlot != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), timetableSlot);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(
                    new TimetableLog(
                    operation,
                    timetableSlot.getRefID(),
                    timetableSlot.getStartTime(),
                    timetableSlot.getChannel() != null ? timetableSlot.getChannel().getLabel() : null,
                    allBoundChannels));
        }
    }

    protected void notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION operation, Channel channel, TimetableSlot timetableSlot) throws Exception {
        notify(operation, channel, timetableSlot, operation.getRecipients(getSecurityManager(), channel, timetableSlot));
    }

    protected void notify(CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION operation, Channel channel, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && timetableSlot != null && recipients != null) {
            Collection<ChannelTimetableSlotLinkNotification> notifications = new HashSet<ChannelTimetableSlotLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelTimetableSlotLinkNotification(operation, channel, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION operation, Playlist playlist, TimetableSlot timetableSlot) throws Exception {
        notify(operation, playlist, timetableSlot, operation.getRecipients(getSecurityManager(), playlist, timetableSlot));
    }

    protected void notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION operation, Playlist playlist, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (playlist != null && timetableSlot != null && recipients != null) {
            Collection<PlaylistTimetableSlotLinkNotification> notifications = new HashSet<PlaylistTimetableSlotLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new PlaylistTimetableSlotLinkNotification(operation, playlist, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION operation, Programme programme, TimetableSlot timetableSlot) throws Exception {
        notify(operation, programme, timetableSlot, operation.getRecipients(getSecurityManager(), programme, timetableSlot));
    }

    protected void notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION operation, Programme programme, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && timetableSlot != null && recipients != null) {
            Collection<ProgrammeTimetableSlotLinkNotification> notifications = new HashSet<ProgrammeTimetableSlotLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeTimetableSlotLinkNotification(operation, programme, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(TIMETABLE_NOTIFICATION_OPERATION operation, TimetableSlot timetableSlot) throws Exception {
        notify(operation, timetableSlot, operation.getRecipients(getSecurityManager(), timetableSlot));
    }

    protected void notify(TIMETABLE_NOTIFICATION_OPERATION operation, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (timetableSlot != null && recipients != null) {
            Collection<TimetableContentNotification> notifications = new HashSet<TimetableContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new TimetableContentNotification(operation, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
