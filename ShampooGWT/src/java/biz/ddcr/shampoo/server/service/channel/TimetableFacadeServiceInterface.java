/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotForm.TIMETABLE_TAGS;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import java.util.Collection;



/**
 *
 * @author okay_awright
 **/
public interface TimetableFacadeServiceInterface {

    
    public void addSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception;
    //Specializations of inserts for timetable slots
    
    public void addSecuredTimetableSlotClones(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface originalStartTime, Collection<YearMonthDayHourMinuteSecondMillisecondInterface> newDates) throws Exception;

    
    public void updateSecuredTimetableSlot(TimetableSlotForm timetableSlotForm) throws Exception;
    //subsets of updates
    
    public void updateShiftSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long shift) throws Exception;
    
    public void updateResizeSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, long size) throws Exception;
    
    public void updateDecommissioningDateSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, YearMonthDayInterface decommissioningDate) throws Exception;
    
    public void updateEnableSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, boolean enabled) throws Exception;

    public void updatePlaylistSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime, String playlistId) throws Exception;
    
    public void deleteSecuredTimetableSlot(TimetableSlotFormID id) throws Exception;
    
    public void deleteSecuredTimetableSlots(Collection<TimetableSlotFormID> ids) throws Exception;

    public ActionCollectionEntry<TimetableSlotForm> getSecuredTimetableSlot(String channelId, YearMonthDayHourMinuteSecondMillisecondInterface startTime) throws Exception;
    
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelId(GroupAndSort<TIMETABLE_TAGS> constraint, String channelId, String localTimeZone) throws Exception;
    
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForChannelIdAndWeek(
            String channelId,
            YearMonthDayHourMinuteInterface from) throws Exception;

    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForProgrammeId(GroupAndSort<TIMETABLE_TAGS> constraint, String programmeId, String localTimeZone) throws Exception;
    
    public ActionCollection<TimetableSlotForm> getSecuredTimetableSlotsForPlaylistId(GroupAndSort<TIMETABLE_TAGS> constraint, String playlistId, String localTimeZone) throws Exception;

}
