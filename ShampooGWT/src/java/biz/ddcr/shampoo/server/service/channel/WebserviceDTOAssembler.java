/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
  *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class WebserviceDTOAssembler extends GenericService implements WebserviceDTOAssemblerInterface {

    private ChannelManagerInterface channelManager;

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    @Override
    public Webservice fromDTO(WebserviceForm wsForm) throws Exception {
        if (wsForm != null) {
            Webservice newWebservice = new Webservice();

            //Fill in all attributes
            newWebservice.setApiKey(wsForm.getApiKey());
            newWebservice.setPrivateKey(wsForm.getPrivateKey());

            //Channel
            newWebservice.addChannel(getChannelManager().getChannel(wsForm.getChannel()));

            //Modules
            newWebservice.setArchiveModule(wsForm.isArchiveModule());
            newWebservice.setTimetableModule(wsForm.isTimetableModule());
            newWebservice.setVoteModule(wsForm.isVoteModule());
            newWebservice.setRequestModule(wsForm.isRequestModule());
            newWebservice.setNowPlayingModule(wsForm.isNowPlayingModule());
            newWebservice.setComingNextModule(wsForm.isComingNextModule());

            //Options
            newWebservice.setMaxDailyLimit(wsForm.getMaxDailyQuota());
            newWebservice.setMaxFireRate(wsForm.getMaxFireRate());
            newWebservice.setWhitelistRegexp(wsForm.getWhitelistRegexp());

            return newWebservice;
        }
        return null;
    }

    /**
     * update an existing webservice with data from a DTO.
     * Returns true if attributes of the original webservice have been changed.
     * @param webservice
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(Webservice webservice, WebserviceForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && webservice != null) {

            if ((form.getPrivateKey()!=null && !form.getPrivateKey().equals(webservice.getPrivateKey())
                    || (form.getPrivateKey()==null && webservice.getPrivateKey()!=null))) {
                webservice.setPrivateKey(form.getPrivateKey());
                contentUpdated = true;
            }
            if ((form.getMaxDailyQuota()!=null && !form.getMaxDailyQuota().equals(webservice.getMaxDailyLimit())
                    || (form.getMaxDailyQuota()==null && webservice.getMaxDailyLimit()!=null))) {
                webservice.setMaxDailyLimit(form.getMaxDailyQuota());
                contentUpdated = true;
            }
            if ((form.getMaxFireRate()!=null && !form.getMaxFireRate().equals(webservice.getMaxFireRate())
                    || (form.getMaxFireRate()==null && webservice.getMaxFireRate()!=null))) {
                webservice.setMaxFireRate(form.getMaxFireRate());
                contentUpdated = true;
            }
            if ((form.getWhitelistRegexp()!=null && !form.getWhitelistRegexp().equals(webservice.getWhitelistRegexp())
                    || (form.getWhitelistRegexp()==null && webservice.getWhitelistRegexp()!=null))) {
                webservice.setWhitelistRegexp(form.getWhitelistRegexp());
                contentUpdated = true;
            }
            if (form.isArchiveModule()!=webservice.isArchiveModule()) {
                webservice.setArchiveModule(form.isArchiveModule());
                contentUpdated = true;
            }
            if (form.isTimetableModule()!=webservice.isTimetableModule()) {
                webservice.setTimetableModule(form.isTimetableModule());
                contentUpdated = true;
            }
            if (form.isVoteModule()!=webservice.isVoteModule()) {
                webservice.setVoteModule(form.isVoteModule());
                contentUpdated = true;
            }
            if (form.isRequestModule()!=webservice.isRequestModule()) {
                webservice.setRequestModule(form.isRequestModule());
                contentUpdated = true;
            }
            if (form.isNowPlayingModule()!=webservice.isNowPlayingModule()) {
                webservice.setNowPlayingModule(form.isNowPlayingModule());
                contentUpdated = true;
            }
            if (form.isComingNextModule()!=webservice.isComingNextModule()) {
                webservice.setComingNextModule(form.isComingNextModule());
                contentUpdated = true;
            }

            //Leave the previous one if we cannot access it
            //or an exception if it can be properly updated
            if (getChannelManager().checkAccessUpdateChannel(webservice.getChannel())) {
                webservice.removeChannel();
                Channel c = getChannelManager().getChannel(form.getChannel());
                if (getChannelManager().checkAccessUpdateChannel(c)) {
                    webservice.addChannel(c);
                } else {
                    throw new AccessDeniedException();
                }
            }

        }
        return contentUpdated;
    }

    @Override
    public ActionCollectionEntry<WebserviceForm> toDTO(Webservice webservice) throws Exception {
        if (webservice == null) {
            return null;
        }

        WebserviceForm wsForm = new WebserviceForm();

            //Fill in common properties
            wsForm.setCreatorId(webservice.getCreator() != null ? webservice.getCreator().getUsername() : null);
            wsForm.setLatestEditorId(webservice.getLatestEditor() != null ? webservice.getLatestEditor().getUsername() : null);
            wsForm.setCreationDate(webservice.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(webservice.getCreationDate().getTime(), null)) : null);
            wsForm.setLatestModificationDate(webservice.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(webservice.getLatestModificationDate().getTime(), null)) : null);

            wsForm.setApiKey(webservice.getApiKey());
            wsForm.setPrivateKey(webservice.getPrivateKey());

            //Channel
            wsForm.setChannel(webservice.getChannel()!=null?webservice.getChannel().getLabel():null);

            //Modules
            wsForm.setArchiveModule(webservice.isArchiveModule());
            wsForm.setTimetableModule(webservice.isTimetableModule());
            wsForm.setVoteModule(webservice.isVoteModule());
            wsForm.setRequestModule(webservice.isRequestModule());
            wsForm.setNowPlayingModule(webservice.isNowPlayingModule());
            wsForm.setComingNextModule(webservice.isComingNextModule());

            //Options
            wsForm.setMaxDailyQuota(webservice.getMaxDailyLimit());
            wsForm.setMaxFireRate(webservice.getMaxFireRate());
            wsForm.setWhitelistRegexp((webservice.getWhitelistRegexp()));

        return new ActionMapEntry<WebserviceForm>(wsForm, decorateEntryWithActions(webservice));
    }

    @Override
    public ActionCollection<WebserviceForm> toDTO(Collection<Webservice> webservices, boolean doSort) throws Exception {
        ActionMap<WebserviceForm> wsForms = doSort ? new ActionSortedMap<WebserviceForm>() : new ActionUnsortedMap<WebserviceForm>();
        for (Webservice webservice : webservices) {
            wsForms.set(toDTO(webservice));
        }
        return wsForms;
    }

    public ActionCollectionEntry<WebserviceModule> toMetadataDTO(Webservice webservice, Long time1, Long time2) throws Exception {
        if (webservice == null || time1 == null && time2 == null) {
            return null;
        }
        return new ActionMapEntry<WebserviceModule>(new WebserviceModule(time1, time2), decorateEntryWithActions(webservice));
    }    
    
    protected Collection<ACTION> decorateEntryWithActions(Webservice webservice) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewWebservice(webservice.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateWebservice(webservice.getChannel())) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteWebservice(webservice.getChannel())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

}
