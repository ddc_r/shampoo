/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelWebserviceLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelWebserviceLinkNotification.CHANNELWEBSERVICE_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.webservice.log.WebserviceLog;
import biz.ddcr.shampoo.server.domain.webservice.log.WebserviceLog.WEBSERVICE_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.webservice.notification.WebserviceContentNotification;
import biz.ddcr.shampoo.server.domain.webservice.notification.WebserviceContentNotification.WEBSERVICE_NOTIFICATION_OPERATION;
import java.util.Collection;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.HashSet;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 *
 */
public class WebserviceFacadeService extends GenericService implements WebserviceFacadeServiceInterface {

    private WebserviceDTOAssemblerInterface webserviceDTOAssembler;
    private ChannelManagerInterface channelManager;
    private SecurityManagerInterface securityManager;

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public WebserviceDTOAssemblerInterface getWebserviceDTOAssembler() {
        return webserviceDTOAssembler;
    }

    public void setWebserviceDTOAssembler(WebserviceDTOAssemblerInterface webserviceDTOAssembler) {
        this.webserviceDTOAssembler = webserviceDTOAssembler;
    }

    /**
     *
     * Validation process for webservices Incorrect values are replaced with
     * default ones unless the problem is not recoverable, then an Exception is
     * thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     *
     */
    private boolean checkWebservice(Webservice ws) throws Exception {
        boolean result = false;

        if (ws != null) {

            //Quotas cannot go lower than 1
            if (ws.getMaxDailyLimit() != null && ws.getMaxDailyLimit() < 1) {
                ws.setMaxDailyLimit(null);
            }
            if (ws.getMaxFireRate() != null && ws.getMaxFireRate() < 1) {
                ws.setMaxFireRate(null);
            }

            result = true;
        }

        return result;
    }

    /**
     * Add a webservice and link it to other dependent objects
     *
     * @param timetableSlotForm
     * @throws Exception
     *
     */
    @Override
    public void addSecuredWebservice(WebserviceForm wsForm) throws Exception {

        if (wsForm != null) {

            //hydrate the entity from the DTO
            Webservice ws = webserviceDTOAssembler.fromDTO(wsForm);
            //Then check if it can be accessed
            if (getChannelManager().checkAccessAddWebservice(ws.getChannel())) {

                //Check and adjust its dependencies before insertion
                if (checkWebservice(ws)) {
                    //Everything's fine, now make it persistent in the database
                    try {
                        getChannelManager().addWebservice(ws);
                        //Log it
                        log(WEBSERVICE_LOG_OPERATION.add, ws);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        if (ws.getChannel() != null) {
                            notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.add, ws.getChannel(), ws);
                        }

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    public ActionCollectionEntry<WebserviceForm> getSecuredWebservice(String apiKey) throws Exception {

        Webservice ws = getChannelManager().loadWebservice(apiKey);
        ActionCollectionEntry<WebserviceForm> wsFormEntry = webserviceDTOAssembler.toDTO(ws);
        if (wsFormEntry == null || wsFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(TIMETABLE_LOG_OPERATION.read, tts);
            return wsFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<WebserviceModule> getSecuredDynamicPublicWebserviceMetadata(String apiKey) throws Exception {
        Webservice ws = getChannelManager().getWebservice(apiKey);
        return webserviceDTOAssembler.toMetadataDTO(ws, getChannelManager().getNumberOfHitsIn5Minutes(ws), getChannelManager().getDailyNumberOfHits(ws));
    }
    
    @Override
    public ActionCollection<WebserviceForm> getSecuredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint) throws Exception {

        Collection<Webservice> wses = getChannelManager().getFilteredWebservices(constraint);
        ActionCollection<WebserviceForm> forms = webserviceDTOAssembler.toDTO(wses, constraint == null || constraint.getSortByFeature() == null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(TIMETABLE_LOG_OPERATION.read, timetableSlots);
        return forms;
    }

    @Override
    public void updateSecuredWebservice(WebserviceForm wsForm) throws Exception {
        if (wsForm != null) {

            //Get the original version of this slot
            //It will be this version that will be modified with data coming from the DTO
            Webservice originalWS = getChannelManager().loadWebservice(wsForm.getApiKey());
            //Check if it can be accessed

            if (getChannelManager().checkAccessUpdateWebservice(wsForm.getChannel())) {
                if (originalWS != null) {
                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = WEBSERVICE_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalWS);
                    Channel originallyLinkedChannel = originalWS.getChannel();

                    //Update the existing entity from the DTO
                    boolean areAttributesUpdated = webserviceDTOAssembler.augmentWithDTO(originalWS, wsForm);

                    //Check and adjust its dependencies before updating
                    if (checkWebservice(originalWS)) {
                        
                        //Everything's fine, now make it persistent in the database
                        getChannelManager().updateWebservice(originalWS);
                        //Log it
                        log(WEBSERVICE_LOG_OPERATION.edit, originalWS, originallyBoundChannels);
                        //Notify responsible users of any changes
                        if (areAttributesUpdated) {
                            notify(WEBSERVICE_NOTIFICATION_OPERATION.edit, originalWS);
                        }
                        //All linked entities are then new links
                        if (!originallyLinkedChannel.equals(originalWS.getChannel())) {
                            notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.add, originalWS.getChannel(), originalWS);
                            notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, originalWS);
                        }
                    }
                }
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    private void processWebserviceForDeletion(Webservice ws) throws Exception {

        if (ws != null) {

            ws.removeChannel();
            ws.clearHits();

        }

    }

    @Override
    public void deleteSecuredWebservice(String id) throws Exception {
        if (id != null) {

            Webservice ws = getChannelManager().loadWebservice(id);

            //Then check if it can be accessed
            if (getChannelManager().checkAccessDeleteWebservice(ws.getChannel())) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = WEBSERVICE_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), ws);
                Channel originallyLinkedChannel = ws.getChannel();
                //Whatever happens, someone who can remove a timetable slot can unlink other stuff bound to it: no need for extra check
                //Cascade delete will take care of their unlinking
                processWebserviceForDeletion(ws);

                //Everything's fine, now make it persistent in the database
                getChannelManager().deleteWebservice(ws);
                //Log it
                log(WEBSERVICE_LOG_OPERATION.delete, ws, originallyBoundChannels);
                //Notify responsible users
                notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, ws);
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredWebservices(Collection<String> ids) throws Exception {
        if (ids != null) {

            Collection<Webservice> wsesToDelete = new HashSet<Webservice>();

            for (String id : ids) {

                Webservice ws = getChannelManager().loadWebservice(id);

                //Then check if it can be accessed
                if (getChannelManager().checkAccessDeleteWebservice(ws.getChannel())) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = WEBSERVICE_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), ws);
                    //Save original links before modification (clones)
                    Channel originallyLinkedChannel = ws.getChannel();
                    //Whatever happens, someone who can remove a timetable slot can unlink other stuff bound to it: no need for extra check
                    //Cascade delete will take care of their unlinking
                    processWebserviceForDeletion(ws);

                    wsesToDelete.add(ws);
                    //Log it
                    log(WEBSERVICE_LOG_OPERATION.delete, ws, originallyBoundChannels);
                    //Notify responsible users
                    notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete, originallyLinkedChannel, ws);

                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!wsesToDelete.isEmpty()) {
                getChannelManager().deleteWebservices(wsesToDelete);
            }

        }
    }

    protected void log(WEBSERVICE_LOG_OPERATION operation, Collection<Webservice> wses) throws Exception {
        if (wses != null && !wses.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<WebserviceLog> logs = new HashSet<WebserviceLog>();
            for (Webservice ws : wses) {
                logs.add(
                        new WebserviceLog(
                        operation,
                        ws.getApiKey(),
                        ws.getChannel() != null ? ws.getChannel().getLabel() : null,
                        operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), ws)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(WEBSERVICE_LOG_OPERATION operation, Webservice ws) throws Exception {
        log(operation, ws, null);
    }

    protected void log(WEBSERVICE_LOG_OPERATION operation, Webservice ws, Collection<Channel> supplementaryChannels) throws Exception {
        if (ws != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), ws);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(
                    new WebserviceLog(
                    operation,
                    ws.getApiKey(),
                    ws.getChannel() != null ? ws.getChannel().getLabel() : null,
                    allBoundChannels));
        }
    }

    protected void notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION operation, Channel channel, Webservice ws) throws Exception {
        notify(operation, channel, ws, operation.getRecipients(getSecurityManager(), channel, ws));
    }

    protected void notify(CHANNELWEBSERVICE_NOTIFICATION_OPERATION operation, Channel channel, Webservice ws, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && ws != null && recipients != null) {
            Collection<ChannelWebserviceLinkNotification> notifications = new HashSet<ChannelWebserviceLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelWebserviceLinkNotification(operation, channel, ws, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(WEBSERVICE_NOTIFICATION_OPERATION operation, Webservice ws) throws Exception {
        notify(operation, ws, operation.getRecipients(getSecurityManager(), ws));
    }

    protected void notify(WEBSERVICE_NOTIFICATION_OPERATION operation, Webservice ws, Collection<RestrictedUser> recipients) throws Exception {
        if (ws != null && recipients != null) {
            Collection<WebserviceContentNotification> notifications = new HashSet<WebserviceContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new WebserviceContentNotification(operation, ws, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
