/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.channel;

import biz.ddcr.shampoo.client.form.webservice.WebserviceForm;
import biz.ddcr.shampoo.client.form.webservice.WebserviceForm.WEBSERVICE_TAGS;
import biz.ddcr.shampoo.client.form.webservice.WebserviceModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import java.util.Collection;



/**
 *
 * @author okay_awright
 **/
public interface WebserviceFacadeServiceInterface {

    public void addSecuredWebservice(WebserviceForm webserviceForm) throws Exception;

    public void updateSecuredWebservice(WebserviceForm webserviceForm) throws Exception;

    public void deleteSecuredWebservice(String id) throws Exception;
    public void deleteSecuredWebservices(Collection<String> ids) throws Exception;

    public ActionCollectionEntry<WebserviceForm> getSecuredWebservice(String id) throws Exception;

    public ActionCollectionEntry<WebserviceModule> getSecuredDynamicPublicWebserviceMetadata(String id) throws Exception;
    
    public ActionCollection<WebserviceForm> getSecuredWebservices(GroupAndSort<WEBSERVICE_TAGS> constraint) throws Exception;

}
