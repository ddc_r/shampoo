/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.AbstractPilePool.PileEventInterface;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 *
 * A (thread-safe) singleton bean -check the Spring context- that acts as a router for piles of signals
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public abstract class AbstractPilePool<U extends PileEventInterface> extends GenericService {

    /** Callback methods that act as generic runnables, embedded within StreamerEvents and called by the current thread **/
    public interface RunnableGenericEvent {
        public Object synchronous() throws Exception;
    }
    
    public interface PileEventInterface<V extends RunnableGenericEvent> extends Comparable<PileEventInterface<V>> {
        /** Identifier of the event **/
        public UUID getUUID();
        /** when was it created? **/
        public long getPublished();
        /** Runnable associated **/
        public V getRunnable();
        @Override
        public int compareTo(PileEventInterface other);
    }
    
    /** Life span of an event in the queue (in milliseconds) before being garbage-collectable; 0 or less means unlimited */
    protected abstract long getEventLifeSpan();    
    
    protected abstract HashMap<Long, PriorityBlockingQueue<U>> getPiles();
    
    /** Retrieve a MessageChannel dedicated to a specific streamer **/
    public BlockingQueue<U> getPile(long id) {
        PriorityBlockingQueue<U> pile = getPiles().get(id);
        if (pile==null) {
            pile = new PriorityBlockingQueue<U>();
            getPiles().put(id, pile);
        }
        return pile;
    }

    /** Manually unregister (and free the resources associated with) a pile **/
    public void clearPile(long id) {
        PriorityBlockingQueue<U> pile = getPiles().get(id);
        if (pile!=null) {
            pile.clear();
            getPiles().remove(id);
        }
    }

    /**
     * Make sure no stalled event gets stuck on the top.
     * Meant to be called via StreamerEventCleanupRunnable
     * @throws Exception
     */
    public void purgeStalledEvents() throws Exception {

        if (getEventLifeSpan()>0) {
            long expiry = DateHelper.getCurrentTime() - getEventLifeSpan();
            for ( Iterator<Entry<Long,PriorityBlockingQueue<U>>> iter = getPiles().entrySet().iterator(); iter.hasNext(); ) {
                PriorityBlockingQueue<U> pile = iter.next().getValue();
                U topEvent = pile.peek();
                if (topEvent!=null && topEvent.getPublished()<expiry) pile.remove();
            }
        }

    }

    /**
     * In order to free up some memory, check if piles contain no more events
     * And in this case, deallocate them from the map
     * Meant to be called via StreamerEventCleanupRunnable
     * @throws Exception
     */
    public void pruneEmptyPiles() throws Exception {

        for ( Iterator<Entry<Long,PriorityBlockingQueue<U>>> iter = getPiles().entrySet().iterator(); iter.hasNext(); ) {
            PriorityBlockingQueue<U> pile = iter.next().getValue();
            if (pile.isEmpty()) iter.remove();
        }

    }
       
}
