/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.GenericService;
import java.util.HashMap;
import java.util.Map;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * Read HTML from templates for displaying in the GUI
 *
 * @author okay_awright
 **/
public class BrandingService extends GenericService {

    private String headerTemplateFile;
    private String frontpageTemplateFile;
    private transient VelocityEngine templatingEngine;
    //System configuration
    private SystemConfigurationHelper systemConfigurationHelper;

    public void setHeaderTemplateFile(String headerTemplateFile) {
        this.headerTemplateFile = headerTemplateFile;
    }

    public void setFrontpageTemplateFile(String frontpageTemplateFile) {
        this.frontpageTemplateFile = frontpageTemplateFile;
    }

    public void setTemplateProvider(VelocityEngine templatingEngine) {
        this.templatingEngine = templatingEngine;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public String getHTMLHeader() {
        Map templateVariables = new HashMap();
        templateVariables.put("url", getSystemConfigurationHelper().getHostAndDeploymentGWTURL()+getSystemConfigurationHelper().getGWTContextPath());
        templateVariables.put("staticAppName", VersioningHelper.getStaticAppName());
        
        return VelocityEngineUtils.mergeTemplateIntoString(
                    templatingEngine,
                    headerTemplateFile,
                    templateVariables);
    }

    public String getHTMLFrontpage() {
        Map templateVariables = new HashMap();
        templateVariables.put("url", getSystemConfigurationHelper().getHostAndDeploymentGWTURL()+getSystemConfigurationHelper().getGWTContextPath());
        templateVariables.put("staticAppName", VersioningHelper.getStaticAppName());

        return VelocityEngineUtils.mergeTemplateIntoString(
                    templatingEngine,
                    frontpageTemplateFile,
                    templateVariables);
    }
    
}
