/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.errors.DownloadingNotFoundException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.archive.format.TextFileInfo;
import biz.ddcr.shampoo.server.domain.archive.format.TextFormat.TEXT_FORMAT;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.FileInfoInterface;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.helper.DatastoreCachingHelper;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface;
import biz.ddcr.shampoo.server.io.datastore.DataStoreInterface.TransferCallbackInterface;
import biz.ddcr.shampoo.server.io.helper.SessionPoolHandler;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.util.TextStream;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface.ReportFiles;
import biz.ddcr.shampoo.server.service.programme.PlaylistDTOAssemblerInterface;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface.PlaylistFiles;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import biz.ddcr.shampoo.server.service.track.TrackDTOAssemblerInterface;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface.TrackFiles;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DatastoreService {

    //System configuration bean
    private SystemConfigurationHelper systemConfigurationHelper;
    //Session pool handler
    private SessionPoolHandler sessionPoolHandler;
    //datastore
    private DataStoreInterface dataStore;
    //cache
    private DatastoreCachingHelper cache;
    private TrackManagerInterface trackManager;
    private SecurityManagerInterface securityManager;
    private ProgrammeManagerInterface programmeManager;
    private ChannelManagerInterface channelManager;
    private TrackDTOAssemblerInterface trackDTOAssembler;
    private PlaylistDTOAssemblerInterface playlistDTOAssembler;
    private WebserviceAuthenticationService webserviceAuthenticationService;
    
    public DatastoreCachingHelper getCache() {
        return cache;
    }

    public void setCache(DatastoreCachingHelper cache) {
        this.cache = cache;
    }

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public PlaylistDTOAssemblerInterface getPlaylistDTOAssembler() {
        return playlistDTOAssembler;
    }

    public void setPlaylistDTOAssembler(PlaylistDTOAssemblerInterface playlistDTOAssembler) {
        this.playlistDTOAssembler = playlistDTOAssembler;
    }

    public TrackDTOAssemblerInterface getTrackDTOAssembler() {
        return trackDTOAssembler;
    }

    public void setTrackDTOAssembler(TrackDTOAssemblerInterface trackDTOAssembler) {
        this.trackDTOAssembler = trackDTOAssembler;
    }

    public DataStoreInterface getDataStore() {
        return dataStore;
    }

    public void setDataStore(DataStoreInterface dataStore) {
        this.dataStore = dataStore;
    }

    public SessionPoolHandler getSessionPoolHandler() {
        return sessionPoolHandler;
    }

    public void setSessionPoolHandler(SessionPoolHandler sessionPoolHandler) {
        this.sessionPoolHandler = sessionPoolHandler;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public WebserviceAuthenticationService getWebserviceAuthenticationService() {
        return webserviceAuthenticationService;
    }

    public void setWebserviceAuthenticationService(WebserviceAuthenticationService webserviceAuthenticationService) {
        this.webserviceAuthenticationService = webserviceAuthenticationService;
    }

    public boolean setStreamToSessionPool(String uploadId, TypedStreamInterface file) throws Exception {
        //Reset cache for this object
        getCache().removeObjectInCache(file.getClass().getSimpleName()+"!"+uploadId);
        //Quirk for tracks: some might have contained the associated picture but there is no way to tell, just to be on the safe side: drop the corresponding picture as well
        if (file instanceof TrackStream)
            getCache().removeObjectInCache(PictureStream.class.getSimpleName()+"!"+uploadId);
        return getSessionPoolHandler().addSessionPoolUploadFile(uploadId, file);
    }

    public TrackStream getTrackStreamFromSessionPool(final String trackUploadId) throws Exception {

        return getCache().new CacheInspector<TrackStream>(TrackStream.class.getSimpleName()+"!"+trackUploadId) {
            @Override
            public TrackStream getMissed() throws Exception {
                //Track
                if (trackUploadId != null) {
                    TypedStreamInterface trackFile = getSessionPoolHandler().getSessionPoolUploadFile(trackUploadId);
                    if (trackFile != null && trackFile instanceof TrackStream) {
                        return (TrackStream) trackFile;
                    }
                    //Otherwise throw a generic exception; most likely the resource to acces is no longer available
                    throw new DownloadingNotFoundException();
                } else {
                    return null;
                }
            }
        }.find();

    }

    public TextStream getTextStreamFromSessionPool(final String textId) throws Exception {

        return getCache().new CacheInspector<TextStream>(TextStream.class.getSimpleName()+"!" + textId) {
            @Override
            public TextStream getMissed() throws Exception {
                //Text
                if (textId != null) {
                    TypedStreamInterface textFile = getSessionPoolHandler().getSessionPoolUploadFile(textId);
                    if (textFile != null && textFile instanceof TextStream) {
                        return (TextStream) textFile;
                    }
                    //Otherwise throw a generic exception; most likely the resource to access is no longer available
                    throw new DownloadingNotFoundException();
                } else {
                    return null;
                }
            }
        }.find();


    }

    public PictureStream getPictureStreamFromSessionPool(final String coverArtUploadId) throws Exception {

        return getCache().new CacheInspector<PictureStream>(PictureStream.class.getSimpleName()+"!" + coverArtUploadId) {
            @Override
            public PictureStream getMissed() throws Exception {
                //CoverArt
                //A cover art may be a stand alone file, or a picture embedded within a track tag
                //1) standalone, if applicable
                if (coverArtUploadId != null) {
                    TypedStreamInterface pictureFile = getSessionPoolHandler().getSessionPoolUploadFile(coverArtUploadId);
                    if (pictureFile instanceof PictureStream) {
                        //Standalone picture
                        return (PictureStream) pictureFile;
                    } else if (pictureFile instanceof TrackStream) {
                        //Embedded cover art
                        Tag tag = ((TrackStream) pictureFile).getTag();
                        if (tag != null) {
                            return tag.getCoverArt();
                        }
                    }
                    //Otherwise throw a generic exception; most likely the resource to access is no longer available
                    throw new DownloadingNotFoundException();
                } else {
                    return null;
                }
            }
        }.find();


    }

    public Collection<Integer> movePlaylistFilesFromPoolToDataStore(Map<Playlist, PlaylistFiles> playlistFiles) throws Exception {
        Collection<Integer> transactions = new HashSet<Integer>();
        if (playlistFiles != null && !playlistFiles.isEmpty()) {

            //Collection tracksToUpdate = new HashSet<Track>();

            for (Entry<Playlist, PlaylistFiles> playlistFilesEntry : playlistFiles.entrySet()) {
                final PlaylistFiles files = playlistFilesEntry.getValue();
                final Playlist playlist = playlistFilesEntry.getKey();
                if (playlist != null && files != null) {

                    if (files.doUpdatePicture()) {

                        if (files.getCoverArtUploadId() == null) {
                            Integer transaction = unsetPlaylistFilesFromDatastore(playlist);
                            if (transaction != null) {
                                transactions.add(transaction);
                            }
                        } else {
                            PictureStream coverArtStream = getPictureStreamFromSessionPool(files.getCoverArtUploadId());
                            playlist.setCoverArtContainer(coverArtStream.getContainer());                            
                            Integer transaction = getDataStore().moveTypedStream(playlist.getRefID(), coverArtStream, new TransferCallbackInterface() {
                                @Override
                                public void onNewHeader(FileInfoInterface updatedFileInfo) {
                                    playlist.setCoverArtContainer((PictureFileInfo) updatedFileInfo);
                                }

                                @Override
                                public void onProgress(byte percentDone) {
                                    //Not supported yet
                                }

                                @Override
                                public boolean onCancellable() {
                                    //Not supported yet
                                    return false;
                                }
                            }, true);
                            if (transaction != null) {
                                transactions.add(transaction);
                            }
                        }
                    }

                }
            }

        }
        return transactions;
    }

    public Collection<Integer> copyReportFilesFromPoolToDataStore(Map<Report, ReportFiles> reportFiles) throws Exception {
        Collection<Integer> transactions = new HashSet<Integer>();
        if (reportFiles != null && !reportFiles.isEmpty()) {

            //Collection tracksToUpdate = new HashSet<Track>();

            for (Entry<Report, ReportFiles> reportFilesEntry : reportFiles.entrySet()) {
                final ReportFiles files = reportFilesEntry.getValue();
                final Report report = reportFilesEntry.getKey();
                if (report != null && files != null) {

                    if (files.doUpdateText()) {

                        if (files.getTextUploadId() == null) {
                            Integer transaction = unsetReportFilesFromDatastore(report);
                            if (transaction != null) {
                                transactions.add(transaction);
                            }
                        } else {
                            TextStream textStream = getTextStreamFromSessionPool(files.getTextUploadId());
                            report.setTextContainer(textStream.getContainer());
                            Integer transaction = getDataStore().copyTypedStream(report.getRefID(), textStream, new TransferCallbackInterface() {
                                @Override
                                public void onNewHeader(FileInfoInterface updatedFileInfo) {
                                    report.setTextContainer((TextFileInfo) updatedFileInfo);
                                }

                                @Override
                                public void onProgress(byte percentDone) {
                                    //Not supported yet
                                }

                                @Override
                                public boolean onCancellable() {
                                    //Not supported yet
                                    return false;
                                }
                            }, true);
                            if (transaction != null) {
                                transactions.add(transaction);
                            }
                        }
                    }

                }
            }

        }
        return transactions;
    }

    private Integer unsetAudioFileFromDataStore(final String id) throws Exception {
        if (id!=null) {
            getCache().removeObjectInCache(TrackStream.class.getSimpleName()+"!"+id);
            return getDataStore().removeAudio(id);
        }
        return null;
    }
    private Integer unsetPictureFileFromDataStore(final String id) throws Exception {
        if (id!=null) {
            getCache().removeObjectInCache(PictureStream.class.getSimpleName()+"!"+id);
            return getDataStore().removePicture(id);
        }
        return null;
    }
    private Integer unsetTextFileFromDataStore(TEXT_FORMAT format, final String id) throws Exception {
        if (id!=null && format!=null) {
            getCache().removeObjectInCache(TextStream.class.getSimpleName()+"!"+id);
            return getDataStore().remove(format, id);
        }
        return null;
    }
    
    public Integer unsetPlaylistFilesFromDatastore(Playlist playlist) throws Exception {
        if (playlist != null) {
            if (playlist.getCoverArtContainer() != null) {
                playlist.setCoverArtContainer(null);
                return unsetPictureFileFromDataStore(playlist.getRefID());
            }
        }
        return null;
    }

    public Integer unsetReportFilesFromDatastore(Report report) throws Exception {
        if (report != null) {
            if (report.getTextContainer() != null) {
                TEXT_FORMAT fileFormat = report.getTextContainer().getFormat();
                report.setTextContainer(null);
                return unsetTextFileFromDataStore(fileFormat, report.getRefID());
            }
        }
        return null;
    }

    public Collection<Integer> unsetPlaylistFilesFromDatastore(Collection<Playlist> playlists) throws Exception {
        if (playlists != null) {
            Collection<Integer> transactions = new HashSet<Integer>();
            for (Playlist playlist : playlists) {
                Integer transaction = unsetPlaylistFilesFromDatastore(playlist);
                if (transaction != null) {
                    transactions.add(transaction);
                }
            }
            return transactions;
        }
        return null;
    }

    public Collection<Integer> unsetReportFilesFromDatastore(Collection<Report> reports) throws Exception {
        if (reports != null) {
            Collection<Integer> transactions = new HashSet<Integer>();
            for (Report report : reports) {
                Integer transaction = unsetReportFilesFromDatastore(report);
                if (transaction != null) {
                    transactions.add(transaction);
                }
            }
            return transactions;
        }
        return null;
    }

    public PictureStream getPlaylistPictureStreamFromDataStore(String coverArtUploadId) throws Exception {
        //fileId must be a Long representing a flyer Id
        //Whoever can view (simple read) a playlist can download the coverart picture
        return getPictureStreamFromDataStore(getProgrammeManager().getPlaylist(coverArtUploadId));
    }

    public TextStream getReportTextStreamFromDataStore(String textUploadId) throws Exception {
        //fileId must be a Long representing a text Id
        //Whoever can view (simple read) a report can download the text
        return getTextStreamFromDataStore(getChannelManager().getReport(textUploadId));
    }

    public PictureStream getPictureStreamFromDataStore(final Playlist playlist) throws Exception {

        return getCache().new CacheInspector<PictureStream>(playlist != null ? PictureStream.class.getSimpleName()+"!"+playlist.getRefID() : null) {
            @Override
            public PictureStream getMissed() throws Exception {
                if (playlist != null) {
                    PictureFileInfo metadata = null;
                    UntypedStreamInterface coverArtFile = null;
                    try {
                        metadata = playlist.getCoverArtContainer();
                        if (metadata != null) {
                            coverArtFile = getDataStore().getRawCoverArtStream(playlist.getRefID());
                        }
                        if (coverArtFile != null) {
                            return new PictureStream(
                                    metadata,
                                    coverArtFile);
                        }

                    } catch (NumberFormatException e) {
                        throw new DownloadingNotFoundException();
                    } catch (NoEntityException e) {
                        throw new DownloadingNotFoundException();
                    }
                }

                //Otherwise throw a generic exception; most likely the queried URL is bogus
                throw new DownloadingNotFoundException();
            }
        }.find();

    }

    public TextStream getTextStreamFromDataStore(final Report report) throws Exception {

        return getCache().new CacheInspector<TextStream>(report != null ? TextStream.class.getSimpleName()+"!"+report.getRefID() : null) {
            @Override
            public TextStream getMissed() throws Exception {
                if (report != null) {
                    TextFileInfo metadata = null;
                    UntypedStreamInterface textFile = null;
                    try {
                        metadata = report.getTextContainer();
                        if (metadata != null) {
                            textFile = getDataStore().getRawStream(metadata.getFormat(), report.getRefID());
                        }
                        if (textFile != null) {
                            return new TextStream(
                                    metadata,
                                    textFile);
                        }

                    } catch (NumberFormatException e) {
                        throw new DownloadingNotFoundException();
                    } catch (NoEntityException e) {
                        throw new DownloadingNotFoundException();
                    }
                }

                //Otherwise throw a generic exception; most likely the queried URL is bogus
                throw new DownloadingNotFoundException();
            }
        }.find();

    }

    public TrackStream getTrackAudioStreamFromDataStore(String coverArtUploadId) throws Exception {
        //fileId must be a Long representing a track Id
        //Whoever can view (simple read) a track can download the coverart picture
        return getAudioStreamFromDataStore(getTrackManager().getTrack(coverArtUploadId));
    }

    public TrackStream getAudioStreamFromDataStore(final Track track) throws Exception {

        return getCache().new CacheInspector<TrackStream>(track != null ? TrackStream.class.getSimpleName()+"!"+track.getRefID() : null) {
            @Override
            public TrackStream getMissed() throws Exception {
                if (track != null) {
                    AudioFileInfo metadata = null;
                    UntypedStreamInterface audioFile = null;
                    try {
                        metadata = track.getTrackContainer();
                        if (metadata != null) {
                            audioFile = getDataStore().getRawAudioStream(track.getRefID());
                        }
                        if (audioFile != null) {
                            return new TrackStream(
                                    metadata,
                                    audioFile);
                        }

                    } catch (NumberFormatException e) {
                        throw new DownloadingNotFoundException();
                    } catch (NoEntityException e) {
                        throw new DownloadingNotFoundException();
                    }
                }

                //Otherwise throw a generic exception; most likely the queried URL is bogus
                throw new DownloadingNotFoundException();
            }
        }.find();


    }

    public PictureStream getTrackPictureStreamFromDataStore(String coverArtUploadId) throws Exception {
        //fileId must be a Long representing a track Id
        //Whoever can view (simple read) a track can download the coverart picture
        return getPictureStreamFromDataStore(getTrackManager().getTrack(coverArtUploadId));
    }

    public PictureStream getPictureStreamFromDataStore(final Track track) throws Exception {

        return getCache().new CacheInspector<PictureStream>(track != null ? PictureStream.class.getSimpleName()+"!"+track.getRefID() : null) {

            @Override
            public PictureStream getMissed() throws Exception {
                if (track != null) {
            PictureFileInfo metadata = null;
            UntypedStreamInterface coverArtFile = null;
            try {
                metadata = track.getCoverArtContainer();
                if (metadata != null) {
                    coverArtFile = getDataStore().getRawCoverArtStream(track.getRefID());
                }
                if (coverArtFile != null) {
                    return new PictureStream(
                            metadata,
                            coverArtFile);
                }

            } catch (NumberFormatException e) {
                throw new DownloadingNotFoundException();
            } catch (NoEntityException e) {
                throw new DownloadingNotFoundException();
            }
        }

        //Otherwise throw a generic exception; most likely the queried URL is bogus
        throw new DownloadingNotFoundException();
            }
        }.find();        
        
    }

    public EmbeddedTagModule getDraftTags(String id) throws Exception {

        //Retrieve the handle to the temporary file thorugh the session using the specified upload tracking ID
        //And make sure this file really exists
        //Only track files contain tags
        final TrackStream draftStream = getTrackStreamFromSessionPool(id);
        //Embedded cover art
        if (draftStream!=null)
            return getTrackDTOAssembler().toDraftTagDTO(draftStream, id);

        //Otherwise throw a generic exception; most likely the queried URL is bogus
        throw new DownloadingNotFoundException();
    }

    public ContainerModule getDraftAudioFileInfo(String id) throws Exception {

        //Retrieve the handle to the temporary file thorugh the session using the specified upload tracking ID
        //And make sure this file really exists
        final TrackStream draftStream = getTrackStreamFromSessionPool(id);
        //Container info
        if (draftStream!=null)
            return getTrackDTOAssembler().toContainerDTO(draftStream, id, true);
        
        //Otherwise throw a generic exception; most likely the queried URL is bogus
        throw new DownloadingNotFoundException();
    }

    public CoverArtModule getDraftPictureFileInfo(String id) throws Exception {

        //Retrieve the handle to the temporary file thorugh the session using the specified upload tracking ID
        //And make sure this file really exists
        final PictureStream draftStream = getPictureStreamFromSessionPool(id);
        if (draftStream!=null)
            return getTrackDTOAssembler().toCoverArtDTO(draftStream, id, true);
        
        //Otherwise throw a generic exception; most likely the queried URL is bogus
        throw new DownloadingNotFoundException();
    }

    public void commitChangesInDataStore(Collection<Integer> transactionIds) {
        getDataStore().commit(transactionIds);
    }

    public void rollbackChangesInDataStore(Collection<Integer> transactionIds) {
        getDataStore().rollback(transactionIds);
    }

    public Collection<Integer> moveTrackFilesFromPoolToDataStore(Map<? extends Track, TrackFiles> trackFiles) throws Exception {
        Collection<Integer> transactions = new HashSet<Integer>();
        if (trackFiles != null && !trackFiles.isEmpty()) {

            //Collection tracksToUpdate = new HashSet<Track>();

            for (Entry<? extends Track, TrackFiles> trackFilesEntry : trackFiles.entrySet()) {
                final TrackFiles files = trackFilesEntry.getValue();
                final Track track = trackFilesEntry.getKey();
                if (track != null && files != null) {

                    if (files.doUpdateTrack() || files.doUpdatePicture()) {

                        if (files.doUpdatePicture()) {
                            if (files.getCoverArtUploadId() == null) {
                                //Failsafe: when in bulk edition mode it comes handy if we discard from processing tracks that already got no file of this kind, it should rollback the whole set of pending operations when it fails finding the file
                                if (track.getCoverArtContainer() != null) {
                                    track.setCoverArtContainer(null);
                                    transactions.add(
                                            unsetPictureFileFromDataStore(track.getRefID()));
                                }
                            } else {
                                PictureStream coverArtStream = getPictureStreamFromSessionPool(files.getCoverArtUploadId());
                                track.setCoverArtContainer(coverArtStream.getContainer());
                                transactions.add(
                                        getDataStore().moveTypedStream(track.getRefID(), coverArtStream, new TransferCallbackInterface() {
                                    @Override
                                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                                        track.setCoverArtContainer((PictureFileInfo) updatedFileInfo);
                                    }

                                    @Override
                                    public void onProgress(byte percentDone) {
                                        //Not supported yet
                                    }

                                    @Override
                                    public boolean onCancellable() {
                                        //Not supported yet
                                        return false;
                                    }
                                }, true));
                            }
                        }
                        if (files.doUpdateTrack()) {
                            if (files.getTrackUploadId() == null) {
                                //Failsafe: when in bulk edition mode it comes handy if we discard from processing tracks that already got no file of this kind, it should rollback the whole set of pending operations when it fails finding the file
                                if (track.getTrackContainer() != null) {
                                    track.setTrackContainer(null);
                                    transactions.add(
                                            unsetAudioFileFromDataStore(track.getRefID()));
                                }
                            } else {
                                TrackStream trackStream = getTrackStreamFromSessionPool(files.getTrackUploadId());
                                track.setTrackContainer(trackStream.getContainer());
                                transactions.add(
                                        getDataStore().moveTypedStream(track.getRefID(), trackStream, new TransferCallbackInterface() {
                                    @Override
                                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                                        track.setTrackContainer((AudioFileInfo) updatedFileInfo);
                                    }

                                    @Override
                                    public void onProgress(byte percentDone) {
                                        //Not supported yet
                                    }

                                    @Override
                                    public boolean onCancellable() {
                                        //Not supported yet
                                        return false;
                                    }
                                }, true));
                            }
                        }

                        //tracksToUpdate.add(track);
                    }

                }
            }

            /*if (!tracksToUpdate.isEmpty())
             updateTracks(tracksToUpdate);*/

        }
        return transactions;
    }

    public Collection<Integer> moveTrackToDataStore(final Track track, TrackStream audio, PictureStream picture) throws Exception {
        Collection<Integer> transactions = new HashSet<Integer>();
        if (track != null && (audio != null || picture != null)) { //Either audio or picture is mandatory

            if (picture != null) {
                track.setCoverArtContainer(picture.getContainer());
                transactions.add(
                        getDataStore().moveTypedStream(track.getRefID(), picture, new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        track.setCoverArtContainer((PictureFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                }, true));
            }
            if (audio != null) {
                track.setTrackContainer(audio.getContainer());
                transactions.add(
                        getDataStore().moveTypedStream(track.getRefID(), audio, new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        track.setTrackContainer((AudioFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                }, true));
            }

        }
        return transactions;
    }

    public Collection<Integer> copyTrackToDataStore(final Track track, TrackStream audio, PictureStream picture) throws Exception {
        Collection<Integer> transactions = new HashSet<Integer>();
        if (track != null && (audio != null || picture != null)) { //Either audio or picture is mandatory

            if (picture != null) {
                track.setCoverArtContainer(picture.getContainer());
                transactions.add(
                        getDataStore().copyTypedStream(track.getRefID(), picture, new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        track.setCoverArtContainer((PictureFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                }, true));
            }
            if (audio != null) {
                track.setTrackContainer(audio.getContainer());
                transactions.add(
                        getDataStore().copyTypedStream(track.getRefID(), audio, new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        track.setTrackContainer((AudioFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                }, true));
            }

        }
        return transactions;
    }

    public Collection<Integer> swapTrackFilesWithinDatastore(
            Map<PendingTrack, BroadcastableTrack> trackPairs) throws Exception {

        Collection<Integer> transactions = new HashSet<Integer>();
        for (Entry<PendingTrack, BroadcastableTrack> trackPair : trackPairs.entrySet()) {
            //key is source
            //value is destination

            final Track source = trackPair.getKey();
            final Track destination = trackPair.getValue();
            if (source != null && destination != null) {

                //Copy or move files from source to destination, if applicable
                //First tracks
                TransferCallbackInterface copyTrackCallback = new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        destination.setTrackContainer((AudioFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                };
                if (source.getTrackContainer() != null) {
                    //Reset cache for this object
                    getCache().removeObjectInCache(source.getTrackContainer().getClass().getSimpleName()+"!"+source.getRefID());
                    getCache().removeObjectInCache(source.getTrackContainer().getClass().getSimpleName()+"!"+destination.getRefID());
                    //move file from source to destination
                    destination.setTrackContainer(source.getTrackContainer());
                    source.setTrackContainer(null);
                    transactions.add(
                            getDataStore().renameAudio(source.getRefID(), destination.getRefID(), copyTrackCallback, true));
                } else {
                    //The source had no track? remove th one from destination, if applicable
                    if (destination.getTrackContainer() != null) {
                        //Drop the track attached to destination if source has none
                        destination.setTrackContainer(null);
                        transactions.add(
                                unsetAudioFileFromDataStore(destination.getRefID())
                                );
                    }
                }
                //Then cover arts
                TransferCallbackInterface copyCoverArtCallback = new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        destination.setCoverArtContainer((PictureFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                };
                if (source.getCoverArtContainer() != null) {
                    //Reset cache for this object
                    getCache().removeObjectInCache(source.getCoverArtContainer().getClass().getSimpleName()+"!"+source.getRefID());
                    getCache().removeObjectInCache(source.getCoverArtContainer().getClass().getSimpleName()+"!"+destination.getRefID());
                    //move file from source to destination
                    destination.setCoverArtContainer(source.getCoverArtContainer());
                    source.setCoverArtContainer(null);
                    transactions.add(
                            getDataStore().renamePicture(source.getRefID(), destination.getRefID(), copyCoverArtCallback, true));
                } else {
                    //The source had no track? remove th one from destination, if applicable
                    if (destination.getCoverArtContainer() != null) {
                        //Drop the track attached to destination if source has none
                        destination.setCoverArtContainer(null);
                        transactions.add(
                                unsetPictureFileFromDataStore(destination.getRefID())
                                );
                    }
                }

            }

        }
        return transactions;
    }

    public Collection<Integer> copyTrackFilesWithinDatastore(
            Map<PendingTrack, BroadcastableTrack> trackPairs) throws Exception {

        Collection<Integer> transactions = new HashSet<Integer>();
        //Tracks are sequentially and synchronously processed one after the other
        for (Entry<PendingTrack, BroadcastableTrack> trackPair : trackPairs.entrySet()) {
            //key is source
            //value is destination

            final Track source = trackPair.getKey();
            final Track destination = trackPair.getValue();
            if (source != null && destination != null) {

                //Copy or move files from source to destination, if applicable
                //First tracks
                TransferCallbackInterface copyTrackCallback = new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        destination.setTrackContainer((AudioFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                };
                if (source.getTrackContainer() != null) {
                    //Reset cache for this object
                    getCache().removeObjectInCache(source.getTrackContainer().getClass().getSimpleName()+"!"+destination.getRefID());
                    //copy the file from the source to the destination
                    destination.setTrackContainer(source.getTrackContainer());
                    transactions.add(
                            getDataStore().copyAudio(source.getRefID(), destination.getRefID(), copyTrackCallback, true));
                } else {
                    //The source had no track? remove th one from destination, if applicable
                    if (destination.getTrackContainer() != null) {
                        //Drop the track attached to destination if source has none
                        destination.setTrackContainer(null);
                        transactions.add(
                                unsetAudioFileFromDataStore(destination.getRefID())
                                );
                    }
                }
                //Then cover arts
                TransferCallbackInterface copyCoverArtCallback = new TransferCallbackInterface() {
                    @Override
                    public void onNewHeader(FileInfoInterface updatedFileInfo) {
                        destination.setCoverArtContainer((PictureFileInfo) updatedFileInfo);
                    }

                    @Override
                    public void onProgress(byte percentDone) {
                        //Not supported yet
                    }

                    @Override
                    public boolean onCancellable() {
                        //Not supported yet
                        return false;
                    }
                };
                if (source.getCoverArtContainer() != null) {
                    //copy the file from the source to the destination
                    destination.setCoverArtContainer(source.getCoverArtContainer());
                    transactions.add(
                            getDataStore().copyPicture(source.getRefID(), destination.getRefID(), copyCoverArtCallback, true));
                } else {
                    //The source had no track? remove th one from destination, if applicable
                    if (destination.getCoverArtContainer() != null) {
                        //Drop the track attached to destination if source has none
                        destination.setCoverArtContainer(null);
                        transactions.add(
                                unsetPictureFileFromDataStore(destination.getRefID())
                                );
                    }
                }

            }

        }
        return transactions;
    }

    public Collection<Integer> unsetTrackFilesFromDatastore(
            Collection<? extends Track> tracks) throws Exception {

        if (tracks != null) {
            Collection<Integer> transactions = new HashSet<Integer>();

            for (Track track : tracks) {

                if (track.getCoverArtContainer() != null) {
                    transactions.add(
                            unsetPictureFileFromDataStore(track.getRefID())
                            );
                }
                if (track.getTrackContainer() != null) {
                    transactions.add(
                            unsetAudioFileFromDataStore(track.getRefID())
                            );
                }
            }

            return transactions;
        }
        return null;
    }

    protected String getTrackStreamDirectDownloadPrivateURL(String channelId, String privateKey, String trackUploadId) throws Exception {
        return getDataStore().getAudioDirectSecuredAccessPrivateURL(channelId, privateKey, trackUploadId);
    }

    protected boolean hasTrackStreamDirectDownloadPrivateURL(String trackUploadId) {
        return getDataStore().hasAudioDirectSecuredAccessPrivateURL(trackUploadId);
    }

    protected String getPictureStreamDirectDownloadPrivateURL(String channelId, String privateKey, String pictureUploadId) throws Exception {
        return getDataStore().getPictureDirectSecuredAccessPrivateURL(channelId, privateKey, pictureUploadId);
    }

    protected boolean hasPictureStreamDirectDownloadPrivateURL(String pictureUploadId) {
        return getDataStore().hasPictureDirectSecuredAccessPrivateURL(pictureUploadId);
    }

    protected String getTrackAudioStreamDownloadPrivateURL(String channelId, String privateKey, String trackUploadId) throws Exception {
        if (hasTrackStreamDirectDownloadPrivateURL(trackUploadId)) {
            return getTrackStreamDirectDownloadPrivateURL(channelId, privateKey, trackUploadId);
        } else {
            //Append the channelId, and the privateKey, for security concerns it is so-so
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSAudioURLPrivateEndpoint());
            url = WebserviceAuthenticationService.appendWSPrivateParameters(url, trackUploadId, ChannelStreamerConnectionStatusHelper.getSeatNumber(channelId), privateKey);

            return url.toString();
        }
    }

    protected String getTrackPictureStreamDownloadPrivateURL(String channelId, String privateKey, String itemUploadId) throws Exception {
        if (hasPictureStreamDirectDownloadPrivateURL(itemUploadId)) {
            return getPictureStreamDirectDownloadPrivateURL(channelId, privateKey, itemUploadId);
        } else {
            //Append the channelId, and the privateKey, for security concerns it is so-so
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSCoverArtURLPrivateEndpoint());
            url = WebserviceAuthenticationService.appendWSPrivateParameters(url, itemUploadId, ChannelStreamerConnectionStatusHelper.getSeatNumber(channelId), privateKey);

            return url.toString();
        }
    }

    protected String getPlaylistPictureStreamDownloadPrivateURL(String channelId, String privateKey, String itemUploadId) throws Exception {
        if (hasPictureStreamDirectDownloadPrivateURL(itemUploadId)) {
            return getPictureStreamDirectDownloadPrivateURL(channelId, privateKey, itemUploadId);
        } else {
            //Append the channelId, and the privateKey, for security concerns it is so-so
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSFlyerURLPrivateEndpoint());
            url = WebserviceAuthenticationService.appendWSPrivateParameters(url, itemUploadId, ChannelStreamerConnectionStatusHelper.getSeatNumber(channelId), privateKey);

            return url.toString();
        }
    }

    protected String getTrackStreamDirectDownloadPublicURLForAuthenticatedUser(String trackUploadId) throws Exception {
        User currentUser = getSecurityManager().getCurrentlyAuthenticatedUser();
        return getDataStore().getAudioDirectSecuredAccessPublicURL(currentUser.getUsername(), currentUser.getPassword(), trackUploadId);
    }

    protected String getTrackStreamDirectDownloadPublicURLForUser(String trackUploadId, String userId, String password) throws Exception {
        return getDataStore().getAudioDirectSecuredAccessPublicURL(userId, password, trackUploadId);
    }

    protected boolean hasTrackStreamDirectDownloadPublicURL(String trackUploadId) {
        return getDataStore().hasAudioDirectSecuredAccessPublicURL(trackUploadId);
    }

    protected String getPictureStreamDirectDownloadPublicURLForAuthenticatedUser(String pictureUploadId) throws Exception {
        User currentUser = getSecurityManager().getCurrentlyAuthenticatedUser();
        return getDataStore().getPictureDirectSecuredAccessPublicURL(currentUser.getUsername(), currentUser.getPassword(), pictureUploadId);
    }

    protected String getPictureStreamDirectDownloadPublicURLForUser(String pictureUploadId, String userId, String password) throws Exception {
        return getDataStore().getPictureDirectSecuredAccessPublicURL(userId, password, pictureUploadId);
    }

    protected String getPictureStreamDirectDownloadPublicURLForWS(String pictureUploadId, String wsAPIKey, String wsPrivateKey) throws Exception {
        return getDataStore().getPictureDirectSecuredAccessPublicURL(wsAPIKey, wsPrivateKey, pictureUploadId);
    }

    protected boolean hasPictureStreamDirectDownloadPublicURL(String pictureUploadId) {
        return getDataStore().hasPictureDirectSecuredAccessPublicURL(pictureUploadId);
    }

    protected String getTrackAudioStreamDownloadPublicURLForAuthenticatedUser(String wsAPIKey, String wsPrivateKey, String trackUploadId) throws Exception {

        if (hasTrackStreamDirectDownloadPublicURL(trackUploadId)) {
            return getTrackStreamDirectDownloadPublicURLForAuthenticatedUser(trackUploadId);
        } else {
            //Do not append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSAudioURLPublicEndpoint());
            User currentUser = getSecurityManager().getCurrentlyAuthenticatedUser();
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, currentUser.getUsername(), currentUser.getPassword());

            return url.toString();
        }
    }

    protected String getTrackAudioStreamDownloadPublicURL(String wsAPIKey, String wsPrivateKey, String userId, String password, String trackUploadId) throws Exception {

        if (hasTrackStreamDirectDownloadPublicURL(trackUploadId)) {
            return getTrackStreamDirectDownloadPublicURLForUser(trackUploadId, userId, password);
        } else {
            //Do not append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSAudioURLPublicEndpoint());
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, userId, password);

            return url.toString();
        }
    }

    protected String getTrackPictureStreamDownloadPublicURLForAuthenticatedUser(String wsAPIKey, String wsPrivateKey, String trackUploadId) throws Exception {

        if (hasPictureStreamDirectDownloadPublicURL(trackUploadId)) {
            return getPictureStreamDirectDownloadPublicURLForAuthenticatedUser(trackUploadId);
        } else {
            //Append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSCoverArtURLPublicEndpoint());
            User currentUser = getSecurityManager().getCurrentlyAuthenticatedUser();
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, currentUser.getUsername(), currentUser.getPassword());

            return url.toString();
        }
    }

    protected String getPlaylistPictureStreamDownloadPublicURLForAuthenticatedUser(String wsAPIKey, String wsPrivateKey, String trackUploadId) throws Exception {

        if (hasPictureStreamDirectDownloadPublicURL(trackUploadId)) {
            return getPictureStreamDirectDownloadPublicURLForAuthenticatedUser(trackUploadId);
        } else {
            //Append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSFlyerURLPublicEndpoint());
            User currentUser = getSecurityManager().getCurrentlyAuthenticatedUser();
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, currentUser.getUsername(), currentUser.getPassword());

            return url.toString();
        }
    }

    protected String getTrackPictureStreamDownloadPublicURL(String wsAPIKey, String wsPrivateKey, String userId, String password, String trackUploadId) throws Exception {

        if (hasPictureStreamDirectDownloadPublicURL(trackUploadId)) {
            return getPictureStreamDirectDownloadPublicURLForUser(trackUploadId, userId, password);
        } else {
            //Do not append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSCoverArtURLPublicEndpoint());
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, userId, password);

            return url.toString();
        }
    }

    protected String getPlaylistPictureStreamDownloadPublicURL(String wsAPIKey, String wsPrivateKey, String userId, String password, String trackUploadId) throws Exception {

        if (hasPictureStreamDirectDownloadPublicURL(trackUploadId)) {
            return getPictureStreamDirectDownloadPublicURLForUser(trackUploadId, userId, password);
        } else {
            //Do not append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSFlyerURLPublicEndpoint());
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, userId, password);

            return url.toString();
        }
    }

    protected String getTrackPictureStreamDownloadPublicURL(String wsAPIKey, String wsPrivateKey, String trackUploadId) throws Exception {

        if (hasPictureStreamDirectDownloadPublicURL(trackUploadId)) {
            return getPictureStreamDirectDownloadPublicURLForWS(wsAPIKey, wsPrivateKey, trackUploadId);
        } else {
            //Do not append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSCoverArtURLPublicEndpoint());
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, null, null);

            return url.toString();
        }
    }

    protected String getPlaylistPictureStreamDownloadPublicURL(String wsAPIKey, String wsPrivateKey, String trackUploadId) throws Exception {

        if (hasPictureStreamDirectDownloadPublicURL(trackUploadId)) {
            return getPictureStreamDirectDownloadPublicURLForWS(wsAPIKey, wsPrivateKey, trackUploadId);
        } else {
            //Do not append the userId, nor the password, for security concerns; it's the streamer's responsibility to add them to the URL when it will try to fetch the file
            StringBuilder url = new StringBuilder(getSystemConfigurationHelper().getDefaultWSFlyerURLPublicEndpoint());
            url = WebserviceAuthenticationService.appendWSPublicParameters(url, trackUploadId, wsAPIKey, wsPrivateKey, null, null);

            return url.toString();
        }
    }

    public String getGWTCoverArtDownloadURLEndpointForUser(String id) throws Exception {
        return getSystemConfigurationHelper().getDefaultGWTCoverArtURLPublicEndpoint() + '?' + ParameterMapping.ITEM_ID.getQueryParameter() + '=' + id;
    }

    public String getGWTFlyerDownloadURLEndpointForUser(String id) throws Exception {
        return getSystemConfigurationHelper().getDefaultGWTFlyerURLPublicEndpoint() + '?' + ParameterMapping.ITEM_ID.getQueryParameter() + '=' + id;
    }

    public String getPublicWSTrackPictureDownloadURLEndpointForUser(String wsAPIKey, String wsPrivateKey, String id) throws Exception {
        return getTrackPictureStreamDownloadPublicURLForAuthenticatedUser(wsAPIKey, wsPrivateKey, id);
    }

    public String getPublicWSPlaylistPictureDownloadURLEndpointForUser(String wsAPIKey, String wsPrivateKey, String id) throws Exception {
        return getPlaylistPictureStreamDownloadPublicURLForAuthenticatedUser(wsAPIKey, wsPrivateKey, id);
    }

    public String getPublicWSTrackPictureDownloadURLEndpointForUser(String wsAPIKey, String wsPrivateKey, String userId, String password, String itemId) throws Exception {
        return getTrackPictureStreamDownloadPublicURL(wsAPIKey, wsPrivateKey, userId, password, itemId);
    }

    public String getPublicWSPlaylistPictureDownloadURLEndpointForUser(String wsAPIKey, String wsPrivateKey, String userId, String password, String itemId) throws Exception {
        return getPlaylistPictureStreamDownloadPublicURL(wsAPIKey, wsPrivateKey, userId, password, itemId);
    }

    public String getPublicWSTrackPictureDownloadURLEndpoint(String wsAPIKey, String wsPrivateKey, String id) throws Exception {
        return getTrackPictureStreamDownloadPublicURL(wsAPIKey, wsPrivateKey, id);
    }

    public String getPublicWSPlaylistPictureDownloadURLEndpoint(String wsAPIKey, String wsPrivateKey, String id) throws Exception {
        return getPlaylistPictureStreamDownloadPublicURL(wsAPIKey, wsPrivateKey, id);
    }

    public String getPrivateWSTrackPictureDownloadURLEndpoint(String channelId, String password, String itemId) throws Exception {
        return getTrackPictureStreamDownloadPrivateURL(channelId, password, itemId);
    }

    public String getPrivateWSPlaylistPictureDownloadURLEndpoint(String channelId, String password, String itemId) throws Exception {
        return getPlaylistPictureStreamDownloadPrivateURL(channelId, password, itemId);
    }

    public String getGWTDraftPictureDownloadURLEndpointForUser(String id) {
        return getSystemConfigurationHelper().getTemporaryPictureDownloadURL() + '?' + ParameterMapping.ITEM_ID.getQueryParameter() + '=' + id;
    }

    public String getGWTTrackDownloadURLEndpointForUser(String id) throws Exception {
        return getSystemConfigurationHelper().getDefaultGWTAudioURLPublicEndpoint() + '?' + ParameterMapping.ITEM_ID.getQueryParameter() + '=' + id;
    }

    public String getPrivateWSTrackDownloadURLEndpoint(String channelId, String password, String trackId) throws Exception {
        return getTrackAudioStreamDownloadPrivateURL(channelId, password, trackId);
    }

    public String getGWTDraftTrackDownloadURLEndpointForUser(String id) {
        return getSystemConfigurationHelper().getTemporaryAudioDownloadURL() + '?' + ParameterMapping.ITEM_ID.getQueryParameter() + '=' + id;
    }

    public String getGWTTextDownloadURLEndpointForUser(String id) {
        return getSystemConfigurationHelper().getDefaultGWTReportURLPublicEndpoint() + '?' + ParameterMapping.ITEM_ID.getQueryParameter() + '=' + id;
    }
}
