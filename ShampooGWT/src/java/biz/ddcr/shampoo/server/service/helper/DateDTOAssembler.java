/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.client.helper.date.JSYearMonthDay;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinute;
import biz.ddcr.shampoo.client.helper.date.JSYearMonthDayHourMinuteSecondMillisecond;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DateDTOAssembler {

    public static biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface toJS(biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface date) {
        return new JSYearMonthDay(date.getTimeZone(), date.getYear(), date.getMonthOfYear(), date.getDayOfMonth());
    }

    public static biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface toJS(biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteInterface date) {
        return new JSYearMonthDayHourMinute(date.getTimeZone(), date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), date.getHourOfDay(), date.getMinuteOfHour());
    }

    public static biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface toJS(biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface date) {
        return new JSYearMonthDayHourMinuteSecondMillisecond(date.getTimeZone(), date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), date.getHourOfDay(), date.getMinuteOfHour(), date.getSecondOfMinute(), date.getMillisecondOfSecond());
    }

    public static biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface fromJS(biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface date) {
        return new biz.ddcr.shampoo.server.helper.YearMonthWeekDay(date.getYear(), date.getMonth(), date.getDayOfMonth(), date.getTimeZone());
    }

    public static biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteInterface fromJS(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface date) {
        return new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(date.getYear(), date.getMonth(), date.getDayOfMonth(), date.getHourOfDay(), date.getMinuteOfHour(), date.getTimeZone());
    }

    public static biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface fromJS(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface date) {
        return new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond(date.getYear(), date.getMonth(), date.getDayOfMonth(), date.getHourOfDay(), date.getMinuteOfHour(), date.getSecondOfMinute(), date.getMillisecondOfSecond(), date.getTimeZone());
    }

    private DateDTOAssembler() {
    }
}
