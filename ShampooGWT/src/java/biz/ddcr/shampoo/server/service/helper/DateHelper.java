/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;
import org.joda.time.DateTimeZone;

/**
 *
 * @author okay_awright
 **/
public class DateHelper {

    private DateHelper() {
        //cheap singleton
    }

    /**
     *
     * Return a list of Java compliant time zone identifiers
     *
     * @return
     * @throws Exception
     **/
    public static Collection<String> getTimezones() throws Exception {
        //A treeset guarantees that values are sorted according to their natural order
        TreeSet<String> timezones = new TreeSet<String>();
        timezones.addAll(DateTimeZone.getAvailableIDs());
        return timezones;
    }

    public static YearMonthWeekDayHourMinuteSecondMillisecond getCurrentTime(String localTimeZone) throws Exception {
        return new YearMonthWeekDayHourMinuteSecondMillisecond(
                getCurrentTime(),
                localTimeZone
                );
    }
    
    public static long getCurrentTime() {
        return System.currentTimeMillis();
    }

}
