/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.client.form.timetable.RecurringTimetableSlotForm.RECURRING_EVENT;
import biz.ddcr.shampoo.client.form.track.format.AUDIO_TYPE;
import biz.ddcr.shampoo.client.form.track.format.AllowedContainerModule;
import biz.ddcr.shampoo.client.form.track.format.PICTURE_TYPE;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.serviceAsync.helper.HelperRPCServiceInterface;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import biz.ddcr.shampoo.server.helper.ServletUtils2;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;
import biz.ddcr.shampoo.client.helper.resource.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.TrackConfigurationHelper;
import biz.ddcr.shampoo.server.service.track.TrackDTOAssembler;
import java.util.Collection;
import java.util.HashSet;
import javax.servlet.http.HttpSession;

/**
 *
 * @author okay_awright
 **/
public class HelperRPCService extends GWTRPCLocalThreadService implements HelperRPCServiceInterface {

    private transient TrackConfigurationHelper trackConfigurationHelper;
    private transient CoverArtConfigurationHelper coverArtConfigurationHelper;
    private SystemConfigurationHelper systemConfigurationHelper;
    private BrandingService brandingHelper;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public TrackConfigurationHelper getTrackConfigurationHelper() {
        return trackConfigurationHelper;
    }

    public void setTrackConfigurationHelper(TrackConfigurationHelper trackConfigurationHelper) {
        this.trackConfigurationHelper = trackConfigurationHelper;
    }

    public CoverArtConfigurationHelper getCoverArtConfigurationHelper() {
        return coverArtConfigurationHelper;
    }

    public void setCoverArtConfigurationHelper(CoverArtConfigurationHelper coverArtConfigurationHelper) {
        this.coverArtConfigurationHelper = coverArtConfigurationHelper;
    }

    public BrandingService getBrandingHelper() {
        return brandingHelper;
    }

    public void setBrandingHelper(BrandingService brandingHelper) {
        this.brandingHelper = brandingHelper;
    }

    @Override
    public Collection<String> getTimezones() throws Exception {
        return DateHelper.getTimezones();
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface getCurrentTime(String localTimeZone) throws Exception {
        if (localTimeZone != null) {
            return DateDTOAssembler.toJS(DateHelper.getCurrentTime(localTimeZone));
        } else {
            return null;
        }
    }

    @Override
    public byte getWeekOfMonth(biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface currentDate) throws Exception {
        return DateDTOAssembler.fromJS(currentDate).getWeekOfMonth();
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface switchTimeZone(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface currentTime, String localTimeZone) throws Exception {
        return DateDTOAssembler.toJS(DateDTOAssembler.fromJS(currentTime).switchTimeZone(localTimeZone));
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface switchTimeZone(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface currentTime, String localTimeZone) throws Exception {
        return DateDTOAssembler.toJS(DateDTOAssembler.fromJS(currentTime).switchTimeZone(localTimeZone));
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface addSeconds(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface currentTime, long secondsToAdd) throws Exception {
        biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface t = DateDTOAssembler.fromJS(currentTime);
        t.addSeconds(secondsToAdd);
        return DateDTOAssembler.toJS(t);
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface addDays(biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface currentTime, long daysToAdd) throws Exception {
        biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface t = DateDTOAssembler.fromJS(currentTime);
        t.addDays(daysToAdd);
        return DateDTOAssembler.toJS(t);
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface addDays(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteInterface currentTime, long daysToAdd) throws Exception {
        biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteInterface t = DateDTOAssembler.fromJS(currentTime);
        t.addDays(daysToAdd);
        return DateDTOAssembler.toJS(t);
    }

    @Override
    public biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface addDays(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface currentTime, long daysToAdd) throws Exception {
        biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface t = DateDTOAssembler.fromJS(currentTime);
        t.addDays(daysToAdd);
        return DateDTOAssembler.toJS(t);
    }

    @Override
    public long diffMilliseconds(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface fromTime, biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface toTime) throws Exception {
        return YearMonthWeekDayHourMinuteSecondMillisecond.diffMilliseconds(
                DateDTOAssembler.fromJS(fromTime),
                DateDTOAssembler.fromJS(toTime));
    }
    @Override
    public long diffNowMilliseconds(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface fromTime) throws Exception {
        return YearMonthWeekDayHourMinuteSecondMillisecond.diffMilliseconds(
                DateDTOAssembler.fromJS(fromTime),
                DateHelper.getCurrentTime(fromTime.getTimeZone()));
    }

    /**
     * @deprecated
     * @param fromTime
     * @param toTime
     * @param frequency
     * @return
     * @throws Exception
     **/
    @Override
    public Collection<biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface> getInterimDates(biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface fromTime, biz.ddcr.shampoo.client.helper.date.YearMonthDayInterface toTime, RECURRING_EVENT frequency) throws Exception {
        Collection<biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface> interimDates = new HashSet<biz.ddcr.shampoo.client.helper.date.YearMonthDayHourMinuteSecondMillisecondInterface>();

        if (fromTime != null && toTime != null & frequency != null) {
            biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface currentTime = DateDTOAssembler.fromJS(fromTime);
            biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface endTime = DateDTOAssembler.fromJS(toTime);
            //Compute new dates until the end time is not yet reached
            for (;;) {
                switch (frequency) {
                    case daily:
                        currentTime.addDays(1);
                        break;
                    case weekly:
                        currentTime.addDays(7);
                        break;
                    case monthly:
                        currentTime.addMonths(1);
                        break;
                    case yearly:
                        currentTime.addYears(1);
                        break;
                    default:
                        break;
                }
                if (currentTime.compareTo(endTime) >= 0) {
                    break;
                }
                interimDates.add(DateDTOAssembler.toJS(currentTime));
            }
        }

        return interimDates;
    }

    @Override
    public String getAppVersion() {
        return VersioningHelper.getFullAppReference();
    }

    @Override
    public String getCopyrightNotice() {
        return VersioningHelper.getFullCopyrightText();
    }

    @Override
    public Collection<AUDIO_TYPE> getAllowedAudioFormats() {
        return TrackDTOAssembler.toAudioDTO(getTrackConfigurationHelper().getAllowedFormats());
    }

    @Override
    public Collection<PICTURE_TYPE> getAllowedPictureFormats() {
        return TrackDTOAssembler.toPictureDTO(getCoverArtConfigurationHelper().getAllowedFormats());
    }

    @Override
    public AllowedContainerModule getAllowedAudioFeatures(AUDIO_TYPE audioType) {
        AUDIO_FORMAT audioFormat = TrackDTOAssembler.fromDTO(audioType);

        AllowedContainerModule allowedContainerBoundaries = new AllowedContainerModule();
        allowedContainerBoundaries.setAcceptMono(getTrackConfigurationHelper().isAcceptMono());
        allowedContainerBoundaries.setAcceptVBR(getTrackConfigurationHelper().isAcceptVBR(audioFormat));
        allowedContainerBoundaries.setAllowedFormats(getAllowedAudioFormats());
        allowedContainerBoundaries.setMaxBitrate(getTrackConfigurationHelper().getMaxBitrate(audioFormat));
        allowedContainerBoundaries.setMinBitrate(getTrackConfigurationHelper().getMinBitrate(audioFormat));
        allowedContainerBoundaries.setMaxSamplerate(getTrackConfigurationHelper().getMaxSamplerate());
        allowedContainerBoundaries.setMinSamplerate(getTrackConfigurationHelper().getMinSamplerate());
        allowedContainerBoundaries.setMaxDuration(new JSDuration(getTrackConfigurationHelper().getMaxDuration()));
        allowedContainerBoundaries.setMinDuration(new JSDuration(getTrackConfigurationHelper().getMinDuration()));
        return allowedContainerBoundaries;
    }

    @Override
    public String getHTMLHeader() {
        return getBrandingHelper().getHTMLHeader();
    }

    @Override
    public String getHTMLFrontpage() {
        return getBrandingHelper().getHTMLFrontpage();
    }

    @Override
    public Collection<String> getAllowedPictureFormatExtensions() throws Exception {
        Collection<String> allowedExtensions = new HashSet<String>();
        for (PICTURE_FORMAT allowedFormat : getCoverArtConfigurationHelper().getAllowedFormats()) {
            allowedExtensions.addAll(PictureFormat.FILE_EXTENSIONS_PICTURE_FORMAT.get(allowedFormat));
        }
        return allowedExtensions;
    }

    @Override
    public Collection<String> getAllowedAudioFormatExtensions() throws Exception {
        Collection<String> allowedExtensions = new HashSet<String>();
        for (AUDIO_FORMAT allowedFormat : getTrackConfigurationHelper().getAllowedFormats()) {
            allowedExtensions.addAll(AudioFormat.FILE_EXTENSIONS_AUDIO_FORMAT.get(allowedFormat));
        }
        return allowedExtensions;
    }

    @Override
    public String getTemporaryAudioUploadURL() {
        return getSystemConfigurationHelper().getTemporaryAudioUploadURL();
    }

    @Override
    public String getTemporaryPictureUploadURL() {
        return getSystemConfigurationHelper().getTemporaryPictureUploadURL();
    }

    @Override
    public String getJSESSIONID() {
        HttpSession session = ServletUtils2.getLocalSession();
        if (session != null) {
            String jsessionToken = session.getId();
            return jsessionToken;
        }
        return null;
    }

    @Override
    public Collection<SERIALIZATION_METADATA_FORMAT> getTextSerializationFormats() {
        Collection<SERIALIZATION_METADATA_FORMAT> allowedFormats = new HashSet<SERIALIZATION_METADATA_FORMAT>();
        allowedFormats.add(SERIALIZATION_METADATA_FORMAT.genericCSV);
        allowedFormats.add(SERIALIZATION_METADATA_FORMAT.genericJSON);
        allowedFormats.add(SERIALIZATION_METADATA_FORMAT.genericXML);
        return allowedFormats;
    }
    
}
