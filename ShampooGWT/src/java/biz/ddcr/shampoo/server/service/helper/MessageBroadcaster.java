/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.server.domain.feedback.Feedback;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackMessage;
import biz.ddcr.shampoo.server.service.helper.gwt.GWTMessageBroadcaster;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.logging.LogFactory;

/**
 *
 * Static object bound to a threadlocal responsible for broadcasting messages on-demand.
 * Currently only used to broadcast messages to the GWT GUI via reverse Ajax
 * Messages must be explicitly marked to be sent before the localthread dies out for actual notification.
 * No message can be stored unless you explicitly say so
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class MessageBroadcaster {

    private final static ThreadLocal<List<Feedback>> threadBoundMessages = new ThreadLocal<List<Feedback>>() {
        @Override
         protected synchronized List<Feedback> initialValue() {
             return null;
         }
    };    
    
    /**Mark the bean as ready to handle and process messages
        start() called multiple times for the same thread has no effect **/
    public static void start() {
        if (threadBoundMessages.get()==null) {
            //Put everything in a list so that the sequence is guaranteed to be FIFO, otherwise the GWT Comet broadcaster can mix up the items into unordered chunks of data
            threadBoundMessages.set(new ArrayList<Feedback>());
        }
    }
    /**Store messages**/
    public static void add(Feedback feedback) {
        if (threadBoundMessages.get()!=null && feedback!=null) {
            threadBoundMessages.get().add(feedback);
        }
    }
    /**Remove messages**/
    public static void remove(Feedback feedback) {
        if (threadBoundMessages.get()!=null && feedback!=null) {
            threadBoundMessages.get().remove(feedback);    
        }
    }
    /**Remove all messages**/
    public static void clear() {
        if (threadBoundMessages.get()!=null) {
            threadBoundMessages.get().clear();        
        }
    }
    /**Remove all messages**/
    public static Collection<Feedback> get() {
        return threadBoundMessages.get()!=null ? threadBoundMessages.get() : new HashSet<Feedback>();        
    }

    /**Commit stored messages for broadcast**/
    public static void flush() {
         if (threadBoundMessages.get()!=null && !threadBoundMessages.get().isEmpty()) {
            try {
                //Reverse AJAX notification
                GWTMessageBroadcaster.send(threadBoundMessages.get());
            } catch (Exception ex) {
                LogFactory.getLog(MessageBroadcaster.class).warn("Message broadcast to GWT failed", ex);
            }
         }             
     }
    public static void flushErrorsOnly() {
         if (threadBoundMessages.get()!=null && !threadBoundMessages.get().isEmpty()) {
            try {
                List<Feedback> feedbacks = threadBoundMessages.get();
                if (feedbacks!=null && !feedbacks.isEmpty()) {
                    List<Feedback> filteredFeedbacks = new ArrayList<Feedback>();
                    for (Feedback feedback : feedbacks)
                        if (feedback instanceof FeedbackMessage && ((FeedbackMessage)feedback).getType()==FeedbackMessage.TYPE.error)
                            filteredFeedbacks.add(feedback);                        
                    //Reverse AJAX notification
                    GWTMessageBroadcaster.send(filteredFeedbacks);
                }
            } catch (Exception ex) {
                LogFactory.getLog(MessageBroadcaster.class).warn("Message broadcast to GWT failed", ex);
            }
         }             
     }
    
    private MessageBroadcaster() {
    }
    
}
