/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import java.util.UUID;

/**
 *
 * The actual events that are being piled up in a specific pile from the pool
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PilePoolEvent<T extends AbstractPilePool.RunnableGenericEvent> implements AbstractPilePool.PileEventInterface<T> {

    private UUID uuid;
    private T runnable;
    private long published;

    public PilePoolEvent(long published, UUID uuid, T runnable) {
        this.published = published;
        this.uuid = uuid;
        this.runnable = runnable;
    }

    /**
     * Identifier of the event *
     */
    @Override
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Runnable associated *
     */
    @Override
    public T getRunnable() {
        return runnable;
    }

    /**
     * when was it created? *
     */
    @Override
    public long getPublished() {
        return published;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PilePoolEvent<T> other = (PilePoolEvent<T>) obj;
        if (this.uuid != other.uuid && (this.uuid == null || !this.uuid.equals(other.uuid))) {
            return false;
        }
        if (this.published != other.published) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.uuid != null ? this.uuid.hashCode() : 0);
        hash = 89 * hash + (int) (this.published ^ (this.published >>> 32));
        return hash;
    }

    @Override
    public int compareTo(AbstractPilePool.PileEventInterface other) {
        long diff = getPublished() - other.getPublished();
        if (diff == 0) {
            return getUUID().compareTo(other.getUUID());
        } else {
            return diff > 0 ? 1 : -1;
        }
    }

    @Override
    public String toString() {
        return getUUID() + "@" + getPublished();
    }
}
