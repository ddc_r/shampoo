/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.server.service.GenericService;

/**
 *
 * Bean that piles up (FIFO) events and make sure they're sequentially processed
 * Piles are not polled in a separate thread on purpose: it's a blocking behaviour
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PilePoolManager<T extends AbstractPilePool> extends GenericService {

    private T eventPilePoolHelper;

    /** Accesses the pool of actually available piles for the whole application **/
    public T getEventPilePoolHelper() {
        return eventPilePoolHelper;
    }

    public void setEventPilePoolHelper(T eventPilePoolHelper) {
        this.eventPilePoolHelper = eventPilePoolHelper;
    }

}
