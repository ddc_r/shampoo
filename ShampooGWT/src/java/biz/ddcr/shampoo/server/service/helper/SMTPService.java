/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.client.helper.errors.MessageAuthenticateException;
import biz.ddcr.shampoo.client.helper.errors.MessageException;
import biz.ddcr.shampoo.client.helper.errors.MessageSendException;
import biz.ddcr.shampoo.server.helper.BasicMessageWrapper;
import biz.ddcr.shampoo.server.helper.MessagingOutServiceInterface;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.service.GenericService;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class SMTPService extends GenericService implements MessagingOutServiceInterface<JavaMailSender, VelocityEngine, BasicMessageWrapper> {

    private JavaMailSender provider;
    private String defaultSender;
    private transient VelocityEngine templatingEngine;

    @Override
    public void setTemplateProvider(VelocityEngine templatingEngine) {
        this.templatingEngine = templatingEngine;
    }

    public VelocityEngine getTemplateProvider() {
        return templatingEngine;
    }    

    @Override
    public void setSmtpProvider(JavaMailSender provider) {
        this.provider = provider;
    }

    public JavaMailSender getSmtpProvider() {
        return provider;
    }

    @Override
    public String getDefaultSender() {
        return defaultSender;
    }

    @Override
    public void setDefaultSender(String defaultSender) {
        this.defaultSender = defaultSender;
    }

    @Override
    public boolean isEnabled() {
        return (provider!=null && templatingEngine!=null);
    }

    /**
     * Convert a BasicMessageWrapper into a SimpleMailMessage
     * @param mail
     * @return
     */
    protected SimpleMailMessage toMessage(BasicMessageWrapper mail) throws Exception {
        //Don't bother creating a mail if there are no recipients, nor any message
        if (mail != null
                && (mail.getLongTemplateLocation() != null && mail.getLongTemplateLocation().length()!=0)
                && (mail.getRecipients() != null && !mail.getRecipients().isEmpty())) {

            SimpleMailMessage message = new SimpleMailMessage();
            //BasicMessageWrapper header
            message.setFrom(mail.getSender() != null ? mail.getSender().getEmail() : defaultSender);
            message.setSentDate(new Date(mail.getDate() != null ? mail.getDate().getUnixTime() : DateHelper.getCurrentTime("UTC").getUnixTime()));
            String[] tos = new String[mail.getRecipients().size()];
            int i = 0;
            for (User recipient : mail.getRecipients()) {
                tos[i++] = recipient.getEmail();
            }
            message.setTo(tos);
            //BasicMessageWrapper subject, with pseudo basic text structuring
            String subject = VelocityEngineUtils.mergeTemplateIntoString(
                    templatingEngine,
                    mail.getShortTemplateLocation(),
                    mail.getTemplateVariables());
            message.setSubject(subject);
            //BasicMessageWrapper body, with pseudo basic text structuring
            String body = VelocityEngineUtils.mergeTemplateIntoString(
                    templatingEngine,
                    mail.getLongTemplateLocation(),
                    mail.getTemplateVariables());
            message.setText(body);
            return message;
        }
        return null;
    }

    @Override
    public void send(BasicMessageWrapper mail) throws Exception {
        SimpleMailMessage message;
        if ((message = toMessage(mail)) != null) {
            try {
                provider.send(message);
            } catch (MailSendException e) {
                throw new MessageSendException(e.getMessage());
            } catch (MailAuthenticationException e) {
                throw new MessageAuthenticateException(e.getMessage());
            } catch (MailException e) {
                throw new MessageException(e.getMessage());
            }
        }
    }

    @Override
    public void send(Collection<BasicMessageWrapper> mails) throws Exception {
        if (mails != null && mails.size() > 0) {
            Collection<SimpleMailMessage> messages = new HashSet<SimpleMailMessage>();
            for (BasicMessageWrapper mail : mails) {
                SimpleMailMessage message = toMessage(mail);
                if (message != null) {
                    messages.add(message);
                }
            }
            if (!messages.isEmpty()) {
                try {
                    provider.send(messages.toArray(new SimpleMailMessage[messages.size()]));
                } catch (MailSendException e) {
                    throw new MessageSendException(e.getMessage());
                } catch (MailAuthenticationException e) {
                    throw new MessageAuthenticateException(e.getMessage());
                } catch (MailException e) {
                    throw new MessageException(e.getMessage());
                }
            }
        }
    }
}
