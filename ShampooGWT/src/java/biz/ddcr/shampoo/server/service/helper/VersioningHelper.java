/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.helper;

import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 *
 * Reading a MANIFEST from a WAR should be simple; it is not
 *
 * @author okay_awright
 **/
public class VersioningHelper /*implements ApplicationContextAware*/ {

    private static final String APPLICATION_NAME = "Shampoo";
    private static final String DEFAULT_IMPLEMENTATION_VENDOR = "okay_awright/DDC(R) and contributors";
    private static Attributes implementationInfo;

    /*public void setApplicationContext(ApplicationContext ac) throws BeansException {
        readManifest(ac);
    }

    private void readManifest(ApplicationContext applicationContext) {

        try {

            Attributes attr = new Manifest(applicationContext.getResource("/META-INF/MANIFEST.MF").getInputStream()).getMainAttributes();

            IMPLEMENTATION_VERSION = attr.getValue(Attributes.Name.IMPLEMENTATION_VERSION);
            IMPLEMENTATION_VENDOR = attr.getValue(Attributes.Name.IMPLEMENTATION_VENDOR);
            IMPLEMENTATION_TITLE = attr.getValue(Attributes.Name.IMPLEMENTATION_TITLE);

        } catch (IOException ex) {

            IMPLEMENTATION_VERSION = null;
            IMPLEMENTATION_VENDOR = null;
            IMPLEMENTATION_TITLE = null;

        }

    }*/
    public static String getStaticAppName() {
        return APPLICATION_NAME;
    }

    static String getImplementationAttribute(Attributes.Name key) {
        if (implementationInfo!=null || getImplementationAttributesFromJar() || getImplementationAttributesWorkaround())
            return implementationInfo.getValue(key);
        else
            return null;
    }

    private static boolean getImplementationAttributesFromJar() {
        try {
            Package pkg = VersioningHelper.class.getPackage();
            String implementationVersion = pkg!=null?pkg.getImplementationVersion():null;
            String implementationVendor = pkg!=null?pkg.getImplementationVendor():null;
            String implementationTitle = pkg!=null?pkg.getImplementationTitle():null;
            if (implementationVersion!=null || implementationVendor!=null || implementationTitle!=null) {
                implementationInfo = new Attributes();
                implementationInfo.put(Attributes.Name.IMPLEMENTATION_VERSION, implementationVersion);
                implementationInfo.put(Attributes.Name.IMPLEMENTATION_VENDOR, implementationVendor);
                implementationInfo.put(Attributes.Name.IMPLEMENTATION_TITLE, implementationTitle);
                return true;
            } else
                return false;
        } catch (Exception e) {
            implementationInfo = null;
            return false;
        }
    }

    private static boolean getImplementationAttributesWorkaround() {
        try {
            //Huge kludge, dirty and mostly inefficient
            //TODO: use Spring's ApplicationContextAware if one day I can make it work properly
            URL urlToThisClass = VersioningHelper.class.getResource("VersioningHelper.class");
            String pathToThisClass = urlToThisClass.toString();
            String manifestPath = pathToThisClass.substring(0, pathToThisClass.lastIndexOf("WEB-INF")) + "META-INF/MANIFEST.MF";
            Manifest fileManifest = new Manifest(new URL(manifestPath).openStream());

            implementationInfo = fileManifest.getMainAttributes();
            return true;
        } catch (Throwable ex) {
            implementationInfo = null;
            return false;
        }
    }

    public static String getImplementationVersion() {
        return getImplementationAttribute(Attributes.Name.IMPLEMENTATION_VERSION);
    }

    public static String getImplementationTitle() {
        return getImplementationAttribute(Attributes.Name.IMPLEMENTATION_TITLE);
    }

    public static String getImplementationVendor() {
        return getImplementationAttribute(Attributes.Name.IMPLEMENTATION_VENDOR);
    }

    public static String getFullAppReference() {
        StringBuilder s = new StringBuilder();

        s.append(getStaticAppName());
        if (getImplementationTitle()!=null || getImplementationVendor()!=null) {
            s.append("(");
            if (getImplementationTitle()!=null)
                s.append(getImplementationTitle());
            if (getImplementationTitle()!=null && getImplementationVendor()!=null)
                s.append("-");
            if (getImplementationVendor()!=null)
                s.append(getImplementationVendor());
            s.append(")");
        }
        if (getImplementationVersion()!=null)
            s.append("/").append(getImplementationVersion());

        return s.toString();
    }

    public static String getFullCopyrightText() {
        StringBuilder s = new StringBuilder();

        s.append("Copyright © 2011- ");
        if (getImplementationVendor()!=null)
            s.append(getImplementationVendor());
        else
            s.append(DEFAULT_IMPLEMENTATION_VENDOR);

        return s.toString();
    }

    private VersioningHelper() {
    }
    
}
