/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.WebserviceOutOfQuotaException;
import biz.ddcr.shampoo.server.domain.webservice.Hit;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.webservice.Webservice.MODULE;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.net.URLCodec;

/**
 * Library for decoding/encoding HMACs in payload for public webservices
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class WebserviceAuthenticationService {

    /** Cannot replay the same request after this duration after the first hit */
    private long authenticationNoReplayInterval;
    /** fuzzy duration that allows for asynchronicity between client and server clocks */
    private long authenticationJitter;
    /** disables all authentication for debugging purposes */
    private boolean debugDisableAuthentication;
    
    private ChannelManagerInterface channelManager;

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public boolean isDebugDisableAuthentication() {
        return debugDisableAuthentication;
    }

    public void setDebugDisableAuthentication(boolean debugDisableAuthentication) {
        this.debugDisableAuthentication = debugDisableAuthentication;
    }

    public long getAuthenticationNoReplayInterval() {
        if (authenticationNoReplayInterval<0)
            authenticationNoReplayInterval = 5 * 60 * 1000; //in millisecond, equals 5 minutes by default
        return authenticationNoReplayInterval;
    }

    public void setAuthenticationNoReplayInterval(long authenticationNoReplayInterval) {
        this.authenticationNoReplayInterval = authenticationNoReplayInterval;
    }

    public long getAuthenticationJitter() {
        if (authenticationJitter<0)
            authenticationJitter = 30 * 1000; //in millisecond, equals 30 seconds by default
        return authenticationJitter;
    }

    public void setAuthenticationJitter(long authenticationJitter) {
        this.authenticationJitter = authenticationJitter;
    }

    /**
     * Get a user from the URL query *
     */
    public boolean isValidUserAuthentication(User user, String hmac, long timestamp) throws Exception {
        if (user != null && hmac != null) {

            //If the given hashed password doesn't match the one we compute then it's been spoofed
            if (checkHMACValidity(user.getUsername(), user.getPassword(), timestamp, hmac)) {
                return true;
            }

        }
        throw new AccessDeniedException();
    }

    /**
     * Check whether the access to a webservice is granted by analyzing the query parameters and HMAC
     */
    public boolean checkAccessPublicResource(Webservice ws, long timestamp, MODULE module, String hmac, String remoteIP) throws Exception {

        if (ws != null && hmac != null && module != null) {

            //Check that first because it's inexpensive to check (no further database round trip)
            if (checkHMACValidity(ws.getApiKey(), ws.getPrivateKey(), timestamp, hmac)) {

                //Check whether the given module is on for this webservice
                //Or if the remote IP is allowed
                if (!ws.isModuleOn(module) || (ws.getWhitelistRegexp() != null && ws.getWhitelistRegexp().length() != 0 && remoteIP != null && !remoteIP.matches(ws.getWhitelistRegexp()))) {
                    return false;
                }

                //Check whether the call was respectful of the enforced max. firerate
                Long maxFireRate = ws.getMaxFireRate(); //Max. firerate is the minimum time interval to observe between two hits, in milliseconds
                //if it's null then there's no limit
                if (maxFireRate != null) {
                    Hit lastHit = null;
                    //UPDATE: possible (not fully benchmarked) performance gains by using a dedicated HQL request
                    /*try {
                        lastHit = ws.getHits().first();
                    } catch (NoSuchElementException e) {
                        //Sliently drops this one: it means the webservice has never been called yet, so it's alright
                    }*/
                    try {
                        lastHit = getChannelManager().getLatestHit(ws);
                    } catch (NoEntityException e) {
                        //Sliently drops this one: it means the webservice has never been called yet, so it's alright
                    }
                    //Can be null if it's a brand new shiny webservice, and then it's never been called yet
                    if (lastHit != null && (DateHelper.getCurrentTime() - lastHit.getTimestamp() < maxFireRate)) //Deny access
                    //don't update the lastAccessed property in this case, otherwise it will withdraw any benefit of shutting down the access during heavy load, i.e. reduce Hibernate usage
                    {
                        throw new WebserviceOutOfQuotaException();
                    }
                }

                //Check whether we're not out of quota
                Long maxDailyQuota = ws.getMaxDailyLimit(); //max number of requests/day
                //if it's null then there's no limit
                if (maxDailyQuota != null) {
                    Long previousHitNumberForToday = getChannelManager().getDailyNumberOfHits(ws);
                    //Can be null if it's a brand new shiny webservice, and then it's never been called yet
                    if (previousHitNumberForToday != null && previousHitNumberForToday > maxDailyQuota) //Deny access
                    //don't update the lastAccessed property in this case, otherwise it will withdraw any benefit of shutting down the access during heavy load, i.e. reduce Hibernate usage
                    {
                        throw new WebserviceOutOfQuotaException();
                    }
                }

                //add a new hit
                //and update the webservice last accession time
                if (maxFireRate != null || maxDailyQuota != null) {
                    getChannelManager().addHit(ws);
                }

                return true;
            }
            return false;
        }
        throw new BadRequestException();
    }

    /**
     * Check if the API_HMAC is valid *
     */
    private boolean checkHMACValidity(String publicKey, String privateKey, long timestamp, String hmac) throws Exception {
        //Debugging feature: do not turn on
        if (isDebugDisableAuthentication()) return true;

        //First see whether the timestamp is not too old, othrwise simply decline the access
        //It's not good if the timestamp specifies some time in the future as well
        long replayedTimeWindow = DateHelper.getCurrentTime() - timestamp;
        if (replayedTimeWindow >= -getAuthenticationJitter() && replayedTimeWindow <= (getAuthenticationNoReplayInterval()+getAuthenticationJitter())) {

            //Re-generate the hmac and then compare the genuine one from the one sent along with the query
            return makeHMAC(//
                    publicKey,//
                    privateKey,//
                    timestamp).equals(hmac);
        }
        return false;
    }

    public static String makeHMAC(String publicKey, String privateKey, long timestamp) throws EncoderException {
        //HMACs are computed like that: md5(publicKey + md5(privateKey) + timestamp)
        return DigestUtils.md5Hex(//
                publicKey//
                + DigestUtils.md5Hex(//
                privateKey//
                )//
                + timestamp);
    }

    /**
     * Append parameters to private webservice URLs *
     */
    public static StringBuilder appendWSPrivateParameters(StringBuilder url, String itemUploadId, Long seatNumber, String privateKey) throws EncoderException {
        URLCodec urlCodec = new URLCodec();
        url.append(url.indexOf("?") < 0 ? "?" : "&");
        if (seatNumber != null) {
            url.append(ParameterMapping.SEAT_NUMBER.getQueryParameter()).append("=").append(seatNumber);
        }
        if (privateKey != null) {
            if (seatNumber != null) url.append("&");
            url.append(ParameterMapping.MASTER_KEY.getQueryParameter()).append("=").append(urlCodec.encode(privateKey));
        }
        if (seatNumber != null || privateKey != null) {
            url.append("&");
        }
        url.append(ParameterMapping.ITEM_ID.getQueryParameter()).append("=").append(urlCodec.encode(itemUploadId));
        return url;
    }

    public static StringBuilder appendWSPublicParameters(StringBuilder url, String itemUploadId, String wsAPIKey, String wsPrivateKey, String username, String password) throws Exception {
        URLCodec urlCodec = new URLCodec();
        long time = DateHelper.getCurrentTime();
        url.append(url.indexOf("?") < 0 ? "?" : "&");
        if (wsAPIKey != null && wsPrivateKey != null) {
            url//
                    .append(ParameterMapping.API_KEY.getQueryParameter()).append("=").append(urlCodec.encode(wsAPIKey))//
                    .append("&")//
                    .append(ParameterMapping.API_HMAC.getQueryParameter()).append("=").append(urlCodec.encode(makeHMAC(wsAPIKey, wsPrivateKey, time)));
        }
        if (username != null && password != null) {
            if (wsAPIKey != null && wsPrivateKey != null) {
                url.append("&");
            }
            url//
                    .append(ParameterMapping.LOGIN.getQueryParameter()).append("=").append(urlCodec.encode(username))//
                    .append("&")//
                    .append(ParameterMapping.LOGIN_HMAC.getQueryParameter()).append("=").append(urlCodec.encode(makeHMAC(username, password, time)));
        }
        if ((username != null && password != null) || (wsAPIKey != null && wsPrivateKey != null)) {
            url//
                    .append("&")//
                    .append(ParameterMapping.TIMESTAMP.getQueryParameter()).append("=")//
                    .append(time)//
                    .append("&");
        }
        url.append(ParameterMapping.ITEM_ID.getQueryParameter()).append("=").append(urlCodec.encode(itemUploadId));
        return url;
    }
}
