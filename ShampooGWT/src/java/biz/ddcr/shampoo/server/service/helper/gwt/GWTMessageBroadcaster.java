/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper.gwt;

import biz.ddcr.shampoo.client.form.feedback.FeedbackFormInterface;
import biz.ddcr.shampoo.client.form.feedback.FeedbackMessageForm;
import biz.ddcr.shampoo.client.form.feedback.FeedbackObjectForm;
import biz.ddcr.shampoo.server.domain.feedback.Feedback;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackMessage;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackObject;
import biz.ddcr.shampoo.server.helper.MarshallUtil;
import biz.ddcr.shampoo.server.service.security.SecurityManager;
import java.io.IOException;
import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.atmosphere.cache.UUIDBroadcasterCache;
import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.config.service.Disconnect;
import org.atmosphere.config.service.ManagedService;
import org.atmosphere.config.service.Ready;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.MetaBroadcaster;
import org.atmosphere.gwt20.server.GwtRpcInterceptor;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.HeartbeatInterceptor;
import org.atmosphere.interceptor.IdleResourceInterceptor;

/**
 * Reverse AJAX broadcaster for the GWT GUI
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
@ManagedService(path = "/",
        broadcasterCache = UUIDBroadcasterCache.class,
        interceptors = {
            AtmosphereResourceLifecycleInterceptor.class,
            GwtRpcInterceptor.class,
            IdleResourceInterceptor.class,
            HeartbeatInterceptor.class,
            TrackMessageSizeInterceptor.class
        })
public class GWTMessageBroadcaster {

    protected final Log logger = LogFactory.getLog(getClass());    
    
    /**
     * Mark a resource as being currently connected via the GWT GUI
     */
    @Ready
    public void onReady(AtmosphereResource resource) throws IOException {
        if (resource != null && resource.getRequest()!=null) {
            try {
                final String connectedUser = SecurityManager.getCurrentlyAuthenticatedUsernameFromSession(resource.getRequest().getSession(false));
                if (connectedUser!=null)
                    BroadcasterFactory.getDefault().lookup("/"+MarshallUtil.escapeText(connectedUser), true).addAtmosphereResource(resource);
            } catch (Exception ex) {
                logger.error("Unavailable session for comet", ex);
            }
        }
    }

    /**
     * Unmark a comet resource from being currently connected via the GWT GUI *
     */
    @Disconnect
    public void onDisconnect(final AtmosphereResourceEvent event) throws IOException {
        if ((event.isClosedByApplication() || event.isClosedByClient()) && event.getResource()!=null && event.getResource().getRequest()!=null) {
            try {
                final String connectedUser = SecurityManager.getCurrentlyAuthenticatedUsernameFromSession(event.getResource().getRequest().getSession(false));
                if (connectedUser!=null)
                    BroadcasterFactory.getDefault().remove("/"+MarshallUtil.escapeText(connectedUser));
            } catch (Exception ex) {
                logger.error("Unavailable session for comet", ex);
            }
        }
    }
    
    public static void send(final List<Feedback> messages) throws Exception {
        if (messages != null) {
            int sequenceIndex = 0;
            for (Feedback message : messages) {
                String targetedUser = message.getRecipientId();
                FeedbackFormInterface serializableMessage = toDTO(message, ++sequenceIndex);
                
                MetaBroadcaster.getDefault().broadcastTo("/"+(targetedUser == null ? "*" : MarshallUtil.escapeText(targetedUser)), serializableMessage);                
            }
        }
    }

    public static void send(final Feedback message) throws Exception {
        if (message != null) {
            send(Collections.singletonList(message));
        }
    }

    private static FeedbackFormInterface toDTO(final Feedback message, final int sequenceIndex) {
        if (message != null) {
            if (message instanceof FeedbackMessage) {
                return new FeedbackMessageForm(//
                        //TODO Dirty but quick; must implement a real DTO
                        FeedbackMessageForm.TYPE.valueOf(((FeedbackMessage) message).getType().name()),//
                        ((FeedbackMessage) message).getText(),//
                        ((FeedbackMessage) message).getSenderId(),//
                        message.getTimestamp(),//
                        sequenceIndex);
            } else if (message instanceof FeedbackObject) {
                return new FeedbackObjectForm(//
                        //TODO Dirty but quick; must implement a real DTO
                        FeedbackObjectForm.TYPE.valueOf(((FeedbackObject) message).getType().name()),//
                        FeedbackObjectForm.FLAG.valueOf(((FeedbackObject) message).getFlag().name()),//
                        ((FeedbackObject) message).getObjectId(),//
                        ((FeedbackObject) message).getSenderId(),//
                        message.getTimestamp(),//
                        sequenceIndex);
            }
        }
        return null;
    }
}
