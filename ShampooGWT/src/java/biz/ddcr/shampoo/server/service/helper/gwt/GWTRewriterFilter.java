/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.service.helper.gwt;

import biz.ddcr.shampoo.server.helper.GenericRewriterFilter;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class GWTRewriterFilter extends GenericRewriterFilter {

    private SystemConfigurationHelper systemConfigurationHelper;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    @Override
    public String getNewContextPath() {
        return getSystemConfigurationHelper().getGWTContextPath();
    }    
    
    @Override
    public void onPreRewrite(ServletRequest request, ServletResponse response) throws IOException, ServletException {      
        //Fill in the necessary URLs for automatic link generations when not using reverse-proxy configurations
        if (getSystemConfigurationHelper().getHostAndDeploymentGWTURL()==null || getSystemConfigurationHelper().getHostAndDeploymentGWTURL().length()==0) getSystemConfigurationHelper().setHostAndDeploymentGWTURL((HttpServletRequest)request);
    }

    @Override
    public void onRewrite(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //Do nothing
    }

    @Override
    public void onPostRewrite(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //Do nothing
    }

}
