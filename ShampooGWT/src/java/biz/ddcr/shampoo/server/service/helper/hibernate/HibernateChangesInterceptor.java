/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.helper.hibernate;

import biz.ddcr.shampoo.server.domain.DomainVisitor;
import biz.ddcr.shampoo.server.domain.GenericEntityInterface;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.Report;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackObject;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackObject.FLAG;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.domain.track.filter.Filter;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.webservice.Hit;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.service.helper.MessageBroadcaster;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.io.Serializable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class HibernateChangesInterceptor extends EmptyInterceptor {

    protected final Log logger = LogFactory.getLog(getClass());
    
    private SecurityManagerInterface securityManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }
    
    protected void message(Object entity, final FeedbackObject.FLAG flag) {
        if (ProxiedClassUtil.castableAs(entity, GenericEntityInterface.class)){
            GenericEntityInterface persistableEntity = (GenericEntityInterface)entity;
            User sender = null;
            try {
                sender = getSecurityManager().getCurrentlyAuthenticatedUser();
            } catch (Exception ex) {
                logger.info("Detached session: no sender for feedback message");
            }
            final String senderId = sender!=null ? sender.getUsername() : null;
            DomainVisitor visitor = new DomainVisitor() {

                @Override
                public void visit(Archive object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.archive,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(Channel object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.channel,object.getLabel(),flag,senderId));
                }

                @Override
                public void visit(biz.ddcr.shampoo.server.domain.journal.Log object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.journal,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(Notification object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.notification,object.getRefID(),flag,senderId,object.getRecipient()!=null?object.getRecipient().getUsername():null));
                }

                @Override
                public void visit(Playlist object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.playlist,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(Programme object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.programme,object.getLabel(),flag,senderId));
                }

                @Override
                public void visit(QueueItem object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.queueitem,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(TimetableSlot object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.timetableslot,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(BroadcastableTrack object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.broadcasttrack,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(PendingTrack object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.pendingtrack,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(PlaylistEntry object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.playlistentry,object.getRefID(),flag,senderId));
                    /*if (object.getPlaylist()!=null)
                        MessageBroadcaster.add(new FeedbackObject(TYPE.playlist,object.getPlaylist().getRefID(),FLAG.edit,senderId));*/
                }

                @Override
                public void visit(StreamItem object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.streamitem,object.getRefID(),flag,senderId));
                }

                @Override
                public void visit(Filter object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.trackfilter,object.getRefID(),flag,senderId));
                    /*if (object.getEntry()!=null && object.getEntry().getPlaylist()!=null)
                        MessageBroadcaster.add(new FeedbackObject(TYPE.playlist,object.getEntry().getPlaylist().getRefID(),FLAG.edit,senderId));*/
                }

                @Override
                public void visit(Request object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.request,object.getRefID(),flag,senderId));                    
                    /*if (object.getLocation().getSong()!=null)
                        MessageBroadcaster.add(new FeedbackObject(TYPE.broadcasttrack,object.getLocation().getSong().getRefID(),FLAG.edit,senderId));*/
                }

                @Override
                public void visit(Voting object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.vote,object.getRefID(),flag,senderId));
                    //Edition doesn't automatically touch the following entities as per the Hibernate mapping but should still produce notifications
                    if (flag==FeedbackObject.FLAG.edit && object.getLocation().getSong()!=null)
                        MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.broadcasttrack,object.getLocation().getSong().getRefID(),FeedbackObject.FLAG.edit,senderId));
                }

                @Override
                public void visit(User object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.user,object.getUsername(),flag,senderId));
                }

                @Override
                public void visit(Webservice object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.webservice,object.getApiKey(),flag,senderId));
                }

                @Override
                public void visit(Hit object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.hit,object.getWebservice()!=null?object.getWebservice().getApiKey():null,flag,senderId));
                }

                @Override
                public void visit(Report object) throws Exception {
                    MessageBroadcaster.add(new FeedbackObject(FeedbackObject.TYPE.report,object.getRefID(),flag,senderId));
                }
            };
            try {
                persistableEntity.acceptDomainVisit(visitor);
            } catch (Exception ex) {
                logger.warn(ex);
            }
        }
    }
    
    @Override
    public boolean onSave(Object entity,
            Serializable id,
            Object[] state,
            String[] propertyNames,
            Type[] types)
            throws CallbackException {
        message(entity, FLAG.add);
        return false;
    }

    /*
     * (non-Javadoc) @see
     * org.hibernate.EmptyInterceptor#onDelete(java.lang.Object,
     * java.io.Serializable, java.lang.Object[], java.lang.String[],
     * org.hibernate.type.Type[])
     */
    @Override
    public void onDelete(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {
        message(entity, FLAG.delete);
    }

    @Override
    public boolean onFlushDirty(Object entity,
            Serializable id,
            Object[] currentState,
            Object[] previousState,
            String[] propertyNames,
            Type[] types)
            throws CallbackException {
        message(entity, FLAG.edit);
        return false;
    }
}
