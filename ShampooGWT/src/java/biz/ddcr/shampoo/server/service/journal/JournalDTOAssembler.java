/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
  *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.journal;

import biz.ddcr.shampoo.client.form.archive.log.ArchiveLogForm;
import biz.ddcr.shampoo.client.form.archive.log.ReportLogForm;
import biz.ddcr.shampoo.client.form.channel.CHANNEL_ACTION;
import biz.ddcr.shampoo.client.form.channel.log.ChannelLogForm;
import biz.ddcr.shampoo.client.form.journal.ExceptionLogForm;
import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.playlist.log.PlaylistLogForm;
import biz.ddcr.shampoo.client.form.programme.log.ProgrammeLogForm;
import biz.ddcr.shampoo.client.form.queue.log.QueueLogForm;
import biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm;
import biz.ddcr.shampoo.client.form.track.log.TrackLogForm;
import biz.ddcr.shampoo.client.form.user.log.ProfileLogForm;
import biz.ddcr.shampoo.client.form.user.log.UserLogForm;
import biz.ddcr.shampoo.client.form.webservice.log.WebserviceLogForm;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.archive.log.ArchiveLog;
import biz.ddcr.shampoo.server.domain.archive.log.ReportLog;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.log.ChannelLog;
import biz.ddcr.shampoo.server.domain.journal.ExceptionLog;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.journal.LogVisitor;
import biz.ddcr.shampoo.server.domain.playlist.log.PlaylistLog;
import biz.ddcr.shampoo.server.domain.programme.log.ProgrammeLog;
import biz.ddcr.shampoo.server.domain.queue.log.QueueLog;
import biz.ddcr.shampoo.server.domain.timetable.log.TimetableLog;
import biz.ddcr.shampoo.server.domain.track.log.TrackLog;
import biz.ddcr.shampoo.server.domain.user.log.ProfileLog;
import biz.ddcr.shampoo.server.domain.user.log.UserLog;
import biz.ddcr.shampoo.server.domain.webservice.log.WebserviceLog;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class JournalDTOAssembler extends GenericService implements JournalDTOAssemblerInterface {

    /** dummy marker class for form export **/
    private interface LogVisitorDTO extends LogVisitor {

        public LogForm getForm();
    };
    private SecurityManagerInterface securityManager;
    private ChannelManagerInterface channelManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public static biz.ddcr.shampoo.client.form.journal.ExceptionLogForm.EXCEPTION_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.journal.ExceptionLog.EXCEPTION_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case data:
                    return biz.ddcr.shampoo.client.form.journal.ExceptionLogForm.EXCEPTION_LOG_OPERATION.data;
                case io:
                    return biz.ddcr.shampoo.client.form.journal.ExceptionLogForm.EXCEPTION_LOG_OPERATION.io;
                case general:
                    return biz.ddcr.shampoo.client.form.journal.ExceptionLogForm.EXCEPTION_LOG_OPERATION.general;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.user.log.UserLogForm.USER_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.user.log.UserLog.USER_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.user.log.UserLogForm.USER_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.user.log.UserLogForm.USER_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.user.log.UserLogForm.USER_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.user.log.ProfileLogForm.PROFILE_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.user.log.ProfileLog.PROFILE_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.user.log.ProfileLogForm.PROFILE_LOG_OPERATION.edit;
                case login:
                    return biz.ddcr.shampoo.client.form.user.log.ProfileLogForm.PROFILE_LOG_OPERATION.login;
                case logout:
                    return biz.ddcr.shampoo.client.form.user.log.ProfileLogForm.PROFILE_LOG_OPERATION.logout;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.track.log.TrackLog.TRACK_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION.add;
                case reject:
                    return biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION.reject;
                case validate:
                    return biz.ddcr.shampoo.client.form.track.log.TrackLogForm.TRACK_LOG_OPERATION.validate;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.timetable.log.TimetableLog.TIMETABLE_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.add;
                case clone:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.clone;
                case shift:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.shift;
                case resize:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.shift;
                case cancel:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.cancel;
                case confirm:
                    return biz.ddcr.shampoo.client.form.timetable.log.TimetableLogForm.TIMETABLE_LOG_OPERATION.confirm;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.archive.log.ArchiveLogForm.ARCHIVE_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.archive.log.ArchiveLog.ARCHIVE_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.archive.log.ArchiveLogForm.ARCHIVE_LOG_OPERATION.delete;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.queue.log.QueueLogForm.QUEUE_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.queue.log.QueueLog.QUEUE_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.queue.log.QueueLogForm.QUEUE_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.queue.log.QueueLogForm.QUEUE_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }
    
    public static biz.ddcr.shampoo.client.form.programme.log.ProgrammeLogForm.PROGRAMME_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.log.ProgrammeLog.PROGRAMME_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.programme.log.ProgrammeLogForm.PROGRAMME_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.log.ProgrammeLogForm.PROGRAMME_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.log.ProgrammeLogForm.PROGRAMME_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.playlist.log.PlaylistLogForm.PLAYLIST_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.playlist.log.PlaylistLog.PLAYLIST_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.playlist.log.PlaylistLogForm.PLAYLIST_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.playlist.log.PlaylistLogForm.PLAYLIST_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.playlist.log.PlaylistLogForm.PLAYLIST_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.log.ChannelLogForm.CHANNEL_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.log.ChannelLog.CHANNEL_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.channel.log.ChannelLogForm.CHANNEL_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.log.ChannelLogForm.CHANNEL_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.channel.log.ChannelLogForm.CHANNEL_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.webservice.log.WebserviceLogForm.WEBSERVICE_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.webservice.log.WebserviceLog.WEBSERVICE_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.webservice.log.WebserviceLogForm.WEBSERVICE_LOG_OPERATION.edit;
                case delete:
                    return biz.ddcr.shampoo.client.form.webservice.log.WebserviceLogForm.WEBSERVICE_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.webservice.log.WebserviceLogForm.WEBSERVICE_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.archive.log.ReportLogForm.REPORT_LOG_OPERATION toDTO(biz.ddcr.shampoo.server.domain.archive.log.ReportLog.REPORT_LOG_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.archive.log.ReportLogForm.REPORT_LOG_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.archive.log.ReportLogForm.REPORT_LOG_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }
    
    @Override
    public ActionCollectionEntry<? extends LogForm> toDTO(Log log) throws Exception {
        if (log == null) {
            return null;
        }

        LogVisitorDTO logVisitor = new LogVisitorDTO() {

            LogForm logModule = null;

            @Override
            public LogForm getForm() {
                return logModule;
            }

            @Override
            public void visit(ExceptionLog log) throws Exception {
                logModule = new ExceptionLogForm(log.getExceptionCaption());
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(UserLog log) throws Exception {
                logModule = new UserLogForm();
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(ProfileLog log) throws Exception {
                logModule = new ProfileLogForm();
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(TimetableLog log) throws Exception {
                logModule = new TimetableLogForm(
                        log.getSlotStartTime() != null ? DateDTOAssembler.toJS(log.getSlotStartTime()) : null,
                        log.getSlotChannelCaption());
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(TrackLog log) throws Exception {
                logModule = new TrackLogForm(
                        log.getTrackAuthorCaption(),
                        log.getTrackTitleCaption());
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(ProgrammeLog log) throws Exception {
                logModule = new ProgrammeLogForm();
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(PlaylistLog log) throws Exception {
                logModule = new PlaylistLogForm(log.getPlaylistCaption());
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(ChannelLog log) throws Exception {
                logModule = new ChannelLogForm();
                logModule.setAction(toDTO(log.getAction()));
            }
            
            @Override
            public void visit(ArchiveLog log) throws Exception {
                logModule = new ArchiveLogForm(
                        log.getArchiveStartTime() != null ? DateDTOAssembler.toJS(log.getArchiveStartTime()) : null,
                        log.getArchiveChannelCaption());
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(QueueLog log) throws Exception {
                logModule = new QueueLogForm(
                        log.getQueueItemStartTime() != null ? DateDTOAssembler.toJS(log.getQueueItemStartTime()) : null,
                        log.getQueueItemChannelCaption());
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(WebserviceLog log) throws Exception {
                logModule = new WebserviceLogForm();
                logModule.setAction(toDTO(log.getAction()));
            }

            @Override
            public void visit(ReportLog log) throws Exception {
                logModule = new ReportLogForm(
                        log.getReportStartTime() != null ? DateDTOAssembler.toJS(log.getReportStartTime()) : null,
                        log.getReportEndTime() != null ? DateDTOAssembler.toJS(log.getReportEndTime()) : null,
                        log.getReportChannelCaption()
                        );
                logModule.setAction(toDTO(log.getAction()));
            }

        };
        log.acceptVisit(logVisitor);

        if (logVisitor.getForm() != null) {
            //Fill in common properties
            logVisitor.getForm().setRefID(log.getRefID());
            logVisitor.getForm().setActee(log.getActeeID());
            logVisitor.getForm().setActor(log.getActorID());
            //Actions handled w\in the vivitor pattern, along with form instantiation
            logVisitor.getForm().setTime(DateDTOAssembler.toJS(log.getDateStamp()));
            //Channels
            ActionMap<String> channelEntries = new ActionSortedMap<String>();
            if (log.getChannels() != null) {
                for (Channel channel : log.getChannels()) {
                    channelEntries.set(channel.getLabel(), decorateEntryWithActions(channel));
                }
            }
            logVisitor.getForm().setChannels(channelEntries);

            return new ActionMapEntry<LogForm>(logVisitor.getForm(), decorateEntryWithActions(log));
        } else {
            return null;
        }
    }


    protected Collection<ACTION> decorateEntryWithActions(Log log) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getSecurityManager().checkAccessViewJournal(log)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getSecurityManager().checkAccessDeleteJournal(log)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Channel channel) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewChannel(channel)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(channel)) {
                actions.add(CHANNEL_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateChannel(channel)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteChannel(channel)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    @Override
    public ActionCollection<? extends LogForm> toDTO(Collection<Log> logs, boolean doSort) throws Exception {
        ActionMap<LogForm> logModules = doSort ? new ActionSortedMap<LogForm>() : new ActionUnsortedMap<LogForm>();
        for (Log log : logs) {
            logModules.set(toDTO(log));
        }
        return logModules;
    }
}
