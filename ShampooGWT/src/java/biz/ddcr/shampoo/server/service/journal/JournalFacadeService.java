/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.journal;

import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.journal.LogForm.LOG_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class JournalFacadeService extends GenericService implements JournalFacadeServiceInterface {

    private JournalDTOAssemblerInterface journalDTOAssembler;
    private SecurityManagerInterface securityManager;

    public JournalDTOAssemblerInterface getJournalDTOAssembler() {
        return journalDTOAssembler;
    }

    public void setJournalDTOAssembler(JournalDTOAssemblerInterface journalDTOAssembler) {
        this.journalDTOAssembler = journalDTOAssembler;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void deleteSecuredLog(String id) throws Exception {

        if (id != null) {

            Log log = getSecurityManager().getJournal(id);
            //Then check if it can be accessed

            if (getSecurityManager().checkAccessDeleteJournal(log)) {

                //Whatever happens, someone who can remove a log can unlink other stuff bound to it: no need for extra check
                //Cascade delete will take care of their unlinking
                log.clearChannels();

                //Everything's fine, now make it persistent in the database
                getSecurityManager().deleteJournal(log);
                //We don' log logs

            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredLogs(Collection<String> ids) throws Exception {
        if (ids != null) {

            Collection logsToDelete = new HashSet<Log>();
            for (String id : ids) {
                //First get a copy of the entity from a DTO
                Log log = getSecurityManager().getJournal(id);

                if (getSecurityManager().checkAccessDeleteJournal(log)) {

                    log.clearChannels();

                    logsToDelete.add(log);
                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!logsToDelete.isEmpty()) {

                getSecurityManager().deleteJournals(logsToDelete);
                //We don't log logs

            }
        }
    }

    @Override
    public ActionCollectionEntry<? extends LogForm> getSecuredLog(String id) throws Exception {
        Log log = getSecurityManager().loadJournal(id);
        ActionCollectionEntry<? extends LogForm> logModuleEntry = journalDTOAssembler.toDTO(log);
        if (logModuleEntry == null || logModuleEntry.isReadable()) {
            //We don't log logs
            return logModuleEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<? extends LogForm> getSecuredLogs(GroupAndSort<LOG_TAGS> constraint) throws Exception {
        Collection<Log> logs = getSecurityManager().getFilteredJournals(constraint);
        ActionCollection<? extends LogForm> forms = journalDTOAssembler.toDTO(logs, constraint==null || constraint.getSortByFeature()==null);
        //We dont log logs
        return forms;
    }

}
