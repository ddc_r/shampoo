/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.journal;


import biz.ddcr.shampoo.client.form.journal.LogForm;
import biz.ddcr.shampoo.client.form.journal.LogForm.LOG_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import java.util.Collection;



/**
 *
 * @author okay_awright
 **/
public interface JournalFacadeServiceInterface {

    public void deleteSecuredLog(String id) throws Exception;
    public void deleteSecuredLogs(Collection<String> ids) throws Exception;

    public ActionCollectionEntry<? extends LogForm> getSecuredLog(String id) throws Exception;
    public ActionCollection<? extends LogForm> getSecuredLogs(GroupAndSort<LOG_TAGS> constraint) throws Exception;

}
