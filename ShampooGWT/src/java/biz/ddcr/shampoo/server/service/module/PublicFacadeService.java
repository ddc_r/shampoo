/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.module;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.webservice.Webservice.MODULE;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalStreamableItemMetadataInterface;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.helper.PublicFacadeCachingHelper;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainer;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.helper.WebserviceAuthenticationService;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PublicFacadeService implements PublicFacadeServiceInterface {

    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private TrackManagerInterface trackManager;
    private PublicMetadataDTOAssemblerInterface publicMetadataDTOAssembler;
    private DatastoreService datastoreService;
    private WebserviceAuthenticationService webserviceAuthenticationService;
    private SystemConfigurationHelper systemConfigurationHelper;
    //cache
    private PublicFacadeCachingHelper cache;

    public PublicFacadeCachingHelper getCache() {
        return cache;
    }

    public void setCache(PublicFacadeCachingHelper cache) {
        this.cache = cache;
    }

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public PublicMetadataDTOAssemblerInterface getPublicMetadataDTOAssembler() {
        return publicMetadataDTOAssembler;
    }

    public void setPublicMetadataDTOAssembler(PublicMetadataDTOAssemblerInterface publicMetadataDTOAssembler) {
        this.publicMetadataDTOAssembler = publicMetadataDTOAssembler;
    }

    public WebserviceAuthenticationService getWebserviceAuthenticationService() {
        return webserviceAuthenticationService;
    }

    public void setWebserviceAuthenticationService(WebserviceAuthenticationService webserviceAuthenticationService) {
        this.webserviceAuthenticationService = webserviceAuthenticationService;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    @Override
    public PictureStream fetchCoverArtStreamFromDataStore(String remoteIP, String apiKey, String hmac, long timestamp, String itemId) throws Exception {

        //Then check if the call is legitimate
        Webservice webservice = getChannelManager().getWebservice(apiKey);
        if (getWebserviceAuthenticationService().checkAccessPublicResource(webservice, timestamp, MODULE.download, hmac, remoteIP)) {
            Track track = getTrackManager().getTrack(itemId);
            return getDatastoreService().getPictureStreamFromDataStore(track);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public PictureStream fetchFlyerStreamFromDataStore(String remoteIP, String apiKey, String hmac, long timestamp, String itemId) throws Exception {

        //Check then if the call is legitimate
        Webservice webservice = getChannelManager().getWebservice(apiKey);
        if (getWebserviceAuthenticationService().checkAccessPublicResource(webservice, timestamp, MODULE.download, hmac, remoteIP)) {
            Playlist playlist = getProgrammeManager().getPlaylist(itemId);
            return getDatastoreService().getPictureStreamFromDataStore(playlist);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public GenericStreamableMetadataContainerInterface<MinimalStreamableItemMetadataInterface> fetchNowPlaying(String remoteIP, String apiKey, String hmac, long timestamp, final SERIALIZATION_METADATA_FORMAT metadataFormat) throws Exception {

        //Then check if the call is legitimate
        final Webservice webservice = getChannelManager().getWebservice(apiKey);
        if (getWebserviceAuthenticationService().checkAccessPublicResource(webservice, timestamp, MODULE.nowPlaying, hmac, remoteIP)) {

            return getCache().new CacheInspector<GenericStreamableMetadataContainer<MinimalStreamableItemMetadataInterface>>( 
                 
                "fetchNowPlaying#"+metadataFormat+"#"+webservice.getChannel().getLabel()){
                @Override
                public GenericStreamableMetadataContainer<MinimalStreamableItemMetadataInterface> getMissed() throws Exception {

                    return new GenericStreamableMetadataContainer<MinimalStreamableItemMetadataInterface>(
                            metadataFormat,
                            getPublicMetadataDTOAssembler().toDTO(webservice, webservice.getChannel().getStreamItem()));

                }
            }.find();

        } else {
            throw new AccessDeniedException();
        }

    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface>> fetchComingNext(String remoteIP, String apiKey, String hmac, long timestamp, final SERIALIZATION_METADATA_FORMAT metadataFormat, Integer maxNumberOfItems) throws Exception {

        //Then check if the call is legitimate
        final Webservice webservice = getChannelManager().getWebservice(apiKey);
        if (getWebserviceAuthenticationService().checkAccessPublicResource(webservice, timestamp, MODULE.comingNext, hmac, remoteIP)) {

            final int maxItems = (maxNumberOfItems == null || maxNumberOfItems < 1) ? getSystemConfigurationHelper().getPublicWSComingNextMaxItems() : Math.max(maxNumberOfItems, getSystemConfigurationHelper().getPublicWSComingNextMaxItems());

            return getCache().new CacheInspector<GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface>>>( 
                 
                "fetchComingNext#"+metadataFormat+"#"+webservice.getChannel().getLabel()+"#"+maxItems){

                @Override
                public GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface>> getMissed() throws Exception {
                    final Collection<QueueItem> nextItems = new ArrayList<QueueItem>();
                    int _maxItems = maxItems;
                    for (final QueueItem item : webservice.getChannel().getQueueItems()) {
                        if (_maxItems-- < 1) {
                            break;
                        }
                        nextItems.add(item);
                    }
                    return new GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface>>(
                            metadataFormat,
                            getPublicMetadataDTOAssembler().toQueueItemDTO(webservice, nextItems));
                }
            }.find();

        } else {
            throw new AccessDeniedException();
        }

    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> fetchHistory(String remoteIP, String apiKey, String hmac, long timestamp, final SERIALIZATION_METADATA_FORMAT metadataFormat, Integer maxNumberOfItems) throws Exception {

        //Then check if the call is legitimate
        final Webservice webservice = getChannelManager().getWebservice(apiKey);
        if (getWebserviceAuthenticationService().checkAccessPublicResource(webservice, timestamp, MODULE.history, hmac, remoteIP)) {

            final int maxItems = (maxNumberOfItems == null || maxNumberOfItems < 1) ? getSystemConfigurationHelper().getPublicWSArchiveMaxItems() : Math.max(maxNumberOfItems, getSystemConfigurationHelper().getPublicWSArchiveMaxItems());

            return getCache().new CacheInspector<GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>>>( 
                "fetchHistory#"+metadataFormat+"#"+webservice.getChannel().getLabel()+"#"+maxItems){

                @Override
                public GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> getMissed() throws Exception {
                    return new GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>>(
                            metadataFormat,
                            getPublicMetadataDTOAssembler().toArchiveDTO(webservice, getChannelManager().getFreshestArchives(webservice.getChannel(), maxItems)));
                }
            }.find();

        } else {
            throw new AccessDeniedException();
        }

    }
}
