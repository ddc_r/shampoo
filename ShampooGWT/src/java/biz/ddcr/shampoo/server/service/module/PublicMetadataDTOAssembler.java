/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.module;

import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.queue.BlankQueueItem;
import biz.ddcr.shampoo.server.domain.queue.LiveQueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.RequestQueueItem;
import biz.ddcr.shampoo.server.domain.queue.SimpleTrackQueueItem;
import biz.ddcr.shampoo.server.domain.queue.TrackQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.BlankQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueVisitor;
import biz.ddcr.shampoo.server.domain.streamer.RequestQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.SimpleTrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemVisitor;
import biz.ddcr.shampoo.server.domain.streamer.TrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalSimpleStreamableBlankQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalSimpleStreamableLiveGenericItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalSimpleStreamableLiveQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalSimpleStreamableTrackQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalStreamableItemMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalStreamableLiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableChannelGenericMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableChannelMetadataInterface;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.CommonTagInterface;
import biz.ddcr.shampoo.server.domain.track.PEGIRating;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataCollection;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.archive.ArchiveDTOAssembler;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

/**
 *
 * @author okay_awright
 **/
public class PublicMetadataDTOAssembler extends GenericService implements PublicMetadataDTOAssemblerInterface {

    private interface FloatTrackVisitor extends TrackVisitor {
        public Float get();
    }

    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    private interface QueueItemConverterVisitor extends QueueItemVisitor {

        public MinimalStreamableItemMetadataInterface convert();
    }
    private interface StreamItemConverterVisitor extends StreamItemVisitor {

        public MinimalStreamableItemMetadataInterface convert();
    }

    private interface NonPersistentNonQueueItemConverterVisitor extends NonPersistableNonQueueVisitor {

        public MinimalStreamableItemMetadataInterface convert();
    }

    @Override
    public MinimalStreamableItemMetadataInterface toDTO(final Webservice ws, StreamItem streamItem) throws Exception {
        if (streamItem != null) {
            StreamItemConverterVisitor streamItemConverterVisitor = new StreamItemConverterVisitor() {

                MinimalStreamableItemMetadataInterface streamableItemMetadata;

                @Override
                public MinimalStreamableItemMetadataInterface convert() {
                    return streamableItemMetadata;
                }

                @Override
                public void visit(LiveNonQueueStreamItem streamItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, streamItem);
                }

                @Override
                public void visit(LiveQueueStreamItem streamItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, streamItem);
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem streamItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, (TrackQueueStreamItem)streamItem);
                }

                @Override
                public void visit(BlankQueueStreamItem streamItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, streamItem);
                }

                @Override
                public void visit(RequestQueueStreamItem streamItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, streamItem);
                }
            };
            streamItem.acceptVisit(streamItemConverterVisitor);
            return streamItemConverterVisitor.convert();
        }
        return null;
    }

    @Override
    public MinimalStreamableItemMetadataInterface toDTO(final Webservice ws, QueueItem queueItem) throws Exception {
        if (queueItem != null) {
            QueueItemConverterVisitor queueItemConverterVisitor = new QueueItemConverterVisitor() {

                MinimalStreamableItemMetadataInterface streamableItemMetadata;

                @Override
                public MinimalStreamableItemMetadataInterface convert() {
                    return streamableItemMetadata;
                }

                @Override
                public void visit(SimpleTrackQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, (TrackQueueItem)queueItem);
                }

                @Override
                public void visit(LiveQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, queueItem);
                }

                @Override
                public void visit(BlankQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, queueItem);
                }

                @Override
                public void visit(RequestQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, queueItem);
                }
            };
            queueItem.acceptVisit(queueItemConverterVisitor);
            return queueItemConverterVisitor.convert();
        }
        return null;
    }

    @Override
    public SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface> toQueueItemDTO(Webservice ws, Collection<QueueItem> queueItems) throws Exception {
        //LikedHashSet gathers the best of Set features (no null, no duplicates) and the best of List features (fixed order) at the cost of some memory overhead
        LinkedHashSet<MinimalStreamableItemMetadataInterface> queueItemsMetadata = new LinkedHashSet<MinimalStreamableItemMetadataInterface>();
        for (QueueItem queueItem : queueItems) {
            queueItemsMetadata.add(toDTO(ws, queueItem));
        }
        return SerializableMetadataCollection.wrap(queueItemsMetadata);
    }    

    @Override
    public SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface> toArchiveDTO(Webservice ws, Collection<Archive> archives) throws Exception {
        //LikedHashSet gathers the best of Set features (no null, no duplicates) and the best of List features (fixed order) at the cost of some memory overhead
        LinkedHashSet<StreamableArchiveMetadataInterface> archivesMetadata = new LinkedHashSet<StreamableArchiveMetadataInterface>();
        for (Archive archive : archives) {
            archivesMetadata.add(ArchiveDTOAssembler.toSerializableDTO(archive));
        }
        return SerializableMetadataCollection.wrap(archivesMetadata);
    }
    
    protected MinimalSimpleStreamableTrackQueueItemMetadata toDTO(Webservice ws, TrackQueueStreamItem streamItem) throws Exception {
        if (streamItem == null) {
            return null;
        }
        MinimalSimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = new MinimalSimpleStreamableTrackQueueItemMetadata();

        //Do only fill in the the likely duration if it's any different than the track full length
        simpleStreamableTrackMetadata.setAuthorizedPlayingDuration(streamItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        simpleStreamableTrackMetadata.setScheduledStartTime(streamItem.getStartTime().getUnixTime());
        simpleStreamableTrackMetadata.setTimezone(streamItem.getStartTime().getTimeZone());
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getChannel() != null) {
            Channel c = streamItem.getTimetableSlot().getChannel();
            simpleStreamableTrackMetadata.setChannelLabel(c.getLabel());
            simpleStreamableTrackMetadata.setChannelDescription(c.getDescription());
            simpleStreamableTrackMetadata.setChannelTag(c.getTag());
            simpleStreamableTrackMetadata.setChannelURL(c.getUrl());
        }
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = streamItem.getTimetableSlot().getPlaylist();
            simpleStreamableTrackMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableTrackMetadata.setPlaylistLabel(playlist.getLabel());
        }
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableTrackMetadata.setProgrammeLabel(streamItem.getTimetableSlot().getProgramme().getLabel());
            if (streamItem.getTrack() != null) {
                simpleStreamableTrackMetadata.setProgrammeRotationCount(streamItem.getTrack().getRotationCount(streamItem.getTimetableSlot().getProgramme()));
            }
            simpleStreamableTrackMetadata.setFriendlyCaptionPattern(streamItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (streamItem.getTrack() != null) {
            BroadcastableTrack track = streamItem.getTrack();
            if (ProxiedClassUtil.castableAs(track, BroadcastableSong.class)) {
                simpleStreamableTrackMetadata.setTrackType("song");
                simpleStreamableTrackMetadata.setAverageVote(ProxiedClassUtil.cast(track, BroadcastableSong.class).getAverageVote());
            } else if (ProxiedClassUtil.castableAs(track, BroadcastableJingle.class)) {
                simpleStreamableTrackMetadata.setTrackType("jingle");
            } else if (ProxiedClassUtil.castableAs(track, BroadcastableAdvert.class)) {
                simpleStreamableTrackMetadata.setTrackType("advert");
            }
            FloatTrackVisitor visitor = new FloatTrackVisitor() {

                private Float f;
                @Override
                public Float get() {
                    return f;
                }

                @Override
                public void visit(PendingAdvert track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(PendingJingle track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(PendingSong track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BroadcastableAdvert track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BroadcastableJingle track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BroadcastableSong track) throws Exception {
                    f = track.getAverageVote();
                }
            };
            track.acceptVisit(visitor);
            Float f = visitor.get();
            if (f!=null) simpleStreamableTrackMetadata.setAverageVote(f);

        }

        //Fill in the tag set now
        CommonTagInterface tagSet = null;
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getPlaylist() != null && streamItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            tagSet = streamItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
        } else if (streamItem.getTrack() != null) {
            tagSet = streamItem.getTrack();
        }
        if (tagSet != null) {
            simpleStreamableTrackMetadata.setTrackPublisher(tagSet.getPublisher());
            simpleStreamableTrackMetadata.setCopyright(tagSet.getCopyright());
            if (tagSet.getRating() != null) {
                simpleStreamableTrackMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableTrackMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableTrackMetadata.setTag(tagSet.getTag());
            simpleStreamableTrackMetadata.setTrackAlbum(tagSet.getAlbum());
            simpleStreamableTrackMetadata.setTrackTitle(tagSet.getTitle());
            simpleStreamableTrackMetadata.setTrackAuthor(tagSet.getAuthor());
            simpleStreamableTrackMetadata.setTrackDateOfRelease(tagSet.getDateOfRelease());
            simpleStreamableTrackMetadata.setTrackDescription(tagSet.getDescription());
            simpleStreamableTrackMetadata.setTrackGenre(tagSet.getGenre());
        }

        //Cover art
        //Use the playlist flyer if there's no cover art for the track
        if (streamItem.getTrack()!=null && streamItem.getTrack().getCoverArtContainer()!=null) {
            simpleStreamableTrackMetadata.setPicture(true);
            simpleStreamableTrackMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSTrackPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), streamItem.getTrack().getRefID()));
        } else if (streamItem.getTimetableSlot()!=null && streamItem.getTimetableSlot().getPlaylist()!=null && streamItem.getTimetableSlot().getPlaylist().getCoverArtContainer()!=null) {
            simpleStreamableTrackMetadata.setPicture(true);
            simpleStreamableTrackMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSPlaylistPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), streamItem.getTimetableSlot().getPlaylist().getRefID()));
        } else {
            simpleStreamableTrackMetadata.setPicture(false);
        }
        return simpleStreamableTrackMetadata;
    }

    protected MinimalSimpleStreamableTrackQueueItemMetadata toDTO(Webservice ws, TrackQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        MinimalSimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = new MinimalSimpleStreamableTrackQueueItemMetadata();

        //Do only fill in the the likely duration if it's any different than the track full length
        simpleStreamableTrackMetadata.setAuthorizedPlayingDuration(queueItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        simpleStreamableTrackMetadata.setScheduledStartTime(queueItem.getStartTime().getUnixTime());
        simpleStreamableTrackMetadata.setTimezone(queueItem.getStartTime().getTimeZone());
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getChannel() != null) {
            Channel c = queueItem.getTimetableSlot().getChannel();
            simpleStreamableTrackMetadata.setChannelLabel(c.getLabel());
            simpleStreamableTrackMetadata.setChannelDescription(c.getDescription());
            simpleStreamableTrackMetadata.setChannelTag(c.getTag());
            simpleStreamableTrackMetadata.setChannelURL(c.getUrl());
        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = queueItem.getTimetableSlot().getPlaylist();
            simpleStreamableTrackMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableTrackMetadata.setPlaylistLabel(playlist.getLabel());
        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableTrackMetadata.setProgrammeLabel(queueItem.getTimetableSlot().getProgramme().getLabel());
            if (queueItem.getItem() != null) {
                simpleStreamableTrackMetadata.setProgrammeRotationCount(queueItem.getItem().getRotationCount(queueItem.getTimetableSlot().getProgramme()));
            }
            simpleStreamableTrackMetadata.setFriendlyCaptionPattern(queueItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (queueItem.getItem() != null) {
            BroadcastableTrack track = queueItem.getItem();
            simpleStreamableTrackMetadata.setTrackID(track.getRefID());
            if (ProxiedClassUtil.castableAs(track, BroadcastableSong.class)) {
                simpleStreamableTrackMetadata.setTrackType("song");
                simpleStreamableTrackMetadata.setAverageVote(ProxiedClassUtil.cast(track, BroadcastableSong.class).getAverageVote());
            } else if (ProxiedClassUtil.castableAs(track, BroadcastableJingle.class)) {
                simpleStreamableTrackMetadata.setTrackType("jingle");
            } else if (ProxiedClassUtil.castableAs(track, BroadcastableAdvert.class)) {
                simpleStreamableTrackMetadata.setTrackType("advert");
            }
        }

        //Fill in the tag set now
        CommonTagInterface tagSet = null;
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null && queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            tagSet = queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
        } else if (queueItem.getItem() != null) {
            tagSet = queueItem.getItem();
        }
        if (tagSet != null) {
            simpleStreamableTrackMetadata.setTrackPublisher(tagSet.getPublisher());
            simpleStreamableTrackMetadata.setCopyright(tagSet.getCopyright());
            if (tagSet.getRating() != null) {
                simpleStreamableTrackMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableTrackMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableTrackMetadata.setTag(tagSet.getTag());
            simpleStreamableTrackMetadata.setTrackAlbum(tagSet.getAlbum());
            simpleStreamableTrackMetadata.setTrackTitle(tagSet.getTitle());
            simpleStreamableTrackMetadata.setTrackAuthor(tagSet.getAuthor());
            simpleStreamableTrackMetadata.setTrackDateOfRelease(tagSet.getDateOfRelease());
            simpleStreamableTrackMetadata.setTrackDescription(tagSet.getDescription());
            simpleStreamableTrackMetadata.setTrackGenre(tagSet.getGenre());
        }

        //Cover art
        //Use the playlist flyer if there's no cover art for the track
        if (queueItem.getItem()!=null && queueItem.getItem().getCoverArtContainer()!=null) {
            simpleStreamableTrackMetadata.setPicture(true);
            simpleStreamableTrackMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSTrackPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), queueItem.getItem().getRefID()));
        } else if (queueItem.getTimetableSlot()!=null && queueItem.getTimetableSlot().getPlaylist()!=null && queueItem.getTimetableSlot().getPlaylist().getCoverArtContainer()!=null) {
            simpleStreamableTrackMetadata.setPicture(true);
            simpleStreamableTrackMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSPlaylistPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), queueItem.getTimetableSlot().getPlaylist().getRefID()));
        } else {
            simpleStreamableTrackMetadata.setPicture(false);
        }
        return simpleStreamableTrackMetadata;
    }

    protected MinimalSimpleStreamableTrackQueueItemMetadata toDTO(Webservice ws, SimpleTrackQueueStreamItem streamItem) throws Exception {
        if (streamItem == null) {
            return null;
        }
        MinimalSimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = toDTO(ws, (TrackQueueStreamItem)streamItem);

        if (simpleStreamableTrackMetadata != null) {
            //Request flags
            simpleStreamableTrackMetadata.setRequested(false);
            simpleStreamableTrackMetadata.setRequestAuthor(null);
            simpleStreamableTrackMetadata.setRequestMessage(null);
        }

        return simpleStreamableTrackMetadata;
    }

    protected MinimalSimpleStreamableTrackQueueItemMetadata toDTO(Webservice ws, SimpleTrackQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        MinimalSimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = toDTO(ws, (TrackQueueItem)queueItem);

        if (simpleStreamableTrackMetadata != null) {
            //Request flags
            simpleStreamableTrackMetadata.setRequested(false);
            simpleStreamableTrackMetadata.setRequestAuthor(null);
            simpleStreamableTrackMetadata.setRequestMessage(null);
        }

        return simpleStreamableTrackMetadata;
    }

    protected MinimalSimpleStreamableTrackQueueItemMetadata toDTO(Webservice ws, RequestQueueStreamItem streamItem) throws Exception {
        if (streamItem == null) {
            return null;
        }
        MinimalSimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = toDTO(ws, (TrackQueueStreamItem)streamItem);

        if (simpleStreamableTrackMetadata != null) {
            //Request flags
            simpleStreamableTrackMetadata.setRequested(streamItem.getRequest() != null);
            if (streamItem.getRequest() != null && streamItem.getRequest().getRequester() != null) {
                simpleStreamableTrackMetadata.setRequestAuthor(streamItem.getRequest().getRequester().getUsername());
            }
            if (streamItem.getRequest() != null) {
                simpleStreamableTrackMetadata.setRequestMessage(streamItem.getRequest().getMessage());
            }
        }

        return simpleStreamableTrackMetadata;
    }

    protected MinimalSimpleStreamableTrackQueueItemMetadata toDTO(Webservice ws, RequestQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        MinimalSimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = toDTO(ws, (TrackQueueItem)queueItem);

        if (simpleStreamableTrackMetadata != null) {
            //Request flags
            simpleStreamableTrackMetadata.setRequested(queueItem.getRequest() != null);
            if (queueItem.getRequest() != null && queueItem.getRequest().getRequester() != null) {
                simpleStreamableTrackMetadata.setRequestAuthor(queueItem.getRequest().getRequester().getUsername());
            }
            if (queueItem.getRequest() != null) {
                simpleStreamableTrackMetadata.setRequestMessage(queueItem.getRequest().getMessage());
            }
        }

        return simpleStreamableTrackMetadata;
    }

    protected MinimalSimpleStreamableLiveQueueItemMetadata toDTO(Webservice ws, LiveQueueStreamItem streamItem) throws Exception {
        if (streamItem == null) {
            return null;
        }
        MinimalSimpleStreamableLiveQueueItemMetadata simpleStreamableLiveMetadata = new MinimalSimpleStreamableLiveQueueItemMetadata();

        simpleStreamableLiveMetadata.setAuthorizedPlayingDuration(streamItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        simpleStreamableLiveMetadata.setScheduledStartTime(streamItem.getStartTime().getUnixTime());
        simpleStreamableLiveMetadata.setTimezone(streamItem.getStartTime().getTimeZone());
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getChannel() != null) {
            Channel c = streamItem.getTimetableSlot().getChannel();
            simpleStreamableLiveMetadata.setChannelLabel(c.getLabel());
            simpleStreamableLiveMetadata.setChannelDescription(c.getDescription());
            simpleStreamableLiveMetadata.setChannelTag(c.getTag());
            simpleStreamableLiveMetadata.setChannelURL(c.getUrl());
        }
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = streamItem.getTimetableSlot().getPlaylist();
            simpleStreamableLiveMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableLiveMetadata.setPlaylistLabel(playlist.getLabel());

            //Cover art
            if (playlist.getCoverArtContainer()!=null) {
                simpleStreamableLiveMetadata.setPicture(true);
                simpleStreamableLiveMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSPlaylistPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), playlist.getRefID()));
            } else {
                simpleStreamableLiveMetadata.setPicture(false);
            }

        }
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableLiveMetadata.setProgrammeLabel(streamItem.getTimetableSlot().getProgramme().getLabel());
            simpleStreamableLiveMetadata.setFriendlyCaptionPattern(streamItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        //Fill in the tag set now
        if (streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getPlaylist() != null && streamItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {

            Live live = streamItem.getTimetableSlot().getPlaylist().getLive();
            if (live!=null) {
                simpleStreamableLiveMetadata.setLiveBroadcaster(live.getBroadcaster());
            }

            CommonTagInterface tagSet = streamItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
            if (tagSet.getRating() != null) {
                simpleStreamableLiveMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableLiveMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableLiveMetadata.setLiveGenre(tagSet.getGenre());
        }

        return simpleStreamableLiveMetadata;
    }

    protected MinimalSimpleStreamableLiveQueueItemMetadata toDTO(Webservice ws, LiveQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        MinimalSimpleStreamableLiveQueueItemMetadata simpleStreamableLiveMetadata = new MinimalSimpleStreamableLiveQueueItemMetadata();

        //Do only fill in the the likely duration if it's any different than the track full length
        simpleStreamableLiveMetadata.setAuthorizedPlayingDuration(queueItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        simpleStreamableLiveMetadata.setScheduledStartTime(queueItem.getStartTime().getUnixTime());
        simpleStreamableLiveMetadata.setTimezone(queueItem.getStartTime().getTimeZone());
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getChannel() != null) {
            Channel c = queueItem.getTimetableSlot().getChannel();
            simpleStreamableLiveMetadata.setChannelLabel(c.getLabel());
            simpleStreamableLiveMetadata.setChannelDescription(c.getDescription());
            simpleStreamableLiveMetadata.setChannelTag(c.getTag());
            simpleStreamableLiveMetadata.setChannelURL(c.getUrl());
        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = queueItem.getTimetableSlot().getPlaylist();
            simpleStreamableLiveMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableLiveMetadata.setPlaylistLabel(playlist.getLabel());

            //Cover art
            if (playlist.getCoverArtContainer()!=null) {
                simpleStreamableLiveMetadata.setPicture(true);
                simpleStreamableLiveMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSPlaylistPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), playlist.getRefID()));
            } else {
                simpleStreamableLiveMetadata.setPicture(false);
            }

        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableLiveMetadata.setProgrammeLabel(queueItem.getTimetableSlot().getProgramme().getLabel());
            simpleStreamableLiveMetadata.setFriendlyCaptionPattern(queueItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (queueItem.getItem() != null) {
            Live live = queueItem.getLive();
            simpleStreamableLiveMetadata.setLiveBroadcaster(live.getBroadcaster());
        }

        //Fill in the tag set now
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null && queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            CommonTagInterface tagSet = queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
            if (tagSet.getRating() != null) {
                simpleStreamableLiveMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableLiveMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableLiveMetadata.setLiveGenre(tagSet.getGenre());
        }

        return simpleStreamableLiveMetadata;
    }

    protected MinimalSimpleStreamableBlankQueueItemMetadata toDTO(Webservice ws, BlankQueueStreamItem streamItem) {
        if (streamItem == null) {
            return null;
        }
        MinimalSimpleStreamableBlankQueueItemMetadata simpleStreamableBlankMetadata = new MinimalSimpleStreamableBlankQueueItemMetadata();

        //Do only fill in the the likely duration if it's any different than the track full length
        simpleStreamableBlankMetadata.setAuthorizedPlayingDuration(streamItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        simpleStreamableBlankMetadata.setScheduledStartTime(streamItem.getStartTime().getUnixTime());
        simpleStreamableBlankMetadata.setTimezone(streamItem.getStartTime().getTimeZone());
        Channel c = streamItem.getChannel();
        simpleStreamableBlankMetadata.setChannelLabel(c.getLabel());
        simpleStreamableBlankMetadata.setChannelDescription(c.getDescription());
        simpleStreamableBlankMetadata.setChannelTag(c.getTag());
        simpleStreamableBlankMetadata.setChannelURL(c.getUrl());
        //Provide a default metaInfoPattern, since this item is not linked to any programme
        simpleStreamableBlankMetadata.setFriendlyCaptionPattern("${itemTitle}");

        return simpleStreamableBlankMetadata;
    }

    protected MinimalSimpleStreamableBlankQueueItemMetadata toDTO(Webservice ws, BlankQueueItem queueItem) {
        if (queueItem == null) {
            return null;
        }
        MinimalSimpleStreamableBlankQueueItemMetadata simpleStreamableBlankMetadata = new MinimalSimpleStreamableBlankQueueItemMetadata();

        simpleStreamableBlankMetadata.setAuthorizedPlayingDuration(queueItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        simpleStreamableBlankMetadata.setScheduledStartTime(queueItem.getStartTime().getUnixTime());
        simpleStreamableBlankMetadata.setTimezone(queueItem.getStartTime().getTimeZone());
        Channel c = queueItem.getChannel();
        simpleStreamableBlankMetadata.setChannelLabel(c.getLabel());
        simpleStreamableBlankMetadata.setChannelDescription(c.getDescription());
        simpleStreamableBlankMetadata.setChannelTag(c.getTag());
        simpleStreamableBlankMetadata.setChannelURL(c.getUrl());
        //Provide a default metaInfoPattern, since this item is not linked to any programme
        simpleStreamableBlankMetadata.setFriendlyCaptionPattern("${itemTitle}");

        return simpleStreamableBlankMetadata;
    }

    public MinimalStreamableItemMetadataInterface toDTO(final Webservice ws, NonPersistableNonQueueItem miscItem) throws Exception {
        if (miscItem != null) {
            NonPersistentNonQueueItemConverterVisitor miscQueueItemConverterVisitor = new NonPersistentNonQueueItemConverterVisitor() {

                MinimalStreamableItemMetadataInterface streamableItemMetadata;

                @Override
                public MinimalStreamableItemMetadataInterface convert() {
                    return streamableItemMetadata;
                }

                @Override
                public void visit(LiveNonPersistableNonQueueItem nonQueueItem) throws Exception {
                    streamableItemMetadata = toDTO(ws, nonQueueItem);
                }
            };
            miscItem.acceptVisit(miscQueueItemConverterVisitor);
            return miscQueueItemConverterVisitor.convert();
        }
        return null;
    }

    public MinimalSimpleStreamableLiveGenericItemMetadata toDTO(Webservice ws, LiveNonQueueStreamItem liveItem) throws Exception {
        if (liveItem == null) {
            return null;
        }
        MinimalSimpleStreamableLiveGenericItemMetadata simpleStreamableLiveMetadata = new MinimalSimpleStreamableLiveGenericItemMetadata();

        //Fill in the main attributes
        simpleStreamableLiveMetadata.setStartTime(liveItem.getStartTime().getUnixTime());
        simpleStreamableLiveMetadata.setTimezone(liveItem.getStartTime().getTimeZone());
        simpleStreamableLiveMetadata.setEndTime(liveItem.getStartTime().getUnixTime() + liveItem.getDuration());
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getChannel() != null) {
            Channel c = liveItem.getTimetableSlot().getChannel();
            simpleStreamableLiveMetadata.setChannelLabel(c.getLabel());
            simpleStreamableLiveMetadata.setChannelDescription(c.getDescription());
            simpleStreamableLiveMetadata.setChannelTag(c.getTag());
            simpleStreamableLiveMetadata.setChannelURL(c.getUrl());
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = liveItem.getTimetableSlot().getPlaylist();
            simpleStreamableLiveMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableLiveMetadata.setPlaylistLabel(playlist.getLabel());

            //Cover art
            if (playlist.getCoverArtContainer()!=null) {
                simpleStreamableLiveMetadata.setPicture(true);
                simpleStreamableLiveMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSPlaylistPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), playlist.getRefID()));
            } else {
                simpleStreamableLiveMetadata.setPicture(false);
            }
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableLiveMetadata.setProgrammeLabel(liveItem.getTimetableSlot().getProgramme().getLabel());
            simpleStreamableLiveMetadata.setFriendlyCaptionPattern(liveItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        //Fill in the tag set now
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getPlaylist() != null && liveItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {

            Live live = liveItem.getTimetableSlot().getPlaylist().getLive();
            if (live!=null)
                simpleStreamableLiveMetadata.setLiveBroadcaster(live.getBroadcaster());

            CommonTagInterface tagSet = liveItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
            if (tagSet.getRating() != null) {
                simpleStreamableLiveMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableLiveMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableLiveMetadata.setLiveGenre(tagSet.getGenre());

        }

        return simpleStreamableLiveMetadata;
    }

    public MinimalStreamableLiveMetadataInterface toDTO(Webservice ws, LiveNonPersistableNonQueueItem liveItem) throws Exception {
        if (liveItem == null) {
            return null;
        }
        MinimalSimpleStreamableLiveGenericItemMetadata simpleStreamableLiveMetadata = new MinimalSimpleStreamableLiveGenericItemMetadata();

        //Fill in the main attributes
        simpleStreamableLiveMetadata.setStartTime(liveItem.getStartTime().getUnixTime());
        simpleStreamableLiveMetadata.setTimezone(liveItem.getStartTime().getTimeZone());
        simpleStreamableLiveMetadata.setEndTime(liveItem.getStartTime().getUnixTime() + liveItem.getDuration());
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getChannel() != null) {
            Channel c = liveItem.getTimetableSlot().getChannel();
            simpleStreamableLiveMetadata.setChannelLabel(c.getLabel());
            simpleStreamableLiveMetadata.setChannelDescription(c.getDescription());
            simpleStreamableLiveMetadata.setChannelTag(c.getTag());
            simpleStreamableLiveMetadata.setChannelURL(c.getUrl());
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = liveItem.getTimetableSlot().getPlaylist();
            simpleStreamableLiveMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableLiveMetadata.setPlaylistLabel(playlist.getLabel());

            //Cover art
            if (playlist.getCoverArtContainer()!=null) {
                simpleStreamableLiveMetadata.setPicture(true);
                simpleStreamableLiveMetadata.setPictureURLEndpoint(getDatastoreService().getPublicWSPlaylistPictureDownloadURLEndpoint(ws.getApiKey(), ws.getPrivateKey(), playlist.getRefID()));
            } else {
                simpleStreamableLiveMetadata.setPicture(false);
            }
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableLiveMetadata.setProgrammeLabel(liveItem.getTimetableSlot().getProgramme().getLabel());
            simpleStreamableLiveMetadata.setFriendlyCaptionPattern(liveItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (liveItem.getLive() != null) {
            Live live = liveItem.getLive();
            simpleStreamableLiveMetadata.setLiveBroadcaster(live.getBroadcaster());
        }

        //Fill in the tag set now
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getPlaylist() != null && liveItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            CommonTagInterface tagSet = liveItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
            if (tagSet.getRating() != null) {
                simpleStreamableLiveMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableLiveMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableLiveMetadata.setLiveGenre(tagSet.getGenre());

        }

        return simpleStreamableLiveMetadata;
    }

    public StreamableChannelMetadataInterface toDTO(Webservice ws, Channel miscItem) {
        if (miscItem == null) {
            return null;
        }
        SimpleStreamableChannelGenericMetadata simpleStreamableChannelMetadata = new SimpleStreamableChannelGenericMetadata();

        //Fill in the main attributes
        simpleStreamableChannelMetadata.setChannelLabel(miscItem.getLabel());
        simpleStreamableChannelMetadata.setChannelDescription(miscItem.getDescription());
        simpleStreamableChannelMetadata.setChannelTag(miscItem.getTag());
        simpleStreamableChannelMetadata.setChannelURL(miscItem.getUrl());

        return simpleStreamableChannelMetadata;
    }

    protected String[] toStringList(PEGIRating rating) {
        if (rating != null) {

            ArrayList<String> list = new ArrayList<String>();
            if (rating.getDiscrimination()) {
                list.add("discrimination");
            }
            if (rating.getDrugs()) {
                list.add("drugs");
            }
            if (rating.getFear()) {
                list.add("fear");
            }
            if (rating.getProfanity()) {
                list.add("profanity");
            }
            if (rating.getSex()) {
                list.add("sex");
            }
            if (rating.getViolence()) {
                list.add("violence");
            }

            if (!list.isEmpty()) {
                return list.toArray(new String[list.size()]);
            }
        }
        return null;
    }
}
