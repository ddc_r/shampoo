/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.module;

import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.webservice.Webservice;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalStreamableItemMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface PublicMetadataDTOAssemblerInterface {
    
    //Domain to DTO conversion
    public MinimalStreamableItemMetadataInterface toDTO(Webservice ws, StreamItem streamItem) throws Exception;
    public MinimalStreamableItemMetadataInterface toDTO(Webservice ws, QueueItem queueItem) throws Exception;    
    public SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface> toQueueItemDTO(Webservice ws, Collection<QueueItem> queueItems) throws Exception;
    public SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface> toArchiveDTO(Webservice ws, Collection<Archive> archives) throws Exception;
    
}
