/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.notification;

import biz.ddcr.shampoo.client.form.channel.notification.*;
import biz.ddcr.shampoo.client.form.notification.NotificationForm;
import biz.ddcr.shampoo.client.form.playlist.notification.PlaylistContentNotificationForm;
import biz.ddcr.shampoo.client.form.playlist.notification.PlaylistProgrammeLinkNotificationForm;
import biz.ddcr.shampoo.client.form.playlist.notification.PlaylistTimetableSlotLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeAnimatorLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContentNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContributorLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeCuratorLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammePendingTrackLinkNotificationForm;
import biz.ddcr.shampoo.client.form.programme.notification.ProgrammeTimetableSlotLinkNotificationForm;
import biz.ddcr.shampoo.client.form.timetable.notification.TimetableSlotContentNotificationForm;
import biz.ddcr.shampoo.client.form.track.notification.BroadcastTrackContentNotificationForm;
import biz.ddcr.shampoo.client.form.track.notification.PendingTrackContentNotificationForm;
import biz.ddcr.shampoo.client.form.user.notification.RestrictedUserContentNotificationForm;
import biz.ddcr.shampoo.client.form.webservice.notification.WebserviceContentNotificationForm;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.channel.notification.*;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.domain.notification.NotificationVisitor;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistContentNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContentNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.streamer.notification.StreamerStatusNotification;
import biz.ddcr.shampoo.server.domain.timetable.notification.TimetableContentNotification;
import biz.ddcr.shampoo.server.domain.track.notification.BroadcastableTrackContentNotification;
import biz.ddcr.shampoo.server.domain.track.notification.PendingTrackContentNotification;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification;
import biz.ddcr.shampoo.server.domain.webservice.notification.WebserviceContentNotification;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class NotificationDTOAssembler extends GenericService implements NotificationDTOAssemblerInterface {

    /** dummy marker class for form export **/
    private interface NotificationVisitorDTO extends NotificationVisitor {

        public NotificationForm getForm();
    };
    private SecurityManagerInterface securityManager;
    private ChannelManagerInterface channelManager;
    private UserManagerInterface userManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelChannelAdministratorLinkNotificationForm.CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelChannelAdministratorLinkNotification.CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelChannelAdministratorLinkNotificationForm.CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.add;
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelChannelAdministratorLinkNotificationForm.CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelContentNotificationForm.CHANNEL_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelContentNotification.CHANNEL_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelContentNotificationForm.CHANNEL_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelListenerLinkNotificationForm.CHANNELLISTENER_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification.CHANNELLISTENER_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelListenerLinkNotificationForm.CHANNELLISTENER_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelListenerLinkNotificationForm.CHANNELLISTENER_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelProgrammeLinkNotificationForm.CHANNELPROGRAMME_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeLinkNotification.CHANNELPROGRAMME_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelProgrammeLinkNotificationForm.CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelProgrammeLinkNotificationForm.CHANNELPROGRAMME_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelProgrammeManagerLinkNotificationForm.CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeManagerLinkNotification.CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelProgrammeManagerLinkNotificationForm.CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelProgrammeManagerLinkNotificationForm.CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelTimetableSlotLinkNotificationForm.CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelTimetableSlotLinkNotification.CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelTimetableSlotLinkNotificationForm.CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelTimetableSlotLinkNotificationForm.CHANNELTIMETABLESLOT_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelArchiveLinkNotificationForm.CHANNELARCHIVE_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelArchiveLinkNotification.CHANNELARCHIVE_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelArchiveLinkNotificationForm.CHANNELARCHIVE_NOTIFICATION_OPERATION.delete;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelQueueItemLinkNotificationForm.CHANNELQUEUE_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelQueueItemLinkNotification.CHANNELQUEUE_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelQueueItemLinkNotificationForm.CHANNELQUEUE_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelQueueItemLinkNotificationForm.CHANNELQUEUE_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.playlist.notification.PlaylistContentNotificationForm.PLAYLIST_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistContentNotification.PLAYLIST_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.playlist.notification.PlaylistContentNotificationForm.PLAYLIST_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.playlist.notification.PlaylistProgrammeLinkNotificationForm.PLAYLISTPROGRAMME_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification.PLAYLISTPROGRAMME_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.playlist.notification.PlaylistProgrammeLinkNotificationForm.PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.playlist.notification.PlaylistProgrammeLinkNotificationForm.PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.playlist.notification.PlaylistTimetableSlotLinkNotificationForm.PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification.PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.playlist.notification.PlaylistTimetableSlotLinkNotificationForm.PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.playlist.notification.PlaylistTimetableSlotLinkNotificationForm.PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammeAnimatorLinkNotificationForm.PROGRAMMEANIMATOR_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification.PROGRAMMEANIMATOR_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeAnimatorLinkNotificationForm.PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeAnimatorLinkNotificationForm.PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add;
                case validate:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeBroadcastableTrackLinkNotificationForm.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.validate;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContentNotificationForm.PROGRAMME_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContentNotification.PROGRAMME_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContentNotificationForm.PROGRAMME_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContributorLinkNotificationForm.PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification.PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContributorLinkNotificationForm.PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeContributorLinkNotificationForm.PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammeCuratorLinkNotificationForm.PROGRAMMECURATOR_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification.PROGRAMMECURATOR_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeCuratorLinkNotificationForm.PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeCuratorLinkNotificationForm.PROGRAMMECURATOR_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammePendingTrackLinkNotificationForm.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammePendingTrackLinkNotificationForm.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammePendingTrackLinkNotificationForm.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add;
                case reject:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammePendingTrackLinkNotificationForm.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.reject;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.programme.notification.ProgrammeTimetableSlotLinkNotificationForm.PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification.PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case delete:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeTimetableSlotLinkNotificationForm.PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete;
                case add:
                    return biz.ddcr.shampoo.client.form.programme.notification.ProgrammeTimetableSlotLinkNotificationForm.PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.add;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.timetable.notification.TimetableSlotContentNotificationForm.TIMETABLESLOT_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.timetable.notification.TimetableContentNotification.TIMETABLE_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.timetable.notification.TimetableSlotContentNotificationForm.TIMETABLESLOT_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.track.notification.BroadcastTrackContentNotificationForm.BROADCASTTRACK_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.track.notification.BroadcastableTrackContentNotification.BROADCASTABLETRACK_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.track.notification.BroadcastTrackContentNotificationForm.BROADCASTTRACK_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.track.notification.PendingTrackContentNotificationForm.PENDINGTRACK_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.track.notification.PendingTrackContentNotification.PENDINGTRACK_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.track.notification.PendingTrackContentNotificationForm.PENDINGTRACK_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.user.notification.RestrictedUserContentNotificationForm.RESTRICTEDUSER_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification.RESTRICTEDUSER_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.user.notification.RestrictedUserContentNotificationForm.RESTRICTEDUSER_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.StreamerStatusNotificationForm.STREAMER_STATUS_OPERATION toDTO(biz.ddcr.shampoo.server.domain.streamer.notification.StreamerStatusNotification.STREAMER_STATUS_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case channel_reserved:
                    return biz.ddcr.shampoo.client.form.channel.notification.StreamerStatusNotificationForm.STREAMER_STATUS_OPERATION.channel_reserved;
                case channel_free:
                    return biz.ddcr.shampoo.client.form.channel.notification.StreamerStatusNotificationForm.STREAMER_STATUS_OPERATION.channel_free;
                case channel_onair:
                    return biz.ddcr.shampoo.client.form.channel.notification.StreamerStatusNotificationForm.STREAMER_STATUS_OPERATION.channel_onair;
                case channel_offair:
                    return biz.ddcr.shampoo.client.form.channel.notification.StreamerStatusNotificationForm.STREAMER_STATUS_OPERATION.channel_offair;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelWebserviceLinkNotificationForm.CHANNELWEBSERVICE_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelWebserviceLinkNotification.CHANNELWEBSERVICE_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelWebserviceLinkNotificationForm.CHANNELWEBSERVICE_NOTIFICATION_OPERATION.add;
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelWebserviceLinkNotificationForm.CHANNELWEBSERVICE_NOTIFICATION_OPERATION.delete;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.webservice.notification.WebserviceContentNotificationForm.WEBSERVICE_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.webservice.notification.WebserviceContentNotification.WEBSERVICE_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case edit:
                    return biz.ddcr.shampoo.client.form.webservice.notification.WebserviceContentNotificationForm.WEBSERVICE_NOTIFICATION_OPERATION.edit;
                default:
                    return null;
            }
        }
        return null;
    }

    public static biz.ddcr.shampoo.client.form.channel.notification.ChannelReportLinkNotificationForm.CHANNELREPORT_NOTIFICATION_OPERATION toDTO(biz.ddcr.shampoo.server.domain.channel.notification.ChannelReportLinkNotification.CHANNELREPORT_NOTIFICATION_OPERATION operation) {
        if (operation != null) {
            switch (operation) {
                case add:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelReportLinkNotificationForm.CHANNELREPORT_NOTIFICATION_OPERATION.add;
                case delete:
                    return biz.ddcr.shampoo.client.form.channel.notification.ChannelReportLinkNotificationForm.CHANNELREPORT_NOTIFICATION_OPERATION.delete;
                default:
                    return null;
            }
        }
        return null;
    }
    
    @Override
    public ActionCollectionEntry<? extends NotificationForm> toDTO(Notification message) throws Exception {
        if (message == null) {
            return null;
        }


        NotificationVisitorDTO messageVisitor = new NotificationVisitorDTO() {

            NotificationForm messageModule = null;

            @Override
            public NotificationForm getForm() {
                return messageModule;
            }

            @Override
            public void visit(ChannelContentNotification message) throws Exception {
                messageModule = new ChannelContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID());
            }

            @Override
            public void visit(PlaylistContentNotification message) throws Exception {
                messageModule = new PlaylistContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID(),
                        message.getPlaylistCaption());
            }

            @Override
            public void visit(ProgrammeContentNotification message) throws Exception {
                messageModule = new ProgrammeContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID());
            }

            @Override
            public void visit(TimetableContentNotification message) throws Exception {
                messageModule = new TimetableSlotContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID(),
                        DateDTOAssembler.toJS(message.getSlotStartTime()),
                        message.getSlotChannelCaption());
            }

            @Override
            public void visit(BroadcastableTrackContentNotification message) throws Exception {
                messageModule = new BroadcastTrackContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID(),
                        message.getTrackAuthorCaption(),
                        message.getTrackTitleCaption());
            }

            @Override
            public void visit(PendingTrackContentNotification message) throws Exception {
                messageModule = new PendingTrackContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID(),
                        message.getTrackAuthorCaption(),
                        message.getTrackTitleCaption());
            }

            @Override
            public void visit(RestrictedUserContentNotification message) throws Exception {
                messageModule = new RestrictedUserContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID());
            }

            @Override
            public void visit(ChannelChannelAdministratorLinkNotification message) throws Exception {
                messageModule = new ChannelChannelAdministratorLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ChannelListenerLinkNotification message) throws Exception {
                messageModule = new ChannelListenerLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ChannelProgrammeLinkNotification message) throws Exception {
                messageModule = new ChannelProgrammeLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ChannelProgrammeManagerLinkNotification message) throws Exception {
                messageModule = new ChannelProgrammeManagerLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ChannelTimetableSlotLinkNotification message) throws Exception {
                messageModule = new ChannelTimetableSlotLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        DateDTOAssembler.toJS(message.getSlotStartTime()),
                        message.getSlotChannelCaption());
            }

            @Override
            public void visit(ChannelArchiveLinkNotification message) throws Exception {
                messageModule = new ChannelArchiveLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        DateDTOAssembler.toJS(message.getArchiveStartTime()),
                        message.getArchiveChannelCaption());
            }

            @Override
            public void visit(ChannelQueueItemLinkNotification message) throws Exception {
                messageModule = new ChannelQueueItemLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        DateDTOAssembler.toJS(message.getQueueItemStartTime()),
                        message.getQueueItemChannelCaption());
            }

            @Override
            public void visit(PlaylistProgrammeLinkNotification message) throws Exception {
                messageModule = new PlaylistProgrammeLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getPlaylistCaption(),
                        message.getDestinationID());
            }

            @Override
            public void visit(PlaylistTimetableSlotLinkNotification message) throws Exception {
                messageModule = new PlaylistTimetableSlotLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getPlaylistCaption(),
                        message.getDestinationID(),
                        DateDTOAssembler.toJS(message.getSlotStartTime()),
                        message.getSlotChannelCaption());
            }

            @Override
            public void visit(ProgrammeAnimatorLinkNotification message) throws Exception {
                messageModule = new ProgrammeAnimatorLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ProgrammeBroadcastableTrackLinkNotification message) throws Exception {
                messageModule = new ProgrammeBroadcastableTrackLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        message.getTrackAuthorCaption(),
                        message.getTrackTitleCaption());
            }

            @Override
            public void visit(ProgrammeContributorLinkNotification message) throws Exception {
                messageModule = new ProgrammeContributorLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ProgrammeCuratorLinkNotification message) throws Exception {
                messageModule = new ProgrammeCuratorLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ProgrammePendingTrackLinkNotification message) throws Exception {
                messageModule = new ProgrammePendingTrackLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        message.getTrackAuthorCaption(),
                        message.getTrackTitleCaption());
            }

            @Override
            public void visit(ProgrammeTimetableSlotLinkNotification message) throws Exception {
                messageModule = new ProgrammeTimetableSlotLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        DateDTOAssembler.toJS(message.getSlotStartTime()),
                        message.getSlotChannelCaption());
            }

            @Override
            public void visit(StreamerStatusNotification message) throws Exception {
                messageModule = new StreamerStatusNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID());
            }

            @Override
            public void visit(WebserviceContentNotification message) throws Exception {
                messageModule = new WebserviceContentNotificationForm(
                        toDTO(message.getAction()),
                        message.getEntityID());
            }

            @Override
            public void visit(ChannelWebserviceLinkNotification message) throws Exception {
                messageModule = new ChannelWebserviceLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID());
            }

            @Override
            public void visit(ChannelReportLinkNotification message) throws Exception {
                messageModule = new ChannelReportLinkNotificationForm(
                        toDTO(message.getAction()),
                        message.getSourceID(),
                        message.getDestinationID(),
                        DateDTOAssembler.toJS(message.getReportStartTime()),
                        DateDTOAssembler.toJS(message.getReportEndTime()),
                        message.getReportChannelCaption());
            }
        };
        message.acceptVisit(messageVisitor);

        if (messageVisitor.getForm() != null) {
            //Fill in common properties
            messageVisitor.getForm().setRefID(message.getRefID());
            if (message.getActorID() != null) {
                //We get a NoEntityException while loading the sender if he doesn't exist anymore, replace it with null
                try {
                    messageVisitor.getForm().setSender(
                            new ActionMapEntry<String>(
                            message.getActorID(),
                            decorateEntryWithActions(
                            getUserManager().getUser(message.getActorID()))));
                } catch (NoEntityException e) {
                    //Drop silently
                }
            }
            messageVisitor.getForm().setTime(DateDTOAssembler.toJS(message.getDateStamp()));
            messageVisitor.getForm().setRecipient(
                    new ActionMapEntry<String>(
                    message.getRecipient() != null ? message.getRecipient().getUsername() : null,
                    decorateEntryWithActions(message.getRecipient())));
            messageVisitor.getForm().setFreeFormText(message.getFreeText());
            messageVisitor.getForm().setRecipientNotified(message.isRecipientNotified());

            return new ActionMapEntry<NotificationForm>(messageVisitor.getForm(), decorateEntryWithActions(message));
        } else {
            return null;
        }
    }

    protected Collection<ACTION> decorateEntryWithActions(Notification message) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getSecurityManager().checkAccessViewNotification(message)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getSecurityManager().checkAccessDeleteNotification(message)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(User user) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getUserManager().checkAccessViewUser(user)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessUpdateUser(user)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessDeleteUser(user)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    @Override
    public ActionCollection<? extends NotificationForm> toDTO(Collection<Notification> messages, boolean doSort) throws Exception {
        ActionMap<NotificationForm> messageModules = doSort ? new ActionSortedMap<NotificationForm>() : new ActionUnsortedMap<NotificationForm>();
        for (Notification message : messages) {
            messageModules.set(toDTO(message));
        }
        return messageModules;
    }
}
