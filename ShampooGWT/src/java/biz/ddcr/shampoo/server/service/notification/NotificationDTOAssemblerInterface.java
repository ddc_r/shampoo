/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.notification;

import biz.ddcr.shampoo.client.form.notification.NotificationForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface NotificationDTOAssemblerInterface {
    
    //Domain to DTO conversion
    public ActionCollectionEntry<? extends NotificationForm> toDTO(Notification message) throws Exception;

    //Helpers
    public ActionCollection<? extends NotificationForm> toDTO(Collection<Notification> messages, boolean doSort) throws Exception;

}
