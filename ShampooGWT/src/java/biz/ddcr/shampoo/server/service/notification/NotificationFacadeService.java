/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.notification;

import biz.ddcr.shampoo.client.form.notification.NotificationForm;
import biz.ddcr.shampoo.client.form.notification.NotificationForm.NOTIFICATION_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 **/
public class NotificationFacadeService extends GenericService implements NotificationFacadeServiceInterface {

    private NotificationDTOAssemblerInterface notificationDTOAssembler;
    private SecurityManagerInterface securityManager;

    public NotificationDTOAssemblerInterface getNotificationDTOAssembler() {
        return notificationDTOAssembler;
    }

    public void setNotificationDTOAssembler(NotificationDTOAssemblerInterface notificationDTOAssembler) {
        this.notificationDTOAssembler = notificationDTOAssembler;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void deleteSecuredNotification(String id) throws Exception {

        if (id != null) {

            Notification message = getSecurityManager().getNotification(id);
            //Then check if it can be accessed

            if (getSecurityManager().checkAccessDeleteNotification(message)) {

                //Whatever happens, someone who can remove a message can unlink other stuff bound to it: no need for extra check
                //Cascade delete will take care of their unlinking

                //Everything's fine, now make it persistent in the database
                getSecurityManager().deleteNotification(message);
                //We don't log notifications

            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredNotifications(Collection<String> ids) throws Exception {
        if (ids != null) {

            Collection messagesToDelete = new HashSet<Notification>();
            for (String id : ids) {
                //First get a copy of the entity from a DTO
                Notification message = getSecurityManager().getNotification(id);

                if (getSecurityManager().checkAccessDeleteNotification(message)) {
                    messagesToDelete.add(message);
                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!messagesToDelete.isEmpty()) {

                getSecurityManager().deleteNotifications(messagesToDelete);
                //We don't log notifications

            }
        }
    }

    @Override
    public ActionCollectionEntry<? extends NotificationForm> getSecuredNotification(String id) throws Exception {
        Notification message = getSecurityManager().loadNotification(id);
        ActionCollectionEntry<? extends NotificationForm> messageModuleEntry = notificationDTOAssembler.toDTO(message);
        if (messageModuleEntry == null || messageModuleEntry.isReadable()) {
            //We don't log notifications
            return messageModuleEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<? extends NotificationForm> getSecuredNotifications(GroupAndSort<NOTIFICATION_TAGS> constraint) throws Exception {
        Collection<Notification> messages = getSecurityManager().getFilteredNotifications(constraint);
        ActionCollection<? extends NotificationForm> forms = notificationDTOAssembler.toDTO(messages, constraint==null || constraint.getSortByFeature()==null);
        //We dont log notifications
        return forms;
    }

}
