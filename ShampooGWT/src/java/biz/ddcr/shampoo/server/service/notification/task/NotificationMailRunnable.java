/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.notification.task;

import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;

/**
 *
 * @author okay_awright
 **/
public class NotificationMailRunnable extends GenericService implements NotificationMailRunnableInterface {

    private SecurityManagerInterface securityManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /**
     * Mail new notifications to users
     * @throws Exception
     */
    @Override
    public void sendNewMessages() throws Exception {
        getSecurityManager().sendNotifications();
    }

    @Override
    public void run() {
        try {
            sendNewMessages();
        } catch (Exception e) {
            logger.error("Notification runnable failed", e);
        }
    }

}
