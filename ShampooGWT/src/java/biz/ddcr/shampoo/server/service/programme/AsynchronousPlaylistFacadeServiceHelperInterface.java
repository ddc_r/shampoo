/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface AsynchronousPlaylistFacadeServiceHelperInterface {

    public interface AsynchronousDeletePlaylistTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Playlist> {
        public void add(PlaylistForm playlist);
    }

    public interface AsynchronousAddPlaylistTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Playlist> {
        public void add(PlaylistForm playlist, String coverArtUploadId);
        public void add(PlaylistForm playlist);
    }

    public interface AsynchronousUpdatePlaylistTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Playlist> {
        public void add(PlaylistForm playlist, String coverArtUploadId);
        public void add(PlaylistForm playlist);
    }

    public void synchronousInitialization(Collection<AsynchronousUnmanagedTransactionUnitInterface<Playlist>> asynchronousPlaylistOps) throws Exception;
    public void synchronousProcessing(Collection<AsynchronousUnmanagedTransactionUnitInterface<Playlist>> asynchronousPlaylistOps) throws Exception;
    public void synchronousCommit(Collection<AsynchronousUnmanagedTransactionUnitInterface<Playlist>> asynchronousPlaylistOps) throws Exception;
    public void synchronousRollback(Collection<AsynchronousUnmanagedTransactionUnitInterface<Playlist>> asynchronousPlaylistOps, Exception originalException) throws Exception;
}
