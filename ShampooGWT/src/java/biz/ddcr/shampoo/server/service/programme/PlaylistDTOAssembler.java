/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.SubSelectionModule;
import biz.ddcr.shampoo.client.form.playlist.DynamicPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.LiveForm;
import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistEntryForm;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistTagSetForm;
import biz.ddcr.shampoo.client.form.playlist.StaticPlaylistEntryForm;
import biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.CategoryFilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.FilterForm;
import biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm;
import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.form.timetable.TimetableSlotFormID;
import biz.ddcr.shampoo.client.form.track.TRACK_ACTION;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.DynamicPlaylistEntry;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntryVisitor;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistTagSet;
import biz.ddcr.shampoo.server.domain.playlist.StaticPlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.filter.AlphabeticalFilter;
import biz.ddcr.shampoo.server.domain.track.filter.CategoryFilter;
import biz.ddcr.shampoo.server.domain.track.filter.Filter;
import biz.ddcr.shampoo.server.domain.track.filter.FilterVisitor;
import biz.ddcr.shampoo.server.domain.track.filter.NumericalFilter;
import biz.ddcr.shampoo.server.domain.playlist.SubSelection;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.channel.TimetableDTOAssembler;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.track.TrackDTOAssembler;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author okay_awright
 **/
public class PlaylistDTOAssembler extends GenericService implements PlaylistDTOAssemblerInterface {

    private interface DTOPlaylistEntryVisitor extends PlaylistEntryVisitor {

        public PlaylistEntryForm get();
    }
    private ProgrammeManagerInterface programmeManager;
    private ChannelManagerInterface channelManager;
    private TrackManagerInterface trackManager;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    public static biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE fromDTO(biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE feature) {
        if (feature == null) {
            return null;
        }
        switch (feature) {
            case title:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.title;
            case author:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.author;
            case album:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.album;
            case genre:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.genre;
            case flag:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.flag;
            case copyright:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.copyright;
            case advisory:
                return biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE.advisory;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE toDTO(biz.ddcr.shampoo.server.domain.track.filter.ALPHABETICAL_FEATURE feature) {
        if (feature == null) {
            return null;
        }
        switch (feature) {
            case title:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.title;
            case author:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.author;
            case album:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.album;
            case genre:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.genre;
            case flag:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.flag;
            case copyright:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.copyright;
            case advisory:
                return biz.ddcr.shampoo.client.form.playlist.filter.AlphabeticalFilterForm.ALPHABETICAL_FEATURE.advisory;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE fromDTO(biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE feature) {
        if (feature == null) {
            return null;
        }
        switch (feature) {
            case averageVote:
                return biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE.averageRating;
            case dateOfSubmission:
                return biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE.databaseRecentness;
            case dateOfRelease:
                return biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE.dateOfRelease;
            case rotationCount:
                return biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE.rotationCount;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE toDTO(biz.ddcr.shampoo.server.domain.track.filter.NUMERICAL_FEATURE feature) {
        if (feature == null) {
            return null;
        }
        switch (feature) {
            case averageRating:
                return biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE.averageVote;
            case databaseRecentness:
                return biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE.dateOfSubmission;
            case dateOfRelease:
                return biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE.dateOfRelease;
            case rotationCount:
                return biz.ddcr.shampoo.client.form.playlist.filter.NumericalFilterForm.NUMERICAL_FEATURE.rotationCount;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.server.domain.track.filter.CATEGORY_FEATURE fromDTO(biz.ddcr.shampoo.client.form.track.TYPE feature) {
        if (feature == null) {
            return null;
        }
        switch (feature) {
            case song:
                return biz.ddcr.shampoo.server.domain.track.filter.CATEGORY_FEATURE.song;
            case jingle:
                return biz.ddcr.shampoo.server.domain.track.filter.CATEGORY_FEATURE.jingle;
            case advert:
                return biz.ddcr.shampoo.server.domain.track.filter.CATEGORY_FEATURE.advert;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.client.form.track.TYPE toDTO(biz.ddcr.shampoo.server.domain.track.filter.CATEGORY_FEATURE feature) {
        if (feature == null) {
            return null;
        }
        switch (feature) {
            case song:
                return biz.ddcr.shampoo.client.form.track.TYPE.song;
            case jingle:
                return biz.ddcr.shampoo.client.form.track.TYPE.jingle;
            case advert:
                return biz.ddcr.shampoo.client.form.track.TYPE.advert;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.server.domain.track.SORT fromDTO(biz.ddcr.shampoo.client.form.track.SORT rule) {
        if (rule == null) {
            return null;
        }
        switch (rule) {
            case submissionDateAsc:
                return biz.ddcr.shampoo.server.domain.track.SORT.creationDateAsc;
            case submissionDateDesc:
                return biz.ddcr.shampoo.server.domain.track.SORT.creationDateDesc;
            case releaseDateAsc:
                return biz.ddcr.shampoo.server.domain.track.SORT.releaseDateAsc;
            case releaseDateDesc:
                return biz.ddcr.shampoo.server.domain.track.SORT.releaseDateDesc;
            case rotationCountAsc:
                return biz.ddcr.shampoo.server.domain.track.SORT.rotationCountAsc;
            case rotationCountDesc:
                return biz.ddcr.shampoo.server.domain.track.SORT.rotationCountDesc;
            case averageRatingAsc:
                return biz.ddcr.shampoo.server.domain.track.SORT.averageRatingAsc;
            case averageRatingDesc:
                return biz.ddcr.shampoo.server.domain.track.SORT.averageRatingDesc;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.client.form.track.SORT toDTO(biz.ddcr.shampoo.server.domain.track.SORT rule) {
        if (rule == null) {
            return null;
        }
        switch (rule) {
            case creationDateAsc:
                return biz.ddcr.shampoo.client.form.track.SORT.submissionDateAsc;
            case creationDateDesc:
                return biz.ddcr.shampoo.client.form.track.SORT.submissionDateDesc;
            case releaseDateAsc:
                return biz.ddcr.shampoo.client.form.track.SORT.releaseDateAsc;
            case releaseDateDesc:
                return biz.ddcr.shampoo.client.form.track.SORT.releaseDateDesc;
            case rotationCountAsc:
                return biz.ddcr.shampoo.client.form.track.SORT.rotationCountAsc;
            case rotationCountDesc:
                return biz.ddcr.shampoo.client.form.track.SORT.rotationCountDesc;
            case averageRatingAsc:
                return biz.ddcr.shampoo.client.form.track.SORT.averageRatingAsc;
            case averageRatingDesc:
                return biz.ddcr.shampoo.client.form.track.SORT.averageRatingDesc;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.server.domain.track.SELECTION fromDTO(biz.ddcr.shampoo.client.form.track.SELECTION selection) {
        if (selection == null) {
            return null;
        }
        switch (selection) {
            case random:
                return biz.ddcr.shampoo.server.domain.track.SELECTION.random;
            case weightedRandom:
                return biz.ddcr.shampoo.server.domain.track.SELECTION.weightedRandom;
            case orderedSequence:
                return biz.ddcr.shampoo.server.domain.track.SELECTION.orderedSequence;
            default:
                return null;
        }
    }

    public static biz.ddcr.shampoo.client.form.track.SELECTION toDTO(biz.ddcr.shampoo.server.domain.track.SELECTION selection) {
        if (selection == null) {
            return null;
        }
        switch (selection) {
            case random:
                return biz.ddcr.shampoo.client.form.track.SELECTION.random;
            case weightedRandom:
                return biz.ddcr.shampoo.client.form.track.SELECTION.weightedRandom;
            case orderedSequence:
                return biz.ddcr.shampoo.client.form.track.SELECTION.orderedSequence;
            default:
                return null;
        }
    }

    protected SubSelection fromDTO(SubSelectionModule criterionModule) throws Exception {
        if (criterionModule != null) {
            SubSelection criterion = new SubSelection();
            criterion.setStartIndex(criterionModule.getFrom());
            criterion.setEndIndex(criterionModule.getTo());
            criterion.setSort(
                    fromDTO(criterionModule.getSort()));
            return criterion;
        }
        return null;
    }

    protected SubSelectionModule toDTO(SubSelection criterion) throws Exception {
        if (criterion != null) {
            SubSelectionModule criterionModule = new SubSelectionModule();
            criterionModule.setFrom(criterion.getStartIndex());
            criterionModule.setTo(criterion.getEndIndex());
            criterionModule.setSort(
                    toDTO(criterion.getSort()));
            return criterionModule;
        }
        return null;
    }

    protected Filter fromDTO(DynamicPlaylistEntry entry, FilterForm filterForm) throws Exception {
        if (filterForm != null && entry != null) {
            Filter newFilter;

            if (ProxiedClassUtil.castableAs(filterForm, AlphabeticalFilterForm.class)) {
                //AlphabeticalFilterForm
                newFilter = new AlphabeticalFilter(entry);

                ((AlphabeticalFilter) newFilter).setContains(((AlphabeticalFilterForm) filterForm).getContains());
                ((AlphabeticalFilter) newFilter).setFeature(
                        fromDTO(((AlphabeticalFilterForm) filterForm).getFeature()));

            } else if (ProxiedClassUtil.castableAs(filterForm, NumericalFilterForm.class)) {
                //NumericalFilterForm
                newFilter = new NumericalFilter(entry);

                ((NumericalFilter) newFilter).setMax(((NumericalFilterForm) filterForm).getMax());
                ((NumericalFilter) newFilter).setMin(((NumericalFilterForm) filterForm).getMin());
                ((NumericalFilter) newFilter).setFeature(
                        fromDTO(((NumericalFilterForm) filterForm).getFeature()));

            } else {
                //CategoryFilterForm
                newFilter = new CategoryFilter(entry);
                ((CategoryFilter) newFilter).setFeature(
                        fromDTO(((CategoryFilterForm) filterForm).getFeature()));
            }
            newFilter.setInclude(filterForm.isInclude());
            return newFilter;
        }
        return null;
    }

    protected AlphabeticalFilterForm toDTO(AlphabeticalFilter filter) throws Exception {
        AlphabeticalFilterForm newFilterForm = new AlphabeticalFilterForm();

        newFilterForm.setContains(filter.getContains());
        newFilterForm.setFeature(
                toDTO(filter.getFeature()));
        newFilterForm.setInclude(filter.isInclude());
        return newFilterForm;
    }

    protected NumericalFilterForm toDTO(NumericalFilter filter) throws Exception {
        NumericalFilterForm newFilterForm = new NumericalFilterForm();

        newFilterForm.setMax(filter.getMax());
        newFilterForm.setMin(filter.getMin());
        newFilterForm.setFeature(
                toDTO(filter.getFeature()));
        newFilterForm.setInclude(filter.isInclude());
        return newFilterForm;
    }

    protected CategoryFilterForm toDTO(CategoryFilter filter) throws Exception {
        CategoryFilterForm newFilterForm = new CategoryFilterForm();

        newFilterForm.setFeature(
                toDTO(filter.getFeature()));
        newFilterForm.setInclude(filter.isInclude());
        return newFilterForm;
    }

    protected Collection<FilterForm> toFilterDTO(Collection<? extends Filter> filters) throws Exception {
        if (filters != null) {
            final Collection<FilterForm> filterForms = new HashSet<FilterForm>();

            FilterVisitor filterVisitor = new FilterVisitor() {

                @Override
                public void visit(AlphabeticalFilter filter) throws Exception {
                    //AlphabeticalFilter
                    filterForms.add(
                            toDTO(filter));
                }

                @Override
                public void visit(NumericalFilter filter) throws Exception {
                    //NumericalFilter
                    filterForms.add(
                            toDTO(filter));
                }

                @Override
                public void visit(CategoryFilter filter) throws Exception {
                    //CategoryFilter
                    filterForms.add(
                            toDTO(filter));
                }
            };

            for (Filter filter : filters) {
                filter.acceptVisit(filterVisitor);
            }

            return filterForms;
        }
        return null;

    }

    /**
     * Playlist CAN be null
     * @param playlist
     * @param playlistEntryForm
     * @return
     * @throws Exception
     */
    protected PlaylistEntry fromDTO(Playlist playlist, PlaylistEntryForm playlistEntryForm) throws Exception {
        if (playlistEntryForm != null /*&& playlist != null*/) {

            PlaylistEntry newPlaylistEntry;

            if (ProxiedClassUtil.castableAs(playlistEntryForm, StaticPlaylistEntryForm.class)) {

                newPlaylistEntry = new StaticPlaylistEntry(playlist, playlistEntryForm.getSequenceIndex());

                //load(), don't get(); otherwise you will face the dreaded "flush before select" and it's deadly with partially constructed objects
                BroadcastableTrack t = getTrackManager().getBroadcastTrack(
                        ((StaticPlaylistEntryForm) playlistEntryForm).getBroadcastTrackId().getItem());

                //This is one of the very only instances where Read permission is only required
                if (getTrackManager().checkAccessViewBroadcastTrack(t)) {
                    ((StaticPlaylistEntry) newPlaylistEntry).addTrack(t);
                }

            } else {
                newPlaylistEntry = new DynamicPlaylistEntry(playlist, playlistEntryForm.getSequenceIndex());

                for (FilterForm filterForm : ((DynamicPlaylistEntryForm) playlistEntryForm).getFilters()) {
                    ((DynamicPlaylistEntry) newPlaylistEntry).addFilter(
                            fromDTO((DynamicPlaylistEntry) newPlaylistEntry, filterForm));
                }

                ((DynamicPlaylistEntry) newPlaylistEntry).setSelection(
                        fromDTO(((DynamicPlaylistEntryForm) playlistEntryForm).getSelection()));
                ((DynamicPlaylistEntry) newPlaylistEntry).setSubSelection(
                        fromDTO(((DynamicPlaylistEntryForm) playlistEntryForm).getSubSelection()));

                ((DynamicPlaylistEntry) newPlaylistEntry).setNoReplayDelay(((DynamicPlaylistEntryForm) playlistEntryForm).getNoReplayDelay());
                ((DynamicPlaylistEntry) newPlaylistEntry).setNoReplayInPlaylist(((DynamicPlaylistEntryForm) playlistEntryForm).isNoReplayInPlaylist());
            }
            newPlaylistEntry.setRequestAllowed(playlistEntryForm.isRequestAllowed());
            newPlaylistEntry.setFadeIn(playlistEntryForm.getFadeIn());
            newPlaylistEntry.setLoop(playlistEntryForm.getLoop());
            newPlaylistEntry.setSequenceIndex(playlistEntryForm.getSequenceIndex());

            return newPlaylistEntry;
        }
        return null;
    }

    protected PlaylistEntryForm toDTO(PlaylistEntry playlistEntry) throws Exception {
        if (playlistEntry != null) {

            DTOPlaylistEntryVisitor visitor = new DTOPlaylistEntryVisitor() {

                private PlaylistEntryForm form;

                @Override
                public PlaylistEntryForm get() {
                    return form;
                }

                @Override
                public void visit(DynamicPlaylistEntry entry) throws Exception {
                    DynamicPlaylistEntryForm newPlaylistEntryForm = new DynamicPlaylistEntryForm();

                    newPlaylistEntryForm.setFilters(
                            toFilterDTO(entry.getFilters()));

                    newPlaylistEntryForm.setSelection(
                            toDTO(entry.getSelection()));
                    newPlaylistEntryForm.setSubSelection(
                            toDTO(entry.getSubSelection()));

                    newPlaylistEntryForm.setNoReplayDelay(entry.getNoReplayDelay());
                    newPlaylistEntryForm.setNoReplayInPlaylist(entry.isNoReplayInPlaylist());

                    newPlaylistEntryForm.setRequestAllowed(entry.isRequestAllowed());
                    newPlaylistEntryForm.setFadeIn(entry.getFadeIn());
                    newPlaylistEntryForm.setLoop(entry.getLoop());
                    newPlaylistEntryForm.setSequenceIndex(entry.getSequenceIndex());

                    form = newPlaylistEntryForm;
                }

                @Override
                public void visit(StaticPlaylistEntry entry) throws Exception {
                    //StaticPlaylistEntry
                    StaticPlaylistEntryForm newPlaylistEntryForm = new StaticPlaylistEntryForm();

                    //Track
                    //A track can be null when removed from outside the playlist
                    if (entry.getTrack() != null) {
                        newPlaylistEntryForm.setBroadcastTrackId(
                                new ActionMapEntry<String>(
                                entry.getTrack().getRefID(),
                                decorateEntryWithActions(entry.getTrack())));
                    }
                    newPlaylistEntryForm.setRequestAllowed(entry.isRequestAllowed());
                    newPlaylistEntryForm.setFadeIn(entry.getFadeIn());
                    newPlaylistEntryForm.setLoop(entry.getLoop());
                    newPlaylistEntryForm.setSequenceIndex(entry.getSequenceIndex());

                    form = newPlaylistEntryForm;
                }
            };
            playlistEntry.acceptVisit(visitor);
            return visitor.get();
        }
        return null;
    }

    public static PlaylistTagSet fromDTO(PlaylistTagSetForm playlistTagSetForm) throws Exception {
        if (playlistTagSetForm != null) {
            PlaylistTagSet newPlaylistTagSet = new PlaylistTagSet();
            newPlaylistTagSet.setRating(TrackDTOAssembler.fromDTO(playlistTagSetForm.getAdvisory()));
            newPlaylistTagSet.setAlbum(playlistTagSetForm.getAlbum());
            newPlaylistTagSet.setAuthor(playlistTagSetForm.getAuthor());
            newPlaylistTagSet.setPublisher(playlistTagSetForm.getPublisher());
            newPlaylistTagSet.setCopyright(playlistTagSetForm.getCopyright());
            newPlaylistTagSet.setDescription(playlistTagSetForm.getDescription());
            newPlaylistTagSet.setGenre(playlistTagSetForm.getGenre());
            newPlaylistTagSet.setTitle(playlistTagSetForm.getTitle());
            newPlaylistTagSet.setDateOfRelease(playlistTagSetForm.getYearOfRelease());

            return newPlaylistTagSet;
        }
        return null;
    }

    public static PlaylistTagSetForm toDTO(PlaylistTagSet playlistTagSet) throws Exception {
        if (playlistTagSet != null) {
            PlaylistTagSetForm newPlaylistTagSetForm = new PlaylistTagSetForm();
            newPlaylistTagSetForm.setAdvisory(TrackDTOAssembler.toDTO(playlistTagSet.getRating()));
            newPlaylistTagSetForm.setAlbum(playlistTagSet.getAlbum());
            newPlaylistTagSetForm.setAuthor(playlistTagSet.getAuthor());
            newPlaylistTagSetForm.setPublisher(playlistTagSet.getPublisher());
            newPlaylistTagSetForm.setCopyright(playlistTagSet.getCopyright());
            newPlaylistTagSetForm.setDescription(playlistTagSet.getDescription());
            newPlaylistTagSetForm.setGenre(playlistTagSet.getGenre());
            newPlaylistTagSetForm.setTitle(playlistTagSet.getTitle());
            newPlaylistTagSetForm.setYearOfRelease(playlistTagSet.getDateOfRelease());

            return newPlaylistTagSetForm;
        }
        return null;
    }

    protected Live fromDTO(LiveForm liveForm) throws Exception {
        if (liveForm != null) {
            Live newLive = new Live();
            newLive.setBroadcaster(liveForm.getBroadcaster());
            newLive.setLogin(liveForm.getLogin());
            newLive.setPassword(liveForm.getPassword());
            newLive.setRating(TrackDTOAssembler.fromDTO(liveForm.getAdvisory()));

            return newLive;
        }
        return null;
    }

    protected LiveForm toDTO(Live live) throws Exception {
        if (live != null) {
            LiveForm newLiveForm = new LiveForm();
            newLiveForm.setBroadcaster(live.getBroadcaster());
            newLiveForm.setLogin(live.getLogin());
            newLiveForm.setPassword(live.getPassword());
            newLiveForm.setAdvisory(TrackDTOAssembler.toDTO(live.getRating()));

            return newLiveForm;
        }
        return null;
    }

    @Override
    public Playlist fromDTO(PlaylistForm playlistForm) throws Exception {

        if (playlistForm != null) {
            Playlist newPlaylist = new Playlist();

            //Fill in all attributes
            newPlaylist.setRefID(playlistForm.getRefID());
            newPlaylist.setDescription(playlistForm.getDescription());
            newPlaylist.setLabel(playlistForm.getLabel());
            newPlaylist.setMaxUserRequestLimit(playlistForm.getMaxUserRequestLimit());
            newPlaylist.setNoRequestReplayDelay(playlistForm.getNoRequestReplayDelay());
            newPlaylist.setNoRequestReplayInPlaylist(playlistForm.isNoRequestReplayInPlaylist());

            //We don't really care about the ready state since it's a DB thing only
            newPlaylist.setReady(playlistForm.isReady());
            newPlaylist.setCoverArtContainer(TrackDTOAssembler.fromDTO(playlistForm.getCoverArtFile()));

            Programme p = getProgrammeManager().getProgramme(playlistForm.getProgrammeId().getKey());

            //Someone who can create a playlist can see the attached programme
            if (getProgrammeManager().checkAccessUpdatePlaylist(p)) {
                newPlaylist.addProgramme(p);
            } else {
                throw new AccessDeniedException();
            }

            //Global tag set?
            newPlaylist.setGlobalTagSet(
                    fromDTO(playlistForm.getGlobalTagSet()));

            //Live?
            newPlaylist.setLive(
                    fromDTO(playlistForm.getLive()));
            //Playlist entries
            for (PlaylistEntryForm playlistEntryForm : playlistForm.getPlaylistEntries()) {
                newPlaylist.addPlaylistEntry(
                        fromDTO(newPlaylist, playlistEntryForm));
            }
            //Timetable slots linked to
            for (ActionCollectionEntry<TimetableSlotFormID> timetableEntry : playlistForm.getTimetableSlotIds()) {

                /*TimetableSlot slot = getChannelManager().getTimetableSlot(
                new TimetableSlotLocation(
                getChannelManager().getChannel(
                timetableEntry.getItem().getChannelId()),
                YearMonthWeekDayHourMinute.fromJS(
                timetableEntry.getItem().getSchedulingTime())));*/
                TimetableSlot slot = getChannelManager().getTimetableSlot(
                        timetableEntry.getItem().getRefID());
                if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(slot.getProgramme())) {
                    newPlaylist.addTimetableSlot(slot);
                }
            }

            return newPlaylist;
        }
        return null;
    }

    @Override
    public ActionCollectionEntry<PlaylistForm> toDTO(Playlist playlist) throws Exception {
        return toDTO(playlist, null);
    }

    @Override
    public ActionCollectionEntry<PlaylistForm> toDTO(Playlist playlist, String timeZone) throws Exception {
        if (playlist != null) {
            PlaylistForm newPlaylistForm = new PlaylistForm();

            //Fill in common properties
            newPlaylistForm.setCreatorId(playlist.getCreator() != null ? playlist.getCreator().getUsername() : null);
            newPlaylistForm.setLatestEditorId(playlist.getLatestEditor() != null ? playlist.getLatestEditor().getUsername() : null);
            newPlaylistForm.setCreationDate(playlist.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(playlist.getCreationDate().getTime(), null)) : null);
            newPlaylistForm.setLatestModificationDate(playlist.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(playlist.getLatestModificationDate().getTime(), null)) : null);

            //Fill in all attributess
            newPlaylistForm.setRefID(playlist.getRefID());
            newPlaylistForm.setLabel(playlist.getLabel());
            newPlaylistForm.setProgrammeId(
                    new ActionMapEntry<String>(
                    playlist.getProgramme().getLabel(),
                    decorateEntryWithActions(playlist.getProgramme())));
            newPlaylistForm.setDescription(playlist.getDescription());
            newPlaylistForm.setMaxUserRequestLimit(playlist.getMaxUserRequestLimit());
            newPlaylistForm.setNoRequestReplayDelay(playlist.getNoRequestReplayDelay());
            newPlaylistForm.setNoRequestReplayInPlaylist(playlist.isNoRequestReplayInPlaylist());

            newPlaylistForm.setReady(playlist.isReady());
            newPlaylistForm.setCoverArtFile(toCoverArtDTO(playlist));

            //Global tag set?
            newPlaylistForm.setGlobalTagSet(
                    toDTO(playlist.getGlobalTagSet()));

            //Live
            newPlaylistForm.setLive(toDTO(playlist.getLive()));

            //Entries
            //Make sure the entries are always sorted using a fixed order
            Collection<PlaylistEntryForm> newPlaylistEntryForms = new TreeSet<PlaylistEntryForm>();

            for (PlaylistEntry playlistEntry : playlist.getPlaylistEntries()) {
                newPlaylistEntryForms.add(toDTO(playlistEntry));
            }
            newPlaylistForm.setPlaylistEntries(newPlaylistEntryForms);

            //Timetable slots
            ActionMap<TimetableSlotFormID> newTimetables = new ActionSortedMap<TimetableSlotFormID>();

            for (TimetableSlot timetableSlot : playlist.getTimetableSlots()) {
                newTimetables.set(TimetableDTOAssembler.toIdDTO(timetableSlot, timeZone), decorateEntryWithActions(timetableSlot));
            }
            newPlaylistForm.setTimetableSlotIds(newTimetables);

            return new ActionMapEntry<PlaylistForm>(newPlaylistForm, decorateEntryWithActions(playlist));
        }
        return null;
    }

    /**
     * update an existing playlist with data from a DTO.
     * Returns true if attributes of the original playlist have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(Playlist playlist, PlaylistForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && playlist != null) {

            //Move over all regular properties and the identifier
            if ((form.getDescription() != null && !form.getDescription().equals(playlist.getDescription())
                    || (form.getDescription() == null && playlist.getDescription() != null))) {
                playlist.setDescription(form.getDescription());
                contentUpdated = true;
            }
            if ((form.getLabel() != null && !form.getLabel().equals(playlist.getLabel())
                    || (form.getLabel() == null && playlist.getLabel() != null))) {
                playlist.setLabel(form.getLabel());
                contentUpdated = true;
            }
            if ((form.getMaxUserRequestLimit() != null && !form.getMaxUserRequestLimit().equals(playlist.getMaxUserRequestLimit())
                    || (form.getMaxUserRequestLimit() == null && playlist.getMaxUserRequestLimit() != null))) {
                playlist.setMaxUserRequestLimit(form.getMaxUserRequestLimit());
                contentUpdated = true;
            }
            if ((form.getNoRequestReplayDelay() != null && !form.getNoRequestReplayDelay().equals(playlist.getNoRequestReplayDelay())
                    || (form.getNoRequestReplayDelay() == null && playlist.getNoRequestReplayDelay() != null))) {
                playlist.setNoRequestReplayDelay(form.getNoRequestReplayDelay());
                contentUpdated = true;
            }
            if (form.isNoRequestReplayInPlaylist() != playlist.isNoRequestReplayInPlaylist()) {
                playlist.setNoRequestReplayInPlaylist(form.isNoRequestReplayInPlaylist());
                contentUpdated = true;
            }

            //Global tag set?
            PlaylistTagSet newGlobalTagSet = fromDTO(form.getGlobalTagSet());
            if ((newGlobalTagSet != null && !newGlobalTagSet.equals(playlist.getGlobalTagSet())
                    || (newGlobalTagSet == null && playlist.getGlobalTagSet() != null))) {
                playlist.setGlobalTagSet(newGlobalTagSet);
                contentUpdated = true;
            }
            //Live
            Live newLive = fromDTO(form.getLive());
            if ((newLive != null && !newLive.equals(playlist.getLive())
                    || (newLive == null && playlist.getLive() != null))) {
                playlist.setLive(newLive);
                contentUpdated = true;
            }

            //Don't check if the programme bound to the playlist can be updated; this right is implied with the updatePlaylist check
            if (getProgrammeManager().checkAccessUpdatePlaylist(playlist.getProgramme())) {
                playlist.removeProgramme();
                Programme p = getProgrammeManager().getProgramme(form.getProgrammeId().getItem());
                if (getProgrammeManager().checkAccessUpdatePlaylist(p)) {
                    playlist.addProgramme(p);
                } else {
                    throw new AccessDeniedException();
                }
            }

            //Entries
            //There's no permission issue with entries: even if a user can't read a Track attached to an entry he may remove or edit the whole row if he's allowed to touch the playlist

            //Hibernate is Hell on Earth! Lots of tweaks and non-portable tricks must be carried out to programmatically detect and intercept changes between two collections

            //1) Generate the new list of entries from the DTO
            //2) for each entry in the old list check if it's included in the new list
            //2.1) if it is, then drop the matching entry in the new list
            //2.2) if it's not, drop the unmatched entry from the old list + mark the playlist as tainted
            //3) add remaining entries from the new list into the old one + mark the playlist as tainted

            Collection<PlaylistEntry> newEntries = new HashSet<PlaylistEntry>();
            for (PlaylistEntryForm playlistEntryForm : form.getPlaylistEntries()) {
                newEntries.add(
                        fromDTO(null, playlistEntryForm));
            }
            if (playlist.getPlaylistEntries().size() != newEntries.size()) {
                contentUpdated = true;
            }
            for (Iterator<PlaylistEntry> oldEntryIterator = playlist.getPlaylistEntries().iterator(); oldEntryIterator.hasNext();) {
                PlaylistEntry oldEntry = oldEntryIterator.next();

                boolean prematureBreak = false;

                for (Iterator<PlaylistEntry> newEntryIterator = newEntries.iterator(); newEntryIterator.hasNext();) {
                    PlaylistEntry newEntry = newEntryIterator.next();
                    if (newEntry.getSequenceIndex() == oldEntry.getSequenceIndex()) {
                        //Entry updated
                        if (!oldEntry.isDuplicateNoPlaylist(newEntry)) {
                            //This entry will be wiped out using "delete-orphan"
                            oldEntryIterator.remove();
                            contentUpdated = true;
                            oldEntry.removePlaylist();
                            //FIX: no rework of the hibernate mappings and ownerships, it's just that the entity must be dereferenced from *ALL* collections it belongs to before finally dropping it: the "delete-orphan" cascade doesn't work exactly like "delete"
                            if (ProxiedClassUtil.castableAs(oldEntry, StaticPlaylistEntry.class)) {
                                ((StaticPlaylistEntry) oldEntry).removeTrack();
                            }
                            //unchanged entry
                        } else {
                            newEntryIterator.remove();
                            //FIX: no rework of the hibernate mappings and ownerships, it's just that the entity must be dereferenced from *ALL* collections it belongs to before finally dropping it: the "delete-orphan" cascade doesn't work exactly like "delete"
                            if (ProxiedClassUtil.castableAs(newEntry, StaticPlaylistEntry.class)) {
                                ((StaticPlaylistEntry) newEntry).removeTrack();
                            }
                        }
                        prematureBreak = true;
                        break;
                    }
                }

                if (!prematureBreak) {
                    //This entry will be wiped out using "delete-orphan"
                    oldEntryIterator.remove();
                    contentUpdated = true;
                    oldEntry.removePlaylist();
                    //FIX: no rework of the hibernate mappings and ownerships, it's just that the entity must be dereferenced from *ALL* collections it belongs to before finally dropping it: the "delete-orphan" cascade doesn't work exactly like "delete"
                    if (ProxiedClassUtil.castableAs(oldEntry, StaticPlaylistEntry.class)) {
                        ((StaticPlaylistEntry) oldEntry).removeTrack();
                    }
                }

            }
            for (PlaylistEntry newEntry : newEntries) {
                playlist.addPlaylistEntry(newEntry);
            }

            //Programmes
            //First remove all programmes from the original channel that the authenticated user can access
            for (Iterator<TimetableSlot> i = playlist.getTimetableSlots().iterator(); i.hasNext();) {
                TimetableSlot boundSlot = i.next();
                if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(boundSlot.getProgramme())) {
                    i.remove();
                    boundSlot.removePlaylist();
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (ActionCollectionEntry<TimetableSlotFormID> slotEntry : form.getTimetableSlotIds()) {
                /*TimetableSlot t = getChannelManager().getTimetableSlot(
                new TimetableSlotLocation(
                getChannelManager().getChannel(
                slotEntry.getItem().getChannelId()),
                YearMonthWeekDayHourMinute.fromJS(
                slotEntry.getItem().getSchedulingTime())));*/
                TimetableSlot slot = getChannelManager().getTimetableSlot(
                        slotEntry.getItem().getRefID());
                if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(slot.getProgramme())) {
                    playlist.addTimetableSlot(slot);
                }
            }

        }
        return contentUpdated;
    }

    @Override
    public CoverArtModule toCoverArtDTO(Playlist playlist) throws Exception {
        if (playlist != null && playlist.getCoverArtContainer() != null) {
            PictureFileInfo info = playlist.getCoverArtContainer();
            CoverArtModule container = new CoverArtModule();

            //Format
            container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));

            //metadata
            container.setSize(info.getSize());

            //Download info
            container.setURL(
                    getDatastoreService().getGWTFlyerDownloadURLEndpointForUser(playlist.getRefID()));

            return container;
        }
        return null;
    }

    @Override
    public CoverArtModule toCoverArtDTO(PictureStream pictureStream, String id, boolean isDraft) throws Exception {
        if (pictureStream != null && pictureStream.getContainer() != null) {
            PictureFileInfo info = pictureStream.getContainer();
            CoverArtModule container = new CoverArtModule();

            //Format
            container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));

            //metadata
            container.setSize(info.getSize());

            //Download info
            container.setURL(//
                isDraft ? //
                        getDatastoreService().getGWTDraftPictureDownloadURLEndpointForUser(id)//
                    ://
                        getDatastoreService().getGWTFlyerDownloadURLEndpointForUser(id)//
            );

            return container;
        }
        return null;
    }

    @Override
    public ActionCollection<PlaylistForm> toDTO(Collection<Playlist> playlists, boolean doSort) throws Exception {
        return toDTO(playlists, null, doSort);
    }

    @Override
    public ActionCollection<PlaylistForm> toDTO(Collection<Playlist> playlists, String timeZone, boolean doSort) throws Exception {
        ActionMap<PlaylistForm> playlistForms = doSort ? new ActionSortedMap<PlaylistForm>() : new ActionUnsortedMap<PlaylistForm>();

        for (Playlist playlist : playlists) {
            playlistForms.set(toDTO(playlist, timeZone));
        }
        return playlistForms;
    }

    protected Collection<ACTION> decorateEntryWithActions(Programme programme) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewProgramme(programme)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Curators and Contributors for linking programmes to tracks, they cannot edit a programme yet they can bind tracks
        try {
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programme)) {
                actions.add(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(TimetableSlot timetableSlot) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewTimetableSlot(timetableSlot.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Animators for linking playlists to timetable slots, they cannot edit a timetable yet but they can bind playlists
        try {
            if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(timetableSlot.getProgramme())) {
                actions.add(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateTimetableSlot(timetableSlot.getChannel())) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteTimetableSlot(timetableSlot.getChannel())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Playlist playlist) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewPlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Animators for linking playlists to timetable slots, they cannot edit a timetable yet but they can bind playlists
        try {
            if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(playlist.getProgramme())) {
                actions.add(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdatePlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeletePlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(BroadcastableTrack track) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getTrackManager().checkAccessViewBroadcastTrack(track)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessExtendedViewBroadcastTrack(track)) {
                actions.add(TRACK_ACTION.EXTENDED_VIEW);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessUpdateBroadcastTrack(track)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessDeleteBroadcastTrack(track)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    @Override
    public Collection<Channel> getImmutableChannelSet(PlaylistForm playlist) throws Exception {
        Set<Channel> boundChannels = new HashSet<Channel>();
        if (playlist != null) {
            Programme boundProgramme = getProgrammeManager().getProgramme(playlist.getProgrammeId().getItem());
            boundChannels.addAll(boundProgramme.getChannels());
        }
        return boundChannels;
    }

    @Override
    public Programme getImmutableProgramme(PlaylistForm playlist) throws Exception {
        if (playlist != null) {
            return getProgrammeManager().getProgramme(playlist.getProgrammeId().getItem());
        }
        return null;
    }

    @Override
    public Collection<TimetableSlot> getImmutableTimetableSlotSet(PlaylistForm playlist) throws Exception {
        Set<TimetableSlot> boundSlots = new HashSet<TimetableSlot>();
        if (playlist != null) {
            Collection<TimetableSlotFormID> _slotIds = playlist.getTimetableSlotIds().keySet();
            Collection<String> slotIds = new HashSet<String>();
            if (_slotIds != null) {
                for (TimetableSlotFormID _slotId : _slotIds) {
                    slotIds.add(_slotId.getRefID());
                }
            }
            boundSlots.addAll(getChannelManager().getTimetableSlots(slotIds));
        }
        return boundSlots;
    }
}
