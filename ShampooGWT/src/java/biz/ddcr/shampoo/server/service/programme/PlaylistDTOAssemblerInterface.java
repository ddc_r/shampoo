/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import java.util.Collection;


/**
 *
 * @author okay_awright
 **/
public interface PlaylistDTOAssemblerInterface {
    //DTO to domain conversion
    public Playlist fromDTO(PlaylistForm playlistForm) throws Exception;
    public boolean augmentWithDTO(Playlist playist, PlaylistForm playlistForm) throws Exception;

    //Domain to DTO conversion
    public ActionCollectionEntry<PlaylistForm> toDTO(Playlist playlist, String timeZone) throws Exception;
    public ActionCollectionEntry<PlaylistForm> toDTO(Playlist playlist) throws Exception;

    //Helpers
    public ActionCollection<PlaylistForm> toDTO(Collection<Playlist> playlists, String timeZone, boolean doSort) throws Exception;
    public ActionCollection<PlaylistForm> toDTO(Collection<Playlist> playlists, boolean doSort) throws Exception;
    public Collection<Channel> getImmutableChannelSet(PlaylistForm playlist) throws Exception;
    public Programme getImmutableProgramme(PlaylistForm playlist) throws Exception;
    public Collection<TimetableSlot> getImmutableTimetableSlotSet(PlaylistForm playlist) throws Exception;

    public CoverArtModule toCoverArtDTO(Playlist playlist) throws Exception;
    public CoverArtModule toCoverArtDTO(PictureStream pictureStream, String id, boolean isDraft) throws Exception;

}
