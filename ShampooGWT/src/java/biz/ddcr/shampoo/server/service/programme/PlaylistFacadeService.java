/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.PlaylistNotReadyException;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.log.PlaylistLog.PLAYLIST_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistContentNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistContentNotification.PLAYLIST_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.playlist.log.PlaylistLog;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification.PLAYLISTPROGRAMME_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistTimetableSlotLinkNotification.PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.helper.MicroTask;
import biz.ddcr.shampoo.server.helper.TaskCallback;
import biz.ddcr.shampoo.server.helper.TaskPerformer;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import org.springframework.dao.DataIntegrityViolationException;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnit;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface.PlaylistFiles;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.springframework.core.task.TaskExecutor;

/**
 *
 * @author okay_awright
 *
 */
public class PlaylistFacadeService extends GenericService implements PlaylistFacadeServiceInterface {

    /**
     * Internal interim struct
     *
     */
    protected class AsynchronousDeleteOperation extends AsynchronousUnmanagedTransactionUnit<Playlist> implements AsynchronousDeletePlaylistTransactionInterface {

        AsynchronousDeleteOperation() throws Exception {
            super();
            //Init
            idsAndBackups = new HashMap<String, PlaylistForm>();
        }
        protected Map<String, PlaylistForm> idsAndBackups;

        @Override
        public void add(String id) {
            if (id != null) {
                idsAndBackups.put(id, null);
            }
        }

        @Override
        public void init() throws Exception {
            //Mark all tracks to delete as unavailable
            clearTransactions();
            synchronousSafePreDeletePlaylists(idsAndBackups);
        }

        @Override
        public void processing() throws Exception {
            //Remove all files attached to the tracks that have no remaining programme attached to them
            //otherwise just reset their activation state
            addTransactions(synchronousSafePostDeletePlaylists(idsAndBackups));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to do, really
        }

        @Override
        public void rollback(Exception originalException) {
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Rollback any modification performed during initialization() and mark them as ready
                Collection<Playlist> cleansedPlaylists = new HashSet<Playlist>();
                for (PlaylistForm backup : idsAndBackups.values()) {
                    Playlist contaminatedPlaylist = getProgrammeManager().loadPlaylist(backup.getRefID());
                    getPlaylistDTOAssembler().augmentWithDTO(contaminatedPlaylist, backup);
                    contaminatedPlaylist.setReady(true);
                    cleansedPlaylists.add(contaminatedPlaylist);
                }
                getProgrammeManager().updatePlaylists(cleansedPlaylists);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    protected class AsynchronousAddOperation extends AsynchronousUnmanagedTransactionUnit<Playlist> implements AsynchronousAddPlaylistTransactionInterface {

        AsynchronousAddOperation() throws Exception {
            super();
            //Init
            files = new HashMap<String, PlaylistFiles>();
            playlists = new HashSet<PlaylistForm>();
        }
        protected Map<String, PlaylistFiles> files;
        protected Collection<PlaylistForm> playlists;

        @Override
        public void add(PlaylistForm playlist, String coverArtUploadId) {
            //if trackUploadId or coverArtUploadId is null, it means the corresponding entry for the track has to be removed
            //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
            if (playlist != null) {
                if (playlist.getRefID() == null) {
                    playlist.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }
                PlaylistFiles newPlaylistFile = new PlaylistFiles(
                        true, coverArtUploadId);
                files.put(playlist.getRefID(), newPlaylistFile);
                playlists.add(playlist);
            }
        }

        @Override
        public void add(PlaylistForm playlist) {
            //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
            if (playlist != null) {
                if (playlist.getRefID() == null) {
                    playlist.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }            
                files.put(playlist.getRefID(), null);
                playlists.add(playlist);
            }
        }

        @Override
        public void init() throws Exception {
            clearTransactions();
            synchronousSafePreAddPlaylists(playlists);
        }

        @Override
        public void processing() throws Exception {
            //Add and bind all files to the tracks that have just been inserted
            addTransactions(synchronousSafePostAddPlaylists(files));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to perform
        }

        @Override
        public void rollback(Exception originalException) {
            //Cancel the persistance of the changes in the datastore
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Remove all files that have been inserted during initialization()
                Collection<Playlist> playlistsToUpdate = getProgrammeManager().loadPlaylists(files.keySet());
                getProgrammeManager().deletePlaylists(playlistsToUpdate);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    protected class AsynchronousUpdateOperation extends AsynchronousUnmanagedTransactionUnit<Playlist> implements AsynchronousUpdatePlaylistTransactionInterface {

        AsynchronousUpdateOperation() throws Exception {
            super();
            //Init
            files = new HashMap<String, PlaylistFiles>();
            playlists = new HashSet<PlaylistForm>();
            idsAndBackups = new HashMap<String, PlaylistForm>();
        }
        protected Map<String, PlaylistFiles> files;
        protected Collection<PlaylistForm> playlists;
        protected Map<String, PlaylistForm> idsAndBackups;
        protected Collection<String> contentUpdatedIds;

        @Override
        public void add(PlaylistForm playlist, String coverArtUploadId) {
            //if playlistUploadId or coverArtUploadId is null, it means the corresponding entry for the playlist has to be removed
            if (playlist != null) {
                playlists.add(playlist);
                PlaylistFiles newPlaylistFile = new PlaylistFiles(
                        true, coverArtUploadId);
                files.put(playlist.getRefID(), newPlaylistFile);
            }
        }

        @Override
        public void add(PlaylistForm playlist) {
            if (playlist != null) {
                playlists.add(playlist);
                files.put(playlist.getRefID(), null);
            }
        }

        @Override
        public void init() throws Exception {
            //Mark all playlists to update as unavailable
            contentUpdatedIds = synchronousSafePreUpdatePlaylists(playlists, idsAndBackups, files);
        }

        @Override
        public void processing() throws Exception {
            //Add and bind all files to the playlists that have just been inserted
            addTransactions(synchronousSafePostUpdatePlaylists(idsAndBackups, files, contentUpdatedIds));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to perform
        }

        @Override
        public void rollback(Exception originalException) {
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Rollback any modification performed during initialization() and mark them as ready
                Collection<Playlist> cleansedPlaylists = new HashSet<Playlist>();
                for (PlaylistForm backup : idsAndBackups.values()) {
                    Playlist contaminatedPlaylist = getProgrammeManager().loadPlaylist(backup.getRefID());
                    getPlaylistDTOAssembler().augmentWithDTO(contaminatedPlaylist, backup);
                    contaminatedPlaylist.setReady(true);
                    cleansedPlaylists.add(contaminatedPlaylist);
                }
                getProgrammeManager().updatePlaylists(cleansedPlaylists);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };
    private PlaylistDTOAssemblerInterface playlistDTOAssembler;
    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private SecurityManagerInterface securityManager;
    //Run sequential steps in an asynchronous thread
    private transient TaskPerformer taskPerformer;
    private AsynchronousPlaylistFacadeServiceHelperInterface asynchronousPlaylistFacadeServiceHelper;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public PlaylistDTOAssemblerInterface getPlaylistDTOAssembler() {
        return playlistDTOAssembler;
    }

    public void setPlaylistDTOAssembler(PlaylistDTOAssemblerInterface playlistDTOAssembler) {
        this.playlistDTOAssembler = playlistDTOAssembler;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskPerformer = new TaskPerformer(taskExecutor);
    }

    public AsynchronousPlaylistFacadeServiceHelperInterface getAsynchronousPlaylistFacadeServiceHelper() {
        return asynchronousPlaylistFacadeServiceHelper;
    }

    public void setAsynchronousPlaylistFacadeServiceHelper(AsynchronousPlaylistFacadeServiceHelperInterface asynchronousPlaylistFacadeServiceHelper) {
        this.asynchronousPlaylistFacadeServiceHelper = asynchronousPlaylistFacadeServiceHelper;
    }

    /**
     *
     * Validation process for playlists Incorrect values are replaced with
     * default ones unless the problem is not recoverable, then an Exception is
     * thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     *
     */
    private boolean checkPlaylist(Playlist playlist) throws Exception {
        boolean result = false;

        if (playlist != null) {

            result = true;
        }
        return result;
    }

    protected void startAsynchronousTask(AsynchronousUnmanagedTransactionUnitInterface<Playlist> asynchronousPlaylistOps) throws Exception {
        Collection<AsynchronousUnmanagedTransactionUnitInterface<Playlist>> asynchronousPlaylistOpsCollection = new HashSet<AsynchronousUnmanagedTransactionUnitInterface<Playlist>>();
        asynchronousPlaylistOpsCollection.add(asynchronousPlaylistOps);
        startAsynchronousTask(asynchronousPlaylistOpsCollection);
    }

    /**
     * MUST be called outside any transactionnal scope. Will use transactional
     * methods once embedded within an asynchronous workers.
     *
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     *
     */
    protected void startAsynchronousTask(final Collection<AsynchronousUnmanagedTransactionUnitInterface<Playlist>> asynchronousPlaylistOps) throws Exception {
        //No batch version of this method exists, process those tracks one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (asynchronousPlaylistOps != null) {

            //First step: initialization of units of work
            getAsynchronousPlaylistFacadeServiceHelper().synchronousInitialization(asynchronousPlaylistOps);

            //Second step: actual processing
            MicroTask loop = new MicroTask() {
                @Override
                public boolean loop() throws Exception {
                    getAsynchronousPlaylistFacadeServiceHelper().synchronousProcessing(asynchronousPlaylistOps);
                    return false;
                }
            };
            TaskCallback loopCallback = new TaskCallback() {
                @Override
                public void onSuccess() throws Exception {
                    getAsynchronousPlaylistFacadeServiceHelper().synchronousCommit(asynchronousPlaylistOps);
                }

                @Override
                public void onFailure(Exception e) throws Exception {
                    getAsynchronousPlaylistFacadeServiceHelper().synchronousRollback(asynchronousPlaylistOps, e);
                }
            };
            taskPerformer.runTask(loop, loopCallback);

        }
    }

    /**
     *
     * Returns -1 if the playlist cannot be added, the newly affected identifier
     * for the playlist otherwise
     *
     * @param playlistForm
     * @return
     * @throws Exception
     *
     */
    @Override
    public void addSecuredPlaylist(PlaylistForm playlistForm) throws Exception {

        if (playlistForm != null) {

            //hydrate the entity from the DTO
            Playlist playlist = playlistDTOAssembler.fromDTO(playlistForm);
            if (getProgrammeManager().checkAccessAddPlaylist(playlist.getProgramme())) {

                if (checkPlaylist(playlist)) {
                    try {
                        //Mark it as ready since no files will be attached
                        playlist.setReady(true);
                        getProgrammeManager().addPlaylist(playlist);
                        //Log it
                        log(PLAYLIST_LOG_OPERATION.add, playlist);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        if (playlist.getProgramme() != null) {
                            notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.add, playlist, playlist.getProgramme());
                        }
                        for (TimetableSlot boundSlot : playlist.getTimetableSlots()) {
                            notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, playlist, boundSlot);
                        }

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }

            } else {
                throw new AccessDeniedException();
            }

        }

    }

    protected void synchronousSafePreAddPlaylists(Collection<PlaylistForm> playlistForms) throws Exception {
        if (playlistForms != null) {

            //Check if it can be accessed
            if (getProgrammeManager().checkAccessAddPlaylists()) {

                Collection<Playlist> addedPlaylists = new HashSet<Playlist>();
                for (PlaylistForm playlistForm : playlistForms) {
                    //hydrate the entity from the DTO
                    Playlist playlist = playlistDTOAssembler.fromDTO(playlistForm);
                    if (checkPlaylist(playlist)) {
                        //Mark it as not ready
                        playlist.setReady(false);
                        //IMPORTANT: update the original form with the new id!
                        playlist.setRefID(playlistForm.getRefID());
                        addedPlaylists.add(playlist);
                    }
                }
                //Everything's fine, now make it persistent in the database
                try {
                    //Add all playlists to insert
                    getProgrammeManager().addPlaylists(addedPlaylists);
                    //Log and notify only when the actual file copy has been performed
                } catch (DataIntegrityViolationException e) {
                    throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                }
            } else {
                throw new AccessDeniedException();
            }

        }

    }

    protected Collection<Integer> synchronousSafePostAddPlaylists(
            Map<String, PlaylistFiles> playlistIdsAndFiles) throws Exception {

        if (playlistIdsAndFiles != null) {
            Collection<Integer> transactions = null;
            Map<Playlist, PlaylistFiles> playlistsToReset = new HashMap<Playlist, PlaylistFiles>();

            final Collection<? extends Playlist> playlists = getProgrammeManager().loadPlaylists(playlistIdsAndFiles.keySet());
            if (playlists != null) {
                for (Playlist playlist : playlists) {

                    //reset its activation state
                    playlist.setReady(true);
                    playlistsToReset.put(playlist, playlistIdsAndFiles.get(playlist.getRefID()));
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    log(PLAYLIST_LOG_OPERATION.add, playlist);
                    //Notify responsible users of any changes
                    //All linked entities are then new links
                    if (playlist.getProgramme() != null) {
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.add, playlist, playlist.getProgramme());
                    }
                    for (TimetableSlot boundSlot : playlist.getTimetableSlots()) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, playlist, boundSlot);
                    }

                }
            }
            if (!playlistsToReset.isEmpty()) {
                transactions = getDatastoreService().movePlaylistFilesFromPoolToDataStore(playlistsToReset);
                getProgrammeManager().updatePlaylists(playlistsToReset.keySet());
            }
            return transactions;
        }
        return null;
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once
     * embedded within an asynchronous workers.
     *
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     *
     */
    @Override
    public void asynchronousAddSecuredPlaylistAndPictureFile(final PlaylistForm playlistForm, final String coverArtUploadId) throws Exception {
        if (playlistForm != null) {

            AsynchronousAddPlaylistTransactionInterface op = new AsynchronousAddOperation();
            op.add(playlistForm, coverArtUploadId);
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    @Override
    public void asynchronousDeleteSecuredPlaylist(String playlistId) throws Exception {
        if (playlistId != null) {
            Collection<String> playlistIds = new HashSet<String>();
            playlistIds.add(playlistId);
            asynchronousDeleteSecuredPlaylists(playlistIds);
        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once
     * embedded within an asynchronous workers.
     *
     * @param playlistForm
     * @param playlistUploadId
     * @throws Exception
     *
     */
    @Override
    public void asynchronousDeleteSecuredPlaylists(Collection<String> playlistIds) throws Exception {
        //No batch version of this method exists, process those playlists one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (playlistIds != null) {

            AsynchronousDeletePlaylistTransactionInterface op = new AsynchronousDeleteOperation();
            for (String playlistId : playlistIds) {
                op.add(playlistId);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once
     * embedded within an asynchronous workers.
     *
     * @param playlistForm
     * @param playlistUploadId
     * @throws Exception
     *
     */
    @Override
    public void asynchronousUpdateSecuredPlaylistAndPictureFile(final PlaylistForm playlistForm, final String coverArtUploadId) throws Exception {
        if (playlistForm != null) {

            AsynchronousUpdatePlaylistTransactionInterface op = new AsynchronousUpdateOperation();
            op.add(playlistForm, coverArtUploadId);
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    @Override
    public void updateSecuredPlaylist(PlaylistForm playlistForm) throws Exception {

        if (playlistForm != null) {

            //Get the original version of this programme
            //It will be this version that will be modified with data coming from the DTO
            Playlist originalPlaylist = getProgrammeManager().loadPlaylist(playlistForm.getRefID());

            //Then check if it can be accessed
            if (getProgrammeManager().checkAccessUpdatePlaylist(originalPlaylist.getProgramme())) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = PLAYLIST_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalPlaylist);
                //Save original links before modification (clones)
                Programme originallyLinkedProgramme = originalPlaylist.getProgramme();
                Set<TimetableSlot> originallyLinkedSlots = new HashSet(originalPlaylist.getTimetableSlots());

                //Update the existing entity from the DTO
                boolean areAttributesUpdated = playlistDTOAssembler.augmentWithDTO(originalPlaylist, playlistForm);

                //Check and adjust its dependencies before updating
                if (checkPlaylist(originalPlaylist)) {

                    //No attached file, so it's ready by default
                    originalPlaylist.setReady(true);
                    //Everything's fine, now make it persistent in the database
                    getProgrammeManager().updatePlaylist(originalPlaylist);
                    //Log it
                    log(PLAYLIST_LOG_OPERATION.edit, originalPlaylist, originallyBoundChannels);
                    //Notify responsible users of any changes
                    if (areAttributesUpdated) //Only users still linked to the playlist after update will receive this notification
                    {
                        notify(PLAYLIST_NOTIFICATION_OPERATION.edit, originalPlaylist);
                    }
                    //Linked programme changed
                    if (!originallyLinkedProgramme.equals(originalPlaylist.getProgramme())) {
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.add, originalPlaylist, originalPlaylist.getProgramme());
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, originalPlaylist, originallyLinkedProgramme);
                    }
                    //New linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(originallyLinkedSlots, originalPlaylist.getTimetableSlots())) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, originalPlaylist, boundSlot);
                    }
                    //Old linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(originalPlaylist.getTimetableSlots(), originallyLinkedSlots)) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, originalPlaylist, boundSlot);
                    }

                }

            } else {
                throw new AccessDeniedException();
            }

        }

    }

    /**
     * Returns the id list of track whose content has been modified (attributes,
     * not relationships)
     *
     * @param idsAndForms
     * @param idsAndBackups
     * @return
     * @throws Exception
     */
    protected Collection<String> synchronousSafePreUpdatePlaylists(
            Collection<? extends PlaylistForm> playlistForms,
            Map<String, PlaylistForm> idsAndBackups,
            Map<String, PlaylistFiles> idsAndFiles) throws Exception {

        HashSet<String> contentModifiedPlaylistIds = new HashSet<String>();
        if (playlistForms != null && idsAndBackups != null) {
            Map<String, PlaylistForm> idsAndForms = new HashMap<String, PlaylistForm>();
            for (PlaylistForm playlistForm : playlistForms) {
                idsAndForms.put(playlistForm.getRefID(), playlistForm);
            }
            if (!idsAndForms.isEmpty()) {
                contentModifiedPlaylistIds.addAll(
                        synchronousSafePreUpdatePlaylists(idsAndForms, idsAndBackups, idsAndFiles));
            }
        }
        return contentModifiedPlaylistIds;
    }

    /**
     * Returns the id list of track whose content has been modified (attributes,
     * not relationships)
     *
     * @param idsAndForms
     * @param idsAndBackups
     * @return
     * @throws Exception
     */
    protected Collection<String> synchronousSafePreUpdatePlaylists(
            Map<String, PlaylistForm> idsAndForms,
            Map<String, PlaylistForm> idsAndBackups,
            Map<String, PlaylistFiles> idsAndFiles) throws Exception {

        HashSet<String> contentUpdatedIds = new HashSet<String>();
        boolean isContentUpdated;
        if (idsAndForms != null && idsAndBackups != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            Collection<Playlist> originalPlaylists = getProgrammeManager().loadPlaylists(idsAndForms.keySet());
            Collection<Playlist> modifiedPlaylists = new HashSet<Playlist>();
            for (Playlist originalPlaylist : originalPlaylists) {

                if (getProgrammeManager().checkAccessUpdatePlaylist(originalPlaylist.getProgramme()) && isPlaylistTouchable(originalPlaylist)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<PlaylistForm> rollbackCopyEntry = playlistDTOAssembler.toDTO(originalPlaylist);
                    PlaylistForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;

                    //First get a copy of the entity from a DTO
                    isContentUpdated = playlistDTOAssembler.augmentWithDTO(originalPlaylist, idsAndForms.get(originalPlaylist.getRefID()));
                    //Check if either the playlist file or the cover sleeve needs update or both
                    if (!isContentUpdated) {
                        isContentUpdated = idsAndFiles.containsKey(originalPlaylist.getRefID());
                    }

                    //Check and adjust its dependencies before updating
                    if (checkPlaylist(originalPlaylist)) {

                        originalPlaylist.setReady(false);
                        modifiedPlaylists.add(originalPlaylist);
                        //Update the referenced id list with rollback copies
                        idsAndBackups.put(originalPlaylist.getRefID(), rollbackCopy);

                        if (isContentUpdated) {
                            contentUpdatedIds.add(originalPlaylist.getRefID());
                        }

                    }
                } else {
                    throw new AccessDeniedException();
                }

                getProgrammeManager().updatePlaylists(modifiedPlaylists);
            }
        }
        return contentUpdatedIds;
    }

    protected Collection<Integer> synchronousSafePostUpdatePlaylists(
            Map<String, PlaylistForm> playlistIdsAndBackups,
            Map<String, PlaylistFiles> playlistIdsAndFiles,
            Collection<String> contentUpdatedIds) throws Exception {

        if (playlistIdsAndBackups != null && playlistIdsAndFiles != null) {
            Collection<Integer> transactions = null;
            Map<Playlist, PlaylistFiles> playlistsToReset = new HashMap<Playlist, PlaylistFiles>();

            final Collection<? extends Playlist> playlists = getProgrammeManager().loadPlaylists(playlistIdsAndBackups.keySet());
            if (playlists != null) {
                for (Playlist playlist : playlists) {

                    //reset its activation state
                    playlist.setReady(true);
                    playlistsToReset.put(playlist, playlistIdsAndFiles.get(playlist.getRefID()));
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    log(PLAYLIST_LOG_OPERATION.edit, playlist,
                            playlistDTOAssembler.getImmutableChannelSet(playlistIdsAndBackups.get(playlist.getRefID())));
                    //Notify responsible users of any changes
                    //Hibernate proxies cannot be directly cast so reload the object with the proper type
                    //TODO do something clever with a visitor pattern
                    //FIX don't use session load() but get() within the DAO
                    if (contentUpdatedIds != null && contentUpdatedIds.contains(playlist.getRefID())) //Only users still linked to the playlist after update will receive this notification
                    {
                        notify(PLAYLIST_NOTIFICATION_OPERATION.edit, playlist);
                    }
                    //Linked programme changed
                    Programme originallyLinkedProgramme = playlistDTOAssembler.getImmutableProgramme(playlistIdsAndBackups.get(playlist.getRefID()));
                    if (!originallyLinkedProgramme.equals(playlist.getProgramme())) {
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.add, playlist, playlist.getProgramme());
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, playlist, originallyLinkedProgramme);
                    }
                    Collection<TimetableSlot> originallyLinkedSlots = playlistDTOAssembler.getImmutableTimetableSlotSet(playlistIdsAndBackups.get(playlist.getRefID()));
                    //New linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(originallyLinkedSlots, playlist.getTimetableSlots())) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.add, playlist, boundSlot);
                    }
                    //Old linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(playlist.getTimetableSlots(), originallyLinkedSlots)) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, playlist, boundSlot);
                    }

                }
            }
            if (!playlistsToReset.isEmpty()) {
                transactions = getDatastoreService().movePlaylistFilesFromPoolToDataStore(playlistsToReset);
                getProgrammeManager().updatePlaylists(playlistsToReset.keySet());
            }
            return transactions;
        }
        return null;
    }

    private void processPlaylistForDeletion(Playlist playlist) throws Exception {

        if (playlist != null) {

            playlist.clearPlaylistEntries();
            playlist.clearTimetableSlots();

        }

    }

    @Override
    public void deleteSecuredPlaylist(String id) throws Exception {

        if (id != null) {

            Playlist playlist = getProgrammeManager().loadPlaylist(id);
            //Then check if it can be accessed

            if (getProgrammeManager().checkAccessDeletePlaylist(playlist.getProgramme())) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = PLAYLIST_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), playlist);
                //Save original links before modification (clones)
                Programme originallyLinkedProgramme = playlist.getProgramme();
                Set<TimetableSlot> originallyLinkedSlots = new HashSet(playlist.getTimetableSlots());

                //Don't check if the programme bound to the playlist can be updated; this right is implied with the deletePlaylist check
                processPlaylistForDeletion(playlist);
                //If a user can delete a playlist, then it doesn't matter if he can access its timetable slots or not
                getProgrammeManager().deletePlaylist(playlist);
                //Log it
                log(PLAYLIST_LOG_OPERATION.delete, playlist, originallyBoundChannels);
                //Notify responsible users of any changes
                //All links have been successfully removed
                notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, playlist, originallyLinkedProgramme);
                for (TimetableSlot boundSlot : originallyLinkedSlots) {
                    notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, playlist, boundSlot);
                }
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void deleteSecuredPlaylists(Collection<String> ids) throws Exception {

        if (ids != null) {

            Collection<Playlist> playlistsToDelete = new HashSet<Playlist>();

            for (String id : ids) {

                Playlist playlist = getProgrammeManager().loadPlaylist(id);
                //Then check if it can be accessed

                if (getProgrammeManager().checkAccessDeletePlaylist(playlist.getProgramme()) && isPlaylistTouchable(playlist)) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = PLAYLIST_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), playlist);
                    //Save original links before modification (clones)
                    Programme originallyLinkedProgramme = playlist.getProgramme();
                    Set<TimetableSlot> originallyLinkedSlots = new HashSet(playlist.getTimetableSlots());

                    //Don't check if the programme bound to the playlist can be updated; this right is implied with the deletePlaylist check
                    processPlaylistForDeletion(playlist);
                    //If a user can delete a playlist, then it doesn't matter if he can access its timetable slots or not
                    playlistsToDelete.add(playlist);
                    //Log it
                    log(PLAYLIST_LOG_OPERATION.delete, playlist, originallyBoundChannels);
                    //Notify responsible users of any changes
                    //All links have been successfully removed
                    notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, playlist, originallyLinkedProgramme);
                    for (TimetableSlot boundSlot : originallyLinkedSlots) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, playlist, boundSlot);
                    }
                } else {
                    throw new AccessDeniedException();
                }
            }

            //Everything's fine, now make it persistent in the database
            if (!playlistsToDelete.isEmpty()) {
                getProgrammeManager().deletePlaylists(playlistsToDelete);
            }

        }
    }

    protected void synchronousSafePreDeletePlaylists(Map<String, PlaylistForm> playlistIdsAndBackups) throws Exception {
        if (playlistIdsAndBackups != null) {

            //Get the original version of this playlist
            //It will be this version that will be modified with data coming from the DTO
            Collection<Playlist> playlists = getProgrammeManager().loadPlaylists(playlistIdsAndBackups.keySet());
            Collection<Playlist> deletedPlaylists = new HashSet<Playlist>();
            for (Playlist playlist : playlists) {

                if (getProgrammeManager().checkAccessDeletePlaylist(playlist.getProgramme()) && isPlaylistTouchable(playlist)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<? extends PlaylistForm> rollbackCopyEntry = playlistDTOAssembler.toDTO(playlist);
                    PlaylistForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;
                    //Check if it can be accessed

                    //Then remove dependencies
                    processPlaylistForDeletion(playlist);

                    playlist.setReady(false);
                    deletedPlaylists.add(playlist);
                    //Update the referenced id list with rollback copies
                    playlistIdsAndBackups.put(playlist.getRefID(), rollbackCopy);

                } else {
                    throw new AccessDeniedException();
                }

            }
            getProgrammeManager().updatePlaylists(deletedPlaylists);
        }

    }

    protected Collection<Integer> synchronousSafePostDeletePlaylists(
            Map<String, PlaylistForm> playlistIdsAndBackups) throws Exception {

        if (playlistIdsAndBackups != null) {
            Collection<Integer> transactions = null;
            Collection<Playlist> playlistsToDelete = new HashSet<Playlist>();

            final Collection<? extends Playlist> playlists = getProgrammeManager().loadPlaylists(playlistIdsAndBackups.keySet());
            if (playlists != null) {
                for (Playlist playlist : playlists) {

                    playlistsToDelete.add(playlist);
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    PlaylistForm rollbackCopy = playlistIdsAndBackups.get(playlist.getRefID());
                    log(PLAYLIST_LOG_OPERATION.delete, playlist,
                            playlistDTOAssembler.getImmutableChannelSet(rollbackCopy));
                    //Notify responsible users of any changes
                    //All links have been successfully removed
                    //use the rollback copy to know which one was originally linked to the playlist
                    notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, playlist, playlistDTOAssembler.getImmutableProgramme(rollbackCopy));
                    for (TimetableSlot boundSlot : playlistDTOAssembler.getImmutableTimetableSlotSet(rollbackCopy)) {
                        notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION.delete, playlist, boundSlot);
                    }
                }
            }
            if (!playlistsToDelete.isEmpty()) {
                transactions = getDatastoreService().unsetPlaylistFilesFromDatastore(playlistsToDelete);
                getProgrammeManager().deletePlaylists(playlistsToDelete);
            }
            return transactions;
        }
        return null;
    }

    @Override
    public ActionCollectionEntry<PlaylistForm> getSecuredPlaylist(String id, String localTimeZone) throws Exception {

        Playlist playlist = getProgrammeManager().loadPlaylist(id);
        ActionCollectionEntry<? extends PlaylistForm> playlistEntry = playlistDTOAssembler.toDTO(playlist, localTimeZone);
        //Check if the currently authenticated user can access this resource
        if (playlistEntry != null && playlistEntry.isReadable()) {

            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(PLAYLIST_LOG_OPERATION.read, playlist);

            return (ActionCollectionEntry<PlaylistForm>) playlistEntry;
        } else {
            throw new AccessDeniedException();
        }

    }

    @Override
    public ActionCollection<PlaylistForm> getSecuredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint, String localTimeZone) throws Exception {
        Collection<Playlist> playlists = getProgrammeManager().getFilteredPlaylists(constraint);
        ActionCollection<PlaylistForm> forms = playlistDTOAssembler.toDTO(playlists, localTimeZone, constraint == null || constraint.getSortByFeature() == null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PLAYLIST_LOG_OPERATION.read, playlists);
        return forms;
    }

    @Override
    public ActionCollection<PlaylistForm> getSecuredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId, String localTimeZone) throws Exception {
        Collection<Playlist> playlists = getProgrammeManager().getFilteredPlaylistsForProgrammeId(constraint, programmeId);
        ActionCollection<PlaylistForm> forms = playlistDTOAssembler.toDTO(playlists, localTimeZone, constraint == null || constraint.getSortByFeature() == null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PLAYLIST_LOG_OPERATION.read, playlists);
        return forms;
    }

    @Override
    public PictureStream getSecuredPictureStreamFromDataStore(String playlistId) throws Exception {
        //fileId must be a Long representing a playlist Id
        //Whoever can view (simple read) a programme can download the coverart picture
        Playlist playlist = getProgrammeManager().getPlaylist(playlistId);
        if (getProgrammeManager().checkAccessViewProgramme(playlist.getProgramme())) {
            return getDatastoreService().getPictureStreamFromDataStore(playlist);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public PictureStream getSecuredPictureStreamFromSessionPool(String coverArtUploadId) throws Exception {
        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().getPictureStreamFromSessionPool(coverArtUploadId);
    }

    protected void log(PLAYLIST_LOG_OPERATION operation, Collection<Playlist> playlists) throws Exception {
        if (playlists != null && !playlists.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<PlaylistLog> logs = new HashSet<PlaylistLog>();
            for (Playlist playlist : playlists) {
                logs.add(new PlaylistLog(operation, playlist.getRefID(), playlist.getLabel(), operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), playlist)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(PLAYLIST_LOG_OPERATION operation, Playlist playlist) throws Exception {
        log(operation, playlist, null);
    }

    protected void log(PLAYLIST_LOG_OPERATION operation, Playlist playlist, Collection<Channel> supplementaryChannels) throws Exception {
        if (playlist != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), playlist);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(new PlaylistLog(operation, playlist.getRefID(), playlist.getLabel(), allBoundChannels));
        }
    }

    protected void notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION operation, Playlist playlist, Programme programme) throws Exception {
        notify(operation, playlist, programme, operation.getRecipients(getSecurityManager(), playlist, programme));
    }

    protected void notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION operation, Playlist playlist, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (playlist != null && programme != null && recipients != null) {
            Collection<PlaylistProgrammeLinkNotification> notifications = new HashSet<PlaylistProgrammeLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new PlaylistProgrammeLinkNotification(operation, playlist, programme, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION operation, Playlist playlist, TimetableSlot timetableSlot) throws Exception {
        notify(operation, playlist, timetableSlot, operation.getRecipients(getSecurityManager(), playlist, timetableSlot));
    }

    protected void notify(PLAYLISTTIMETABLESLOT_NOTIFICATION_OPERATION operation, Playlist playlist, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (playlist != null && timetableSlot != null && recipients != null) {
            Collection<PlaylistTimetableSlotLinkNotification> notifications = new HashSet<PlaylistTimetableSlotLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new PlaylistTimetableSlotLinkNotification(operation, playlist, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PLAYLIST_NOTIFICATION_OPERATION operation, Playlist playlist) throws Exception {
        notify(operation, playlist, operation.getRecipients(getSecurityManager(), playlist));
    }

    protected void notify(PLAYLIST_NOTIFICATION_OPERATION operation, Playlist playlist, Collection<RestrictedUser> recipients) throws Exception {
        if (playlist != null && recipients != null) {
            Collection<PlaylistContentNotification> notifications = new HashSet<PlaylistContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new PlaylistContentNotification(operation, playlist, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected boolean isPlaylistTouchable(Playlist playlist) throws Exception {

        if (playlist == null) {
            throw new NoEntityException();
        }

        if (!playlist.isReady()) {
            throw new PlaylistNotReadyException();
        }

        return true;
    }
}
