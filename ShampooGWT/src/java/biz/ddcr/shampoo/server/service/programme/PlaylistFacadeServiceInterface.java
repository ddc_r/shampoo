/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import java.util.Collection;


/**
 *
 * @author okay_awright
 **/
public interface PlaylistFacadeServiceInterface {

    public interface AsynchronousDeletePlaylistTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Playlist> {
        public void add(String id);
    }

    public interface AsynchronousAddPlaylistTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Playlist> {
        public void add(PlaylistForm playlist, String coverArtUploadId);
        public void add(PlaylistForm playlist);
    }

    public interface AsynchronousUpdatePlaylistTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Playlist> {
        public void add(PlaylistForm track, String coverArtUploadId);
        public void add(PlaylistForm track);
    }

    public void addSecuredPlaylist(PlaylistForm playlistForm) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousAddSecuredPlaylistAndPictureFile(PlaylistForm playlistForm, String coverArtUploadId) throws Exception;

    public void updateSecuredPlaylist(PlaylistForm playlistForm) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredPlaylistAndPictureFile(PlaylistForm playlistForm, String coverArtUploadId) throws Exception;

    public void deleteSecuredPlaylist(String id) throws Exception;
    public void deleteSecuredPlaylists(Collection<String> ids) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousDeleteSecuredPlaylist(String playlistId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousDeleteSecuredPlaylists(Collection<String> playlistIds) throws Exception;

    
    public ActionCollectionEntry<PlaylistForm> getSecuredPlaylist(String id, String localTimeZone) throws Exception;   
    public ActionCollection<PlaylistForm> getSecuredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint, String localTimeZone) throws Exception;   
    public ActionCollection<PlaylistForm> getSecuredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId, String localTimeZone) throws Exception;

    public PictureStream getSecuredPictureStreamFromDataStore(String playlistId) throws Exception;

    public PictureStream getSecuredPictureStreamFromSessionPool(String coverArtUploadId) throws Exception;

}
