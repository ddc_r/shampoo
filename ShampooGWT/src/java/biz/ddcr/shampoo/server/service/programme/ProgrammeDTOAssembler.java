/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.channel.CHANNEL_ACTION;
import biz.ddcr.shampoo.client.form.playlist.PLAYLIST_ACTION;
import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.user.RESTRICTEDUSER_ACTION;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeDTOAssembler extends GenericService implements ProgrammeDTOAssemblerInterface {

    private ProgrammeManagerInterface programmeManager;
    private ChannelManagerInterface channelManager;
    private UserManagerInterface userManager;
    private PlaylistDTOAssemblerInterface playlistDTOAssembler;

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public PlaylistDTOAssemblerInterface getPlaylistDTOAssembler() {
        return playlistDTOAssembler;
    }

    public void setPlaylistDTOAssembler(PlaylistDTOAssemblerInterface playlistDTOAssembler) {
        this.playlistDTOAssembler = playlistDTOAssembler;
    }

    @Override
    public Programme fromDTO(ProgrammeForm programmeForm) throws Exception {
        if (programmeForm != null) {

            Programme newProgramme = new Programme();

            //Fill in all attributes
            newProgramme.setMetaInfoPattern(programmeForm.getMetaInfoPattern());
            newProgramme.setOverlappingPlaylistAllowed(programmeForm.isOverlappingPlaylist());
            newProgramme.setDescription(programmeForm.getDescription());
            newProgramme.setLabel(programmeForm.getLabel());

            //Playlists
            for (ActionCollectionEntry<String> playlistEntry : programmeForm.getPlaylists()) {
                Playlist playlist = getProgrammeManager().getPlaylist(playlistEntry.getKey());
                if (getProgrammeManager().checkAccessUpdatePlaylist(playlist.getProgramme()))
                    newProgramme.addPlaylist(playlist);
            }

            //Channels
            for (ActionCollectionEntry<String> channelEntry : programmeForm.getChannels()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(c))
                    newProgramme.addChannel(c);
            }

            //Rights
            for (ActionCollectionEntry<String> userEntry : programmeForm.getAnimatorRights()) {
                RestrictedUser u = getUserManager().getRestrictedUser(userEntry.getKey());
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(u))
                    newProgramme.addAnimatorRight(u);
            }
            for (ActionCollectionEntry<String> userEntry : programmeForm.getCuratorRights()) {
                RestrictedUser u = getUserManager().getRestrictedUser(userEntry.getKey());
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(u))
                    newProgramme.addCuratorRight(u);
            }
            for (ActionCollectionEntry<String> userEntry : programmeForm.getContributorRights()) {
                RestrictedUser u = getUserManager().getRestrictedUser(userEntry.getKey());
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(u))
                    newProgramme.addContributorRight(u);
            }

            return newProgramme;
        }
        return null;
    }

    /**
     * update an existing programme with data from a DTO.
     * Returns true if attributes of the original programme have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(Programme programme, ProgrammeForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && programme != null) {

            if ((form.getDescription()!=null && !form.getDescription().equals(programme.getDescription())
                    || (form.getDescription()==null && programme.getDescription()!=null))) {
                programme.setDescription(form.getDescription());
                contentUpdated = true;
            }
            if ((form.getLabel()!=null && !form.getLabel().equals(programme.getLabel())
                    || (form.getLabel()==null && programme.getLabel()!=null))) {
                programme.setLabel(form.getLabel());
                contentUpdated = true;
            }
            if ((form.getMetaInfoPattern()!=null && !form.getMetaInfoPattern().equals(programme.getMetaInfoPattern())
                    || (form.getMetaInfoPattern()==null && programme.getMetaInfoPattern()!=null))) {
                programme.setMetaInfoPattern(form.getMetaInfoPattern());
                contentUpdated = true;
            }
            if (form.isOverlappingPlaylist()!=programme.isOverlappingPlaylistAllowed()) {
                programme.setOverlappingPlaylistAllowed(form.isOverlappingPlaylist());
                contentUpdated = true;
            }

            //First remove all playlists from the original programme that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            Collection<Playlist> playlistEntries = new HashSet<Playlist>();
            for (Playlist p : getProgrammeManager().getPlaylists(form.getPlaylists().keySet())) {
                //Re-inject the new playlists, if the playlist has been previously removed then its playlist is now null and will automatically be included
                if (p.getProgramme()==null || getProgrammeManager().checkAccessUpdatePlaylist(p.getProgramme())) {
                    playlistEntries.add(p);
                }
            }

            for (Iterator<Playlist> i = programme.getPlaylists().iterator(); i.hasNext();) {
                Playlist boundPlaylist = i.next();
                if (getProgrammeManager().checkAccessUpdatePlaylist(boundPlaylist.getProgramme())) {
                    i.remove();
                    programme.removePlaylist(boundPlaylist);
                }
            }

            //Then add back to the lists the ones provided by the DTO
            for (Playlist playlistEntry : playlistEntries) {
                //Re-inject the new playlists, if the playlist has been previously removed then its playlist is now null and will automatically be included
                programme.addPlaylist(playlistEntry);
            }

            //First remove all channels from the original programme that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            Collection<Channel> channelEntries = new HashSet<Channel>();
            for (Channel c : getChannelManager().getChannels(form.getChannels().keySet())) {
                //Re-inject the new playlists, if the playlist has been previously removed then its playlist is now null and will automatically be included
                if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(c)) {
                    channelEntries.add(c);
                }
            }

            for (Iterator<Channel> i = programme.getChannels().iterator(); i.hasNext();) {
                Channel boundChannel = i.next();
                if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(boundChannel)) {
                    i.remove();
                    programme.removeChannel(boundChannel);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (Channel c : channelEntries) {
                programme.addChannel(c);
            }

            //First remove all the rights from the original user that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            Collection<RestrictedUser> formUserEntries = new HashSet<RestrictedUser>();
            for (RestrictedUser u : getUserManager().getRestrictedUsers(form.getAnimatorRights().keySet())) {
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(u)) {
                    formUserEntries.add(u);
                }
            }

            for (Iterator<RestrictedUser> i = programme.getAnimatorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    programme.removeAnimatorRight(boundUser);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (RestrictedUser u : formUserEntries) {
                programme.addAnimatorRight(u);
            }

            //First remove all the rights from the original user that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            formUserEntries = new HashSet<RestrictedUser>();
            for (RestrictedUser u : getUserManager().getRestrictedUsers(form.getContributorRights().keySet())) {
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(u)) {
                    formUserEntries.add(u);
                }
            }

            for (Iterator<RestrictedUser> i = programme.getContributorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    programme.removeContributorRight(boundUser);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (RestrictedUser u : formUserEntries) {
                programme.addContributorRight(u);
            }

            //First remove all the rights from the original user that the authenticated user can access
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            //We must load them first since the forms only reference names and their links will disappear when refreshed
            formUserEntries = new HashSet<RestrictedUser>();
            for (RestrictedUser u : getUserManager().getRestrictedUsers(form.getCuratorRights().keySet())) {
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(u)) {
                    formUserEntries.add(u);
                }
            }

            for (Iterator<RestrictedUser> i = programme.getCuratorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    programme.removeCuratorRight(boundUser);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (RestrictedUser u : formUserEntries) {
                programme.addCuratorRight(u);
            }

        }
        return contentUpdated;
    }

    @Override
    public ActionCollectionEntry<ProgrammeForm> toDTO(Programme programme) throws Exception {
        if (programme == null) {
            return null;
        }

        ProgrammeForm programmeForm = new ProgrammeForm();

        //Playlists
        ActionMap<String> newPlaylists = new ActionSortedMap<String>();
        for (Playlist playlist : programme.getPlaylists()) {
            newPlaylists.set(playlist.getRefID(), decorateEntryWithActions(playlist));
        }
        programmeForm.setPlaylists(newPlaylists);

        //Fill in common properties
        programmeForm.setCreatorId(programme.getCreator() != null ? programme.getCreator().getUsername() : null);
        programmeForm.setLatestEditorId(programme.getLatestEditor() != null ? programme.getLatestEditor().getUsername() : null);
        programmeForm.setCreationDate(programme.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(programme.getCreationDate().getTime(), null)) : null);
        programmeForm.setLatestModificationDate(programme.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(programme.getLatestModificationDate().getTime(), null)) : null);

        //Fill in all attributes
        programmeForm.setMetaInfoPattern(programme.getMetaInfoPattern());
        programmeForm.setOverlappingPlaylist(programme.isOverlappingPlaylistAllowed());
        programmeForm.setDescription(programme.getDescription());
        programmeForm.setLabel(programme.getLabel());

        //Channels
        ActionMap<String> channelEntries = new ActionSortedMap<String>();
        if (programme.getChannels() != null) {
            for (Channel channel : programme.getChannels()) {
                channelEntries.set(channel.getLabel(), decorateEntryWithActions(channel));
            }
        }
        programmeForm.setChannels(channelEntries);

        //Rights
        ActionMap<String> newAnimatorRights = new ActionSortedMap<String>();
        for (RestrictedUser user : programme.getAnimatorRights()) {
            newAnimatorRights.set(user.getUsername(), decorateEntryWithActions(user));
        }
        programmeForm.setAnimatorRights(newAnimatorRights);
        ActionMap<String> newCuratorRights = new ActionSortedMap<String>();
        for (RestrictedUser user : programme.getCuratorRights()) {
            newCuratorRights.set(user.getUsername(), decorateEntryWithActions(user));
        }
        programmeForm.setCuratorRights(newCuratorRights);
        ActionMap<String> newContributorRights = new ActionSortedMap<String>();
        for (RestrictedUser user : programme.getContributorRights()) {
            newContributorRights.set(user.getUsername(), decorateEntryWithActions(user));
        }
        programmeForm.setContributorRights(newContributorRights);

        return new ActionMapEntry<ProgrammeForm>(programmeForm, decorateEntryWithActions(programme));
    }

    @Override
    public ActionCollection<String> toStringDTO(Collection<? extends Programme> programmes, boolean doSort) throws Exception {
        ActionMap<String> programmeStrings = doSort ? new ActionSortedMap<String>() : new ActionUnsortedMap<String>();
        for (Programme programme : programmes) {
            programmeStrings.set(programme.getLabel(), decorateEntryWithActions(programme));
        }
        return programmeStrings;
    }

    @Override
    public ActionCollection<? extends ProgrammeForm> toDTO(Collection<? extends Programme> programmes, boolean doSort) throws Exception {
        ActionMap<ProgrammeForm> programmeForms = doSort ? new ActionSortedMap<ProgrammeForm>() : new ActionUnsortedMap<ProgrammeForm>();
        for (Programme programme : programmes) {
            programmeForms.set(toDTO(programme));
        }
        return programmeForms;
    }

    protected Collection<ACTION> decorateEntryWithActions(Playlist playlist) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewPlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Animators for linking playlists to timetable slots, they cannot edit a timetable yet but they can bind playlists
        try {
            if (getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(playlist.getProgramme())) {
                actions.add(PLAYLIST_ACTION.LIMITED_TIMETABLE_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdatePlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeletePlaylist(playlist.getProgramme())) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(RestrictedUser user) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getUserManager().checkAccessViewRestrictedUser(user)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(user)) {
                actions.add(RESTRICTEDUSER_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessUpdateRestrictedUser(user)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessDeleteRestrictedUser(user)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Channel channel) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewChannel(channel)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(channel)) {
                actions.add(CHANNEL_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateChannel(channel)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteChannel(channel)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Programme programme) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewProgramme(programme)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Curators and Contributors for linking programmes to tracks, they cannot edit a programme yet they can bind tracks
        try {
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programme)) {
                actions.add(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }
}
