/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.MalformedTextPatternExpressionException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeLinkNotification.CHANNELPROGRAMME_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification;
import biz.ddcr.shampoo.server.domain.playlist.notification.PlaylistProgrammeLinkNotification.PLAYLISTPROGRAMME_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.log.ProgrammeLog.PROGRAMME_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import org.springframework.dao.DataIntegrityViolationException;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContentNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContentNotification.PROGRAMME_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.log.ProgrammeLog;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification.PROGRAMMEANIMATOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification.PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification.PROGRAMMECURATOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeTimetableSlotLinkNotification.PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.streamer.metadata.DisplayMetadataPatternResolver;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeFacadeService extends GenericService implements ProgrammeFacadeServiceInterface {

    private ProgrammeDTOAssemblerInterface programmeDTOAssembler;
    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private SecurityManagerInterface securityManager;

    public ProgrammeDTOAssemblerInterface getProgrammeDTOAssembler() {
        return programmeDTOAssembler;
    }

    public void setProgrammeDTOAssembler(ProgrammeDTOAssemblerInterface programmeDTOAssembler) {
        this.programmeDTOAssembler = programmeDTOAssembler;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /**
     *
     * Validation process for programmes
     * Incorrect values are replaced with default ones unless the problem is not recoverable, then an Exception is thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean checkProgramme(Programme programme) throws Exception {
        boolean result = false;

        if (programme != null) {

            //We must check the Apache Strsubstitutor variable as soon as possible, otherwise it will likely mess up with the whole queue
            try {
                DisplayMetadataPatternResolver.check(programme.getMetaInfoPattern());
            } catch (IllegalArgumentException e) {
                throw new MalformedTextPatternExpressionException(programme.getMetaInfoPattern());
            }

            result = true;

        }
        return result;
    }

    /**
     * Add a programme to the database if the authenticated user can access it
     * Check every rights and programme s belonging to the user to add and strip them down if the authenticated user cannot access them
     * Tracks and timetableSlots are not managed here
     *
     * @param programmeForm
     * @throws Exception
     **/
    @Override
    public void addSecuredProgramme(ProgrammeForm programmeForm) throws Exception {

        if (programmeForm != null) {

            //Then check if it can be accessed
            if (getProgrammeManager().checkAccessAddProgrammes()) {

                //hydrate the entity from the DTO
                Programme programme = programmeDTOAssembler.fromDTO(programmeForm);

                if (checkProgramme(programme)) {
                    //Everything's fine, now make it persistent in the database
                    try {

                        getProgrammeManager().addProgramme(programme);
                        //Log it
                        log(PROGRAMME_LOG_OPERATION.add, programme);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        for (RestrictedUser boundUser : programme.getAnimatorRights()) {
                            notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.add, programme, boundUser);
                        }
                        for (RestrictedUser boundUser : programme.getContributorRights()) {
                            notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.add, programme, boundUser);
                        }
                        for (RestrictedUser boundUser : programme.getCuratorRights()) {
                            notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.add, programme, boundUser);
                        }
                        for (Channel boundChannel : programme.getChannels()) {
                            notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.add, boundChannel, programme);
                        }
                        //Tracks and TimetableSlots are not handled on this side of the relationship

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    /**
     *
     * Update a programme if the authenticated user can access it
     * Do only add or remove rights and programme s bound to the programme that the authenticated user can acccess
     * Tracks and timetableSlots are not managed here
     *
     * @param programmeForm
     * @throws Exception
     **/
    @Override
    public void updateSecuredProgramme(ProgrammeForm programmeForm) throws Exception {

        if (programmeForm != null) {

            //Get the original version of this programme
            //It will be this version that will be modified with data coming from the DTO
            Programme originalProgramme = getProgrammeManager().loadProgramme(programmeForm.getLabel());
            //Check if it can be accessed

            if (getProgrammeManager().checkAccessUpdateProgramme(originalProgramme)) {

                //Copy all originally bound channels before touching them
                Collection<Channel> originallyBoundChannels = PROGRAMME_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalProgramme);
                //Save original links before modification (clones)
                Set<Channel> originallyLinkedChannels = new HashSet<Channel>(originalProgramme.getChannels());
                Set<RestrictedUser> originallyLinkedAnimators = new HashSet<RestrictedUser>(originalProgramme.getAnimatorRights());
                Set<RestrictedUser> originallyLinkedContributors = new HashSet<RestrictedUser>(originalProgramme.getContributorRights());
                Set<RestrictedUser> originallyLinkedCurators = new HashSet<RestrictedUser>(originalProgramme.getCuratorRights());

                //Update the existing entity from the DTO
                boolean areAttributesUpdated = programmeDTOAssembler.augmentWithDTO(originalProgramme, programmeForm);

                if (checkProgramme(originalProgramme)) {

                    //Everything's fine, now make it persistent in the database
                    getProgrammeManager().updateProgramme(originalProgramme);
                    //Log it
                    log(PROGRAMME_LOG_OPERATION.edit, originalProgramme, originallyBoundChannels);
                    //Notify responsible users of any changes
                    if (areAttributesUpdated) //Only users still linked to the playlist after update will receive this notification
                    {
                        notify(PROGRAMME_NOTIFICATION_OPERATION.edit, originalProgramme);
                    }
                    //New linked channels
                    for (Channel boundChannel : CollectionUtil.exclusion(originallyLinkedChannels, originalProgramme.getChannels())) {
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.add, boundChannel, originalProgramme);
                    }
                    //Old linked channels
                    for (Channel boundChannel : CollectionUtil.exclusion(originalProgramme.getChannels(), originallyLinkedChannels)) {
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, boundChannel, originalProgramme);
                    }
                    //New linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originallyLinkedAnimators, originalProgramme.getAnimatorRights())) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.add, originalProgramme, boundUser);
                    }
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originalProgramme.getAnimatorRights(), originallyLinkedAnimators)) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, originalProgramme, boundUser);
                    }
                    //New linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originallyLinkedContributors, originalProgramme.getContributorRights())) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.add, originalProgramme, boundUser);
                    }
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originalProgramme.getContributorRights(), originallyLinkedContributors)) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, originalProgramme, boundUser);
                    }
                    //New linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originallyLinkedCurators, originalProgramme.getCuratorRights())) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.add, originalProgramme, boundUser);
                    }
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(originalProgramme.getCuratorRights(), originallyLinkedCurators)) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, originalProgramme, boundUser);
                    }

                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    /**
     *
     * Pre-process a programme for removal
     * Outputs true if this entity must be removed, false for updating it instead
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean processProgrammeForDeletion(Programme programme) throws Exception {
        //Flag to know whether to update or delete the channel regarding how the checks end up
        boolean mustBeRemoved = true;

        if (programme != null) {

            //And every channel of its
            //Don't throw an exception if a programme  cannot be accessed, just remove it from the list and make sure the programme is updated, not removed actually
            //for (Channel boundChannel : programme.getChannels()) {
            for (Iterator<Channel> i = programme.getChannels().iterator(); i.hasNext();) {
                Channel boundChannel = i.next();
                if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(boundChannel)) {
                    i.remove();
                    programme.removeChannel(boundChannel);
                } else {
                    mustBeRemoved = false;
                }
            }

            //And every rights of its
            //Don't throw an exception if a right cannot be accessed, just remove it from the list and make sure the programme is updated, not removed actually
            //for (RestrictedUser boundUser : programme.getAnimatorRights()) {
            for (Iterator<RestrictedUser> i = programme.getAnimatorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    programme.removeAnimatorRight(boundUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (RestrictedUser boundUser : programme.getContributorRights()) {
            for (Iterator<RestrictedUser> i = programme.getContributorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    programme.removeContributorRight(boundUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (RestrictedUser boundUser : programme.getCuratorRights()) {
            for (Iterator<RestrictedUser> i = programme.getCuratorRights().iterator(); i.hasNext();) {
                RestrictedUser boundUser = i.next();
                if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(boundUser)) {
                    i.remove();
                    programme.removeCuratorRight(boundUser);
                } else {
                    mustBeRemoved = false;
                }
            }

            //Whatever happens, someone who can remove a programme can unlink a playlist and other stuff: no need for extra check
            //Cascade delete will take care of their unlinking
            for (Playlist playlist : programme.getPlaylists()) {
                playlist.clearPlaylistEntries();
            }
            programme.clearPlaylists();
            programme.clearTrackProgrammes();
            programme.clearTimetableSlots();

        }

        return mustBeRemoved;
    }

    /**
     *
     * Delete a programme from the database if the authenticated user can access it
     * If the authenticated user cannot access all users or programme s bound to this user then unlink those that can be accessed and do not actually remove the programme, but update it
     * Otherwise fully drop the programme from database
     * Tracks and timetableSlots are managed here
     *
     * @param userForm
     * @throws Exception
     **/
    @Override
    public void deleteSecuredProgramme(String id) throws Exception {

        if (id != null) {

            //First get a copy of the entity from a DTO
            //Programme programme = programmeDTOAssembler.fromDTO(programmeForm);
            Programme programme = getProgrammeManager().loadProgramme(id);
            //Then check if it can be accessed

            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = PROGRAMME_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), programme);
                //Save original links before modification (clones)
                Set<Channel> originallyLinkedChannels = new HashSet(programme.getChannels());
                Set<RestrictedUser> originallyLinkedAnimators = new HashSet(programme.getAnimatorRights());
                Set<RestrictedUser> originallyLinkedContributors = new HashSet(programme.getContributorRights());
                Set<RestrictedUser> originallyLinkedCurators = new HashSet(programme.getCuratorRights());
                Set<Playlist> originallyLinkedPlaylists = new HashSet(programme.getPlaylists());
                Set<TimetableSlot> originallyLinkedSlots = new HashSet(programme.getTimetableSlots());
                final Set<BroadcastableTrack> originallyLinkedBroadcastableTracks = new HashSet<BroadcastableTrack>();
                final Set<PendingTrack> originallyLinkedPendingTracks = new HashSet<PendingTrack>();
                for (Track track : programme.getImmutableTracks()) {
                    TrackVisitor visitor = new TrackVisitor() {

                        @Override
                        public void visit(PendingAdvert track) throws Exception {
                            originallyLinkedPendingTracks.add( track);
                        }

                        @Override
                        public void visit(PendingJingle track) throws Exception {
                            originallyLinkedPendingTracks.add( track);
                        }

                        @Override
                        public void visit(PendingSong track) throws Exception {
                            originallyLinkedPendingTracks.add( track);
                        }

                        @Override
                        public void visit(BroadcastableAdvert track) throws Exception {
                            originallyLinkedBroadcastableTracks.add( track);
                        }

                        @Override
                        public void visit(BroadcastableJingle track) throws Exception {
                            originallyLinkedBroadcastableTracks.add( track);
                        }

                        @Override
                        public void visit(BroadcastableSong track) throws Exception {
                            originallyLinkedBroadcastableTracks.add( track);
                        }
                    };
                    track.acceptVisit(visitor);
                }


                //Flag to know whether to update or delete the programme regarding how the checks end up
                if (processProgrammeForDeletion(programme)) {

                    //Everything's fine, now make it persistent in the database
                    getProgrammeManager().deleteProgramme(programme);
                    //Log it
                    log(PROGRAMME_LOG_OPERATION.delete, programme, originallyBoundChannels);
                    //Notify responsible users of any changes
                    //All links have been successfully removed
                    for (Channel boundChannel : originallyLinkedChannels) {
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, boundChannel, programme);
                    }
                    for (RestrictedUser boundUser : originallyLinkedAnimators) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                    }
                    for (RestrictedUser boundUser : originallyLinkedContributors) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                    }
                    for (RestrictedUser boundUser : originallyLinkedCurators) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                    }
                    for (Playlist boundPlaylist : originallyLinkedPlaylists) {
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, boundPlaylist, programme);
                    }
                    for (TimetableSlot boundSlot : originallyLinkedSlots) {
                        notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, programme, boundSlot);
                    }
                    for (BroadcastableTrack boundTrack : originallyLinkedBroadcastableTracks) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                    }
                    for (PendingTrack boundTrack : originallyLinkedPendingTracks) {
                        notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                    }

                } else {

                    getProgrammeManager().updateProgramme(programme);
                    //Log it
                    log(PROGRAMME_LOG_OPERATION.edit, programme, originallyBoundChannels);
                    //Notify responsible users of any changes
                    //Old linked programmes
                    for (Channel boundChannel : CollectionUtil.exclusion(programme.getChannels(), originallyLinkedChannels)) {
                        notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, boundChannel, programme);
                    }
                    //Old linked users
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(programme.getAnimatorRights(), originallyLinkedAnimators)) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                    }
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(programme.getContributorRights(), originallyLinkedContributors)) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                    }
                    for (RestrictedUser boundUser : CollectionUtil.exclusion(programme.getCuratorRights(), originallyLinkedCurators)) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                    }
                    //Old linked playlists
                    for (Playlist boundPlaylist : CollectionUtil.exclusion(programme.getPlaylists(), originallyLinkedPlaylists)) {
                        notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, boundPlaylist, programme);
                    }
                    //Old linked slots
                    for (TimetableSlot boundSlot : CollectionUtil.exclusion(programme.getTimetableSlots(), originallyLinkedSlots)) {
                        notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, programme, boundSlot);
                    }
                    //Old linked tracks
                    final Set<BroadcastableTrack> newLinkedBroadcastableTracks = new HashSet<BroadcastableTrack>();
                    final Set<PendingTrack> newLinkedPendingTracks = new HashSet<PendingTrack>();
                    for (Track track : programme.getImmutableTracks()) {
                        TrackVisitor visitor = new TrackVisitor() {

                            @Override
                            public void visit(PendingAdvert track) throws Exception {
                                newLinkedPendingTracks.add( track);
                            }

                            @Override
                            public void visit(PendingJingle track) throws Exception {
                                newLinkedPendingTracks.add( track);
                            }

                            @Override
                            public void visit(PendingSong track) throws Exception {
                                newLinkedPendingTracks.add( track);
                            }

                            @Override
                            public void visit(BroadcastableAdvert track) throws Exception {
                                newLinkedBroadcastableTracks.add( track);
                            }

                            @Override
                            public void visit(BroadcastableJingle track) throws Exception {
                                newLinkedBroadcastableTracks.add( track);
                            }

                            @Override
                            public void visit(BroadcastableSong track) throws Exception {
                                newLinkedBroadcastableTracks.add( track);
                            }
                        };
                        track.acceptVisit(visitor);
                    }
                    for (BroadcastableTrack boundTrack : CollectionUtil.exclusion(newLinkedBroadcastableTracks, originallyLinkedBroadcastableTracks)) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                    }
                    for (PendingTrack boundTrack : CollectionUtil.exclusion(newLinkedPendingTracks, originallyLinkedPendingTracks)) {
                        notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                    }

                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    public void deleteSecuredProgrammes(Collection<String> ids) throws Exception {
        if (ids != null) {

            Collection programmesToUpdate = new HashSet<Programme>();
            Collection programmesToDelete = new HashSet<Programme>();
            for (String id : ids) {
                //First get a copy of the entity from a DTO
                Programme programme = getProgrammeManager().loadProgramme(id);

                if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = PROGRAMME_LOG_OPERATION.delete.getChannels(getSecurityManager().getSystemConfigurationHelper(), programme);
                    //Save original links before modification (clones)
                    Set<Channel> originallyLinkedChannels = new HashSet(programme.getChannels());
                    Set<RestrictedUser> originallyLinkedAnimators = new HashSet(programme.getAnimatorRights());
                    Set<RestrictedUser> originallyLinkedContributors = new HashSet(programme.getContributorRights());
                    Set<RestrictedUser> originallyLinkedCurators = new HashSet(programme.getCuratorRights());
                    Set<Playlist> originallyLinkedPlaylists = new HashSet(programme.getPlaylists());
                    Set<TimetableSlot> originallyLinkedSlots = new HashSet(programme.getTimetableSlots());
                    final Set<BroadcastableTrack> originallyLinkedBroadcastableTracks = new HashSet<BroadcastableTrack>();
                    final Set<PendingTrack> originallyLinkedPendingTracks = new HashSet<PendingTrack>();
                    for (Track track : programme.getImmutableTracks()) {
                        TrackVisitor visitor = new TrackVisitor() {

                            @Override
                            public void visit(PendingAdvert track) throws Exception {
                                originallyLinkedPendingTracks.add( track);
                            }

                            @Override
                            public void visit(PendingJingle track) throws Exception {
                                originallyLinkedPendingTracks.add( track);
                            }

                            @Override
                            public void visit(PendingSong track) throws Exception {
                                originallyLinkedPendingTracks.add( track);
                            }

                            @Override
                            public void visit(BroadcastableAdvert track) throws Exception {
                                originallyLinkedBroadcastableTracks.add( track);
                            }

                            @Override
                            public void visit(BroadcastableJingle track) throws Exception {
                                originallyLinkedBroadcastableTracks.add( track);
                            }

                            @Override
                            public void visit(BroadcastableSong track) throws Exception {
                                originallyLinkedBroadcastableTracks.add( track);
                            }
                        };
                        track.acceptVisit(visitor);
                    }

                    //Flag to know whether to update or delete the programme regarding how the checks end up
                    if (processProgrammeForDeletion(programme)) {

                        programmesToDelete.add(programme);
                        //Log it
                        log(PROGRAMME_LOG_OPERATION.delete, programme, originallyBoundChannels);
                        //Notify responsible users of any changes
                        //All links have been successfully removed
                        for (Channel boundChannel : originallyLinkedChannels) {
                            notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, boundChannel, programme);
                        }
                        for (RestrictedUser boundUser : originallyLinkedAnimators) {
                            notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                        }
                        for (RestrictedUser boundUser : originallyLinkedContributors) {
                            notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                        }
                        for (RestrictedUser boundUser : originallyLinkedCurators) {
                            notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                        }
                        for (Playlist boundPlaylist : originallyLinkedPlaylists) {
                            notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, boundPlaylist, programme);
                        }
                        for (TimetableSlot boundSlot : originallyLinkedSlots) {
                            notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, programme, boundSlot);
                        }
                        for (BroadcastableTrack boundTrack : originallyLinkedBroadcastableTracks) {
                            notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                        }
                        for (PendingTrack boundTrack : originallyLinkedPendingTracks) {
                            notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                        }

                    } else {

                        programmesToUpdate.add(programme);
                        //Log it
                        log(PROGRAMME_LOG_OPERATION.edit, programme, originallyBoundChannels);
                        //Notify responsible users of any changes
                        //Old linked programmes
                        for (Channel boundChannel : CollectionUtil.exclusion(programme.getChannels(), originallyLinkedChannels)) {
                            notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION.delete, boundChannel, programme);
                        }
                        //Old linked users
                        for (RestrictedUser boundUser : CollectionUtil.exclusion(programme.getAnimatorRights(), originallyLinkedAnimators)) {
                            notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                        }
                        for (RestrictedUser boundUser : CollectionUtil.exclusion(programme.getContributorRights(), originallyLinkedContributors)) {
                            notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                        }
                        for (RestrictedUser boundUser : CollectionUtil.exclusion(programme.getCuratorRights(), originallyLinkedCurators)) {
                            notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, programme, boundUser);
                        }
                        //Old linked playlists
                        for (Playlist boundPlaylist : CollectionUtil.exclusion(programme.getPlaylists(), originallyLinkedPlaylists)) {
                            notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION.delete, boundPlaylist, programme);
                        }
                        //Old linked slots
                        for (TimetableSlot boundSlot : CollectionUtil.exclusion(programme.getTimetableSlots(), originallyLinkedSlots)) {
                            notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION.delete, programme, boundSlot);
                        }
                        //Old linked tracks
                        final Set<BroadcastableTrack> newLinkedBroadcastableTracks = new HashSet<BroadcastableTrack>();
                        final Set<PendingTrack> newLinkedPendingTracks = new HashSet<PendingTrack>();
                        for (Track track : programme.getImmutableTracks()) {
                            TrackVisitor visitor = new TrackVisitor() {

                                @Override
                                public void visit(PendingAdvert track) throws Exception {
                                    newLinkedPendingTracks.add( track);
                                }

                                @Override
                                public void visit(PendingJingle track) throws Exception {
                                    newLinkedPendingTracks.add( track);
                                }

                                @Override
                                public void visit(PendingSong track) throws Exception {
                                    newLinkedPendingTracks.add( track);
                                }

                                @Override
                                public void visit(BroadcastableAdvert track) throws Exception {
                                    newLinkedBroadcastableTracks.add( track);
                                }

                                @Override
                                public void visit(BroadcastableJingle track) throws Exception {
                                    newLinkedBroadcastableTracks.add( track);
                                }

                                @Override
                                public void visit(BroadcastableSong track) throws Exception {
                                    newLinkedBroadcastableTracks.add( track);
                                }
                            };
                            track.acceptVisit(visitor);
                        }
                        for (BroadcastableTrack boundTrack : CollectionUtil.exclusion(newLinkedBroadcastableTracks, originallyLinkedBroadcastableTracks)) {
                            notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                        }
                        for (PendingTrack boundTrack : CollectionUtil.exclusion(newLinkedPendingTracks, originallyLinkedPendingTracks)) {
                            notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, boundTrack, programme);
                        }

                    }
                } else {
                    throw new AccessDeniedException();
                }

            }

            //Everything's fine, now make it persistent in the database
            if (!programmesToUpdate.isEmpty()) {
                getProgrammeManager().updateProgrammes(programmesToUpdate);
            }
            if (!programmesToDelete.isEmpty()) {
                getProgrammeManager().deleteProgrammes(programmesToDelete);
            }
        }
    }

    /**
     *
     * Fetch a programme from the database if the authenticated user can access it
     *
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public ActionCollectionEntry<ProgrammeForm> getSecuredProgramme(String id) throws Exception {

        Programme programme = getProgrammeManager().loadProgramme(id);
        ActionCollectionEntry<ProgrammeForm> programmeFormEntry = programmeDTOAssembler.toDTO(programme);
        //Check if the currently authenticated user can access this resource
        if (programmeFormEntry == null || programmeFormEntry.isReadable()) {

            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(PROGRAMME_LOG_OPERATION.read, programme);

            return programmeFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint) throws Exception {
        Collection<Programme> programmes = getProgrammeManager().getFilteredProgrammes(constraint);
        ActionCollection<ProgrammeForm> forms = (ActionCollection<ProgrammeForm>) programmeDTOAssembler.toDTO(programmes, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROGRAMME_LOG_OPERATION.read, programmes);
        return forms;
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForChannelId(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId) throws Exception {
        Collection<Programme> programmes = getProgrammeManager().getFilteredProgrammesForChannelId(constraint, channelId);
        ActionCollection<ProgrammeForm> forms = (ActionCollection<ProgrammeForm>) programmeDTOAssembler.toDTO(programmes, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROGRAMME_LOG_OPERATION.read, programmes);
        return forms;
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForTrackId(GroupAndSort<PROGRAMME_TAGS> constraint, String trackId) throws Exception {
        Collection<Programme> programmes = getProgrammeManager().getFilteredProgrammesForTrackId(constraint, trackId);
        ActionCollection<ProgrammeForm> forms = (ActionCollection<ProgrammeForm>) programmeDTOAssembler.toDTO(programmes, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROGRAMME_LOG_OPERATION.read, programmes);
        return forms;
    }

    @Override
   public ActionCollection<ProgrammeForm> getSecuredProgrammesForSongAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception {
        Collection<Programme> programmes = getProgrammeManager().getFilteredProgrammesForSongAndProgrammeIds(author, title, programmeIds.keySet());
        ActionCollection<ProgrammeForm> forms = (ActionCollection<ProgrammeForm>) programmeDTOAssembler.toDTO(programmes, true);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROGRAMME_LOG_OPERATION.read, programmes);
        return forms;
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForAdvertAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception {
        Collection<Programme> programmes = getProgrammeManager().getFilteredProgrammesForAdvertAndProgrammeIds(author, title, programmeIds.keySet());
        ActionCollection<ProgrammeForm> forms = (ActionCollection<ProgrammeForm>) programmeDTOAssembler.toDTO(programmes, true);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROGRAMME_LOG_OPERATION.read, programmes);
        return forms;
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForJingleAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception {
        Collection<Programme> programmes = getProgrammeManager().getFilteredProgrammesForJingleAndProgrammeIds(author, title, programmeIds.keySet());
        ActionCollection<ProgrammeForm> forms = (ActionCollection<ProgrammeForm>) programmeDTOAssembler.toDTO(programmes, true);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROGRAMME_LOG_OPERATION.read, programmes);
        return forms;
    }

    protected void log(PROGRAMME_LOG_OPERATION operation, Collection<Programme> programmes) throws Exception {
        if (programmes != null && !programmes.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<ProgrammeLog> logs = new HashSet<ProgrammeLog>();
            for (Programme programme : programmes) {
                logs.add(new ProgrammeLog(operation, programme.getLabel(), operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), programme)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(PROGRAMME_LOG_OPERATION operation, Programme programme) throws Exception {
        log(operation, programme, null);
    }

    protected void log(PROGRAMME_LOG_OPERATION operation, Programme programme, Collection<Channel> supplementaryChannels) throws Exception {
        if (programme != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), programme);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(new ProgrammeLog(operation, programme.getLabel(), allBoundChannels));
        }
    }

    protected void notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION operation, Channel channel, Programme programme) throws Exception {
        notify(operation, channel, programme, operation.getRecipients(getSecurityManager(), channel, programme));
    }

    protected void notify(CHANNELPROGRAMME_NOTIFICATION_OPERATION operation, Channel channel, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && programme != null && recipients != null) {
            Collection<ChannelProgrammeLinkNotification> notifications = new HashSet<ChannelProgrammeLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelProgrammeLinkNotification(operation, channel, programme, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser) throws Exception {
        notify(operation, programme, restrictedUser, operation.getRecipients(getSecurityManager(), programme, restrictedUser));
    }

    protected void notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && restrictedUser != null && recipients != null) {
            Collection<ProgrammeAnimatorLinkNotification> notifications = new HashSet<ProgrammeAnimatorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeAnimatorLinkNotification(operation, programme, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser) throws Exception {
        notify(operation, programme, restrictedUser, operation.getRecipients(getSecurityManager(), programme, restrictedUser));
    }

    protected void notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && restrictedUser != null && recipients != null) {
            Collection<ProgrammeContributorLinkNotification> notifications = new HashSet<ProgrammeContributorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeContributorLinkNotification(operation, programme, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser) throws Exception {
        notify(operation, programme, restrictedUser, operation.getRecipients(getSecurityManager(), programme, restrictedUser));
    }

    protected void notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && restrictedUser != null && recipients != null) {
            Collection<ProgrammeCuratorLinkNotification> notifications = new HashSet<ProgrammeCuratorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeCuratorLinkNotification(operation, programme, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION operation, Playlist playlist, Programme programme) throws Exception {
        notify(operation, playlist, programme, operation.getRecipients(getSecurityManager(), playlist, programme));
    }

    protected void notify(PLAYLISTPROGRAMME_NOTIFICATION_OPERATION operation, Playlist playlist, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (playlist != null && programme != null && recipients != null) {
            Collection<PlaylistProgrammeLinkNotification> notifications = new HashSet<PlaylistProgrammeLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new PlaylistProgrammeLinkNotification(operation, playlist, programme, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION operation, Programme programme, TimetableSlot timetableSlot) throws Exception {
        notify(operation, programme, timetableSlot, operation.getRecipients(getSecurityManager(), programme, timetableSlot));
    }

    protected void notify(PROGRAMMETIMETABLESLOT_NOTIFICATION_OPERATION operation, Programme programme, TimetableSlot timetableSlot, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && timetableSlot != null && recipients != null) {
            Collection<ProgrammeTimetableSlotLinkNotification> notifications = new HashSet<ProgrammeTimetableSlotLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeTimetableSlotLinkNotification(operation, programme, timetableSlot, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION operation, BroadcastableTrack broadcastableTrack, Programme programme) throws Exception {
        notify(operation, broadcastableTrack, programme, operation.getRecipients(getSecurityManager(), programme, broadcastableTrack));
    }

    protected void notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION operation, BroadcastableTrack broadcastableTrack, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (broadcastableTrack != null && programme != null && recipients != null) {
            Collection<ProgrammeBroadcastableTrackLinkNotification> notifications = new HashSet<ProgrammeBroadcastableTrackLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeBroadcastableTrackLinkNotification(operation, programme, broadcastableTrack, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack pendingTrack, Programme programme) throws Exception {
        notify(operation, pendingTrack, programme, operation.getRecipients(getSecurityManager(), programme, pendingTrack));
    }

    protected void notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack pendingTrack, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (pendingTrack != null && programme != null && recipients != null) {
            Collection<ProgrammePendingTrackLinkNotification> notifications = new HashSet<ProgrammePendingTrackLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammePendingTrackLinkNotification(operation, programme, pendingTrack, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMME_NOTIFICATION_OPERATION operation, Programme programme) throws Exception {
        notify(operation, programme, operation.getRecipients(getSecurityManager(), programme));
    }

    protected void notify(PROGRAMME_NOTIFICATION_OPERATION operation, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && recipients != null) {
            Collection<ProgrammeContentNotification> notifications = new HashSet<ProgrammeContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeContentNotification(operation, programme, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
