/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import java.util.Collection;



/**
 *
 * @author okay_awright
 **/
public interface ProgrammeFacadeServiceInterface {

    
    public void addSecuredProgramme(ProgrammeForm programme) throws Exception;

    
    public void updateSecuredProgramme(ProgrammeForm programmeForm) throws Exception;

    
    public void deleteSecuredProgramme(String id) throws Exception;
    
    public void deleteSecuredProgrammes(Collection<String> ids) throws Exception;

    
    public ActionCollectionEntry<ProgrammeForm> getSecuredProgramme(String id) throws Exception;

    
    public ActionCollection<ProgrammeForm> getSecuredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint) throws Exception;
    
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForChannelId(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId) throws Exception;
    
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForTrackId(GroupAndSort<PROGRAMME_TAGS> constraint, String trackId) throws Exception;

    
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForSongAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception;
    
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForAdvertAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception;
    
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForJingleAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception;

}
