/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.dao.programme.ProgrammeDao;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.channel.ChannelDao;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.helper.RightsBundle;

import org.springframework.dao.DataIntegrityViolationException;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeManager extends GenericService implements ProgrammeManagerInterface {

    private SecurityManagerInterface securityManager;
    private transient ProgrammeDao programmeDao;
    private transient ChannelDao channelDao;

    public ProgrammeDao getProgrammeDao() {
        return programmeDao;
    }

    public void setProgrammeDao(ProgrammeDao programmeDao) {
        this.programmeDao = programmeDao;
    }

    public ChannelDao getChannelDao() {
        return channelDao;
    }

    public void setChannelDao(ChannelDao channelDao) {
        this.channelDao = channelDao;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void addProgramme(Programme programme) throws Exception {
        try {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(programme);
            programmeDao.addProgramme(programme);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        }
    }

    @Override
    public void addPlaylist(Playlist playlist) throws Exception {
        try {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(playlist);
            programmeDao.addPlaylist(playlist);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        }
    }

    @Override
    public void addPlaylists(Collection<Playlist> playlists) throws Exception {
        if (playlists != null) {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(playlists);
            programmeDao.addBatchPlaylists(playlists);
            //No file from the datastore to update
        }
    }

    @Override
    public void updateProgramme(Programme programme) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(programme);
        programmeDao.updateProgramme(programme);
    }

    @Override
    public void updateProgrammes(Collection<Programme> programmes) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(programmes);
        programmeDao.updateBatchProgrammes(programmes);
    }

    @Override
    public void updatePlaylist(Playlist playlist) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(playlist);
        programmeDao.updatePlaylist(playlist);
    }

    @Override
    public void updatePlaylists(Collection<Playlist> playlists) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(playlists);
        programmeDao.updateBatchPlaylists(playlists);
    }

    @Override
    public Programme getProgramme(String id) throws Exception {
        Programme programme = programmeDao.getProgramme(id);
        if (programme == null) {
            throw new NoEntityException();
        }
        return programme;
    }

    @Override
    public Programme loadProgramme(String id) throws Exception {
        Programme programme = programmeDao.loadProgramme(id);
        if (programme == null) {
            throw new NoEntityException();
        }
        return programme;
    }
    @Override
    public Programme loadProgrammePrefetchTracks(String id) throws Exception {
        Programme programme = programmeDao.loadProgramme(id, true);
        if (programme == null) {
            throw new NoEntityException();
        }
        return programme;
    }

    @Override
    public Collection<Programme> getProgrammes(Collection<String> ids) throws Exception {
        Collection<Programme> programmes = programmeDao.getProgrammes(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (programmes == null || ids.size() != programmes.size()) {
            throw new NoEntityException();
        }
        //log it
        //logBatch(channels, COMMON_OPERATION.read);
        return programmes;
    }

    @Override
    public Collection<Programme> loadProgrammes(Collection<String> ids) throws Exception {
        Collection<Programme> programmes = programmeDao.loadProgrammes(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (programmes == null || ids.size()!=programmes.size()) {
            throw new NoEntityException();
        }
        return programmes;
    }
    @Override
    public Collection<Programme> loadProgrammesPrefetchTracks(Collection<String> ids) throws Exception {
        Collection<Programme> programmes = programmeDao.loadProgrammes(ids, true);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (programmes == null || ids.size()!=programmes.size()) {
            throw new NoEntityException();
        }
        return programmes;
    }
    
    @Override
    public Playlist getPlaylist(String id) throws Exception {
        Playlist playlist = programmeDao.getPlaylist(id);
        if (playlist == null) {
            throw new NoEntityException();
        }
        return playlist;
    }

    @Override
    public Playlist loadPlaylist(String id) throws Exception {
        Playlist playlist = programmeDao.loadPlaylist(id);
        if (playlist == null) {
            throw new NoEntityException();
        }
        return playlist;
    }

    @Override
    public Collection<Playlist> getPlaylists(Collection<String> ids) throws Exception {
        Collection<Playlist> playlists = programmeDao.getPlaylists(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (playlists == null || ids.size() != playlists.size()) {
            throw new NoEntityException();
        }
        return playlists;
    }

    @Override
    public Collection<Playlist> loadPlaylists(Collection<String> ids) throws Exception {
        Collection<Playlist> playlists = programmeDao.loadPlaylists(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (playlists == null || ids.size() != playlists.size()) {
            throw new NoEntityException();
        }
        return playlists;
    }

    @Override
    public void deleteProgramme(Programme programme) throws Exception {
        programmeDao.deleteProgramme(programme);
    }

    @Override
    public void deleteProgrammes(Collection<Programme> programmes) throws Exception {
        programmeDao.deleteBatchProgrammes(programmes);
    }

    @Override
    public void deleteProgramme(String id) throws Exception {
        Programme programme = getProgramme(id);
        if (programme == null) {
            throw new NoEntityException();
        }
        deleteProgramme(programme);
    }

    @Override
    public void deletePlaylist(Playlist playlist) throws Exception {
        programmeDao.deletePlaylist(playlist);
    }

    @Override
    public void deletePlaylists(Collection<Playlist> playlists) throws Exception {
        programmeDao.deleteBatchPlaylists(playlists);
    }

    @Override
    public void deletePlaylist(String id) throws Exception {
        Playlist playlist = getPlaylist(id);
        if (playlist == null) {
            throw new NoEntityException();
        }
        deletePlaylist(playlist);
    }

    @Override
    public void deletePlaylistEntry(PlaylistEntry playlistEntry) throws Exception {
        programmeDao.deletePlaylistEntry(playlistEntry);
    }

    @Override
    public void deletePlaylistEntries(Collection<PlaylistEntry> playlistEntries) throws Exception {
        programmeDao.deleteBatchPlaylistEntries(playlistEntries);
    }

    protected boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(Programme programmeToCheck, RightsBundle rightsBundle) throws Exception {
        if (programmeToCheck != null) {

            if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle)) {
                return true;
            }

            Collection<Channel> boundChannels = programmeToCheck.getChannels();
            //First see if at least one channel is bound to this programme
            if (boundChannels.isEmpty()) {
                //no programme is bound to the entity
                //Check first if the authenticated user has the minimal rights to touch the entity
                if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle)) {
                    //see if he's either its creator, or the latest editor, or if it has none
                    if (
                            (programmeToCheck.getCreator() == null && programmeToCheck.getLatestEditor() == null)
                            ||
                            (programmeToCheck.getCreator() != null && getSecurityManager().isCurrentlyAuthenticatedUser(programmeToCheck.getCreator()))
                            ||
                            (programmeToCheck.getLatestEditor() != null && getSecurityManager().isCurrentlyAuthenticatedUser(programmeToCheck.getLatestEditor()))
                            ) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewProgrammes() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Programme.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewPlaylists() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Playlist.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewProgramme(String programmeIdToCheck) throws Exception {
        return checkAccessViewProgramme(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessViewProgramme(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Programme.getViewRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    /**
     * Special rights a bit broader than a simple View but less than Edit
     * Used by Curators and Contributors for linking programmes to tracks
     * 
     * @param programmeToCheckId
     * @return
     * @throws Exception
     **/
    @Override
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(String programmeIdToCheck) throws Exception {
        return checkAccessLimitedTrackDependencyUpdateProgramme(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Programme.getLimitedTrackBindingRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewPlaylist(String programmeIdToCheck) throws Exception {
        return checkAccessViewPlaylist(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessViewPlaylist(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Playlist.getViewRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    /**
     * Special rights a bit broader than a simple View but less than Edit
     * Used by Curators and Contributors for linking programmes to tracks
     *
     * @param programmeToCheckId
     * @return
     * @throws Exception
     **/
    @Override
    public boolean checkAccessLimitedTimetableDependencyUpdatePlaylist(String programmeIdToCheck) throws Exception {
        return checkAccessLimitedTimetableDependencyUpdatePlaylist(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessLimitedTimetableDependencyUpdatePlaylist(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = Playlist.getLimitedTimetableBindingRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAddProgrammes() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Programme.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddPlaylists() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Playlist.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddPlaylist(String programmeIdToCheck) throws Exception {
        return checkAccessAddPlaylist(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessAddPlaylist(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Playlist.getAddRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateProgramme(String programmeIdToCheck) throws Exception {
        return checkAccessUpdateProgramme(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateProgramme(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Programme.getUpdateRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateProgrammes() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Programme.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdatePlaylists() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Playlist.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdatePlaylist(String programmeIdToCheck) throws Exception {
        return checkAccessUpdatePlaylist(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessUpdatePlaylist(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Playlist.getUpdateRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteProgrammes() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Programme.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeletePlaylists() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = Playlist.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteProgramme(String programmeIdToCheck) throws Exception {
        return checkAccessDeleteProgramme(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessDeleteProgramme(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Programme.getDeleteRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeletePlaylist(String programmeIdToCheck) throws Exception {
        return checkAccessDeletePlaylist(getProgramme(programmeIdToCheck));
    }

    @Override
    public boolean checkAccessDeletePlaylist(Programme programmeToCheck) throws Exception {
        if (programmeToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = Playlist.getDeleteRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(programmeToCheck, rightsBundle);

            }
        }
        return false;
    }

    public Collection<Programme> getFilteredUnlinkedProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint) throws Exception {
        Collection<Programme> programmes;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            programmes = programmeDao.getAllProgrammesWithNoRight(constraint);
        } else {
            programmes = programmeDao.getAllProgrammesWithNoRightForCreator(getSecurityManager().getCurrentlyAuthenticatedUser().getUsername(), constraint);
        }
        return programmes;
    }

    @Override
    public Collection<Programme> getFilteredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint) throws Exception {
        return getFilteredProgrammes(constraint, null);
    }

    @Override
    public Collection<Programme> getFilteredProgrammesForChannelId(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId) throws Exception {
        return getFilteredProgrammes(constraint, channelId);
    }

    @Override
    public Collection<Playlist> getFilteredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint) throws Exception {
        return (Collection<Playlist>) getFilteredPlaylists(constraint, null);
    }

    @Override
    public Collection<Playlist> getFilteredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId) throws Exception {
        return (Collection<Playlist>) getFilteredPlaylists(constraint, programmeId);
    }

    protected Collection<Programme> getFilteredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId) throws Exception {
        Collection<Programme> programmes;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (channelId == null) {
                programmes = programmeDao.getAllProgrammes(constraint);
            } else {
                programmes = programmeDao.getProgrammesForChannel(constraint, channelId);
            }
        } else {

            //Gather all programmes visible by the authenticated user
            RightsBundle rightsBundle = Programme.getViewRights();

            if (channelId == null)
                programmes = programmeDao.getProgrammesWithRightsByProgrammesAndOrphans(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername());
            else
                programmes = programmeDao.getProgrammesWithRightsByProgrammesForChannel(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    channelId);
        }
        return programmes;
    }

    protected Collection<Programme> getFilteredProgrammesForTrackAndProgrammeIds(Class<? extends BroadcastableTrack> clazz, String author, String title, Collection<String> programmeIds) throws Exception {
        Collection<Programme> programmes;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            programmes = programmeDao.getProgrammesForTrackAndProgrammes(clazz, author, title, programmeIds);
        } else {

            //Gather all programmes visible by the authenticated user
            RightsBundle rightsBundle = Programme.getViewRights();

            programmes = programmeDao.getProgrammesWithRightsByProgrammesForTrackAndProgrammes(
                    clazz,
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    author,
                    title,
                    programmeIds);
        }
        return programmes;
    }

    @Override
    public Collection<Programme> getFilteredProgrammesForSongAndProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return getFilteredProgrammesForTrackAndProgrammeIds(BroadcastableSong.class, author, title, programmeIds);
    }

    @Override
    public Collection<Programme> getFilteredProgrammesForJingleAndProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return getFilteredProgrammesForTrackAndProgrammeIds(BroadcastableJingle.class, author, title, programmeIds);
    }

    @Override
    public Collection<Programme> getFilteredProgrammesForAdvertAndProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return getFilteredProgrammesForTrackAndProgrammeIds(BroadcastableAdvert.class, author, title, programmeIds);
    }

    @Override
    public Collection<Programme> getFilteredProgrammesForTrackId(GroupAndSort<PROGRAMME_TAGS> constraint, String trackId) throws Exception {
        Collection<Programme> programmes;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (trackId == null) {
                programmes = programmeDao.getAllProgrammes(constraint);
            } else {
                programmes = programmeDao.getProgrammesForTrack(constraint, trackId);
            }
        } else {

            //Gather all programmes visible by the authenticated user
            RightsBundle rightsBundle = Programme.getViewRights();

            programmes = programmeDao.getProgrammesWithRightsByProgrammesForTrack(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    trackId);
        }
        return programmes;
    }

    protected Collection<? extends Playlist> getFilteredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId) throws Exception {
        Collection<? extends Playlist> playlists;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (programmeId == null) {
                playlists = programmeDao.getAllPlaylists(constraint);
            } else {
                playlists = programmeDao.getPlaylistsForProgramme(constraint, programmeId);
            }
        } else {

            //Gather all playlists visible by the authenticated user
            RightsBundle rightsBundle = Playlist.getViewRights();

            playlists = programmeDao.getPlaylistsWithRightsByProgrammesForProgramme(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    programmeId);
        }
        return playlists;
    }

}
