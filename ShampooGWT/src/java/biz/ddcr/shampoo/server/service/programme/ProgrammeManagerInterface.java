/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;

import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface ProgrammeManagerInterface {

    /** Internal struct **/
    public class PlaylistFiles {

        public PlaylistFiles(boolean doUpdatePicture, String coverArtUploadId) {
            this.doUpdatePicture = doUpdatePicture;
            this.coverArtUploadId = coverArtUploadId;
        }
        private boolean doUpdatePicture;
        private String coverArtUploadId;

        public String getCoverArtUploadId() {
            return coverArtUploadId;
        }

        public void setCoverArtUploadId(String coverArtUploadId) {
            this.coverArtUploadId = coverArtUploadId;
        }

        public boolean doUpdatePicture() {
            return doUpdatePicture;
        }

        public void doUpdatePicture(boolean doUpdatePicture) {
            this.doUpdatePicture = doUpdatePicture;
        }

    }

    //Check access to resource
    public boolean checkAccessViewProgrammes() throws Exception;
    public boolean checkAccessViewProgramme(String programmeToCheckId) throws Exception;
    public boolean checkAccessViewProgramme(Programme programmeToCheck) throws Exception;
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(String programmeToCheckId) throws Exception;
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(Programme programmeToCheck) throws Exception;
    public boolean checkAccessAddProgrammes() throws Exception;
    public boolean checkAccessUpdateProgrammes() throws Exception;
    public boolean checkAccessUpdateProgramme(String programmeToCheckId) throws Exception;
    public boolean checkAccessUpdateProgramme(Programme programmeToCheck) throws Exception;
    public boolean checkAccessDeleteProgrammes() throws Exception;
    public boolean checkAccessDeleteProgramme(String programmeToCheckId) throws Exception;
    public boolean checkAccessDeleteProgramme(Programme programmeToCheck) throws Exception;
    public boolean checkAccessViewPlaylists() throws Exception;
    public boolean checkAccessViewPlaylist(String programmeToCheckId) throws Exception;
    public boolean checkAccessViewPlaylist(Programme programmeToCheck) throws Exception;
    public boolean checkAccessLimitedTimetableDependencyUpdatePlaylist(String programmeToCheckId) throws Exception;
    public boolean checkAccessLimitedTimetableDependencyUpdatePlaylist(Programme programmeToCheck) throws Exception;
    public boolean checkAccessAddPlaylists() throws Exception;
    public boolean checkAccessAddPlaylist(String programmeToCheckId) throws Exception;
    public boolean checkAccessAddPlaylist(Programme programmeToCheck) throws Exception;
    public boolean checkAccessUpdatePlaylists() throws Exception;
    public boolean checkAccessUpdatePlaylist(String programmeToCheckId) throws Exception;
    public boolean checkAccessUpdatePlaylist(Programme programmeToCheck) throws Exception;
    public boolean checkAccessDeletePlaylists() throws Exception;
    public boolean checkAccessDeletePlaylist(String programmeToCheckId) throws Exception;
    public boolean checkAccessDeletePlaylist(Programme programmeToCheck) throws Exception;
   
    //Edit objects
    public void addProgramme(Programme programme) throws Exception;
    public void addPlaylist(Playlist playlist) throws Exception;
    public void addPlaylists(Collection<Playlist> playlists) throws Exception;

    public void updateProgramme(Programme programme) throws Exception;
    public void updateProgrammes(Collection<Programme> programmes) throws Exception;
    public void updatePlaylist(Playlist playlist) throws Exception;
    public void updatePlaylists(Collection<Playlist> playlists) throws Exception;
    
    public void deleteProgramme(Programme programme) throws Exception;
    public void deleteProgrammes(Collection<Programme> programmes) throws Exception;
    public void deleteProgramme(String id) throws Exception;
    public void deletePlaylist(Playlist playlist) throws Exception;
    public void deletePlaylists(Collection<Playlist> playlists) throws Exception;
    public void deletePlaylist(String id) throws Exception;
    public void deletePlaylistEntry(PlaylistEntry playlistEntry) throws Exception;
    public void deletePlaylistEntries(Collection<PlaylistEntry> playlistEntries) throws Exception;

    //Fetch objects
    public Programme getProgramme(String id) throws Exception;
    public Collection<Programme> getProgrammes(Collection<String> ids) throws Exception;
    public Programme loadProgramme(String id) throws Exception;
    public Collection<Programme> loadProgrammes(Collection<String> ids) throws Exception;
    public Programme loadProgrammePrefetchTracks(String id) throws Exception;
    public Collection<Programme> loadProgrammesPrefetchTracks(Collection<String> ids) throws Exception;
    public Playlist getPlaylist(String id) throws Exception;
    public Collection<Playlist> getPlaylists(Collection<String> ids) throws Exception;
    public Playlist loadPlaylist(String id) throws Exception;
    public Collection<Playlist> loadPlaylists(Collection<String> ids) throws Exception;

    //Fetching objects with associated access policy flags
    public Collection<Programme> getFilteredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint) throws Exception;
    public Collection<Programme> getFilteredProgrammesForChannelId(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId) throws Exception;
    public Collection<Programme> getFilteredProgrammesForTrackId(GroupAndSort<PROGRAMME_TAGS> constraint, String trackId) throws Exception;
    public Collection<Playlist> getFilteredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint) throws Exception;
    public Collection<Playlist> getFilteredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId) throws Exception;

    public Collection<Programme> getFilteredProgrammesForSongAndProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    public Collection<Programme> getFilteredProgrammesForJingleAndProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    public Collection<Programme> getFilteredProgrammesForAdvertAndProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;

}
