/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.programme;

import biz.ddcr.shampoo.client.form.playlist.PlaylistForm;
import biz.ddcr.shampoo.client.form.playlist.PlaylistForm.PLAYLIST_TAGS;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm;
import biz.ddcr.shampoo.client.form.programme.ProgrammeForm.PROGRAMME_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.serviceAsync.programme.ProgrammeRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class ProgrammeRPCService extends GWTRPCLocalThreadService implements ProgrammeRPCServiceInterface {

    private ProgrammeFacadeServiceInterface programmeFacadeService;
    private PlaylistFacadeServiceInterface playlistFacadeService;
    
    public ProgrammeFacadeServiceInterface getProgrammeFacadeService() {
        return programmeFacadeService;
    }

    public void setProgrammeFacadeService(ProgrammeFacadeServiceInterface programmeFacadeService) {
        this.programmeFacadeService = programmeFacadeService;
    }

    public PlaylistFacadeServiceInterface getPlaylistFacadeService() {
        return playlistFacadeService;
    }

    public void setPlaylistFacadeService(PlaylistFacadeServiceInterface playlistFacadeService) {
        this.playlistFacadeService = playlistFacadeService;
    }

    @Override
    public void addSecuredProgramme(ProgrammeForm programmeForm) throws Exception {
        programmeFacadeService.addSecuredProgramme(programmeForm);
    }

    @Override
    public void updateSecuredProgramme(ProgrammeForm programmeForm) throws Exception {
        programmeFacadeService.updateSecuredProgramme(programmeForm);
    }
    
    @Override
    public void deleteSecuredProgramme(String id) throws Exception {
        programmeFacadeService.deleteSecuredProgramme(id);
    }
    @Override
    public void deleteSecuredProgrammes(Collection<String> ids) throws Exception {
        programmeFacadeService.deleteSecuredProgrammes(ids);
    }

    @Override
    public ActionCollectionEntry<ProgrammeForm> getSecuredProgramme(String id) throws Exception {
        return programmeFacadeService.getSecuredProgramme(id);
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammes(GroupAndSort<PROGRAMME_TAGS> constraint) throws Exception {
        return programmeFacadeService.getSecuredProgrammes(constraint);
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForChannelId(GroupAndSort<PROGRAMME_TAGS> constraint, String channelId) throws Exception {
        return programmeFacadeService.getSecuredProgrammesForChannelId(constraint, channelId);
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForTrackId(GroupAndSort<PROGRAMME_TAGS> constraint, String trackId) throws Exception {
        return programmeFacadeService.getSecuredProgrammesForTrackId(constraint, trackId);
    }

    @Override
    public void addSecuredPlaylist(PlaylistForm playlistForm) throws Exception {
        playlistFacadeService.addSecuredPlaylist(playlistForm);
    }
    @Override
    public void addSecuredPlaylist(PlaylistForm playlistForm, String flyerUploadId) throws Exception {
        playlistFacadeService.asynchronousAddSecuredPlaylistAndPictureFile(playlistForm, flyerUploadId);
    }

    @Override
    public void updateSecuredPlaylist(PlaylistForm playlistForm) throws Exception {
       playlistFacadeService.updateSecuredPlaylist(playlistForm);
    }
    @Override
    public void updateSecuredPlaylist(PlaylistForm playlistForm, String flyerUploadId) throws Exception {
       playlistFacadeService.asynchronousUpdateSecuredPlaylistAndPictureFile(playlistForm, flyerUploadId);
    }

    @Override
    public void deleteSecuredPlaylist(String id) throws Exception {
        playlistFacadeService.asynchronousDeleteSecuredPlaylist(id);
    }
    @Override
    public void deleteSecuredPlaylists(Collection<String> ids) throws Exception {
        playlistFacadeService.asynchronousDeleteSecuredPlaylists(ids);
    }

    @Override
    public ActionCollectionEntry<PlaylistForm> getSecuredPlaylist(String id) throws Exception {
        return playlistFacadeService.getSecuredPlaylist(id, null);
    }
    @Override
    public ActionCollectionEntry<PlaylistForm> getSecuredPlaylist(String id, String localTimeZone) throws Exception {
        return playlistFacadeService.getSecuredPlaylist(id, localTimeZone);
    }

    @Override
    public ActionCollection<PlaylistForm> getSecuredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint) throws Exception {
        return playlistFacadeService.getSecuredPlaylists(constraint, null);
    }
    @Override
    public ActionCollection<PlaylistForm> getSecuredPlaylists(GroupAndSort<PLAYLIST_TAGS> constraint, String localTimeZone) throws Exception {
        return playlistFacadeService.getSecuredPlaylists(constraint, localTimeZone);
    }

    @Override
    public ActionCollection<PlaylistForm> getSecuredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId) throws Exception {
        return playlistFacadeService.getSecuredPlaylistsForProgrammeId(constraint, programmeId, null);
    }
    @Override
    public ActionCollection<PlaylistForm> getSecuredPlaylistsForProgrammeId(GroupAndSort<PLAYLIST_TAGS> constraint, String programmeId, String localTimeZone) throws Exception {
        return playlistFacadeService.getSecuredPlaylistsForProgrammeId(constraint, programmeId, localTimeZone);
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForSongAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception {
        return programmeFacadeService.getSecuredProgrammesForSongAndProgrammeIds(author, title, programmeIds);
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForAdvertAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception {
        return programmeFacadeService.getSecuredProgrammesForAdvertAndProgrammeIds(author, title, programmeIds);
    }

    @Override
    public ActionCollection<ProgrammeForm> getSecuredProgrammesForJingleAndProgrammeIds(String author, String title, ActionCollection<String> programmeIds) throws Exception {
        return programmeFacadeService.getSecuredProgrammesForJingleAndProgrammeIds(author, title, programmeIds);
    }

}
