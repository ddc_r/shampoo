/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.queue;

import biz.ddcr.shampoo.client.form.archive.ArchiveFormID;
import biz.ddcr.shampoo.client.form.queue.BlankQueueForm;
import biz.ddcr.shampoo.client.form.queue.BroadcastableTrackQueueForm;
import biz.ddcr.shampoo.client.form.queue.LiveQueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm;
import biz.ddcr.shampoo.client.form.queue.RequestQueueForm;
import biz.ddcr.shampoo.client.form.track.AverageVoteModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.playlist.Blank;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.queue.BlankQueueItem;
import biz.ddcr.shampoo.server.domain.queue.LiveQueueItem;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.RequestQueueItem;
import biz.ddcr.shampoo.server.domain.queue.SchedulableQueueItem;
import biz.ddcr.shampoo.server.domain.queue.SimpleTrackQueueItem;
import biz.ddcr.shampoo.server.domain.queue.TrackQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.BlankQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.RequestQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.SimpleTrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemVisitor;
import biz.ddcr.shampoo.server.domain.streamer.TrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.track.TrackDTOAssembler;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 *
 */
public class QueueDTOAssembler extends GenericService implements QueueDTOAssemblerInterface {

    /**
     * dummy marker class for form export *
     */
    private interface QueueItemVisitorDTO extends QueueItemVisitor {

        public QueueForm getForm();
    };

    /**
     * dummy visitor for Hibernate proxies *
     */
    private interface AverageVoteVisitor extends PlayableItemVisitor {

        public AverageVoteModule get();
    }

    private interface StreamItemVisitorDTO extends StreamItemVisitor {

        public QueueForm getForm();
    };
    private ChannelManagerInterface channelManager;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public static ArchiveFormID toIdDTO(QueueItem queueItem, String localTimeZone) throws Exception {
        ArchiveFormID newId = new ArchiveFormID();

        newId.setRefID(queueItem.getRefID());
        newId.setChannelId(queueItem.getChannel().getLabel());
        if (localTimeZone != null && localTimeZone.length() != 0) {
            newId.setSchedulingTime(DateDTOAssembler.toJS(queueItem.getStartTime().switchTimeZone(localTimeZone)));
        } else {
            newId.setSchedulingTime(DateDTOAssembler.toJS(queueItem.getStartTime()));
        }

        return newId;
    }

    public static ArchiveFormID toIdDTO(StreamItem queueItem, String localTimeZone) throws Exception {
        ArchiveFormID newId = new ArchiveFormID();

        newId.setRefID(queueItem.getRefID());
        newId.setChannelId(queueItem.getChannel().getLabel());
        if (localTimeZone != null && localTimeZone.length() != 0) {
            newId.setSchedulingTime(DateDTOAssembler.toJS(queueItem.getStartTime().switchTimeZone(localTimeZone)));
        } else {
            newId.setSchedulingTime(DateDTOAssembler.toJS(queueItem.getStartTime()));
        }

        return newId;
    }

    public CoverArtModule toCoverArtDTO(QueueItem queueItem) throws Exception {
        if (queueItem != null) {

            CoverArtModule container = null;

            if (ProxiedClassUtil.castableAs(queueItem, RequestQueueItem.class)) {
                RequestQueueItem requestQueueItem = (RequestQueueItem) queueItem;
                if (requestQueueItem.getRequest() != null && requestQueueItem.getRequest().getSong() != null && requestQueueItem.getRequest().getSong().getCoverArtContainer() != null) {
                    PictureFileInfo info = requestQueueItem.getRequest().getSong().getCoverArtContainer();
                    container = new CoverArtModule();
                    //Format
                    container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));
                    //metadata
                    container.setSize(info.getSize());
                    //Download info
                    container.setURL(
                            getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(requestQueueItem.getRequest().getSong().getRefID()));
                }
            } else if (ProxiedClassUtil.castableAs(queueItem, TrackQueueItem.class)) {
                TrackQueueItem trackQueueItem = (TrackQueueItem) queueItem;
                if (trackQueueItem.getItem() != null && trackQueueItem.getItem().getCoverArtContainer() != null) {
                    PictureFileInfo info = trackQueueItem.getItem().getCoverArtContainer();
                    container = new CoverArtModule();
                    //Format
                    container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));
                    //metadata
                    container.setSize(info.getSize());
                    //Download info
                    container.setURL(
                            getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(trackQueueItem.getItem().getRefID()));
                }
            }

            if (container == null && ProxiedClassUtil.castableAs(queueItem, SchedulableQueueItem.class)) {
                SchedulableQueueItem schedulableQueueItem = (SchedulableQueueItem) queueItem;
                if (schedulableQueueItem.getTimetableSlot() != null && schedulableQueueItem.getTimetableSlot().getPlaylist() != null && schedulableQueueItem.getTimetableSlot().getPlaylist().getCoverArtContainer() != null) {
                    PictureFileInfo info = schedulableQueueItem.getTimetableSlot().getPlaylist().getCoverArtContainer();
                    container = new CoverArtModule();
                    //Format
                    container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));
                    //metadata
                    container.setSize(info.getSize());
                    //Download info
                    container.setURL(
                            getDatastoreService().getGWTFlyerDownloadURLEndpointForUser(schedulableQueueItem.getTimetableSlot().getPlaylist().getRefID()));
                }
            }

            return container;
        }
        return null;
    }

    public CoverArtModule toCoverArtDTO(StreamItem streamItem) throws Exception {
        if (streamItem != null) {

            CoverArtModule container = null;

            if (ProxiedClassUtil.castableAs(streamItem, RequestQueueStreamItem.class)) {
                RequestQueueStreamItem requestQueueItem = (RequestQueueStreamItem) streamItem;
                if (requestQueueItem.getRequest() != null && requestQueueItem.getRequest().getSong() != null && requestQueueItem.getRequest().getSong().getCoverArtContainer() != null) {
                    PictureFileInfo info = requestQueueItem.getRequest().getSong().getCoverArtContainer();
                    container = new CoverArtModule();
                    //Format
                    container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));
                    //metadata
                    container.setSize(info.getSize());
                    //Download info
                    container.setURL(
                            getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(requestQueueItem.getRequest().getSong().getRefID()));
                }
            } else if (ProxiedClassUtil.castableAs(streamItem, TrackQueueStreamItem.class)) {
                TrackQueueStreamItem trackQueueItem = (TrackQueueStreamItem) streamItem;
                if (trackQueueItem.getTrack() != null && trackQueueItem.getTrack().getCoverArtContainer() != null) {
                    PictureFileInfo info = trackQueueItem.getTrack().getCoverArtContainer();
                    container = new CoverArtModule();
                    //Format
                    container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));
                    //metadata
                    container.setSize(info.getSize());
                    //Download info
                    container.setURL(
                            getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(trackQueueItem.getTrack().getRefID()));
                }
            }

            if (container == null && streamItem.getTimetableSlot() != null && streamItem.getTimetableSlot().getPlaylist() != null && streamItem.getTimetableSlot().getPlaylist().getCoverArtContainer() != null) {
                PictureFileInfo info = streamItem.getTimetableSlot().getPlaylist().getCoverArtContainer();
                container = new CoverArtModule();
                //Format
                container.setFormat(TrackDTOAssembler.toDTO(info.getFormat()));
                //metadata
                container.setSize(info.getSize());
                //Download info
                container.setURL(
                        getDatastoreService().getGWTFlyerDownloadURLEndpointForUser(streamItem.getTimetableSlot().getPlaylist().getRefID()));
            }

            return container;
        }
        return null;
    }

    private AverageVoteModule toDTO(PlayableItemInterface item) throws Exception {
        if (item != null) {
            AverageVoteVisitor visitor = new AverageVoteVisitor() {

                private AverageVoteModule avgRating = null;

                @Override
                public AverageVoteModule get() {
                    return avgRating;
                }

                @Override
                public void visit(BroadcastableAdvert track) throws Exception {
                    avgRating = null;
                }

                @Override
                public void visit(BroadcastableJingle track) throws Exception {
                    avgRating = null;
                }

                @Override
                public void visit(BroadcastableSong track) throws Exception {
                    avgRating = new AverageVoteModule();
                    Float avgScore = track.getAverageVote();
                    avgRating.setAverageScore(
                            avgScore);
                    avgRating.setNumberOfVotes(
                            track.getVotes().size());
                }

                @Override
                public void visit(Live live) throws Exception {
                    avgRating = null;
                }

                @Override
                public void visit(Blank blank) throws Exception {
                    avgRating = null;
                }

                @Override
                public void visit(Request request) throws Exception {
                    if (request.getSong() != null) {
                        avgRating = new AverageVoteModule();
                        Float avgScore = request.getSong().getAverageVote();
                        avgRating.setAverageScore(
                                avgScore);
                        avgRating.setNumberOfVotes(
                                request.getSong().getVotes().size());
                    } else {
                        avgRating = null;
                    }
                }
            };
            item.is(visitor);
            return visitor.get();
        }
        return null;
    }

    @Override
    public ActionCollectionEntry<? extends QueueForm> toDTO(QueueItem queueItem) throws Exception {
        return toDTO(queueItem, null);
    }

    @Override
    public ActionCollectionEntry<? extends QueueForm> toDTO(QueueItem queueItem, String localTimeZone) throws Exception {
        if (queueItem == null) {
            return null;
        }

        QueueItemVisitorDTO queueItemVisitor = new QueueItemVisitorDTO() {

            QueueForm queueItemModule = null;

            @Override
            public QueueForm getForm() {
                return queueItemModule;
            }

            @Override
            public void visit(BlankQueueItem entry) throws Exception {
                queueItemModule = new BlankQueueForm();
            }

            @Override
            public void visit(LiveQueueItem entry) throws Exception {
                queueItemModule = new LiveQueueForm();
                ((LiveQueueForm) queueItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null ? (entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null) : null);
                ((LiveQueueForm) queueItemModule).setProgrammeLabel(entry.getTimetableSlot() != null ? (entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null) : null);
                ((LiveQueueForm) queueItemModule).setBroadcasterCaption(entry.getLive() != null ? entry.getLive().getBroadcaster() : null);
            }

            @Override
            public void visit(SimpleTrackQueueItem entry) throws Exception {
                queueItemModule = new BroadcastableTrackQueueForm();
                ((BroadcastableTrackQueueForm) queueItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null ? (entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null) : null);
                ((BroadcastableTrackQueueForm) queueItemModule).setProgrammeLabel(entry.getTimetableSlot() != null ? (entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null) : null);
                ((BroadcastableTrackQueueForm) queueItemModule).setTrackId(entry.getItem() != null ? entry.getItem().getRefID() : null);
                ((BroadcastableTrackQueueForm) queueItemModule).setTrackAuthorCaption(entry.getItem() != null ? entry.getItem().getAuthor() : null);
                ((BroadcastableTrackQueueForm) queueItemModule).setTrackTitleCaption(entry.getItem() != null ? entry.getItem().getTitle() : null);
                if (entry.getItem() != null && ProxiedClassUtil.castableAs(entry.getItem(), BroadcastableSong.class)) {
                    ((BroadcastableTrackQueueForm) queueItemModule).setCanBeVoted(true);
                    ((BroadcastableTrackQueueForm) queueItemModule).setAverageVote(toDTO(entry.getItem()));
                } else {
                    ((BroadcastableTrackQueueForm) queueItemModule).setCanBeVoted(false);
                }
            }

            @Override
            public void visit(RequestQueueItem entry) throws Exception {
                queueItemModule = new RequestQueueForm();
                ((RequestQueueForm) queueItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null ? (entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null) : null);
                ((RequestQueueForm) queueItemModule).setProgrammeLabel(entry.getTimetableSlot() != null ? (entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null) : null);
                ((RequestQueueForm) queueItemModule).setTrackId(entry.getItem() != null ? entry.getItem().getRefID() : null);
                ((RequestQueueForm) queueItemModule).setTrackAuthorCaption(entry.getItem() != null ? entry.getItem().getAuthor() : null);
                ((RequestQueueForm) queueItemModule).setTrackTitleCaption(entry.getItem() != null ? entry.getItem().getTitle() : null);
                ((RequestQueueForm) queueItemModule).setCanBeVoted(true);
                if (entry.getItem() != null && ProxiedClassUtil.castableAs(entry.getItem(), BroadcastableSong.class)) {
                    ((RequestQueueForm) queueItemModule).setCanBeVoted(true);
                    ((RequestQueueForm) queueItemModule).setAverageVote(toDTO(entry.getItem()));
                } else {
                    ((RequestQueueForm) queueItemModule).setCanBeVoted(false);
                }
                Request request = entry.getRequest();
                if (request != null) {
                    ((RequestQueueForm) queueItemModule).setRequestAuthor(request.getRequester() != null ? request.getRequester().getUsername() : null);
                    ((RequestQueueForm) queueItemModule).setRequestMessage(request.getMessage());
                }
            }
        };
        queueItem.acceptVisit(queueItemVisitor);

        if (queueItemVisitor.getForm() != null) {
            //Fill in common properties
            queueItemVisitor.getForm().setRefId(toIdDTO(queueItem, localTimeZone));
            queueItemVisitor.getForm().setDuration(new JSDuration(queueItem.getDuration()));
            queueItemVisitor.getForm().setPooled(queueItem.isPooled());

            queueItemVisitor.getForm().setCoverArtFile(toCoverArtDTO(queueItem));

            return new ActionMapEntry<QueueForm>(queueItemVisitor.getForm(), decorateEntryWithActions(queueItem));
        } else {
            return null;
        }
    }

    @Override
    public ActionCollectionEntry<? extends QueueForm> toDTO(StreamItem streamItem) throws Exception {
        return toDTO(streamItem, null);
    }

    @Override
    public ActionCollectionEntry<? extends QueueForm> toDTO(StreamItem streamItem, String localTimeZone) throws Exception {
        if (streamItem == null) {
            return null;
        }

        StreamItemVisitorDTO streamItemVisitor = new StreamItemVisitorDTO() {

            QueueForm streamItemModule = null;

            @Override
            public QueueForm getForm() {
                return streamItemModule;
            }

            @Override
            public void visit(BlankQueueStreamItem entry) throws Exception {
                streamItemModule = new BlankQueueForm();
            }

            @Override
            public void visit(LiveQueueStreamItem entry) throws Exception {
                streamItemModule = new LiveQueueForm();
                ((LiveQueueForm) streamItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null && entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null);
                ((LiveQueueForm) streamItemModule).setProgrammeLabel(entry.getTimetableSlot() != null && entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null);
                ((LiveQueueForm) streamItemModule).setBroadcasterCaption(entry.getTimetableSlot() != null && entry.getTimetableSlot().getPlaylist() != null && entry.getTimetableSlot().getPlaylist().getLive() != null ? entry.getTimetableSlot().getPlaylist().getLive().getBroadcaster() : null);
            }

            @Override
            public void visit(LiveNonQueueStreamItem entry) throws Exception {
                streamItemModule = new LiveQueueForm();
                ((LiveQueueForm) streamItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null && entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null);
                ((LiveQueueForm) streamItemModule).setProgrammeLabel(entry.getTimetableSlot() != null && entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null);
                ((LiveQueueForm) streamItemModule).setBroadcasterCaption(entry.getTimetableSlot() != null && entry.getTimetableSlot().getPlaylist() != null && entry.getTimetableSlot().getPlaylist().getLive() != null ? entry.getTimetableSlot().getPlaylist().getLive().getBroadcaster() : null);
            }

            @Override
            public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                streamItemModule = new BroadcastableTrackQueueForm();
                ((BroadcastableTrackQueueForm) streamItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null && entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null);
                ((BroadcastableTrackQueueForm) streamItemModule).setProgrammeLabel(entry.getTimetableSlot() != null && entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null);
                ((BroadcastableTrackQueueForm) streamItemModule).setTrackId(entry.getTrack() != null ? entry.getTrack().getRefID() : null);
                ((BroadcastableTrackQueueForm) streamItemModule).setTrackAuthorCaption(entry.getTrack() != null ? entry.getTrack().getAuthor() : null);
                ((BroadcastableTrackQueueForm) streamItemModule).setTrackTitleCaption(entry.getTrack() != null ? entry.getTrack().getTitle() : null);
                if (entry.getTrack() != null && ProxiedClassUtil.castableAs(entry.getTrack(), BroadcastableSong.class)) {
                    ((BroadcastableTrackQueueForm) streamItemModule).setCanBeVoted(true);
                    ((BroadcastableTrackQueueForm) streamItemModule).setAverageVote(toDTO(entry.getTrack()));
                } else {
                    ((BroadcastableTrackQueueForm) streamItemModule).setCanBeVoted(false);
                }
            }

            @Override
            public void visit(RequestQueueStreamItem entry) throws Exception {
                streamItemModule = new RequestQueueForm();
                ((RequestQueueForm) streamItemModule).setPlaylistFriendlyId(entry.getTimetableSlot() != null && entry.getTimetableSlot().getPlaylist() != null ? entry.getTimetableSlot().getPlaylist().getLabel() : null);
                ((RequestQueueForm) streamItemModule).setProgrammeLabel(entry.getTimetableSlot() != null && entry.getTimetableSlot().getProgramme() != null ? entry.getTimetableSlot().getProgramme().getLabel() : null);
                ((BroadcastableTrackQueueForm) streamItemModule).setTrackId(entry.getTrack() != null ? entry.getTrack().getRefID() : null);
                ((RequestQueueForm) streamItemModule).setTrackAuthorCaption(entry.getTrack() != null ? entry.getTrack().getAuthor() : null);
                ((RequestQueueForm) streamItemModule).setTrackTitleCaption(entry.getTrack() != null ? entry.getTrack().getTitle() : null);
                if (entry.getTrack() != null && ProxiedClassUtil.castableAs(entry.getTrack(), BroadcastableSong.class)) {
                    ((RequestQueueForm) streamItemModule).setCanBeVoted(true);
                    ((RequestQueueForm) streamItemModule).setAverageVote(toDTO(entry.getTrack()));
                } else {
                    ((RequestQueueForm) streamItemModule).setCanBeVoted(false);
                }
                Request r = entry.getRequest();
                if (r != null) {
                    ((RequestQueueForm) streamItemModule).setRequestAuthor(r.getRequester() != null ? r.getRequester().getUsername() : null);
                    ((RequestQueueForm) streamItemModule).setRequestMessage(r.getMessage());
                }
            }
        };
        streamItem.acceptVisit(streamItemVisitor);

        if (streamItemVisitor.getForm() != null) {
            //Fill in common properties
            streamItemVisitor.getForm().setRefId(toIdDTO(streamItem, localTimeZone));
            streamItemVisitor.getForm().setDuration(new JSDuration(streamItem.getDuration()));
            //'Now playing' item IS obviously pooled
            streamItemVisitor.getForm().setPooled(true);
            streamItemVisitor.getForm().setCoverArtFile(toCoverArtDTO(streamItem));

            return new ActionMapEntry<QueueForm>(streamItemVisitor.getForm(), decorateEntryWithActions(streamItem));
        } else {
            return null;
        }
    }

    protected Collection<ACTION> decorateEntryWithActions(QueueItem queueItem) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewQueue(queueItem.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(StreamItem queueItem) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewQueue(queueItem.getChannel())) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    @Override
    public ActionCollection<? extends QueueForm> toDTO(Collection<QueueItem> queueItems, boolean doSort) throws Exception {
        return toDTO(queueItems, null, doSort);
    }

    @Override
    public ActionCollection<? extends QueueForm> toDTO(Collection<QueueItem> queueItems, String localTimeZone, boolean doSort) throws Exception {
        ActionMap<QueueForm> queueItemModules = doSort ? new ActionSortedMap<QueueForm>() : new ActionUnsortedMap<QueueForm>();
        for (QueueItem queueItem : queueItems) {
            queueItemModules.set(toDTO(queueItem, localTimeZone));
        }
        return queueItemModules;
    }

    @Override
    public ActionCollection<? extends QueueForm> toStreamDTO(Collection<StreamItem> streamItems, boolean doSort) throws Exception {
        return toStreamDTO(streamItems, null, doSort);
    }

    @Override
    public ActionCollection<? extends QueueForm> toStreamDTO(Collection<StreamItem> streamItems, String localTimeZone, boolean doSort) throws Exception {
        ActionMap<QueueForm> queueItemModules = doSort ? new ActionSortedMap<QueueForm>() : new ActionUnsortedMap<QueueForm>();
        for (StreamItem streamItem : streamItems) {
            queueItemModules.set(toDTO(streamItem, localTimeZone));
        }
        return queueItemModules;
    }
}
