/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.queue;

import biz.ddcr.shampoo.client.form.queue.QueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm.QUEUE_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelQueueItemLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelQueueItemLinkNotification.CHANNELQUEUE_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.queue.log.QueueLog;
import biz.ddcr.shampoo.server.domain.queue.log.QueueLog.QUEUE_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class QueueFacadeService extends GenericService implements QueueFacadeServiceInterface {

    private QueueDTOAssemblerInterface queueDTOAssembler;
    private ChannelManagerInterface channelManager;
    private SecurityManagerInterface securityManager;

    public QueueDTOAssemblerInterface getQueueDTOAssembler() {
        return queueDTOAssembler;
    }

    public void setQueueDTOAssembler(QueueDTOAssemblerInterface queueDTOAssembler) {
        this.queueDTOAssembler = queueDTOAssembler;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public ActionCollection<? extends QueueForm> getSecuredQueueItemsForChannelId(GroupAndSort<QUEUE_TAGS> constraint, String channelId, String localTimeZone) throws Exception {
        Collection<QueueItem> queueItems = getChannelManager().getFilteredQueueItemsForChannelId(constraint, channelId);
        ActionCollection<? extends QueueForm> forms = queueDTOAssembler.toDTO(queueItems, localTimeZone, constraint==null || constraint.getSortByFeature()==null);

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(QUEUE_LOG_OPERATION.read, queueItems);
        return forms;
    }
    @Override
    public ActionCollectionEntry<? extends QueueForm> getSecuredNowPlayingItemForChannelId(String channelId, String localTimeZone) throws Exception {
        Channel channel = getChannelManager().getChannel(channelId);
        StreamItem streamItem = channel.getStreamItem();
        ActionCollectionEntry<? extends QueueForm> form = queueDTOAssembler.toDTO(streamItem, localTimeZone);

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(QUEUE_LOG_OPERATION.read, streamItem);
        return form;
    }

    protected void log(QUEUE_LOG_OPERATION operation, Collection<QueueItem> queueItems) throws Exception {
        if (queueItems != null && !queueItems.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<QueueLog> logs = new HashSet<QueueLog>();
            for (QueueItem queueItem : queueItems) {
                logs.add(
                    new QueueLog(
                        operation,
                        queueItem.getRefID(),
                        queueItem.getStartTime(),
                        queueItem.getChannel() != null ? queueItem.getChannel().getLabel() : null,
                        operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), queueItem)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(QUEUE_LOG_OPERATION operation, StreamItem streamItem) throws Exception {
        log(operation, streamItem, null);
    }
    
    protected void log(QUEUE_LOG_OPERATION operation, StreamItem streamItem, Collection<Channel> supplementaryChannels) throws Exception {
        if (streamItem != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), streamItem);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(
                new QueueLog(
                    operation,
                    streamItem.getRefID(),
                    streamItem.getStartTime(),
                    streamItem.getChannel() != null ? streamItem.getChannel().getLabel() : null,
                    allBoundChannels));
        }
    }
    
    protected void notify(CHANNELQUEUE_NOTIFICATION_OPERATION operation, Channel channel, QueueItem queue) throws Exception {
        notify(operation, channel, queue, operation.getRecipients(getSecurityManager(), channel, queue));
    }

    protected void notify(CHANNELQUEUE_NOTIFICATION_OPERATION operation, Channel channel, QueueItem queue, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && queue != null && recipients != null) {
            Collection<ChannelQueueItemLinkNotification> notifications = new HashSet<ChannelQueueItemLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ChannelQueueItemLinkNotification(operation, channel, queue, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
    
}
