/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.queue;

import biz.ddcr.shampoo.client.form.queue.QueueForm;
import biz.ddcr.shampoo.client.form.queue.QueueForm.QUEUE_TAGS;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.serviceAsync.queue.QueueRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;

/**
 *
 * @author okay_awright
 **/
public class QueueRPCService extends GWTRPCLocalThreadService implements QueueRPCServiceInterface {

    private QueueFacadeServiceInterface queueFacadeService;

    public QueueFacadeServiceInterface getQueueFacadeService() {
        return queueFacadeService;
    }

    public void setQueueFacadeService(QueueFacadeServiceInterface queueFacadeService) {
        this.queueFacadeService = queueFacadeService;
    }

    @Override
    public ActionCollection<? extends QueueForm> getSecuredQueueItemsForChannelId(GroupAndSort<QUEUE_TAGS> constraint, String channelId) throws Exception {
        return queueFacadeService.getSecuredQueueItemsForChannelId(constraint, channelId, null);
    }
    @Override
    public ActionCollection<? extends QueueForm> getSecuredQueueItemsForChannelId(GroupAndSort<QUEUE_TAGS> constraint, String channelId, String localTimeZone) throws Exception {
        return queueFacadeService.getSecuredQueueItemsForChannelId(constraint, channelId, localTimeZone);
    }

    @Override
    public ActionCollectionEntry<? extends QueueForm> getSecuredNowPlayingItemForChannelId(String channelId) throws Exception {
        return queueFacadeService.getSecuredNowPlayingItemForChannelId(channelId, null);
    }
    @Override
    public ActionCollectionEntry<? extends QueueForm> getSecuredNowPlayingItemForChannelId(String channelId, String localTimeZone) throws Exception {
        return queueFacadeService.getSecuredNowPlayingItemForChannelId(channelId, localTimeZone);
    }

}
