/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.security;

import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;

/**
 *
 * @author okay_awright
 **/
public class AuthorizationFacadeService extends GenericService implements AuthorizationFacadeServiceInterface {

    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private TrackManagerInterface trackManager;
    private SecurityManagerInterface securityManager;

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }
    
    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public boolean checkAccessViewChannels() throws Exception {
        return getChannelManager().checkAccessViewChannels();
    }

    @Override
    public boolean checkAccessViewTimetableSlots() throws Exception {
        return getChannelManager().checkAccessViewTimetableSlots();
    }

    @Override
    public boolean checkAccessViewChannel(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessViewChannel(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewTimetableSlot(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessViewTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessAddChannels() throws Exception {
        return getChannelManager().checkAccessAddChannels();
    }

    @Override
    public boolean checkAccessAddTimetableSlots() throws Exception {
        return getChannelManager().checkAccessAddTimetableSlots();
    }

    @Override
    public boolean checkAccessAddTimetableSlot(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessAddTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessUpdateChannels() throws Exception {
        return getChannelManager().checkAccessUpdateChannels();
    }

    @Override
    public boolean checkAccessUpdateTimetableSlots() throws Exception {
        return getChannelManager().checkAccessUpdateTimetableSlots();
    }

    @Override
    public boolean checkAccessUpdateChannel(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessUpdateChannel(channelIdToCheck);
    }

    @Override
    public boolean checkAccessUpdateTimetableSlot(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessUpdateTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessLimitedPlaylistDependencyUpdateTimetable(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessLimitedTimetableDependencyUpdatePlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteChannels() throws Exception {
        return getChannelManager().checkAccessDeleteChannels();
    }

    @Override
    public boolean checkAccessDeleteTimetableSlots() throws Exception {
        return getChannelManager().checkAccessDeleteTimetableSlots();
    }

    @Override
    public boolean checkAccessDeleteChannel(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessDeleteChannel(channelIdToCheck);
    }

    @Override
    public boolean checkAccessPollStatusStreamer(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessQueryStatusStreamer(channelIdToCheck);
    }
    
    @Override
    public boolean checkAccessListenStreamers() throws Exception {
        return getChannelManager().checkAccessListenStreamers();
    }
    
    @Override
    public boolean checkAccessListenStreamer(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessListenStreamer(channelIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteTimetableSlot(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessDeleteTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewProgrammes() throws Exception {
        return getProgrammeManager().checkAccessViewProgrammes();
    }

    @Override
    public boolean checkAccessViewProgramme(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessViewProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessAddProgrammes() throws Exception {
        return getProgrammeManager().checkAccessAddProgrammes();
    }

    @Override
    public boolean checkAccessUpdateProgrammes() throws Exception {
        return getProgrammeManager().checkAccessUpdateProgrammes();
    }

    @Override
    public boolean checkAccessUpdateProgramme(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessUpdateProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteProgrammes() throws Exception {
        return getProgrammeManager().checkAccessDeleteProgrammes();
    }

    @Override
    public boolean checkAccessDeleteProgramme(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessDeleteProgramme(programmeIdToCheck);
    }

    public boolean checkAccessViewUsers() throws Exception {
        return getUserManager().checkAccessViewUsers();
    }

    @Override
    public boolean checkAccessViewRestrictedUsers() throws Exception {
        return getUserManager().checkAccessViewRestrictedUsers();
    }

    @Override
    public boolean checkAccessViewAdministrators() throws Exception {
        return getUserManager().checkAccessViewAdministrators();
    }

    @Override
    public boolean checkAccessViewUser(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessViewUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessViewRestrictedUser(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessViewRestrictedUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessViewAdministrator(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessViewAdministrator(userIdToCheck);
    }

    @Override
    public boolean checkAccessAddRestrictedUsers() throws Exception {
        return getUserManager().checkAccessAddRestrictedUsers();
    }

    @Override
    public boolean checkAccessAddAdministrators() throws Exception {
        return getUserManager().checkAccessAddAdministrators();
    }

    @Override
    public boolean checkAccessUpdateRestrictedUsers() throws Exception {
        return getUserManager().checkAccessUpdateRestrictedUsers();
    }

    @Override
    public boolean checkAccessUpdateAdministrators() throws Exception {
        return getUserManager().checkAccessUpdateAdministrators();
    }

    @Override
    public boolean checkAccessUpdateRestrictedUser(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessUpdateRestrictedUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessUpdateAdministrator(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessUpdateAdministrator(userIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteAdministrators() throws Exception {
        return getUserManager().checkAccessDeleteAdministrators();
    }

    @Override
    public boolean checkAccessDeleteRestrictedUsers() throws Exception {
        return getUserManager().checkAccessDeleteRestrictedUsers();
    }

    @Override
    public boolean checkAccessDeleteRestrictedUser(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessDeleteRestrictedUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteAdministrator(String userIdToCheck) throws Exception {
        return getUserManager().checkAccessDeleteAdministrator(userIdToCheck);
    }

    @Override
    public boolean checkAccessViewPlaylists() throws Exception {
        return getProgrammeManager().checkAccessViewPlaylists();
    }

    @Override
    public boolean checkAccessViewPlaylist(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessViewPlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessAddPlaylists() throws Exception {
        return getProgrammeManager().checkAccessAddPlaylists();
    }

    @Override
    public boolean checkAccessAddPlaylist(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessAddPlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessUpdatePlaylists() throws Exception {
        return getProgrammeManager().checkAccessUpdatePlaylists();
    }

    @Override
    public boolean checkAccessUpdatePlaylist(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessUpdatePlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessDeletePlaylists() throws Exception {
        return getProgrammeManager().checkAccessDeletePlaylists();
    }

    @Override
    public boolean checkAccessDeletePlaylist(String programmeIdToCheck) throws Exception {
        return getProgrammeManager().checkAccessDeletePlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessViewPendingTracks() throws Exception {
        return getTrackManager().checkAccessViewPendingTracks();
    }

    @Override
    public boolean checkAccessViewBroadcastTracks() throws Exception {
        return getTrackManager().checkAccessViewBroadcastTracks();
    }

    @Override
    public boolean checkAccessViewBroadcastTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessViewBroadcastTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessViewPendingTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessViewPendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessViewTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessViewTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessAddBroadcastTracks() throws Exception {
        return getTrackManager().checkAccessAddBroadcastTracks();
    }

    @Override
    public boolean checkAccessAddPendingTracks() throws Exception {
        return getTrackManager().checkAccessAddPendingTracks();
    }

    @Override
    public boolean checkAccessUpdateBroadcastTracks() throws Exception {
        return getTrackManager().checkAccessUpdateBroadcastTracks();
    }

    @Override
    public boolean checkAccessUpdatePendingTracks() throws Exception {
        return getTrackManager().checkAccessUpdatePendingTracks();
    }

    @Override
    public boolean checkAccessUpdateBroadcastTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessUpdateBroadcastTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessUpdatePendingTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessUpdatePendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessDeletePendingTracks() throws Exception {
        return getTrackManager().checkAccessDeletePendingTracks();
    }

    @Override
    public boolean checkAccessDeleteBroadcastTracks() throws Exception {
        return getTrackManager().checkAccessDeleteBroadcastTracks();
    }

    @Override
    public boolean checkAccessDeleteBroadcastTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessDeleteBroadcastTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessDeletePendingTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessDeletePendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessViewTracks() throws Exception {
        return getTrackManager().checkAccessViewTracks();
    }

    @Override
    public boolean checkAccessReviewPendingTracks() throws Exception {
        return getTrackManager().checkAccessReviewPendingTracks();
    }

    @Override
    public boolean checkAccessReviewPendingTrack(String trackIdToCheck) throws Exception {
        return getTrackManager().checkAccessReviewPendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteJournal(String logIdToCheck) throws Exception {
        return getSecurityManager().checkAccessDeleteJournal(logIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteJournals() throws Exception {
        return getSecurityManager().checkAccessDeleteJournals();
    }

    @Override
    public boolean checkAccessViewJournal(String logIdToCheck) throws Exception {
        return getSecurityManager().checkAccessViewJournal(logIdToCheck);
    }

    @Override
    public boolean checkAccessViewJournals() throws Exception {
        return getSecurityManager().checkAccessViewJournals();
    }

    @Override
    public boolean checkAccessViewNotifications() throws Exception {
        return getSecurityManager().checkAccessViewNotifications();
    }

    @Override
    public boolean checkAccessViewNotification(String messageIdToCheck) throws Exception {
        return getSecurityManager().checkAccessViewNotification(messageIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteNotifications() throws Exception {
        return getSecurityManager().checkAccessDeleteNotifications();
    }

    @Override
    public boolean checkAccessDeleteNotification(String messageIdToCheck) throws Exception {
        return getSecurityManager().checkAccessDeleteNotification(messageIdToCheck);
    }
    
    @Override
    public boolean checkAccessDeleteArchive(String archiveIdToCheck) throws Exception {
        return getChannelManager().checkAccessDeleteArchive(archiveIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteArchives() throws Exception {
        return getChannelManager().checkAccessDeleteArchives();
    }

    @Override
    public boolean checkAccessViewArchive(String archiveIdToCheck) throws Exception {
        return getChannelManager().checkAccessViewArchive(archiveIdToCheck);
    }

    @Override
    public boolean checkAccessViewArchives() throws Exception {
        return getChannelManager().checkAccessViewArchives();
    }

    @Override
    public boolean checkAccessViewQueues() throws Exception {
        return getChannelManager().checkAccessViewQueues();
    }

    @Override
    public boolean checkAccessViewQueue(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessViewQueue(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewWebservices() throws Exception {
        return getChannelManager().checkAccessViewWebservices();
    }

    @Override
    public boolean checkAccessViewWebservice(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessViewWebservice(channelIdToCheck);
    }

    @Override
    public boolean checkAccessAddWebservices() throws Exception {
        return getChannelManager().checkAccessAddWebservices();
    }

    @Override
    public boolean checkAccessUpdateWebservices() throws Exception {
        return getChannelManager().checkAccessUpdateWebservices();
    }

    @Override
    public boolean checkAccessUpdateWebservice(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessUpdateWebservice(channelIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteWebservices() throws Exception {
        return getChannelManager().checkAccessDeleteWebservices();
    }

    @Override
    public boolean checkAccessDeleteWebservice(String channelIdToCheck) throws Exception {
        return getChannelManager().checkAccessDeleteWebservice(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewReports() throws Exception {
        return getChannelManager().checkAccessViewReports();
    }

    @Override
    public boolean checkAccessViewReport(String reportIdToCheck) throws Exception {
        return getChannelManager().checkAccessViewReport(reportIdToCheck);
    }

    @Override
    public boolean checkAccessAddReports() throws Exception {
        return getChannelManager().checkAccessAddReports();
    }

    @Override
    public boolean checkAccessDeleteReports() throws Exception {
        return getChannelManager().checkAccessDeleteReports();
    }

    @Override
    public boolean checkAccessDeleteReport(String reportIdToCheck) throws Exception {
        return getChannelManager().checkAccessDeleteReport(reportIdToCheck);
    }
    
}
