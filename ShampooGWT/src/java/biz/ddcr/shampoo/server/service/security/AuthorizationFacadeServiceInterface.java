/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.security;




/**
 *
 * @author okay_awright
 **/
public interface AuthorizationFacadeServiceInterface {

    //Check access to ressources
    
    public boolean checkAccessViewRestrictedUsers() throws Exception;
    
    public boolean checkAccessViewAdministrators() throws Exception;

    public boolean checkAccessViewUser(String userId) throws Exception;
    
    public boolean checkAccessViewRestrictedUser(String userId) throws Exception;
    
    public boolean checkAccessViewAdministrator(String userId) throws Exception;
    
    public boolean checkAccessAddRestrictedUsers() throws Exception;
    
    public boolean checkAccessAddAdministrators() throws Exception;
    
    public boolean checkAccessUpdateRestrictedUsers() throws Exception;
    
    public boolean checkAccessUpdateAdministrators() throws Exception;
    
    public boolean checkAccessUpdateRestrictedUser(String userId) throws Exception;
    
    public boolean checkAccessUpdateAdministrator(String userId) throws Exception;
    
    public boolean checkAccessDeleteAdministrators() throws Exception;
    
    public boolean checkAccessDeleteRestrictedUsers() throws Exception;
    
    public boolean checkAccessDeleteRestrictedUser(String userId) throws Exception;
    
    public boolean checkAccessDeleteAdministrator(String userId) throws Exception;

    //Check access to ressources
    
    public boolean checkAccessViewTracks() throws Exception;
    
    public boolean checkAccessViewPendingTracks() throws Exception;
    
    public boolean checkAccessViewBroadcastTracks() throws Exception;
    
    public boolean checkAccessViewBroadcastTrack(String trackIdToCheck) throws Exception;
    
    public boolean checkAccessViewPendingTrack(String trackIdToCheck) throws Exception;
    
    public boolean checkAccessViewTrack(String trackIdToCheck) throws Exception;
    
    public boolean checkAccessAddBroadcastTracks() throws Exception;
    
    public boolean checkAccessAddPendingTracks() throws Exception;
    
    public boolean checkAccessUpdateBroadcastTracks() throws Exception;
    
    public boolean checkAccessUpdatePendingTracks() throws Exception;
    
    public boolean checkAccessUpdateBroadcastTrack(String trackIdToCheck) throws Exception;
    
    public boolean checkAccessUpdatePendingTrack(String trackIdToCheck) throws Exception;
    
    public boolean checkAccessDeletePendingTracks() throws Exception;
    
    public boolean checkAccessDeleteBroadcastTracks() throws Exception;
    
    public boolean checkAccessDeleteBroadcastTrack(String trackIdToCheck) throws Exception;
    
    public boolean checkAccessDeletePendingTrack(String trackIdToCheck) throws Exception;

    //Supplementary checks
    
    public boolean checkAccessReviewPendingTracks() throws Exception;
    
    public boolean checkAccessReviewPendingTrack(String trackIdToCheck) throws Exception;

    //Check access to resource
    
    public boolean checkAccessViewChannels() throws Exception;
    
    public boolean checkAccessViewChannel(String channelId) throws Exception;
    
    public boolean checkAccessAddChannels() throws Exception;
    
    public boolean checkAccessUpdateChannels() throws Exception;
    
    public boolean checkAccessUpdateChannel(String channelId) throws Exception;
    
    public boolean checkAccessDeleteChannels() throws Exception;
    
    public boolean checkAccessDeleteChannel(String channelId) throws Exception;

    public boolean checkAccessPollStatusStreamer(String channelId) throws Exception;
    
    public boolean checkAccessListenStreamers() throws Exception;

    public boolean checkAccessListenStreamer(String channelId) throws Exception;
    
    public boolean checkAccessViewTimetableSlots() throws Exception;
    
    public boolean checkAccessViewTimetableSlot(String channelId) throws Exception;
    
    public boolean checkAccessAddTimetableSlots() throws Exception;
    
    public boolean checkAccessAddTimetableSlot(String channelId) throws Exception;
    
    public boolean checkAccessUpdateTimetableSlots() throws Exception;
    
    public boolean checkAccessUpdateTimetableSlot(String channelId) throws Exception;
    
    public boolean checkAccessDeleteTimetableSlots() throws Exception;
    
    public boolean checkAccessDeleteTimetableSlot(String channelId) throws Exception;
    
    //Special credentials

    public boolean checkAccessLimitedPlaylistDependencyUpdateTimetable(String programmeIdToCheck) throws Exception;

    //Check access to resource
    
    public boolean checkAccessViewProgrammes() throws Exception;
    
    public boolean checkAccessViewProgramme(String programmeId) throws Exception;
    
    public boolean checkAccessAddProgrammes() throws Exception;
    
    public boolean checkAccessUpdateProgrammes() throws Exception;
    
    public boolean checkAccessUpdateProgramme(String programmeId) throws Exception;
    
    public boolean checkAccessDeleteProgrammes() throws Exception;
    
    public boolean checkAccessDeleteProgramme(String programmeId) throws Exception;

    //Special credentials
    
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(String programmeId) throws Exception;

    
    public boolean checkAccessViewPlaylists() throws Exception;
    
    public boolean checkAccessViewPlaylist(String programmeId) throws Exception;
    
    public boolean checkAccessAddPlaylists() throws Exception;
    
    public boolean checkAccessAddPlaylist(String programmeId) throws Exception;
    
    public boolean checkAccessUpdatePlaylists() throws Exception;
    
    public boolean checkAccessUpdatePlaylist(String programmeId) throws Exception;
    
    public boolean checkAccessDeletePlaylists() throws Exception;
    
    public boolean checkAccessDeletePlaylist(String programmeId) throws Exception;

    //Check access to resource
    public boolean checkAccessViewJournals() throws Exception;
    public boolean checkAccessViewJournal(String logIdToCheck) throws Exception;
    public boolean checkAccessDeleteJournals() throws Exception;
    public boolean checkAccessDeleteJournal(String logIdToCheck) throws Exception;

    //Check access to resource
    public boolean checkAccessViewNotifications() throws Exception;
    public boolean checkAccessViewNotification(String messageIdToCheck) throws Exception;
    public boolean checkAccessDeleteNotifications() throws Exception;
    public boolean checkAccessDeleteNotification(String messageIdToCheck) throws Exception;

    //Check access to resource
    public boolean checkAccessViewArchives() throws Exception;
    public boolean checkAccessViewArchive(String archiveIdToCheck) throws Exception;
    public boolean checkAccessDeleteArchives() throws Exception;
    public boolean checkAccessDeleteArchive(String archiveIdToCheck) throws Exception;
    public boolean checkAccessViewReports() throws Exception;
    public boolean checkAccessViewReport(String reportIdToCheck) throws Exception;
    public boolean checkAccessAddReports() throws Exception;
    public boolean checkAccessDeleteReports() throws Exception;
    public boolean checkAccessDeleteReport(String reportIdToCheck) throws Exception;

    //Check accessto resource
    public boolean checkAccessViewQueues() throws Exception;
    public boolean checkAccessViewQueue(String channelIdToCheck) throws Exception;

    //Check access to resource
    public boolean checkAccessViewWebservices() throws Exception;
    public boolean checkAccessViewWebservice(String channelIdToCheck) throws Exception;
    public boolean checkAccessAddWebservices() throws Exception;
    public boolean checkAccessUpdateWebservices() throws Exception;
    public boolean checkAccessUpdateWebservice(String channelIdToCheck) throws Exception;
    public boolean checkAccessDeleteWebservices() throws Exception;
    public boolean checkAccessDeleteWebservice(String channelIdToCheck) throws Exception;

}
