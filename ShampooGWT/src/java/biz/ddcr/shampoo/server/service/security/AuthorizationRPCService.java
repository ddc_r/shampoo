/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.security;

import biz.ddcr.shampoo.client.serviceAsync.security.AuthorizationRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;

/**
 *
 * @author okay_awright
 **/
public class AuthorizationRPCService extends GWTRPCLocalThreadService implements AuthorizationRPCServiceInterface {

    private AuthorizationFacadeServiceInterface authorizationFacadeService;

    public AuthorizationFacadeServiceInterface getAuthorizationFacadeService() {
        return authorizationFacadeService;
    }

    public void setAuthorizationFacadeService(AuthorizationFacadeServiceInterface authorizationFacadeService) {
        this.authorizationFacadeService = authorizationFacadeService;
    }

    @Override
    public boolean checkAccessViewChannels() throws Exception {
        return authorizationFacadeService.checkAccessViewChannels();
    }

    @Override
    public boolean checkAccessViewChannel(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewChannel(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewTimetableSlots() throws Exception {
        return authorizationFacadeService.checkAccessViewTimetableSlots();
    }

    @Override
    public boolean checkAccessViewTimetableSlot(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessAddChannels() throws Exception {
        return authorizationFacadeService.checkAccessAddChannels();
    }

    @Override
    public boolean checkAccessAddTimetableSlots() throws Exception {
        return authorizationFacadeService.checkAccessAddTimetableSlots();
    }

    @Override
    public boolean checkAccessAddTimetableSlot(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessAddTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessUpdateChannels() throws Exception {
        return authorizationFacadeService.checkAccessUpdateChannels();
    }

    @Override
    public boolean checkAccessUpdateChannel(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateChannel(channelIdToCheck);
    }

    @Override
    public boolean checkAccessUpdateTimetableSlots() throws Exception {
        return authorizationFacadeService.checkAccessUpdateTimetableSlots();
    }

    @Override
    public boolean checkAccessUpdateTimetableSlot(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteChannels() throws Exception {
        return authorizationFacadeService.checkAccessDeleteChannels();
    }

    @Override
    public boolean checkAccessDeleteChannel(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteChannel(channelIdToCheck);
    }

    @Override
    public boolean checkAccessPollStatusStreamer(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessPollStatusStreamer(channelIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteTimetableSlots() throws Exception {
        return authorizationFacadeService.checkAccessDeleteTimetableSlots();
    }

    @Override
    public boolean checkAccessDeleteTimetableSlot(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteTimetableSlot(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewProgrammes() throws Exception {
        return authorizationFacadeService.checkAccessViewProgrammes();
    }

    @Override
    public boolean checkAccessViewProgramme(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessLimitedTrackDependencyUpdateProgramme(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessLimitedTrackDependencyUpdateProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessLimitedPlaylistDependencyUpdateTimetable(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessLimitedPlaylistDependencyUpdateTimetable(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessAddProgrammes() throws Exception {
        return authorizationFacadeService.checkAccessAddProgrammes();
    }

    @Override
    public boolean checkAccessUpdateProgrammes() throws Exception {
        return authorizationFacadeService.checkAccessUpdateProgrammes();
    }

    @Override
    public boolean checkAccessUpdateProgramme(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteProgrammes() throws Exception {
        return authorizationFacadeService.checkAccessDeleteProgrammes();
    }

    @Override
    public boolean checkAccessDeleteProgramme(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteProgramme(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessViewUsers() throws Exception {
        return authorizationFacadeService.checkAccessViewRestrictedUsers();
    }

    @Override
    public boolean checkAccessViewRestrictedUsers() throws Exception {
        return authorizationFacadeService.checkAccessViewRestrictedUsers();
    }

    @Override
    public boolean checkAccessViewAdministrators() throws Exception {
        return authorizationFacadeService.checkAccessViewAdministrators();
    }

    @Override
    public boolean checkAccessViewUser(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewUser(userIdToCheck);
    }
    
    @Override
    public boolean checkAccessViewRestrictedUser(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewRestrictedUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessViewAdministrator(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewAdministrator(userIdToCheck);
    }

    @Override
    public boolean checkAccessAddRestrictedUsers() throws Exception {
        return authorizationFacadeService.checkAccessAddRestrictedUsers();
    }

    @Override
    public boolean checkAccessAddAdministrators() throws Exception {
        return authorizationFacadeService.checkAccessAddAdministrators();
    }

    @Override
    public boolean checkAccessUpdateRestrictedUsers() throws Exception {
        return authorizationFacadeService.checkAccessUpdateRestrictedUsers();
    }

    @Override
    public boolean checkAccessUpdateAdministrators() throws Exception {
        return authorizationFacadeService.checkAccessUpdateAdministrators();
    }

    @Override
    public boolean checkAccessUpdateRestrictedUser(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateRestrictedUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessUpdateAdministrator(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateAdministrator(userIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteAdministrators() throws Exception {
        return authorizationFacadeService.checkAccessDeleteAdministrators();
    }

    @Override
    public boolean checkAccessDeleteRestrictedUsers() throws Exception {
        return authorizationFacadeService.checkAccessDeleteRestrictedUsers();
    }

    @Override
    public boolean checkAccessDeleteRestrictedUser(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteRestrictedUser(userIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteAdministrator(String userIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteAdministrator(userIdToCheck);
    }

    @Override
    public boolean checkAccessViewPlaylists() throws Exception {
        return authorizationFacadeService.checkAccessViewPlaylists();
    }

    @Override
    public boolean checkAccessViewPlaylist(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewPlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessAddPlaylists() throws Exception {
        return authorizationFacadeService.checkAccessAddPlaylists();
    }

    @Override
    public boolean checkAccessAddPlaylist(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessAddPlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessUpdatePlaylists() throws Exception {
        return authorizationFacadeService.checkAccessUpdatePlaylists();
    }

    @Override
    public boolean checkAccessUpdatePlaylist(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdatePlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessDeletePlaylists() throws Exception {
        return authorizationFacadeService.checkAccessDeletePlaylists();
    }

    @Override
    public boolean checkAccessDeletePlaylist(String programmeIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeletePlaylist(programmeIdToCheck);
    }

    @Override
    public boolean checkAccessViewPendingTracks() throws Exception {
        return authorizationFacadeService.checkAccessViewPendingTracks();
    }

    @Override
    public boolean checkAccessViewBroadcastTracks() throws Exception {
        return authorizationFacadeService.checkAccessViewBroadcastTracks();
    }

    @Override
    public boolean checkAccessViewBroadcastTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewBroadcastTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessViewPendingTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewPendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessAddBroadcastTracks() throws Exception {
        return authorizationFacadeService.checkAccessAddBroadcastTracks();
    }

    @Override
    public boolean checkAccessAddPendingTracks() throws Exception {
        return authorizationFacadeService.checkAccessAddPendingTracks();
    }

    @Override
    public boolean checkAccessUpdateBroadcastTracks() throws Exception {
        return authorizationFacadeService.checkAccessUpdateBroadcastTracks();
    }

    @Override
    public boolean checkAccessUpdatePendingTracks() throws Exception {
        return authorizationFacadeService.checkAccessUpdatePendingTracks();
    }

    @Override
    public boolean checkAccessUpdateBroadcastTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateBroadcastTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessUpdatePendingTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdatePendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessDeletePendingTracks() throws Exception {
        return authorizationFacadeService.checkAccessDeletePendingTracks();
    }

    @Override
    public boolean checkAccessDeleteBroadcastTracks() throws Exception {
        return authorizationFacadeService.checkAccessDeleteBroadcastTracks();
    }

    @Override
    public boolean checkAccessDeleteBroadcastTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteBroadcastTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessDeletePendingTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeletePendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessViewTracks() throws Exception {
        return authorizationFacadeService.checkAccessViewTracks();
    }

    @Override
    public boolean checkAccessReviewPendingTracks() throws Exception {
        return authorizationFacadeService.checkAccessReviewPendingTracks();
    }

    @Override
    public boolean checkAccessReviewPendingTrack(String trackIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessReviewPendingTrack(trackIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteJournal(String logIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteJournal(logIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteJournals() throws Exception {
        return authorizationFacadeService.checkAccessDeleteJournals();
    }

    @Override
    public boolean checkAccessViewJournal(String logIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewJournal(logIdToCheck);
    }

    @Override
    public boolean checkAccessViewJournals() throws Exception {
        return authorizationFacadeService.checkAccessViewJournals();
    }

    @Override
    public boolean checkAccessDeleteNotification(String messageIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteNotification(messageIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteNotifications() throws Exception {
        return authorizationFacadeService.checkAccessDeleteNotifications();
    }

    @Override
    public boolean checkAccessViewNotification(String messageIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewNotification(messageIdToCheck);
    }

    @Override
    public boolean checkAccessViewNotifications() throws Exception {
        return authorizationFacadeService.checkAccessViewNotifications();
    }

    @Override
    public boolean checkAccessDeleteArchive(String archiveIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteArchive(archiveIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteArchives() throws Exception {
        return authorizationFacadeService.checkAccessDeleteArchives();
    }

    @Override
    public boolean checkAccessViewArchive(String archiveIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewArchive(archiveIdToCheck);
    }

    @Override
    public boolean checkAccessViewArchives() throws Exception {
        return authorizationFacadeService.checkAccessViewArchives();
    }

    @Override
    public boolean checkAccessViewQueues() throws Exception {
        return authorizationFacadeService.checkAccessViewQueues();
    }

    @Override
    public boolean checkAccessViewQueue(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewQueue(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewWebservices() throws Exception {
        return authorizationFacadeService.checkAccessViewWebservices();
    }

    @Override
    public boolean checkAccessViewWebservice(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewWebservice(channelIdToCheck);
    }

    @Override
    public boolean checkAccessAddWebservices() throws Exception {
        return authorizationFacadeService.checkAccessAddWebservices();
    }

    @Override
    public boolean checkAccessUpdateWebservices() throws Exception {
        return authorizationFacadeService.checkAccessUpdateWebservices();
    }

    @Override
    public boolean checkAccessUpdateWebservice(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessUpdateWebservice(channelIdToCheck);
    }

    @Override
    public boolean checkAccessDeleteWebservices() throws Exception {
        return authorizationFacadeService.checkAccessDeleteWebservices();
    }

    @Override
    public boolean checkAccessDeleteWebservice(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteWebservice(channelIdToCheck);
    }

    @Override
    public boolean checkAccessListenStreamers() throws Exception {
        return authorizationFacadeService.checkAccessListenStreamers();
    }

    @Override
    public boolean checkAccessListenStreamer(String channelIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessListenStreamer(channelIdToCheck);
    }

    @Override
    public boolean checkAccessViewReport(String reportIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessViewReport(reportIdToCheck);
    }

    @Override
    public boolean checkAccessViewReports() throws Exception {
        return authorizationFacadeService.checkAccessViewReports();
    }

    @Override
    public boolean checkAccessAddReports() throws Exception {
        return authorizationFacadeService.checkAccessAddReports();
    }

    @Override
    public boolean checkAccessDeleteReports() throws Exception {
        return authorizationFacadeService.checkAccessDeleteReports();
    }

    @Override
    public boolean checkAccessDeleteReport(String reportIdToCheck) throws Exception {
        return authorizationFacadeService.checkAccessDeleteReport(reportIdToCheck);
    }

}
