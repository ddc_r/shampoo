/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.security;

import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.form.user.LoginForm;
import biz.ddcr.shampoo.client.form.user.ProfileDetailForm;
import biz.ddcr.shampoo.client.form.user.ProfilePasswordForm;
import biz.ddcr.shampoo.client.form.user.ProfileRightForm;
import biz.ddcr.shampoo.client.helper.errors.AdministratorsCannotRequestException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.RequestLimitReachedException;
import biz.ddcr.shampoo.client.helper.errors.RequestAlreadyScheduledException;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification.CHANNELLISTENER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.log.ProfileLog.PROFILE_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.user.log.ProfileLog;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification;
import biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification.RESTRICTEDUSER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerInterface;
import biz.ddcr.shampoo.server.service.track.TrackDTOAssembler;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 **/
public class LoginFacadeService extends GenericService implements LoginFacadeServiceInterface {

    private interface ProfileRightFormVisitor extends UserVisitor {

        public ProfileRightForm get();
    }
    private interface BooleanVisitor extends UserVisitor {

        public boolean get();
    }
    private SecurityManagerInterface securityManager;
    private ChannelManagerInterface channelManager;
    private UserManagerInterface userManager;
    private QueueSchedulerInterface queueScheduler;
    private TrackManagerInterface trackManager;

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public QueueSchedulerInterface getQueueScheduler() {
        return queueScheduler;
    }

    public void setQueueScheduler(QueueSchedulerInterface queueScheduler) {
        this.queueScheduler = queueScheduler;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    @Override
    public void logIn(LoginForm loginForm) throws Exception {
        getSecurityManager().logInCaseInsensitive(loginForm.getUsername(), loginForm.getPassword(), loginForm.isRememberme());
        //log it
        log(PROFILE_LOG_OPERATION.login);
    }

    @Override
    public void logInFromCookie() throws Exception {
        getSecurityManager().autoLogInFromCookie();
        //log it
        log(PROFILE_LOG_OPERATION.login);
    }

    @Override
    public void logOut() throws Exception {
        //log it
        log(PROFILE_LOG_OPERATION.logout);
        getSecurityManager().logOut();
    }

    @Override
    public void updateCurrentlyAuthenticatedUserPasswordAndProfile(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile) throws Exception {
        updateCurrentlyAuthenticatedUserPassword(newProfilePassword);
        updateCurrentlyAuthenticatedUserProfile(newProfile);
    }

    @Override
    public void updateCurrentlyAuthenticatedUserPasswordAndProfile(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception {
        updateCurrentlyAuthenticatedUserPassword(newProfilePassword);
        updateCurrentlyAuthenticatedUserProfile(newProfile);
        updateCurrentlyAuthenticatedUserListenerRights(listenerRights);
    }

    @Override
    public void updateCurrentlyAuthenticatedUserProfile(ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception {
        updateCurrentlyAuthenticatedUserProfile(newProfile);
        updateCurrentlyAuthenticatedUserListenerRights(listenerRights);
    }

    @Override
    public void updateCurrentlyAuthenticatedUserProfile(ProfileDetailForm newProfile) throws Exception {
        //Then the email address
        if (newProfile.getEmail() != null
                || newProfile.getFirstAndLastName() != null
                || newProfile.getTimezone() != null) {

            if (getSecurityManager().updateCurrentlyAuthenticatedUserProfileData(newProfile.getEmail(), newProfile.isEmailNotification(), newProfile.getFirstAndLastName(), newProfile.getTimezone())) {
                //Log it
                log(PROFILE_LOG_OPERATION.edit);
                //Notify responsible users of any changes
                //Notify responsible users of any changes
                User authenticatedUser = getSecurityManager().getCurrentlyAuthenticatedUser();
                UserVisitor visitor = new UserVisitor() {

                    @Override
                    public void visit(RestrictedUser user) throws Exception {
                        LoginFacadeService.this.notify(RESTRICTEDUSER_NOTIFICATION_OPERATION.edit, user);
                    }

                    @Override
                    public void visit(Administrator user) throws Exception {
                        //Do nothing
                    }
                };
                authenticatedUser.acceptVisit(visitor);
            }
        }
    }

    @Override
    public void updateCurrentlyAuthenticatedUserListenerRights(final Collection<String> listenerRights) throws Exception {
        if (listenerRights != null) {

            User authenticatedUser = getSecurityManager().getCurrentlyAuthenticatedUser();
            UserVisitor visitor = new UserVisitor() {

                @Override
                public void visit(RestrictedUser authenticatedRestrictedUser) throws Exception {
                    //Copy all originally bound channels before touching them
                    Set<Channel> originallyLinkedListeners = new HashSet(authenticatedRestrictedUser.getListenerRights());

                    //List open channels that are currently bound to the user
                    //And the new set
                    Collection<String> openChannels = getChannelManager().getOpenChannelLabels();
                    Collection<String> currentChannelLabels = new HashSet<String>();
                    for (Iterator<Channel> i = authenticatedRestrictedUser.getListenerRights().iterator(); i.hasNext();) {
                        Channel c = i.next();
                        if (openChannels.contains(c.getLabel())) {
                            currentChannelLabels.add(c.getLabel());
                        }
                    }
                    Collection<String> updatedChannelLabels = new HashSet<String>();
                    for (String channelEntry : listenerRights) {
                        Channel c = getChannelManager().getChannel(channelEntry);
                        if (openChannels.contains(c.getLabel())) {
                            updatedChannelLabels.add(c.getLabel());
                        }
                    }

                    //See what's new and what's old and then update the user accordingly
                    Collection<String> newChannelLabels = CollectionUtil.exclusion(currentChannelLabels, updatedChannelLabels);
                    Collection<String> deletedChannelLabels = CollectionUtil.exclusion(updatedChannelLabels, currentChannelLabels);
                    if (getSecurityManager().updateCurrentlyAuthenticatedUserListenerRights(newChannelLabels, deletedChannelLabels)) {

                        //Log it
                        log(PROFILE_LOG_OPERATION.edit);
                        //New linked users
                        for (Channel boundChannel : CollectionUtil.exclusion(originallyLinkedListeners, authenticatedRestrictedUser.getListenerRights())) {
                            LoginFacadeService.this.notify(CHANNELLISTENER_NOTIFICATION_OPERATION.add, boundChannel, authenticatedRestrictedUser);
                        }
                        //Old linked users
                        for (Channel boundChannel : CollectionUtil.exclusion(authenticatedRestrictedUser.getListenerRights(), originallyLinkedListeners)) {
                            LoginFacadeService.this.notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, boundChannel, authenticatedRestrictedUser);
                        }
                    }
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    //Do nothing
                }
            };
            authenticatedUser.acceptVisit(visitor);
        }
    }

    @Override
    public void addSelfAuthenticatedUser(String registeredUsername, ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception {
        if (registeredUsername != null && newProfilePassword != null && newProfile != null && listenerRights != null) {

            //Check if username is available
            try {
                getUserManager().getUserByCaseInsensitiveUsername(registeredUsername);
                throw new DuplicatedEntityException(registeredUsername);
            } catch (NoEntityException e) {
                //Ok
            }
            //Check if e-mail is available
            try {
                getUserManager().getUserByCaseInsensitiveEmail(newProfile.getEmail());
                throw new DuplicatedEntityException(newProfile.getEmail());
            } catch (NoEntityException e) {
                //Ok
            }

            //hydrate the entity on-the-fly
            RestrictedUser newUser = new RestrictedUser();
            //Fill in all the fields
            newUser.setEnabled(true);
            newUser.setTimezone(newProfile.getTimezone());
            newUser.setFirstAndLastName(newProfile.getFirstAndLastName());
            newUser.setPassword(newProfilePassword.getNewPassword());
            newUser.setEmail(newProfile.getEmail());
            newUser.setUsername(registeredUsername);
            newUser.setEmailNotification(newProfile.isEmailNotification());

            Collection<String> openChannels = getChannelManager().getOpenChannelLabels();
            for (String channelEntry : listenerRights) {
                if (openChannels.contains(channelEntry)) {
                    Channel c = getChannelManager().loadChannel(channelEntry);
                    newUser.addListenerRight(c);
                }
            }

            //Everything's fine, now make it persistent in the database
            try {

                getUserManager().addSelf(newUser);
                //Log it
                log(PROFILE_LOG_OPERATION.add, newUser);
                //Notify responsible users of any changes
                //All linked entities are then new links
                for (Channel boundChannel : newUser.getListenerRights()) {
                    notify(CHANNELLISTENER_NOTIFICATION_OPERATION.add, boundChannel, newUser);
                }

            } catch (DataIntegrityViolationException e) {
                throw new DuplicatedEntityException(e.getClass().getCanonicalName());
            }

        }
    }

    @Override
    public String getCurrentlyAuthenticatedUserLogin() throws Exception {
        return getSecurityManager().getCurrentlyAuthenticatedUser().getUsername();
    }

    @Override
    public boolean isCurrentlyAuthenticatedUserAnAdministrator() throws Exception {
        return getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator();
    }

    @Override
    public void updateCurrentlyAuthenticatedUserPassword(ProfilePasswordForm newProfilePassword) throws Exception {
        //Change the password if required
        if (newProfilePassword.getOldPassword() != null && newProfilePassword.getNewPassword() != null) {
            if (getSecurityManager().updateCurrentlyAuthenticatedUserPassword(newProfilePassword.getOldPassword(), newProfilePassword.getNewPassword())) {
                //Log it
                log(PROFILE_LOG_OPERATION.edit);
                //Notify responsible users of any changes
                User authenticatedUser = getSecurityManager().getCurrentlyAuthenticatedUser();
                UserVisitor visitor = new UserVisitor() {

                    @Override
                    public void visit(RestrictedUser user) throws Exception {
                        LoginFacadeService.this.notify(RESTRICTEDUSER_NOTIFICATION_OPERATION.edit, user);
                    }

                    @Override
                    public void visit(Administrator user) throws Exception {
                        //Do nothing
                    }
                };
                authenticatedUser.acceptVisit(visitor);

            }
        }
    }

    @Override
    public ProfileRightForm getCurrentlyAuthenticatedRestrictedUserRights() throws Exception {
        User authenticatedUser = getSecurityManager().getCurrentlyAuthenticatedUser();
        ProfileRightFormVisitor visitor = new ProfileRightFormVisitor() {

            private ProfileRightForm form;

            @Override
            public ProfileRightForm get() {
                return form;
            }

            @Override
            public void visit(RestrictedUser authenticatedRestrictedUser) throws Exception {
                ProfileRightForm profileRightForm = new ProfileRightForm();
                profileRightForm.setAnimatorRights(convertProgrammesToIds(authenticatedRestrictedUser.getAnimatorRights()));
                profileRightForm.setContributorRights(convertProgrammesToIds(authenticatedRestrictedUser.getContributorRights()));
                profileRightForm.setCuratorRights(convertProgrammesToIds(authenticatedRestrictedUser.getCuratorRights()));
                profileRightForm.setListenerRights(convertChannelsToIds(authenticatedRestrictedUser.getListenerRights()));
                profileRightForm.setProgrammeManagerRights(convertChannelsToIds(authenticatedRestrictedUser.getProgrammeManagerRights()));
                profileRightForm.setChannelAdministratorRights(convertChannelsToIds(authenticatedRestrictedUser.getChannelAdministratorRights()));

                //Log it
                //UPDATE: Logging of 'reading' events has been disabled for performance sake
                //log(PROFILE_LOG_OPERATION.read);

                form = profileRightForm;
            }

            @Override
            public void visit(Administrator user) throws Exception {
                form = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();

    }

    private Collection<String> convertChannelsToIds(Collection<Channel> channels) {
        Collection<String> channelIds = new HashSet<String>();
        if (channels != null) {
            for (Channel channel : channels) {
                channelIds.add(channel.getLabel());
            }
        }
        return channelIds;
    }

    private Collection<String> convertProgrammesToIds(Collection<Programme> programmes) {
        Collection<String> programmeIds = new HashSet<String>();
        if (programmes != null) {
            for (Programme programme : programmes) {
                programmeIds.add(programme.getLabel());
            }
        }
        return programmeIds;
    }

    @Override
    public ProfileDetailForm getCurrentlyAuthenticatedUserProfileDetails() throws Exception {

        ProfileDetailForm profile = new ProfileDetailForm();
        User authenticatedUser = getSecurityManager().getCurrentlyAuthenticatedUser();
        profile.setEmail(authenticatedUser.getEmail());
        BooleanVisitor visitor = new BooleanVisitor() {

            boolean result;
            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                result = user.isEmailNotification();
            }

            @Override
            public void visit(Administrator user) throws Exception {
                result = false;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        profile.setEmailNotification(visitor.get());
        profile.setFirstAndLastName(authenticatedUser.getFirstAndLastName());
        profile.setTimezone(authenticatedUser.getTimezone());

        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(PROFILE_LOG_OPERATION.read);

        return profile;
    }

    @Override
    public String getCurrentlyAuthenticatedUserTimezone() throws Exception {
        User authenticatedUser = getSecurityManager().getCurrentlyAuthenticatedUser();
        return authenticatedUser.getTimezone();
    }

    @Override
    public void setCurrentlyAuthenticatedUserVote(String songId, VOTE score) throws Exception {
        getSecurityManager().setCurrentlyAuthenticatedUserVote(songId, TrackDTOAssembler.fromDTO(score));
        //Don't log this event
    }

    @Override
    public VOTE getCurrentlyAuthenticatedUserVote(String songId) throws Exception {
        //Don't log this event
        return TrackDTOAssembler.toDTO(getSecurityManager().getCurrentlyAuthenticatedUserVote(songId));
    }

    @Override
    public void resetCurrentlyAuthenticatedUserRequest(String songId) throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            throw new AdministratorsCannotRequestException();
        }
        RestrictedUser restrictedUser = (RestrictedUser) getSecurityManager().getCurrentlyAuthenticatedUser();
        if (getQueueScheduler().checkRequestDeletable(restrictedUser.getUsername(), songId)) {
            getSecurityManager().setCurrentlyAuthenticatedUserRequest(songId, null, null);
        } else {
            throw new RequestAlreadyScheduledException();
        }
        //Don't log this event
    }

    @Override
    public void setCurrentlyAuthenticatedUserRequest(String songId, String channelId, String message) throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            throw new AdministratorsCannotRequestException();
        }
        RestrictedUser user = (RestrictedUser) getSecurityManager().getCurrentlyAuthenticatedUser();
        if (getQueueScheduler().checkNewRequestQueueable(user.getUsername(), channelId)) {
            getSecurityManager().setCurrentlyAuthenticatedUserRequest(songId, channelId, message);
        } else {
            throw new RequestLimitReachedException();
        }
        //Don't log this event
    }

    @Override
    public RequestModule getCurrentlyAuthenticatedUserRequest(String songId) throws Exception {
        //Don't log this event
        Request request = getSecurityManager().getCurrentlyAuthenticatedUserRequest(songId);
        if (request != null) {
            return new RequestModule(
                    request.getChannel() != null ? request.getChannel().getLabel() : null,
                    request.getRequester().getUsername(),
                    request.getMessage(),
                    request.getStreamItem() != null,
                    request.getQueueItem() != null);
        } else {
            return null;
        }
    }

    @Override
    public void sendUserPasswordToMailboxViaEmail(String registeredEmailAddress) throws Exception {
        User user = getUserManager().getUserByCaseInsensitiveEmail(registeredEmailAddress);
        getSecurityManager().sendPasswordToEmail(user);
    }

    @Override
    public void sendUserPasswordToMailboxViaUsername(String registeredUsername) throws Exception {
        User user = getUserManager().getUserByCaseInsensitiveUsername(registeredUsername);
        getSecurityManager().sendPasswordToEmail(user);
    }

    protected void log(PROFILE_LOG_OPERATION operation) throws Exception {
        if (operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            getSecurityManager().addJournal(
                    new ProfileLog(
                    operation,
                    operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), getSecurityManager().getCurrentlyAuthenticatedUser())));
        }
    }

    protected void log(PROFILE_LOG_OPERATION operation, RestrictedUser forcedUser) throws Exception {
        if (operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            getSecurityManager().addJournal(
                    new ProfileLog(
                    operation,
                    operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), forcedUser)));
        }
    }

    protected void notify(RESTRICTEDUSER_NOTIFICATION_OPERATION operation, RestrictedUser restrictedUser) throws Exception {
        notify(operation, restrictedUser, operation.getRecipients(getSecurityManager(), restrictedUser));
    }

    protected void notify(RESTRICTEDUSER_NOTIFICATION_OPERATION operation, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (restrictedUser != null && recipients != null) {
            Collection<RestrictedUserContentNotification> notifications = new HashSet<RestrictedUserContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new RestrictedUserContentNotification(operation, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNELLISTENER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }

    protected void notify(CHANNELLISTENER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelListenerLinkNotification> notifications = new HashSet<ChannelListenerLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == CHANNELLISTENER_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ChannelListenerLinkNotification(operation, channel, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
