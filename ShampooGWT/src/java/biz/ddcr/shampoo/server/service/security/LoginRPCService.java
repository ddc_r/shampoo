/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.security;

import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.form.user.LoginForm;
import biz.ddcr.shampoo.client.form.user.ProfileDetailForm;
import biz.ddcr.shampoo.client.form.user.ProfilePasswordForm;
import biz.ddcr.shampoo.client.form.user.ProfileRightForm;
import biz.ddcr.shampoo.client.serviceAsync.security.LoginRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class LoginRPCService extends GWTRPCLocalThreadService implements LoginRPCServiceInterface {

    private LoginFacadeServiceInterface loginFacadeService;

    public LoginFacadeServiceInterface getLoginFacadeService() {
        return loginFacadeService;
    }

    public void setLoginFacadeService(LoginFacadeServiceInterface loginFacadeService) {
        this.loginFacadeService = loginFacadeService;
    }

    @Override
    public String getCurrentlyAuthenticatedUserLogin() throws Exception {
        return loginFacadeService.getCurrentlyAuthenticatedUserLogin();
    }

    @Override
    public void logIn(LoginForm loginForm) throws Exception {
        loginFacadeService.logIn(loginForm);
    }

    @Override
    public void autoLogInFromCookie() throws Exception {
        loginFacadeService.logInFromCookie();
    }

    @Override
    public void logOut() throws Exception {
        loginFacadeService.logOut();
    }

    @Override
    public void updateCurrentlyAuthenticatedUser(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile) throws Exception {
        loginFacadeService.updateCurrentlyAuthenticatedUserPasswordAndProfile(newProfilePassword, newProfile);
    }
    @Override
    public void updateCurrentlyAuthenticatedUser(ProfilePasswordForm newProfilePassword) throws Exception {
        loginFacadeService.updateCurrentlyAuthenticatedUserPassword(newProfilePassword);
    }
    @Override
    public void updateCurrentlyAuthenticatedUser(ProfileDetailForm newProfile) throws Exception {
        loginFacadeService.updateCurrentlyAuthenticatedUserProfile(newProfile);
    }
    @Override
    public void updateCurrentlyAuthenticatedUserAndListenerRights(ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception {
        loginFacadeService.updateCurrentlyAuthenticatedUserPasswordAndProfile(newProfilePassword, newProfile, listenerRights);
    }
    @Override
    public void updateCurrentlyAuthenticatedUserAndListenerRights(ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception {
        loginFacadeService.updateCurrentlyAuthenticatedUserProfile(newProfile, listenerRights);
    }

    @Override
    public boolean isCurrentlyAuthenticatedUserAnAdministrator() throws Exception {
        return loginFacadeService.isCurrentlyAuthenticatedUserAnAdministrator();
    }

    @Override
    public ProfileRightForm getCurrentlyAuthenticatedRestrictedUserRights() throws Exception {
        return loginFacadeService.getCurrentlyAuthenticatedRestrictedUserRights();
    }

    @Override
    public ProfileDetailForm getCurrentlyAuthenticatedUserProfileDetails() throws Exception {
        return loginFacadeService.getCurrentlyAuthenticatedUserProfileDetails();
    }

    @Override
    public void setCurrentlyAuthenticatedUserVote(String songId, VOTE score) throws Exception {
        loginFacadeService.setCurrentlyAuthenticatedUserVote(songId, score);
    }
    @Override
    public VOTE getCurrentlyAuthenticatedUserVote(String songId) throws Exception {
        return loginFacadeService.getCurrentlyAuthenticatedUserVote(songId);
    }

    @Override
    public void setCurrentlyAuthenticatedUserRequest(String songId, String channelId, String message) throws Exception {
        loginFacadeService.setCurrentlyAuthenticatedUserRequest(songId, channelId, message);
    }
    @Override
    public void resetCurrentlyAuthenticatedUserRequest(String songId) throws Exception {
        loginFacadeService.resetCurrentlyAuthenticatedUserRequest(songId);
    }
    @Override
    public RequestModule getCurrentlyAuthenticatedUserRequest(String songId) throws Exception {
        return loginFacadeService.getCurrentlyAuthenticatedUserRequest(songId);
    }

    @Override
    public String getCurrentlyAuthenticatedUserTimezone() throws Exception {
        return loginFacadeService.getCurrentlyAuthenticatedUserTimezone();
    }

    @Override
    public void sendUserPasswordToMailboxViaUsername(String registeredUsername) throws Exception {
        loginFacadeService.sendUserPasswordToMailboxViaUsername(registeredUsername);
    }

    @Override
    public void sendUserPasswordToMailboxViaEmail(String registeredEmailAddress) throws Exception {
        loginFacadeService.sendUserPasswordToMailboxViaEmail(registeredEmailAddress);
    }

    @Override
    public void addSelfAuthenticatedUser(String registeredUsername, ProfilePasswordForm newProfilePassword, ProfileDetailForm newProfile, Collection<String> listenerRights) throws Exception {
        loginFacadeService.addSelfAuthenticatedUser(registeredUsername, newProfilePassword, newProfile, listenerRights);
    }

}
