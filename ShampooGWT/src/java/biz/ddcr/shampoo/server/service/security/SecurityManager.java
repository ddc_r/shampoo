/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.security;

import biz.ddcr.shampoo.client.form.journal.LogForm.LOG_TAGS;
import biz.ddcr.shampoo.client.form.notification.NotificationForm.NOTIFICATION_TAGS;
import biz.ddcr.shampoo.client.helper.errors.*;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.programme.ProgrammeDao;
import biz.ddcr.shampoo.server.dao.channel.ChannelDao;
import biz.ddcr.shampoo.server.dao.journal.JournalDao;
import biz.ddcr.shampoo.server.dao.notification.NotificationDao;
import biz.ddcr.shampoo.server.dao.track.TrackDao;
import biz.ddcr.shampoo.server.dao.user.UserDao;
import biz.ddcr.shampoo.server.domain.GenericPersistentEntityInterface;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.SCORE;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.helper.BasicMessageWrapper;
import biz.ddcr.shampoo.server.helper.FinalBoolean;
import biz.ddcr.shampoo.server.helper.MessagingOutServiceInterface;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.VersioningHelper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentMap;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import biz.ddcr.shampoo.server.helper.ServletUtils2;
import java.util.Iterator;
import org.apache.commons.logging.LogFactory;

/**
 *
 * Handle user authentication mechanism. This object is currently bound to the
 * current local thread: it depends on the one provided by the servlet
 * dispatcher called. Only one authenticated user is authorized for each local
 * thread.
 *
 * TODO: Storing hashed password in cookies is a very BAD idea, and good
 * practices such as
 * http://raibledesigns.com/rd/entry/appfuse_refactorings_part_iii_remember
 * should be implemented instead, but I'm lazy and I'm no friend with
 * Hibernate... TODO: implement concurrent login check
 *
 * @author okay_awright
 *
 */
public class SecurityManager extends GenericService implements SecurityManagerInterface {

    private interface BooleanUserVisitor extends UserVisitor {

        public boolean get();
    }

    private interface ChannelsUserVisitor extends UserVisitor {

        public Collection<Channel> get();
    }

    private interface ProgrammesUserVisitor extends UserVisitor {

        public Collection<Programme> get();
    }

    private interface StringsUserVisitor extends UserVisitor {

        public Collection<String> get();
    }
    private transient UserDao userDao;
    private transient ChannelDao channelDao;
    private transient ProgrammeDao programmeDao;
    private transient TrackDao trackDao;
    private transient JournalDao journalDao;
    private transient NotificationDao notificationDao;
    //Cookie and session-based attributes for user authentication
    private static final String AUTHENTICATED_USER_SESSION_KEY = "SHAMPOO.AUTHENTICATED.USER";
    //TODO: Caching non-immeutable data is really dirty and should not ideally be used
    /*private static final String AUTHENTICATED_USER_CACHEDLINKEDCHANNELS_SESSION_KEY = "SHAMPOO.AUTHENTICATED.USER.CACHE.LINKED_CHANNELS";
     private static final String AUTHENTICATED_USER_CACHEDLINKEDPROGRAMMES_SESSION_KEY = "SHAMPOO.AUTHENTICATED.USER.CACHE.LINKED_PROGRAMMES";**/
    private static final String AUTHENTICATED_USER_COOKIE_KEY = "SHAMPOO.AUTHENTICATED.USER";
    private static final String AUTHENTICATED_USERNAME_COOKIE_ATTRIBUTE = "SHAMPOO.AUTHENTICATED.USER.USERNAME";
    private static final String AUTHENTICATED_HASHED_USERPASSWORD_COOKIE_ATTRIBUTE = "SHAMPOO.AUTHENTICATED.USER.HASHED_PASSWORD";
    //Life expectancy of a cookie
    private int cookieMaxAge;
    //Life expectancy of an HTTP session
    private int sessionMaxAge;
    //Currently authenticated user related data, bound to the local thread: test variable, do not reactivate it in prouction of course
    //private UserSessionData userData;
    //System configuration
    private SystemConfigurationHelper systemConfigurationHelper;
    //Messaging service (e.g. mail out to users)
    private MessagingOutServiceInterface messagingService;
    //Whether channels and programmes should be cached in the authentication cookie for restricted users
    //CON: since it may be changed during a concurrent session and there's no way to know if they're still up-to-date
    //PRO: everything is blazingly faster since this constant is read by all entry points to facade methods
    private static final boolean CACHE_RIGHTS_IN_SESSION = false;

    private String templateNotificationShortFile;
    private String templateNotificationLongFile;
    private String templateSendPasswordShortFile;
    private String templateSendPasswordLongFile;

    @Override
    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public MessagingOutServiceInterface getMessagingService() {
        return messagingService;
    }

    public void setMessagingService(MessagingOutServiceInterface messagingService) {
        this.messagingService = messagingService;
    }

    public void setTemplateNotificationShortFile(String templateNotificationShortFile) {
        this.templateNotificationShortFile = templateNotificationShortFile;
    }

    public void setTemplateNotificationLongFile(String templateNotificationLongFile) {
        this.templateNotificationLongFile = templateNotificationLongFile;
    }

    public void setTemplateSendPasswordShortFile(String templateSendPasswordShortFile) {
        this.templateSendPasswordShortFile = templateSendPasswordShortFile;
    }

    public void setTemplateSendPasswordLongFile(String templateSendPasswordLongFile) {
        this.templateSendPasswordLongFile = templateSendPasswordLongFile;
    }

    protected static UserSessionData getUserData() {
        //try to use any pre-registered UserSessionData first
        /*if (userData != null) {
         return userData;
         } else {*/
        //Otherwise use the one that is currently bound to HttpServletRequest
        //getRequest() may occasionally return null when used within a seperate thread
        try {
            return getUserData(ServletUtils2.getLocalSession());
        } catch (NullPointerException e) {
            //The session has been invalidated and terminated
            LogFactory.getLog(SecurityManager.class).info("The session is already closed, the currently authenticated user is unavailable and cannot be saved");
        }
        return null;
        //}
    }

    protected static UserSessionData getUserData(HttpSession currentlyOpenedSession) {
        try {
            if (currentlyOpenedSession != null) { //Just checking the validity of an opened session is not enough, one must also check if a UserSessionData is available
                return (UserSessionData) currentlyOpenedSession.getAttribute(AUTHENTICATED_USER_SESSION_KEY);
            }
        } catch (IllegalStateException e) {
            //The session has been invalidated and terminated
            LogFactory.getLog(SecurityManager.class).info("The session is already closed, the currently authenticated user is unavailable and cannot be saved");
        }
        return null;
    }

    protected void setUserData(UserSessionData userData) {
        //this.userData = userData;
        //Automatically register it to the HttpServletRequest if one is available
        try {
            HttpSession currentlyOpenedSession = ServletUtils2.getLocalSession();
            if (currentlyOpenedSession == null && userData != null) {
                //No session yet? and no invalidation pending? then create a new one
                currentlyOpenedSession = ServletUtils2.getRequest().getSession(true);
                currentlyOpenedSession.setMaxInactiveInterval(getSessionMaxAge());
                ServletUtils2.setLocalSession(currentlyOpenedSession);
            }
            if (currentlyOpenedSession != null) //Just checking the validity of an opened session is not enough, one must also check if a UserSessionData is available
            {
                if (userData != null) {
                    currentlyOpenedSession.setAttribute(AUTHENTICATED_USER_SESSION_KEY, userData);
                    //Dperecated: not used anymore
                    /*try {
                     //Add the user from the list of currently active users
                     ServletUtils2.registerHTTPSession(getCurrentlyAuthenticatedUsernameFromUserData(userData), currentlyOpenedSession);
                     } catch (Exception ex) {
                     logger.error("Cannot link user from local sessions", ex);
                     }*/
                } else {
                    //FIX for PWC2789: setAttribute: Session already invalidated
                    try {
                        //Dperecated: not used anymore
                        /*try {
                         //Remove the user from the list of currently active users
                         ServletUtils2.unregisterHTTPSession(getCurrentlyAuthenticatedUsernameFromUserData(getUserData(currentlyOpenedSession)), currentlyOpenedSession);
                         } catch (Exception ex) {
                         logger.error("Cannot unlink user from local sessions", ex);
                         }*/
                        currentlyOpenedSession.removeAttribute(AUTHENTICATED_USER_SESSION_KEY);
                        ServletUtils2.resetLocalSession();
                        //Invalidate the whole session then
                        currentlyOpenedSession.invalidate();
                    } catch (IllegalStateException e) {
                        //Already invalidated, drop error silently
                    }
                }
            }
        } catch (NullPointerException e) {
            //The session has been invalidated and terminated
            logger.info("The session is already closed, the currently authenticated user is unavailable and cannot be saved");
        }
    }

    protected void setCookie(Cookie cookie) {
        //Add cookies to the response
        try {
            ServletUtils2.getResponse().addCookie(cookie);
        } catch (NullPointerException e) {
            //The session has been invalidated and terminated
            logger.info("Cannot set cookies, detached response");
        }
    }

    protected Cookie[] getCookies() {
        try {
            return ServletUtils2.getRequest().getCookies();
        } catch (NullPointerException e) {
            //The session has been invalidated and terminated
            logger.info("Cannot get cookies, detached request");
        }
        return null;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public ChannelDao getChannelDao() {
        return channelDao;
    }

    public void setChannelDao(ChannelDao channelDao) {
        this.channelDao = channelDao;
    }

    public ProgrammeDao getProgrammeDao() {
        return programmeDao;
    }

    public void setProgrammeDao(ProgrammeDao programmeDao) {
        this.programmeDao = programmeDao;
    }

    public TrackDao getTrackDao() {
        return trackDao;
    }

    public void setTrackDao(TrackDao trackDao) {
        this.trackDao = trackDao;
    }

    public JournalDao getJournalDao() {
        return journalDao;
    }

    public void setJournalDao(JournalDao journalDao) {
        this.journalDao = journalDao;
    }

    public NotificationDao getNotificationDao() {
        return notificationDao;
    }

    public void setNotificationDao(NotificationDao notificationDao) {
        this.notificationDao = notificationDao;
    }

    public int getCookieMaxAge() {
        return cookieMaxAge;
    }

    public void setCookieMaxAge(int cookieMaxAge) {
        this.cookieMaxAge = cookieMaxAge;
    }

    public int getSessionMaxAge() {
        return sessionMaxAge;
    }

    public void setSessionMaxAge(int sessionMaxAge) {
        this.sessionMaxAge = sessionMaxAge;
    }

    /**
     * Unrafels a cookie string containing some tagged attributes. Return a map
     * of ids and their associated values.
     *
     */
    protected Properties unwrapCookieAttributes(String cookieVal) {
        // check that the cookie value isn't null or zero-length
        if (cookieVal == null || cookieVal.length() == 0) {
            return null;
        }
        try {
            // unrafel the cookie value
            Properties cookieAttributes = new Properties();
            ByteArrayInputStream bais = new ByteArrayInputStream(Base64.decodeBase64(cookieVal.getBytes()));
            cookieAttributes.load(bais);
            return cookieAttributes;
        } catch (IOException ex) {
            logger.error("LoginManager().unwrapCookieAttributes()", ex);
            return null;
        }
    }

    /**
     * Unrafels a cookie string containing some tagged attributes. Return a map
     * of ids and their associated values.
     *
     */
    protected String wrapCookieAttributes(Properties cookieAttributes) {
        // check that the cookie value isn't null or zero-length
        if (cookieAttributes == null || cookieAttributes.isEmpty()) {
            return null;
        }
        try {
            // unrafel the cookie value
            ByteArrayOutputStream rawCookieString = new ByteArrayOutputStream();
            cookieAttributes.store(rawCookieString, null);
            return new String(Base64.encodeBase64(rawCookieString.toByteArray()));
        } catch (IOException ex) {
            logger.error("LoginManager().wrapCookieAttributes()", ex);
            return null;
        }
    }

    /**
     * TODO: setPath() should be different either it's a webservice or the
     * Javascript GUI
     *
     * @param cookie
     */
    protected void bindCookieToHTTPResponse(Cookie cookie) {
        //TODO: setDomain() too
        //TODO: adapt and customize setPath()
        if (ServletUtils2.getRequest() != null) {
            cookie.setPath(ServletUtils2.getRequest().getContextPath());
            //Add cookies to the response
            setCookie(cookie);
        }
    }

    /**
     * Store a cookie and save current authentication details
     *
     * @param username
     * @param password
     *
     */
    protected void bakeAutoLoginCookie(String username, String password, Boolean isAdmin) throws Exception {

        Properties cookieAttributes = new Properties();
        cookieAttributes.setProperty(AUTHENTICATED_USERNAME_COOKIE_ATTRIBUTE, username);
        cookieAttributes.setProperty(AUTHENTICATED_HASHED_USERPASSWORD_COOKIE_ATTRIBUTE, DigestUtils.shaHex(password));

        String cookieString = wrapCookieAttributes(cookieAttributes);
        Cookie cookie = new Cookie(AUTHENTICATED_USER_COOKIE_KEY, cookieString);
        //Tweak some properties, such as their lifetime
        if (getCookieMaxAge() > 0) {
            cookie.setMaxAge(getCookieMaxAge());
        }
        bindCookieToHTTPResponse(cookie);
    }

    /**
     * Unset cookies from the session
     *
     * @param username
     * @param password
     *
     */
    protected void eatAutoLoginCookie() throws Exception {
        Cookie cookie = new Cookie(AUTHENTICATED_USER_COOKIE_KEY, "");
        cookie.setMaxAge(0);
        bindCookieToHTTPResponse(cookie);
    }

    /**
     * Check if the currentHTTP session is not invalidated and is already been
     * open
     *
     * @return
     *
     */
    /*protected boolean isSessionOpen() {
     //getRequest() may occasionally return null when used within a seperate thread
     try {
     HttpSession currentlyOpenedSession = getRequest().getSession(false);
     if (currentlyOpenedSession != null) //Just checking the validity of an opened session is not enough, one must also check if a UserSessionData is available
     {
     return ((UserSessionData) getRequest().getSession().getAttribute(AUTHENTICATED_USER_SESSION_KEY) != null);
     }
     } catch (NullPointerException e) {
     //The session has been invalidated and terminated
     logger.info("The session is already closed, the currently authenticated user is unavailable and cannot be saved");
     }
     return false;
     }*/
    /**
     * Browse the session attributes corresponding to the currently
     * authenticated user Returns null if no user is found
     *
     * @return
     *
     */
    @Override
    public User getCurrentlyAuthenticatedUserFromSession() throws Exception {

        UserSessionData userSessionData = getUserData();
        try {
            if (userSessionData != null) {
                return CACHE_RIGHTS_IN_SESSION ? (User) userSessionData.getAuthenticatedUser() : userDao.getUser((String) userSessionData.getAuthenticatedUser());
            }
        } catch (ClassCastException ex) {
            //isCacheRightsInSession() has been switched
            logger.info("Switching user right cache model, resetting session");
        }
        return null;
    }

    public static String getCurrentlyAuthenticatedUsernameFromUserData(UserSessionData userSessionData) throws Exception {
        try {
            if (userSessionData != null) {
                if (CACHE_RIGHTS_IN_SESSION) {
                    User currentUser = (User) userSessionData.getAuthenticatedUser();
                    if (currentUser != null) {
                        return currentUser.getUsername();
                    }
                    return null;
                } else {
                    return (String) userSessionData.getAuthenticatedUser();
                }
            }
        } catch (ClassCastException e) {
            //isCacheRightsInSession() has been switched
            LogFactory.getLog(SecurityManager.class).debug("Switching user right cache model, resetting session");
        }
        return null;
    }

    public static String getCurrentlyAuthenticatedUsernameFromSession(HttpSession session) throws Exception {
        return getCurrentlyAuthenticatedUsernameFromUserData(getUserData(session));
    }

    /*protected Collection<String> getCurrentlyAuthenticatedUserCachedLinkedChannelsFromSession() throws Exception {
     return (Collection<String>) getRequest().getSession().getAttribute(AUTHENTICATED_USER_CACHEDLINKEDCHANNELS_SESSION_KEY);
     }

     protected Collection<String> getCurrentlyAuthenticatedUserCachedLinkedProgrammesFromSession() throws Exception {
     return (Collection<String>) getRequest().getSession().getAttribute(AUTHENTICATED_USER_CACHEDLINKEDPROGRAMMES_SESSION_KEY);
     }**/
    /**
     *
     * Fleshen a user entity from the database
     *
     *
     */
    protected RestrictedUser rehydrateRestrictedUserFromDatabase(RestrictedUser user) throws Exception {
        //Do only output a refreshed version of the user if it's not already in the Hibernate session
        if (user != null) {
            //Get a new version of the user from the database
            RestrictedUser rehydratedUser = userDao.getFullyHydratedRestrictedUser(user.getUsername());
            //Hibernate quirk: sometimes the rights are fully hydrated, sometimes not, I really can't see why: force their rehydration
            ProxiedClassUtil.hydrate(rehydratedUser.getChannelAdministratorRights());
            for (Channel r : rehydratedUser.getChannelAdministratorRights()) {
                ProxiedClassUtil.hydrate(r.getProgrammes());
            }
            ProxiedClassUtil.hydrate(rehydratedUser.getProgrammeManagerRights());
            for (Channel r : rehydratedUser.getProgrammeManagerRights()) {
                ProxiedClassUtil.hydrate(r.getProgrammes());
            }
            ProxiedClassUtil.hydrate(rehydratedUser.getListenerRights());
            for (Channel r : rehydratedUser.getListenerRights()) {
                ProxiedClassUtil.hydrate(r.getProgrammes());
            }
            ProxiedClassUtil.hydrate(rehydratedUser.getAnimatorRights());
            for (Programme p : rehydratedUser.getAnimatorRights()) {
                ProxiedClassUtil.hydrate(p.getChannels());
            }
            ProxiedClassUtil.hydrate(rehydratedUser.getContributorRights());
            for (Programme p : rehydratedUser.getContributorRights()) {
                ProxiedClassUtil.hydrate(p.getChannels());
            }
            ProxiedClassUtil.hydrate(rehydratedUser.getAnimatorRights());
            for (Programme p : rehydratedUser.getAnimatorRights()) {
                ProxiedClassUtil.hydrate(p.getChannels());
            }
            return rehydratedUser;
        } else {
            return user;
        }
    }

    /**
     *
     * Fleshen a user entity from the database
     *
     *
     */
    protected Administrator rehydrateAdministratorFromDatabase(Administrator user) throws Exception {
        //Do only output a refreshed version of the user if it's not already in the Hibernate session
        if (user != null) {
            //Get a new version of the user from the database
            return userDao.getAdministrator(user.getUsername());
        }
        return null;
    }

    /**
     * Store in the current session the provided authenticated user
     *
     * @return
     *
     */
    @Override
    public void setCurrentlyAuthenticatedUserFromSession(User authenticatedUser) throws Exception {
        if (CACHE_RIGHTS_IN_SESSION) {
            //Force casting and cache every rights if necessary
            UserVisitor visitor = new UserVisitor() {
                @Override
                public void visit(RestrictedUser authenticatedRestrictedUser) throws Exception {
                    setUserData(
                            new UserSessionData<RestrictedUser>(
                                    rehydrateRestrictedUserFromDatabase(authenticatedRestrictedUser)));
                }

                @Override
                public void visit(Administrator authenticatedUser) throws Exception {
                    setUserData(
                            new UserSessionData<Administrator>(
                                    authenticatedUser));
                }
            };
            authenticatedUser.acceptVisit(visitor);
        } else {
            setUserData(
                    new UserSessionData<String>(
                            authenticatedUser != null ? authenticatedUser.getUsername() : null));
        }

    }

    /**
     * Get additional metadata bound to the currently authenticated user session
     * For example, it is used to transparently store pointers to recently
     * uploaded files for later user
     *
     */
    @Override
    public ConcurrentMap<String, SessionMetadata> getCurrentlyAuthenticatedUserMetadataFromSession() {
        UserSessionData userSessionData = getUserData();
        return userSessionData != null
                ? userSessionData.getMetadata()
                : null;
    }

    @Override
    public SessionMetadata getCurrentlyAuthenticatedUserMetadataFromSession(String objectId) {
        UserSessionData userSessionData = getUserData();
        return userSessionData != null ? userSessionData.getMetadata(objectId) : null;
    }

    @Override
    public void addCurrentlyAuthenticatedUserMetadataFromSession(String objectId, SessionMetadata object) {
        UserSessionData userSessionData = getUserData();
        if (userSessionData != null) {
            userSessionData.addMetadata(objectId, object);
            //Update the session
            setUserData(
                    userSessionData);
        }
    }

    @Override
    public void removeCurrentlyAuthenticatedUserMetadataFromSession(String objectId) {
        UserSessionData userSessionData = getUserData();
        if (userSessionData != null) {
            userSessionData.removeMetadata(objectId);
            //Update the session
            setUserData(
                    userSessionData);
        }
    }

    /**
     * Clear the reference to the currently authenticated user in session, if
     * any
     *
     * @throws Exception
     *
     */
    protected void clearCurrentlyAuthenticatedUserFromSession() throws Exception {
        setUserData(null);
    }

    /**
     * Browse browser cookies for a valid username and password couple Returns
     * null if no cookie has been set or if invalid
     *
     * @return
     *
     */
    /*protected User getCurrentlyAuthenticatedUserFromAutoLoginCookie() throws Exception {

     String currentUsername = null;
     String currentHashedPassword = null;

     //Browse cookies and gather the required id
     Cookie[] cookies = null;
     try {
     cookies = getRequest().getCookies();
     } catch (NullPointerException e) {
     //The session has been invalidated and terminated
     logger.info("The session is already closed, no currently authenticated user is available");
     }
     if (cookies != null) {
     for (Cookie cookie : cookies) {
     String cookieName = cookie.getName();
     if (cookieName.equals(AUTHENTICATED_USER_COOKIE_KEY)) {
     Properties cookieAttributes = unwrapCookieAttributes(cookie.getValue());
     if (cookieAttributes != null) {
     currentUsername = cookieAttributes.getProperty(AUTHENTICATED_USERNAME_COOKIE_ATTRIBUTE);
     currentHashedPassword = cookieAttributes.getProperty(AUTHENTICATED_HASHED_USERPASSWORD_COOKIE_ATTRIBUTE);
     }
     break;
     }
     }
     }

     //Check both username and hashed password against database and see if credentials are valid
     if ((currentUsername != null && !currentUsername.length()==0) && (currentHashedPassword != null && !currentHashedPassword.length()==0)) {
     //Fetch the user specified
     User candidateUser = userDao.getUser(currentUsername);
     //log it
     internalLog(candidateUser, INTERNAL_OPERATION.read);
     //Hash its password and check if they match
     if (candidateUser != null && currentHashedPassword.equals(DigestUtils.shaHex(candidateUser.getPassword()))) {
     //Last check, is this account disabled in anyway?
     if (candidateUser.isEnabled() && candidateUser.isAccountNonExpired() && candidateUser.isAccountNonLocked() && candidateUser.isCredentialsNonExpired()) {
     //First, update the currently authenticatedUser (in case user never went through logIn() beforehand)
     setCurrentlyAuthenticatedUserFromSession(candidateUser);
     //Then return the authenticated user
     return candidateUser;
     }
     }
     }

     //There was no cookie or it's been forged
     return null;
     }**/
    /**
     * Fetch the currently authenticated user Throws an Exception if none is
     * logged in
     *
     * @return
     *
     */
    @Override
    public User getCurrentlyAuthenticatedUser() throws Exception {

        //First check if the session is still open and has already been initiated
        //Get the user in session, if any
        User authenticatedUser = getCurrentlyAuthenticatedUserFromSession();
        if (authenticatedUser != null) {
            return authenticatedUser;
        } else {
            //Then try to get it from a rememberme cookie
            /*authenticatedUser = getCurrentlyAuthenticatedUserFromAutoLoginCookie();
             if (authenticatedUser == null) //No user is logged in, access is then denied
             {**/
            //Try to automatically reconnect using cookies
            autoLogInFromCookie();
            return getCurrentlyAuthenticatedUserFromSession();
            /*} else {
             return authenticatedUser;
             }**/
        }
    }

    @Override
    public void autoLogInFromCookie() throws Exception {
        //Try to get the user from a rememberme cookie
        String currentUsername = null;
        String currentHashedPassword = null;

        //Browse cookies and gather the required id
        Cookie[] cookies = getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                String cookieName = cookie.getName();
                if (cookieName.equals(AUTHENTICATED_USER_COOKIE_KEY)) {
                    Properties cookieAttributes = unwrapCookieAttributes(cookie.getValue());
                    if (cookieAttributes != null) {
                        currentUsername = cookieAttributes.getProperty(AUTHENTICATED_USERNAME_COOKIE_ATTRIBUTE);
                        currentHashedPassword = cookieAttributes.getProperty(AUTHENTICATED_HASHED_USERPASSWORD_COOKIE_ATTRIBUTE);
                    }
                    break;
                }
            }
        }

        //Check both username and hashed password against database and see if credentials are valid
        if ((currentUsername != null && currentUsername.length() != 0) && (currentHashedPassword != null && currentHashedPassword.length() != 0)) {
            logIn(currentUsername, true, currentHashedPassword, true, true);
        } else {
            //Just to be on the safe side and if a previous cookie was stored but was invalid, clear it
            logOut();
            throw new SessionClosedException();
        }
    }

    /**
     * Remove any reference to an authenticated user, if any
     *
     * @throws Exception
     *
     */
    @Override
    public void logOut() throws Exception {
        clearCurrentlyAuthenticatedUserFromSession();
        eatAutoLoginCookie();
    }

    @Override
    public void logInCaseInsensitive(String username, String password, boolean rememberme) throws Exception {
        logIn(username, false, password, false, rememberme);
    }

    @Override
    public void logIn(String username, String password, boolean rememberme) throws Exception {
        logIn(username, true, password, false, rememberme);
    }

    /**
     * Try to authenticate the specify user using the specified credentials
     * Throws an Exception if anything wrong happens or if the user cannot login
     *
     */
    protected void logIn(String username, boolean isUsernameCaseSensitive, String password, boolean isPasswordHashed, boolean rememberme) throws Exception {

        //log out first
        logOut();

        User candidateUser = fetchRegisteredUser(username, isUsernameCaseSensitive, password, isPasswordHashed);

        if (candidateUser != null) {
            //Update the currently authenticatedUser
            setCurrentlyAuthenticatedUserFromSession(candidateUser);
            //Make it a cookie if required
            try {
                if (rememberme) {
                    bakeAutoLoginCookie(candidateUser.getUsername(), candidateUser.getPassword(), ProxiedClassUtil.castableAs(candidateUser, Administrator.class));
                }
            } catch (Exception e) {
                //Not baking a cookie is not a fatal error
                logger.error("LoginManager.logIn()", e);
            }
        } else {
            throw new AccessDeniedException("The specified user doesn't exist or is not allowed to log in");
        }

    }

    /**
     * Returns the User associated with the given username and password if he's
     * allowed to log in
     *
     * @param username
     * @param isUsernameCaseSensitive
     * @param password
     * @param isPasswordHashed
     * @return
     */
    @Override
    public User fetchRegisteredUser(String username, boolean isUsernameCaseSensitive, String password, boolean isPasswordHashed) {
        /*Check if someone else is already logged in either with or without the same credentials for this session (only one authenticated user by instance, each instance is bound to one local thread)
         if (getCurrentlyAuthenticatedUserFromSession() != null) {
         throw new ConcurrentLoginException();
         }*/
        //Check if the specified user exists
        User candidateUser = null;
        if (username == null || username.length() == 0 || (candidateUser = (isUsernameCaseSensitive ? userDao.getUser(username) : userDao.getUserByCaseInsensitiveId(username))) == null) {
            throw new UsernameNotFoundException();
        }

        //Compare passwords and see if user is allowed to login
        String challenge
                = isPasswordHashed
                        ? DigestUtils.shaHex(candidateUser.getPassword())
                        : candidateUser.getPassword();
        if (!challenge.equals(password)) {
            throw new BadCredentialsException();
        }

        //Last check, is this account disabled in anyway?
        if (!candidateUser.isEnabled()) {
            throw new UserDisabledException();
        }
        if (!candidateUser.isAccountNonExpired() || !candidateUser.isAccountNonLocked() || !candidateUser.isCredentialsNonExpired()) {
            throw new UserLockedException();
        }

        //Ok, user can login
        return candidateUser;
    }

    @Override
    public boolean updateCurrentlyAuthenticatedUserPassword(String oldPassword, String newPassword) throws Exception {
        boolean isUserSuccessfullyUpdated = false;
        if (oldPassword != null && newPassword != null) {
            User currentUser = getCurrentlyAuthenticatedUser();
            if (currentUser.getPassword() == null || currentUser.getPassword().equals(oldPassword)) {
                //Check if the old password matched with the current one otherwise refuse the update
                //Because it is sensible matter it's checked within the business layer
                if (currentUser.getPassword() == null || !currentUser.getPassword().equals(newPassword)) {
                    currentUser.setPassword(newPassword);
                    //Timestamp the edition
                    setEditionTimestamp(currentUser);
                    userDao.updateUser(currentUser);

                    //Update the currently authenticatedUser
                    setCurrentlyAuthenticatedUserFromSession(currentUser);

                    isUserSuccessfullyUpdated = true;
                }
            } else {
                throw new BadCredentialsException();
            }
        }
        return isUserSuccessfullyUpdated;
    }

    @Override
    public boolean updateCurrentlyAuthenticatedUserProfileData(String newEmail, final boolean emailNotification, String newFirstAndLastName, String newTimezone) throws Exception {
        final FinalBoolean isUpdated = new FinalBoolean();
        isUpdated.value = false;
        //All values are mandatory
        if (newEmail != null && newFirstAndLastName != null && newTimezone != null) {
            User currentUser = getCurrentlyAuthenticatedUser();
            if (currentUser.getEmail() == null || !currentUser.getEmail().equals(newEmail)
                    || currentUser.getFirstAndLastName() == null || !currentUser.getFirstAndLastName().equals(newFirstAndLastName)
                    || currentUser.getTimezone() == null || !currentUser.getTimezone().equals(newTimezone)) {
                currentUser.setEmail(newEmail);
                currentUser.setFirstAndLastName(newFirstAndLastName);
                currentUser.setTimezone(newTimezone);
                isUpdated.value = true;
            }
            UserVisitor visitor = new UserVisitor() {
                @Override
                public void visit(RestrictedUser user) throws Exception {
                    user.setEmailNotification(emailNotification);
                    isUpdated.value = true;
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    //Do nothing
                }
            };
            currentUser.acceptVisit(visitor);

            if (isUpdated.value) {
                //Timestamp the edition
                setEditionTimestamp(currentUser);
                userDao.updateUser(currentUser);

                //Update the currently authenticatedUser
                setCurrentlyAuthenticatedUserFromSession(currentUser);
            }
        }
        return isUpdated.value;
    }

    @Override
    public void setCurrentlyAuthenticatedUserVote(String songId, SCORE score) throws Exception {
        //Adminsitrator cannot vote
        if (songId != null) {

            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                throw new AdministratorsCannotVoteException();
            }

            //Try to find if a previous vote has already been saved
            Voting vote = trackDao.getUserVote(getCurrentlyAuthenticatedUser().getUsername(), songId);
            if (vote != null) {
                //Yep, there was one
                if (score == null) {
                    //Remove it
                    //ORM limit: everything must be rehydrated and sync-ed with the current session for a simple delete
                    /*RestrictedUser user = userDao.getRestrictedUser(getCurrentlyAuthenticatedUser().getUsername());
                     vote.removeSong();
                     user.removeVote(vote);*/
                    vote.drop();
                    trackDao.removeVote(vote);
                } else {
                    //Edit it
                    //Just modify its value
                    vote.setValue(score);
                    //Timestamp the edition
                    setEditionTimestamp(vote);
                    trackDao.updateVote(vote);
                }
            } else {
                if (score != null) {
                    //Add one
                    //ORM limit: everything must be rehydrated and sync-ed with the current session for a simple insert
                    RestrictedUser user = userDao.loadRestrictedUser(getCurrentlyAuthenticatedUser().getUsername());
                    BroadcastableSong song = (BroadcastableSong) trackDao.loadTrack(BroadcastableSong.class, songId);
                    if (user != null && song
                            != null) {
                        Voting newVote = new Voting(user, song, score);
                        //Timestamp the creation
                        setCreationTimestamp(newVote);
                        //user.addVote(vote);
                        trackDao.addVote(newVote);
                    }
                }
            }


            /*BroadcastableSong currentTrack = (BroadcastableSong) trackDao.getTrack(BroadcastableSong.class, songId);
             RestrictedUser currentUser = userDao.getRestrictedUserWithVotesHydrated(getCurrentlyAuthenticatedUserLogin());*/
            //TODO: iterating through a huge collection is not a solution, either use a Map implementation of getVotes() or use native SQL and manually synchronize the result with the session
            /*for (Iterator<Voting> i = currentUser.getVotes().iterator(); i.hasNext();) {
             Voting currentVote = i.next();
             //BUG? Javassist sometimes doesn't allow its getters to be resolved, equality tests cannot be performed because one in ten enunexpectedly fails
             if (currentVote.getSong().equals(currentTrack)) {
             //Drop the current vote, now outdated
             currentTrack.removeVote(currentVote);
             currentUser.removeVote(currentVote);
             //no two scores for a same combo of user and song
             break;
             }
             }
             if (score!=null) {
             if (currentTrack!=null) {
             //Store the new Vote, on both sides: user + track
             new Voting(currentUser, currentTrack, score);
             }
             }
             //log it
             //internalLog(currentUser, INTERNAL_OPERATION.edit);
             userDao.updateUser(currentUser);**/
            //trackDao.updateTrack(currentTrack);
            //Update the currently authenticatedUser
            //setCurrentlyAuthenticatedUserFromSession(currentUser);
        }
    }

    @Override
    public SCORE getCurrentlyAuthenticatedUserVote(String songId) throws Exception {
        //Adminsitrator cannot vote
        if (songId != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                throw new AdministratorsCannotVoteException();
            }
            //TODO: iterating through a huge collection is not a solution, either use a Map implementation of getVotes() or use native SQL and manually synchronize the result with the session
            //Finding tracks from a user could be potentially faster than browsing users from a track: a single user rarely rates a track
            //Hydrate the list of votes from a user
            /*for (Iterator<Voting> i = song.getVotes().iterator(); i.hasNext();) {
             Voting currentVote = i.next();
             //Cannot do an equals() test since most of the objects from the currentlyauthenticateduser and required for such a test (i.e. creator) have not been hydrated beforhand
             //Just rely on a plain database id equality test
             if (currentVote.getRestrictedUser().getUsername().equals((ProxiedClassUtil.cast(getCurrentlyAuthenticatedUser()).getUsername())) {
             return currentVote.getValue();
             }
             }**/
            //Should be a bit faster than browsing the whole collection of votes for a given user when lots of data are to be processed
            Voting vote = trackDao.getUserVote(getCurrentlyAuthenticatedUser().getUsername(), songId);
            if (vote != null) {
                return vote.getValue();
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public void setCurrentlyAuthenticatedUserRequest(String songId, String channelId, String message) throws Exception {
        //Adminsitrator cannot vote
        if (songId != null) {

            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                throw new AdministratorsCannotRequestException();
            }

            //Try to find if a previous request has already been saved
            Request request = trackDao.getUserRequest(getCurrentlyAuthenticatedUser().getUsername(), songId);
            if (request != null) {
                //Yep, there was one
                if (channelId == null) {
                    //Delete request
                    //ORM limit: everything must be rehydrated and sync-ed with the current session for a simple delete
                    /*RestrictedUser user = userDao.getRestrictedUser(getCurrentlyAuthenticatedUser().getUsername());
                     user.removeRequest(request);*/
                    request.removeStreamItem();
                    request.removeQueueItem();
                    //request.removeSong();
                    request.drop();
                    //remove it
                    trackDao.removeRequest(request);
                } else {
                    //Edit request
                    /*//Just modify its value; drop everything and re-link with new data
                     request.removeChannel();
                     request.removeStreamItem();
                     request.removeQueueItem();
                     request.addChannel(channelDao.getChannel(channelId));
                     request.setMessage(message);
                     //Timestamp the edition
                     setEditionTimestamp(request);
                     trackDao.updateRequest(request);*/
                    throw new RequestAlreadyScheduledException();
                }
            } else {
                if (songId != null && channelId != null) {
                    //Add one
                    //ORM limit: everything must be rehydrated and sync-ed with the current session for a simple insert
                    RestrictedUser user = userDao.getRestrictedUser(getCurrentlyAuthenticatedUser().getUsername());
                    BroadcastableSong song = (BroadcastableSong) trackDao.getTrack(BroadcastableSong.class, songId);
                    if (user != null && song
                            != null) {
                        Request newRequest = new Request(
                                user,
                                song,
                                channelDao.getChannel(channelId),
                                message);
                        //Timestamp the edition
                        setCreationTimestamp(newRequest);
                        //user.addRequest(newRequest);                        
                        trackDao.addRequest(newRequest);
                    }
                }
            }
        }
    }

    @Override
    public boolean updateCurrentlyAuthenticatedUserListenerRights(Collection<String> newChannels, Collection<String> deletedChannels) throws Exception {
        boolean result = false;
        if (newChannels != null && deletedChannels != null && !isCurrentlyAuthenticatedUserAnAdministrator() && (!deletedChannels.isEmpty() || !newChannels.isEmpty())) {

            //ORM limit: everything must be rehydrated and sync-ed with the current session for a simple delete
            RestrictedUser user = userDao.loadRestrictedUser(getCurrentlyAuthenticatedUser().getUsername());

            for (Iterator<String> i = deletedChannels.iterator(); i.hasNext();) {
                String oldCString = i.next();
                i.remove();
                Channel oldC = channelDao.loadChannel(oldCString);
                user.removeListenerRight(oldC);
            }
            for (String channelEntry : newChannels) {
                Channel newC = channelDao.loadChannel(channelEntry);
                user.addListenerRight(newC);
            }

            //Timestamp the edition
            setEditionTimestamp(user);
            userDao.updateUser(user);

            //Update the currently authenticated user sessionData
            setCurrentlyAuthenticatedUserFromSession(user);

            result = true;

        }
        return result;
    }

    @Override
    public Request getCurrentlyAuthenticatedUserRequest(String songId) throws Exception {
        //Adminsitrator cannot vote
        if (songId != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                throw new AdministratorsCannotRequestException();
            }
            //Should be a bit faster than browsing the whole collection of requests for a given user when lots of data are to be processed
            return trackDao.getUserRequest(getCurrentlyAuthenticatedUser().getUsername(), songId);
        }
        return null;
    }

    @Override
    public boolean isCurrentlyAuthenticatedUserAnAdministrator() throws Exception {
        User currentUser = getCurrentlyAuthenticatedUser();

        return ProxiedClassUtil.castableAs(currentUser, Administrator.class
        );
    }

    @Override
    public boolean isCurrentlyAuthenticatedUser(User userToCheck) throws Exception {
        return getCurrentlyAuthenticatedUser().equals(userToCheck);
    }

    @Override
    public boolean hasCurrentlyAuthenticatedRestrictedUserRights(final RightsBundle rightsBundle) throws Exception {
        User currentUser = getCurrentlyAuthenticatedUser();
        BooleanUserVisitor visitor = new BooleanUserVisitor() {
            private boolean result;

            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(RestrictedUser ru) throws Exception {
                result = false;
                if (rightsBundle.isAnimatorRights() && ru.isAnimator()) {
                    result = true;
                } else if (rightsBundle.isContributorRights() && ru.isContributor()) {
                    result = true;
                } else if (rightsBundle.isCuratorRights() && ru.isCurator()) {
                    result = true;
                } else if (rightsBundle.isListenerRights() && ru.isListener()) {
                    result = true;
                } else if (rightsBundle.isProgrammeManagerRights() && ru.isProgrammeManager()) {
                    result = true;
                } else if (rightsBundle.isChannelAdministratorRights() && ru.isChannelAdministrator()) {
                    result = true;
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                result = false;
            }
        };
        currentUser.acceptVisit(visitor);
        return visitor.get();

    }

    @Override
    public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(Channel channel, RightsBundle rightsBundle) throws Exception {
        Collection<Channel> channels = getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(rightsBundle);
        return (channels != null && channels.contains(channel));
    }

    public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(String channelId, RightsBundle rightsBundle) throws Exception {
        Collection<String> channels = getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle);
        return (channels != null && channels.contains(channelId));
    }

    @Override
    public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(Programme programme, RightsBundle rightsBundle) throws Exception {
        Collection<Programme> programmes = getLinkedProgrammesForCurrentlyAuthenticatedRestrictedUser(rightsBundle);
        return (programmes != null && programmes.contains(programme));
    }

    public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(String programmeId, RightsBundle rightsBundle) throws Exception {
        Collection<String> programmes = getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle);
        return (programmes != null && programmes.contains(programmeId));
    }

    /*public Collection<RestrictedUser> getRestrictedUsersWithRightsOverChannel(String channelId, RightsBundle rightsBundle) throws Exception {
     Collection<String> channelIds = new HashSet<String>();
     channelIds.add(channelId);
     return getRestrictedUsersWithRights(null, channelIds, rightsBundle);
     }

     public Collection<RestrictedUser> getRestrictedUsersWithRightsOverChannel(Channel channel, RightsBundle rightsBundle) throws Exception {
     Collection<String> channelIds = new HashSet<String>();
     if (channel != null) {
     channelIds.add(channel.getLabel());
     }
     return getRestrictedUsersWithRights(null, channelIds, rightsBundle);
     }

     public Collection<RestrictedUser> getRestrictedUsersWithRightsOverProgramme(String programmeId, RightsBundle rightsBundle) throws Exception {
     Collection<String> programmeIds = new HashSet<String>();
     programmeIds.add(programmeId);
     return getRestrictedUsersWithRights(programmeIds, null, rightsBundle);
     }

     public Collection<RestrictedUser> getRestrictedUsersWithRightsOverProgramme(Programme programme, RightsBundle rightsBundle) throws Exception {
     Collection<String> programmeIds = new HashSet<String>();
     if (programme != null) {
     programmeIds.add(programme.getLabel());
     }
     return getRestrictedUsersWithRights(programmeIds, null, rightsBundle);
     }*/
    @Override
    public Collection<RestrictedUser> getRestrictedUsersWithRightsOverChannelsOrProgrammes(
            Collection<Channel> channels,
            Collection<Programme> programmes,
            RightsBundle rightsBundle) throws Exception {
        //@deprecated
        /*Collection<String> programmeIds = new HashSet<String>();
         if (programmes != null) {
         for (Programme programme : programmes) {
         programmeIds.add(programme.getLabel());
         }
         }
         Collection<String> channelIds = new HashSet<String>();
         if (channels != null) {
         for (Channel channel : channels) {
         channelIds.add(channel.getLabel());
         }
         }
         return getRestrictedUsersWithRights(programmeIds, channelIds, rightsBundle);*/

        Collection<RestrictedUser> result = new HashSet<RestrictedUser>();
        if (channels != null) {
            for (Channel c : channels) {
                if (rightsBundle.isChannelAdministratorRights()) {
                    result.addAll(c.getChannelAdministratorRights());
                }
                if (rightsBundle.isProgrammeManagerRights()) {
                    result.addAll(c.getProgrammeManagerRights());
                }
                if (rightsBundle.isListenerRights()) {
                    result.addAll(c.getListenerRights());
                }
            }
        }
        if (programmes != null) {
            for (Programme p : programmes) {
                if (rightsBundle.isAnimatorRights()) {
                    result.addAll(p.getAnimatorRights());
                }
                if (rightsBundle.isContributorRights()) {
                    result.addAll(p.getContributorRights());
                }
                if (rightsBundle.isCuratorRights()) {
                    result.addAll(p.getCuratorRights());
                }
            }
        }

        return result;

    }

    /*public Collection<RestrictedUser> getRestrictedUsersWithRights(Collection<String> channelIds, Collection<String> programmeIds, RightsBundle rightsBundle) throws Exception {
     return userDao.getLinkedRestrictedUserIdsByProgrammesOrChannels(programmeIds, channelIds, rightsBundle);
     }*/
    @Override
    public Collection<Channel> getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(final RightsBundle rightsBundle) throws Exception {
        User currentUser = getCurrentlyAuthenticatedUser();
        ChannelsUserVisitor visitor = new ChannelsUserVisitor() {
            private Collection<Channel> channels;

            @Override
            public Collection<Channel> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(user, rightsBundle);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        currentUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<String> getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(final RightsBundle rightsBundle) throws Exception {
        User currentUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> channels;

            @Override
            public Collection<String> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(user, rightsBundle);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        currentUser.acceptVisit(visitor);
        return visitor.get();
    }

    private Collection<String> getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(RestrictedUser authenticatedUser, RightsBundle rightsBundle) throws Exception {
        HashSet<String> l = new HashSet<String>();
        if (rightsBundle.isChannelAdministratorRights()) {
            for (Channel r : authenticatedUser.getChannelAdministratorRights()) {
                l.add(r.getLabel());
            }
        }
        if (rightsBundle.isProgrammeManagerRights()) {
            for (Channel r : authenticatedUser.getProgrammeManagerRights()) {
                l.add(r.getLabel());
            }
        }
        if (rightsBundle.isListenerRights()) {
            for (Channel r : authenticatedUser.getListenerRights()) {
                l.add(r.getLabel());
            }
        }
        if (rightsBundle.isAnimatorRights()) {
            for (Programme p : authenticatedUser.getAnimatorRights()) {
                for (Channel r : p.getChannels()) {
                    l.add(r.getLabel());
                }
            }
        }
        if (rightsBundle.isContributorRights()) {
            for (Programme p : authenticatedUser.getContributorRights()) {
                for (Channel r : p.getChannels()) {
                    l.add(r.getLabel());
                }
            }
        }
        if (rightsBundle.isCuratorRights()) {
            for (Programme p : authenticatedUser.getCuratorRights()) {
                for (Channel r : p.getChannels()) {
                    l.add(r.getLabel());
                }
            }
        }
        return l;
    }

    private Collection<Channel> getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(RestrictedUser authenticatedUser, RightsBundle rightsBundle) throws Exception {
        HashSet<Channel> l = new HashSet<Channel>();
        if (rightsBundle.isChannelAdministratorRights()) {
            l.addAll(authenticatedUser.getChannelAdministratorRights());
        }
        if (rightsBundle.isProgrammeManagerRights()) {
            l.addAll(authenticatedUser.getProgrammeManagerRights());
        }
        if (rightsBundle.isListenerRights()) {
            l.addAll(authenticatedUser.getListenerRights());
        }
        if (rightsBundle.isAnimatorRights()) {
            for (Programme p : authenticatedUser.getAnimatorRights()) {
                l.addAll(p.getChannels());
            }
        }
        if (rightsBundle.isContributorRights()) {
            for (Programme p : authenticatedUser.getContributorRights()) {
                l.addAll(p.getChannels());
            }
        }
        if (rightsBundle.isCuratorRights()) {
            for (Programme p : authenticatedUser.getCuratorRights()) {
                l.addAll(p.getChannels());
            }
        }
        return l;
    }

    private Collection<String> getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(RestrictedUser authenticatedUser, RightsBundle rightsBundle) throws Exception {
        HashSet<String> l = new HashSet<String>();
        if (rightsBundle.isAnimatorRights()) {
            for (Programme p : authenticatedUser.getAnimatorRights()) {
                l.add(p.getLabel());
            }
        }
        if (rightsBundle.isContributorRights()) {
            for (Programme p : authenticatedUser.getContributorRights()) {
                l.add(p.getLabel());
            }
        }
        if (rightsBundle.isCuratorRights()) {
            for (Programme p : authenticatedUser.getCuratorRights()) {
                l.add(p.getLabel());
            }
        }
        if (rightsBundle.isChannelAdministratorRights()) {
            for (Channel r : authenticatedUser.getChannelAdministratorRights()) {
                for (Programme p : r.getProgrammes()) {
                    l.add(p.getLabel());
                }
            }
        }
        if (rightsBundle.isProgrammeManagerRights()) {
            for (Channel r : authenticatedUser.getProgrammeManagerRights()) {
                for (Programme p : r.getProgrammes()) {
                    l.add(p.getLabel());
                }
            }
        }
        if (rightsBundle.isListenerRights()) {
            for (Channel r : authenticatedUser.getListenerRights()) {
                for (Programme p : r.getProgrammes()) {
                    l.add(p.getLabel());
                }
            }
        }
        return l;
    }

    private Collection<Programme> getLinkedProgrammesForCurrentlyAuthenticatedRestrictedUser(RestrictedUser authenticatedUser, RightsBundle rightsBundle) throws Exception {
        HashSet<Programme> l = new HashSet<Programme>();
        if (rightsBundle.isAnimatorRights()) {
            l.addAll(authenticatedUser.getAnimatorRights());
        }
        if (rightsBundle.isContributorRights()) {
            l.addAll(authenticatedUser.getContributorRights());
        }
        if (rightsBundle.isCuratorRights()) {
            l.addAll(authenticatedUser.getCuratorRights());
        }
        if (rightsBundle.isChannelAdministratorRights()) {
            for (Channel r : authenticatedUser.getChannelAdministratorRights()) {
                l.addAll(r.getProgrammes());
            }
        }
        if (rightsBundle.isProgrammeManagerRights()) {
            for (Channel r : authenticatedUser.getProgrammeManagerRights()) {
                l.addAll(r.getProgrammes());
            }
        }
        if (rightsBundle.isListenerRights()) {
            for (Channel r : authenticatedUser.getListenerRights()) {
                l.addAll(r.getProgrammes());
            }
        }
        return l;
    }

    @Override
    public Collection<String> getCurrentlyAuthenticatedRestrictedUserChannelAdministratorRightIds() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> channels;

            @Override
            public Collection<String> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = new HashSet<String>();
                for (Channel r : user.getChannelAdministratorRights()) {
                    channels.add(r.getLabel());
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Channel> getCurrentlyAuthenticatedRestrictedUserChannelAdministratorRights() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        ChannelsUserVisitor visitor = new ChannelsUserVisitor() {
            private Collection<Channel> channels;

            @Override
            public Collection<Channel> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = new HashSet<Channel>();
                for (Channel r : user.getChannelAdministratorRights()) {
                    channels.add(r);
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<String> getCurrentlyAuthenticatedRestrictedUserProgrammeManagerRightIds() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> channels;

            @Override
            public Collection<String> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = new HashSet<String>();
                for (Channel r : user.getProgrammeManagerRights()) {
                    channels.add(r.getLabel());
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Channel> getCurrentlyAuthenticatedRestrictedUserProgrammeManagerRights() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        ChannelsUserVisitor visitor = new ChannelsUserVisitor() {
            private Collection<Channel> channels;

            @Override
            public Collection<Channel> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = new HashSet<Channel>();
                for (Channel r : user.getProgrammeManagerRights()) {
                    channels.add(r);
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<String> getCurrentlyAuthenticatedRestrictedUserListenerRightIds() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> channels;

            @Override
            public Collection<String> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = new HashSet<String>();
                for (Channel r : user.getListenerRights()) {
                    channels.add(r.getLabel());
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Channel> getCurrentlyAuthenticatedRestrictedUserListenerRights() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        ChannelsUserVisitor visitor = new ChannelsUserVisitor() {
            private Collection<Channel> channels;

            @Override
            public Collection<Channel> get() {
                return channels;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                channels = new HashSet<Channel>();
                for (Channel r : user.getListenerRights()) {
                    channels.add(r);
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                channels = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<String> getCurrentlyAuthenticatedRestrictedUserAnimatorRightIds() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> programmes;

            @Override
            public Collection<String> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = new HashSet<String>();
                for (Programme p : user.getAnimatorRights()) {
                    programmes.add(p.getLabel());
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Programme> getCurrentlyAuthenticatedRestrictedUserAnimatorRights() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        ProgrammesUserVisitor visitor = new ProgrammesUserVisitor() {
            private Collection<Programme> programmes;

            @Override
            public Collection<Programme> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = new HashSet<Programme>();
                for (Programme p : user.getAnimatorRights()) {
                    programmes.add(p);
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<String> getCurrentlyAuthenticatedRestrictedUserCuratorRightIds() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> programmes;

            @Override
            public Collection<String> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = new HashSet<String>();
                for (Programme p : user.getCuratorRights()) {
                    programmes.add(p.getLabel());
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Programme> getCurrentlyAuthenticatedRestrictedUserCuratorRights() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        ProgrammesUserVisitor visitor = new ProgrammesUserVisitor() {
            private Collection<Programme> programmes;

            @Override
            public Collection<Programme> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = new HashSet<Programme>();
                for (Programme p : user.getCuratorRights()) {
                    programmes.add(p);
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<String> getCurrentlyAuthenticatedRestrictedUserContributorRightIds() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> programmes;

            @Override
            public Collection<String> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = new HashSet<String>();
                for (Programme p : user.getContributorRights()) {
                    programmes.add(p.getLabel());
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Programme> getCurrentlyAuthenticatedRestrictedUserContributorRights() throws Exception {
        User authenticatedUser = getCurrentlyAuthenticatedUser();
        ProgrammesUserVisitor visitor = new ProgrammesUserVisitor() {
            private Collection<Programme> programmes;

            @Override
            public Collection<Programme> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = new HashSet<Programme>();
                for (Programme p : user.getContributorRights()) {
                    programmes.add(p);
                }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        authenticatedUser.acceptVisit(visitor);
        return visitor.get();
    }


    /*private Collection<String> setAllLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(RestrictedUser authenticatedUser) throws Exception {
     return getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(authenticatedUser, new RightsBundle(true));
     }

     private Collection<String> setAllLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(RestrictedUser authenticatedUser) throws Exception {
     return getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(authenticatedUser, new RightsBundle(true));
     }**/
    @Override
    public Collection<String> getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(final RightsBundle rightsBundle) throws Exception {
        User currentUser = getCurrentlyAuthenticatedUser();
        StringsUserVisitor visitor = new StringsUserVisitor() {
            private Collection<String> programmes;

            @Override
            public Collection<String> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(user, rightsBundle);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        currentUser.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public Collection<Programme> getLinkedProgrammesForCurrentlyAuthenticatedRestrictedUser(final RightsBundle rightsBundle) throws Exception {
        User currentUser = getCurrentlyAuthenticatedUser();
        ProgrammesUserVisitor visitor = new ProgrammesUserVisitor() {
            private Collection<Programme> programmes;

            @Override
            public Collection<Programme> get() {
                return programmes;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                programmes = getLinkedProgrammesForCurrentlyAuthenticatedRestrictedUser(user, rightsBundle);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                programmes = null;
            }
        };
        currentUser.acceptVisit(visitor);
        return visitor.get();
    }

    /**
     * @deprecated @return
     */
    private String getStackTraceInfo() {
        //4 means the fourth most recent stack, i.e. not this current call, not the call that called this call, but the one before: it should be the method that called addLog()
        try {
            StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[5];
            String methodName = stackTraceElement.getMethodName();
            return methodName;
        } catch (ArrayIndexOutOfBoundsException e) {
            //The stack was shortened by the host JVM for some obscure reasons
            //Don't do anything
        } catch (NullPointerException e) {
            //The stack was shortened by the host JVM for some obscure reasons
            //Don't do anything
        }
        return null;
    }

    protected void setCreationTimestamp(GenericPersistentEntityInterface entity, User creator) throws Exception {
        //Update the creator and editor entity properties?
        //Specify who touched this object last
        if (entity != null) {
            entity.setCreator(creator);
            //The entity creation timestamp is the responsibility of each domain entity
        }
        setEditionTimestamp(entity, creator);
    }

    protected void setCreationTimestamp(Collection<? extends GenericPersistentEntityInterface> entities, User creator) throws Exception {
        if (entities != null) {
            for (GenericPersistentEntityInterface entity : entities) {
                setCreationTimestamp(entity, creator);
            }
        }

    }

    protected void setEditionTimestamp(GenericPersistentEntityInterface entity, User editor) throws Exception {
        //Update the creator and editor entity properties?
        //Specify who touched this object last
        if (entity != null) {
            entity.setLatestEditor(editor);
            entity.setLatestModificationDate(new Timestamp(System.currentTimeMillis()));
        }
    }

    protected void setEditionTimestamp(Collection<? extends GenericPersistentEntityInterface> entities, User creator) throws Exception {
        if (entities != null) {
            for (GenericPersistentEntityInterface entity : entities) {
                setEditionTimestamp(entity, creator);
            }
        }

    }

    @Override
    public void setCreationTimestamp(GenericPersistentEntityInterface entity) throws Exception {
        setCreationTimestamp(entity, getCurrentlyAuthenticatedUser());
    }

    @Override
    public void setCreationTimestamp(GenericPersistentEntityInterface entity, String forcedAuthenticatedUserLogin) throws Exception {
        setCreationTimestamp(entity, userDao.getUser(forcedAuthenticatedUserLogin));
    }

    @Override
    public void setCreationTimestamp(Collection<? extends GenericPersistentEntityInterface> entities) throws Exception {
        setCreationTimestamp(entities, getCurrentlyAuthenticatedUser());
    }

    @Override
    public void setCreationTimestamp(Collection<? extends GenericPersistentEntityInterface> entities, String forcedAuthenticatedUserLogin) throws Exception {
        setCreationTimestamp(entities, userDao.getUser(forcedAuthenticatedUserLogin));
    }

    @Override
    public void setEditionTimestamp(GenericPersistentEntityInterface entity) throws Exception {
        setEditionTimestamp(entity, getCurrentlyAuthenticatedUser());
    }

    @Override
    public void setEditionTimestamp(GenericPersistentEntityInterface entity, String forcedAuthenticatedUserLogin) throws Exception {
        setEditionTimestamp(entity, userDao.getUser(forcedAuthenticatedUserLogin));
    }

    @Override
    public void setEditionTimestamp(Collection<? extends GenericPersistentEntityInterface> entities) throws Exception {
        setEditionTimestamp(entities, getCurrentlyAuthenticatedUser());
    }

    @Override
    public void setEditionTimestamp(Collection<? extends GenericPersistentEntityInterface> entities, String forcedAuthenticatedUserLogin) throws Exception {
        setEditionTimestamp(entities, userDao.getUser(forcedAuthenticatedUserLogin));
    }

    @Override
    public void addJournal(Log log) throws Exception {
        if (log != null && log.getAction() != null && log.getAction().canLog(getSystemConfigurationHelper())) {

            //Fill in missing data in log objects
            try {
                log.setActorID(getCurrentlyAuthenticatedUser() != null ? getCurrentlyAuthenticatedUser().getUsername() : null);
            } catch (SessionClosedException e) {
                //Just to be sure that exceptions coming from streamingmodules for example are properly logged in the end
                log.setActorID(null);
                logger.info("Logging with no authenticated user, no active session");
            }
            log.setDateStamp(new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), /**
                     * do not enforce timezone*
                     */
                    null));

            //Store log in database
            try {
                journalDao.addLog(log);
            } catch (Exception e) {
                logger.error("Logging disabled", e);
            } finally {
                logger.debug("Log:" + getStackTraceInfo());
            }
        }
    }

    @Override
    public void addJournals(Collection<? extends Log> logs) throws Exception {
        if (logs != null && !logs.isEmpty()) {

            //Fill in missing data in log objects
            String actorId = null;
            try {
                actorId = getCurrentlyAuthenticatedUser() != null ? getCurrentlyAuthenticatedUser().getUsername() : null;
            } catch (Exception e) {
                //Just to be sure that exceptions coming from streamingmodules for example are properly logged in the end
                //Silently drop the exception
                logger.error("Logging with no authenticated user", e);
            }
            YearMonthWeekDayHourMinuteSecondMillisecond dateStamp = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), /**
                     * do not enforce timezone*
                     */
                    null);
            Collection<Log> loggableLogs = new HashSet<Log>();
            for (Log log : logs) {
                if (log.getAction() != null && log.getAction().canLog(getSystemConfigurationHelper())) {
                    log.setActorID(actorId);
                    log.setDateStamp(dateStamp);
                    loggableLogs.add(log);
                }
            }

            if (!loggableLogs.isEmpty()) {
                //Store logs in database
                try {
                    journalDao.addBatchLog(loggableLogs);
                } catch (Exception e) {
                    logger.error("Logging disabled", e);
                } finally {
                    logger.debug("Log:" + getStackTraceInfo());
                }
            }

        }
    }

    @Override
    public Log loadJournal(String id) throws Exception {
        Log log = journalDao.loadLog(id);
        if (log == null) {
            throw new NoEntityException();
        }
        return log;
    }

    @Override
    public Log getJournal(String id) throws Exception {
        Log log = journalDao.getLog(id);
        if (log == null) {
            throw new NoEntityException();
        }
        return log;
    }

    @Override
    public Collection<Log> getJournals(Collection<String> ids) throws Exception {
        Collection<Log> logs = journalDao.getLogs(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (logs == null || ids.size() != logs.size()) {
            throw new NoEntityException();
        }
        return logs;
    }

    @Override
    public void deleteJournal(Log log) throws Exception {
        journalDao.deleteLog(log);
    }

    @Override
    public void deleteJournals(Collection<Log> logs) throws Exception {
        journalDao.deleteBatchLogs(logs);
    }

    @Override
    public void purgeJournals() throws Exception {
        if (getSystemConfigurationHelper().getLogMaxAge() > 0) {

            //Compute the expiration date from now
            YearMonthWeekDayHourMinuteSecondMillisecond expirationDate = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), Log.getFixedTimezone());
            expirationDate.addDays(-getSystemConfigurationHelper().getLogMaxAge());

            //EJB3 limitation: cannot use purgeLogs() anymore because of foreign key constraints not respected: a Delete HQL statement does not handle foreign keys
            //and a many-to-many directive cannot use on-delete="cascade" so it's a no-go, unless you're willing to rewrite the many-to-many into many-to-ones
            //As a result, the request is abysmally slow
            //journalDao.purgeJournals(expirationDate);
            Collection<Log> logsToPurge = journalDao.getAllLogsAfterExpiration(expirationDate);
            if (logsToPurge != null) {
                for (Log logToPurge : logsToPurge) {
                    logToPurge.clearChannels();
                }
            }
            journalDao.deleteBatchLogs(logsToPurge);
        }
    }

    @Override
    public Collection<Log> getFilteredJournals(GroupAndSort<LOG_TAGS> constraint) throws Exception {
        return getFilteredJournalsForChannelId(constraint, null);
    }

    @Override
    public Collection<Log> getFilteredJournalsForChannelId(GroupAndSort<LOG_TAGS> constraint, String channelId) throws Exception {
        Collection<Log> logs;
        if (isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (channelId == null) {
                logs = journalDao.getAllLogs(constraint);
            } else {
                logs = journalDao.getLogsForChannel(constraint, channelId);
            }
        } else {

            //Gather all channels visible by the authenticated user
            RightsBundle rightsBundle = Log.getViewRights();

            if (channelId == null) {
                logs = journalDao.getLogsWithRightsByChannels(
                        getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint);
            } else {
                logs = journalDao.getLogsWithRightsByChannelsForChannel(
                        getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                        constraint,
                        channelId);
            }
        }
        return logs;
    }

    protected boolean checkAccessLogChannels(Log logToCheck, RightsBundle rightsBundle) throws Exception {
        if (logToCheck != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //Check if any programmes or channels are found in common between the currently authenticated user and the log
                Set<Channel> c = logToCheck.getChannels();
                c.retainAll(getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(rightsBundle));
                if (!c.isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewJournals() throws Exception {
        if (isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = Log.getViewRights();

            return hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessViewJournal(String logToCheckId) throws Exception {
        return checkAccessViewJournal(getJournal(logToCheckId));
    }

    @Override
    public boolean checkAccessViewJournal(Log logToCheck) throws Exception {
        if (logToCheck != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {

                RightsBundle rightsBundle = Log.getViewRights();

                return checkAccessLogChannels(logToCheck, rightsBundle);
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteJournals() throws Exception {
        if (isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = Log.getDeleteRights();

            return hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessDeleteJournal(String logToCheckId) throws Exception {
        return checkAccessDeleteJournal(getJournal(logToCheckId));
    }

    @Override
    public boolean checkAccessDeleteJournal(Log logToCheck) throws Exception {
        if (logToCheck != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {

                RightsBundle rightsBundle = Log.getDeleteRights();

                return checkAccessLogChannels(logToCheck, rightsBundle);
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessPrivateResource(String privateKey) throws Exception {
        //If the streamer has no enforced key (which is bad, but whatever) then automatically grant access
        if (getSystemConfigurationHelper().getPrivateKey() == null || getSystemConfigurationHelper().getPrivateKey().length() == 0) {
            return true;
        }
        //otherwise if the provided key matches the one from the streamer
        return (getSystemConfigurationHelper().getPrivateKey().equals(privateKey));
    }

    @Override
    public void addNotification(Notification notification) throws Exception {
        if (notification != null && notification.getAction() != null && notification.getAction().canNotify(getSystemConfigurationHelper())) {

            //Fill in missing data in notification objects
            try {
                notification.setActorID(getCurrentlyAuthenticatedUser() != null ? getCurrentlyAuthenticatedUser().getUsername() : null);
            } catch (SessionClosedException e) {
                notification.setActorID(null);
                logger.info("Notifying with no authenticated user, no active session");
            }

            notification.setDateStamp(new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), /**
                     * do not enforce timezone*
                     */
                    null));

            //Store notification in database
            try {
                notificationDao.addNotification(notification);
            } catch (Exception e) {
                logger.error("Notifications disabled", e);
            } finally {
                logger.debug("Notification:" + getStackTraceInfo());
            }
        }
    }

    @Override
    public void addNotifications(Collection<? extends Notification> notifications) throws Exception {
        if (notifications != null && !notifications.isEmpty()) {

            //Fill in missing data in notification objects
            String actorId = null;
            try {
                actorId = getCurrentlyAuthenticatedUser() != null ? getCurrentlyAuthenticatedUser().getUsername() : null;
            } catch (SessionClosedException e) {
                //Silently fails, it doesn't matter that much
                logger.info("Notifying with no authenticated user, no active session");
            }
            YearMonthWeekDayHourMinuteSecondMillisecond dateStamp = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), /**
                     * do not enforce timezone*
                     */
                    null);
            Collection<Notification> validNotifications = new HashSet<Notification>();
            for (Notification notification : notifications) {
                if (notification.getAction() != null && notification.getAction().canNotify(getSystemConfigurationHelper())) {
                    notification.setActorID(actorId);
                    notification.setDateStamp(dateStamp);
                    validNotifications.add(notification);
                }
            }

            if (!validNotifications.isEmpty()) {
                //Store notifications in database
                try {
                    notificationDao.addBatchNotification(validNotifications);
                } catch (Exception e) {
                    logger.error("Notifications disabled", e);
                } finally {
                    logger.debug("Notification:" + getStackTraceInfo());
                }
            }

        }
    }

    @Override
    public Notification loadNotification(String id) throws Exception {
        Notification notification = notificationDao.loadNotification(id);
        if (notification == null) {
            throw new NoEntityException();
        }
        return notification;
    }

    @Override
    public Notification getNotification(String id) throws Exception {
        Notification notification = notificationDao.getNotification(id);
        if (notification == null) {
            throw new NoEntityException();
        }
        return notification;
    }

    @Override
    public Collection<Notification> getNotifications(Collection<String> ids) throws Exception {
        Collection<Notification> notifications = notificationDao.getNotifications(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (notifications == null || ids.size() != notifications.size()) {
            throw new NoEntityException();
        }
        return notifications;
    }

    @Override
    public void deleteNotification(Notification notification) throws Exception {
        notificationDao.deleteNotification(notification);
    }

    @Override
    public void deleteNotifications(Collection<Notification> notifications) throws Exception {
        notificationDao.deleteBatchNotifications(notifications);
    }

    @Override
    public void purgeNotifications() throws Exception {
        if (getSystemConfigurationHelper().getNotificationMaxAge() > 0) {

            //Compute the expiration date from now
            YearMonthWeekDayHourMinuteSecondMillisecond expirationDate = new YearMonthWeekDayHourMinuteSecondMillisecond(System.currentTimeMillis(), Notification.getFixedTimezone());
            expirationDate.addDays(-getSystemConfigurationHelper().getNotificationMaxAge());

            notificationDao.purgeNotifications(expirationDate);
        }
    }

    @Override
    public void sendNotifications() throws Exception {

        //Don't send notifications if no mail server is configured
        if (getMessagingService().isEnabled()) {

            //Retrieve all notifications that need to be sent out to users who accepted to receive notifications
            //Cluster those notifications by user and make a mail per user with all the corresponding notifications
            //Finally send all the mails out
            //Update the sent flag in the database
            Collection<Notification> allUnseenNotifications = notificationDao.getAllUnseenNotificationsForOptedRestrictedUsersOnly();

            if (allUnseenNotifications != null && !allUnseenNotifications.isEmpty()) {

                try {
                    TreeMap<RestrictedUser, Collection<Notification>> friendlyNotificationsByUsers = new TreeMap<RestrictedUser, Collection<Notification>>();
                    for (Notification unseenNotification : allUnseenNotifications) {

                        //Check that the corresponding user  accepts to recieve notifications
                        if (unseenNotification.getRecipient().isEmailNotification() && !unseenNotification.isRecipientNotified()) {

                            Collection<Notification> friendlyNotifications = friendlyNotificationsByUsers.get(unseenNotification.getRecipient());
                            if (friendlyNotifications == null) {
                                //Use a SortedSet if you want your notifications to be sorted out
                                friendlyNotifications = new TreeSet<Notification>();
                            }
                            //Notifications should be naturally sorted by date first
                            friendlyNotifications.add(
                                    unseenNotification);
                            friendlyNotificationsByUsers.put(
                                    unseenNotification.getRecipient(),
                                    friendlyNotifications);
                        }

                    }

                    if (!friendlyNotificationsByUsers.isEmpty()) {

                        Collection<BasicMessageWrapper> mails = new HashSet<BasicMessageWrapper>();
                        for (Entry<RestrictedUser, Collection<Notification>> friendlyNotificationsByUser : friendlyNotificationsByUsers.entrySet()) {

                            Map templateVariables = new HashMap();
                            templateVariables.put("user", friendlyNotificationsByUser.getKey());
                            templateVariables.put("url", getSystemConfigurationHelper().getHostAndDeploymentGWTURL() + getSystemConfigurationHelper().getGWTContextPath());
                            templateVariables.put("notifications", friendlyNotificationsByUser.getValue());
                            templateVariables.put("staticAppName", VersioningHelper.getStaticAppName());
                            BasicMessageWrapper mail = new BasicMessageWrapper(
                                    templateNotificationShortFile,
                                    templateNotificationLongFile,
                                    templateVariables,
                                    friendlyNotificationsByUser.getKey());
                            mails.add(mail);
                        }

                        getMessagingService().send(mails);
                    }

                } finally {
                    //update all seen flags
                    //Even if an exception occurred
                    notificationDao.updateAllNotificationUnseenFlags();
                }

            }
        }

    }

    @Override
    public void sendPasswordToEmail(User user) throws Exception {

        if (user != null) {
            Map templateVariables = new HashMap();
            templateVariables.put("user", user);
            templateVariables.put("url", getSystemConfigurationHelper().getHostAndDeploymentGWTURL() + getSystemConfigurationHelper().getGWTContextPath());
            templateVariables.put("staticAppName", VersioningHelper.getStaticAppName());
            BasicMessageWrapper mail = new BasicMessageWrapper(
                    templateSendPasswordShortFile,
                    templateSendPasswordLongFile,
                    templateVariables,
                    user);
            getMessagingService().send(mail);
        }

    }

    @Override
    public Collection<Notification> getFilteredNotifications(GroupAndSort<NOTIFICATION_TAGS> constraint) throws Exception {
        if (isCurrentlyAuthenticatedUserAnAdministrator()) {
            return notificationDao.getAllNotifications(constraint);
        } else {
            return notificationDao.getNotificationsForRestrictedUser(getCurrentlyAuthenticatedUser().getUsername(), constraint);
        }
    }

    @Override
    public boolean checkAccessViewNotifications() throws Exception {
        /*if (isCurrentlyAuthenticatedUserAnAdministrator()) {
         //An administrator can do about everything: don't go any further, allow everything
         return true;
         } else {
         //At least one request must return true to grant access
         RightsBundle bundle = Notification.getViewRights();

         return hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
         }*/
        //Simplified to the max, everybody can see notifications, whether they're bound to programmes or channels
        return true;
    }

    @Override
    public boolean checkAccessViewNotification(String notificationToCheckId) throws Exception {
        return checkAccessViewNotification(getNotification(notificationToCheckId));
    }

    @Override
    public boolean checkAccessViewNotification(Notification notificationToCheck) throws Exception {
        if (notificationToCheck != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {

                /*RightsBundle rightsBundle = Notification.getViewRights();

                 //A user can only access his own notifications, whatever his credentials are
                 return (getCurrentlyAuthenticatedUser().equals(notificationToCheck.getRecipient()) && hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle));*/
                //A user can only access his own notifications, whatever his credentials are
                return getCurrentlyAuthenticatedUser().equals(notificationToCheck.getRecipient());
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteNotifications() throws Exception {
        /*if (isCurrentlyAuthenticatedUserAnAdministrator()) {
         //An administrator can do about everything: don't go any further, allow everything
         return true;
         } else {
         //At least one request must return true to grant access
         RightsBundle bundle = Notification.getDeleteRights();

         return hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
         }*/
        //Simplified to the max, everybody can delete his own notifications, whether they're bound to programmes or channels
        return true;
    }

    @Override
    public boolean checkAccessDeleteNotification(String notificationToCheckId) throws Exception {
        return checkAccessDeleteNotification(getNotification(notificationToCheckId));
    }

    @Override
    public boolean checkAccessDeleteNotification(Notification notificationToCheck) throws Exception {
        if (notificationToCheck != null) {
            if (isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {

                /*RightsBundle rightsBundle = Notification.getDeleteRights();

                 //A user can only access his own notifications, whatever his credentials are
                 return (getCurrentlyAuthenticatedUser().equals(notificationToCheck.getRecipient()) && hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle));*/
                //A user can only access his own notifications, whatever his credentials are
                return getCurrentlyAuthenticatedUser().equals(notificationToCheck.getRecipient());
            }
        }
        return false;
    }
}
