/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.security;

import biz.ddcr.shampoo.client.form.journal.LogForm.LOG_TAGS;
import biz.ddcr.shampoo.client.form.notification.NotificationForm.NOTIFICATION_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.GenericPersistentEntityInterface;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.journal.Log;
import biz.ddcr.shampoo.server.domain.notification.Notification;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.SCORE;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.helper.StaticSessionPoolInterface;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface.SessionMetadata;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author okay_awright
 **/
public interface SecurityManagerInterface extends StaticSessionPoolInterface {

    public interface SessionMetadata extends Serializable {

        public void cleanUp();
    }

    public class UserSessionData<T> implements Serializable {

        private T authenticatedUser;
        /**
         * Misc data bound to the current session for an authenticated user
         **/
        private ConcurrentMap<String, SessionMetadata> metadata;

        public UserSessionData(T authenticatedUser) {
            this.authenticatedUser = authenticatedUser;
        }

        public T getAuthenticatedUser() {
            return authenticatedUser;
        }

        public void setAuthenticatedUser(T authenticatedUser) {
            this.authenticatedUser = authenticatedUser;
        }

        public ConcurrentMap<String, SessionMetadata> getMetadata() {
            if (metadata == null) {
                metadata = new ConcurrentHashMap<String, SessionMetadata>();
            }
            return metadata;
        }

        public void setMetadata(ConcurrentMap<String, SessionMetadata> metadata) {
            this.metadata = metadata;
        }

        public SessionMetadata getMetadata(String objectId) {
            return getMetadata().get(objectId);
        }

        public void addMetadata(String objectId, SessionMetadata object) {
            getMetadata().put(objectId, object);
        }

        public boolean removeMetadata(String objectId) {
            if (getMetadata().containsKey(objectId)) {
                SessionMetadata localMetadata = getMetadata(objectId);
                if (localMetadata!=null) localMetadata.cleanUp();
                return (getMetadata().remove(objectId) != null);
            }
            return false;
        }

        public boolean clearMetatadata() {
            for (Iterator<Entry<String, SessionMetadata>> i = getMetadata().entrySet().iterator(); i.hasNext();) {
                Entry<String, SessionMetadata> metadataEntry = i.next();
                if (metadataEntry.getValue()!=null) metadataEntry.getValue().cleanUp();
                i.remove();
            }
            return true;
        }

        @Override
        protected void finalize() throws Throwable {
            try {
                clearMetatadata();
            } finally {
                super.finalize();
            }
        }
    }

    public SystemConfigurationHelper getSystemConfigurationHelper();

    //Check access to resource
    public boolean checkAccessViewJournals() throws Exception;
    public boolean checkAccessViewJournal(String logToCheckId) throws Exception;
    public boolean checkAccessViewJournal(Log logToCheck) throws Exception;
    public boolean checkAccessDeleteJournals() throws Exception;
    public boolean checkAccessDeleteJournal(String logToCheckId) throws Exception;
    public boolean checkAccessDeleteJournal(Log logToCheck) throws Exception;

    //Check access to resource
    public boolean checkAccessViewNotifications() throws Exception;
    public boolean checkAccessViewNotification(String messageToCheckId) throws Exception;
    public boolean checkAccessViewNotification(Notification messageToCheck) throws Exception;
    public boolean checkAccessDeleteNotifications() throws Exception;
    public boolean checkAccessDeleteNotification(String messageToCheckId) throws Exception;
    public boolean checkAccessDeleteNotification(Notification messageToCheck) throws Exception;

    //Check access to resource from a webservice request by the streaming software
    public boolean checkAccessPrivateResource(String privateKey) throws Exception;

    //Login
    public User fetchRegisteredUser(String username, boolean isUsernameCaseSensitive, String password, boolean isPasswordHashed);
    public void setCurrentlyAuthenticatedUserFromSession(User authenticatedUser) throws Exception;
    public User getCurrentlyAuthenticatedUserFromSession() throws Exception;
    public void logInCaseInsensitive(String username, String password, boolean rememberme) throws Exception;
    public void logIn(String username, String password, boolean rememberme) throws Exception;
    public void autoLogInFromCookie() throws Exception;

    public void logOut() throws Exception;

    public User getCurrentlyAuthenticatedUser() throws Exception;

    public ConcurrentMap<String, SessionMetadata> getCurrentlyAuthenticatedUserMetadataFromSession();

    public SessionMetadata getCurrentlyAuthenticatedUserMetadataFromSession(String objectId);

    public void addCurrentlyAuthenticatedUserMetadataFromSession(String objectId, SessionMetadata object);

    public void removeCurrentlyAuthenticatedUserMetadataFromSession(String objectId);

    public boolean isCurrentlyAuthenticatedUserAnAdministrator() throws Exception;

    public boolean isCurrentlyAuthenticatedUser(User userToCheck) throws Exception;

    /**
     * Returns true if the password has been successfully updated
     * @param oldPassword
     * @param newPassword
     * @return
     * @throws Exception
     */
    public boolean updateCurrentlyAuthenticatedUserPassword(String oldPassword, String newPassword) throws Exception;

    public boolean updateCurrentlyAuthenticatedUserListenerRights(Collection<String> newChannels, Collection<String> deletedChannels) throws Exception;

    /**
     * Returns true if at least one of the specified field has been successfully updated
     * @param newEmail
     * @param emailNotification
     * @param newFirstAndLastName
     * @param newTimezone
     * @return
     * @throws Exception
     */
    public boolean updateCurrentlyAuthenticatedUserProfileData(String newEmail, boolean emailNotification, String newFirstAndLastName, String newTimezone) throws Exception;

    public void setCurrentlyAuthenticatedUserVote(String songId, SCORE score) throws Exception;
    public SCORE getCurrentlyAuthenticatedUserVote(String songId) throws Exception;

    public void setCurrentlyAuthenticatedUserRequest(String songId, String channelId, String message) throws Exception;
    public Request getCurrentlyAuthenticatedUserRequest(String songId) throws Exception;

    public boolean hasCurrentlyAuthenticatedRestrictedUserRights(RightsBundle rightsBundle) throws Exception;

    //@deprecated public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(String channelId, RightsBundle rightsBundle) throws Exception;
    public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverChannel(Channel channel, RightsBundle rightsBundle) throws Exception;
    //@deprecated public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(String programmeId, RightsBundle rightsBundle) throws Exception;
    public boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(Programme programme, RightsBundle rightsBundle) throws Exception;

    /*
    //@deprecated public Collection<RestrictedUser> getRestrictedUsersWithRightsOverChannel(String channelId, RightsBundle rightsBundle) throws Exception;
    public Collection<RestrictedUser> getRestrictedUsersWithRightsOverChannel(Channel channel, RightsBundle rightsBundle) throws Exception;
    //@deprecated public Collection<RestrictedUser> getRestrictedUsersWithRightsOverProgramme(String programmeId, RightsBundle rightsBundle) throws Exception;
    public Collection<RestrictedUser> getRestrictedUsersWithRightsOverProgramme(Programme programme, RightsBundle rightsBundle) throws Exception;*/
    //@deprecated public Collection<RestrictedUser> getRestrictedUsersWithRights(Collection<String> channelIds, Collection<String> programmeIds, RightsBundle rightsBundle) throws Exception;
    public Collection<RestrictedUser> getRestrictedUsersWithRightsOverChannelsOrProgrammes(Collection<Channel> channels, Collection<Programme> programmes, RightsBundle rightsBundle) throws Exception;

    public Collection<String> getCurrentlyAuthenticatedRestrictedUserChannelAdministratorRightIds() throws Exception;
    public Collection<Channel> getCurrentlyAuthenticatedRestrictedUserChannelAdministratorRights() throws Exception;

    public Collection<String> getCurrentlyAuthenticatedRestrictedUserProgrammeManagerRightIds() throws Exception;
    public Collection<Channel> getCurrentlyAuthenticatedRestrictedUserProgrammeManagerRights() throws Exception;

    public Collection<String> getCurrentlyAuthenticatedRestrictedUserListenerRightIds() throws Exception;
    public Collection<Channel> getCurrentlyAuthenticatedRestrictedUserListenerRights() throws Exception;

    public Collection<String> getCurrentlyAuthenticatedRestrictedUserAnimatorRightIds() throws Exception;
    public Collection<Programme> getCurrentlyAuthenticatedRestrictedUserAnimatorRights() throws Exception;

    public Collection<String> getCurrentlyAuthenticatedRestrictedUserCuratorRightIds() throws Exception;
    public Collection<Programme> getCurrentlyAuthenticatedRestrictedUserCuratorRights() throws Exception;

    public Collection<String> getCurrentlyAuthenticatedRestrictedUserContributorRightIds() throws Exception;
    public Collection<Programme> getCurrentlyAuthenticatedRestrictedUserContributorRights() throws Exception;

    public Collection<String> getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(RightsBundle rightsBundle) throws Exception;
    public Collection<Channel> getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(RightsBundle rightsBundle) throws Exception;

    public Collection<String> getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(RightsBundle rightsBundle) throws Exception;
    public Collection<Programme> getLinkedProgrammesForCurrentlyAuthenticatedRestrictedUser(RightsBundle rightsBundle) throws Exception;

    //Edit generic object
    public void setCreationTimestamp(GenericPersistentEntityInterface entity) throws Exception;
    public void setCreationTimestamp(GenericPersistentEntityInterface entity, String forcedAuthenticatedUserLogin) throws Exception;
    public void setCreationTimestamp(Collection<? extends GenericPersistentEntityInterface> entities) throws Exception;
    public void setCreationTimestamp(Collection<? extends GenericPersistentEntityInterface> entities, String forcedAuthenticatedUserLogin) throws Exception;
    public void setEditionTimestamp(GenericPersistentEntityInterface entity) throws Exception;
    public void setEditionTimestamp(GenericPersistentEntityInterface entity, String forcedAuthenticatedUserLogin) throws Exception;
    public void setEditionTimestamp(Collection<? extends GenericPersistentEntityInterface> entities) throws Exception;
    public void setEditionTimestamp(Collection<? extends GenericPersistentEntityInterface> entities, String forcedAuthenticatedUserLogin) throws Exception;

    //Journalization
    public void addJournal(Log log) throws Exception;
    public void addJournals(Collection<? extends Log> logs) throws Exception;

    public void deleteJournal(Log log) throws Exception;
    public void deleteJournals(Collection<Log> logs) throws Exception;
    public void purgeJournals() throws Exception;

    public Log getJournal(String id) throws Exception;
    public Log loadJournal(String id) throws Exception;
    public Collection<Log> getJournals(Collection<String> ids) throws Exception;

    public Collection<Log> getFilteredJournals(GroupAndSort<LOG_TAGS> constraint) throws Exception;
    public Collection<Log> getFilteredJournalsForChannelId(GroupAndSort<LOG_TAGS> constraint, String channelId) throws Exception;

    //Notifications
    public void addNotification(Notification notification) throws Exception;
    public void addNotifications(Collection<? extends Notification> notifications) throws Exception;
    
    public void deleteNotification(Notification notification) throws Exception;
    public void deleteNotifications(Collection<Notification> notifications) throws Exception;
    public void purgeNotifications() throws Exception;

    public void sendNotifications() throws Exception;

    public Notification getNotification(String id) throws Exception;
    public Notification loadNotification(String id) throws Exception;
    public Collection<Notification> getNotifications(Collection<String> ids) throws Exception;

    public Collection<Notification> getFilteredNotifications(GroupAndSort<NOTIFICATION_TAGS> constraint) throws Exception;

    public void sendPasswordToEmail(User user) throws Exception;

}
