/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.client.helper.errors.ChannelAlreadyStreamedException;
import biz.ddcr.shampoo.client.helper.errors.SeatAlreadyReservedException;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackObject;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackObject.FLAG;
import biz.ddcr.shampoo.server.domain.feedback.FeedbackObject.TYPE;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.helper.MessageBroadcaster;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * non-persisted bean that lists which streamers are offline and which ones are
 * online. This bean lives for as long as Spring does, even not directly managed
 * by it.
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public final class ChannelStreamerConnectionStatusHelper {

    /**
     * The list of piles of states associated with each channel/streamer
     * *
     */
    private static final Map<Long, DynamicStreamerData> streamerStates;
    private static final Map<Long, ChannelKey> streamerChannelConnections;

    static {
        streamerStates = new ConcurrentHashMap<Long, DynamicStreamerData>();
        streamerChannelConnections = new ConcurrentHashMap<Long, ChannelKey>();
    }

    private ChannelStreamerConnectionStatusHelper() {
        //Library
    }
    
    public static Collection<Long> getAllSeats() {
        return streamerChannelConnections.keySet();
    }
    
    public static Long getSeatNumber(String channelId) {
        for (Entry<Long, ChannelKey> entry : streamerChannelConnections.entrySet()) {
            final ChannelKey channelKey = entry.getValue();
            if (channelKey!=null && channelKey.getChannelId()!=null && channelKey.getChannelId().equals(channelId)) {
                return entry.getKey();
            }
        }
        return null;
    }
    
    public static String getChannelId(long seatNumber) {
        final ChannelKey channelKey = streamerChannelConnections.get(seatNumber);
        return channelKey!=null ? channelKey.getChannelId() : null;
    }

    public static String getProtectionKey(long seatNumber) {
        final ChannelKey channelKey = streamerChannelConnections.get(seatNumber);
        return channelKey!=null ? channelKey.getProtectionKey() : null;
    }

    public static String getProtectionKey(String channelId) {
        Long seatNumber = getSeatNumber(channelId);
        return seatNumber!=null ? getProtectionKey(seatNumber) : null;
    }    
    
    public static Long getRealTimeNextFreeSeatNumber() {
        return getRealTimeNextFreeSeatNumber(1L, Long.MAX_VALUE);
    }
    public static Long getRealTimeNextFreeSeatNumber(long startingSeatNumber) {
        return getRealTimeNextFreeSeatNumber(startingSeatNumber, Long.MAX_VALUE);
    }
    public static Long getRealTimeNextFreeSeatNumber(long startingSeatNumber, long endingSeatNumber) {
        for (long i=startingSeatNumber;i<=endingSeatNumber;i++) {
            if (getStreamerData(i) == null) {
                return i;
            }
        }
        return null;
    }
    
    public static boolean addStreamerChannelStateConnection(long seatNumber, String channelId, String protectionKey, DynamicStreamerData state) {
        if (channelId != null && state != null) {

            //Avoid concurrent modifications while creating new channel/streamer bridge
            //synchronized (streamerChannelConnections) {
                //Check whether there's no same seat already allocated
                DynamicStreamerData previousState = getStreamerData(seatNumber);
                if (previousState != null) {
                    throw new SeatAlreadyReservedException();
                }
                //Check whether the channel is not already handled by another streamer
                Long previousSeatNumber = getSeatNumber(channelId);
                if (previousSeatNumber != null) {
                    throw new ChannelAlreadyStreamedException();
                }

                streamerChannelConnections.put(seatNumber, new ChannelKey(channelId, protectionKey));
                mergeStreamerState(seatNumber, state);
                //Notification feedback
                MessageBroadcaster.add(new FeedbackObject(TYPE.streamer, Long.toString(seatNumber), FLAG.add, null/*
                         * no explicit sender
                         */));
                return true;
            //}
        }
        return false;
    }

    public static boolean deleteStreamerChannelStateConnection(long seatNumber) {
            //Avoid concurrent modifications while deleting new channel/streamer bridge
            //synchronized (streamerChannelConnections) {
                //Check whether the channel is not already handled by another streamer
                    //then remove both associations
                    clearStreamerState(seatNumber);
                    if (streamerChannelConnections.remove(seatNumber) != null) {
                        //Notification feedback
                        MessageBroadcaster.add(new FeedbackObject(TYPE.streamer, Long.toString(seatNumber), FLAG.delete, null/*
                                 * no explicit sender
                                 */));
                        return true;
                    }
            //}
        return false;
    }

    /**
     * fecth the current configuration of a given streamer *
     */
    public static DynamicStreamerData getStreamerData(long seatNumber) {
        return streamerStates.get(seatNumber);
    }

    /**
     * fecth the current configuration of a given streamer,
     * null means the streamer is offline
     */
    public static DynamicStreamerData getStreamerData(String channelId) {
        final Long seatNumber = getSeatNumber(channelId);
        return seatNumber!=null ? getStreamerData(seatNumber) : null;
    }
    
    public static boolean refreshHeartbeat(String channelId) {
        final Long seatNumber = getSeatNumber(channelId);
        return seatNumber!=null ? refreshHeartbeat(seatNumber) : false;
    }
    public static boolean refreshHeartbeat(long seatNumber) {
        final DynamicStreamerData previousState = getStreamerData(seatNumber);
        if (previousState != null) {
            previousState.setLatestHeartbeat(DateHelper.getCurrentTime());
            streamerStates.put(seatNumber, previousState);
            //Notification feedback
            //UPDATE disabled 'edit' notification: GUI gunned down by and server doesn't scale well to very frequent heartbeat updates
            //MessageBroadcaster.add(new FeedbackObject(TYPE.streamer, Long.toString(seatNumber), FLAG.edit, null));
            return true;
        }
        return false;
    }

    public static boolean refreshStreamingState(String channelId, boolean isStreamOnline) {
        final Long seatNumber = getSeatNumber(channelId);
        return seatNumber!=null ? refreshStreamingState(seatNumber, isStreamOnline) : false;
    }
    public static boolean refreshStreamingState(long seatNumber, boolean isStreamOnline) {
        final DynamicStreamerData previousState = getStreamerData(seatNumber);
        if (previousState != null) {
            previousState.setStreaming(isStreamOnline);
            streamerStates.put(seatNumber, previousState);
            //Notification feedback
            MessageBroadcaster.add(new FeedbackObject(TYPE.streamer, Long.toString(seatNumber), FLAG.edit, null));
            return true;
        }
        return false;
    }    
    
    public static boolean updateStreamerState(String channelId, DynamicStreamerData state) {
        final Long seatNumber = getSeatNumber(channelId);
        return seatNumber!=null ? updateStreamerState(seatNumber, state) : false;
    }
    /**
     * Updates the configuiration options for a given streamer BUT does not automatically capture any heartbeat
     * @param seatNumber
     * @param state
     * @return 
     */
    public static boolean updateStreamerState(long seatNumber, DynamicStreamerData state) {
        if (mergeStreamerState(seatNumber, state)) {
            //Notification feedback
            MessageBroadcaster.add(new FeedbackObject(TYPE.streamer, Long.toString(seatNumber), FLAG.edit, null));
            return true;
        }
        return false;
    }

    /**
     * Manually set the configuration of a given streamer *
     */
    protected static boolean mergeStreamerState(long seatNumber, DynamicStreamerData state) {
        if (state != null) {
            //Do only edit if different
            boolean dirtyEdit = false;
            DynamicStreamerData previousState = getStreamerData(seatNumber);
            if (previousState == null) {
                previousState = new DynamicStreamerData();
            }
            //Checking
            if (previousState.isEnableEmergencyItems() != state.isEnableEmergencyItems()) {
                previousState.setEnableEmergencyItems(state.isEnableEmergencyItems());
                dirtyEdit = true;
            }
            if (previousState.isQueueableBlank() != state.isQueueableBlank()) {
                previousState.setQueueableBlank(state.isQueueableBlank());
                dirtyEdit = true;
            }
            if (previousState.getQueueableBlankURI() == null  || !state.getQueueableBlankURI().equals(previousState.getQueueableBlankURI())) {
                previousState.setQueueableBlankURI(state.getQueueableBlankURI());
                dirtyEdit = true;
            }
            if (previousState.isQueueableLive() != state.isQueueableLive()) {
                previousState.setQueueableLive(state.isQueueableLive());
                dirtyEdit = true;
            }
            if (previousState.getQueueableLiveURI() == null  || !state.getQueueableLiveURI().equals(previousState.getQueueableLiveURI())) {
                previousState.setQueueableLiveURI(state.getQueueableLiveURI());
                dirtyEdit = true;
            }
            if (state.getMetadataFormat() == null || !state.getMetadataFormat().equals(previousState.getMetadataFormat())) {
                previousState.setMetadataFormat(state.getMetadataFormat());
                dirtyEdit = true;
            }
            if (previousState.getMinPlayableDuration() != state.getMinPlayableDuration()) {
                previousState.setMinPlayableDuration(state.getMinPlayableDuration());
                dirtyEdit = true;
            }
            if (previousState.getQueueMinItems() != state.getQueueMinItems()) {
                previousState.setQueueMinItems(state.getQueueMinItems());
                dirtyEdit = true;
            }
            if (previousState.getQueueableBlankChunkSize() != state.getQueueableBlankChunkSize()) {
                previousState.setQueueableBlankChunkSize(state.getQueueableBlankChunkSize());
                dirtyEdit = true;
            }
            if (previousState.getQueueableLiveChunkSize() != state.getQueueableLiveChunkSize()) {
                previousState.setQueueableLiveChunkSize(state.getQueueableLiveChunkSize());
                dirtyEdit = true;
            }
            if (state.getStreamURI() == null || !state.getStreamURI().equals(previousState.getStreamURI())) {
                previousState.setStreamURI(state.getStreamURI());
                dirtyEdit = true;
            }
            if (state.getUserAgentID() == null || !state.getUserAgentID().equals(previousState.getUserAgentID())) {
                previousState.setUserAgentID(state.getUserAgentID());
                dirtyEdit = true;
            }
            if (state.getTtl() == null || !state.getTtl().equals(previousState.getTtl())) {
                previousState.setTtl(state.getTtl());
                dirtyEdit = true;
            }
            if (state.getLatestHeartbeat() == null || (previousState.getLatestHeartbeat()!=null && state.getLatestHeartbeat().compareTo(previousState.getLatestHeartbeat())!=0)) {
                previousState.setLatestHeartbeat(state.getLatestHeartbeat());
                dirtyEdit = true;
            }

            if (dirtyEdit) {
                streamerStates.put(seatNumber, previousState);
            }
            return dirtyEdit;
        }
        return false;
    }

    /**
     * Manually unregister (and free the resources associated with) a pile *
     */
    protected static boolean clearStreamerState(long seatNumber) {
        return (streamerStates.remove(seatNumber) != null);
    }
}
