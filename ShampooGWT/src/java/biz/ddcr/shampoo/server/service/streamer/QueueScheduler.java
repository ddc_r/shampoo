/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as publideshed
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoRequestAllowedException;
import biz.ddcr.shampoo.server.dao.archive.ArchiveDao;
import biz.ddcr.shampoo.server.dao.channel.ChannelDao;
import biz.ddcr.shampoo.server.dao.track.TrackDao;
import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.BroadcastableTrackArchive;
import biz.ddcr.shampoo.server.domain.archive.NonQueuedLiveArchive;
import biz.ddcr.shampoo.server.domain.archive.QueuedLiveArchive;
import biz.ddcr.shampoo.server.domain.archive.RequestArchive;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Blank;
import biz.ddcr.shampoo.server.domain.playlist.DynamicPlaylistEntry;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntry;
import biz.ddcr.shampoo.server.domain.playlist.StaticPlaylistEntry;
import biz.ddcr.shampoo.server.domain.playlist.SubSelection;
import biz.ddcr.shampoo.server.domain.queue.BlankQueueItem;
import biz.ddcr.shampoo.server.domain.queue.LiveQueueItem;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemInterface;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.RequestQueueItem;
import biz.ddcr.shampoo.server.domain.queue.SchedulableQueueItem;
import biz.ddcr.shampoo.server.domain.queue.SimpleTrackQueueItem;
import biz.ddcr.shampoo.server.domain.queue.TrackQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.BlankQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.domain.streamer.GenericItemInterface;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.RequestQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.SimpleTrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemVisitor;
import biz.ddcr.shampoo.server.domain.streamer.TrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlotWrapper;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.SORT;
import biz.ddcr.shampoo.server.domain.track.filter.Filter;
import biz.ddcr.shampoo.server.helper.FinalInteger;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.SortUtil;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.NonQueueItemVisitorStreamItemConversionInterface;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.PlayableItemEqualityCheck;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.PlayableItemIsSameTrackCheck;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.PlaylistEntryVisitorTrackInterface;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.QueueItemFromPlayableItemVisitorInterface;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.QueueItemVisitorStreamItemConversionInterface;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.StreamItemVisitorArchiveConversionInterface;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.TimetableSlotVisitorAbsoluteStartTimeAroundRelativeNow;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.TimetableSlotVisitorAbsoluteStartTimeInRelativeInterval;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerHelper.TimetableSlotWrapperBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.SortedSet;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class QueueScheduler extends GenericService implements QueueSchedulerInterface {

    /**
     * index of the first item in the queue *
     */
    private static final long QUEUE_FIRST_SEQUENCE_INDEX = 0;
    private byte watchdogIterationMax;
    /**
     * minimum length of an item, in ms *
     */
    private long watchdogDurationMin;
    private transient ChannelDao channelDao;
    private transient TrackDao trackDao;
    private transient ArchiveDao archiveDao;

    public byte getWatchdogIterationMax() {
        if (watchdogIterationMax<0)
            watchdogIterationMax = 5;
        return watchdogIterationMax;
    }

    public void setWatchdogIterationMax(byte watchdogIterationMax) {
        this.watchdogIterationMax = watchdogIterationMax;
    }

    public long getWatchdogDurationMin() {
        if (watchdogDurationMin<0)
            watchdogDurationMin = 200;
        return watchdogDurationMin;
    }

    public void setWatchdogDurationMin(long watchdogDurationMin) {
        this.watchdogDurationMin = watchdogDurationMin;
    }

    /**
     * dummy class to pass as a refernce on a method parameter, for updating*
     */
    public class PlaylistEntryLocation {

        public Long currentSequenceIndex;
        public Long currentLoopCounter;

        public Integer currentFadeInAmount;
    }

    public ChannelDao getChannelDao() {
        return channelDao;
    }

    public void setChannelDao(ChannelDao channelDao) {
        this.channelDao = channelDao;
    }

    public TrackDao getTrackDao() {
        return trackDao;
    }

    public void setTrackDao(TrackDao trackDao) {
        this.trackDao = trackDao;
    }

    public ArchiveDao getArchiveDao() {
        return archiveDao;
    }

    public void setArchiveDao(ArchiveDao archiveDao) {
        this.archiveDao = archiveDao;
    }

    private void addChannelArchive(Archive archive) {
        if (archive != null) {
            logger.info("addChannelArchive(" + archive.getRefID() + ")");
            archiveDao.addArchive(archive);
            //archive.getChannel().addArchive(archive);
        }
    }

    private void addChannelQueueItem(QueueItem item) {
        if (item != null) {
            logger.info("addChannelQueueItem(" + item.getRefID() + ")");
            channelDao.addQueueItem(item);
            //item.getChannel().addQueueItem(item);
        }
    }

    private void updateChannelQueueItem(QueueItem item) {
        if (item != null) {
            logger.info("updateChannelQueueItem(" + item.getRefID() + ")");
            channelDao.updateQueueItem(item);
        }
    }

    private void addChannelStreamItem(StreamItem item) {
        if (item != null) {
            logger.info("addChannelStreamItem(" + item.getRefID() + ")");
            channelDao.addStreamItem(item);
            //item.getChannel().addStreamItem(item);
        }
    }

    private void dropChannelStreamItem(StreamItem item) throws Exception {
        if (item != null) {
            logger.info("dropChannelStreamItem(" + item.getRefID() + ")");
            //Unlink dependencies
            StreamItemVisitor visitor = new StreamItemVisitor() {

                @Override
                public void visit(LiveNonQueueStreamItem entry) throws Exception {
                    entry.removeTimetableSlot();
                }

                @Override
                public void visit(LiveQueueStreamItem entry) throws Exception {
                    entry.removeTimetableSlot();
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                    entry.removeTimetableSlot();
                    entry.removeTrack();
                }

                @Override
                public void visit(BlankQueueStreamItem entry) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(RequestQueueStreamItem entry) throws Exception {
                    entry.removeTimetableSlot();
                    entry.removeRequest();
                }
            };
            item.acceptVisit(visitor);
            //Remove it at the end only since it is part of the equals() method and removing it earlier would interfer with the other object removal methods
            item.removeChannel();
            channelDao.deleteStreamItem(item);
        }
    }

    private void dropChannelQueueItems(SortedSet<QueueItem> items) throws Exception {
        if (items != null) {
            logger.info("dropChannelQueueItems(multiple)");
            for (Iterator<QueueItem> i = items.iterator(); i.hasNext();) {
                QueueItem qi = i.next();
                i.remove();
                dropChannelQueueItem(qi);
            }
        }
    }

    private void dropChannelQueueItem(QueueItem item) throws Exception {
        if (item != null) {
            logger.info("dropChannelQueueItem(" + item.getRefID() + ")");
            //unlink dependencies
            QueueItemVisitor visitor = new QueueItemVisitor() {

                @Override
                public void visit(SimpleTrackQueueItem queueItem) throws Exception {
                    queueItem.removeTimetableSlot();
                    queueItem.removeItem();
                }

                @Override
                public void visit(LiveQueueItem queueItem) throws Exception {
                    queueItem.removeTimetableSlot();
                }

                @Override
                public void visit(BlankQueueItem queueItem) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(RequestQueueItem queueItem) throws Exception {
                    queueItem.removeTimetableSlot();
                    queueItem.removeRequest();
                }
            };
            item.acceptVisit(visitor);
            //Remove it at the end only since it is part of the equals() method and removing it earlier would interfer with the other object removal methods
            item.removeChannel();
            channelDao.deleteQueueItem(item);
        }
    }

    private void dropRequest(Request request) {
        if (request != null) {
            logger.info("dropRequest(" + request + ")");
            request.removeQueueItem();
            //request.removeRequester();
            request.removeStreamItem();
            /*request.removeSong();
             request.removeChannel();*/
            request.drop();
            trackDao.removeRequest(request);
        }
    }

    /**
     * Fetch the first available item from the channel queue Available means not
     * already pooled by the streamer, once an available item has been popped
     * out the queue its pooled state is set to true
     *
     * @param channel
     * @return
     */
    @Override
    public QueueItem popAvailableQueueItem(Channel channel) throws Exception {
        QueueItem newItem = null;
        if (channel != null) {

            //First try to populate the queue if needed with just one item to reduce the overhead
            populateQueue(channel, 1);

            logger.info("popAvailableQueueItem(" + channel.getLabel() + ")");
            //browse the channel queue from top to bottom
            //channel.getQueueItems() already sorted like a LIFO pile via Hibernate markup
            for (QueueItem item : channel.getQueueItems()) {
                if (!item.isPooled()) {
                    //This one is fine, set it pooled now and update its state in the database
                    item.setPooled(true);
                    updateChannelQueueItem(item);

                    newItem = item;
                    break;
                }
            }

        }
        return newItem;
    }

    /**
     * Get a queueItem from its id, only if it's already been pooled by the
     * streamer
     *
     * @param channel
     * @param itemId
     * @return
     * @throws Exception
     */
    @Override
    public QueueItem getPooledQueueItem(String itemId) throws Exception {
        logger.info("getPooledQueueItem(" + itemId + ")");
        QueueItem item = channelDao.getQueueItem(itemId);
        if (item == null || !item.isPooled()) {
            throw new NoEntityException();
        }
        return item;
    }

    protected Archive convertStreamItemToArchive(StreamItem item, final YearMonthWeekDayHourMinuteSecondMillisecondInterface newStartTime, final long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamItemToArchive(" + item.getRefID() + "," + newStartTime + "," + duration + ")");
            StreamItemVisitorArchiveConversionInterface visitor = new StreamItemVisitorArchiveConversionInterface() {

                private Archive archive = null;

                @Override
                public Archive getArchive() {
                    return archive;
                }

                @Override
                public void visit(LiveNonQueueStreamItem entry) throws Exception {
                    archive = convertStreamItemToArchive(entry, newStartTime, duration);
                }

                @Override
                public void visit(LiveQueueStreamItem entry) throws Exception {
                    archive = convertStreamItemToArchive(entry, newStartTime, duration);
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                    archive = convertStreamItemToArchive(entry, newStartTime, duration);
                }

                @Override
                public void visit(BlankQueueStreamItem entry) throws Exception {
                    archive = null;//Blank are not persisted as Archives
                }

                @Override
                public void visit(RequestQueueStreamItem entry) throws Exception {
                    archive = convertStreamItemToArchive(entry, newStartTime, duration);
                }
            };
            item.acceptVisit(visitor);
            return visitor.getArchive();
        }
        return null;
    }

    protected StreamItem convertNonQueueItemToStreamItem(NonPersistableNonQueueItem item, final YearMonthWeekDayHourMinuteSecondMillisecondInterface newStartTime, final long duration) throws Exception {
        if (item != null) {
            logger.info("convertNonQueueItemToStreamItem(" + item.getItem() + "," + newStartTime + "," + duration + ")");
            NonQueueItemVisitorStreamItemConversionInterface visitor = new NonQueueItemVisitorStreamItemConversionInterface() {

                private StreamItem streamItem = null;

                @Override
                public StreamItem getStreamItem() {
                    return streamItem;
                }

                @Override
                public void visit(LiveNonPersistableNonQueueItem entry) throws Exception {
                    streamItem = convertStreamableItemToStreamItem(entry, newStartTime, duration);
                }
            };
            item.acceptVisit(visitor);
            return visitor.getStreamItem();
        }
        return null;
    }

    protected StreamItem convertQueueItemToStreamItem(QueueItem item, final YearMonthWeekDayHourMinuteSecondMillisecondInterface newStartTime, final long duration) throws Exception {
        if (item != null) {
            logger.info("convertQueueItemToStreamItem(" + item.getRefID() + "," + newStartTime + "," + duration + ")");
            QueueItemVisitorStreamItemConversionInterface visitor = new QueueItemVisitorStreamItemConversionInterface() {

                private StreamItem streamItem = null;

                @Override
                public StreamItem getStreamItem() {
                    return streamItem;
                }

                @Override
                public void visit(SimpleTrackQueueItem entry) throws Exception {
                    streamItem = convertStreamableItemToStreamItem(entry, newStartTime, duration);
                }

                @Override
                public void visit(LiveQueueItem entry) throws Exception {
                    streamItem = convertStreamableItemToStreamItem(entry, newStartTime, duration);
                }

                @Override
                public void visit(BlankQueueItem entry) throws Exception {
                    streamItem = convertStreamableItemToStreamItem(entry, newStartTime, duration);
                }

                @Override
                public void visit(RequestQueueItem entry) throws Exception {
                    streamItem = convertStreamableItemToStreamItem(entry, newStartTime, duration);
                }
            };
            item.acceptVisit(visitor);
            return visitor.getStreamItem();
        }
        return null;
    }

    protected BroadcastableTrackArchive convertStreamItemToArchive(SimpleTrackQueueStreamItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamItemToArchive(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new BroadcastableTrackArchive(
                    item.getTrack(),
                    item.getChannel(),
                    startTime,
                    duration,
                    item.getTimetableSlot(),
                    item.getRefID());
        }
        return null;
    }

    protected RequestArchive convertStreamItemToArchive(RequestQueueStreamItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamItemToArchive(" + item.getRefID() + "," + startTime + "," + duration + ")");
            RequestArchive archive = new RequestArchive(
                    item.getTrack(),
                    item.getChannel(),
                    startTime,
                    duration,
                    item.getTimetableSlot(),
                    item.getRequest() != null ? item.getRequest().getRequester() : null,
                    item.getRefID());
            //The linked Request MUST be deleted once it's been archived
            dropRequest(item.getRequest());

            return archive;
        }
        return null;
    }

    protected QueuedLiveArchive convertStreamItemToArchive(LiveQueueStreamItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamItemToArchive(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new QueuedLiveArchive(
                    (item.getTimetableSlot() != null && item.getTimetableSlot().getPlaylist() != null ? item.getTimetableSlot().getPlaylist().getLive() : null),
                    item.getChannel(),
                    startTime,
                    duration,
                    item.getTimetableSlot(),
                    item.getRefID());
        }
        return null;
    }

    protected NonQueuedLiveArchive convertStreamItemToArchive(LiveNonQueueStreamItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamItemToArchive(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new NonQueuedLiveArchive(
                    (item.getTimetableSlot() != null && item.getTimetableSlot().getPlaylist() != null ? item.getTimetableSlot().getPlaylist().getLive() : null),
                    item.getChannel(),
                    startTime,
                    duration,
                    item.getTimetableSlot());
        }
        return null;
    }

    protected TrackQueueStreamItem convertStreamableItemToStreamItem(SimpleTrackQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, Long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamableItemToStreamItem(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new SimpleTrackQueueStreamItem(
                    item.getChannel(),
                    item.getTimetableSlot(),
                    startTime,
                    duration,
                    item.getRefID(),
                    item.getItem());
        }
        return null;
    }

    protected RequestQueueStreamItem convertStreamableItemToStreamItem(RequestQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, Long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamableItemToStreamItem(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new RequestQueueStreamItem(
                    item.getTimetableSlot(),
                    startTime,
                    duration,
                    item.getRefID(),
                    item.getRequest());
        }
        return null;
    }

    protected LiveQueueStreamItem convertStreamableItemToStreamItem(LiveQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, Long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamableItemToStreamItem(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new LiveQueueStreamItem(
                    item.getChannel(),
                    item.getTimetableSlot(),
                    startTime,
                    duration,
                    item.getRefID());
        }
        return null;
    }

    protected LiveNonQueueStreamItem convertStreamableItemToStreamItem(LiveNonPersistableNonQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, Long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamableItemToStreamItem(" + item.getItem() + "," + startTime + "," + duration + ")");
            return new LiveNonQueueStreamItem(
                    item.getChannel(),
                    item.getTimetableSlot(),
                    startTime,
                    duration);
        }
        return null;
    }

    protected BlankQueueStreamItem convertStreamableItemToStreamItem(BlankQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, Long duration) throws Exception {
        if (item != null) {
            logger.info("convertStreamableItemToStreamItem(" + item.getRefID() + "," + startTime + "," + duration + ")");
            return new BlankQueueStreamItem(
                    item.getChannel(),
                    startTime,
                    duration,
                    item.getRefID());
        }
        return null;
    }

    /**
     * now playing
     *
     * @param item
     * @param startTime
     * @throws Exception
     */
    protected void addNewNowPlaying(NonPersistableNonQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        if (item != null) {
            logger.info("addNewNowPlaying(" + item.getItem() + "," + startTime + ")");
            //Move any previous item in the channel "now playing" slot to the archives
            StreamItem previouslyPlayingItem = item.getChannel().getStreamItem();
            if (previouslyPlayingItem != null) {

                //update its end time with the provided one
                long newDuration = startTime != null ? startTime.getUnixTime() - previouslyPlayingItem.getStartTime().getUnixTime() : previouslyPlayingItem.getDuration();
                Archive newArchive = convertStreamItemToArchive(previouslyPlayingItem, previouslyPlayingItem.getStartTime(), newDuration);
                //persist changes
                addChannelArchive(newArchive);
                dropChannelStreamItem(previouslyPlayingItem);
            }

            //Now register the new item
            StreamItem nowPlayingItem = convertNonQueueItemToStreamItem(item, startTime, item.getDuration());
            //persist changes
            addChannelStreamItem(nowPlayingItem);
        }
    }

    /**
     * Update the track rotation counter when an item is being played out, if
     * applicable
     */
    protected void incrementRotationCounter(QueueItem item) throws Exception {
        if (item != null && item.getItem() != null) {
            QueueItemVisitor visitor = new QueueItemVisitor() {

                protected void incrementTrackRotationCounter(BroadcastableTrack track, TimetableSlot timetableSlot) {
                    if (track != null && timetableSlot != null) {
                        track.setRotationCount(timetableSlot.getProgramme(), track.getRotationCount(timetableSlot.getProgramme()) + 1);
                        //Persist the changes, if applicable
                        trackDao.updateTrack(track);
                    }
                }

                @Override
                public void visit(SimpleTrackQueueItem queueItem) throws Exception {
                    incrementTrackRotationCounter(queueItem.getItem(), queueItem.getTimetableSlot());
                }

                @Override
                public void visit(LiveQueueItem queueItem) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BlankQueueItem queueItem) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(RequestQueueItem queueItem) throws Exception {
                    incrementTrackRotationCounter(queueItem.getItem(), queueItem.getTimetableSlot());
                }
            };
            item.acceptVisit(visitor);
        }
    }

    protected void addNewNowPlaying(QueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        if (item != null) {
            logger.info("addNewNowPlaying(" + item.getRefID() + "," + startTime + ")");
            //Move any previous item in the channel "now playing" slot to the archives
            StreamItem previouslyPlayingItem = item.getChannel().getStreamItem();
            if (previouslyPlayingItem != null) {

                //update its end time with the provided one
                long newDuration = startTime != null ? startTime.getUnixTime() - previouslyPlayingItem.getStartTime().getUnixTime() : previouslyPlayingItem.getDuration();
                Archive newArchive = convertStreamItemToArchive(previouslyPlayingItem, previouslyPlayingItem.getStartTime(), newDuration);
                //persist changes
                addChannelArchive(newArchive);
                dropChannelStreamItem(previouslyPlayingItem);
            }

            //update the trackRotation count if applicable
            //Since the method uses queue items, make sure it's lcoated before the call to convertQueueItemToStreamItem()
            incrementRotationCounter(item);

            //Now register the new item
            StreamItem nowPlayingItem = convertQueueItemToStreamItem(item, startTime, item.getDuration());
            //persist changes
            addChannelStreamItem(nowPlayingItem);
        }
    }

    /**
     * internal, used within removeCurrentNowPlaying() *
     */
    private void _archiveUnregisteredItem(QueueItem unregisteredItem, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        long newDurationForUnregistered = startTime.getUnixTime() - unregisteredItem.getStartTime().getUnixTime();
        //If it's less then 0 we can safely guess that something wrong happened; don't archive it
        //The most likely error is the item being played before it was even programmed for broadcast
        if (newDurationForUnregistered > 0) {
            StreamItem _o = convertQueueItemToStreamItem(unregisteredItem, unregisteredItem.getStartTime(), unregisteredItem.getDuration());
            Archive newArchiveForUnregistered = convertStreamItemToArchive(_o, _o.getStartTime(), newDurationForUnregistered);
            //persist changes
            addChannelArchive(newArchiveForUnregistered);
        }
    }

    private void _archiveUnregisteredItem(NonPersistableNonQueueItem unregisteredItem, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        long newDurationForUnregistered = startTime.getUnixTime() - unregisteredItem.getStartTime().getUnixTime();
        //If it's less then 0 we can safely guess that something wrong happened; don't archive it
        if (newDurationForUnregistered > 0) {
            StreamItem _o = convertNonQueueItemToStreamItem(unregisteredItem, unregisteredItem.getStartTime(), unregisteredItem.getDuration());
            Archive newArchiveForUnregistered = convertStreamItemToArchive(_o, _o.getStartTime(), newDurationForUnregistered);
            //persist changes
            addChannelArchive(newArchiveForUnregistered);
        }
    }

    /**
     * drop the "now playing" slot
     */
    protected void removeCurrentNowPlaying(final NonPersistableNonQueueItem item, final YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        if (item != null) {
            logger.info("removeCurrentNowPlaying(" + item.getItem() + "," + startTime + ")");

            //Check if the current "now playing" item is the same as the given one
            StreamItem previouslyPlayingItem = item.getChannel().getStreamItem();
            //Don't rely on the equals(), Hibernate breaks everything
            //TODO: cannot discriminate lives and non lives if the latters are added sometime
            StreamItemVisitor visitor = new StreamItemVisitor() {

                private void archiveCurrent(StreamItem currentItem, NonPersistableNonQueueItem unregisteredItem) throws Exception {
                    long newDurationForCurrent = unregisteredItem.getEndTimestamp() - currentItem.getStartTime().getUnixTime();
                    //update its end time with the provided one
                    Archive newArchive = convertStreamItemToArchive(currentItem, currentItem.getStartTime(), newDurationForCurrent);
                    //persist changes
                    addChannelArchive(newArchive);
                }

                private void archiveBoth(StreamItem currentItem, NonPersistableNonQueueItem unregisteredItem) throws Exception {
                    long newDurationForCurrent = unregisteredItem.getStartTime().getUnixTime() - currentItem.getStartTime().getUnixTime();
                    //If it's less then 0 we can safely guess that something wrong happened; don't archive it
                    if (newDurationForCurrent > 0) {
                        Archive newArchiveForCurrent = convertStreamItemToArchive(currentItem, item.getStartTime(), newDurationForCurrent);
                        //persist changes
                        addChannelArchive(newArchiveForCurrent);
                    }
                    _archiveUnregisteredItem(unregisteredItem, startTime);
                }

                @Override
                public void visit(LiveNonQueueStreamItem entry) throws Exception {
                    //Check if they're from the same timetable, if they are assume they're the same
                    //Hibernate may be a deal breaker, let's not try to compare proxies, just extract the id
                    String itemSlotId = item.getTimetableSlot() != null ? item.getTimetableSlot().getRefID() : null;
                    String entrySlotId = entry.getTimetableSlot() != null ? entry.getTimetableSlot().getRefID() : null;
                    if (itemSlotId != null && itemSlotId.equals(entrySlotId)) {
                        archiveCurrent(entry, item);
                    } else {
                        archiveBoth(entry, item);
                    }
                }

                @Override
                public void visit(LiveQueueStreamItem entry) throws Exception {
                    archiveBoth(entry, item);
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                    archiveBoth(entry, item);
                }

                @Override
                public void visit(BlankQueueStreamItem entry) throws Exception {
                    //Blanks are not archived; but for simplicty sakes use the standard routines
                    archiveBoth(entry, item);
                }

                @Override
                public void visit(RequestQueueStreamItem entry) throws Exception {
                    archiveBoth(entry, item);
                }
            };
            //Check if there was something playing beforehand
            if (previouslyPlayingItem != null) {
                previouslyPlayingItem.acceptVisit(visitor);
            } else {
                //Otherwise just archive the given item
                _archiveUnregisteredItem(item, startTime);
            }

            dropChannelStreamItem(previouslyPlayingItem);

        }
    }

    protected void removeCurrentNowPlaying(final QueueItem item, final YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        if (item != null) {
            logger.info("removeCurrentNowPlaying(" + item.getRefID() + "," + startTime + ")");

            //Check if the current "now playing" item is the same as the given one
            StreamItem previouslyPlayingItem = item.getChannel().getStreamItem();
            //Don't rely on the equals(), Hibernate breaks everything
            //TODO: cannot discriminate lives and non lives if the latters are added sometime
            StreamItemVisitor visitor = new StreamItemVisitor() {

                private void archiveCurrent(StreamItem currentItem, QueueItem unregisteredItem) throws Exception {
                    //update its end time with the provided one
                    Archive newArchive = convertStreamItemToArchive(currentItem, currentItem.getStartTime(), unregisteredItem.getDuration());
                    //persist changes
                    addChannelArchive(newArchive);
                }

                private void archiveBoth(StreamItem currentItem, QueueItem unregisteredItem) throws Exception {
                    long newDurationForCurrent = unregisteredItem.getStartTime().getUnixTime() - currentItem.getStartTime().getUnixTime();
                    //If it's less then 0 we can safely guess that something wrong happened; don't archive it
                    if (newDurationForCurrent > 0) {
                        Archive newArchiveForCurrent = convertStreamItemToArchive(currentItem, item.getStartTime(), newDurationForCurrent);
                        //persist changes
                        addChannelArchive(newArchiveForCurrent);
                    }
                    _archiveUnregisteredItem(unregisteredItem, startTime);
                }

                @Override
                public void visit(LiveNonQueueStreamItem entry) throws Exception {
                    archiveBoth(entry, item);
                }

                @Override
                public void visit(LiveQueueStreamItem entry) throws Exception {
                    //Check if they share the same queueid, if they do assume they're the same
                    if (entry.getQueueId().equals(item.getRefID())) {
                        archiveCurrent(entry, item);
                    } else {
                        archiveBoth(entry, item);
                    }
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                    //Check if they share the same queueid, if they do assume they're the same
                    if (entry.getQueueId().equals(item.getRefID())) {
                        archiveCurrent(entry, item);
                    } else {
                        archiveBoth(entry, item);
                    }
                }

                @Override
                public void visit(BlankQueueStreamItem entry) throws Exception {
                    //Blanks are not archived; but for simplicty sakes use the standard routines
                    //Check if they share the same queueid, if they do assume they're the same
                    if (entry.getQueueId().equals(item.getRefID())) {
                        archiveCurrent(entry, item);
                    } else {
                        archiveBoth(entry, item);
                    }
                }

                @Override
                public void visit(RequestQueueStreamItem entry) throws Exception {
                    if (entry.getQueueId().equals(item.getRefID())) {
                        archiveCurrent(entry, item);
                    } else {
                        archiveBoth(entry, item);
                    }
                }
            };
            //Check if there was something playing beforehand
            if (previouslyPlayingItem != null) {
                previouslyPlayingItem.acceptVisit(visitor);
            } else {
                //Otherwise just archive the given item
                _archiveUnregisteredItem(item, startTime);
            }

            dropChannelStreamItem(previouslyPlayingItem);

        }
    }

    protected void removeCurrentQueueNowPlaying(Channel channel, final String queueId, final YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime) throws Exception {
        if (channel != null && queueId != null && endTime != null) {
            logger.info("removeCurrentQueueNowPlaying(" + channel.getLabel() + "," + queueId + "," + endTime + ")");

            //Check if the current "now playing" item is the same as the given one
            StreamItem previouslyPlayingItem = channel.getStreamItem();
            //Don't rely on the equals(), Hibernate breaks everything
            //TODO: cannot discriminate lives and non lives if the latters are added sometime
            StreamItemVisitor visitor = new StreamItemVisitor() {

                private void archiveCurrent(StreamItem currentItem) throws Exception {
                    //update its end time with the provided one
                    long newDuration = endTime.getUnixTime() - currentItem.getStartTime().getUnixTime();
                    //If it's less then 0 we can safely guess that something wrong happened; don't archive it
                    if (newDuration > 0) {
                        Archive newArchive = convertStreamItemToArchive(currentItem, currentItem.getStartTime(), newDuration);
                        //persist changes
                        addChannelArchive(newArchive);
                    }
                }

                @Override
                public void visit(LiveNonQueueStreamItem entry) throws Exception {
                    {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(LiveQueueStreamItem entry) throws Exception {
                    //Check if they share the same queueid, if they do assume they're the same
                    if (queueId.equals(entry.getQueueId())) {
                        archiveCurrent(entry);
                    } else {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                    //Check if they share the same queueid, if they do assume they're the same
                    if (queueId.equals(entry.getQueueId())) {
                        archiveCurrent(entry);
                    } else {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(BlankQueueStreamItem entry) throws Exception {
                    //Blanks are not archived; but for simplicty sakes use the standard routines
                    //Check if they share the same queueid, if they do assume they're the same
                    if (queueId.equals(entry.getQueueId())) {
                        archiveCurrent(entry);
                    } else {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(RequestQueueStreamItem entry) throws Exception {
                    //Check if they share the same queueid, if they do assume they're the same
                    if (queueId.equals(entry.getQueueId())) {
                        archiveCurrent(entry);
                    } else {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }
            };
            //Check if there was something playing beforehand
            if (previouslyPlayingItem != null) {
                previouslyPlayingItem.acceptVisit(visitor);
            } else {
                //Otherwise do nothing and throw an exception
                throw new NoEntityException();
            }
            dropChannelStreamItem(previouslyPlayingItem);

        }
    }

    protected void removeCurrentNonQueueNowPlaying(Channel channel, final String timetableSlotId, final YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime) throws Exception {
        if (channel != null && timetableSlotId != null && endTime != null) {
            logger.info("removeCurrentNonQueueNowPlaying(" + channel.getLabel() + "," + timetableSlotId + "," + endTime + ")");

            //Check if the current "now playing" item is the same as the given one
            StreamItem previouslyPlayingItem = channel.getStreamItem();
            //Don't rely on the equals(), Hibernate breaks everything
            //TODO: cannot discriminate live and non-live non-queued items if the latters are added sometime
            StreamItemVisitor visitor = new StreamItemVisitor() {

                private void archiveCurrent(StreamItem currentItem) throws Exception {
                    //update its end time with the provided one
                    long newDuration = endTime.getUnixTime() - currentItem.getStartTime().getUnixTime();
                    //If it's less then 0 we can safely guess that something wrong happened; don't archive it
                    if (newDuration > 0) {
                        Archive newArchive = convertStreamItemToArchive(currentItem, currentItem.getStartTime(), newDuration);
                        //persist changes
                        addChannelArchive(newArchive);
                    }
                }

                @Override
                public void visit(LiveNonQueueStreamItem entry) throws Exception {
                    //Check if they share the same queueid, if they do assume they're the same
                    if (timetableSlotId.equals(entry.getTimetableSlot() != null ? entry.getTimetableSlot().getRefID() : null)) {
                        archiveCurrent(entry);
                    } else {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(LiveQueueStreamItem entry) throws Exception {
                    {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                    {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(BlankQueueStreamItem entry) throws Exception {
                    {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }

                @Override
                public void visit(RequestQueueStreamItem entry) throws Exception {
                    {
                        //Otherwise do nothing and throw an exception
                        throw new NoEntityException();
                    }
                }
            };
            //Check if there was something playing beforehand
            if (previouslyPlayingItem != null) {
                previouslyPlayingItem.acceptVisit(visitor);
            } else {
                //Otherwise do nothing and throw an exception
                throw new NoEntityException();
            }
            //Otherwise do nothing
            dropChannelStreamItem(previouslyPlayingItem);

        }
    }

    protected Archive browseArchivesForPlayableItemInPlaylist(Channel channel, TimetableSlot timetableSlot, PlayableItemInterface item) throws Exception {
        return browseArchivesForPlayableItem(channel, timetableSlot, null, item);
    }

    protected Archive browseArchivesForPlayableItemBeforeTimeThreshold(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time, PlayableItemInterface item) throws Exception {
        return browseArchivesForPlayableItem(channel, null, time, item);
    }

    protected Archive getLatestArchive(Channel channel) throws Exception {
        if (channel != null) {
            logger.info("getLatestArchive(" + channel.getLabel() + ")");
            //The lack of backward iterator for sets is a real burden,
            //use a custom HQL request for this specific task
            final byte selectionSize = 1;
            long selectionIndex = 0;
            Collection<? extends Archive> archives
                    = archiveDao.getSelectedArchives(
                            channel.getLabel(),
                            selectionIndex,
                            selectionIndex + selectionSize - 1);
            //The collection should be sorted with freshest archives first
            if (archives != null) {
                for (Archive archive : archives) {
                    return archive;
                }
            }
        }
        return null;
    }

    /**
     * Returns null if no Archive matches
     *
     * @param channel
     * @param playlist
     * @param time
     * @param item
     * @return
     * @throws Exception
     */
    protected Archive browseArchivesForPlayableItem(Channel channel, TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface time, PlayableItemInterface item) throws Exception {

        if (channel != null && (time != null || timetableSlot != null) && item != null) {
            logger.info("browseArchivesForPlayableItem(" + channel.getLabel() + "," + timetableSlot + "," + time + "," + item + ")");
            //The lack of backward iterator for sets is a real burden,
            //use a custom HQL request for this specific task: gather sorted records in small quantities, iterate through them, if there's no match repeat those steps
            final byte selectionSize = 6;
            long selectionIndex = 0;
            Collection<? extends Archive> archives;
            do {
                archives = archiveDao.getSelectedArchives(
                        channel.getLabel(),
                        selectionIndex,
                        selectionSize + selectionIndex - 1);

                //The collection should be sorted with freshest archives first
                if (archives != null) {
                    for (Archive archive : archives) {

                        //if the archive happened before th specified time then break
                        if (time != null) {
                            if (archive.getStartTime().compareTo(time) < 0) {
                                logger.info("past time threshold: break");
                                return null;
                            }
                        }

                        //if the archive doesn't belong to the same playlist then break
                        if (timetableSlot != null) {
                            if (!archive.getTimetableSlot().equals(timetableSlot.getRefID())) {
                                logger.info("different timetable slot: break");
                                return null;
                            }
                        }

                        boolean _i = isSurfaceItemSimilar(item, archive);
                        if (_i) {
                            logger.info("same item already played within range");
                            return archive;
                        }

                    }
                }
                selectionIndex += selectionSize;
                //Fetch the next batch of archives, unless there is no archive left to browse
            } while (archives != null && archives.size() == selectionSize);
        }
        return null;
    }

    protected long getMinPlayableDuration(Channel channel) {
        final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
        return streamerState != null ? Math.min(streamerState.getMinPlayableDuration(), getWatchdogDurationMin()) : getWatchdogDurationMin();
    }

    /**
     * If timestamps returned by the webservices are off compared to what was
     * expected from the queue scheduling then "fix" the queue according to
     * those new times
     *
     * @param item
     * @param newStartTime
     * @param newEndTime
     * @throws Exception
     */
    protected void fixQueueScheduling(QueueItem returnedItem, YearMonthWeekDayHourMinuteSecondMillisecondInterface newStartTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface newEndTime) throws Exception {
        if (returnedItem != null && newStartTime != null && newEndTime != null) {
            logger.info("fixQueueScheduling(" + returnedItem.getRefID() + "," + newStartTime + "," + newEndTime + ")");

            YearMonthWeekDayHourMinuteSecondMillisecondInterface previousStartTime = returnedItem.getStartTime();
            long previousDuration = returnedItem.getLikelyDuration();
            YearMonthWeekDayHourMinuteSecondMillisecondInterface previousEndTime = new YearMonthWeekDayHourMinuteSecondMillisecond(previousStartTime);
            previousEndTime.addMilliseconds(previousDuration);

            long newDuration = newEndTime.getUnixTime() - newStartTime.getUnixTime();

            long diff = newEndTime.getUnixTime() - previousEndTime.getUnixTime();

            //update the current item with the new times, if applicable
            returnedItem.setLikelyStartTime(newStartTime);
            returnedItem.setLikelyDuration(newDuration);
            //Persist the changes, if applicable
            //FIX don't, since pruneQueueItem() will drop it later on: rare Hibernate concurrency issues (?)
            //channelDao.updateQueueItem(returnedItem);

            //diff > 0 means the item stopped later than scheduled by diff ms
            //diff < 0 means the item stopped earlier than scheduled by diff ms
            if (diff != 0) {
                //Okay, the current item doesn't end at the right time, there is a discrepancy with timings, we need to intervene on subsequent items

                //Browse the queuedItems and fix their timings
                //Check if TimeMarkers are coherent, otherwise drop all unpooled items, the queue will have to be repopulated
                //Get all items that are meant to be played after the current item (+ the current item)
                //.tailSet() works with pointers so updates on the returned set hopefully impact the original queue
                SortedSet<QueueItem> tailItems = returnedItem.getChannel().getQueueItems().tailSet(returnedItem);

                if (tailItems != null) {
                    previousEndTime = newEndTime;
                    Iterator<QueueItem> i = tailItems.iterator();

                    //Skip the current Item
                    if (i.hasNext()) {
                        i.next();
                    }

                    boolean scrapOffWholeQueue = false;
                    //Now browse through all remaining items, and until the timing problem has been fully fixed (first pass)
                    while (i.hasNext() && diff != 0) {
                        QueueItem currentItem = i.next();

                        YearMonthWeekDayHourMinuteSecondMillisecondInterface currentStartTime = currentItem.getStartTime();
                        long currentDuration = currentItem.getLikelyDuration();
                        YearMonthWeekDayHourMinuteSecondMillisecondInterface currentEndTime = new YearMonthWeekDayHourMinuteSecondMillisecond(currentStartTime);
                        currentEndTime.addMilliseconds(currentDuration);

                        //If likelyDuration() is any different than naturalDuration() then there must ba a change of playlist going on: we must try to respect this end time to the best of our abilities
                        //boolean isEndTimePlaylistMarker = (currentItem.getNaturalDuration() != null && currentItem.getLikelyDuration() < currentItem.getNaturalDuration());
                        //STEP 1:
                        //Use scheduled gaps as buffers if available
                        long gapBufferLength = currentStartTime.getUnixTime() - previousEndTime.getUnixTime();
                        if (gapBufferLength > 0) {
                            //ok there's a programmed gap at the end of the previous item: make the best of it
                            if (diff > 0) {
                                //the item stopped after the scheduled time
                                diff = Math.max(0, diff - gapBufferLength);
                            }
                        }

                        //STEP 2:
                        //Play on start times
                        //Make the next item starts earlier if diff is positive
                        //Or make the next item starts later if diff is negative
                        if (diff != 0) {
                            //if diff < 0 then advance startTime to diff ms
                            //if diff > 0 then delay startTime by diff ms
                            currentStartTime.addMilliseconds(diff);
                            //Don't readjust diff, it has only shifted the startTime but didn't fix the time delta
                            currentItem.setLikelyStartTime(currentStartTime);
                        }

                        //STEP 3:
                        //Play on durations
                        //Make the next item longer if diff is positive
                        //Or make the next item shorter if diff is negative
                        //Only works on UNPOOLED items, we asume the streamer will respect the given duration when first pooled
                        //TODO: compare the new diff/timeMarker and the new track duration and see if timeMarkers must be relocated
                        if (diff != 0 && !currentItem.isPooled() && currentItem.getTimeMarkerAtEnd() != null) {
                            //if end times are time markers then re-adjust diff accordingly
                            currentDuration += (-diff + currentItem.getTimeMarkerAtEnd());

                            long min_duration = getMinPlayableDuration(returnedItem.getChannel());

                            //ADDON see if timeMarkers make sense otherwise drop the whole queue to be safe
                            if (currentDuration < min_duration || currentDuration > currentItem.getNaturalDuration()) {
                                scrapOffWholeQueue = true;
                                //Don't break here, just in case some pooled items are interleaved with unpooled items (it shouldn't happen though)
                                //Their timings will be fixed next time fixQueueScheduling() is called
                            }

                            //CHECK: deactivate min. size check, Liqudisoap appears to perfectly handle tiny tracks
                            //We should not use getNaturalDuration() for blanks for example since the result is unspecified, instead restore getLikelyDuration() in this case
                            //FIX blanks now support getNaturalDuration()
                            currentDuration = Math.min(currentDuration, currentItem.getNaturalDuration());
                            currentDuration = Math.max(currentDuration, min_duration);
                            //Adjust diff accordingly
                            long newOffset = currentDuration - currentItem.getLikelyDuration();
                            diff += newOffset;
                            currentItem.setLikelyDuration(currentDuration);
                            currentItem.setTimeMarkerAtEnd(diff);

                        }

                        //Compute the new likely end time, will be used for taking care of natural gaps
                        previousEndTime = new YearMonthWeekDayHourMinuteSecondMillisecond(currentItem.getStartTime());
                        previousEndTime.addMilliseconds(currentItem.getLikelyDuration());

                    } //END while

                    //Persist required changes
                    //Re-acquire an iterator over the collection (second pass)
                    Iterator<QueueItem> j = tailItems.iterator();
                    //Skip the current Item
                    if (j.hasNext()) {
                        j.next();
                    }
                    while (j.hasNext()) {
                        QueueItem currentItem = j.next();
                        if (!currentItem.isPooled() && scrapOffWholeQueue) {
                            j.remove();
                            dropChannelQueueItem(currentItem);
                        } else {
                            channelDao.updateQueueItem(currentItem);
                        }
                    }

                }//END tailItems!=null

            }//END diff!=0
        }
    }

    /**
     * notify the application that the streamer has started to play the given
     * non queued item, on the specified time
     *
     * @param item
     * @param time
     * @throws Exception
     */
    @Override
    public void setNonQueueItemOnAir(NonPersistableNonQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setNonQueueItemOnAir(" + item + "," + time + ")");
        //We can't do much since it's off the queue
        //-Add an Archive entry
        //-Do misc. stuff like notifying last.fm, whatever

        //DEPRECATED: archives do not display "now playing" items any more: more consistent behaviour
        //addUnfinishedNonQueueItemToArchive(item, time);
        addNewNowPlaying(item, time);

        //TODO lastfm
        //Non queue items will obviously mess up the whole queue; so scrap it (make sure the streamer will perform the same operation as well)
        if (item != null) {
            purgeQueueItems(item.getChannel());
        }

    }

    /**
     * notify the application that the streamer has stopped playing the given
     * non queued item, on the specified time
     *
     * @param item
     * @param time
     * @throws Exception
     */
    @Override
    public void setNonQueueItemOffAir(NonPersistableNonQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setNonQueueItemOffAir(" + item + "," + time + ")");
        //We can't do much since it's off the queue
        //-Modify the corresponding Archive entry with the proper ending time
        //-Do misc. stuff like notifying last.fm, whatever

        //DEPRECATED: archives do not display "now playing" items any more: more consistent behaviour
        //pushFinishedItemWithinArchive(item, time);
        removeCurrentNowPlaying(item, time);

        //TODO lastfm
        //Non queue items will obviously mess up the whole queue; so scrap it (make sure the streamer will perform the same operation as well)
        if (item != null) {
            purgeQueueItems(item.getChannel());
        }
    }

    @Override
    public void setNonQueueReferenceOffAir(Channel channel, String timetableSlotId, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setNonQueueReferenceOffAir(" + channel + "," + timetableSlotId + "," + time + ")");
        //Same as setNonQueueItemOffAir() but less things to do actually
        removeCurrentNonQueueNowPlaying(channel, timetableSlotId, time);

        //Non queue items will obviously mess up the whole queue; so scrap it (make sure the streamer will perform the same operation as well)
        purgeQueueItems(channel);
    }

    /**
     * Clean the queue of any item that were stuck
     *
     * @param item
     */
    protected void pruneQueueItems(QueueItem item) throws Exception {
        logger.info("pruneQueueItems(" + item + ")");
        //Get all items that were played before the current item
        //Unplayable items whose status change hasn't been notified may still be in the queue (e.g. unplayable items)
        //Do only move the current item to the archive, unnotified items are just scrapped from the queue
        //.headSet() works with pointers so updates on the returned set hopefully impact the original queue
        if (item != null) {
            SortedSet<QueueItem> headItems = item.getChannel().getQueueItems().headSet(item);
            if (headItems != null) {
                //Cascade should drop dependencies dead, need exhaustive tests
                //persist changes
                dropChannelQueueItems(headItems);
            }
            //And then kill the current item
            dropChannelQueueItem(item);
        }
    }

    /**
     * notify the application that the streamer has started to play the given
     * pooled queue item, on the specified time
     *
     * @param item
     * @param time
     * @throws Exception
     */
    @Override
    public void setPooledQueueItemOnAir(QueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime) throws Exception {
        logger.info("setPooledQueueItemOnAir(" + item + "," + startTime + ")");
        //-Adjust the item registered start time if different, and then the whole the queue if necessary
        //-Modify the latest Archive entry with the proper ending time if setPooledQueueItemOffAir() hasn't been called beforehand
        //-Move every item located before in the archive
        //-Do misc. stuff like notifying last.fm, whatever

        //Let's try to guess the likely end time for this item based on info from the queue
        //The start time is actually what the webservice returned and is considered fixed
        YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime = null;
        if (item != null && startTime != null) {
            (endTime = new YearMonthWeekDayHourMinuteSecondMillisecond(startTime)).addMilliseconds(item.getLikelyDuration());
        }
        fixQueueScheduling(item, startTime, endTime);

        //DEPRECATED: archives do not display "now playing" items any more: more consistent behaviour
        //copyUnfinishedQueueItemToArchive(item, startTime);
        addNewNowPlaying(item, startTime);

        // Must be located at the end, since it erases the reference to the current item
        pruneQueueItems(item);

        //TODO lastfm
    }

    /**
     * notify the application that the streamer has stopped playing the given
     * pooled queue item, on the specified time
     *
     * @param item
     * @param time
     * @throws Exception
     */
    @Override
    public void setPooledQueueItemOffAir(QueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setPooledQueueItemOffAir(" + item + "," + time + ")");
        //-Adjust the item registered end time if different, and then the whole queue if necessary
        //-Modify the corresponding Archive entry with the proper ending time
        //-Do misc. stuff like notifying last.fm, whatever

        fixQueueScheduling(item, item.getStartTime(), time);

        //DEPRECATED: archives do not display "now playing" items any more: more consistent behaviour
        //pushFinishedItemWithinArchive(item, time);
        removeCurrentNowPlaying(item, time);

        // Must be located at the end, since it erases the reference to the current item
        pruneQueueItems(item);

        //TODO lastfm
    }

    @Override
    public void setPooledQueueReferenceOffAir(Channel channel, String queueId, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setPooledQueueReferenceOffAir(" + channel + "," + queueId + "," + time + ")");
        //same as setPooledQueueItemOffAir() except that we have no queue item to play with, it's already been removed from the queue
        removeCurrentQueueNowPlaying(channel, queueId, time);
    }

    /**
     * Returns true if the streamer status has changed after this function has
     * been called
     *
     * @param channel
     * @param time
     * @return
     * @throws Exception
     */
    @Override
    public boolean setChannelFree(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setChannelFree(" + channel + "," + time + ")");
        //Don't do much
        //-Purge the queue
        //-TODO: log the event and warn the administrator

        boolean result = false;
        if (channel != null) {

            //Only update the streamer status if necessary
            //Check if streamer has already been unbound to a seat beforehand and deactivate it, otherwise do nothing
            if (ChannelStreamerConnectionStatusHelper.getSeatNumber(channel.getLabel()) == null) {
                //Cascade should drop dependencies dead, need exhaustive tests
                //persist changes
                purgeStreamItem(channel);
                purgeQueueItems(channel);
                //channelDao.deleteBatchQueueItems(channel.getQueueItems());
                result = true;
            }
        }
        return result;
    }

    /**
     * Returns true if the streamer status has changed after this function has
     * been called
     *
     * @param channel
     * @param time
     * @return
     * @throws Exception
     */
    @Override
    public boolean setChannelReserved(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        logger.info("setChannelReserved(" + channel + "," + time + ")");
        //Don't do much
        //-Purge the queue
        //-TODO: log the event and warn the administrator

        boolean result = false;
        if (channel != null) {

            //Only update the streamer status if necessary, i.e. already bound to a seat
            if (ChannelStreamerConnectionStatusHelper.getSeatNumber(channel.getLabel()) != null) {
                //Cascade should drop dependencies dead, need exhaustive tests
                //persist changes
                purgeObsoleteQueueItems(channel);
                //channelDao.deleteBatchQueueItems(channel.getQueueItems());
                result = true;
            }
        }
        return result;
    }

    private long getNumberOfUnpooledQueueItems(Channel channel) {
        logger.info("getNumberOfUnpooledQueueItems(" + channel + ")");
        long result = 0;
        if (channel != null) {
            for (Iterator<QueueItem> i = channel.getQueueItems().iterator(); i.hasNext();) {
                if (!(i.next()).isPooled()) {
                    result++;
                }
            }
        }
        return result;
    }

    /**
     * fill in the channel queue with a minimum number of items if necessary
     *
     * @param channel
     * @throws Exception
     */
    @Override
    public void populateQueue(Channel channel) throws Exception {
        final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
        if (streamerState != null) {
            populateQueue(channel, streamerState.getQueueMinItems());
        }
    }

    /**
     * fill in the channel queue with a minimum number of items if necessary
     *
     * @param channel
     * @throws Exception
     */
    protected void populateQueue(Channel channel, int requestedNumberOfItems) throws Exception {
        logger.info("populateQueue(" + channel + ")");
        if (channel != null) {

            if (requestedNumberOfItems < 1) {
                requestedNumberOfItems = 1;
            }

            //Drop obsolete items first
            //purgeObsoleteQueueItems(channel);
            //Just to be sure the while loop will end one way or another
            byte loopGuardrail = 0;
            //Find the number of items still required to fill in the queue
            //Do not take into account "pooled" items
            long currentQueueItemsNeededSize = requestedNumberOfItems - getNumberOfUnpooledQueueItems(channel);
            //Fill in the queue, one item at a time
            //Stop the process if the minimum number of items required is fullfilled
            while (currentQueueItemsNeededSize > 0 && loopGuardrail < getWatchdogIterationMax()) {
                //Put a new item at the end of the queue
                //First try t find an item starting just after the last one from the queue, then find the next timetableslot if there's none
                if (appendNextQueueItem(channel, loopGuardrail) != null) {
                    //Correct track found, reinit the loop counter
                    loopGuardrail = 0;
                    currentQueueItemsNeededSize--;
                } else {
                    loopGuardrail++;
                }
            }
        }
    }

    private Collection<TimetableSlotWrapper> wrapNext(Collection<? extends TimetableSlot> slots, YearMonthWeekDayHourMinuteSecondMillisecondInterface lowerTime) throws Exception {
        if (slots != null && !slots.isEmpty()) {
            TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow v = new TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow(lowerTime);
            Collection<TimetableSlotWrapper> bs = new HashSet<TimetableSlotWrapper>();
            for (TimetableSlot slot : slots) {
                slot.acceptVisit(v);
                YearMonthWeekDayHourMinuteSecondMillisecondInterface aST = v.getAbsoluteStartTime();
                if (aST != null) {
                    TimetableSlotWrapperBuilder b = new TimetableSlotWrapperBuilder(aST);
                    slot.acceptVisit(b);
                    bs.add(b.get());
                }
            }
            return bs;
        }
        return null;
    }

    private TimetableSlotWrapper wrapNext(TimetableSlot slot, YearMonthWeekDayHourMinuteSecondMillisecondInterface lowerTime) throws Exception {
        if (slot != null) {
            TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow v = new TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow(lowerTime);
            slot.acceptVisit(v);
            YearMonthWeekDayHourMinuteSecondMillisecondInterface aST = v.getAbsoluteStartTime();
            if (aST != null) {
                TimetableSlotWrapperBuilder b = new TimetableSlotWrapperBuilder(aST);
                slot.acceptVisit(b);
                return b.get();
            }
        }
        return null;
    }

    private TimetableSlotWrapper wrapAround(TimetableSlot slot, YearMonthWeekDayHourMinuteSecondMillisecondInterface aroundTime) throws Exception {
        if (slot != null) {
            TimetableSlotVisitorAbsoluteStartTimeAroundRelativeNow v = new TimetableSlotVisitorAbsoluteStartTimeAroundRelativeNow(aroundTime);
            slot.acceptVisit(v);
            YearMonthWeekDayHourMinuteSecondMillisecondInterface aST = v.getAbsoluteStartTime();
            if (aST != null) {
                TimetableSlotWrapperBuilder b = new TimetableSlotWrapperBuilder(aST);
                slot.acceptVisit(b);
                return b.get();
            }
        }
        return null;
    }

    private TimetableSlotWrapper wrapInside(TimetableSlot slot, YearMonthWeekDayHourMinuteSecondMillisecondInterface lowerTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface upperTime) throws Exception {
        if (slot != null) {
            TimetableSlotVisitorAbsoluteStartTimeInRelativeInterval v = new TimetableSlotVisitorAbsoluteStartTimeInRelativeInterval(lowerTime, upperTime);
            slot.acceptVisit(v);
            YearMonthWeekDayHourMinuteSecondMillisecondInterface aST = v.getAbsoluteStartTime();
            if (aST != null) {
                TimetableSlotWrapperBuilder b = new TimetableSlotWrapperBuilder(aST);
                slot.acceptVisit(b);
                return b.get();
            }
        }
        return null;
    }

    private Collection<TimetableSlotWrapper> wrapAround(Collection<TimetableSlot> slots, YearMonthWeekDayHourMinuteSecondMillisecondInterface aroundTime) throws Exception {
        Collection<TimetableSlotWrapper> c = new HashSet<TimetableSlotWrapper>();
        if (slots != null) {
            for (TimetableSlot slot : slots) {
                TimetableSlotWrapper w = wrapAround(slot, aroundTime);
                if (w != null) {
                    c.add(w);
                }
            }
        }
        return c;
    }

    private Collection<TimetableSlotWrapper> wrapInside(Collection<TimetableSlot> slots, YearMonthWeekDayHourMinuteSecondMillisecondInterface lowerTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface upperTime) throws Exception {
        Collection<TimetableSlotWrapper> c = new HashSet<TimetableSlotWrapper>();
        if (slots != null) {
            for (TimetableSlot slot : slots) {
                TimetableSlotWrapper w = wrapInside(slot, lowerTime, upperTime);
                if (w != null) {
                    c.add(w);
                }
            }
        }
        return c;
    }

    public TimetableSlotWrapper getFirstBroadcastTimetableSlotStartingAfter(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface fuzzyTime) throws Exception {
        TimetableSlotWrapper broadcastSlot = null;
        if (channel != null && fuzzyTime != null) {
            logger.info("getBroadcastTimetableSlotStartingAfter(" + channel + "," + fuzzyTime + ")");
            Collection<TimetableSlotWrapper> interlacedSlots = new HashSet<TimetableSlotWrapper>(); //HashSets accept null so make sure the sorting method correctly handles null
            //Get the first slot for this channel starting after this time
            //One for each type of slots
            TimetableSlotWrapper u = wrapNext(channelDao.getNextEnabledUniqueTimetableSlotForChannelFuzzyFrom(channel.getLabel(), channel.getTimezone(), fuzzyTime), fuzzyTime);
            if (u != null) {
                interlacedSlots.add(u);
            }
            Collection<TimetableSlotWrapper> ds = wrapNext(channelDao.getNextEnabledDailyTimetableSlotForChannelFuzzyFrom(channel.getLabel(), channel.getTimezone(), fuzzyTime), fuzzyTime);
            if (ds != null) {
                interlacedSlots.addAll(ds);
            }
            Collection<TimetableSlotWrapper> ws = wrapNext(channelDao.getNextEnabledWeeklyTimetableSlotForChannelFuzzyFrom(channel.getLabel(), channel.getTimezone(), fuzzyTime), fuzzyTime);
            if (ws != null) {
                interlacedSlots.addAll(ws);
            }
            Collection<TimetableSlotWrapper> ms = wrapNext(channelDao.getNextEnabledMonthlyTimetableSlotForChannelFuzzyFrom(channel.getLabel(), channel.getTimezone(), fuzzyTime), fuzzyTime);
            if (ms != null) {
                interlacedSlots.addAll(ms);
            }
            Collection<TimetableSlotWrapper> ys = wrapNext(channelDao.getNextEnabledYearlyTimetableSlotForChannelFuzzyFrom(channel.getLabel(), channel.getTimezone(), fuzzyTime), fuzzyTime);
            if (ys != null) {
                interlacedSlots.addAll(ys);
            }
            //then pick the correct one among all
            broadcastSlot = pickFirstStartingValidBroadcastTimetableSlotFromDisjointOrInterlacedSlots(interlacedSlots, fuzzyTime);
        }
        return broadcastSlot;
    }

    /**
     * Select one single slot over multiple interlaced slots based on precedence
     * rules
     *
     * @param interlacedSlots
     * @return
     * @throws Exception
     */
    public TimetableSlotWrapper pickValidBroadcastTimetableSlotFromInterlacedSlots(Collection<TimetableSlotWrapper> interlacedSlots, YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeNow) throws Exception {
        TimetableSlotWrapper broadcastSlot = null;
        if (interlacedSlots != null && !interlacedSlots.isEmpty()) {
            logger.info("pickValidBroadcastTimetableSlotFromInterlacedSlots(multiple," + relativeNow + ")");

            //Find the only one to select among all candidates according to the following rule:
            //Unique > Yearly > Monthly > Weekly > Daily
            //If multiple slots of the same kind are available then:
            //the slot that starts last has precedence; there could be no ex-aequo since it would violate database constraints
            //BEWARE that those rules and their logic are replicated within the client; always sync. them to avoid inconsistencies
            byte currentBroadcastSlotValue = -1;

            QueueSchedulerHelper.TimetableSlotVisitorValue visitor = new QueueSchedulerHelper.TimetableSlotVisitorValue();

            for (TimetableSlotWrapper availableSlot : interlacedSlots) {
                if (availableSlot != null) {
                    availableSlot.acceptVisit(visitor);

                    //none picked up yet? choose the current one by default
                    if (broadcastSlot == null) {
                        broadcastSlot = availableSlot;
                        currentBroadcastSlotValue = visitor.getValue();
                    } else {
                        if (visitor.getValue() > currentBroadcastSlotValue) {
                            //take the one that has precedence because of its type as new current slot
                            broadcastSlot = availableSlot;
                            currentBroadcastSlotValue = visitor.getValue();
                        } else if (visitor.getValue() == currentBroadcastSlotValue) {
                            //take the one that starts last based on the reference date relativeNow in case of equality as a new current slot
                            if (availableSlot.getAbsoluteStartTime().compareTo(broadcastSlot.getAbsoluteStartTime()) > 0) {
                                broadcastSlot = availableSlot;
                                currentBroadcastSlotValue = visitor.getValue();
                            }
                        }
                    }
                }
            }
        }
        return broadcastSlot;
    }

    /**
     * Select one single slot over multiple interlaced or disjoint slots based
     * on precedence rules pickValidBroadcastTimetableSlotFromInterlacedSlots()
     * uses different rules than this one
     *
     * @param interlacedSlots
     * @return
     * @throws Exception
     */
    public TimetableSlotWrapper pickFirstStartingValidBroadcastTimetableSlotFromDisjointOrInterlacedSlots(Collection<TimetableSlotWrapper> interlacedSlots, YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeNow) throws Exception {
        TimetableSlotWrapper broadcastSlot = null;
        if (interlacedSlots != null && !interlacedSlots.isEmpty()) {
            logger.info("pickFirstStartingValidBroadcastTimetableSlotFromDisjointOrInterlacedSlots(multiple," + relativeNow + ")");

            //Find the only one to select among all candidates according to the following rule:
            //the slot that starts first in this interval has precedence
            //if there are ex-aequos then:
            //Unique > Yearly > Monthly > Weekly > Daily
            byte currentBroadcastSlotValue = -1;

            QueueSchedulerHelper.TimetableSlotVisitorValue visitor = new QueueSchedulerHelper.TimetableSlotVisitorValue();

            for (TimetableSlotWrapper availableSlot : interlacedSlots) {
                if (availableSlot != null) {
                    availableSlot.acceptVisit(visitor);

                    //none picked up yet? choose the current one by default
                    if (broadcastSlot == null) {
                        broadcastSlot = availableSlot;
                        currentBroadcastSlotValue = visitor.getValue();
                    } else {

                        if (availableSlot.getAbsoluteStartTime().compareTo(broadcastSlot.getAbsoluteStartTime()) < 0) {
                            broadcastSlot = availableSlot;
                            currentBroadcastSlotValue = visitor.getValue();
                        } else if (availableSlot.getAbsoluteStartTime().compareTo(broadcastSlot.getAbsoluteStartTime()) == 0) {
                            if (visitor.getValue() > currentBroadcastSlotValue) {
                                //take the one that has precedence because of its type as new current slot
                                broadcastSlot = availableSlot;
                                currentBroadcastSlotValue = visitor.getValue();
                            }
                        }

                    }
                }
            }
        }
        return broadcastSlot;
    }

    @Override
    public TimetableSlotWrapper getBroadcastTimetableSlotPlayingOn(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface fuzzyTime) throws Exception {
        TimetableSlotWrapper broadcastSlot = null;
        if (channel != null && fuzzyTime != null) {
            logger.info("getBroadcastTimetableSlotPlayingOn(" + channel.getLabel() + "," + fuzzyTime + ")");
            Collection<TimetableSlotWrapper> interlacedSlots;
            //Get all possible slots for this channel and time
            interlacedSlots = wrapAround(channelDao.getEnabledTimetableSlotsForChannelFuzzyAt(channel.getLabel(), channel.getTimezone(), fuzzyTime), fuzzyTime);
            //then pick the correct one among all
            broadcastSlot = pickValidBroadcastTimetableSlotFromInterlacedSlots(interlacedSlots, fuzzyTime);
        }
        return broadcastSlot;
    }

    public TimetableSlotWrapper getFirstBroadcastTimetableSlotInInterval(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface startTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface endTime) throws Exception {
        TimetableSlotWrapper broadcastSlot = null;
        if (channel != null && startTime != null && endTime != null) {
            logger.info("getFirstBroadcastTimetableSlotInInterval(" + channel.getLabel() + "," + startTime + "," + endTime + ")");
            Collection<TimetableSlotWrapper> interlacedSlots;
            //Get all possible slots for this channel and that start within this time interval
            interlacedSlots = wrapInside(channelDao.getEnabledTimetableSlotsForChannelStartingInInterval(channel.getLabel(), channel.getTimezone(), startTime, endTime), startTime, endTime);
            //then pick the correct one among all
            broadcastSlot = pickFirstStartingValidBroadcastTimetableSlotFromDisjointOrInterlacedSlots(interlacedSlots, startTime);
        }
        return broadcastSlot;
    }

    @Override
    public LiveNonPersistableNonQueueItem getBroadcastableLiveAt(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception {
        if (channel != null && time != null) {
            logger.info("getBroadcastableLiveAt(" + channel.getLabel() + "," + time + ")");

            //Fetch the timetable slot corresponding to this new start time
            TimetableSlotWrapper newTimetableSlotWrapper = getBroadcastTimetableSlotPlayingOn(channel, time);

            //Use the current timetable slot's playlist rules to choose what to queue
            if (newTimetableSlotWrapper != null && newTimetableSlotWrapper.getSlot().getPlaylist() != null) {
                if (newTimetableSlotWrapper.getSlot().getPlaylist().getLive() != null) {
                    return new LiveNonPersistableNonQueueItem(newTimetableSlotWrapper.getSlot());
                }
            }
        }
        return null;
    }

    @Override
    public LiveNonPersistableNonQueueItem getBroadcastableLiveFor(TimetableSlot timetableSlot) throws Exception {
        if (timetableSlot != null) {
            logger.info("getBroadcastableLiveFor(" + timetableSlot.getRefID() + ")");

            //Get the live bound to the playlist of this timetableslot
            Live live = timetableSlot.getPlaylist() != null ? timetableSlot.getPlaylist().getLive() : null;
            //And finally wrap it into a new LiveNonQueueItem
            return (live != null)
                    ? new LiveNonPersistableNonQueueItem(timetableSlot) : null;
        }
        return null;
    }

    /**
     * Append a blank item in the specified queue from blankStartTime to
     * nextTimetableSlotWrapper startTime returns the new item now in the queue.
     * null if none has been added.
     *
     * @param blankStartTime
     * @param nextTimetableSlotWrapper
     * @return
     */
    public QueueItem appendBlankQueueItem(Long newQueueSequenceIndex, YearMonthWeekDayHourMinuteSecondMillisecondInterface blankStartTime, TimetableSlotWrapper nextTimetableSlotWrapper) {
        if (blankStartTime != null) {
            if (nextTimetableSlotWrapper != null) {
                logger.info("appendBlankQueueItem(" + newQueueSequenceIndex + "," + blankStartTime + "," + nextTimetableSlotWrapper.getSlot() + ")");

                YearMonthWeekDayHourMinuteSecondMillisecondInterface blankEndTime = nextTimetableSlotWrapper.getAbsoluteStartTime();
                Channel currentChannel = nextTimetableSlotWrapper.getSlot().getChannel();

                long remainingDuration = blankEndTime.getUnixTime() - blankStartTime.getUnixTime();

                final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(currentChannel.getLabel());
                Long maxChunkDuration = streamerState != null ? streamerState.getQueueableBlankChunkSize() : null;
                long iMaxChunkDuration = remainingDuration;
                Long isTimeMarker = nextTimetableSlotWrapper.getSlot().getProgramme().isOverlappingPlaylistAllowed() ? null : 0L;
            //Adjust remainingDuration if blanks are meant to be queued as multiple sequential items instead of as a whole

                if (maxChunkDuration != null && maxChunkDuration > 0) {
                    //maxChunkDuration is millisecond based
                    iMaxChunkDuration = maxChunkDuration;
                    if (remainingDuration > iMaxChunkDuration) {
                        remainingDuration = iMaxChunkDuration;
                        isTimeMarker = null;
                    }
                }

                //Do only register a new item to broadcast if times make sense
                if (remainingDuration > 0) {
                    BlankQueueItem newQueueItem = new BlankQueueItem(currentChannel, newQueueSequenceIndex, iMaxChunkDuration);
                    if (isTimeMarker != null) {
                        newQueueItem.setTimeMarkerAtEnd(isTimeMarker);
                    }
                    //Set the required attributes for this new object
                    newQueueItem.setLikelyStartTime(blankStartTime);
                    newQueueItem.setLikelyDuration(
                            remainingDuration);
                    //newQueueItem.setPooled(false); //Not yet pooled by default

                    addChannelQueueItem(newQueueItem);

                    return newQueueItem;
                }
            } else {
                logger.error("Your radio is not properly configured: no show has ever been scheduled for airing in the future, your radio will remain forever silent");
            }
        }
        return null;
    }

    /**
     * Append a new item to the specified channel queue returns the new item now
     * in the queue. null if none has been added. Finds the next item to play at
     * the time the last item from the queue has stopped playing if isInSequence
     * is true otherwise finds the next item to play from the next available
     * timetable slot at the time the last item from the queue has stopped
     * playing autoInc is a special incremental value that will force the
     * shifting of the entry selection if needed
     *
     * @param channel
     * @param time
     * @return
     */
    public QueueItem appendNextQueueItem(Channel channel, byte autoInc) throws Exception {
        QueueItem newQueueItem = null;
        if (channel != null) {
            logger.info("appendNextQueueItem(" + channel.getLabel() + "," + autoInc + ")");

            //Find the freshest item already in the queue if the queue has already been filled in, at least partially
            //use the recommended finite length of the last item now in the queue and its start time to compute a likely start time for the new item
            YearMonthWeekDayHourMinuteSecondMillisecondInterface newItemStartTime = null;
            QueueItem lastItem = null;
            if (!channel.getQueueItems().isEmpty()) {
                lastItem = channel.getQueueItems().last();
                newItemStartTime = getQueueItemEndTime(lastItem);
                /*if (newItemStartTime==null) {
                 //I believe that if times don't make sense at this point we should clear the current queue
                 //TODO: test this condition thoroughly
                 //TODO: see if trimming pooled and requested items is no big deal for users
                 dropAllChannelQueueItems(channel);
                 }*/
            }
            //Compute a new sequence index for the queue
            long newSequenceIndex = getNewQueueSequenceIndex(lastItem);
            if (newItemStartTime == null) {
                //Most likely the queue has never been filled in yet, or times don't make sense
                //Assume the item will start now
                newItemStartTime = DateHelper.getCurrentTime(channel.getTimezone());

                //But check the currently playing item and make its end time the new start time for the future queue item, iff it's greater than the current time
                StreamItem nowPlayingItem = channel.getStreamItem();
                if (nowPlayingItem != null) {
                    YearMonthWeekDayHourMinuteSecondMillisecond streamItemStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond(nowPlayingItem.getStartTime());
                    streamItemStartTime.addMilliseconds(nowPlayingItem.getLikelyDuration());
                    if (streamItemStartTime.compareTo(newItemStartTime) > 0) {
                        newItemStartTime = streamItemStartTime;
                    }
                }
                lastItem = null;
            }

            //Fetch the timetable slot corresponding to this new start time
            TimetableSlotWrapper newTimetableSlotWrapper = getBroadcastTimetableSlotPlayingOn(channel, newItemStartTime);
            //if there's no current one, find the very first slot starting after this date
            if (newTimetableSlotWrapper == null) {
                newTimetableSlotWrapper = getFirstBroadcastTimetableSlotStartingAfter(channel, newItemStartTime);
                //If off-air events can be queued then go no further and branch the function from here
                final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
                if (streamerState != null && streamerState.isQueueableBlank()) {
                    QueueItem blankItem = appendBlankQueueItem(newSequenceIndex, newItemStartTime, newTimetableSlotWrapper);
                    return blankItem;
                }
            }

            if (newTimetableSlotWrapper != null) {

                YearMonthWeekDayHourMinuteSecondMillisecondInterface newItemEndTime = null;

                //Then adjust its endTime by pre-fetching the next one coming up, if the current one's endTime is past the next one's startTime                
                //Add one millisecond to the new start time, so as to make the start of the interval non-inclusive
                YearMonthWeekDayHourMinuteSecondMillisecond newItemStartTimePlusOne = new YearMonthWeekDayHourMinuteSecondMillisecond(newItemStartTime);
                newItemStartTimePlusOne.addMilliseconds(1);
                YearMonthWeekDayHourMinuteSecondMillisecond newUncutItemEndTime = new YearMonthWeekDayHourMinuteSecondMillisecond(newTimetableSlotWrapper.getAbsoluteStartTime());
                newUncutItemEndTime.addMilliseconds(newTimetableSlotWrapper.getSlot().getLengthInMilliseconds());
                TimetableSlotWrapper nextNewTimetableSlotWrapper = getFirstBroadcastTimetableSlotInInterval(channel, newItemStartTimePlusOne, newUncutItemEndTime);
                if (nextNewTimetableSlotWrapper != null) {
                    //if there's one happening before the end of the next slot, fix the slot length
                    newItemEndTime = nextNewTimetableSlotWrapper.getAbsoluteStartTime();
                } else {
                    newItemEndTime = newUncutItemEndTime;
                }
                //newItemStartTime = newTimetableSlotRelativeTimesVisitor.getRelativeStartTime();

                //Use the current timetable slot's playlist rules to choose what to queue
                if (newTimetableSlotWrapper.getSlot().getPlaylist() != null) {
                    //If lives can be queued and the current playlist is bound to a live then go no further
                    final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
                    if ((streamerState != null && streamerState.isQueueableLive()) && newTimetableSlotWrapper.getSlot().getPlaylist().getLive() != null) {
                        //See if chunk durations are enforced, and used them if they are
                        Long maxChunkDuration = streamerState.getQueueableLiveChunkSize();
                        long iMaxChunkDuration = 0;
                        if (/*maxChunkDuration != null &&*/maxChunkDuration > 0) {
                            iMaxChunkDuration = maxChunkDuration * 60 * 1000;
                        } else {
                            iMaxChunkDuration = newTimetableSlotWrapper.getSlot().getLengthInMilliseconds();
                        }
                        newQueueItem = new LiveQueueItem(channel, newSequenceIndex, newTimetableSlotWrapper.getSlot(), iMaxChunkDuration);
                        //Set the required attributes for this new object
                        newQueueItem.setLikelyStartTime(newItemStartTime);

                        long remainingDuration = newItemEndTime.getUnixTime() - newItemStartTime.getUnixTime();
                        //Adjust remainingDuration if blanks are meant to be queued as multiple sequential items instead of as a whole
                        if (remainingDuration > iMaxChunkDuration) {
                            remainingDuration = iMaxChunkDuration;
                        }

                        newQueueItem.setLikelyDuration(
                                remainingDuration); //By default, a live is the size of the timetable slot it belongs to
                        //newQueueItem.setPooled(false); //Not yet pooled by default
                    } else {
                        //Otherwise use the playlist rules to get a new TrackQueueItem or a RequestQueueItem
                        //Get the last position in the playlist if applicable:
                        //First see if the last played item comes from the same timetable slot (and thus from the same playlist), and was a track, not a live
                        final PlaylistEntryLocation loc = new PlaylistEntryLocation();
                        //Start from scratch by default
                        loc.currentSequenceIndex = null;
                        loc.currentLoopCounter = null;
                        loc.currentFadeInAmount = null;
                        if (lastItem != null) {
                            final TimetableSlot finalTimetableSlot = newTimetableSlotWrapper.getSlot();
                            QueueItemVisitor visitor = new QueueItemVisitor() {

                                @Override
                                public void visit(SimpleTrackQueueItem queueItem) throws Exception {
                                    if (QueueSchedulerHelper.areEqual(finalTimetableSlot, queueItem.getTimetableSlot())) {
                                        //In this case use the recorded item position in the playlist to get the next track to play
                                        loc.currentSequenceIndex = queueItem.getPlaylistEntryCurrentSequenceIndex();
                                        loc.currentLoopCounter = queueItem.getPlaylistEntryCurrentLoopIndex();
                                        loc.currentFadeInAmount = queueItem.getPlaylistEntryCurrentFadeIn();
                                    }
                                    //Otherrwise do nothing, use default loc values
                                }

                                @Override
                                public void visit(LiveQueueItem queueItem) throws Exception {
                                    //Do nothing, use default loc values
                                }

                                @Override
                                public void visit(BlankQueueItem queueItem) throws Exception {
                                    //Do nothing, use default loc values
                                }

                                @Override
                                public void visit(RequestQueueItem queueItem) throws Exception {
                                    if (QueueSchedulerHelper.areEqual(finalTimetableSlot, queueItem.getTimetableSlot())) {
                                        //In this case use the recorded item position in the playlist to get the next track to play
                                        loc.currentSequenceIndex = queueItem.getPlaylistEntryCurrentSequenceIndex();
                                        loc.currentLoopCounter = queueItem.getPlaylistEntryCurrentLoopIndex();
                                        loc.currentFadeInAmount = queueItem.getPlaylistEntryCurrentFadeIn();
                                    }
                                    //Otherwise do nothing, use default loc values
                                }
                            };
                            lastItem.acceptVisit(visitor);
                        }
                        //Other do nothing, use default loc values
                        newQueueItem = getPlaylistNextQueueItem(newSequenceIndex, channel, newTimetableSlotWrapper.getSlot(), newItemStartTime, loc, autoInc);
                    }

                    if (newQueueItem != null) {
                        //Check the duration of this new item and adjust it if the current programme does not allow overlapping playlist
                        if (newTimetableSlotWrapper.getSlot().getPlaylist().getProgramme() != null && !newTimetableSlotWrapper.getSlot().getPlaylist().getProgramme().isOverlappingPlaylistAllowed()) {

                            long maxSlotDuration = newItemEndTime.getUnixTime() - newItemStartTime.getUnixTime();
                            //newDuration E [minFixedDurationForChannel..maxItemDuration]
                            newQueueItem.setLikelyDuration(//
                                    //CHECK: deactivate min. size check, Liqudisoap appears to perfectly handle tiny tracks
                                    Math.max(//
                                            //channel.getStreamer().getMinPlayableDuration(),//*/
                                            getMinPlayableDuration(channel),
                                            Math.min(//
                                                    newQueueItem.getLikelyDuration(),//
                                                    maxSlotDuration//
                                            )//
                                    )//
                            );//

                            //Specify if it's time marker
                            if (newQueueItem.getLikelyDuration() >= maxSlotDuration) {
                                newQueueItem.setTimeMarkerAtEnd(0L);
                            }

                        }
                        //Persist the change in the queue
                        //channelDao.addQueueItem(newQueueItem);
                        addChannelQueueItem(newQueueItem);
                    }

                }

            }
        }
        //The returned item is now either null or a fully-fledged object that has been bound to the specified channel queue
        return newQueueItem;
    }

    /**
     * Wraps getPlaylistNextItem() around sugar-coated visitor patterns to make
     * a new QueueItem from a playlist-generated PlayableItemInterface
     */
    protected QueueItem getPlaylistNextQueueItem(final long newSequenceIndex, final Channel channel, final TimetableSlot currentTimetableSlot, final YearMonthWeekDayHourMinuteSecondMillisecondInterface scheduledStartTime, final PlaylistEntryLocation loc, byte autoInc) throws Exception {
        PlayableItemInterface nextItem = getPlaylistNextItem(channel, currentTimetableSlot, scheduledStartTime, loc, autoInc);
        if (nextItem != null) {

            QueueItemFromPlayableItemVisitorInterface visitor = new QueueItemFromPlayableItemVisitorInterface() {

                QueueItem queueItem = null;

                @Override
                public QueueItem get() {
                    return queueItem;
                }

                public void requestVisit(Request request) {
                    queueItem = new RequestQueueItem(request, newSequenceIndex, currentTimetableSlot);
                    ((RequestQueueItem) queueItem).setPlaylistEntryCurrentSequenceIndex(loc.currentSequenceIndex);
                    ((RequestQueueItem) queueItem).setPlaylistEntryCurrentLoopIndex(loc.currentLoopCounter);
                    //Set the required attributes for this new object
                    queueItem.setLikelyStartTime(scheduledStartTime);
                    queueItem.setLikelyDuration(
                            //convert seconds into milliseconds
                            (long) (request.getSong().getTrackContainer().getDuration() * 1000) //By default, a track queue item lasts as long the duration of the track it embeds
                    );
                    ((RequestQueueItem) queueItem).setPlaylistEntryCurrentFadeIn(loc.currentFadeInAmount);
                    //newQueueItem.setPooled(false); //Not yet pooled by default
                }

                public void trackVisit(BroadcastableTrack track) throws Exception {
                    queueItem = new SimpleTrackQueueItem(channel, newSequenceIndex, currentTimetableSlot, track);
                    ((TrackQueueItem) queueItem).setPlaylistEntryCurrentSequenceIndex(loc.currentSequenceIndex);
                    ((TrackQueueItem) queueItem).setPlaylistEntryCurrentLoopIndex(loc.currentLoopCounter);
                    //Set the required attributes for this new object
                    queueItem.setLikelyStartTime(scheduledStartTime);
                    queueItem.setLikelyDuration(
                            //convert seconds into milliseconds
                            (long) (track.getTrackContainer().getDuration() * 1000) //By default, a track queue item lasts as long the duration of the track it embeds
                    );
                    ((TrackQueueItem) queueItem).setPlaylistEntryCurrentFadeIn(loc.currentFadeInAmount);
                    //newQueueItem.setPooled(false); //Not yet pooled by default
                }

                @Override
                public void visit(BroadcastableAdvert track) throws Exception {
                    trackVisit(track);
                }

                @Override
                public void visit(BroadcastableJingle track) throws Exception {
                    trackVisit(track);
                }

                @Override
                public void visit(BroadcastableSong track) throws Exception {
                    trackVisit(track);
                }

                @Override
                public void visit(Live live) throws Exception {
                    //Nothing to do, yet
                    //Reserved for later
                }

                @Override
                public void visit(Blank blank) throws Exception {
                    //Nothing to do, yet
                    //Reserved for later
                }

                @Override
                public void visit(Request request) throws Exception {
                    requestVisit(request);
                }
            };
            nextItem.is(visitor);

            return visitor.get();
        }
        return null;
    }

    /**
     * Get the next track or request to play based on the playlist rules,
     * starting from the specific position defined by currentSequenceIndex and
     * currentLoopIncrement currentSequenceIndex and currentLoopIncrement are
     * then updated with the new location marker data if a valid track is
     * present autoInc is a special incremental value that will force the
     * shifting of the entry selection if needed
     */
    protected PlayableItemInterface getPlaylistNextItem(final Channel channel, final TimetableSlot currentTimetableSlot, final YearMonthWeekDayHourMinuteSecondMillisecondInterface scheduledStartTime, PlaylistEntryLocation loc, byte autoInc) throws Exception {

        PlayableItemInterface track = null;

        if (loc == null) {
            loc = new PlaylistEntryLocation();
        }
        if (currentTimetableSlot != null) {
            logger.info("getPlaylistNextTrack(" + channel.getLabel() + "," + currentTimetableSlot + "," + scheduledStartTime + "," + loc + "," + autoInc + ")");

            //Track the next playlist slot position to play
            //Compute a new currentSequenceIndex with autoInc: helpful to pass entries that would otherwise loop for ever if e.g. the selection algorithm has depleted all possible tracks to play
            if (autoInc > 0 && loc.currentSequenceIndex != null) {
                loc.currentSequenceIndex += autoInc;
                loc.currentSequenceIndex %= currentTimetableSlot.getPlaylist().getPlaylistEntries().size();
                //If we switch to another entry then make sure the loop counter is also reinitialized
                loc.currentLoopCounter = null; //Will be automatically set to 1 in the next routine
            }

            long newLoopIncrement = 1;
            //If playlist markers are uninitialized then the new sequence index will be the first one available
            PlaylistEntry currentPlaylistEntry = null;
            for (Iterator<PlaylistEntry> i = currentTimetableSlot.getPlaylist().getPlaylistEntries().iterator(); i.hasNext();) {
                PlaylistEntry playlistEntry = i.next();
                if ((loc.currentSequenceIndex == null || loc.currentSequenceIndex < 0) || playlistEntry.getSequenceIndex() == loc.currentSequenceIndex) {
                    //ok we have our current playlist entry now, assume for now that the next playlistentry to play is the current one
                    currentPlaylistEntry = playlistEntry;
                    //We either select the first one available if the playlist entry marker was unspecified
                    //Or the right one if it was mentionned
                    //Check if a current loop increment has been specified and if it hasn't consumed all its loops yet
                    if (loc.currentLoopCounter == null) {
                        newLoopIncrement = 1;
                    } else {
                        if (playlistEntry.getLoop() == null || playlistEntry.getLoop() < 1 || loc.currentLoopCounter < playlistEntry.getLoop()) {
                            //It's an infinite loop or it's not out of bound yet, we can stay within this slot
                            newLoopIncrement = loc.currentLoopCounter + 1;
                        } else {
                            //We have used up all of the loop increments, switch to the next playlist entry if available
                            //Check if we're at the end of the playlist entries
                            if (!i.hasNext()) {
                                //Yes, get back to the beginning of the playlist entries
                                currentPlaylistEntry = currentTimetableSlot.getPlaylist().getPlaylistEntries().first();
                            } else {
                                currentPlaylistEntry = i.next();
                            }
                            //Reinit the loop increment loop since the entry is new
                            newLoopIncrement = 1;
                        }
                    }
                    break;
                }
            }

            //We've got a new playlist entry and a loop counter to guide us on what to play next
            if (currentPlaylistEntry != null) {

                //Check whether if we should queue a request, or just a simple track
                if (isRequestToQueue(channel, currentTimetableSlot, currentPlaylistEntry)) {
                    //Ok, we should queue a request now
                    track = getPlayableRequest(channel, currentTimetableSlot, scheduledStartTime);
                }

                //Fallback: queue a simple track
                if (track == null) {

                    PlaylistEntryVisitorTrackInterface visitor = new PlaylistEntryVisitorTrackInterface() {

                        private BroadcastableTrack track = null;
                        private long nextInternalSelectionIndex = 0;

                        @Override
                        public void visit(DynamicPlaylistEntry entry) throws Exception {
                            track = QueueScheduler.this.getPlayableTrack(channel, currentTimetableSlot, entry, scheduledStartTime, nextInternalSelectionIndex);
                        }

                        @Override
                        public void visit(StaticPlaylistEntry entry) throws Exception {
                            //staticplaylistentries are not constrained by "non-replayability" rules so just fetch the raw track as-is
                            track = entry.getTrack();
                        }

                        @Override
                        public BroadcastableTrack getTrack() {
                            return track;
                        }

                        @Override
                        public void setNextInternalSelectionIndex(long nextInternalSelectionIndex) {
                            this.nextInternalSelectionIndex = nextInternalSelectionIndex;
                        }
                    };
                    currentPlaylistEntry.acceptVisit(visitor);
                    //The loop increment is exactly the same as the internal selection index for OrderedSequenceRules, minus one
                    visitor.setNextInternalSelectionIndex(newLoopIncrement - 1);
                    track = visitor.getTrack();
                }

                //update the function parameters with the working values
                loc.currentSequenceIndex = currentPlaylistEntry.getSequenceIndex();
                loc.currentLoopCounter = newLoopIncrement;

                loc.currentFadeInAmount = currentPlaylistEntry.getFadeIn();

            }

        } else {
            //nullify the position markers if there's no playlist
            loc.currentSequenceIndex = null;
            loc.currentLoopCounter = null;

            loc.currentFadeInAmount = null;
        }
        return track;
    }

    protected boolean isRequestToQueue(Channel channel, TimetableSlot timetableSlot, PlaylistEntry currentPlaylistEntry) throws Exception {
        if (channel != null && currentPlaylistEntry != null) {

            //We must check:
            //-if the channel globa settings allow requests
            //-if the current entry accepts requests
            //-if the playlist maxUserRequestLimit() hasn't been reached yet
            //-if there are pending requests for this playlist
            if (channel.getMaxDailyRequestLimitPerUser() > 0 && currentPlaylistEntry.isRequestAllowed() && currentPlaylistEntry.getPlaylist().getMaxUserRequestLimit() > 0) {
                //Browse the queue, the stream item, and the archives, counting how many requests will/are/have been played
                //This number must be lower than maxUserRequestLimit()

                final FinalInteger numOfCurrentRequestsLeft = new FinalInteger();
                numOfCurrentRequestsLeft.value = currentPlaylistEntry.getPlaylist().getMaxUserRequestLimit();

                //First check what the 'now playing' item is
                StreamItem nowPlayingItem = channel.getStreamItem();
                //Hibernate may be a deal breaker, let's not try to compare proxies, just extract the id
                if (nowPlayingItem != null && nowPlayingItem.getTimetableSlot() != null && nowPlayingItem.getTimetableSlot().getRefID().equals(timetableSlot.getRefID())) {
                    //Now check if the two look identical
                    StreamItemVisitor streamItemVisitor = new StreamItemVisitor() {

                        @Override
                        public void visit(LiveNonQueueStreamItem entry) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(LiveQueueStreamItem entry) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(BlankQueueStreamItem entry) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(RequestQueueStreamItem entry) throws Exception {
                            numOfCurrentRequestsLeft.value--;
                        }
                    };
                    nowPlayingItem.acceptVisit(streamItemVisitor);
                }

                if (numOfCurrentRequestsLeft.value <= 0) {
                    return false;
                }

                //Then within the queue
                for (Iterator<SchedulableQueueItem> iterator = timetableSlot.getQueueItems().iterator(); iterator.hasNext();) {
                    final QueueItem currentQueueItem = iterator.next();

                    QueueItemVisitor queueItemVisitor = new QueueItemVisitor() {

                        @Override
                        public void visit(SimpleTrackQueueItem queueItem) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(LiveQueueItem queueItem) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(BlankQueueItem queueItem) throws Exception {
                            //Do nothing
                        }

                        @Override
                        public void visit(RequestQueueItem queueItem) throws Exception {
                            numOfCurrentRequestsLeft.value--;
                        }
                    };
                    currentQueueItem.acceptVisit(queueItemVisitor);
                    if (numOfCurrentRequestsLeft.value <= 0) {
                        return false;
                    }
                }

                //Then if you've reached there, check within the archives
                //The lack of backward iterator for sets is a real burden,
                //use a custom HQL request for this specific task: gather sorted records in small quantities, iterate through them, if there's no match repeat those steps
                return (browseArchivesForMaxRequests(numOfCurrentRequestsLeft, channel, timetableSlot).value > 0);
            }

        }
        return false;
    }

    protected FinalInteger browseArchivesForMaxRequests(FinalInteger numOfCurrentRequestsLeft, Channel channel, TimetableSlot timetableSlot) {
        final byte selectionSize = 6;
        long selectionIndex = 0;
        Collection<? extends Archive> archives;
        do {
            archives = archiveDao.getSelectedArchives(
                    channel.getLabel(),
                    selectionIndex,
                    selectionSize + selectionIndex - 1);

            //The collection should be sorted with freshest archives first
            if (archives != null) {
                for (Archive archive : archives) {

                    //if the archive doesn't belong to the same playlist then break
                    if (!archive.getTimetableSlot().equals(timetableSlot.getRefID())) {
                        return numOfCurrentRequestsLeft;
                    }

                    if (ProxiedClassUtil.castableAs(archive, RequestArchive.class)) {
                        numOfCurrentRequestsLeft.value--;
                    }

                    if (numOfCurrentRequestsLeft.value <= 0) {
                        return numOfCurrentRequestsLeft;
                    }

                }
            }
            selectionIndex += selectionSize;
            //Fetch the next batch of archives, unless there is no archive left to browse
        } while (archives != null && archives.size() == selectionSize);

        return numOfCurrentRequestsLeft;
    }

    /**
     * Returns a new next queue sequence index based on the given queue item
     * Only useful if the item is the last from the queue, the new sequence
     * index may differ from the one already registered for the next existing
     * item in the queue
     *
     * @param lastItemInQueue
     * @return
     */
    protected long getNewQueueSequenceIndex(QueueItem lastItemInQueue) {
        long newSequenceIndex;

        if (lastItemInQueue != null) {
            logger.info("getNewQueueSequenceIndex(" + lastItemInQueue.getRefID() + ")");

            long previousSequenceIndex = lastItemInQueue.getSequenceIndex();
            //Be sure no to assign a new sequence index that will overflow the long capacity
            if (previousSequenceIndex < Long.MAX_VALUE) {
                newSequenceIndex = previousSequenceIndex + 1;
            } else {
                //Otherwise re-number every item in the queue and reset their sequence item from zero
                long resetSequenceIndex = QUEUE_FIRST_SEQUENCE_INDEX;
                for (QueueItem item : lastItemInQueue.getChannel().getQueueItems()) {
                    item.setSequenceIndex(resetSequenceIndex++);
                }
                //Persist the changes
                channelDao.updateBatchQueueItems(lastItemInQueue.getChannel().getQueueItems());
                newSequenceIndex = resetSequenceIndex;
            }
        } else {
            //If lastItem is null then the queue has not been populated yet
            newSequenceIndex = QUEUE_FIRST_SEQUENCE_INDEX;
        }

        return newSequenceIndex;
    }

    /**
     * Make sure both no item in the queue and the 'now playing' item have
     * expired, i.e. their ending time is past now
     *
     * @param channel
     */
    @Override
    public void purgeObsoleteQueueItems(Channel channel) throws Exception {
        if (channel != null) {
            //First stream item
            StreamItem streamItem = channel.getStreamItem();
            boolean purgeQueue = true;
            if (streamItem != null) {
                if (getQueueItemEndTime(streamItem) == null) {
                    dropChannelStreamItem(streamItem);
                } else {
                    purgeQueue = false;
                }
            }
            if (purgeQueue) {
                //then queue items (if the stream item is off, then obviously the rest is obsolete)
                for (Iterator<QueueItem> i = channel.getQueueItems().iterator(); i.hasNext();) {
                    QueueItem queueItem = i.next();
                    //But don't unlink pooled items if unnecessary, the streamer may still need them!
                    if (!queueItem.isPooled() || getQueueItemEndTime(queueItem) == null) {
                        i.remove();
                        dropChannelQueueItem(queueItem);
                    } else {
                        //Don't go any further, let's try to keep the original scheduling after this point
                        break;
                    }
                }

            }

        }
    }

    public void purgeStreamItem(Channel channel) throws Exception {
        if (channel != null) {
            logger.info("purgeStreamItem(" + channel.getLabel() + ")");
            dropChannelStreamItem(channel.getStreamItem());
        }
    }

    @Override
    public void purgeQueueItems(Channel channel) throws Exception {
        if (channel != null) {
            logger.info("purgeQueueItems(" + channel.getLabel() + ")");
            dropChannelQueueItems(channel.getQueueItems());
        }
    }

    protected void purgeNonPooledQueueItems(Channel channel) throws Exception {
        if (channel != null) {
            logger.info("purgeNonPooledQueueItems(" + channel.getLabel() + ")");
            //then queue items
            for (Iterator<QueueItem> i = channel.getQueueItems().iterator(); i.hasNext();) {
                QueueItem queueItem = i.next();
                if (!queueItem.isPooled()) {
                    i.remove();
                    dropChannelQueueItem(queueItem);
                }
            }
        }
    }

    /**
     * Use the finite length of the last item now in the queue and its start
     * time to compute a likely start time for a new item
     *
     * @param channel
     * @return
     */
    protected static YearMonthWeekDayHourMinuteSecondMillisecondInterface getQueueItemEndTime(final GenericItemInterface item) throws Exception {
        if (item != null) {
            //Check its length
            long maxItemDuration = item.getDuration();
            //One last check, first see if the length delta is positive and then
            //be sure that the end time is not to be scheduled BEFORE now
            //In this case it most likely means the streamer couldn't play the last item and didn't send the appropriate notification to the application
            //Or previous queue items were automatically removed by a user if he edited a dependent object (timetable, playlist, etc)
            //Don't flush the queue just ignore this bogus time

            long newUnixEndTime = item.getStartTime().getUnixTime() + maxItemDuration;

            if (maxItemDuration > 0
                    && newUnixEndTime >= DateHelper.getCurrentTime()) {
                return new YearMonthWeekDayHourMinuteSecondMillisecond(
                        newUnixEndTime,
                        item.getChannel() != null
                        ? item.getChannel().getTimezone()
                        : null);
            }
        }

        //Something bad happens, time doesn't make sense or we don't know
        return null;

    }

    protected BroadcastableTrack getPlayableTrack(Channel channel, TimetableSlot timetableSlot, DynamicPlaylistEntry playlistEntry) throws Exception {
        return getPlayableTrack(channel, timetableSlot, playlistEntry, null, 0);
    }

    protected BroadcastableTrack getPlayableTrack(Channel channel, TimetableSlot timetableSlot, DynamicPlaylistEntry playlistEntry, YearMonthWeekDayHourMinuteSecondMillisecondInterface scheduledStartTime, long nextInternalSelectionIndex) throws Exception {
        BroadcastableTrack track = null;

        if (playlistEntry != null) {
            logger.info("getPlayableTrack(" + channel + "," + timetableSlot + "," + playlistEntry + "," + scheduledStartTime + "," + nextInternalSelectionIndex + ")");

            //Get a track based on Selection, SubSelection, and Filters.
            //Check if it's a valid track if it fullfills noReplayDelay and noReplayDelayInPlaylist constrints
            //Loop until a valid track is found
            //Just to be sure the while loop will end one way or another
            byte loopGuardrail = getWatchdogIterationMax();
            while (track == null && loopGuardrail > 0) {

                switch (playlistEntry.getSelection()) {
                    case random:
                        track = getRandomTrack(
                                playlistEntry.getPlaylist(),
                                playlistEntry.getSubSelection(),
                                playlistEntry.getFilters());
                        break;
                    case weightedRandom:
                        track = getWeightedRandomTrack(
                                playlistEntry.getPlaylist(),
                                playlistEntry.getSubSelection(),
                                playlistEntry.getFilters());
                        break;
                    case orderedSequence:
                        track = getOrderedSequenceTrack(
                                playlistEntry.getPlaylist(),
                                playlistEntry.getSubSelection(),
                                nextInternalSelectionIndex,
                                playlistEntry.getFilters());
                        break;
                }
                track = checkTrackValidity(track, channel, scheduledStartTime, timetableSlot, playlistEntry.isNoReplayInPlaylist(), playlistEntry.getNoReplayDelay());

                loopGuardrail--;
            }

        }
        if (track==null) logger.error("No track can be selected for broadcast: make sure your 'no replayability' rules are not too strict and that the streamer can technically play your content");
        return track;
    }

    /**
     * Check whether a new request can be queued for playing Must respect some
     * criterions and remain within some pre-defined quota limits Returns true
     * if you can queue this new request
     *
     * @param user
     * @param channel
     * @param song
     * @param message
     * @return
     */
    @Override
    public boolean checkNewRequestQueueable(String restrictedUserId, String channelId) throws Exception {
        if (restrictedUserId != null && channelId != null) {

            Channel channel = channelDao.getChannel(channelId);
            if (channel == null) {
                throw new NoEntityException();
            }

            Integer maxDailyLimitPerUser = channel.getMaxDailyRequestLimitPerUser();
            //no limit
            if (maxDailyLimitPerUser == null) {
                return true;
            }
            //Requests forbidden
            if (maxDailyLimitPerUser == 0) {
                throw new NoRequestAllowedException();
            }
            long lMaxDailyLimitPerUser = NumberUtil.safeLongToInt(maxDailyLimitPerUser);

            YearMonthWeekDayHourMinuteSecondMillisecondInterface yesterday = DateHelper.getCurrentTime(channel.getTimezone());
            yesterday.addDays(-1);

            //Otherwise browse the request queue, and the archives, and see
            Long registeredRequestsForToday = trackDao.getNumberOfRequestsForUserAfterTimeThreshold(channel.getLabel(), restrictedUserId, yesterday);
            if (registeredRequestsForToday != null) {
                lMaxDailyLimitPerUser -= registeredRequestsForToday;
            }

            if (lMaxDailyLimitPerUser <= 0) {
                return false;
            }

            Long registeredArchivedRequestsForToday = archiveDao.getNumberOfRequestsForUserAfterTimeThreshold(channel.getLabel(), restrictedUserId, yesterday);
            if (registeredArchivedRequestsForToday != null) {
                lMaxDailyLimitPerUser -= registeredArchivedRequestsForToday;
            }

            if (lMaxDailyLimitPerUser <= 0) {
                return false;
            }

            return true;
        }
        throw new NoEntityException();
    }

    @Override
    public boolean checkRequestDeletable(String restrictedUserId, String songId) throws Exception {
        if (restrictedUserId != null && songId != null) {

            Request request = trackDao.getUserRequest(restrictedUserId, songId);
            if (request != null) //Not queued, and not currently streaming either, we can delete it
            {
                return (request.getStreamItem() == null && request.getQueueItem() == null);
            }
        }
        throw new NoEntityException();
    }

    protected Request getPlayableRequest(Channel channel, TimetableSlot timetableSlot, YearMonthWeekDayHourMinuteSecondMillisecondInterface scheduledStartTime) throws Exception {
        Request request = null;

        if (channel != null && timetableSlot != null) {
            logger.info("getPlayableRequest(" + channel + "," + timetableSlot + "," + scheduledStartTime + ")");

            //Get a Request based on current channel and programme.
            //Check if it's a valid request if it fullfills noRequestReplayDelay and noRequestReplayDelayInPlaylist constraints
            //Loop until a valid Request is found
            //Just to be sure the while loop will end one way or another
            byte loopGuardrail = getWatchdogIterationMax();
            while (request == null && loopGuardrail > 0) {

                request = trackDao.getNextAvailableRequest(//
                        channel.getLabel(),//
                        timetableSlot.getProgramme().getLabel(),//
                        (byte) (getWatchdogIterationMax() - loopGuardrail));
                if (request == null) //No need to go any further, it means the request queue is empty for those criterions
                {
                    return null;
                }
                if (checkTrackValidity(//
                        request.getSong(),//
                        channel,//
                        scheduledStartTime,//
                        timetableSlot,//
                        timetableSlot.getPlaylist().isNoRequestReplayInPlaylist(),//
                        timetableSlot.getPlaylist().getNoRequestReplayDelay()) == null) {
                    request = null;
                }
                loopGuardrail--;
            }

        }

        return request;
    }

    protected BroadcastableTrack checkTrackValidity(BroadcastableTrack track, Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface scheduledStartTime, TimetableSlot timetableSlot, boolean noReplayInPlaylist, Long noReplayDelay) throws Exception {
        BroadcastableTrack validTrack = track;
        //Playability checks, i.e. ready + enabled + fit in playlist and programme no-repeat rules
        if (checkTrackNonReplayability(validTrack, channel, scheduledStartTime, timetableSlot, noReplayInPlaylist, noReplayDelay)) {
            validTrack = null;
        }
        //Check if compatible with the current streamer capabilities
        if (validTrack != null && validTrack.getTrackContainer() != null//
                //CHECK: deactivate min. size check, Liqudisoap appears to perfectly handle tiny tracks
                //&& (track.getTrackContainer().getDuration() * 1000) < channel.getStreamer().getMinPlayableDuration()//
                && (validTrack.getTrackContainer().getDuration() * 1000) < getMinPlayableDuration(channel)//
                ) {
            validTrack = null;
        }
        return validTrack;
    }

    /**
     * Returns true if the given track shouldn't be played based on
     * "non-replayability' rules defined in a playlist and programme
     *
     * @param track
     * @return
     */
    protected boolean checkTrackNonReplayability(BroadcastableTrack track, Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface scheduledStartTime, TimetableSlot timetableSlot, boolean noReplayInPlaylist, Long noReplayDelay) throws Exception {
        logger.info("checkTrackNonReplayability(" + track + "," + channel + "," + scheduledStartTime + "," + timetableSlot + "," + noReplayInPlaylist + "," + noReplayDelay + ")");

        if (track == null) {
            return true;
        }

        //Playability checks, i.e. ready + enabled + fit in playlist and programme no-repeat rules
        if (!track.isEnabled() || !track.isReady()) {
            return true;
        }

        //so far, the track is ready and enabled, that's a start
        //now see if "non-replayability" rules are enforced, and whether they apply
        //First if it's already been played within this playlist
        if (noReplayInPlaylist && channel != null) {
            //Browse the queue backward until the playlist is different and check if the track is not already present
            //Once, and iff, the queue has been thoroughly checked, now use the archives

            if (isTrackAlreadyPlayedInPlaylist(track, channel, timetableSlot)) {
                return true;
            }

        }
        //Then if last time it's been played
        if (noReplayDelay != null && channel != null && scheduledStartTime != null) {
            //Browse the queue backward until the time marker is not exceeded and check if the track is not already present
            //Once and if the queue has been thoroughly checked, now use the archives

            //getNoReplayDelay() is a threshold in minutes, compute a time limit with this attribute
            YearMonthWeekDayHourMinuteSecondMillisecondInterface timeThreshold = new YearMonthWeekDayHourMinuteSecondMillisecond(scheduledStartTime);
            timeThreshold.addMinutes(-noReplayDelay);

            if (isTrackAlreadyPlayedAfterDate(track, channel, timeThreshold)) {
                return true;
            }
        }

        //By default, the track is allowed to be played
        return false;
    }

    protected boolean isTrackAlreadyPlayedAfterDate(BroadcastableTrack track, Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface timeThreshold) throws Exception {
        // Browse the queue backward until the timeThreshold is not expired and check if the track is not already present
        // Once, and iff, the queue has been thoroughly checked, then use the archives

        if (channel != null && track != null && timeThreshold != null) {
            logger.info("isTrackAlreadyPlayedAfterDate(" + track.getRefID() + "," + channel.getLabel() + "," + timeThreshold + ")");

            PlayableItemIsSameTrackCheck playableItemIsSameTrackCheck = new PlayableItemIsSameTrackCheck();

            //First check what the 'now playing' item is
            StreamItem nowPlayingItem = channel.getStreamItem();
            if (nowPlayingItem != null) {
                //If the item started long before the time limit we're looking for then try no further and accept the replay
                if (nowPlayingItem.getStartTime().compareTo(timeThreshold) < 0) {
                    return false;
                }
                //Now check if the two look identical
                if (isSurfaceItemSimilar(track, nowPlayingItem)) {
                    return true;
                }
            }

            //Then within the queue
            //There's no backward iterator for sortedset: do it the dirty way and copy references into an arraylist first
            //It works relatively well if the queue remains small, don't try that with archives though
            ArrayList<QueueItem> _queue = new ArrayList<QueueItem>(channel.getQueueItems());
            for (ListIterator<QueueItem> iterator = _queue.listIterator(_queue.size()); iterator.hasPrevious();) {
                final QueueItem currentQueueItem = iterator.previous();

                //If the item from the queue happened before the specified time then break
                if (currentQueueItem.getStartTime().compareTo(timeThreshold) < 0) {
                    return false;
                }

                //Hibernate limitation: As usual hibernate proxies break even such an important featureas equals()
                //I'm too lazy for doing anything else than not using equals() but directly comparing the ids
                //TODO: Burn Hibernate to ashes
                //FIX: the usual visitor pattern
                //if (track.equals(currentQueueItem.getItem())) {
                if (currentQueueItem.getItem() != null) {
                    currentQueueItem.getItem().is(playableItemIsSameTrackCheck);
                    if (playableItemIsSameTrackCheck.isSame(track)) {
                        return true;
                    }
                }

            }
            //Then if you've reached there, check within the archives
            //The lack of backward iterator for sets is a real burden,
            //use a custom HQL request for this specific task: gather sorted records in small quantities, iterate through them, if there's no match repeat those steps
            return (browseArchivesForPlayableItemBeforeTimeThreshold(channel, timeThreshold, track) != null);

        }
        return false;
    }

    /**
     * Check whether the given track has already been played for this playlist
     * in the queue
     *
     * @param track
     * @param channel
     * @param playlist
     * @return
     */
    protected boolean isTrackAlreadyPlayedInPlaylist(BroadcastableTrack track, Channel channel, TimetableSlot timetableSlot) throws Exception {
        // Browse the queue backward until the playlist is different and check if the track is not already present
        // Once, and iff, the queue has been thoroughly checked, then use the archives

        if (channel != null && track != null && timetableSlot != null) {
            logger.info("isTrackAlreadyPlayedInPlaylist(" + track.getRefID() + "," + channel.getLabel() + "," + timetableSlot.getRefID() + ")");

            PlayableItemIsSameTrackCheck playableItemIsSameTrackCheck = new PlayableItemIsSameTrackCheck();

            //First check what the 'now playing' item is
            StreamItem nowPlayingItem = channel.getStreamItem();
            if (nowPlayingItem != null) {
                //If the item doesn't belong to the same timetable slot as the item to check then look no further and accept the replay
                //Hibernate may be a deal breaker, let's not try to compare proxies, just extract the id
                if (nowPlayingItem.getTimetableSlot() != null && !nowPlayingItem.getTimetableSlot().getRefID().equals(timetableSlot.getRefID())) {
                    logger.info("different timetable slot: break");
                    return false;
                }
                //Now check if the two look identical
                if (isSurfaceItemSimilar(track, nowPlayingItem)) {
                    logger.info("same item already played within range");
                    return true;
                }
            }

            //Then within the queue
            //There's no backward iterator for sortedset: do it the dirty way and copy references into an arraylist first
            //It works relatively well if the queue remains small, don't try that with archives though
            ArrayList<QueueItem> _queue = new ArrayList<QueueItem>(timetableSlot.getQueueItems());
            for (ListIterator<QueueItem> iterator = _queue.listIterator(_queue.size()); iterator.hasPrevious();) {
                final QueueItem currentQueueItem = iterator.previous();

                //Hibernate limitation: As usual hibernate proxies break even such an important featureas equals()
                //I'm too lazy for doing anything else than not using equals() but directly comparing the ids
                //TODO: Burn Hibernate to ashes
                //if (track.equals(currentQueueItem.getItem())) {
                if (currentQueueItem.getItem() != null) {
                    currentQueueItem.getItem().is(playableItemIsSameTrackCheck);
                    if (playableItemIsSameTrackCheck.isSame(track)) {
                        logger.info("same item already played within range");
                        return true;
                    }
                }

            }
            //Then if you've reached there, check within the archives
            //The lack of backward iterator for sets is a real burden,
            //use a custom HQL request for this specific task: gather sorted records in small quantities, iterate through them, if there's no match repeat those steps
            return (browseArchivesForPlayableItemInPlaylist(channel, timetableSlot, track) != null);

        }
        return false;
    }

    protected boolean isSurfaceItemSimilar(PlayableItemInterface item, Archive archive) throws Exception {
        logger.info("isSurfaceItemSimilar(" + item + "," + archive + ")");
        PlayableItemEqualityCheck playableItemVisitorEqualityCheck = new PlayableItemEqualityCheck();
        if (item != null) {
            item.is(playableItemVisitorEqualityCheck);
        }
        return playableItemVisitorEqualityCheck.isContainedIn(archive);
    }

    protected boolean isSurfaceItemSimilar(PlayableItemInterface item, StreamItem nowPlayingItem) throws Exception {
        logger.info("isSurfaceItemSimilar(" + item + "," + nowPlayingItem + ")");
        PlayableItemEqualityCheck playableItemVisitorEqualityCheck = new PlayableItemEqualityCheck();
        if (item != null) {
            item.is(playableItemVisitorEqualityCheck);
        }
        return playableItemVisitorEqualityCheck.isContainedIn(nowPlayingItem);
    }

    protected BroadcastableTrack getRandomTrack(Playlist playlist, SubSelection subSelection, Collection<Filter> filters) {

        BroadcastableTrack track = null;

        if (playlist != null && playlist.getProgramme() != null) {
            logger.info("getRandomTrack(" + playlist.getRefID() + "," + subSelection + ",multiple)");
            //Get the total number of available files and see if the selection end must be accordingly decreased
            //Pick a random index between the selection start and the selection end
            //Return a file with this index

            long forcedStart = (subSelection == null || subSelection.getStartIndex() == null) ? 0 : subSelection.getStartIndex();
            SORT forcedSort = SORT.rotationCountDesc;
            if (subSelection != null && subSelection.getSort() != null) {
                forcedSort = subSelection.getSort();
            }
            Long numOfAvailableTracks = trackDao.getNumberOfBroadcastableReadyAndEnabledTracksInRange(
                    playlist.getProgramme().getLabel(),
                    forcedSort,
                    forcedStart,
                    subSelection != null ? subSelection.getEndIndex() : null,
                    filters);
            if (numOfAvailableTracks != null) {
                long randomIndex = SortUtil.random(forcedStart, forcedStart + numOfAvailableTracks - 1);
                track = trackDao.getBroadcastableReadyAndEnabledTrackAtIndex(
                        playlist.getProgramme().getLabel(),
                        forcedSort,
                        randomIndex,
                        filters);
            }
        }

        return track;
    }

    protected BroadcastableTrack getWeightedRandomTrack(Playlist playlist, SubSelection subSelection, Collection<Filter> filters) {

        if (playlist != null && playlist.getProgramme() != null) {
            logger.info("getWeightedRandomTrack(" + playlist.getRefID() + "," + subSelection + ",multiple)");
            //Get the total number of available files and see if the selection end must be accordingly decreased
            //Draw an index between 1 and the sum of the ranks of the array
            long offset = 0;
            long forcedStart = (subSelection == null || subSelection.getStartIndex() == null) ? 0 : subSelection.getStartIndex();
            SORT forcedSort = SORT.rotationCountDesc;
            if (subSelection != null && subSelection.getSort() != null) {
                forcedSort = subSelection.getSort();
            }
            Long numOfAvailableTracks = trackDao.getNumberOfBroadcastableReadyAndEnabledTracksInRange(
                    playlist.getProgramme().getLabel(),
                    forcedSort,
                    forcedStart,
                    subSelection != null ? subSelection.getEndIndex() : null,
                    filters);
            if (numOfAvailableTracks != null) {

                //(numOfAvailableTracks * (numOfAvailableTracks + 1)) / 2 is just a simple shortcut to get the sum of the ranks of the array
                long randomIndex = SortUtil.random(1, (numOfAvailableTracks * (numOfAvailableTracks + 1)) / 2);
                for (long i = 1; i <= numOfAvailableTracks; i++) {
                    offset += i;
                    if (randomIndex <= offset) {
                        return trackDao.getBroadcastableReadyAndEnabledTrackAtIndex(
                                playlist.getProgramme().getLabel(),
                                forcedSort,
                                forcedStart + (numOfAvailableTracks - i),
                                filters);
                    }
                }
                //If we reach this section then we're out of bounds, don't do anything though and returns a null track
            }

        }

        return null;
    }

    protected BroadcastableTrack getOrderedSequenceTrack(Playlist playlist, SubSelection subSelection, long nextInternalSelectionIndex, Collection<Filter> filters) {

        BroadcastableTrack track = null;

        if (playlist != null && playlist.getProgramme() != null) {
            logger.info("getOrderedSequenceTrack(" + playlist.getRefID() + "," + subSelection + ",multiple)");
            //Get the total number of available files and see if the selection end must be accordingly decreased
            //Then adjust the currentInternalSelectionIndex so that it fits within those bounds (use a modulo to go back from start if needed)
            //Return the track at currentInternalSelectionIndex

            long forcedStart = (subSelection == null || subSelection.getStartIndex() == null) ? 0 : subSelection.getStartIndex();
            SORT forcedSort = SORT.rotationCountDesc;
            if (subSelection != null && subSelection.getSort() != null) {
                forcedSort = subSelection.getSort();
            }
            Long numOfAvailableTracks = trackDao.getNumberOfBroadcastableReadyAndEnabledTracksInRange(
                    playlist.getProgramme().getLabel(),
                    forcedSort,
                    forcedStart,
                    subSelection != null ? subSelection.getEndIndex() : null,
                    filters);
            if (numOfAvailableTracks != null) {

                long sequencedIndex = forcedStart + (nextInternalSelectionIndex % numOfAvailableTracks);
                track = trackDao.getBroadcastableReadyAndEnabledTrackAtIndex(
                        playlist.getProgramme().getLabel(),
                        forcedSort,
                        sequencedIndex,
                        filters);
            }
        }

        return track;
    }
}
