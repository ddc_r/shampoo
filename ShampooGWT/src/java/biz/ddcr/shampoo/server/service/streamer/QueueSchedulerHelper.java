/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.server.domain.archive.Archive;
import biz.ddcr.shampoo.server.domain.archive.ArchiveInterface;
import biz.ddcr.shampoo.server.domain.archive.ArchiveVisitor;
import biz.ddcr.shampoo.server.domain.archive.BroadcastableTrackArchive;
import biz.ddcr.shampoo.server.domain.archive.NonQueuedLiveArchive;
import biz.ddcr.shampoo.server.domain.archive.QueuedLiveArchive;
import biz.ddcr.shampoo.server.domain.archive.RequestArchive;
import biz.ddcr.shampoo.server.domain.playlist.Blank;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.playlist.PlaylistEntryVisitor;
import biz.ddcr.shampoo.server.domain.queue.PlayableItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItemVisitor;
import biz.ddcr.shampoo.server.domain.streamer.BlankQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueVisitor;
import biz.ddcr.shampoo.server.domain.streamer.RequestQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.SimpleTrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItem;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemInterface;
import biz.ddcr.shampoo.server.domain.streamer.StreamItemVisitor;
import biz.ddcr.shampoo.server.domain.timetable.DailyTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.MonthlyTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlotVisitor;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlotWrapper;
import biz.ddcr.shampoo.server.domain.timetable.UniqueTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.WeeklyTimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.YearlyTimetableSlot;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.helper.YearInterface;
import biz.ddcr.shampoo.server.helper.YearMonthInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayInterface;
import biz.ddcr.shampoo.server.helper.YearMonthWeekInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class QueueSchedulerHelper {

    /** Internal working interface for visitor patterns**/
    public interface TimetableSlotVisitorValueInterface extends TimetableSlotVisitor {

        public byte getValue();
    }

    /** Internal working interface for visitor patterns**/
    public interface TimetableSlotVisitorAbsoluteStartTimeInterface extends TimetableSlotVisitor {

        public YearMonthWeekDayHourMinuteSecondMillisecondInterface getAbsoluteStartTime();
    }

    /** Internal working interface for visitor patterns**/
    public interface TimetableSlotVisitorEqualityCheckInterface extends TimetableSlotVisitor {

        public boolean isEqual(TimetableSlot otherObj) throws Exception;

        public TimetableSlot getDeproxiedObject();
    }

    /** Internal working interface for visitor patterns**/
    public interface ArchiveVisitorEqualityCheckInterface extends ArchiveVisitor {

        public boolean contains(BroadcastableTrack item);

        public boolean contains(Live live);
    }

    /** Internal working interface for visitor patterns**/
    public interface StreamItemVisitorEqualityCheckInterface extends StreamItemVisitor {

        public boolean contains(BroadcastableTrack item);

        public boolean contains(Live live);
    }

    /** Internal working interface for visitor patterns**/
    public interface PlayableItemEqualityCheckInterface extends PlayableItemVisitor {

        public boolean isContainedIn(ArchiveInterface archive) throws Exception;
    }

    /** Internal working interface for visitor patterns**/
    public interface QueueItemVisitorLengthInterface extends QueueItemVisitor {

        public long getLengthInMilliseconds();
    }

    /** Internal working interface for visitor patterns**/
    public interface PlaylistEntryVisitorTrackInterface extends PlaylistEntryVisitor {

        public void setNextInternalSelectionIndex(long nextInternalSelectionIndex);

        public BroadcastableTrack getTrack();
    }

    /** Internal working interface for visitor patterns**/
    public interface StreamItemVisitorArchiveConversionInterface extends StreamItemVisitor {

        public Archive getArchive();
    }

    /** Internal working interface for visitor patterns**/
    public interface NonQueueItemVisitorStreamItemConversionInterface extends NonPersistableNonQueueVisitor {

        public StreamItem getStreamItem();
    }

    /** Internal working interface for visitor patterns**/
    public interface QueueItemVisitorStreamItemConversionInterface extends QueueItemVisitor {

        public StreamItem getStreamItem();
    }

    public interface TimetableSlotWrapperVisitorInterface extends TimetableSlotVisitor {

        public TimetableSlotWrapper get();
    }

    /** Internal working interface for visitor patterns**/
    public interface PlayableItemIsSameTrackCheckInterface extends PlayableItemVisitor {

        public boolean isSame(BroadcastableTrack track) throws Exception;
    }

    public interface QueueItemFromPlayableItemVisitorInterface extends PlayableItemVisitor {

        public QueueItem get() throws Exception;
    }

    //Daily == 0
    //Weekly == 1
    //Monthly == 2
    //Yearly == 3
    //Unique == 4
    public static class TimetableSlotVisitorValue implements TimetableSlotVisitorValueInterface {

        private byte value = -1;
        private TimetableSlot obj;

        @Override
        public void visit(UniqueTimetableSlot timetableSlot) throws Exception {
            value = 4;
            obj = timetableSlot;
        }

        @Override
        public void visit(DailyTimetableSlot timetableSlot) throws Exception {
            value = 0;
            obj = timetableSlot;
        }

        @Override
        public void visit(WeeklyTimetableSlot timetableSlot) throws Exception {
            value = 1;
            obj = timetableSlot;
        }

        @Override
        public void visit(MonthlyTimetableSlot timetableSlot) throws Exception {
            value = 2;
            obj = timetableSlot;
        }

        @Override
        public void visit(YearlyTimetableSlot timetableSlot) throws Exception {
            value = 3;
            obj = timetableSlot;
        }

        @Override
        public byte getValue() {
            return value;
        }

        public TimetableSlot getDeproxiedObject() {
            return obj;
        }
    };

    public static class TimetableSlotVisitorEqualityCheck implements TimetableSlotVisitorEqualityCheckInterface {

        private TimetableSlot obj = null;

        @Override
        public void visit(UniqueTimetableSlot timetableSlot) throws Exception {
            obj = timetableSlot;
        }

        @Override
        public void visit(DailyTimetableSlot timetableSlot) throws Exception {
            obj = timetableSlot;
        }

        @Override
        public void visit(WeeklyTimetableSlot timetableSlot) throws Exception {
            obj = timetableSlot;
        }

        @Override
        public void visit(MonthlyTimetableSlot timetableSlot) throws Exception {
            obj = timetableSlot;
        }

        @Override
        public void visit(YearlyTimetableSlot timetableSlot) throws Exception {
            obj = timetableSlot;
        }

        @Override
        public boolean isEqual(TimetableSlot otherObj) throws Exception {
            //Make sure both objects are deproxified from Hibernate
            TimetableSlotVisitorEqualityCheck visitor = new TimetableSlotVisitorEqualityCheck();
            otherObj.acceptVisit(visitor);
            return obj.equals(visitor.getDeproxiedObject());
        }

        @Override
        public TimetableSlot getDeproxiedObject() {
            return obj;
        }
    };

    /**
     * Compute the absolute start time of a slot that is active at the given time
     */
    public static class TimetableSlotVisitorAbsoluteStartTimeAroundRelativeNow implements TimetableSlotVisitorAbsoluteStartTimeInterface {

        private YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeTime = null;
        private YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime = null;

        public TimetableSlotVisitorAbsoluteStartTimeAroundRelativeNow(YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeTime) {
            this.relativeTime = relativeTime;
        }

        @Override
        public void visit(UniqueTimetableSlot timetableSlot) throws Exception {

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0 && timetableSlot.getEndTime().compareTo(relativeTime) >= 0) {
                absoluteStartTime = timetableSlot.getStartTime();
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(DailyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInDayPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();
            long thisEndMillisecondInPeriod = timetableSlot.getEndMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                if (thisStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                    //This slot doesn't span
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod && relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeTime);
                        absoluteStartTime.addDays(-1);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }

        }

        @Override
        public void visit(WeeklyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInWeekPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();
            long thisEndMillisecondInPeriod = timetableSlot.getEndMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                if (thisStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                    //This slot doesn't span
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod && relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeTime);
                        absoluteStartTime.addWeeks(-1);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(MonthlyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInMonthPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();
            long thisEndMillisecondInPeriod = timetableSlot.getEndMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                if (thisStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                    //This slot doesn't span
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod && relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeTime);
                        absoluteStartTime.addMonths(-1);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(YearlyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInYearPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();
            long thisEndMillisecondInPeriod = timetableSlot.getEndMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                if (thisStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                    //This slot doesn't span
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod && relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (relativeStartMillisecondInPeriod <= thisEndMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeTime);
                        absoluteStartTime.addYears(-1);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public YearMonthWeekDayHourMinuteSecondMillisecondInterface getAbsoluteStartTime() {
            return absoluteStartTime;
        }
    };

    /**
     * Compute the absolute start time of a slot that actually starts with the given time interval
     */
    public static class TimetableSlotVisitorAbsoluteStartTimeInRelativeInterval implements TimetableSlotVisitorAbsoluteStartTimeInterface {

        private YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeLowerTime = null;
        private YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeUpperTime = null;
        private YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime = null;

        public TimetableSlotVisitorAbsoluteStartTimeInRelativeInterval(YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeLowerTime, YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeUpperTime) {
            this.relativeLowerTime = relativeLowerTime;
            this.relativeUpperTime = relativeUpperTime;
        }

        @Override
        public void visit(UniqueTimetableSlot timetableSlot) throws Exception {

            if (timetableSlot.getStartTime().compareTo(relativeUpperTime) <= 0 && timetableSlot.getStartTime().compareTo(relativeLowerTime) >= 0) {
                absoluteStartTime = timetableSlot.getStartTime();
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(DailyTimetableSlot timetableSlot) throws Exception {
            long relativeLowerMillisecondInPeriod = relativeLowerTime.getMillisecondInDayPeriod();
            long relativeUpperMillisecondInPeriod = relativeUpperTime.getMillisecondInDayPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeUpperTime) <= 0) {

                if (relativeLowerMillisecondInPeriod < relativeUpperMillisecondInPeriod) {
                    //This interval doesn't span
                    if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod && thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeUpperTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }

        }

        @Override
        public void visit(WeeklyTimetableSlot timetableSlot) throws Exception {
            long relativeLowerMillisecondInPeriod = relativeLowerTime.getMillisecondInWeekPeriod();
            long relativeUpperMillisecondInPeriod = relativeUpperTime.getMillisecondInWeekPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeUpperTime) <= 0) {

                if (relativeLowerMillisecondInPeriod < relativeUpperMillisecondInPeriod) {
                    //This interval doesn't span
                    if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod && thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeUpperTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(MonthlyTimetableSlot timetableSlot) throws Exception {
            long relativeLowerMillisecondInPeriod = relativeLowerTime.getMillisecondInMonthPeriod();
            long relativeUpperMillisecondInPeriod = relativeUpperTime.getMillisecondInMonthPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeUpperTime) <= 0) {

                if (relativeLowerMillisecondInPeriod < relativeUpperMillisecondInPeriod) {
                    //This interval doesn't span
                    if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod && thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeUpperTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(YearlyTimetableSlot timetableSlot) throws Exception {
            long relativeLowerMillisecondInPeriod = relativeLowerTime.getMillisecondInYearPeriod();
            long relativeUpperMillisecondInPeriod = relativeUpperTime.getMillisecondInYearPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeUpperTime) <= 0) {

                if (relativeLowerMillisecondInPeriod < relativeUpperMillisecondInPeriod) {
                    //This interval doesn't span
                    if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod && thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                } else {
                    if (thisStartMillisecondInPeriod >= relativeLowerMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeLowerTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else if (thisStartMillisecondInPeriod <= relativeUpperMillisecondInPeriod) {
                        absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeUpperTime);
                        absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);
                    } else {
                        absoluteStartTime = null;
                    }
                }
                if (absoluteStartTime != null) {
                    YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                    if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                        absoluteStartTime = null;
                    }
                }
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public YearMonthWeekDayHourMinuteSecondMillisecondInterface getAbsoluteStartTime() {
            return absoluteStartTime;
        }
    };

    public static class TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow implements TimetableSlotVisitorAbsoluteStartTimeInterface {

        private YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeTime = null;
        private YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime = null;

        public TimetableSlotVisitorNextAbsoluteStartTimeFromRelativeNow(YearMonthWeekDayHourMinuteSecondMillisecondInterface relativeTime) {
            this.relativeTime = relativeTime;
        }

        @Override
        public void visit(UniqueTimetableSlot timetableSlot) throws Exception {

            //It doesn't work for non-recurring slots, obviously
            if (timetableSlot.getStartTime().compareTo(relativeTime) >= 0) {
                absoluteStartTime = timetableSlot.getStartTime();
            } else {
                absoluteStartTime = null;
            }
        }

        @Override
        public void visit(DailyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInDayPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) relativeTime);

                if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                    absoluteStartTime.addDays(1);
                }
            } else {
                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekDayInterface) timetableSlot.getStartTime());
            }
            absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);

            if (absoluteStartTime != null) {
                YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                    absoluteStartTime = null;
                }
            }

        }

        @Override
        public void visit(WeeklyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInWeekPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) relativeTime);

                if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                    absoluteStartTime.addWeeks(1);
                }

            } else {
                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthWeekInterface) timetableSlot.getStartTime());
            }
            absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);

            if (absoluteStartTime != null) {
                YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                    absoluteStartTime = null;
                }
            }

        }

        @Override
        public void visit(MonthlyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInMonthPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) relativeTime);

                if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                    absoluteStartTime.addMonths(1);
                }
            } else {
                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearMonthInterface) timetableSlot.getStartTime());
            }
            absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);

            if (absoluteStartTime != null) {
                YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                    absoluteStartTime = null;
                }
            }

        }

        @Override
        public void visit(YearlyTimetableSlot timetableSlot) throws Exception {
            long relativeStartMillisecondInPeriod = relativeTime.getMillisecondInYearPeriod();
            long thisStartMillisecondInPeriod = timetableSlot.getStartMillisecondFromPeriod();

            if (timetableSlot.getStartTime().compareTo(relativeTime) <= 0) {

                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) relativeTime);

                if (relativeStartMillisecondInPeriod >= thisStartMillisecondInPeriod) {
                    absoluteStartTime.addYears(1);
                }
            } else {
                absoluteStartTime = new YearMonthWeekDayHourMinuteSecondMillisecond((YearInterface) timetableSlot.getStartTime());
            }
            absoluteStartTime.addMilliseconds(thisStartMillisecondInPeriod);

            if (absoluteStartTime != null) {
                YearMonthWeekDayInterface decom_ = timetableSlot.getDecommissioningTime();
                if (decom_ != null && absoluteStartTime.compareTo(decom_) >= 0) {
                    absoluteStartTime = null;
                }
            }

        }

        @Override
        public YearMonthWeekDayHourMinuteSecondMillisecondInterface getAbsoluteStartTime() {
            return absoluteStartTime;
        }
    };

    public static class StreamItemVisitorEqualityCheck implements StreamItemVisitorEqualityCheckInterface {

        private String trackId = null;
        private String liveId = null;

        @Override
        public boolean contains(BroadcastableTrack track) {
            return (track != null && trackId != null && trackId.equals(track.getRefID()));
        }

        @Override
        public boolean contains(Live live) {
            return (live != null && liveId != null && liveId.equals(live.getBroadcaster()+live.getLogin()));
        }

        @Override
        public void visit(LiveNonQueueStreamItem entry) throws Exception {
            if (entry.getTimetableSlot()!=null && entry.getTimetableSlot().getPlaylist()!=null) {
                Live live = entry.getTimetableSlot().getPlaylist().getLive();
                liveId = live.getBroadcaster()+live.getLogin();
            }  else
                liveId = null;
        }

        @Override
        public void visit(LiveQueueStreamItem entry) throws Exception {
            if (entry.getTimetableSlot()!=null && entry.getTimetableSlot().getPlaylist()!=null) {
                Live live = entry.getTimetableSlot().getPlaylist().getLive();
                liveId = live.getBroadcaster()+live.getLogin();
            }  else
                liveId = null;
        }

        @Override
        public void visit(SimpleTrackQueueStreamItem entry) throws Exception {
            trackId = entry.getTrack()!=null ? entry.getTrack().getRefID() : null;
        }

        @Override
        public void visit(BlankQueueStreamItem entry) throws Exception {
            //Do nothing, blank are not susceptible to no-replayability rules
        }

        @Override
        public void visit(RequestQueueStreamItem entry) throws Exception {
            //same as broadcast tracks, since it's basically just a track with a few additional tags
            trackId = entry.getTrack()!=null ? entry.getTrack().getRefID() : null;
        }

    }

    public static class ArchiveVisitorEqualityCheck implements ArchiveVisitorEqualityCheckInterface {

        private String trackId = null;
        private String broadcasterCaption = null;

        @Override
        public boolean contains(BroadcastableTrack track) {
            boolean _i = (track != null && trackId != null && track.getRefID().equals(trackId));
            return _i;
        }

        @Override
        public boolean contains(Live live) {
            boolean _i = (live != null && broadcasterCaption != null && live.getBroadcaster().equals(broadcasterCaption));
            return _i;
        }

        @Override
        public void visit(NonQueuedLiveArchive entry) throws Exception {
            broadcasterCaption = entry.getBroadcasterCaption();
        }

        @Override
        public void visit(QueuedLiveArchive entry) throws Exception {
            broadcasterCaption = entry.getBroadcasterCaption();
        }

        @Override
        public void visit(BroadcastableTrackArchive entry) throws Exception {
            trackId = entry.getTrack();
        }

        @Override
        public void visit(RequestArchive entry) throws Exception {
            //same as broadcast tracks, since it's basically just a track with a few additional tags
            trackId = entry.getTrack();
        }
    };

    public static class PlayableItemEqualityCheck implements PlayableItemEqualityCheckInterface {

        private BroadcastableTrack track = null;
        private Live live = null;

        @Override
        public boolean isContainedIn(ArchiveInterface archive) throws Exception {
            ArchiveVisitorEqualityCheck archiveItemVisitorEqualityCheck = new ArchiveVisitorEqualityCheck();
            if (archive != null) {
                archive.acceptVisit(archiveItemVisitorEqualityCheck);
            }
            if (track != null) {
                return archiveItemVisitorEqualityCheck.contains(track);
            } else if (live != null) {
                return archiveItemVisitorEqualityCheck.contains(live);
            } else //Fallback
            {
                return false;
            }
        }
        public boolean isContainedIn(StreamItemInterface nowPlayingItem) throws Exception {
            StreamItemVisitorEqualityCheck streamItemVisitorEqualityCheck = new StreamItemVisitorEqualityCheck();
            if (nowPlayingItem != null) {
                nowPlayingItem.acceptVisit(streamItemVisitorEqualityCheck);
            }
            if (track != null) {
                return streamItemVisitorEqualityCheck.contains(track);
            } else if (live != null) {
                return streamItemVisitorEqualityCheck.contains(live);
            } else //Fallback
            {
                return false;
            }
        }

        @Override
        public void visit(BroadcastableAdvert track) throws Exception {
            this.track = track;
        }

        @Override
        public void visit(BroadcastableJingle track) throws Exception {
            this.track = track;
        }

        @Override
        public void visit(BroadcastableSong track) throws Exception {
            this.track = track;
        }

        @Override
        public void visit(Live live) throws Exception {
            this.live = live;
        }

        @Override
        public void visit(Blank blank) throws Exception {
            //Does nothing, off-air events are not archived
        }

        @Override
        public void visit(Request request) throws Exception {
            //same as broadcast tracks, since it's basically just a track with a few additional tags
             this.track = request.getSong();
        }
    };

    /** dummy procedure for fooling hibernate proxies **/
    public static boolean areEqual(TimetableSlot obj1, TimetableSlot obj2) throws Exception {
        if (obj1 != null && obj2 != null) {

            TimetableSlotVisitorEqualityCheck visitor = new TimetableSlotVisitorEqualityCheck();

            obj1.acceptVisit(visitor);

            return visitor.isEqual(obj2);
        } else {
            return obj1 == null && obj2 == null ? true : obj2 == null;
        }
    }

    ;

    public static class TimetableSlotWrapperBuilder implements TimetableSlotWrapperVisitorInterface {

        private YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime;
        private TimetableSlotWrapper wrapper;

        public TimetableSlotWrapperBuilder(YearMonthWeekDayHourMinuteSecondMillisecondInterface absoluteStartTime) {
            this.absoluteStartTime = absoluteStartTime;
        }

        @Override
        public TimetableSlotWrapper get() {
            return wrapper;
        }

        @Override
        public void visit(UniqueTimetableSlot timetableSlot) throws Exception {
            wrapper = new TimetableSlotWrapper<UniqueTimetableSlot>(timetableSlot, absoluteStartTime);
        }

        @Override
        public void visit(DailyTimetableSlot timetableSlot) throws Exception {
            wrapper = new TimetableSlotWrapper<DailyTimetableSlot>(timetableSlot, absoluteStartTime);
        }

        @Override
        public void visit(WeeklyTimetableSlot timetableSlot) throws Exception {
            wrapper = new TimetableSlotWrapper<WeeklyTimetableSlot>(timetableSlot, absoluteStartTime);
        }

        @Override
        public void visit(MonthlyTimetableSlot timetableSlot) throws Exception {
            wrapper = new TimetableSlotWrapper<MonthlyTimetableSlot>(timetableSlot, absoluteStartTime);
        }

        @Override
        public void visit(YearlyTimetableSlot timetableSlot) throws Exception {
            wrapper = new TimetableSlotWrapper<YearlyTimetableSlot>(timetableSlot, absoluteStartTime);
        }
    };

    public static class PlayableItemIsSameTrackCheck implements PlayableItemIsSameTrackCheckInterface {

        private BroadcastableTrack otherTrack = null;

        @Override
        public boolean isSame(BroadcastableTrack track) throws Exception {
            if (otherTrack == null) {
                return track == null;
            } else {
                //Hibernate limitation: As usual hibernate proxies break even such an important featureas equals()
                //I'm too lazy for doing anything else than not using equals() but directly comparing the ids
                //TODO: Burn Hibernate to ashes
                //return otherTrack.equals(track);
                if (track == null) {
                    return false;
                } else {
                    return otherTrack.getRefID().equals(track.getRefID());
                }
            }
        }

        @Override
        public void visit(BroadcastableAdvert track) throws Exception {
            otherTrack = track;
        }

        @Override
        public void visit(BroadcastableJingle track) throws Exception {
            otherTrack = track;
        }

        @Override
        public void visit(BroadcastableSong track) throws Exception {
            otherTrack = track;
        }

        @Override
        public void visit(Live live) throws Exception {
            otherTrack = null;
        }

        @Override
        public void visit(Blank blank) throws Exception {
            otherTrack = null;
        }

        @Override
        public void visit(Request request) throws Exception {
            //same as broadcast tracks, since it's basically just a track with a few additional tags
            otherTrack = request.getSong();
        }
    }

    private QueueSchedulerHelper() {
    }
}
