/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlotWrapper;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecondInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface QueueSchedulerInterface {

    public void purgeObsoleteQueueItems(Channel channel) throws Exception;
    public void purgeQueueItems(Channel channel) throws Exception;

    public void populateQueue(Channel channel) throws Exception;
    public QueueItem getPooledQueueItem(String itemId) throws Exception;
    public QueueItem popAvailableQueueItem(Channel channel) throws Exception;

    public LiveNonPersistableNonQueueItem getBroadcastableLiveAt(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public LiveNonPersistableNonQueueItem getBroadcastableLiveFor(TimetableSlot timetableSlot) throws Exception;
    public void setPooledQueueItemOnAir(QueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public void setPooledQueueItemOffAir(QueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public void setPooledQueueReferenceOffAir(Channel channel, String queueId, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public void setNonQueueItemOnAir(NonPersistableNonQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public void setNonQueueItemOffAir(NonPersistableNonQueueItem item, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public void setNonQueueReferenceOffAir(Channel channel, String timetableSlotId, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;

    public boolean setChannelReserved(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;
    public boolean setChannelFree(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface time) throws Exception;

    //Public helper
    public TimetableSlotWrapper getBroadcastTimetableSlotPlayingOn(Channel channel, YearMonthWeekDayHourMinuteSecondMillisecondInterface fuzzyTime) throws Exception;
    public boolean checkNewRequestQueueable(String restrictedUserId, String channelId) throws Exception;
    public boolean checkRequestDeletable(String restrictedUserId, String songId) throws Exception;
}
