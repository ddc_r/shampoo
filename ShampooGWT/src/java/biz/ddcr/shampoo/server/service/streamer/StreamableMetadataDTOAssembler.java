/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Live;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.queue.BlankQueueItem;
import biz.ddcr.shampoo.server.domain.queue.LiveQueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.queue.QueueItemVisitor;
import biz.ddcr.shampoo.server.domain.queue.RequestQueueItem;
import biz.ddcr.shampoo.server.domain.queue.SimpleTrackQueueItem;
import biz.ddcr.shampoo.server.domain.queue.TrackQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueVisitor;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableBlankQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableChannelGenericMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableLiveGenericItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableLiveQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.SimpleStreamableTrackQueueItemMetadata;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableChannelMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableItemMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableLiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableQueueItemMetadataInterface;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.CommonTagInterface;
import biz.ddcr.shampoo.server.domain.track.PEGIRating;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataCollection;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;

/**
 *
 * @author okay_awright
 **/
public class StreamableMetadataDTOAssembler extends GenericService implements StreamableMetadataDTOAssemblerInterface {

    private interface FloatTrackVisitor extends TrackVisitor {
        public Float get();
    }

    private TrackManagerInterface trackManager;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    private interface QueueItemConverterVisitor extends QueueItemVisitor {

        public StreamableQueueItemMetadataInterface convert();
    }

    private interface NonPersistentNonQueueItemConverterVisitor extends NonPersistableNonQueueVisitor {

        public StreamableItemMetadataInterface convert();
    }

    @Override
    public StreamableQueueItemMetadataInterface toDTO(final String channelId, final String privateKey, QueueItem queueItem) throws Exception {
        if (queueItem != null) {
            QueueItemConverterVisitor queueItemConverterVisitor = new QueueItemConverterVisitor() {

                StreamableQueueItemMetadataInterface streamableItemMetadata;

                @Override
                public StreamableQueueItemMetadataInterface convert() {
                    return streamableItemMetadata;
                }

                @Override
                public void visit(SimpleTrackQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(channelId, privateKey, queueItem);
                }

                @Override
                public void visit(LiveQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(channelId, privateKey, queueItem);
                }

                @Override
                public void visit(BlankQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(channelId, privateKey, queueItem);
                }

                @Override
                public void visit(RequestQueueItem queueItem) throws Exception {
                    streamableItemMetadata = toDTO(channelId, privateKey, queueItem);
                }
            };
            queueItem.acceptVisit(queueItemConverterVisitor);
            return queueItemConverterVisitor.convert();
        }
        return null;
    }

    protected SimpleStreamableTrackQueueItemMetadata toDTO(String channelId, String privateKey, TrackQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        SimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = new SimpleStreamableTrackQueueItemMetadata();

        //Fill in the main attributes
        simpleStreamableTrackMetadata.setQueueID(queueItem.getRefID());
        //Do only fill in the the likely duration if it's any different than the track full length
        if (queueItem.getNaturalDuration() > queueItem.getLikelyDuration()) {
            simpleStreamableTrackMetadata.setAuthorizedPlayingDuration(queueItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        }
        simpleStreamableTrackMetadata.setScheduledStartTime(queueItem.getStartTime().getUnixTime());
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getChannel() != null) {
            Channel c = queueItem.getTimetableSlot().getChannel();
            simpleStreamableTrackMetadata.setChannelLabel(c.getLabel());
            simpleStreamableTrackMetadata.setChannelDescription(c.getDescription());
            simpleStreamableTrackMetadata.setChannelTag(c.getTag());
            simpleStreamableTrackMetadata.setChannelURL(c.getUrl());
        }
        if (queueItem.getItem() != null) {
            simpleStreamableTrackMetadata.setItemURLEndpoint(getDatastoreService().getPrivateWSTrackDownloadURLEndpoint(channelId, privateKey, queueItem.getItem().getRefID()));
        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = queueItem.getTimetableSlot().getPlaylist();
            simpleStreamableTrackMetadata.setPlaylistID(playlist.getRefID());
            simpleStreamableTrackMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableTrackMetadata.setPlaylistLabel(playlist.getLabel());
        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableTrackMetadata.setProgrammeLabel(queueItem.getTimetableSlot().getProgramme().getLabel());
            if (queueItem.getItem() != null) {
                try {
                    simpleStreamableTrackMetadata.setProgrammeRotationCount(queueItem.getItem().getRotationCount(queueItem.getTimetableSlot().getProgramme()));
                } catch (NullPointerException e) {
                    logger.error("Cannot access the specific programme rotation counter", e);
                    simpleStreamableTrackMetadata.setProgrammeRotationCount(0L);
                    //And then silently drop
                }
            }
            simpleStreamableTrackMetadata.setFriendlyCaptionPattern(queueItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (queueItem.getItem() != null) {
            BroadcastableTrack track = queueItem.getItem();
            simpleStreamableTrackMetadata.setTrackID(track.getRefID());
            if (ProxiedClassUtil.castableAs(track, BroadcastableSong.class)) {
                simpleStreamableTrackMetadata.setTrackType("song");
            } else if (ProxiedClassUtil.castableAs(track, BroadcastableJingle.class)) {
                simpleStreamableTrackMetadata.setTrackType("jingle");
            } else if (ProxiedClassUtil.castableAs(track, BroadcastableAdvert.class)) {
                simpleStreamableTrackMetadata.setTrackType("advert");
            }
            FloatTrackVisitor visitor = new FloatTrackVisitor() {

                private Float f;
                @Override
                public Float get() {
                    return f;
                }

                @Override
                public void visit(PendingAdvert track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(PendingJingle track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(PendingSong track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BroadcastableAdvert track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BroadcastableJingle track) throws Exception {
                    //Do nothing
                }

                @Override
                public void visit(BroadcastableSong track) throws Exception {
                    f = track.getAverageVote();
                }
            };
            track.acceptVisit(visitor);
            Float f = visitor.get();
            if (f!=null) simpleStreamableTrackMetadata.setAverageVote(f);
        }

        //Fill in the tag set now
        CommonTagInterface tagSet = null;
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null && queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            tagSet = queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
        } else if (queueItem.getItem() != null) {
            tagSet = queueItem.getItem();
        }
        if (tagSet != null) {
            simpleStreamableTrackMetadata.setTrackPublisher(tagSet.getPublisher());
            simpleStreamableTrackMetadata.setCopyright(tagSet.getCopyright());
            if (tagSet.getRating() != null) {
                simpleStreamableTrackMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableTrackMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableTrackMetadata.setTag(tagSet.getTag());
            simpleStreamableTrackMetadata.setTrackAlbum(tagSet.getAlbum());
            simpleStreamableTrackMetadata.setTrackTitle(tagSet.getTitle());
            simpleStreamableTrackMetadata.setTrackAuthor(tagSet.getAuthor());
            simpleStreamableTrackMetadata.setTrackDateOfRelease(tagSet.getDateOfRelease());
            simpleStreamableTrackMetadata.setTrackDescription(tagSet.getDescription());
            simpleStreamableTrackMetadata.setTrackGenre(tagSet.getGenre());
        }

        //Cover art
        //Use the playlist flyer if there's no cover art for the track
        if (queueItem.getItem()!=null && queueItem.getItem().getCoverArtContainer()!=null) {
            simpleStreamableTrackMetadata.setPicture(true);
            PictureFileInfo fileInfo = queueItem.getItem().getCoverArtContainer();
            simpleStreamableTrackMetadata.setPictureURLEndpoint(getDatastoreService().getPrivateWSTrackPictureDownloadURLEndpoint(channelId, privateKey, queueItem.getItem().getRefID()));
            simpleStreamableTrackMetadata.setPictureFormat(fileInfo.getFormat().name());
            simpleStreamableTrackMetadata.setPictureSize(fileInfo.getSize());
        } else if (queueItem.getTimetableSlot()!=null && queueItem.getTimetableSlot().getPlaylist()!=null && queueItem.getTimetableSlot().getPlaylist().getCoverArtContainer()!=null) {
            simpleStreamableTrackMetadata.setPicture(true);
            PictureFileInfo fileInfo = queueItem.getTimetableSlot().getPlaylist().getCoverArtContainer();
            simpleStreamableTrackMetadata.setPictureURLEndpoint(getDatastoreService().getPrivateWSPlaylistPictureDownloadURLEndpoint(channelId, privateKey, queueItem.getTimetableSlot().getPlaylist().getRefID()));
            simpleStreamableTrackMetadata.setPictureFormat(fileInfo.getFormat().name());
            simpleStreamableTrackMetadata.setPictureSize(fileInfo.getSize());
        } else {
            simpleStreamableTrackMetadata.setPicture(false);
        }

        //Technical data
        if (queueItem.getItem() != null) {
            if (queueItem.getItem().getTrackContainer() != null) {
                AudioFileInfo fileInfo = queueItem.getItem().getTrackContainer();
                simpleStreamableTrackMetadata.setTrackBitrate(fileInfo.getBitrate());
                simpleStreamableTrackMetadata.setTrackChannels(fileInfo.getChannels());
                simpleStreamableTrackMetadata.setTrackDuration(fileInfo.getDuration());
                simpleStreamableTrackMetadata.setTrackFormat(fileInfo.getFormat().name());
                simpleStreamableTrackMetadata.setTrackSamplerate(fileInfo.getSamplerate());
                simpleStreamableTrackMetadata.setTrackSize(fileInfo.getSize());
                simpleStreamableTrackMetadata.setTrackVBR(fileInfo.isVbr());
            }
        }
        return simpleStreamableTrackMetadata;
    }

    protected SimpleStreamableTrackQueueItemMetadata toDTO(String channelId, String privateKey, SimpleTrackQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        SimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = toDTO(channelId, privateKey, (TrackQueueItem) queueItem);

        if (simpleStreamableTrackMetadata != null) {
            //Request flags
            simpleStreamableTrackMetadata.setRequested(false);
            simpleStreamableTrackMetadata.setRequestAuthor(null);
            simpleStreamableTrackMetadata.setRequestMessage(null);
        }

        return simpleStreamableTrackMetadata;
    }

    protected SimpleStreamableTrackQueueItemMetadata toDTO(String channelId, String privateKey, RequestQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        SimpleStreamableTrackQueueItemMetadata simpleStreamableTrackMetadata = toDTO(channelId, privateKey, (TrackQueueItem) queueItem);

        if (simpleStreamableTrackMetadata != null) {
            //Request flags
            simpleStreamableTrackMetadata.setRequested(queueItem.getRequest() != null);
            if (queueItem.getRequest() != null && queueItem.getRequest().getRequester() != null) {
                simpleStreamableTrackMetadata.setRequestAuthor(queueItem.getRequest().getRequester().getUsername());
            }
            if (queueItem.getRequest() != null) {
                simpleStreamableTrackMetadata.setRequestMessage(queueItem.getRequest().getMessage());
            }
        }

        return simpleStreamableTrackMetadata;
    }

    protected SimpleStreamableLiveQueueItemMetadata toDTO(String channelId, String privateKey, LiveQueueItem queueItem) throws Exception {
        if (queueItem == null) {
            return null;
        }
        SimpleStreamableLiveQueueItemMetadata simpleStreamableLiveMetadata = new SimpleStreamableLiveQueueItemMetadata();

        //Fill in the main attributes
        simpleStreamableLiveMetadata.setQueueID(queueItem.getRefID());
        //Do only fill in the the likely duration if it's any different than the track full length
        if (queueItem.getNaturalDuration() > queueItem.getLikelyDuration()) {
            simpleStreamableLiveMetadata.setAuthorizedPlayingDuration(queueItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        }
        simpleStreamableLiveMetadata.setScheduledStartTime(queueItem.getStartTime().getUnixTime());
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getChannel() != null) {
            Channel c = queueItem.getTimetableSlot().getChannel();
            simpleStreamableLiveMetadata.setChannelLabel(c.getLabel());
            simpleStreamableLiveMetadata.setChannelDescription(c.getDescription());
            simpleStreamableLiveMetadata.setChannelTag(c.getTag());
            simpleStreamableLiveMetadata.setChannelURL(c.getUrl());
        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = queueItem.getTimetableSlot().getPlaylist();
            simpleStreamableLiveMetadata.setPlaylistID(playlist.getRefID());
            simpleStreamableLiveMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableLiveMetadata.setPlaylistLabel(playlist.getLabel());

            //Cover art
            if (playlist.getCoverArtContainer()!=null) {
                simpleStreamableLiveMetadata.setPicture(true);
                PictureFileInfo fileInfo = playlist.getCoverArtContainer();
                simpleStreamableLiveMetadata.setPictureURLEndpoint(getDatastoreService().getPrivateWSPlaylistPictureDownloadURLEndpoint(channelId, privateKey, playlist.getRefID()));
                simpleStreamableLiveMetadata.setPictureFormat(fileInfo.getFormat().name());
                simpleStreamableLiveMetadata.setPictureSize(fileInfo.getSize());
            } else {
                simpleStreamableLiveMetadata.setPicture(false);
            }

        }
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableLiveMetadata.setProgrammeLabel(queueItem.getTimetableSlot().getProgramme().getLabel());
            simpleStreamableLiveMetadata.setFriendlyCaptionPattern(queueItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (queueItem.getItem() != null) {
            Live live = queueItem.getLive();
            simpleStreamableLiveMetadata.setLiveBroadcaster(live.getBroadcaster());
            simpleStreamableLiveMetadata.setLiveLogin(live.getLogin());
            simpleStreamableLiveMetadata.setLivePassword(live.getPassword());
        }

        //URI
        final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channelId);
        if (streamerState!=null && streamerState.getQueueableLiveURI()!=null)
            simpleStreamableLiveMetadata.setItemURLEndpointPattern(streamerState.getQueueableLiveURI());
        
        //Fill in the tag set now
        if (queueItem.getTimetableSlot() != null && queueItem.getTimetableSlot().getPlaylist() != null && queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            CommonTagInterface tagSet = queueItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
            if (tagSet.getRating() != null) {
                simpleStreamableLiveMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableLiveMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableLiveMetadata.setLiveGenre(tagSet.getGenre());
        }

        return simpleStreamableLiveMetadata;
    }

    protected SimpleStreamableBlankQueueItemMetadata toDTO(String channelId, String privateKey, BlankQueueItem queueItem) {
        if (queueItem == null) {
            return null;
        }
        SimpleStreamableBlankQueueItemMetadata simpleStreamableBlankMetadata = new SimpleStreamableBlankQueueItemMetadata();

        //Fill in the main attributes
        simpleStreamableBlankMetadata.setQueueID(queueItem.getRefID());
        //Do only fill in the the likely duration if it's any different than the track full length
        if (queueItem.getNaturalDuration() > queueItem.getLikelyDuration()) {
            simpleStreamableBlankMetadata.setAuthorizedPlayingDuration(queueItem.getLikelyDuration() / 1000f); ////Convert from milliseconds to seconds
        }
        simpleStreamableBlankMetadata.setScheduledStartTime(queueItem.getStartTime().getUnixTime());
        Channel c = queueItem.getChannel();
        simpleStreamableBlankMetadata.setChannelLabel(c.getLabel());
        simpleStreamableBlankMetadata.setChannelDescription(c.getDescription());
        simpleStreamableBlankMetadata.setChannelTag(c.getTag());
        simpleStreamableBlankMetadata.setChannelURL(c.getUrl());
        //Provide a default metaInfoPattern, since this item is not linked to any programme
        simpleStreamableBlankMetadata.setFriendlyCaptionPattern("${itemTitle}");

        //URI
        final DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channelId);
        if (streamerState!=null && streamerState.getQueueableBlankURI()!=null)
        simpleStreamableBlankMetadata.setItemURLEndpointPattern(streamerState.getQueueableBlankURI());
        
        return simpleStreamableBlankMetadata;
    }

    @Override
    public StreamableItemMetadataInterface toDTO(final String channelId, final String privateKey, NonPersistableNonQueueItem miscItem) throws Exception {
        if (miscItem != null) {
            NonPersistentNonQueueItemConverterVisitor miscQueueItemConverterVisitor = new NonPersistentNonQueueItemConverterVisitor() {

                StreamableItemMetadataInterface streamableItemMetadata;

                @Override
                public StreamableItemMetadataInterface convert() {
                    return streamableItemMetadata;
                }

                @Override
                public void visit(LiveNonPersistableNonQueueItem nonQueueItem) throws Exception {
                    streamableItemMetadata = toDTO(channelId, privateKey, nonQueueItem);
                }
            };
            miscItem.acceptVisit(miscQueueItemConverterVisitor);
            return miscQueueItemConverterVisitor.convert();
        }
        return null;
    }

    @Override
    public StreamableLiveMetadataInterface toDTO(String channelId, String privateKey, LiveNonPersistableNonQueueItem liveItem) throws Exception {
        if (liveItem == null) {
            return null;
        }
        SimpleStreamableLiveGenericItemMetadata simpleStreamableLiveMetadata = new SimpleStreamableLiveGenericItemMetadata();

        //Fill in the main attributes
        simpleStreamableLiveMetadata.setStartTime(liveItem.getStartTime().getUnixTime());
        simpleStreamableLiveMetadata.setEndTime(liveItem.getStartTime().getUnixTime() + liveItem.getDuration());
        if (liveItem.getTimetableSlot() != null) {
            simpleStreamableLiveMetadata.setTimetableSlotID(liveItem.getTimetableSlot().getRefID());
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getChannel() != null) {
            Channel c = liveItem.getTimetableSlot().getChannel();
            simpleStreamableLiveMetadata.setChannelLabel(c.getLabel());
            simpleStreamableLiveMetadata.setChannelDescription(c.getDescription());
            simpleStreamableLiveMetadata.setChannelTag(c.getTag());
            simpleStreamableLiveMetadata.setChannelURL(c.getUrl());
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getPlaylist() != null) {
            Playlist playlist = liveItem.getTimetableSlot().getPlaylist();
            simpleStreamableLiveMetadata.setPlaylistID(playlist.getRefID());
            simpleStreamableLiveMetadata.setPlaylistDescription(playlist.getDescription());
            simpleStreamableLiveMetadata.setPlaylistLabel(playlist.getLabel());

            //Cover art
            if (playlist.getCoverArtContainer()!=null) {
                simpleStreamableLiveMetadata.setPicture(true);
                PictureFileInfo fileInfo = playlist.getCoverArtContainer();
                simpleStreamableLiveMetadata.setPictureURLEndpoint(getDatastoreService().getPrivateWSPlaylistPictureDownloadURLEndpoint(channelId, privateKey, playlist.getRefID()));
                simpleStreamableLiveMetadata.setPictureFormat(fileInfo.getFormat().name());
                simpleStreamableLiveMetadata.setPictureSize(fileInfo.getSize());
            } else {
                simpleStreamableLiveMetadata.setPicture(false);
            }
        }
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getProgramme() != null) {
            simpleStreamableLiveMetadata.setProgrammeLabel(liveItem.getTimetableSlot().getProgramme().getLabel());
            simpleStreamableLiveMetadata.setFriendlyCaptionPattern(liveItem.getTimetableSlot().getProgramme().getMetaInfoPattern());
        }

        if (liveItem.getLive() != null) {
            Live live = liveItem.getLive();
            simpleStreamableLiveMetadata.setLiveBroadcaster(live.getBroadcaster());
            simpleStreamableLiveMetadata.setLiveLogin(live.getLogin());
            simpleStreamableLiveMetadata.setLivePassword(live.getPassword());
        }

        //Fill in the tag set now
        if (liveItem.getTimetableSlot() != null && liveItem.getTimetableSlot().getPlaylist() != null && liveItem.getTimetableSlot().getPlaylist().getGlobalTagSet() != null) {
            CommonTagInterface tagSet = liveItem.getTimetableSlot().getPlaylist().getGlobalTagSet();
            if (tagSet.getRating() != null) {
                simpleStreamableLiveMetadata.setPEGIRatingAge((byte) tagSet.getRating().getAge().getAge());
                simpleStreamableLiveMetadata.setPEGIRatingFeatures(toStringList(tagSet.getRating()));
            }
            simpleStreamableLiveMetadata.setLiveGenre(tagSet.getGenre());

        }

        return simpleStreamableLiveMetadata;
    }

    @Override
    public StreamableChannelMetadataInterface toDTO(Channel channel) {
        if (channel == null) {
            return null;
        }
        SimpleStreamableChannelGenericMetadata simpleStreamableChannelMetadata = new SimpleStreamableChannelGenericMetadata();

        //Fill in the main attributes
        simpleStreamableChannelMetadata.setChannelLabel(channel.getLabel());
        simpleStreamableChannelMetadata.setChannelDescription(channel.getDescription());
        simpleStreamableChannelMetadata.setChannelTag(channel.getTag());
        simpleStreamableChannelMetadata.setChannelURL(channel.getUrl());

        return simpleStreamableChannelMetadata;
    }

    protected String[] toStringList(PEGIRating rating) {
        if (rating != null) {

            ArrayList<String> list = new ArrayList<String>();
            if (rating.getDiscrimination()) {
                list.add("discrimination");
            }
            if (rating.getDrugs()) {
                list.add("drugs");
            }
            if (rating.getFear()) {
                list.add("fear");
            }
            if (rating.getProfanity()) {
                list.add("profanity");
            }
            if (rating.getSex()) {
                list.add("sex");
            }
            if (rating.getViolence()) {
                list.add("violence");
            }

            if (!list.isEmpty()) {
                return list.toArray(new String[list.size()]);
            }
        }
        return null;
    }

    @Override
    public SerializableMetadataCollection<StreamableChannelMetadataInterface> toDTO(String privateKey, Collection<Channel> channels) throws Exception {
        //LikedHashSet gathers the best of Set features (no null, no duplicates) and the best of List features (fixed order) at the cost of some memory overhead
        LinkedHashSet<StreamableChannelMetadataInterface> channelMetadata = new LinkedHashSet<StreamableChannelMetadataInterface>();
        for (Channel channel : channels) {
            channelMetadata.add(toDTO(channel));
        }
        return SerializableMetadataCollection.wrap(channelMetadata);
    }    
    
}
