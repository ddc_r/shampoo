/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.NonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableChannelMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableItemMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableLiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableQueueItemMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.SerializableMetadataCollection;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface StreamableMetadataDTOAssemblerInterface {
    
    //Domain to DTO conversion
    public StreamableQueueItemMetadataInterface toDTO(final String channelId, final String privateKey, QueueItem queueItem) throws Exception;
    public StreamableItemMetadataInterface toDTO(final String channelId, final String privateKey, NonPersistableNonQueueItem miscItem) throws Exception;
    public StreamableLiveMetadataInterface toDTO(final String channelId, final String privateKey, LiveNonPersistableNonQueueItem miscItem) throws Exception;
    public StreamableChannelMetadataInterface toDTO(Channel channel) throws Exception;
    
    public SerializableMetadataCollection<StreamableChannelMetadataInterface> toDTO(final String privateKey, final Collection<Channel> miscItems) throws Exception;
    
}
