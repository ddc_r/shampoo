/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.queue.QueueItem;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.domain.streamer.LiveNonPersistableNonQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableChannelMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableLiveMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableQueueItemMetadataInterface;
import biz.ddcr.shampoo.server.domain.streamer.notification.StreamerStatusNotification;
import biz.ddcr.shampoo.server.domain.streamer.notification.StreamerStatusNotification.STREAMER_STATUS_OPERATION;
import biz.ddcr.shampoo.server.domain.timetable.TimetableSlot;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainer;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import static biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper.getSeatNumber;
import static biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper.getStreamerData;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class StreamerFacadeService extends GenericService implements StreamerFacadeServiceInterface {

    private ChannelManagerInterface channelManager;
    private SecurityManagerInterface securityManager;
    private QueueSchedulerInterface queueScheduler;
    private TrackManagerInterface trackManager;
    private ProgrammeManagerInterface programmeManager;
    private StreamableMetadataDTOAssemblerInterface streamableMetadataDTOAssembler;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public StreamableMetadataDTOAssemblerInterface getStreamableMetadataDTOAssembler() {
        return streamableMetadataDTOAssembler;
    }

    public void setStreamableMetadataDTOAssembler(StreamableMetadataDTOAssemblerInterface streamableMetadataDTOAssembler) {
        this.streamableMetadataDTOAssembler = streamableMetadataDTOAssembler;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public QueueSchedulerInterface getQueueScheduler() {
        return queueScheduler;
    }

    public void setQueueScheduler(QueueSchedulerInterface queueScheduler) {
        this.queueScheduler = queueScheduler;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    /**
     * Is this seat already allocated by another streamer?
     *
     * @param channelId
     * @param protectionKey
     * @return
     */
    protected boolean checkAccessProtectedSeat(long seatNumber, String protectionKey) {
        return (protectionKey == null || protectionKey.equals(ChannelStreamerConnectionStatusHelper.getProtectionKey(seatNumber)));
    }

    @Override
    public PictureStream getSecuredCoverArtStreamFromDataStore(long seatNumber, String masterKey, String protectionKey, final String trackId, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {
            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            Track track = getTrackManager().getTrack(trackId);
            return getDatastoreService().getPictureStreamFromDataStore(track);

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public PictureStream getSecuredFlyerStreamFromDataStore(long seatNumber, String masterKey, String protectionKey, final String playlistId, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {
            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            Playlist playlist = getProgrammeManager().getPlaylist(playlistId);
            return getDatastoreService().getPictureStreamFromDataStore(playlist);

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public TrackStream getSecuredTrackStreamFromDataStore(long seatNumber, String masterKey, String protectionKey, final String trackId, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {
            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            Track track = getTrackManager().getTrack(trackId);
            return getDatastoreService().getAudioStreamFromDataStore(track);

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setChannelReserved(long seatNumber, final String masterKey, final String protectionKey, final String channelId, final Long time, DynamicStreamerData configuration) throws Exception {
        if (channelId == null || channelId.length() == 0) {
            throw new NoEntityException();
        }

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey)) {
            Channel channel = getChannelManager().getChannel(channelId);
            //Merge streamer configuration, if any
            if (ChannelStreamerConnectionStatusHelper.addStreamerChannelStateConnection(seatNumber, channelId, protectionKey, configuration)) {
                //the streamer is alive and well
                ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

                YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
                getQueueScheduler().setChannelReserved(channel, effectiveTime);

                //Warn every responsible user that the streamer is now online and ready to kick
                notify(STREAMER_STATUS_OPERATION.channel_reserved, channel);
            } else {
                throw new IllegalAccessException("Cannot create a connection between a streamer and a seat");
            }
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setChannelFree(long seatNumber, final String masterKey, final String protectionKey, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);
            //Merge streamer configuration, if any
            if (ChannelStreamerConnectionStatusHelper.deleteStreamerChannelStateConnection(seatNumber)) {
                //the streamer is dead
                //No more heartbeat

                YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
                getQueueScheduler().setChannelFree(channel, effectiveTime);

                //Warn every responsible user that the streamer is now offline and deceased
                notify(STREAMER_STATUS_OPERATION.channel_free, channel);
            } else {
                throw new IllegalAccessException("Cannot destroy a connection between a streamer and a seat");
            }
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setChannelOnAir(long seatNumber, final String masterKey, final String protectionKey, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);
            
            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);
            //the channel is now on
            ChannelStreamerConnectionStatusHelper.refreshStreamingState(seatNumber, true);

            //Warn every responsible user that the streamer is now online and ready to kick
            notify(STREAMER_STATUS_OPERATION.channel_onair, channel);
        } else {
            throw new AccessDeniedException();
        }
    }    
    
    @Override
    public void setChannelOffAir(long seatNumber, final String masterKey, final String protectionKey, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);
            
            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);
            //the channel is now off
            ChannelStreamerConnectionStatusHelper.refreshStreamingState(seatNumber, false);

            //Warn every responsible user that the streamer is now offline and deceased
            notify(STREAMER_STATUS_OPERATION.channel_offair, channel);
        } else {
            throw new AccessDeniedException();
        }
    }    
    
    @Override
    public void setHeartbeat(long seatNumber, final String masterKey, final String protectionKey, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setLiveOnAir(long seatNumber, final String masterKey, final String protectionKey, final String timetableSlotId, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            TimetableSlot timetableSlot = getChannelManager().getTimetableSlot(timetableSlotId);
            LiveNonPersistableNonQueueItem liveItem = getQueueScheduler().getBroadcastableLiveFor(timetableSlot);
            YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
            if (liveItem != null) {
                liveItem.setStartTime(effectiveTime);
            }
            getQueueScheduler().setNonQueueItemOnAir(liveItem, effectiveTime);

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setLiveOffAir(long seatNumber, final String masterKey, final String protectionKey, final String timetableSlotId, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            TimetableSlot timetableSlot = getChannelManager().getTimetableSlot(timetableSlotId);
            YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
            try {
                LiveNonPersistableNonQueueItem liveItem = getQueueScheduler().getBroadcastableLiveFor(timetableSlot);
                if (liveItem != null) {
                    liveItem.setEndTime(effectiveTime);
                }
                getQueueScheduler().setNonQueueItemOffAir(liveItem, effectiveTime);
            } catch (NoEntityException e) {
                //Don't worry, it only means that the item has already been scrapped off the timetable
                getQueueScheduler().setNonQueueReferenceOffAir(channel, timetableSlotId, effectiveTime);
            }

        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setTrackOnAir(long seatNumber, final String masterKey, final String protectionKey, final String queueItemId, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            QueueItem queueItem = getQueueScheduler().getPooledQueueItem(queueItemId);
            YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
            getQueueScheduler().setPooledQueueItemOnAir(queueItem, effectiveTime);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public void setTrackOffAir(long seatNumber, final String masterKey, final String protectionKey, final String queueItemId, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
            try {
                QueueItem queueItem = getQueueScheduler().getPooledQueueItem(queueItemId);
                getQueueScheduler().setPooledQueueItemOffAir(queueItem, effectiveTime);
            } catch (NoEntityException e) {
                //Don't worry, it only means that the itemhas already been scrapped off the queue
                getQueueScheduler().setPooledQueueReferenceOffAir(channel, queueItemId, effectiveTime);
            }

        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     *
     * Fetch the broadcastable live metadata at the specified time, if any if
     * time is null then the time of call is used
     *
     * @param channelId
     * @param masterKey
     * @param startTime
     * @return
     * @throws Exception
     */
    @Override
    public GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> fetchLiveAt(long seatNumber, final String masterKey, final String protectionKey, final Long time, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate        
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            YearMonthWeekDayHourMinuteSecondMillisecond effectiveTime = (time == null ? DateHelper.getCurrentTime(channel.getTimezone()) : new YearMonthWeekDayHourMinuteSecondMillisecond(time, channel.getTimezone()));
            LiveNonPersistableNonQueueItem liveItem = getQueueScheduler().getBroadcastableLiveAt(channel, effectiveTime);

            //if item is null then we've got no current live broadcast
            //TODO see if raising an exception is better than filling in a SimpleStreamableMetadataContainer with no StreamableMetadata
            DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
            return new GenericStreamableMetadataContainer<StreamableLiveMetadataInterface>(
                    streamerState != null ? streamerState.getMetadataFormat() : null,
                    streamableMetadataDTOAssembler.toDTO(channelId, masterKey, liveItem));

        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * Pop the next item from the channel queue
     *
     * @param channelId
     * @param masterKey
     * @return
     * @throws Exception
     */
    @Override
    public GenericStreamableMetadataContainerInterface<StreamableQueueItemMetadataInterface> popNextItem(long seatNumber, final String masterKey, final String protectionKey, DynamicStreamerData configuration) throws Exception {

        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {

            final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
            if (channelId == null || channelId.length() == 0) {
                throw new NoEntityException();
            }
            final Channel channel = getChannelManager().getChannel(channelId);

            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            //then return the first available item from the queue
            QueueItem item = getQueueScheduler().popAvailableQueueItem(channel);
            //if item is null then we've got a problem here, otherwise an exception would have been already raised
            //TODO see if raising an exception is better than filling in a SimpleStreamableMetadataContainer with no StreamableMetadata
            DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
            return new GenericStreamableMetadataContainer<StreamableQueueItemMetadataInterface>(
                    streamerState != null ? streamerState.getMetadataFormat() : null,
                    streamableMetadataDTOAssembler.toDTO(channelId, masterKey, item));

        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * Get the current channel metadata
     *
     * @param channelId
     * @param masterKey
     * @return
     * @throws Exception
     */
    @Override
    public GenericStreamableMetadataContainerInterface<StreamableChannelMetadataInterface> fetchChannel(long seatNumber, String masterKey, String protectionKey, String channelId, DynamicStreamerData configuration) throws Exception {
        if (channelId == null || channelId.length() == 0) {
            throw new NoEntityException();
        }

        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey) && checkAccessProtectedSeat(seatNumber, protectionKey)) {
            Channel channel = getChannelManager().getChannel(channelId);
            //Merge streamer configuration, if any
            ChannelStreamerConnectionStatusHelper.updateStreamerState(seatNumber, configuration);
            //the streamer is alive and well
            ChannelStreamerConnectionStatusHelper.refreshHeartbeat(seatNumber);

            //if item is null then we've got a problem here, otherwise an exception would have been already raised
            //TODO see if raising an exception is better than filling in a SimpleStreamableMetadataContainer with no StreamableMetadata
            DynamicStreamerData streamerState = ChannelStreamerConnectionStatusHelper.getStreamerData(channel.getLabel());
            return new GenericStreamableMetadataContainer<StreamableChannelMetadataInterface>(
                    streamerState != null ? streamerState.getMetadataFormat() : null,
                    streamableMetadataDTOAssembler.toDTO(channel));

        } else {
            throw new AccessDeniedException();
        }

    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableChannelMetadataInterface>> fetchAvailableChannels(long seatNumber, String masterKey, DynamicStreamerData configuration) throws Exception {
        //Check first if the call is legitimate
        if (getSecurityManager().checkAccessPrivateResource(masterKey)) {

            Collection<Channel> channels = new HashSet<Channel>();

            //Check whether there's no same seat already allocated
            if (getStreamerData(seatNumber) == null) {
                //TODO see if raising an exception is better than filling in a SimpleStreamableMetadataContainer with no StreamableMetadata
                Collection<Channel> allChannelsForSeat = getChannelManager().getChannelsForSeatNumber(seatNumber);
                //No channel pre-allocated for this seat? Select unallocated channels then
                if (allChannelsForSeat!=null && allChannelsForSeat.isEmpty())
                    allChannelsForSeat = getChannelManager().getChannelsForDynamicSeats();
                //Remove from the list channels that are already reserved
                if (allChannelsForSeat != null) {
                    for (Channel channelForSeat : allChannelsForSeat) {
                        //Check whether there's no same seat already allocated
                        if (getSeatNumber(channelForSeat.getLabel()) == null) {
                            channels.add(channelForSeat);
                        }
                    }
                }
            }

            return new GenericStreamableMetadataContainer<SerializableIterableMetadataInterface<StreamableChannelMetadataInterface>>(
                    //Since the streamer configuration cannot be saved yet, try to guees it from any passed information
                    configuration != null ? configuration.getMetadataFormat() : null,
                    streamableMetadataDTOAssembler.toDTO(masterKey, channels));
        } else {
            throw new AccessDeniedException();
        }

    }

    @Override
    public void synchronousUnsecuredPurgeObsoleteQueueItems(long seatNumber) throws Exception {
        final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
        if (channelId != null) {
            getQueueScheduler().purgeObsoleteQueueItems(getChannelManager().getChannel(channelId));
        }
    }

    @Override
    public void synchronousUnsecuredPopulateQueue(long seatNumber) throws Exception {
        final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
        if (channelId != null) {
            getQueueScheduler().populateQueue(getChannelManager().getChannel(channelId));
        }
    }

    protected void notify(STREAMER_STATUS_OPERATION operation, Channel channel) throws Exception {
        notify(operation, channel, operation.getRecipients(getSecurityManager(), channel));
    }

    protected void notify(STREAMER_STATUS_OPERATION operation, Channel channel, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && recipients != null) {
            Collection<StreamerStatusNotification> notifications = new HashSet<StreamerStatusNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new StreamerStatusNotification(operation, channel, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
