/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.streamer;

import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.*;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TrackStream;



/**
 *
 * @author okay_awright
 **/
public interface StreamerFacadeServiceInterface {

    public void setChannelReserved(long seatNumber, String masterKey, String protectionKey, String channelId, Long time, DynamicStreamerData configuration) throws Exception;
    public void setChannelFree(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    public void setChannelOnAir(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    public void setChannelOffAir(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    public void setHeartbeat(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    public void setLiveOnAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, Long time, DynamicStreamerData configuration) throws Exception;
    public void setLiveOffAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, Long time, DynamicStreamerData configuration) throws Exception;
    public void setTrackOnAir(long seatNumber, String masterKey, String protectionKey, String queueItemId, Long time, DynamicStreamerData configuration) throws Exception;
    public void setTrackOffAir(long seatNumber, String masterKey, String protectionKey, String queueItemId, Long time, DynamicStreamerData configuration) throws Exception;
    public GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> fetchLiveAt(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    public GenericStreamableMetadataContainerInterface<StreamableQueueItemMetadataInterface> popNextItem(long seatNumber, String masterKey, String protectionKey, DynamicStreamerData configuration) throws Exception;
    public GenericStreamableMetadataContainerInterface<StreamableChannelMetadataInterface> fetchChannel(long seatNumber, String masterKey, String protectionKey, String channelId, DynamicStreamerData configuration) throws Exception;
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableChannelMetadataInterface>> fetchAvailableChannels(long seatNumber, String masterKey, DynamicStreamerData configuration) throws Exception;
    
    public TrackStream getSecuredTrackStreamFromDataStore(long seatNumber, String masterKey, String protectionKey, String trackId, DynamicStreamerData configuration) throws Exception;
    public PictureStream getSecuredCoverArtStreamFromDataStore(long seatNumber, String masterKey, String protectionKey, String trackId, DynamicStreamerData configuration) throws Exception;
    public PictureStream getSecuredFlyerStreamFromDataStore(long seatNumber, String masterKey, String protectionKey, String playlistId, DynamicStreamerData configuration) throws Exception;
    
    //Helpers for asynchronous tasks
    public void synchronousUnsecuredPurgeObsoleteQueueItems(long seatNumber) throws Exception;
    public void synchronousUnsecuredPopulateQueue(long seatNumber) throws Exception;

}
