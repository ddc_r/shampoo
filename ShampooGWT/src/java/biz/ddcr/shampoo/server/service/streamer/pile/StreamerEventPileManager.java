/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer.pile;

import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.helper.PilePoolEvent;
import biz.ddcr.shampoo.server.service.helper.PilePoolManager;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool.RunnableStreamerEvent;
import java.util.concurrent.BlockingQueue;

/**
 *
 * Bean that piles up (FIFO) streamer events received by the WS and make sure they're sequentially processed
 * Piles are not polled in a separate thread on purpose: it's a blocking behaviour
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class StreamerEventPileManager<T extends StreamerEventPilePool> extends PilePoolManager<T> {

    private long sleepTime;
    private long maxRetry;

    public long getSleepTime() {
        if (sleepTime<0)
            sleepTime = 100; //100ms
        return sleepTime;
    }

    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    public long getMaxRetry() {
        if (maxRetry<0)
            maxRetry = 100; //SLEEP_TIME*MAX_RETRY==effective timeout in ms
        return maxRetry;
    }

    public void setMaxRetry(long maxRetry) {
        this.maxRetry = maxRetry;
    }
    
    /**
     * Pile a specific event on the streamer/channel pile and check from time to time if it has become the top event in the pile
     * In this case, run the callback. Until then, wait patiently (block current thread)
     * Basically, it prevents multiple WS threads to access the same Channel resources at the same time, it cannot make sure queued up events will be processed in the right order though, it heavily depends on network latencies and if an event that must be treated before another never arrives while the later event arrives first is not blocked in the queue, then events are mixed-up.
     * @param streamerId
     * @param runnable
     * @return
     * @throws Exception
     */
    public <U extends RunnableStreamerEvent> Object synchronousBlockingEvent(long id, U runnable) throws Exception {
        return synchronousBlockingEvent(id, null, runnable);
    }
    public <U extends RunnableStreamerEvent> Object synchronousBlockingEvent(long id, Long published, U runnable) throws Exception {
        Object result = null;
        if (runnable != null) {
            PilePoolEvent<U> event = new PilePoolEvent<U>(published==null ? DateHelper.getCurrentTime() : published, UUIDHelper.getRandomUUID(), runnable);
            final BlockingQueue<PilePoolEvent<? extends RunnableStreamerEvent>> pile = getEventPilePoolHelper().getPile(id);
            logger.debug("synchronousBlockingEvent() .put("+event.toString()+")");
            pile.put(event);

            long currentRetry = 0;
            while (currentRetry < getMaxRetry()) {
                PilePoolEvent topEvent = pile.peek();
                if (topEvent != null && topEvent.equals(event)) {
                    try {
                        logger.debug("synchronousBlockingEvent() .peeked("+topEvent.toString()+")");
                        result = topEvent.getRunnable().synchronous();
                    } finally {
                        logger.debug("synchronousBlockingEvent() .remove("+topEvent.toString()+")");
                        pile.remove();
                    }
                    break;
                }
                Thread.sleep(getSleepTime());
                currentRetry++;
            }
            //Failsafe: if you cannot access the event within this time interval then drop it, no remorse.
            if (currentRetry == getMaxRetry()) {
                logger.debug("synchronousBlockingEvent() .aborted()");
                pile.remove(event);
            }

        }

        return result;
    }    
    
}
