/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer.pile;

import biz.ddcr.shampoo.server.domain.streamer.GenericItemInterface;
import biz.ddcr.shampoo.server.service.helper.AbstractPilePool;
import biz.ddcr.shampoo.server.service.helper.AbstractPilePool.RunnableGenericEvent;
import biz.ddcr.shampoo.server.service.helper.PilePoolEvent;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool.RunnableStreamerEvent;
import java.util.HashMap;
import java.util.concurrent.PriorityBlockingQueue;

/**
 *
 * A (thread-safe) singleton bean -check the Spring context- that acts as a
 * router for piles of events for streamers
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class StreamerEventPilePool extends AbstractPilePool<PilePoolEvent<? extends RunnableStreamerEvent>> {

    private long eventLifeSpan;

    @Override
    public long getEventLifeSpan() {
        if (eventLifeSpan<0)
            eventLifeSpan = 300000; //5m
        return eventLifeSpan;
    }

    public void setEventLifeSpan(long eventLifeSpan) {
        this.eventLifeSpan = eventLifeSpan;
    }    

    public interface RunnableStreamerEvent extends RunnableGenericEvent {
    }

    public interface ItemRunnableStreamerEvent extends RunnableStreamerEvent {

        @Override
        public GenericItemInterface synchronous() throws Exception;
    }

    public interface NullRunnableStreamerEvent extends RunnableStreamerEvent {

        @Override
        public Void synchronous() throws Exception;
    }
    /**
     * The list of piles of events associated with each channel/streamer,
     * constructed lazily - i.e. a pile is only instantiated when first accessed *
     */
    private static final HashMap<Long, PriorityBlockingQueue<PilePoolEvent<? extends RunnableStreamerEvent>>> piles;

    static {
        piles = new HashMap<Long, PriorityBlockingQueue<PilePoolEvent<? extends RunnableStreamerEvent>>>();
    }

    @Override
    protected HashMap<Long, PriorityBlockingQueue<PilePoolEvent<? extends RunnableStreamerEvent>>> getPiles() {
        return piles;
    }
}
