/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer.task;

import biz.ddcr.shampoo.server.helper.MicroTask;
import biz.ddcr.shampoo.server.helper.TaskPerformer;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.streamer.StreamerFacadeServiceInterface;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPileManager;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool.NullRunnableStreamerEvent;
import org.springframework.core.task.TaskExecutor;

/**
 *
 * Phony facade that has no other use than enforcing a transaction using Spring
 * interface-based proxy mechanism
 *
 * @author okay_awright
 *
 */
public class AsynchronousQueuePopulateRunnableHelper extends GenericService implements AsynchronousQueueRunnableHelperInterface {

    private StreamerFacadeServiceInterface streamerFacadeService;
    private StreamerEventPileManager streamerEventPileManager;
    private transient TaskPerformer taskPerformer;

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskPerformer = new TaskPerformer(taskExecutor);
    }

    public StreamerFacadeServiceInterface getStreamerFacadeService() {
        return streamerFacadeService;
    }

    public void setStreamerFacadeService(StreamerFacadeServiceInterface streamerFacadeService) {
        this.streamerFacadeService = streamerFacadeService;
    }

    public StreamerEventPileManager getStreamerEventPileManager() {
        return streamerEventPileManager;
    }

    public void setStreamerEventPileManager(StreamerEventPileManager streamerEventPileManager) {
        this.streamerEventPileManager = streamerEventPileManager;
    }

    @Override
    public void asynchronousRun(final long seatNumber) throws Exception {
        final long currentTime = DateHelper.getCurrentTime();

        //Each channel processing is performed in its own thread
        MicroTask loop = new MicroTask() {
            @Override
            public boolean loop() throws Exception {
                //Queue up this command so that multiple concurrent accesses for this queue cannot happen
                getStreamerEventPileManager().synchronousBlockingEvent(seatNumber, currentTime, new NullRunnableStreamerEvent() {
                    @Override
                    public Void synchronous() throws Exception {
                        //purge the queue from obsolete items
                        //Do only feed the channel queue if its streamer is online                       
                        getStreamerFacadeService().synchronousUnsecuredPopulateQueue(seatNumber);
                        //no value expected
                        return null;
                    }
                });
                //Return false to notify that no more loop is required
                return false;
            }
        };
        taskPerformer.runTask(loop);
    }
}
