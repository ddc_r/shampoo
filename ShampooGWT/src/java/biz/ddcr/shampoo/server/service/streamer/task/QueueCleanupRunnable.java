/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer.task;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerInterface;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPileManager;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool.NullRunnableStreamerEvent;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class QueueCleanupRunnable extends GenericService implements QueueCleanupRunnableInterface {

    private ChannelManagerInterface channelManager;
    private QueueSchedulerInterface queueScheduler;
    private StreamerEventPileManager streamerEventPileManager;

    public QueueSchedulerInterface getQueueScheduler() {
        return queueScheduler;
    }

    public void setQueueScheduler(QueueSchedulerInterface queueScheduler) {
        this.queueScheduler = queueScheduler;
    }

    public StreamerEventPileManager getStreamerEventPileManager() {
        return streamerEventPileManager;
    }

    public void setStreamerEventPileManager(StreamerEventPileManager streamerEventPileManager) {
        this.streamerEventPileManager = streamerEventPileManager;
    }


    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    /**
     * Purge all queues from items that are no longer valid
     * @throws Exception
     */
    @Override
    public void purgeQueues() throws Exception {

        final long currentTime = DateHelper.getCurrentTime();        
        Collection<Long> seats = ChannelStreamerConnectionStatusHelper.getAllSeats();
        for (final Long seatNumber : seats) {
            //Queue up this command so that multiple concurrent accesses for this queue cannot happen
            if (seatNumber!=null) getStreamerEventPileManager().synchronousBlockingEvent(seatNumber, currentTime, new NullRunnableStreamerEvent() {

                    @Override
                    public Void synchronous() throws Exception {
                        //purge the queue from obsolete items
                        //Do only clean the channel queue if its streamer is offline
                        final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
                        if (channelId!=null) {
                            getQueueScheduler().purgeObsoleteQueueItems(getChannelManager().getChannel(channelId));
                        }
                        //no value expected
                        return null;
                    }
                });

        }
    }

    @Override
    public void run() {
        try {
            purgeQueues();
        } catch (Exception e) {
            logger.error("Queue runnable failed", e);
        }
    }
}
