/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.streamer.task;

import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.domain.streamer.notification.StreamerStatusNotification;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinuteSecondMillisecond;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import biz.ddcr.shampoo.server.service.streamer.QueueSchedulerInterface;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPileManager;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool.NullRunnableStreamerEvent;
import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author okay_awright
 *
 */
public class StreamerChannelConnectionCleanupRunnable extends GenericService implements StreamerChannelConnectionCleanupRunnableInterface {

    private ChannelManagerInterface channelManager;
    private QueueSchedulerInterface queueScheduler;
    private StreamerEventPileManager streamerEventPileManager;
    private SecurityManagerInterface securityManager;

    public QueueSchedulerInterface getQueueScheduler() {
        return queueScheduler;
    }

    public void setQueueScheduler(QueueSchedulerInterface queueScheduler) {
        this.queueScheduler = queueScheduler;
    }

    public StreamerEventPileManager getStreamerEventPileManager() {
        return streamerEventPileManager;
    }

    public void setStreamerEventPileManager(StreamerEventPileManager streamerEventPileManager) {
        this.streamerEventPileManager = streamerEventPileManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    /**
     * Purge all channel streamer connections from items that are no longer in
     * use
     *
     * @throws Exception
     */
    @Override
    public void auditStreamerChannelConnections() throws Exception {

        final long currentTime = DateHelper.getCurrentTime();
        Collection<Long> seats = ChannelStreamerConnectionStatusHelper.getAllSeats();
        for (final Long seatNumber : seats) {
            //Queue up this command so that multiple concurrent accesses for this queue cannot happen
            if (seatNumber!=null) getStreamerEventPileManager().synchronousBlockingEvent(seatNumber, currentTime, new NullRunnableStreamerEvent() {
                @Override
                public Void synchronous() throws Exception {
                    //purge the queue from obsolete items
                    //Clean the streamer connection if its streamer is offline
                    //Is it still registered as being online?
                    final String channelId = ChannelStreamerConnectionStatusHelper.getChannelId(seatNumber);
                    if (channelId!=null) {
                        //Then try to see whether its latest heartbeat is past the Ttl
                        //But notify users first in this case
                        final DynamicStreamerData currentStreamerStateConfiguration = ChannelStreamerConnectionStatusHelper.getStreamerData(seatNumber);
                        //Is TTL enabled? Did we receive at least one heartbeat?
                        if (currentStreamerStateConfiguration!=null && currentStreamerStateConfiguration.getTtl()!=null && currentStreamerStateConfiguration.getTtl()>0 && currentStreamerStateConfiguration.getLatestHeartbeat()!=null) {
                            final long currentTime = DateHelper.getCurrentTime();
                            final long boundaryTime = currentTime - currentStreamerStateConfiguration.getTtl();
                            //The latest received hearbeat shouldn't have happended before this time, otherwise it surely means the streamer is dead
                            if (currentStreamerStateConfiguration.getLatestHeartbeat() < boundaryTime) {
                                
                                if (ChannelStreamerConnectionStatusHelper.deleteStreamerChannelStateConnection(seatNumber)) {
                                    final Channel channel = getChannelManager().getChannel(channelId);
                                    if (channel!=null) {
                                        getQueueScheduler().setChannelFree(channel, new YearMonthWeekDayHourMinuteSecondMillisecond(currentTime, channel.getTimezone()));
                                        //Warn every responsible user that the streamer is now offline and deceased
                                        StreamerChannelConnectionCleanupRunnable.this.notify(StreamerStatusNotification.STREAMER_STATUS_OPERATION.channel_free, channel);
                                    }
                                }
                            }
                        }
                    }
                    //no value expected
                    return null;
                }
            });

        }
    }

    @Override
    public void run() {
        try {
            auditStreamerChannelConnections();
        } catch (Exception e) {
            logger.error("Streamer connection runnable failed", e);
        }
    }

    protected void notify(StreamerStatusNotification.STREAMER_STATUS_OPERATION operation, Channel channel) throws Exception {
        notify(operation, channel, operation.getRecipients(getSecurityManager(), channel));
    }

    protected void notify(StreamerStatusNotification.STREAMER_STATUS_OPERATION operation, Channel channel, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && recipients != null) {
            Collection<StreamerStatusNotification> notifications = new HashSet<StreamerStatusNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new StreamerStatusNotification(operation, channel, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
