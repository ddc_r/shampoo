/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface AsynchronousTrackFacadeServiceHelperInterface {

    public interface AsynchronousDeleteTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm track);
    }

    public interface AsynchronousAddTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm track, String trackUploadId, String coverArtUploadId);
        public void add(TrackForm track, String trackUploadId);
        public void add(TrackForm track);
    }

    public interface AsynchronousUpdateTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm track, String trackUploadId, String coverArtUploadId);
        public void addPictureUnchanged(TrackForm track, String trackUploadId);
        public void addAudioUnchanged(TrackForm track, String coverArtUploadId);
        public void add(TrackForm track);
    }

    public interface AsynchronousValidateTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm source, TrackForm destination);
    }

    public interface AsynchronousRejectTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm track, String message);
    }

    public void synchronousInitialization(Collection<AsynchronousUnmanagedTransactionUnitInterface<Track>> asynchronousTrackOps) throws Exception;
    public void synchronousProcessing(Collection<AsynchronousUnmanagedTransactionUnitInterface<Track>> asynchronousTrackOps) throws Exception;
    public void synchronousCommit(Collection<AsynchronousUnmanagedTransactionUnitInterface<Track>> asynchronousTrackOps) throws Exception;
    public void synchronousRollback(Collection<AsynchronousUnmanagedTransactionUnitInterface<Track>> asynchronousTrackOps, Exception originalException) throws Exception;
}
