/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.form.track.AverageVoteModule;
import biz.ddcr.shampoo.client.form.track.BroadcastAdvertForm;
import biz.ddcr.shampoo.client.form.track.BroadcastJingleForm;
import biz.ddcr.shampoo.client.form.track.BroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule;
import biz.ddcr.shampoo.client.form.track.PEGIRatingModule.PEGI_AGE;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackProgrammeModule;
import biz.ddcr.shampoo.client.form.track.RequestModule;
import biz.ddcr.shampoo.client.form.track.RollbackBroadcastAdvertForm;
import biz.ddcr.shampoo.client.form.track.RollbackBroadcastJingleForm;
import biz.ddcr.shampoo.client.form.track.RollbackBroadcastSongForm;
import biz.ddcr.shampoo.client.form.track.RollbackBroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackPendingTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackTrackForm;
import biz.ddcr.shampoo.client.form.track.TRACK_ACTION;
import biz.ddcr.shampoo.client.form.track.TYPE;
import biz.ddcr.shampoo.client.form.track.TagModule;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.VoteModule;
import biz.ddcr.shampoo.client.form.track.VoteModule.VOTE;
import biz.ddcr.shampoo.client.form.track.format.AUDIO_TYPE;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.form.track.format.PICTURE_TYPE;
import biz.ddcr.shampoo.client.helper.date.JSDuration;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.StaticPlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PEGIRating;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.RATING_AGE;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.SCORE;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.TrackProgramme;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.domain.track.Voting;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.helper.FinalBoolean;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.channel.ChannelManager;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author okay_awright
 **/
public class TrackDTOAssembler extends GenericService implements TrackDTOAssemblerInterface {

    /** Dummy visitor pattern for tracks, with result value **/
    private interface BooleanTrackVisitor extends TrackVisitor {

        public boolean get();
    }

    private interface DTOTrackVisitor extends TrackVisitor {

        public ActionCollectionEntry<? extends TrackForm> get();
    }
    private interface RollbackDTOTrackVisitor extends TrackVisitor {

        public ActionCollectionEntry<? extends RollbackTrackForm> get();
    }
    private TrackManagerInterface trackManager;
    private UserManagerInterface userManager;
    private ProgrammeManagerInterface programmeManager;
    private ChannelManager channelManager;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public ChannelManager getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManager channelManager) {
        this.channelManager = channelManager;
    }

    @Override
    public Track fromDTO(TrackForm trackForm) throws Exception {
        if (trackForm == null) {
            return null;
        }
        if (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class)) {
            return fromDTO((BroadcastTrackForm) trackForm);
        } else if (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class)) {
            return fromDTO((PendingTrackForm) trackForm);
        } else {
            //Extend when necessary
            return null;
        }
    }

    @Override
    public BroadcastableTrack fromDTO(BroadcastTrackForm broadcastTrackForm) throws Exception {
        if (broadcastTrackForm == null) {
            return null;
        }

        BroadcastableTrack broadcastTrack;
        if (ProxiedClassUtil.castableAs(broadcastTrackForm, BroadcastSongForm.class)) {
            broadcastTrack = new BroadcastableSong();
            //no vote is set on creation
            //TODO: see if it's useful to add this feature at creation time
        } else if (ProxiedClassUtil.castableAs(broadcastTrackForm, BroadcastAdvertForm.class)) {
            broadcastTrack = new BroadcastableAdvert();
        } else if (ProxiedClassUtil.castableAs(broadcastTrackForm, BroadcastJingleForm.class)) {
            broadcastTrack = new BroadcastableJingle();
        } else {
            return null;
        }

        //Fill in all attributes
        //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
        if (broadcastTrackForm.getRefID() == null) {
            broadcastTrackForm.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
        }
        broadcastTrack.setRefID(broadcastTrackForm.getRefID());
        broadcastTrack.setEnabled(broadcastTrackForm.isActivated());

        //it's brand new: it could not have been played yet
        //broadcastTrack.setRotation(0);

        broadcastTrack.setRating(fromDTO(broadcastTrackForm.getAdvisory()));
        broadcastTrack.setAlbum(broadcastTrackForm.getAlbum());
        broadcastTrack.setAuthor(broadcastTrackForm.getAuthor());
        broadcastTrack.setPublisher(broadcastTrackForm.getPublisher());
        broadcastTrack.setCopyright(broadcastTrackForm.getCopyright());
        broadcastTrack.setDescription(broadcastTrackForm.getDescription());
        broadcastTrack.setGenre(broadcastTrackForm.getGenre());
        broadcastTrack.setTag(broadcastTrackForm.getTag());
        broadcastTrack.setTitle(broadcastTrackForm.getTitle());
        broadcastTrack.setDateOfRelease(broadcastTrackForm.getYearOfRelease());
        //isReady() has no meaning this way

        //Containers info that need to be imported from external data are handled elsewhere
        //Don't instantiate empty containers if the form has no container attached to it
        if (broadcastTrackForm.getCoverArtFile() != null) {
            broadcastTrack.setCoverArtContainer(broadcastTrackForm.getCoverArtFile()!=null?fromDTO(broadcastTrackForm.getCoverArtFile().getItem()):null);
        }
        if (broadcastTrackForm.getTrackFile() != null) {
            broadcastTrack.setTrackContainer(broadcastTrackForm.getTrackFile()!=null?fromDTO(broadcastTrackForm.getTrackFile().getItem()):null);
        }

        //Programmes
        //Don't throw an exception if one cannot be accessed, just don't add it
        for (ActionCollectionEntry<BroadcastTrackProgrammeModule> programmeEntry : broadcastTrackForm.getTrackProgrammes()) {
            Programme p = getProgrammeManager()/*.loadProgrammePrefetchTracks*/.getProgramme(programmeEntry.getKey().getProgramme());
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(p)
                    || getProgrammeManager().checkAccessUpdateProgramme(p)) {
                //it's a brand new association: the track could not have been played yet, its rotation is zero
                broadcastTrack.addProgramme(p);
            }
        }

        //Slots and selections are handled elsewhere

        return broadcastTrack;
    }

    @Override
    public PendingTrack fromDTO(PendingTrackForm pendingTrackForm) throws Exception {
        if (pendingTrackForm == null) {
            return null;
        }

        PendingTrack pendingTrack;
        switch (pendingTrackForm.getType()) {
            case song:
                pendingTrack = new PendingSong();
                break;
            case jingle:
                pendingTrack = new PendingJingle();
                break;
            case advert:
                pendingTrack = new PendingAdvert();
                break;
            default:
                return null;
        }

        //Fill in all attributes
        //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
        if (pendingTrackForm.getRefID() == null) {
            pendingTrackForm.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
        }
        pendingTrack.setRefID(pendingTrackForm.getRefID());

        pendingTrack.setRating(fromDTO(pendingTrackForm.getAdvisory()));
        pendingTrack.setAlbum(pendingTrackForm.getAlbum());
        pendingTrack.setAuthor(pendingTrackForm.getAuthor());
        pendingTrack.setPublisher(pendingTrackForm.getPublisher());
        pendingTrack.setCopyright(pendingTrackForm.getCopyright());
        pendingTrack.setDescription(pendingTrackForm.getDescription());
        pendingTrack.setGenre(pendingTrackForm.getGenre());
        pendingTrack.setTag(pendingTrackForm.getTag());
        pendingTrack.setTitle(pendingTrackForm.getTitle());
        pendingTrack.setDateOfRelease(pendingTrackForm.getYearOfRelease());
        //isReady() has no meaning this way

        //Containers info that need to be imported from external data are handled elsewhere
        //Don't instantiate empty containers if the form has no container attached to it
        if (pendingTrackForm.getCoverArtFile() != null) {
            pendingTrack.setCoverArtContainer(pendingTrackForm.getCoverArtFile()!=null?fromDTO(pendingTrackForm.getCoverArtFile().getItem()):null);
        }
        if (pendingTrackForm.getTrackFile() != null) {
            pendingTrack.setTrackContainer(pendingTrackForm.getTrackFile()!=null?fromDTO(pendingTrackForm.getTrackFile().getItem()):null);
        }

        //Programmes
        //Don't throw an exception if one cannot be accessed, just don't add it
        for (ActionCollectionEntry<PendingTrackProgrammeModule> programmeEntry : pendingTrackForm.getTrackProgrammes()) {
            //Don't enforce an Update ACTION but a less stringent ReadExtended, so that Curators and Contributors can bind programmes to tracks
            Programme p = getProgrammeManager()/*.loadProgrammePrefetchTracks*/.getProgramme(programmeEntry.getKey().getProgramme());
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(p)
                    || getProgrammeManager().checkAccessUpdateProgramme(p)) {
                pendingTrack.addProgramme(p);
            }
        }

        //No Slots and selections for pending tracks

        return pendingTrack;
    }

    @Override
    public boolean augmentWithDTO(Track track, final TrackForm trackForm) throws Exception {
        if (trackForm == null || track == null) {
            return false;
        }
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;

            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class))
                        ? augmentWithDTO(track, (PendingTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class))
                        ? augmentWithDTO(track, (PendingTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class))
                        ? augmentWithDTO(track, (PendingTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class))
                        ? augmentWithDTO(track, (BroadcastTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class))
                        ? augmentWithDTO(track, (BroadcastTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class))
                        ? augmentWithDTO(track, (BroadcastTrackForm) trackForm)
                        : false;
            }
        };

        track.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public boolean augmentWithRollbackDTO(Track track, final RollbackTrackForm trackForm) throws Exception {
        if (trackForm == null || track == null) {
            return false;
        }
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;

            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class))
                        ? augmentWithRollbackDTO(track, (RollbackPendingTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class))
                        ? augmentWithRollbackDTO(track, (RollbackPendingTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class))
                        ? augmentWithRollbackDTO(track, (RollbackPendingTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class))
                        ? augmentWithRollbackDTO(track, (RollbackBroadcastTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class))
                        ? augmentWithRollbackDTO(track, (RollbackBroadcastTrackForm) trackForm)
                        : false;
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class))
                        ? augmentWithRollbackDTO(track, (RollbackBroadcastTrackForm) trackForm)
                        : false;
            }
        };

        track.acceptVisit(visitor);
        return visitor.get();
    }

    /**
     * update an existing track with data from a DTO.
     * Returns true if attributes of the original track have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(BroadcastableTrack broadcastTrack, BroadcastTrackForm broadcastTrackForm) throws Exception {

        boolean contentUpdated = false;

        if (broadcastTrackForm != null && broadcastTrack != null) {

            //Fill in all attributes
            if ((broadcastTrackForm.getRefID() != null && !broadcastTrackForm.getRefID().equals(broadcastTrack.getRefID())
                    || (broadcastTrackForm.getRefID() == null && broadcastTrack.getRefID() != null))) {
                broadcastTrack.setRefID(broadcastTrackForm.getRefID());
                contentUpdated = true;
            }
            if (broadcastTrackForm.isActivated() != broadcastTrack.isEnabled()) {
                broadcastTrack.setEnabled(broadcastTrackForm.isActivated());
                contentUpdated = true;
            }

            PEGIRating rating = fromDTO(broadcastTrackForm.getAdvisory());
            if ((rating != null && !rating.equals(broadcastTrack.getRating())
                    || (rating == null && broadcastTrack.getRating() != null))) {
                broadcastTrack.setRating(rating);
                contentUpdated = true;
            }
            String album = broadcastTrackForm.getAlbum();
            if ((album != null && !album.equals(broadcastTrack.getAlbum())
                    || (album == null && broadcastTrack.getAlbum() != null))) {
                broadcastTrack.setAlbum(album);
                contentUpdated = true;
            }
            String author = broadcastTrackForm.getAuthor();
            if ((author != null && !author.equals(broadcastTrack.getAuthor())
                    || (author == null && broadcastTrack.getAuthor() != null))) {
                broadcastTrack.setAuthor(author);
                contentUpdated = true;
            }
            if ((broadcastTrackForm.getPublisher() != null && !broadcastTrackForm.getPublisher().equals(broadcastTrack.getPublisher())
                    || (broadcastTrackForm.getPublisher() == null && broadcastTrack.getPublisher() != null))) {
                broadcastTrack.setPublisher(broadcastTrackForm.getPublisher());
                contentUpdated = true;
            }
            if ((broadcastTrackForm.getCopyright() != null && !broadcastTrackForm.getCopyright().equals(broadcastTrack.getCopyright())
                    || (broadcastTrackForm.getCopyright() == null && broadcastTrack.getCopyright() != null))) {
                broadcastTrack.setCopyright(broadcastTrackForm.getCopyright());
                contentUpdated = true;
            }
            if ((broadcastTrackForm.getDescription() != null && !broadcastTrackForm.getDescription().equals(broadcastTrack.getDescription())
                    || (broadcastTrackForm.getDescription() == null && broadcastTrack.getDescription() != null))) {
                broadcastTrack.setDescription(broadcastTrackForm.getDescription());
                contentUpdated = true;
            }
            String genre = broadcastTrackForm.getGenre();
            if ((genre != null && !genre.equals(broadcastTrack.getGenre())
                    || (genre == null && broadcastTrack.getGenre() != null))) {
                broadcastTrack.setGenre(genre);
                contentUpdated = true;
            }
            if ((broadcastTrackForm.getTag() != null && !broadcastTrackForm.getTag().equals(broadcastTrack.getTag())
                    || (broadcastTrackForm.getTag() == null && broadcastTrack.getTag() != null))) {
                broadcastTrack.setTag(broadcastTrackForm.getTag());
                contentUpdated = true;
            }
            String title = broadcastTrackForm.getTitle();
            if ((title != null && !title.equals(broadcastTrack.getTitle())
                    || (title == null && broadcastTrack.getTitle() != null))) {
                broadcastTrack.setTitle(title);
                contentUpdated = true;
            }
            if ((broadcastTrackForm.getYearOfRelease() != null && !broadcastTrackForm.getYearOfRelease().equals(broadcastTrack.getDateOfRelease())
                    || (broadcastTrackForm.getYearOfRelease() == null && broadcastTrack.getDateOfRelease() != null))) {
                broadcastTrack.setDateOfRelease(broadcastTrackForm.getYearOfRelease());
                contentUpdated = true;
            }
            //setReady() and cover art picture and track file are handled lswhere

            //we cannot actually use the regular update strategy which consists in removing all updatable items first and then adding the ones from the form
            //user non-updatable values are bound to a broadcasttrackprogramme, e.g. the rotation count
            //Collect all programmes within the DTO that the user can access
            Collection<TrackProgramme> programmesFromDTO = new HashSet<TrackProgramme>();
            for (ActionCollectionEntry<BroadcastTrackProgrammeModule> programmeEntry : broadcastTrackForm.getTrackProgrammes()) {
                BroadcastTrackProgrammeModule tp = programmeEntry.getKey();
                Programme p = getProgrammeManager()/*.loadProgrammePrefetchTracks*/.getProgramme(tp.getProgramme());
                if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(p)
                        && getProgrammeManager().checkAccessUpdateProgramme(p)) {
                    programmesFromDTO.add(new TrackProgramme(p, broadcastTrack));
                }
            }
            //Check which ones have been added or removed compared to the original set
            Collection<TrackProgramme> currentProgrammes = broadcastTrack.getTrackProgrammes();
            Collection<TrackProgramme> addedProgrammes = CollectionUtil.exclusion(currentProgrammes, programmesFromDTO);
            Collection<TrackProgramme> removedProgrammes = CollectionUtil.exclusion(programmesFromDTO, currentProgrammes);
            //Remove the ones not included in the DTO if the user can access them
            for (Iterator<TrackProgramme> i = removedProgrammes.iterator(); i.hasNext();) {
                TrackProgramme boundTrackProgramme = i.next();
                if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(boundTrackProgramme.getProgramme())
                        && getProgrammeManager().checkAccessUpdateProgramme(boundTrackProgramme.getProgramme())) {
                    i.remove();
                    broadcastTrack.removeTrackProgramme(boundTrackProgramme);                  
                }
            }
            //Then add back to the lists the new ones provided by the DTO
            for (TrackProgramme p : addedProgrammes) {
                if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(p.getProgramme())
                        && getProgrammeManager().checkAccessUpdateProgramme(p.getProgramme())) {
                    broadcastTrack.addTrackProgramme(p);
                }
            }

            //Selections and slots are removed if the programme they belong to are not in the main list anymore
            //for (StaticPlaylistEntry boundPlaylistEntry : broadcastTrack.getSlots()) {
            for (Iterator<StaticPlaylistEntry> i = broadcastTrack.getSlots().iterator(); i.hasNext();) {
                StaticPlaylistEntry boundPlaylistEntry = i.next();
                if (boundPlaylistEntry.getPlaylist() != null
                        && !broadcastTrack.getImmutableProgrammes().contains(boundPlaylistEntry.getPlaylist().getProgramme())) {
                    i.remove();
                    broadcastTrack.removeSlot(boundPlaylistEntry);
                }
            }

            //Votes are not handled here

        }
        return contentUpdated;
    }

    /**
     * update an existing track with data from a DTO.
     * Returns true if attributes of the original track have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
    public boolean augmentWithDTO(PendingTrack pendingTrack, PendingTrackForm pendingTrackForm) throws Exception {

        boolean contentUpdated = false;

        if (pendingTrackForm != null && pendingTrack != null) {

            //Fill in all attributes
            if (!pendingTrackForm.getRefID().equals(pendingTrack.getRefID())) {
                pendingTrack.setRefID(pendingTrackForm.getRefID());
                contentUpdated = true;
            }

            PEGIRating rating = fromDTO(pendingTrackForm.getAdvisory());
            if ((rating != null && !rating.equals(pendingTrack.getRating())
                    || (rating == null && pendingTrack.getRating() != null))) {
                pendingTrack.setRating(rating);
                contentUpdated = true;
            }
            String album = pendingTrackForm.getAlbum();
            if ((album != null && !album.equals(pendingTrack.getAlbum())
                    || (album == null && pendingTrack.getAlbum() != null))) {
                pendingTrack.setAlbum(album);
                contentUpdated = true;
            }
            String author = pendingTrackForm.getAuthor();
            if ((author != null && !author.equals(pendingTrack.getAuthor())
                    || (album == null && pendingTrack.getAuthor() != null))) {
                pendingTrack.setAuthor(author);
                contentUpdated = true;
            }
            if ((pendingTrackForm.getPublisher() != null && !pendingTrackForm.getPublisher().equals(pendingTrack.getPublisher())
                    || (pendingTrackForm.getPublisher() == null && pendingTrack.getPublisher() != null))) {
                pendingTrack.setPublisher(pendingTrackForm.getPublisher());
                contentUpdated = true;
            }
            if ((pendingTrackForm.getCopyright() != null && !pendingTrackForm.getCopyright().equals(pendingTrack.getCopyright())
                    || (pendingTrackForm.getCopyright() == null && pendingTrack.getCopyright() != null))) {
                pendingTrack.setCopyright(pendingTrackForm.getCopyright());
                contentUpdated = true;
            }
            if ((pendingTrackForm.getDescription() != null && !pendingTrackForm.getDescription().equals(pendingTrack.getDescription())
                    || (pendingTrackForm.getDescription() == null && pendingTrack.getDescription() != null))) {
                pendingTrack.setDescription(pendingTrackForm.getDescription());
                contentUpdated = true;
            }
            String genre = pendingTrackForm.getGenre();
            if ((genre != null && !genre.equals(pendingTrack.getGenre())
                    || (genre == null && pendingTrack.getGenre() != null))) {
                pendingTrack.setGenre(genre);
                contentUpdated = true;
            }
            if ((pendingTrackForm.getTag() != null && !pendingTrackForm.getTag().equals(pendingTrack.getTag())
                    || (pendingTrackForm.getTag() == null && pendingTrack.getTag() != null))) {
                pendingTrack.setTag(pendingTrackForm.getTag());
                contentUpdated = true;
            }
            String title = pendingTrackForm.getTitle();
            if ((title != null && !title.equals(pendingTrack.getTitle())
                    || (title == null && pendingTrack.getTitle() != null))) {
                pendingTrack.setTitle(title);
                contentUpdated = true;
            }
            if ((pendingTrackForm.getYearOfRelease() != null && !pendingTrackForm.getYearOfRelease().equals(pendingTrack.getDateOfRelease())
                    || (pendingTrackForm.getYearOfRelease() == null && pendingTrack.getDateOfRelease() != null))) {
                pendingTrack.setDateOfRelease(pendingTrackForm.getYearOfRelease());
                contentUpdated = true;
            }
            //setReady() and cover art picture and track file are handled lswhere

            //First remove all programmes from the original track that the authenticated user can access
            //for (Programme boundProgramme : broadcastTrack.getProgrammes()) {
            for (Iterator<TrackProgramme> i = pendingTrack.getTrackProgrammes().iterator(); i.hasNext();) {
                TrackProgramme boundTrackProgramme = i.next();
                if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(boundTrackProgramme.getProgramme())
                        && getProgrammeManager().checkAccessUpdateProgramme(boundTrackProgramme.getProgramme())) {
                    i.remove();
                    pendingTrack.removeTrackProgramme(boundTrackProgramme);
                }
            }
            //Then add back to the lists the ones provided by the DTO
            for (ActionCollectionEntry<PendingTrackProgrammeModule> programmeEntry : pendingTrackForm.getTrackProgrammes()) {
                PendingTrackProgrammeModule tp = programmeEntry.getKey();
                Programme p = getProgrammeManager()/*.loadProgrammePrefetchTracks*/.getProgramme(tp.getProgramme());
                if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(p)
                        && getProgrammeManager().checkAccessUpdateProgramme(p)) {
                    pendingTrack.addProgramme(p);
                }
            }

        }
        return contentUpdated;
    }

    @Override
    public boolean augmentWithRollbackDTO(BroadcastableTrack broadcastTrack, final RollbackBroadcastTrackForm broadcastTrackForm) throws Exception {
        //No extra data for most objects, use the default fallback
        final FinalBoolean result = new FinalBoolean();
        result.value = augmentWithDTO(broadcastTrack, broadcastTrackForm);
        TrackVisitor visitor = new TrackVisitor() {

            @Override
            public void visit(PendingAdvert track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                RollbackBroadcastSongForm broadcastSongForm = (RollbackBroadcastSongForm) broadcastTrackForm;

                track.getVotes().clear();
                for (VoteModule voteModule : broadcastSongForm.getVotes()) {
                    track.addVote(new Voting(//
                            getUserManager().getRestrictedUser(voteModule.getUser()),//
                            track,//
                            fromDTO(voteModule.getVote())//
                            ), result.value);
                }

                track.getRequests().clear();
                for (RequestModule requestModule : broadcastSongForm.getRequests()) {
                    track.addRequest(new Request(//
                            getUserManager().getRestrictedUser(requestModule.getUser()),//
                            track,//
                            getChannelManager().getChannel(requestModule.getChannel()),
                            requestModule.getMessage()//
                            ), result.value);
                }
            }
        };
        broadcastTrack.acceptVisit(visitor);
        return result.value;
    }

    @Override
    public boolean augmentWithRollbackDTO(PendingTrack pendingTrack, RollbackPendingTrackForm pendingTrackForm) throws Exception {
        //No extra data, use the default fallback
        return augmentWithDTO(pendingTrack, pendingTrackForm);
    }

    public static SCORE fromDTO(VOTE rating) {
        if (rating == null) {
            return null;
        }
        switch (rating) {
            case poor:
                return SCORE.poor;
            case bad:
                return SCORE.bad;
            case average:
                return SCORE.average;
            case good:
                return SCORE.good;
            case excellent:
                return SCORE.excellent;
            default:
                return null;
        }
    }

    public static VOTE toDTO(SCORE rating) {
        if (rating == null) {
            return null;
        }
        switch (rating) {
            case poor:
                return VOTE.poor;
            case bad:
                return VOTE.bad;
            case average:
                return VOTE.average;
            case good:
                return VOTE.good;
            case excellent:
                return VOTE.excellent;
            default:
                return null;
        }
    }

    public static PEGIRating fromDTO(PEGIRatingModule rating) {
        if (rating != null) {
            PEGIRating advisory = new PEGIRating();

            //Age
            switch (rating.getRating()) {
                case earlyChildhood:
                    advisory.setAge(RATING_AGE.earlyChildhood);
                    break;
                case teen:
                    advisory.setAge(RATING_AGE.teen);
                    break;
                case mature:
                    advisory.setAge(RATING_AGE.mature);
                    break;
                case adultsOnly:
                    advisory.setAge(RATING_AGE.adultsOnly);
                    break;
                default:
                    advisory.setAge(RATING_AGE.everyone);
            }

            //Features
            advisory.setDiscrimination(rating.hasDiscrimination());
            advisory.setDrugs(rating.hasDrugs());
            advisory.setFear(rating.hasFear());
            advisory.setProfanity(rating.hasProfanity());
            advisory.setSex(rating.hasSex());
            advisory.setViolence(rating.hasViolence());
            return advisory;
        }
        return null;
    }

    public static PICTURE_FORMAT fromDTO(PICTURE_TYPE pictureType) {
        if (pictureType == null) {
            return null;
        }
        switch (pictureType) {
            case jpeg:
                return PICTURE_FORMAT.jpeg;
            case gif:
                return PICTURE_FORMAT.gif;
            case png:
                return PICTURE_FORMAT.png;
            case wbmp:
                return PICTURE_FORMAT.wbmp;
            default:
                return null;
        }
    }

    public static PictureFileInfo fromDTO(CoverArtModule file) {
        if (file != null) {
            PictureFileInfo container = new PictureFileInfo();

            //Format
            container.setFormat(fromDTO(file.getFormat()));

            //metadata
            container.setSize(file.getSize());
            return container;
        }
        return null;
    }

    public static AUDIO_FORMAT fromDTO(AUDIO_TYPE audioType) {
        if (audioType == null) {
            return null;
        }
        switch (audioType) {
            case mp3:
                return AUDIO_FORMAT.mp3;
            case mp4_aac:
                return AUDIO_FORMAT.mp4_aac;
            case native_flac:
                return AUDIO_FORMAT.native_flac;
            case ogg_vorbis:
                return AUDIO_FORMAT.ogg_vorbis;
            default:
                return null;
        }
    }

    public static AudioFileInfo fromDTO(ContainerModule file) {
        if (file != null) {
            AudioFileInfo container = new AudioFileInfo();

            //Format
            container.setFormat(fromDTO(file.getFormat()));

            //metadata
            container.setBitrate(file.getBitrate());
            container.setChannels(file.getChannels());
            container.setDuration(file.getDuration().getMilliseconds());
            container.setSamplerate(file.getSamplerate());
            container.setSize(file.getSize());
            container.setVbr(file.isVbr());
            return container;
        }
        return null;
    }

    public static PEGIRatingModule toDTO(PEGIRating rating) {
        if (rating != null) {
            PEGIRatingModule advisory = new PEGIRatingModule();

            //Age
            switch (rating.getAge()) {
                case earlyChildhood:
                    advisory.setRating(PEGI_AGE.earlyChildhood);
                    break;
                case teen:
                    advisory.setRating(PEGI_AGE.teen);
                    break;
                case mature:
                    advisory.setRating(PEGI_AGE.mature);
                    break;
                case adultsOnly:
                    advisory.setRating(PEGI_AGE.adultsOnly);
                    break;
                default:
                    advisory.setRating(PEGI_AGE.everyone);
            }

            //Features
            advisory.setDiscrimination(rating.getDiscrimination());
            advisory.setDrugs(rating.getDrugs());
            advisory.setFear(rating.getFear());
            advisory.setProfanity(rating.getProfanity());
            advisory.setSex(rating.getSex());
            advisory.setViolence(rating.getViolence());
            return advisory;
        }
        return null;
    }

    public static Collection<PICTURE_TYPE> toPictureDTO(Collection<PICTURE_FORMAT> pictureFormats) {
        Collection<PICTURE_TYPE> pictureTypes = new HashSet<PICTURE_TYPE>();
        if (pictureFormats != null) {
            for (PICTURE_FORMAT pictureFormat : pictureFormats) {
                PICTURE_TYPE pictureType = toDTO(pictureFormat);
                if (pictureType != null) {
                    pictureTypes.add(pictureType);
                }
            }
        }
        return pictureTypes;
    }

    public static PICTURE_TYPE toDTO(PICTURE_FORMAT pictureType) {
        if (pictureType == null) {
            return null;
        }
        switch (pictureType) {
            case jpeg:
                return PICTURE_TYPE.jpeg;
            case gif:
                return PICTURE_TYPE.gif;
            case png:
                return PICTURE_TYPE.png;
            case wbmp:
                return PICTURE_TYPE.wbmp;
            default:
                return null;
        }
    }

    public static Collection<AUDIO_TYPE> toAudioDTO(Collection<AUDIO_FORMAT> audioFormats) {
        Collection<AUDIO_TYPE> audioTypes = new HashSet<AUDIO_TYPE>();
        if (audioFormats != null) {
            for (AUDIO_FORMAT audioFormat : audioFormats) {
                AUDIO_TYPE audioType = toDTO(audioFormat);
                if (audioType != null) {
                    audioTypes.add(audioType);
                }
            }
        }
        return audioTypes;
    }

    public static AUDIO_TYPE toDTO(AUDIO_FORMAT audioType) {
        if (audioType == null) {
            return null;
        }
        switch (audioType) {
            case mp3:
                return AUDIO_TYPE.mp3;
            case mp4_aac:
                return AUDIO_TYPE.mp4_aac;
            case native_flac:
                return AUDIO_TYPE.native_flac;
            case ogg_vorbis:
                return AUDIO_TYPE.ogg_vorbis;
            default:
                return null;
        }
    }

    @Override
    public EmbeddedTagModule toTagDTO(final Track track) throws Exception {
        EmbeddedTagModule module = new EmbeddedTagModule();
        if (track != null) {
            module.setAuthor(track.getAuthor());
            module.setTitle(track.getTitle());
            module.setAlbum(track.getAlbum());
            module.setAdvisory(toDTO(track.getRating()));
            module.setPublisher(track.getPublisher());
            module.setCopyright(track.getCopyright());
            module.setDescription(track.getDescription());
            module.setEmbeddedCoverArt(track.getCoverArtContainer() != null);
            module.setGenre(track.getGenre());
            module.setYearOfRelease(track.getDateOfRelease());
            //Cover art
            module.setEmbeddedCoverArt(track.getCoverArtContainer() != null);
            if (track.getCoverArtContainer() != null) {
                module.setCoverArtFormat(toDTO(track.getCoverArtContainer().getFormat()));
                module.setCoverArtURL(
                        getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(track.getRefID()));
            }

        }
        return module;
    }

    @Override
    public EmbeddedTagModule toDraftTagDTO(TrackStream trackStream, String id) throws Exception {
        EmbeddedTagModule module = new EmbeddedTagModule();
        if (trackStream != null && trackStream.getTag() != null) {
            Tag info = trackStream.getTag();
            PictureStream picture = info.getCoverArt();

            module.setAuthor(info.getAuthor());
            module.setTitle(info.getTitle());
            module.setAlbum(info.getAlbum());
            module.setAdvisory(toDTO(info.getRating()));
            module.setPublisher(info.getPublisher());
            module.setCopyright(info.getCopyright());
            module.setDescription(info.getDescription());
            module.setGenre(info.getGenre());
            module.setYearOfRelease(info.getDateOfRelease());
            //Cover art
            module.setEmbeddedCoverArt(picture != null);
            if (picture != null && picture.getContainer() != null) {
                PictureFileInfo pictureContainer = picture.getContainer();
                module.setCoverArtFormat(toDTO(pictureContainer.getFormat()));
                module.setCoverArtURL(
                        getDatastoreService().getGWTDraftPictureDownloadURLEndpointForUser(id));
            }

        }
        return module;
    }

    @Override
    public ContainerModule toContainerDTO(Track track) throws Exception {
        if (track != null && track.getTrackContainer() != null) {
            AudioFileInfo info = track.getTrackContainer();
            ContainerModule container = new ContainerModule();

            //Format
            container.setFormat(toDTO(info.getFormat()));

            //metadata
            container.setBitrate(info.getBitrate());
            container.setChannels(info.getChannels());
            container.setDuration(new JSDuration(info.getDuration()));
            container.setSamplerate(info.getSamplerate());
            container.setSize(info.getSize());
            container.setVbr(info.isVbr());

            //Download info
            container.setURL(
                    getDatastoreService().getGWTTrackDownloadURLEndpointForUser(track.getRefID()));

            return container;
        }
        return null;
    }

    @Override
    public ContainerModule toContainerDTO(TrackStream trackStream, String id, boolean isDraft) throws Exception {
        if (trackStream != null && trackStream.getContainer() != null) {
            AudioFileInfo info = trackStream.getContainer();
            ContainerModule container = new ContainerModule();

            //Format
            container.setFormat(toDTO(info.getFormat()));

            //metadata
            container.setBitrate(info.getBitrate());
            container.setChannels(info.getChannels());
            container.setDuration(new JSDuration(info.getDuration()));
            container.setSamplerate(info.getSamplerate());
            container.setSize(info.getSize());
            container.setVbr(info.isVbr());

            //Download info
            container.setURL(//
                isDraft ?//                
                        getDatastoreService().getGWTDraftTrackDownloadURLEndpointForUser(id)//
                    ://
                        getDatastoreService().getGWTTrackDownloadURLEndpointForUser(id)//
            );

            return container;
        }
        return null;
    }

    @Override
    public CoverArtModule toCoverArtDTO(Track track) throws Exception {
        if (track != null && track.getCoverArtContainer() != null) {
            PictureFileInfo info = track.getCoverArtContainer();
            CoverArtModule container = new CoverArtModule();

            //Format
            container.setFormat(toDTO(info.getFormat()));

            //metadata
            container.setSize(info.getSize());

            //Download info
            container.setURL(
                    getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(track.getRefID()));

            return container;
        }
        return null;
    }

    @Override
    public CoverArtModule toCoverArtDTO(PictureStream pictureStream, String id, boolean isDraft) throws Exception {
        if (pictureStream != null && pictureStream.getContainer() != null) {
            PictureFileInfo info = pictureStream.getContainer();
            CoverArtModule container = new CoverArtModule();

            //Format
            container.setFormat(toDTO(info.getFormat()));

            //metadata
            container.setSize(info.getSize());

            //Download info
            container.setURL(//
                isDraft ?//                
                        getDatastoreService().getGWTDraftPictureDownloadURLEndpointForUser(id)//
                    ://
                        getDatastoreService().getGWTCoverArtDownloadURLEndpointForUser(id)//
            );

            return container;
        }
        return null;
    }

    public static BroadcastTrackProgrammeModule toBroadcastDTO(TrackProgramme trackProgramme) {
        if (trackProgramme != null && trackProgramme.getProgramme() != null) {
            BroadcastTrackProgrammeModule module = new BroadcastTrackProgrammeModule();
            module.setProgramme(trackProgramme.getProgramme().getLabel());
            module.setRotationCount(trackProgramme.getRotation()!=null ? trackProgramme.getRotation() : 0);
            return module;
        }
        return null;
    }

    public static PendingTrackProgrammeModule toPendingDTO(TrackProgramme trackProgramme) {
        if (trackProgramme != null && trackProgramme.getProgramme() != null) {
            PendingTrackProgrammeModule module = new PendingTrackProgrammeModule();
            module.setProgramme(trackProgramme.getProgramme().getLabel());
            return module;
        }
        return null;
    }

    @Override
    public ActionCollectionEntry<? extends TrackForm> toDTO(Track track) throws Exception {
        if (track == null) {
            return null;
        }
        DTOTrackVisitor visitor = new DTOTrackVisitor() {

            private ActionCollectionEntry<? extends TrackForm> form;

            @Override
            public ActionCollectionEntry<? extends TrackForm> get() {
                return form;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                form = toDTO(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                form = toDTO(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                form = toDTO(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                form = toDTO(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                form = toDTO(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                form = toDTO(track);
            }
        };
        track.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public ActionCollectionEntry<? extends RollbackTrackForm> toRollbackDTO(Track track) throws Exception {
        if (track == null) {
            return null;
        }
        RollbackDTOTrackVisitor visitor = new RollbackDTOTrackVisitor() {

            private ActionCollectionEntry<? extends RollbackTrackForm> form;

            @Override
            public ActionCollectionEntry<? extends RollbackTrackForm> get() {
                return form;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                form = toRollbackDTO(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                form = toRollbackDTO(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                form = toRollbackDTO(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                form = toRollbackDTO(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                form = toRollbackDTO(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                form = toRollbackDTO(track);
            }
        };
        track.acceptVisit(visitor);
        return visitor.get();
    }

    private ActionMapEntry<? extends BroadcastTrackForm> compose(BroadcastTrackForm broadcastTrackForm, BroadcastableTrack broadcastTrack) throws Exception {
                //Fill in common properties
                broadcastTrackForm.setRefID(broadcastTrack.getRefID());
                broadcastTrackForm.setCreatorId(broadcastTrack.getCreator() != null ? broadcastTrack.getCreator().getUsername() : null);
                broadcastTrackForm.setLatestEditorId(broadcastTrack.getLatestEditor() != null ? broadcastTrack.getLatestEditor().getUsername() : null);
                broadcastTrackForm.setCreationDate(broadcastTrack.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(broadcastTrack.getCreationDate().getTime(), null)) : null);
                broadcastTrackForm.setLatestModificationDate(broadcastTrack.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(broadcastTrack.getLatestModificationDate().getTime(), null)) : null);

                //Fill in all attributes
                broadcastTrackForm.setActivated(broadcastTrack.isEnabled());
                broadcastTrackForm.setReady(broadcastTrack.isReady());
                broadcastTrackForm.setNowPlaying(!broadcastTrack.getStreamItems().isEmpty());

                //broadcastTrackForm.setRotationCount(broadcastTrack.getRotation());

                TagModule commonTags = new TagModule();
                commonTags.setAdvisory(toDTO(broadcastTrack.getRating()));
                commonTags.setAlbum(broadcastTrack.getAlbum());
                commonTags.setAuthor(broadcastTrack.getAuthor());
                commonTags.setPublisher(broadcastTrack.getPublisher());
                commonTags.setCopyright(broadcastTrack.getCopyright());
                commonTags.setDescription(broadcastTrack.getDescription());
                commonTags.setGenre(broadcastTrack.getGenre());
                commonTags.setTag(broadcastTrack.getTag());
                commonTags.setTitle(broadcastTrack.getTitle());
                commonTags.setYearOfRelease(broadcastTrack.getDateOfRelease());
                broadcastTrackForm.setMetadata(commonTags);

                if (broadcastTrack.getTrackContainer() != null) {
                    broadcastTrackForm.setTrackFile(
                            new ActionMapEntry<ContainerModule>(
                                toContainerDTO(broadcastTrack),
                                decorateEntryWithActions(broadcastTrack)));
                }
                if (broadcastTrack.getCoverArtContainer() != null) {
                    broadcastTrackForm.setCoverArtFile(
                            new ActionMapEntry<CoverArtModule>(
                                toCoverArtDTO(broadcastTrack),
                                decorateEntryWithActions(broadcastTrack)));
                }

                //Programmes
                ActionMap<BroadcastTrackProgrammeModule> programmeEntries = new ActionSortedMap<BroadcastTrackProgrammeModule>();
                if (broadcastTrack.getTrackProgrammes() != null) {
                    for (TrackProgramme trackProgramme : broadcastTrack.getTrackProgrammes()) {
                        programmeEntries.set(toBroadcastDTO(trackProgramme), decorateEntryWithActions(trackProgramme.getProgramme()));
                    }
                }
                broadcastTrackForm.setTrackProgrammes(programmeEntries);

                return new ActionMapEntry<BroadcastTrackForm>(broadcastTrackForm, decorateEntryWithActions(broadcastTrack));
    }

    @Override
    public ActionCollectionEntry<BroadcastTrackForm> toDTO(BroadcastableTrack broadcastTrack) throws Exception {
        if (broadcastTrack == null) {
            return null;
        }

        DTOTrackVisitor visitor = new DTOTrackVisitor() {

            ActionCollectionEntry<BroadcastTrackForm> form;

            @Override
            public ActionCollectionEntry<? extends TrackForm> get() {
                return form;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                //Donothing
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                //Donothing
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                //Donothing
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                BroadcastAdvertForm broadcastTrackForm = new BroadcastAdvertForm();
                form = (ActionCollectionEntry<BroadcastTrackForm>) compose(broadcastTrackForm, track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                BroadcastJingleForm broadcastTrackForm = new BroadcastJingleForm();
                form = (ActionCollectionEntry<BroadcastTrackForm>) compose(broadcastTrackForm, track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                BroadcastSongForm broadcastTrackForm = new BroadcastSongForm();


                //Fill in the average score for the song
                AverageVoteModule avgRating = new AverageVoteModule();
                Float avgScore = track.getAverageVote();
                avgRating.setAverageScore(
                        avgScore);
                avgRating.setNumberOfVotes(
                        track.getVotes().size());
                broadcastTrackForm.setAverageVote(avgRating);

                form = (ActionCollectionEntry<BroadcastTrackForm>) compose(broadcastTrackForm, track);

            }
        };
        broadcastTrack.acceptVisit(visitor);
        return (ActionCollectionEntry<BroadcastTrackForm>) visitor.get();

    }

    private ActionMapEntry<? extends PendingTrackForm> compose(PendingTrackForm pendingTrackForm, PendingTrack pendingTrack) throws Exception {

        if (ProxiedClassUtil.castableAs(pendingTrack, PendingSong.class)) {
            pendingTrackForm.setType(TYPE.song);
        } else if (ProxiedClassUtil.castableAs(pendingTrack, PendingAdvert.class)) {
            pendingTrackForm.setType(TYPE.advert);
        } else if (ProxiedClassUtil.castableAs(pendingTrack, PendingJingle.class)) {
            pendingTrackForm.setType(TYPE.jingle);
        } else {
            return null;
        }

        //Fill in common properties
        pendingTrackForm.setRefID(pendingTrack.getRefID());
        pendingTrackForm.setCreatorId(pendingTrack.getCreator() != null ? pendingTrack.getCreator().getUsername() : null);
        pendingTrackForm.setLatestEditorId(pendingTrack.getLatestEditor() != null ? pendingTrack.getLatestEditor().getUsername() : null);
        pendingTrackForm.setCreationDate(pendingTrack.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(pendingTrack.getCreationDate().getTime(), null)) : null);
        pendingTrackForm.setLatestModificationDate(pendingTrack.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(pendingTrack.getLatestModificationDate().getTime(), null)) : null);

        //Fill in all attributes
        pendingTrackForm.setReady(pendingTrack.isReady());

        TagModule commonTags = new TagModule();
        commonTags.setAdvisory(toDTO(pendingTrack.getRating()));
        commonTags.setAlbum(pendingTrack.getAlbum());
        commonTags.setAuthor(pendingTrack.getAuthor());
        commonTags.setPublisher(pendingTrack.getPublisher());
        commonTags.setCopyright(pendingTrack.getCopyright());
        commonTags.setDescription(pendingTrack.getDescription());
        commonTags.setGenre(pendingTrack.getGenre());
        commonTags.setTag(pendingTrack.getTag());
        commonTags.setTitle(pendingTrack.getTitle());
        commonTags.setYearOfRelease(pendingTrack.getDateOfRelease());
        pendingTrackForm.setMetadata(commonTags);

        if (pendingTrack.getTrackContainer() != null) {
            pendingTrackForm.setTrackFile(
                    new ActionMapEntry<ContainerModule>(
                        toContainerDTO(pendingTrack),
                        decorateEntryWithActions(pendingTrack)));
        }
        if (pendingTrack.getCoverArtContainer() != null) {
            pendingTrackForm.setCoverArtFile(
                    new ActionMapEntry<CoverArtModule>(
                        toCoverArtDTO(pendingTrack),
                        decorateEntryWithActions(pendingTrack)));
        }

        //Programmes
        ActionMap<PendingTrackProgrammeModule> programmeEntries = new ActionSortedMap<PendingTrackProgrammeModule>();
        if (pendingTrack.getTrackProgrammes() != null) {
            for (TrackProgramme programme : pendingTrack.getTrackProgrammes()) {
                programmeEntries.set(toPendingDTO(programme), decorateEntryWithActions(programme.getProgramme()));
            }
        }
        pendingTrackForm.setTrackProgrammes(programmeEntries);

        return new ActionMapEntry<PendingTrackForm>(pendingTrackForm, decorateEntryWithActions(pendingTrack));
    }

    @Override
    public ActionCollectionEntry<PendingTrackForm> toDTO(PendingTrack pendingTrack) throws Exception {
        if (pendingTrack == null) {
            return null;
        }

        PendingTrackForm pendingTrackForm = new PendingTrackForm();
        return (ActionCollectionEntry<PendingTrackForm>) compose(pendingTrackForm, pendingTrack);
    }

    @Override
    public ActionCollection<? extends BroadcastTrackForm> toBroadcastDTO(Collection<? extends BroadcastableTrack> broadcastTracks, boolean doSort) throws Exception {
        ActionMap<BroadcastTrackForm> broadcastTrackForms = doSort ? new ActionSortedMap<BroadcastTrackForm>() : new ActionUnsortedMap<BroadcastTrackForm>();
        for (BroadcastableTrack broadcastTrack : broadcastTracks) {
            broadcastTrackForms.set(toDTO(broadcastTrack));
        }
        return broadcastTrackForms;
    }

    @Override
    public ActionCollection<? extends PendingTrackForm> toPendingDTO(Collection<? extends PendingTrack> pendingTracks, boolean doSort) throws Exception {
        ActionMap<PendingTrackForm> pendingTrackForms = doSort ? new ActionSortedMap<PendingTrackForm>() : new ActionUnsortedMap<PendingTrackForm>();
        for (PendingTrack pendingTrack : pendingTracks) {
            pendingTrackForms.set(toDTO(pendingTrack));
        }
        return pendingTrackForms;
    }

    protected Collection<ACTION> decorateVOTEWithActions(BroadcastableSong song) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //At the moment securityManager allows every restricteduser to view and edit their own vote
        actions.add(BASIC_ACTION.READ);
        actions.add(BASIC_ACTION.EDIT);
        actions.add(BASIC_ACTION.DELETE);
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(BroadcastableTrack track) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getTrackManager().checkAccessViewBroadcastTrack(track)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessExtendedViewBroadcastTrack(track)) {
                actions.add(TRACK_ACTION.EXTENDED_VIEW);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessUpdateBroadcastTrack(track)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessDeleteBroadcastTrack(track)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(PendingTrack track) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getTrackManager().checkAccessViewPendingTrack(track)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessExtendedViewPendingTrack(track)) {
                actions.add(TRACK_ACTION.EXTENDED_VIEW);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessReviewPendingTrack(track)) {
                actions.add(TRACK_ACTION.REVIEW);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessUpdatePendingTrack(track)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getTrackManager().checkAccessDeletePendingTrack(track)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Programme programme) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewProgramme(programme)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Curators and Contributors for linking programmes to tracks, they cannot edit a programme yet they can bind tracks
        try {
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programme)) {
                actions.add(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    /**
     * Return all channels linked to this entity, either directly or indirectly
     * The returned collection is not mutable
     * @return
     */
    @Override
    public Collection<Channel> getImmutableChannelSet(TrackForm track) throws Exception {
        Set<Channel> boundChannels = new HashSet<Channel>();
        if (track != null) {
            Collection<Programme> boundProgrammes = getProgrammeManager().getProgrammes(track.getImmutableProgrammes().keySet());
            for (Programme boundProgramme : boundProgrammes) {
                boundChannels.addAll(boundProgramme.getChannels());
            }
        }
        return boundChannels;
    }

    @Override
    public Collection<Programme> getImmutableProgrammeSet(TrackForm track) throws Exception {
        if (track != null) {
            return getProgrammeManager().getProgrammes(track.getImmutableProgrammes().keySet());
        } else {
            return new HashSet<Programme>();
        }

    }

    @Override
    public ActionCollectionEntry<? extends RollbackBroadcastTrackForm> toRollbackDTO(BroadcastableTrack broadcastTrack) throws Exception {
        //No extra data for most objects, use the default fallback
        RollbackDTOTrackVisitor visitor = new RollbackDTOTrackVisitor() {

            ActionCollectionEntry<? extends RollbackBroadcastTrackForm> form;
            @Override
            public ActionCollectionEntry<? extends RollbackTrackForm> get() {
                return form;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                RollbackBroadcastAdvertForm broadcastTrackForm = new RollbackBroadcastAdvertForm();
                form = (ActionCollectionEntry<RollbackBroadcastAdvertForm>) compose(broadcastTrackForm, track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                RollbackBroadcastJingleForm broadcastTrackForm = new RollbackBroadcastJingleForm();
                form = (ActionCollectionEntry<RollbackBroadcastJingleForm>) compose(broadcastTrackForm, track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                RollbackBroadcastSongForm f = new RollbackBroadcastSongForm();

                Collection<VoteModule> voteEntries = new HashSet<VoteModule>();
                if (track.getVotes() != null) {
                    for (Voting vote : track.getVotes()) {
                        voteEntries.add(new VoteModule(//
                                vote.getRestrictedUser().getUsername(),//
                                toDTO(vote.getValue())//
                                ));
                    }
                }
                f.setVotes(voteEntries);

                Collection<RequestModule> requestEntries = new HashSet<RequestModule>();
                if (track.getRequests() != null) {
                    for (Request request : track.getRequests()) {
                        requestEntries.add(new RequestModule(//
                                request.getChannel().getLabel(),//
                                request.getRequester().getUsername(),//
                                request.getMessage(),//
                                request.getStreamItem() != null,//
                                request.getQueueItem() != null//
                                ));
                    }
                }
                f.setRequests(requestEntries);
                form = (ActionCollectionEntry<? extends RollbackBroadcastTrackForm>) compose(f, track);
            }
        };
        broadcastTrack.acceptVisit(visitor);
        return (ActionCollectionEntry<? extends RollbackBroadcastTrackForm>) visitor.get();
    }

    @Override
    public ActionCollectionEntry<RollbackPendingTrackForm> toRollbackDTO(PendingTrack pendingTrack) throws Exception {
        if (pendingTrack == null) {
            return null;
        }

        RollbackPendingTrackForm pendingTrackForm = new RollbackPendingTrackForm();
        return (ActionCollectionEntry<RollbackPendingTrackForm>) compose(pendingTrackForm, pendingTrack);
    }
}
