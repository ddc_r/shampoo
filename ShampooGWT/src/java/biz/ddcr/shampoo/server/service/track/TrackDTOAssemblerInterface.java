/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackBroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackPendingTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackTrackForm;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface TrackDTOAssemblerInterface {
    
    //DTO to domain conversion
    public Track fromDTO(TrackForm trackForm) throws Exception;
    public BroadcastableTrack fromDTO(BroadcastTrackForm broadcastTrackForm) throws Exception;
    public PendingTrack fromDTO(PendingTrackForm pendingTrackForm) throws Exception;
    public boolean augmentWithDTO(Track track, TrackForm trackForm) throws Exception;
    public boolean augmentWithDTO(BroadcastableTrack broadcastTrack, BroadcastTrackForm broadcastTrackForm) throws Exception;
    public boolean augmentWithDTO(PendingTrack pendingTrack, PendingTrackForm pendingTrackForm) throws Exception;
    //Special handling for rollback forms
    public boolean augmentWithRollbackDTO(Track track, RollbackTrackForm trackForm) throws Exception;
    public boolean augmentWithRollbackDTO(BroadcastableTrack broadcastTrack, RollbackBroadcastTrackForm broadcastTrackForm) throws Exception;
    public boolean augmentWithRollbackDTO(PendingTrack pendingTrack, RollbackPendingTrackForm pendingTrackForm) throws Exception;


    //Domain to DTO conversion
    public ActionCollectionEntry<? extends TrackForm> toDTO(Track track) throws Exception;
    public ActionCollectionEntry<BroadcastTrackForm> toDTO(BroadcastableTrack broadcastTrack) throws Exception;
    public ActionCollectionEntry<PendingTrackForm> toDTO(PendingTrack pendingTrack) throws Exception;
    //Special handling for rollback forms
    public ActionCollectionEntry<? extends RollbackTrackForm> toRollbackDTO(Track track) throws Exception;
    public ActionCollectionEntry<? extends RollbackBroadcastTrackForm> toRollbackDTO(BroadcastableTrack broadcastTrack) throws Exception;
    public ActionCollectionEntry<RollbackPendingTrackForm> toRollbackDTO(PendingTrack pendingTrack) throws Exception;

    //Helpers
    public ActionCollection<? extends BroadcastTrackForm> toBroadcastDTO(Collection<? extends BroadcastableTrack> broadcastTracks, boolean doSort) throws Exception;
    public ActionCollection<? extends PendingTrackForm> toPendingDTO(Collection<? extends PendingTrack> pendingTracks, boolean doSort) throws Exception;
    public Collection<Channel> getImmutableChannelSet(TrackForm track) throws Exception;
    public Collection<Programme> getImmutableProgrammeSet(TrackForm track) throws Exception;
    public EmbeddedTagModule toTagDTO(Track track) throws Exception;
    public EmbeddedTagModule toDraftTagDTO(TrackStream trackStream, String id) throws Exception;

    public ContainerModule toContainerDTO(Track track) throws Exception;
    public ContainerModule toContainerDTO(TrackStream trackStream, String id, boolean isDraft) throws Exception;
    public CoverArtModule toCoverArtDTO(Track track) throws Exception;
    public CoverArtModule toCoverArtDTO(PictureStream pictureStream, String id, boolean isDraft) throws Exception;

}
