/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.RollbackBroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackPendingTrackForm;
import biz.ddcr.shampoo.client.form.track.RollbackTrackForm;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.TrackInQueueException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotReadyException;
import biz.ddcr.shampoo.client.helper.errors.TrackNowPlayingException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.playlist.Playlist;
import biz.ddcr.shampoo.server.domain.playlist.StaticPlaylistEntry;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeBroadcastableTrackLinkNotification.PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammePendingTrackLinkNotification.PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.queue.SimpleTrackQueueItem;
import biz.ddcr.shampoo.server.domain.streamer.SimpleTrackQueueStreamItem;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.track.Request;
import biz.ddcr.shampoo.server.domain.track.notification.BroadcastableTrackContentNotification.BROADCASTABLETRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.track.notification.PendingTrackContentNotification.PENDINGTRACK_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.track.log.TrackLog.TRACK_LOG_OPERATION;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.user.UserManagerInterface;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.TrackProgramme;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.domain.track.log.TrackLog;
import biz.ddcr.shampoo.server.domain.track.notification.BroadcastableTrackContentNotification;
import biz.ddcr.shampoo.server.domain.track.notification.PendingTrackContentNotification;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.helper.MicroTask;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.helper.TaskCallback;
import biz.ddcr.shampoo.server.helper.TaskPerformer;
import biz.ddcr.shampoo.server.helper.UUIDHelper;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnit;
import biz.ddcr.shampoo.server.helper.FinalBoolean;
import biz.ddcr.shampoo.server.service.helper.DatastoreService;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import biz.ddcr.shampoo.server.service.track.TrackManagerInterface.TrackFiles;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import org.springframework.core.task.TaskExecutor;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 **/
public class TrackFacadeService extends GenericService implements TrackFacadeServiceInterface {

    private interface BroadcastConverterTrackVisitor extends TrackVisitor {

        public BroadcastableTrack get();
    }

    /**
     * Internal interim struct
     **/
    protected class AsynchronousDeleteOperation extends AsynchronousUnmanagedTransactionUnit<Track> implements AsynchronousDeleteTrackTransactionInterface {

        AsynchronousDeleteOperation() throws Exception {
            super();
            //Init
            idsAndBackups = new HashMap<String, RollbackTrackForm>();
        }
        protected Map<String, RollbackTrackForm> idsAndBackups;

        @Override
        public void add(String id) {
            if (id != null) {
                idsAndBackups.put(id, null);
            }
        }

        @Override
        public void init() throws Exception {
            //Mark all tracks to delete as unavailable
            clearTransactions();
            synchronousSafePreDeleteTracks(idsAndBackups);
        }

        @Override
        public void processing() throws Exception {
            //Remove all files attached to the tracks that have no remaining programme attached to them
            //otherwise just reset their activation state
            addTransactions(synchronousSafePostDeleteTracks(idsAndBackups));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to do, really
        }

        @Override
        public void rollback(Exception originalException) {
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Rollback any modification performed during initialization() and mark them as ready
                Collection<Track> cleansedTracks = new HashSet<Track>();
                for (RollbackTrackForm backup : idsAndBackups.values()) {
                    Track contaminatedTrack = getTrackManager().loadTrack(backup.getRefID());
                    getTrackDTOAssembler().augmentWithRollbackDTO(contaminatedTrack, backup);
                    contaminatedTrack.setReady(true);
                    cleansedTracks.add(contaminatedTrack);
                }
                getTrackManager().updateTracks(cleansedTracks);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    protected class AsynchronousAddOperation extends AsynchronousUnmanagedTransactionUnit<Track> implements AsynchronousAddTrackTransactionInterface {

        AsynchronousAddOperation() throws Exception {
            super();
            //Init
            files = new HashMap<String, TrackFiles>();
            tracks = new HashSet<TrackForm>();
        }
        protected Map<String, TrackFiles> files;
        protected Collection<TrackForm> tracks;

        @Override
        public void add(TrackForm track, String trackUploadId, String coverArtUploadId) {
            //if trackUploadId or coverArtUploadId is null, it means the corresponding entry for the track has to be removed
            if (track != null) {
                //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
                if (track.getRefID() == null) {
                    track.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }
                TrackFiles newTrackFile = new TrackFiles(
                        true, trackUploadId,
                        true, coverArtUploadId);
                files.put(track.getRefID(), newTrackFile);
                tracks.add(track);
            }
        }

        @Override
        public void add(TrackForm track, String trackUploadId) {
            //if trackUploadId or coverArtUploadId is null, it means the corresponding entry for the track has to be removed
            //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
            if (track != null) {
                if (track.getRefID() == null) {
                    track.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }                
                TrackFiles newTrackFile = new TrackFiles(
                        true, trackUploadId,
                        false, null);
                files.put(track.getRefID(), newTrackFile);
                tracks.add(track);
            }
        }

        @Override
        public void add(TrackForm track) {
            //Make sure there is always an identifier attached to a track, otherwise every asynchronous process will break!
            if (track != null) {            
                if (track.getRefID() == null) {
                    track.setRefID(UUIDHelper.toShortenString(UUIDHelper.getRandomUUID()));
                }
                files.put(track.getRefID(), null);
                tracks.add(track);
            }
        }

        @Override
        public void init() throws Exception {
            clearTransactions();
            synchronousSafePreAddTracks(tracks);
        }

        @Override
        public void processing() throws Exception {
            //Add and bind all files to the tracks that have just been inserted
            addTransactions(synchronousSafePostAddTracks(files));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to perform
        }

        @Override
        public void rollback(Exception originalException) {
            //Cancel the persistance of the changes in the datastore
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Remove all files that have been inserted during initialization()
                Collection<? extends Track> tracksToUpdate = getTrackManager().loadTracks(files.keySet());
                getTrackManager().deleteTracks(tracksToUpdate);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    protected class AsynchronousUpdateOperation extends AsynchronousUnmanagedTransactionUnit<Track> implements AsynchronousUpdateTrackTransactionInterface {

        AsynchronousUpdateOperation() throws Exception {
            super();
            //Init
            files = new HashMap<String, TrackFiles>();
            tracks = new HashSet<TrackForm>();
            idsAndBackups = new HashMap<String, RollbackTrackForm>();
        }
        protected Map<String, TrackFiles> files;
        protected Collection<TrackForm> tracks;
        protected Map<String, RollbackTrackForm> idsAndBackups;
        protected Collection<String> contentUpdatedIds;

        @Override
        public void add(TrackForm track, String trackUploadId, String coverArtUploadId) {
            //if trackUploadId or coverArtUploadId is null, it means the corresponding entry for the track has to be removed
            if (track != null) {
                tracks.add(track);
                TrackFiles newTrackFile = new TrackFiles(
                        true, trackUploadId,
                        true, coverArtUploadId);
                files.put(track.getRefID(), newTrackFile);
            }
        }

        @Override
        public void addPictureUnchanged(TrackForm track, String trackUploadId) {
            //if trackUploadId or coverArtUploadId is null, it means the corresponding entry for the track has to be removed
            if (track != null) {
                tracks.add(track);
                TrackFiles newTrackFile = new TrackFiles(
                        true, trackUploadId,
                        false, null);
                files.put(track.getRefID(), newTrackFile);
            }
        }

        @Override
        public void addAudioUnchanged(TrackForm track, String coverArtUploadId) {
            //if trackUploadId or coverArtUploadId is null, it means the corresponding entry for the track has to be removed
            if (track != null) {
                tracks.add(track);
                TrackFiles newTrackFile = new TrackFiles(
                        false, null,
                        true, coverArtUploadId);
                files.put(track.getRefID(), newTrackFile);
            }
        }

        @Override
        public void add(TrackForm track) {
            if (track != null) {
                tracks.add(track);
                files.put(track.getRefID(), null);
            }
        }

        @Override
        public void init() throws Exception {
            clearTransactions();
            //Mark all tracks to update as unavailable
            contentUpdatedIds = synchronousSafePreUpdateTracks(tracks, idsAndBackups, files);
        }

        @Override
        public void processing() throws Exception {
            //Add and bind all files to the tracks that have just been inserted
            addTransactions(synchronousSafePostUpdateTracks(idsAndBackups, files, contentUpdatedIds));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to perform
        }

        @Override
        public void rollback(Exception originalException) {
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Rollback any modification performed during initialization() and mark them as ready
                Collection<Track> cleansedTracks = new HashSet<Track>();
                for (RollbackTrackForm backup : idsAndBackups.values()) {
                    Track contaminatedTrack = getTrackManager().loadTrack(backup.getRefID());
                    getTrackDTOAssembler().augmentWithRollbackDTO(contaminatedTrack, backup);
                    contaminatedTrack.setReady(true);
                    cleansedTracks.add(contaminatedTrack);
                }
                getTrackManager().updateTracks(cleansedTracks);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    protected class AsynchronousValidateOperation extends AsynchronousUnmanagedTransactionUnit<Track> implements AsynchronousValidateTrackTransactionInterface {

        AsynchronousValidateOperation() throws Exception {
            super();
            //Init
            idsAndBackups = new HashMap<String, RollbackPendingTrackForm>();
            idsToRelocate = new HashMap<String, String>();
            idsToCopy = new HashMap<String, String>();
        }
        protected Map<String, RollbackPendingTrackForm> idsAndBackups;
        /** PendingTracks that will be deleted and rebuilt as Broadcastabletracks; key=original pending track, value=new broadcast track **/
        protected Map<String, String> idsToRelocate;
        /** PendingTracks that will be copied as BroadcastableTracks; key=original pending track, value=new broadcast track **/
        protected Map<String, String> idsToCopy;

        @Override
        public void add(String id) {
            if (idsAndBackups != null) {
                idsAndBackups.put(id, null);
            }
        }

        @Override
        public void init() throws Exception {
            clearTransactions();
            //Mark all source and destination tracks as unavailable and persist the changes
            synchronousSafePreValidatePendingTracks(idsAndBackups, idsToRelocate, idsToCopy);
        }

        @Override
        public void processing() throws Exception {
            //Add and bind all files to the tracks that have just been inserted
            addTransactions(synchronousSafePostValidatePendingTracks(idsAndBackups, idsToRelocate, idsToCopy));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Noting else to do
        }

        @Override
        public void rollback(Exception originalException) {
            //the rollback consists in two distinct steps:
            //the update of the original pending tracks from the rollback structures
            //and the deletion of the newly created broadcast tracks
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //First, rollback any modification performed during initialization() and mark them as ready
                Collection<Track> cleansedTracks = new HashSet<Track>();
                for (RollbackTrackForm backup : idsAndBackups.values()) {
                    Track contaminatedTrack = getTrackManager().loadTrack(backup.getRefID());
                    getTrackDTOAssembler().augmentWithRollbackDTO(contaminatedTrack, backup);
                    contaminatedTrack.setReady(true);
                    cleansedTracks.add(contaminatedTrack);
                }
                getTrackManager().updateTracks(cleansedTracks);
                //Then, remove all files that have been inserted during initialization()
                Collection<String> idsToRemove = new HashSet<String>();
                idsToRemove.addAll(idsToCopy.values());
                idsToRemove.addAll(idsToRelocate.values());
                Collection<? extends Track> tracksToDelete = getTrackManager().loadTracks(idsToRemove);

                //Persist changes
                getTrackManager().deleteTracks(tracksToDelete);
                getTrackManager().updateTracks(cleansedTracks);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };

    /**
     * At the moment, same as deletion
     * TODO: provide a *real* track rejection system
     */
    protected class AsynchronousRejectOperation extends AsynchronousUnmanagedTransactionUnit<Track> implements AsynchronousRejectTrackTransactionInterface {

        AsynchronousRejectOperation() throws Exception {
            super();
            //Init
            idsAndBackups = new HashMap<String, RollbackPendingTrackForm>();
            idsAndMessages = new HashMap<String, String>();
        }
        protected Map<String, RollbackPendingTrackForm> idsAndBackups;
        protected Map<String, String> idsAndMessages;

        @Override
        public void add(String id, String message) {
            if (id != null) {
                idsAndBackups.put(id, null);
                idsAndMessages.put(id, message);
            }
        }

        @Override
        public void init() throws Exception {
            //Mark all tracks to delete as unavailable
            clearTransactions();
            synchronousSafePreRejectTracks(idsAndBackups);
        }

        @Override
        public void processing() throws Exception {
            //Remove all files attached to the tracks that have no remaining programme attached to them
            //otherwise just reset their activation state
            addTransactions(synchronousSafePostRejectTracks(idsAndBackups, idsAndMessages));
        }

        @Override
        public void commit() {
            //Confirm the persistance of the changes in the datastore
            getDatastoreService().commitChangesInDataStore(getTransactions());
            //Nothing else to do, really
        }

        @Override
        public void rollback(Exception originalException) {
            getDatastoreService().rollbackChangesInDataStore(getTransactions());
            try {
                //Rollback any modification performed during initialization() and mark them as ready
                Collection<PendingTrack> cleansedTracks = new HashSet<PendingTrack>();
                for (RollbackPendingTrackForm backup : idsAndBackups.values()) {
                    PendingTrack contaminatedTrack = getTrackManager().loadPendingTrack(backup.getRefID());
                    getTrackDTOAssembler().augmentWithRollbackDTO(contaminatedTrack, backup);
                    contaminatedTrack.setReady(true);
                    cleansedTracks.add(contaminatedTrack);
                }
                getTrackManager().updateTracks(cleansedTracks);
            } catch (Exception ex) {
                logger.error("Manual rollback failed!", ex);
            }
        }
    };
    private TrackDTOAssemblerInterface trackDTOAssembler;
    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private TrackManagerInterface trackManager;
    private SecurityManagerInterface securityManager;
    //Run sequential steps in an asynchronous thread
    private transient TaskPerformer taskPerformer;
    private AsynchronousTrackFacadeServiceHelperInterface asynchronousTrackFacadeServiceHelper;
    private transient DatastoreService datastoreService;

    public DatastoreService getDatastoreService() {
        return datastoreService;
    }

    public void setDatastoreService(DatastoreService datastoreService) {
        this.datastoreService = datastoreService;
    }

    public TaskPerformer getTaskPerformer() {
        return taskPerformer;
    }

    public void setTaskPerformer(TaskPerformer taskPerformer) {
        this.taskPerformer = taskPerformer;
    }

    public TrackDTOAssemblerInterface getTrackDTOAssembler() {
        return trackDTOAssembler;
    }

    public void setTrackDTOAssembler(TrackDTOAssemblerInterface trackDTOAssembler) {
        this.trackDTOAssembler = trackDTOAssembler;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public TrackManagerInterface getTrackManager() {
        return trackManager;
    }

    public void setTrackManager(TrackManagerInterface trackManager) {
        this.trackManager = trackManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskPerformer = new TaskPerformer(taskExecutor);
    }

    public AsynchronousTrackFacadeServiceHelperInterface getAsynchronousTrackFacadeServiceHelper() {
        return asynchronousTrackFacadeServiceHelper;
    }

    public void setAsynchronousTrackFacadeServiceHelper(AsynchronousTrackFacadeServiceHelperInterface asynchronousTrackFacadeServiceHelper) {
        this.asynchronousTrackFacadeServiceHelper = asynchronousTrackFacadeServiceHelper;
    }

    /**
     *
     * Validation process for tracks
     * Incorrect values are replaced with default ones unless the problem is not recoverable, then an Exception is thrown
     **/
    private boolean checkTrack(Track track) throws Exception {
        boolean result = false;

        if (track != null) {

            result = true;
        }
        return result;
    }

    protected void startAsynchronousTask(AsynchronousUnmanagedTransactionUnitInterface<Track> asynchronousTrackOps) throws Exception {
        Collection<AsynchronousUnmanagedTransactionUnitInterface<Track>> asynchronousTrackOpsCollection = new HashSet<AsynchronousUnmanagedTransactionUnitInterface<Track>>();
        asynchronousTrackOpsCollection.add(asynchronousTrackOps);
        startAsynchronousTask(asynchronousTrackOpsCollection);
    }

    /**
     * MUST be called outside any transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    protected void startAsynchronousTask(final Collection<AsynchronousUnmanagedTransactionUnitInterface<Track>> asynchronousTrackOps) throws Exception {
        //No batch version of this method exists, process those tracks one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (asynchronousTrackOps != null) {

            //First step: initialization of units of work
            getAsynchronousTrackFacadeServiceHelper().synchronousInitialization(asynchronousTrackOps);

            //Second step: actual processing
            MicroTask loop = new MicroTask() {

                @Override
                public boolean loop() throws Exception {
                    getAsynchronousTrackFacadeServiceHelper().synchronousProcessing(asynchronousTrackOps);
                    return false;
                }
            };
            TaskCallback loopCallback = new TaskCallback() {

                @Override
                public void onSuccess() throws Exception {
                    getAsynchronousTrackFacadeServiceHelper().synchronousCommit(asynchronousTrackOps);
                }

                @Override
                public void onFailure(Exception e) throws Exception {
                    getAsynchronousTrackFacadeServiceHelper().synchronousRollback(asynchronousTrackOps, e);
                }
            };
            taskPerformer.runTask(loop, loopCallback);

        }
    }

    /**
     *
     * Pre-process a track for removal
     * Outputs true if this entity must be removed, false for updating it instead
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean processTrackForDeletion(Track track) throws Exception {
        //Flag to know whether to update or delete the track regarding how the checks end up
        final FinalBoolean mustBeRemoved = new FinalBoolean();
        mustBeRemoved.value = true;

        if (track != null) {

            TrackVisitor visitor = new TrackVisitor() {

                private void processPendingTrack(PendingTrack pendingTrack) throws Exception {
                    //Do nothing
                }

                private void processBroadcastTrack(BroadcastableTrack broadcastTrack) throws Exception {
                    if (mustBeRemoved.value) {
                        //Playlist compositions can be safely removed if the playlist can be unlinked as well

                        //for (StaticPlaylistEntry boundEntry : broadcastTrack.getSlots()) {
                        for (Iterator<StaticPlaylistEntry> i = broadcastTrack.getSlots().iterator(); i.hasNext();) {
                            StaticPlaylistEntry boundEntry = i.next();
                            Playlist playlist = boundEntry.getPlaylist();
                            if (playlist != null) {
                                if (getProgrammeManager().checkAccessUpdatePlaylist(playlist.getProgramme())) {
                                    //Okay we can unlink it
                                    i.remove();
                                    broadcastTrack.removeSlot(boundEntry);
                                }
                            }
                        }
                    }

                    //And everything else
                    //If you can remove a track, you can unlink those objects
                    broadcastTrack.clearSlots();

                }

                @Override
                public void visit(PendingAdvert track) throws Exception {
                    processPendingTrack(track);
                }

                @Override
                public void visit(PendingJingle track) throws Exception {
                    processPendingTrack(track);
                }

                @Override
                public void visit(PendingSong track) throws Exception {
                    processPendingTrack(track);
                }

                @Override
                public void visit(BroadcastableAdvert track) throws Exception {
                    processBroadcastTrack(track);
                }

                @Override
                public void visit(BroadcastableJingle track) throws Exception {
                    processBroadcastTrack(track);
                }

                @Override
                public void visit(BroadcastableSong track) throws Exception {
                    processBroadcastTrack(track);
                    track.getVotes().clear();
                    track.getRequests().clear();
                }
            };
            track.acceptVisit(visitor);

            //And now every programme directly bound to the track
            //If the programme can be touched then the whole track and all its dependencies are erased in one go
            //Don't throw an exception if a programme cannot be accessed, just remove it from the list and make sure the programme is updated, not removed actually
            for (Iterator<TrackProgramme> i = track.getTrackProgrammes().iterator(); i.hasNext();) {
                TrackProgramme boundTrackProgramme = i.next();
                //Use ViewExtended not just Update since curators and contributors can do it too, not just Programme Managers
                if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(boundTrackProgramme.getProgramme())
                        || getProgrammeManager().checkAccessUpdateProgramme(boundTrackProgramme.getProgramme())) {
                    //Drop every programmes bound to this track
                    i.remove();
                    track.removeTrackProgramme(boundTrackProgramme, false);
                } else {
                    mustBeRemoved.value = false;
                }
            }

        }

        return mustBeRemoved.value;
    }

    @Override
    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm) throws Exception {
        if (trackForm != null) {

            //Check if it can be accessed
            if (getTrackManager().checkAccessAddBroadcastTracks()) {
                //hydrate the entity from the DTO
                BroadcastableTrack track = trackDTOAssembler.fromDTO(trackForm);

                if (checkTrack(track)) {
                    //Everything's fine, now make it persistent in the database
                    try {
                        //Mark it as ready since no files will be attached
                        track.setReady(true);
                        getTrackManager().addTrack(track);
                        //Log it
                        log(TRACK_LOG_OPERATION.add, track);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        for (Programme boundProgramme : track.getImmutableProgrammes()) {
                            notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                        }
                        //TimetableSlots and PlaylistEntries are handled elsewhere
                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    public void addSecuredPendingTrack(PendingTrackForm trackForm) throws Exception {
        if (trackForm != null) {

            //Check if it can be accessed
            if (getTrackManager().checkAccessAddPendingTracks()) {
                //hydrate the entity from the DTO
                PendingTrack track = trackDTOAssembler.fromDTO(trackForm);

                if (checkTrack(track)) {
                    //Everything's fine, now make it persistent in the database
                    try {
                        //Mark it as ready since no files will be attached
                        track.setReady(true);
                        getTrackManager().addTrack(track);
                        //Log it
                        log(TRACK_LOG_OPERATION.add, track);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        for (Programme boundProgramme : track.getImmutableProgrammes()) {
                            notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                        }
                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }
        }
    }

    protected void synchronousSafePreAddTracks(Collection<? extends TrackForm> trackForms) throws Exception {
        if (trackForms != null) {
            Collection<BroadcastTrackForm> broadcastTrackForms = new HashSet<BroadcastTrackForm>();
            Collection<PendingTrackForm> pendingTrackForms = new HashSet<PendingTrackForm>();
            for (TrackForm trackForm : trackForms) {
                if (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class)) {
                    broadcastTrackForms.add((BroadcastTrackForm) trackForm);
                } else if (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class)) {
                    pendingTrackForms.add((PendingTrackForm) trackForm);
                }
            }
            if (!broadcastTrackForms.isEmpty()) {
                synchronousSafeAddBroadcastTracks(broadcastTrackForms);
            }
            if (!pendingTrackForms.isEmpty()) {
                synchronousSafeAddPendingTracks(pendingTrackForms);
            }
        }
    }

    protected void synchronousSafeAddBroadcastTracks(Collection<BroadcastTrackForm> trackForms) throws Exception {
        if (trackForms != null) {

            //Check if it can be accessed
            if (getTrackManager().checkAccessAddBroadcastTracks()) {

                Collection<BroadcastableTrack> addedTracks = new HashSet<BroadcastableTrack>();
                for (BroadcastTrackForm trackForm : trackForms) {
                    //hydrate the entity from the DTO
                    BroadcastableTrack track = trackDTOAssembler.fromDTO(trackForm);
                    if (checkTrack(track)) {
                        //Mark it as not ready
                        track.setReady(false);
                        //IMPORTANT: update the original form with the new id!
                        track.setRefID(trackForm.getRefID());
                        addedTracks.add(track);
                    }
                }
                //Everything's fine, now make it persistent in the database
                try {
                    //Add all tracks to insert
                    getTrackManager().addTracks(addedTracks);
                    //Log and notify only when the actual file copy has been performed
                } catch (DataIntegrityViolationException e) {
                    throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                }
            } else {
                throw new AccessDeniedException();
            }

        }

    }

    protected void synchronousSafeAddPendingTracks(Collection<PendingTrackForm> trackForms) throws Exception {
        if (trackForms != null) {

            //Check if it can be accessed
            if (getTrackManager().checkAccessAddPendingTracks()) {

                Collection<PendingTrack> addedTracks = new HashSet<PendingTrack>();
                for (PendingTrackForm trackForm : trackForms) {
                    //hydrate the entity from the DTO
                    PendingTrack track = trackDTOAssembler.fromDTO(trackForm);
                    if (checkTrack(track)) {
                        //Mark it as not ready
                        track.setReady(false);
                        //IMPORTANT: update the original form with the new id!
                        track.setRefID(trackForm.getRefID());
                        addedTracks.add(track);
                    }
                }
                //Everything's fine, now make it persistent in the database
                try {
                    //Add all tracks to insert
                    getTrackManager().addTracks(addedTracks);
                    //Log and notify only when the actual file copy has been performed
                } catch (DataIntegrityViolationException e) {
                    throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                }
            } else {
                throw new AccessDeniedException();
            }

        }

    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousAddSecuredBroadcastTrackAndAudioFile(BroadcastTrackForm trackForm, final String trackUploadId) throws Exception {
        asynchronousAddSecuredTrackAndAudioAndPictureFile(trackForm, trackUploadId, false, null);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousAddSecuredBroadcastTrackAndAudioAndPictureFile(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception {
        asynchronousAddSecuredTrackAndAudioAndPictureFile(trackForm, trackUploadId, true, coverArtUploadId);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousAddSecuredPendingTrackAndAudioFile(PendingTrackForm trackForm, final String trackUploadId) throws Exception {
        asynchronousAddSecuredTrackAndAudioAndPictureFile(trackForm, trackUploadId, false, null);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousAddSecuredPendingTrackAndAudioAndPictureFile(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception {
        asynchronousAddSecuredTrackAndAudioAndPictureFile(trackForm, trackUploadId, true, coverArtUploadId);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    protected void asynchronousAddSecuredTrackAndAudioAndPictureFile(final TrackForm trackForm, final String trackUploadId, final boolean doTransferPicture, final String coverArtUploadId) throws Exception {
        if (trackForm != null) {

            AsynchronousAddTrackTransactionInterface op = new AsynchronousAddOperation();
            if (doTransferPicture) {
                op.add(trackForm, trackUploadId, coverArtUploadId);
            } else {
                op.add(trackForm, trackUploadId);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    @Override
    public void asynchronousDeleteSecuredTrack(String trackId) throws Exception {
        if (trackId != null) {
            Collection<String> trackIds = new HashSet<String>();
            trackIds.add(trackId);
            asynchronousDeleteSecuredTracks(trackIds);
        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousDeleteSecuredTracks(Collection<String> trackIds) throws Exception {
        //No batch version of this method exists, process those tracks one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (trackIds != null) {

            AsynchronousDeleteTrackTransactionInterface op = new AsynchronousDeleteOperation();
            for (String trackId : trackIds) {
                op.add(trackId);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    protected void synchronousSafePreRejectTracks(Map<String, RollbackPendingTrackForm> trackIdsAndBackups) throws Exception {
        if (trackIdsAndBackups != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            Collection<PendingTrack> tracks = getTrackManager().loadPendingTracks(trackIdsAndBackups.keySet());
            Collection<PendingTrack> deletedTracks = new HashSet<PendingTrack>();
            for (PendingTrack track : tracks) {

                if (getTrackManager().checkAccessReviewPendingTrack(track) && isTrackTouchable(track, false)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<RollbackPendingTrackForm> rollbackCopyEntry = trackDTOAssembler.toRollbackDTO(track);
                    RollbackPendingTrackForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;
                    //Check if it can be accessed

                    //Then remove dependencies
                    processTrackForDeletion(track);

                    track.setReady(false);
                    deletedTracks.add(track);
                    //Update the referenced id list with rollback copies
                    trackIdsAndBackups.put(track.getRefID(), rollbackCopy);

                } else {
                    throw new AccessDeniedException();
                }

            }
            getTrackManager().updateTracks(deletedTracks);
        }

    }

    protected void synchronousSafePreDeleteTracks(Map<String, RollbackTrackForm> trackIdsAndBackups) throws Exception {
        if (trackIdsAndBackups != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            Collection<? extends Track> tracks = getTrackManager().loadTracks(trackIdsAndBackups.keySet());
            Collection<Track> deletedTracks = new HashSet<Track>();
            for (Track track : tracks) {

                if (getTrackManager().checkAccessDeleteTrack(track) && isTrackTouchable(track, true)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<? extends RollbackTrackForm> rollbackCopyEntry = trackDTOAssembler.toRollbackDTO(track);
                    RollbackTrackForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;
                    //Check if it can be accessed

                    //Then remove dependencies
                    processTrackForDeletion(track);

                    track.setReady(false);
                    deletedTracks.add(track);
                    //Update the referenced id list with rollback copies
                    trackIdsAndBackups.put(track.getRefID(), rollbackCopy);

                } else {
                    throw new AccessDeniedException();
                }

            }
            getTrackManager().updateTracks(deletedTracks);
        }

    }

    protected Collection<Integer> synchronousSafePostDeleteTracks(
            Map<String, RollbackTrackForm> trackIdsAndBackups) throws Exception {

        if (trackIdsAndBackups != null) {
            Collection<Integer> transactions = null;
            Collection<Track> tracksToDelete = new HashSet<Track>();
            Collection<Track> tracksToReset = new HashSet<Track>();

            final Collection<? extends Track> tracks = getTrackManager().loadTracks(trackIdsAndBackups.keySet());
            if (tracks != null) {
                for (Track track : tracks) {

                    //If no programme is bound then remove the track, otherwise just reset its activation state
                    if (track.getImmutableProgrammes().isEmpty()) {
                        tracksToDelete.add(track);
                        //Log it, use the rollback copies to regenerate the originally bound channel list
                        final RollbackTrackForm rollbackCopy = trackIdsAndBackups.get(track.getRefID());
                        log(TRACK_LOG_OPERATION.delete, track,
                                trackDTOAssembler.getImmutableChannelSet(rollbackCopy));
                        //Notify responsible users of any changes
                        //All links have been successfully removed
                        //use the rollback copy to know which one was originally linked to the track
                        TrackVisitor visitor = new TrackVisitor() {

                            @Override
                            public void visit(PendingAdvert track) throws Exception {
                                for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                    TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(PendingJingle track) throws Exception {
                                for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                    TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(PendingSong track) throws Exception {
                                for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                    TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(BroadcastableAdvert track) throws Exception {
                                for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                    TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(BroadcastableJingle track) throws Exception {
                                for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                    TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(BroadcastableSong track) throws Exception {
                                for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                    TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }
                        };
                        track.acceptVisit(visitor);
                    } else {
                        //reste its activation state
                        track.setReady(true);
                        tracksToReset.add(track);
                        //Log it, use the rollback copies to regenerate the originally bound channel list
                        final RollbackTrackForm rollbackCopy = trackIdsAndBackups.get(track.getRefID());
                        log(TRACK_LOG_OPERATION.edit, track,
                                trackDTOAssembler.getImmutableChannelSet(rollbackCopy));
                        //Notify responsible users of any changes
                        //All links have been successfully removed
                        //use the rollback copy to know which one was originally linked to the track
                        final Collection<Programme> currentProgrammes = track.getImmutableProgrammes();
                        TrackVisitor visitor = new TrackVisitor() {

                            @Override
                            public void visit(PendingAdvert track) throws Exception {
                                for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                    TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(PendingJingle track) throws Exception {
                                for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                    TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(PendingSong track) throws Exception {
                                for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                    TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(BroadcastableAdvert track) throws Exception {
                                for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                    TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(BroadcastableJingle track) throws Exception {
                                for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                    TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }

                            @Override
                            public void visit(BroadcastableSong track) throws Exception {
                                for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                    TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, track, boundProgramme);
                                }
                            }
                        };
                        track.acceptVisit(visitor);
                    }
                }
            }
            if (!tracksToDelete.isEmpty()) {
                transactions = getDatastoreService().unsetTrackFilesFromDatastore(tracksToDelete);
                getTrackManager().deleteTracks(tracksToDelete);
            }
            if (!tracksToReset.isEmpty()) {
                getTrackManager().updateTracks(tracksToReset);
            }
            return transactions;
        }
        return null;
    }

    protected Collection<Integer> synchronousSafePostRejectTracks(
            Map<String, RollbackPendingTrackForm> trackIdsAndBackups,
            Map<String, String> trackIdsAndMessages) throws Exception {

        if (trackIdsAndBackups != null && trackIdsAndMessages != null) {
            Collection<Integer> transactions = null;
            Collection<Track> tracksToDelete = new HashSet<Track>();
            Collection<Track> tracksToReset = new HashSet<Track>();

            final Collection<? extends PendingTrack> tracks = getTrackManager().loadPendingTracks(trackIdsAndBackups.keySet());
            if (tracks != null) {
                for (PendingTrack track : tracks) {

                    //If no programme is bound then remove the track, otherwise just reset its activation state
                    if (track.getImmutableProgrammes().isEmpty()) {
                        tracksToDelete.add(track);
                        //Log it, use the rollback copies to regenerate the originally bound channel list
                        RollbackPendingTrackForm rollbackCopy = trackIdsAndBackups.get(track.getRefID());
                        log(TRACK_LOG_OPERATION.reject, track,
                                trackDTOAssembler.getImmutableChannelSet(rollbackCopy));
                        //Notify responsible users of any changes
                        //All links have been successfully removed
                        //use the rollback copy to know which one was originally linked to the track
                        /*if (ProxiedClassUtil.castableAs(track, BroadcastableTrack.class)) {
                        //Hibernate proxies cannot be directly cast so reload the object with the proper type
                        //TODO do something clever with a visitor pattern
                        //FIX don't use session load() but get() within the DAO
                        for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete,  track, boundProgramme);
                        }
                        } else*/ {
                            //Get the relevant justification message
                            String rejectionMessage = trackIdsAndMessages.get(track.getRefID());
                            //Hibernate proxies cannot be directly cast so reload the object with the proper type
                            //TODO do something clever with a visitor pattern
                            //FIX don't use session load() but get() within the DAO
                            for (Programme boundProgramme : trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy)) {
                                notify(
                                        PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.reject,
                                        track,
                                        boundProgramme,
                                        rejectionMessage);
                            }
                        }
                    } else {
                        //reste its activation state
                        track.setReady(true);
                        tracksToReset.add(track);
                        //Log it, use the rollback copies to regenerate the originally bound channel list
                        RollbackPendingTrackForm rollbackCopy = trackIdsAndBackups.get(track.getRefID());
                        log(TRACK_LOG_OPERATION.reject, track,
                                trackDTOAssembler.getImmutableChannelSet(rollbackCopy));
                        //Notify responsible users of any changes
                        //All links have been successfully removed
                        //use the rollback copy to know which one was originally linked to the track
                        Collection<Programme> currentProgrammes = track.getImmutableProgrammes();
                        /*if (ProxiedClassUtil.castableAs(track, BroadcastableTrack.class)) {
                        //Hibernate proxies cannot be directly cast so reload the object with the proper type
                        //TODO do something clever with a visitor pattern
                        //FIX don't use session load() but get() within the DAO
                        for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete,  track, boundProgramme);
                        }
                        } else*/ {
                            //Get the relevant justification message
                            String rejectionMessage = trackIdsAndMessages.get(track.getRefID());
                            //Hibernate proxies cannot be directly cast so reload the object with the proper type
                            //TODO do something clever with a visitor pattern
                            //FIX don't use session load() but get() within the DAO
                            for (Programme boundProgramme : CollectionUtil.exclusion(currentProgrammes, trackDTOAssembler.getImmutableProgrammeSet(rollbackCopy))) {
                                notify(
                                        PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.reject,
                                        track,
                                        boundProgramme,
                                        rejectionMessage);
                            }
                        }
                    }
                }
            }
            if (!tracksToDelete.isEmpty()) {
                transactions = getDatastoreService().unsetTrackFilesFromDatastore(tracksToDelete);
                getTrackManager().deleteTracks(tracksToDelete);
            }
            if (!tracksToReset.isEmpty()) {
                getTrackManager().updateTracks(tracksToReset);
            }
            return transactions;
        }
        return null;
    }

    @Override
    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm) throws Exception {
        if (trackForm != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            BroadcastableTrack originalTrack = getTrackManager().loadBroadcastTrack(trackForm.getRefID());
            //Check if it can be accessed

            if (getTrackManager().checkAccessUpdateBroadcastTrack(originalTrack) && isTrackTouchable(originalTrack, false)) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = TRACK_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalTrack);
                //Save original links before modification (clones)
                Collection<Programme> originallyLinkedProgrammes = originalTrack.getImmutableProgrammes();

                //Update the existing entity from the DTO
                boolean areAttributesUpdated = trackDTOAssembler.augmentWithDTO(originalTrack, trackForm);

                //Check and adjust its dependencies before updating
                if (checkTrack(originalTrack)) {
                    //Mark it as ready since no files will be attached, nor modified
                    originalTrack.setReady(true);
                    //Everything's fine, now make it persistent in the database
                    getTrackManager().updateTrack(originalTrack);
                    //Log it
                    log(TRACK_LOG_OPERATION.edit, originalTrack, originallyBoundChannels);
                    //Notify responsible users of any changes
                    if (areAttributesUpdated) //Only users still linked to the track after update will receive this notification
                    {
                        notify(BROADCASTABLETRACK_NOTIFICATION_OPERATION.edit, originalTrack);
                    }
                    //New linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedProgrammes, originalTrack.getImmutableProgrammes())) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, originalTrack, boundProgramme);
                    }
                    //Old linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(originalTrack.getImmutableProgrammes(), originallyLinkedProgrammes)) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, originalTrack, boundProgramme);
                    }
                }
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void updateSecuredBroadcastTracks(Collection<BroadcastTrackForm> trackForms) throws Exception {
        if (trackForms != null) {

            Collection<BroadcastableTrack> tracksToUpdate = new HashSet<BroadcastableTrack>();
            for (BroadcastTrackForm trackForm : trackForms) {
                //Get the original version of this track
                //It will be this version that will be modified with data coming from the DTO
                BroadcastableTrack originalTrack = getTrackManager().loadBroadcastTrack(trackForm.getRefID());
                //Check if it can be accessed

                if (getTrackManager().checkAccessUpdateBroadcastTrack(originalTrack) && isTrackTouchable(originalTrack, false)) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = TRACK_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalTrack);
                    //Save original links before modification (clones)
                    Collection<Programme> originallyLinkedProgrammes = originalTrack.getImmutableProgrammes();

                    //Update the existing entity from the DTO
                    boolean areAttributesUpdated = trackDTOAssembler.augmentWithDTO(originalTrack, trackForm);

                    //Check and adjust its dependencies before updating
                    if (checkTrack(originalTrack)) {
                        //Mark it as ready since no files will be attached, nor modified
                        originalTrack.setReady(true);
                        //Everything's fine, now make it persistent in the database
                        tracksToUpdate.add(originalTrack);
                        //Log it
                        log(TRACK_LOG_OPERATION.edit, originalTrack, originallyBoundChannels);
                        //Notify responsible users of any changes
                        if (areAttributesUpdated) //Only users still linked to the track after update will receive this notification
                        {
                            notify(BROADCASTABLETRACK_NOTIFICATION_OPERATION.edit, originalTrack);
                        }
                        //New linked programmes
                        for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedProgrammes, originalTrack.getImmutableProgrammes())) {
                            notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, originalTrack, boundProgramme);
                        }
                        //Old linked programmes
                        for (Programme boundProgramme : CollectionUtil.exclusion(originalTrack.getImmutableProgrammes(), originallyLinkedProgrammes)) {
                            notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, originalTrack, boundProgramme);
                        }
                    }
                } else {
                    throw new AccessDeniedException();
                }

            }
            //Everything's fine, now make it persistent in the database
            if (!tracksToUpdate.isEmpty()) {
                getTrackManager().updateTracks(tracksToUpdate);
            }
        }
    }

    @Override
    public void updateSecuredPendingTrack(PendingTrackForm trackForm) throws Exception {
        if (trackForm != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            PendingTrack originalTrack = getTrackManager().loadPendingTrack(trackForm.getRefID());
            //Check if it can be accessed

            if (getTrackManager().checkAccessUpdatePendingTrack(originalTrack) && isTrackTouchable(originalTrack, false)) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = TRACK_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalTrack);
                //Save original links before modification (clones)
                Collection<Programme> originallyLinkedProgrammes = originalTrack.getImmutableProgrammes();

                //Update the existing entity from the DTO
                boolean areAttributesUpdated = trackDTOAssembler.augmentWithDTO(originalTrack, trackForm);

                //Check and adjust its dependencies before updating
                if (checkTrack(originalTrack)) {
                    //Mark it as ready since no files will be attached
                    originalTrack.setReady(true);
                    //Everything's fine, now make it persistent in the database
                    getTrackManager().updateTrack(originalTrack);
                    //Log it
                    log(TRACK_LOG_OPERATION.edit, originalTrack, originallyBoundChannels);
                    //Notify responsible users of any changes
                    if (areAttributesUpdated) //Only users still linked to the track after update will receive this notification
                    {
                        notify(PENDINGTRACK_NOTIFICATION_OPERATION.edit, originalTrack);
                    }
                    //New linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedProgrammes, originalTrack.getImmutableProgrammes())) {
                        notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, originalTrack, boundProgramme);
                    }
                    //Old linked programmes
                    for (Programme boundProgramme : CollectionUtil.exclusion(originalTrack.getImmutableProgrammes(), originallyLinkedProgrammes)) {
                        notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, originalTrack, boundProgramme);
                    }
                }
            } else {
                throw new AccessDeniedException();
            }

        }
    }

    @Override
    public void updateSecuredPendingTracks(Collection<PendingTrackForm> trackForms) throws Exception {
        if (trackForms != null) {

            Collection<PendingTrack> tracksToUpdate = new HashSet<PendingTrack>();
            for (PendingTrackForm trackForm : trackForms) {
                //Get the original version of this track
                //It will be this version that will be modified with data coming from the DTO
                PendingTrack originalTrack = getTrackManager().loadPendingTrack(trackForm.getRefID());
                //Check if it can be accessed

                if (getTrackManager().checkAccessUpdatePendingTrack(originalTrack) && isTrackTouchable(originalTrack, false)) {

                    //Copy all originally bound channels before touching them
                    Collection originallyBoundChannels = TRACK_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalTrack);
                    //Save original links before modification (clones)
                    Collection<Programme> originallyLinkedProgrammes = originalTrack.getImmutableProgrammes();

                    //Update the existing entity from the DTO
                    boolean areAttributesUpdated = trackDTOAssembler.augmentWithDTO(originalTrack, trackForm);

                    //Check and adjust its dependencies before updating
                    if (checkTrack(originalTrack)) {
                        //Mark it as ready since no files will be attached
                        originalTrack.setReady(true);
                        //Everything's fine, now make it persistent in the database
                        tracksToUpdate.add(originalTrack);
                        //Log it
                        log(TRACK_LOG_OPERATION.edit, originalTrack, originallyBoundChannels);
                        //Notify responsible users of any changes
                        if (areAttributesUpdated) //Only users still linked to the track after update will receive this notification
                        {
                            notify(PENDINGTRACK_NOTIFICATION_OPERATION.edit, originalTrack);
                        }
                        //New linked programmes
                        for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedProgrammes, originalTrack.getImmutableProgrammes())) {
                            notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, originalTrack, boundProgramme);
                        }
                        //Old linked programmes
                        for (Programme boundProgramme : CollectionUtil.exclusion(originalTrack.getImmutableProgrammes(), originallyLinkedProgrammes)) {
                            notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, originalTrack, boundProgramme);
                        }
                    }
                } else {
                    throw new AccessDeniedException();
                }

            }
            //Everything's fine, now make it persistent in the database
            if (!tracksToUpdate.isEmpty()) {
                getTrackManager().updateTracks(tracksToUpdate);
            }

        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousUpdateSecuredBroadcastTrackAndAudioFile(BroadcastTrackForm trackForm, String trackUploadId) throws Exception {
        asynchronousUpdateSecuredTrackAndAudioAndPictureFile(trackForm, true, trackUploadId, false, null);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousUpdateSecuredBroadcastTrackAndPictureFile(BroadcastTrackForm trackForm, String coverArtUploadId) throws Exception {
        asynchronousUpdateSecuredTrackAndAudioAndPictureFile(trackForm, false, null, true, coverArtUploadId);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousUpdateSecuredBroadcastTrackAndAudioAndPictureFile(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception {
        asynchronousUpdateSecuredTrackAndAudioAndPictureFile(trackForm, true, trackUploadId, true, coverArtUploadId);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousUpdateSecuredPendingTrackAndAudioFile(PendingTrackForm trackForm, String trackUploadId) throws Exception {
        asynchronousUpdateSecuredTrackAndAudioAndPictureFile(trackForm, true, trackUploadId, false, null);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousUpdateSecuredPendingTrackAndPictureFile(PendingTrackForm trackForm, String coverArtUploadId) throws Exception {
        asynchronousUpdateSecuredTrackAndAudioAndPictureFile(trackForm, false, null, true, coverArtUploadId);
    }

    @Override
    public void asynchronousUpdateSecuredPendingTracksAndPictureFile(Collection<PendingTrackForm> trackForms, String coverArtUploadId) throws Exception {
        asynchronousUpdateSecuredTracksAndPictureFile(trackForms, false, null, true, coverArtUploadId);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousUpdateSecuredPendingTrackAndAudioAndPictureFile(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception {
        asynchronousUpdateSecuredTrackAndAudioAndPictureFile(trackForm, true, trackUploadId, true, coverArtUploadId);
    }

    @Override
    public void asynchronousUpdateSecuredBroadcastTracksAndPictureFile(Collection<BroadcastTrackForm> trackForms, String coverArtUploadId) throws Exception {
        asynchronousUpdateSecuredTracksAndPictureFile(trackForms, false, null, true, coverArtUploadId);
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    protected void asynchronousUpdateSecuredTrackAndAudioAndPictureFile(final TrackForm trackForm, final boolean doTransferAudio, final String trackUploadId, final boolean doTransferPicture, final String coverArtUploadId) throws Exception {
        if (trackForm != null) {

            AsynchronousUpdateTrackTransactionInterface op = new AsynchronousUpdateOperation();
            if (doTransferPicture && doTransferAudio) {
                op.add(trackForm, trackUploadId, coverArtUploadId);
            } else if (doTransferAudio) {
                op.addPictureUnchanged(trackForm, trackUploadId);
            } else if (doTransferPicture) {
                op.addAudioUnchanged(trackForm, coverArtUploadId);
            } else {
                op.add(trackForm);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    protected void asynchronousUpdateSecuredTracksAndPictureFile(final Collection<? extends TrackForm> trackForms, final boolean doTransferAudio, final String trackUploadId, final boolean doTransferPicture, final String coverArtUploadId) throws Exception {
        if (trackForms != null) {

            AsynchronousUpdateTrackTransactionInterface op = new AsynchronousUpdateOperation();
            for (TrackForm trackForm : trackForms) {
                if (doTransferPicture && doTransferAudio) {
                    op.add(trackForm, trackUploadId, coverArtUploadId);
                } else if (doTransferAudio) {
                    op.addPictureUnchanged(trackForm, trackUploadId);
                } else if (doTransferPicture) {
                    op.addAudioUnchanged(trackForm, coverArtUploadId);
                } else {
                    op.add(trackForm);
                }
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    /**
     * Returns the id list of track whose content has been modified (attributes, not relationships)
     * @param idsAndForms
     * @param idsAndBackups
     * @return
     * @throws Exception
     */
    protected Collection<String> synchronousSafePreUpdateTracks(
            Collection<? extends TrackForm> trackForms,
            Map<String, RollbackTrackForm> idsAndBackups,
            Map<String, TrackFiles> idsAndFiles) throws Exception {

        HashSet<String> contentModifiedTrackIds = new HashSet<String>();
        if (trackForms != null && idsAndBackups != null) {
            Map<String, BroadcastTrackForm> broadcastableIdsAndForms = new HashMap<String, BroadcastTrackForm>();
            Map<String, PendingTrackForm> pendingIdsAndForms = new HashMap<String, PendingTrackForm>();
            for (TrackForm trackForm : trackForms) {
                if (ProxiedClassUtil.castableAs(trackForm, BroadcastTrackForm.class)) {
                    broadcastableIdsAndForms.put(trackForm.getRefID(), (BroadcastTrackForm) trackForm);
                } else if (ProxiedClassUtil.castableAs(trackForm, PendingTrackForm.class)) {
                    pendingIdsAndForms.put(trackForm.getRefID(), (PendingTrackForm) trackForm);
                }
            }
            if (!broadcastableIdsAndForms.isEmpty()) {
                contentModifiedTrackIds.addAll(
                        synchronousSafePreUpdateBroadcastTracks(broadcastableIdsAndForms, idsAndBackups, idsAndFiles));
            }
            if (!pendingIdsAndForms.isEmpty()) {
                contentModifiedTrackIds.addAll(
                        synchronousSafePreUpdatePendingTracks(pendingIdsAndForms, idsAndBackups, idsAndFiles));
            }
        }
        return contentModifiedTrackIds;
    }

    /**
     * Returns the id list of track whose content has been modified (attributes, not relationships)
     * @param idsAndForms
     * @param idsAndBackups
     * @return
     * @throws Exception
     */
    protected Collection<String> synchronousSafePreUpdateBroadcastTracks(
            Map<String, BroadcastTrackForm> idsAndForms,
            Map<String, RollbackTrackForm> idsAndBackups,
            Map<String, TrackFiles> idsAndFiles) throws Exception {

        HashSet<String> contentUpdatedIds = new HashSet<String>();
        boolean isContentUpdated;
        if (idsAndForms != null && idsAndBackups != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            Collection<BroadcastableTrack> originalTracks = getTrackManager().getBroadcastTracks(idsAndForms.keySet());
            Collection<BroadcastableTrack> modifiedTracks = new HashSet<BroadcastableTrack>();
            for (BroadcastableTrack originalTrack : originalTracks) {

                boolean isFileUpdated = idsAndFiles.containsKey(originalTrack.getRefID());
                if (getTrackManager().checkAccessUpdateBroadcastTrack(originalTrack) && isTrackTouchable(originalTrack, isFileUpdated)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<RollbackBroadcastTrackForm> rollbackCopyEntry = (ActionCollectionEntry<RollbackBroadcastTrackForm>) trackDTOAssembler.toRollbackDTO(originalTrack);
                    RollbackBroadcastTrackForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;

                    //First get a copy of the entity from a DTO
                    isContentUpdated = trackDTOAssembler.augmentWithDTO(originalTrack, idsAndForms.get(originalTrack.getRefID()));
                    //Check if either the track file or the cover sleeve needs update or both
                    if (!isContentUpdated) {
                        isContentUpdated = isFileUpdated;
                    }

                    //Check and adjust its dependencies before updating
                    if (checkTrack(originalTrack)) {

                        originalTrack.setReady(false);
                        modifiedTracks.add(originalTrack);
                        //Update the referenced id list with rollback copies
                        idsAndBackups.put(originalTrack.getRefID(), rollbackCopy);

                        if (isContentUpdated) {
                            contentUpdatedIds.add(originalTrack.getRefID());
                        }

                    }
                } else {
                    throw new AccessDeniedException();
                }

                getTrackManager().updateTracks(modifiedTracks);
            }
        }
        return contentUpdatedIds;
    }

    /**
     * Returns the id list of track whose content has been modified (attributes, not relationships)
     * @param idsAndForms
     * @param idsAndBackups
     * @return
     * @throws Exception
     */
    protected Collection<String> synchronousSafePreUpdatePendingTracks(
            Map<String, PendingTrackForm> idsAndForms,
            Map<String, RollbackTrackForm> idsAndBackups,
            Map<String, TrackFiles> idsAndFiles) throws Exception {

        HashSet<String> contentUpdatedIds = new HashSet<String>();
        boolean isContentUpdated;
        if (idsAndForms != null && idsAndBackups != null) {

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            Collection<PendingTrack> originalTracks = getTrackManager().loadPendingTracks(idsAndForms.keySet());
            Collection<PendingTrack> modifiedTracks = new HashSet<PendingTrack>();
            for (PendingTrack originalTrack : originalTracks) {

                if (getTrackManager().checkAccessUpdatePendingTrack(originalTrack) && isTrackTouchable(originalTrack, false)) {

                    //Prepare a safety copy for rollback, just in case
                    ActionCollectionEntry<RollbackPendingTrackForm> rollbackCopyEntry = trackDTOAssembler.toRollbackDTO(originalTrack);
                    RollbackPendingTrackForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;

                    //First get a copy of the entity from a DTO
                    isContentUpdated = trackDTOAssembler.augmentWithDTO(originalTrack, idsAndForms.get(originalTrack.getRefID()));
                    //Check if either the track file or the cover sleeve needs update or both
                    if (!isContentUpdated) {
                        isContentUpdated = idsAndFiles.containsKey(originalTrack.getRefID());
                    }

                    //Check and adjust its dependencies before updating
                    if (checkTrack(originalTrack)) {

                        originalTrack.setReady(false);
                        modifiedTracks.add(originalTrack);
                        //Update the referenced id list with rollback copies
                        idsAndBackups.put(originalTrack.getRefID(), rollbackCopy);

                        if (isContentUpdated) {
                            contentUpdatedIds.add(originalTrack.getRefID());
                        }

                    }
                } else {
                    throw new AccessDeniedException();
                }

                getTrackManager().updateTracks(modifiedTracks);
            }
        }
        return contentUpdatedIds;
    }

    protected Collection<Integer> synchronousSafePostUpdateTracks(
            final Map<String, RollbackTrackForm> trackIdsAndBackups,
            Map<String, TrackFiles> trackIdsAndFiles,
            final Collection<String> contentUpdatedIds) throws Exception {

        if (trackIdsAndBackups != null && trackIdsAndFiles != null) {
            Collection<Integer> transactions = null;
            Map<Track, TrackFiles> tracksToReset = new HashMap<Track, TrackFiles>();

            final Collection<? extends Track> tracks = getTrackManager().loadTracks(trackIdsAndBackups.keySet());
            if (tracks != null) {
                for (Track track : tracks) {

                    //reset its activation state
                    track.setReady(true);
                    tracksToReset.put(track, trackIdsAndFiles.get(track.getRefID()));
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    log(TRACK_LOG_OPERATION.edit, track,
                            trackDTOAssembler.getImmutableChannelSet(trackIdsAndBackups.get(track.getRefID())));
                    //Notify responsible users of any changes
                    TrackVisitor visitor = new TrackVisitor() {

                        private void processBroadcastTrack(BroadcastableTrack broadcastableTrack) throws Exception {
                            if (contentUpdatedIds != null && contentUpdatedIds.contains(broadcastableTrack.getRefID())) //Only users still linked to the track after update will receive this notification
                            {
                                TrackFacadeService.this.notify(BROADCASTABLETRACK_NOTIFICATION_OPERATION.edit, broadcastableTrack);
                            }
                            //New linked programmes
                            for (Programme boundProgramme : CollectionUtil.exclusion(trackDTOAssembler.getImmutableProgrammeSet(trackIdsAndBackups.get(broadcastableTrack.getRefID())), broadcastableTrack.getImmutableProgrammes())) {
                                TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, broadcastableTrack, boundProgramme);
                            }
                            //Old linked programmes
                            for (Programme boundProgramme : CollectionUtil.exclusion(broadcastableTrack.getImmutableProgrammes(), trackDTOAssembler.getImmutableProgrammeSet(trackIdsAndBackups.get(broadcastableTrack.getRefID())))) {
                                TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.delete, broadcastableTrack, boundProgramme);
                            }
                            //TimetableSlots and PlaylistEntries are handled elsewhere
                        }

                        private void processPendingTrack(PendingTrack pendingTrack) throws Exception {
                            if (contentUpdatedIds != null && contentUpdatedIds.contains(pendingTrack.getRefID())) //Only users still linked to the track after update will receive this notification
                            {
                                TrackFacadeService.this.notify(PENDINGTRACK_NOTIFICATION_OPERATION.edit, pendingTrack);
                            }
                            //New linked programmes
                            for (Programme boundProgramme : CollectionUtil.exclusion(trackDTOAssembler.getImmutableProgrammeSet(trackIdsAndBackups.get(pendingTrack.getRefID())), pendingTrack.getImmutableProgrammes())) {
                                TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, pendingTrack, boundProgramme);
                            }
                            //Old linked programmes
                            for (Programme boundProgramme : CollectionUtil.exclusion(pendingTrack.getImmutableProgrammes(), trackDTOAssembler.getImmutableProgrammeSet(trackIdsAndBackups.get(pendingTrack.getRefID())))) {
                                TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.delete, pendingTrack, boundProgramme);
                            }
                            //TimetableSlots and PlaylistEntries are handled elsewhere
                        }

                        @Override
                        public void visit(PendingAdvert track) throws Exception {
                            processPendingTrack(track);
                        }

                        @Override
                        public void visit(PendingJingle track) throws Exception {
                            processPendingTrack(track);
                        }

                        @Override
                        public void visit(PendingSong track) throws Exception {
                            processPendingTrack(track);
                        }

                        @Override
                        public void visit(BroadcastableAdvert track) throws Exception {
                            processBroadcastTrack(track);
                        }

                        @Override
                        public void visit(BroadcastableJingle track) throws Exception {
                            processBroadcastTrack(track);
                        }

                        @Override
                        public void visit(BroadcastableSong track) throws Exception {
                            processBroadcastTrack(track);
                        }
                    };
                    track.acceptVisit(visitor);
                }
            }
            if (!tracksToReset.isEmpty()) {
                transactions = getDatastoreService().moveTrackFilesFromPoolToDataStore(tracksToReset);
                getTrackManager().updateTracks(tracksToReset.keySet());
            }
            return transactions;
        }
        return null;
    }

    protected Collection<Integer> synchronousSafePostAddTracks(
            Map<String, TrackFiles> trackIdsAndFiles) throws Exception {

        if (trackIdsAndFiles != null) {
            Collection<Integer> transactions = null;
            Map<Track, TrackFiles> tracksToReset = new HashMap<Track, TrackFiles>();

            final Collection<? extends Track> tracks = getTrackManager().loadTracks(trackIdsAndFiles.keySet());
            if (tracks != null) {
                for (Track track : tracks) {

                    //reset its activation state
                    track.setReady(true);
                    tracksToReset.put(track, trackIdsAndFiles.get(track.getRefID()));
                    //Log it, use the rollback copies to regenerate the originally bound channel list
                    log(TRACK_LOG_OPERATION.add, track);
                    //Notify responsible users of any changes
                    //All linked entities are then new links
                    final Collection<Programme> boundProgrammes = track.getImmutableProgrammes();

                    TrackVisitor visitor = new TrackVisitor() {

                        @Override
                        public void visit(PendingAdvert track) throws Exception {
                            for (Programme boundProgramme : boundProgrammes) {
                                TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                            }
                        }

                        @Override
                        public void visit(PendingJingle track) throws Exception {
                            for (Programme boundProgramme : boundProgrammes) {
                                TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                            }
                        }

                        @Override
                        public void visit(PendingSong track) throws Exception {
                            for (Programme boundProgramme : boundProgrammes) {
                                TrackFacadeService.this.notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                            }
                        }

                        @Override
                        public void visit(BroadcastableAdvert track) throws Exception {
                            for (Programme boundProgramme : boundProgrammes) {
                                TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                            }
                        }

                        @Override
                        public void visit(BroadcastableJingle track) throws Exception {
                            for (Programme boundProgramme : boundProgrammes) {
                                TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                            }
                        }

                        @Override
                        public void visit(BroadcastableSong track) throws Exception {
                            for (Programme boundProgramme : boundProgrammes) {
                                TrackFacadeService.this.notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.add, track, boundProgramme);
                            }
                        }
                    };
                    track.acceptVisit(visitor);
                    //TimetableSlots and PlaylistEntries are handled elsewhere

                }
            }
            if (!tracksToReset.isEmpty()) {
                transactions = getDatastoreService().moveTrackFilesFromPoolToDataStore(tracksToReset);
                getTrackManager().updateTracks(tracksToReset.keySet());
            }
            return transactions;
        }
        return null;
    }

    /**
     * Create a BroadcastableTrack skeleton from a PendingTrack
     * Programmes are not specified
     * @param pendingTrack
     * @return
     * @throws Exception
     **/
    protected BroadcastableTrack copy(PendingTrack pendingTrack) throws Exception {
        //Make a broadcastableTrack from a pendingTrack

        BroadcastConverterTrackVisitor visitor = new BroadcastConverterTrackVisitor() {

            private BroadcastableTrack newTrack = null;

            @Override
            public BroadcastableTrack get() {
                return newTrack;
            }

            private void compose(PendingTrack pendingTrack) throws Exception {
                //Fill in the common attributes
                newTrack.setAlbum(pendingTrack.getAlbum());
                newTrack.setAuthor(pendingTrack.getAuthor());
                newTrack.setPublisher(pendingTrack.getPublisher());
                newTrack.setCopyright(pendingTrack.getCopyright());
                newTrack.setDateOfRelease(pendingTrack.getDateOfRelease());
                newTrack.setDescription(pendingTrack.getDescription());
                newTrack.setGenre(pendingTrack.getGenre());
                newTrack.setRating(pendingTrack.getRating());
                newTrack.setTag(pendingTrack.getTag());
                newTrack.setTitle(pendingTrack.getTitle());
                //New attributes
                newTrack.setEnabled(true);
                //newTrack.setRotation(0);
                //Keep the original author intact
                newTrack.setCreator(pendingTrack.getCreator());
                //Copy the linked programmes from the pending track to the new broadcast track
                //It means that all user-updatable programmes from the pendingtrack will be transfered to the new broadcastabletrack
                for (TrackProgramme pendingTrackProgramme : pendingTrack.getTrackProgrammes()) {
                    newTrack.addProgramme(pendingTrackProgramme.getProgramme());
                }
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                newTrack = new BroadcastableAdvert();
                compose(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                newTrack = new BroadcastableJingle();
                compose(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                newTrack = new BroadcastableSong();
                compose(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                //Do nothing
            }
        };
        pendingTrack.acceptVisit(visitor);
        return visitor.get();
    }

    protected void synchronousSafePreValidatePendingTracks(Map<String, RollbackPendingTrackForm> trackIdsAndBackups, Map<String, String> outputForRelocation, Map<String, String> outputForCopy) throws Exception {
        if (trackIdsAndBackups != null) {

            if (outputForRelocation == null) {
                outputForRelocation = new HashMap<String, String>();
            }
            if (outputForCopy == null) {
                outputForCopy = new HashMap<String, String>();
            }

            //Get the original version of this track
            //It will be this version that will be modified with data coming from the DTO
            Collection<PendingTrack> originalTracks = getTrackManager().loadPendingTracks(trackIdsAndBackups.keySet());
            Collection<PendingTrack> modifiedTracks = new HashSet<PendingTrack>();
            Collection<BroadcastableTrack> addedTracks = new HashSet<BroadcastableTrack>();
            for (PendingTrack originalTrack : originalTracks) {

                //Prepare a safety copy for rollback, just in case
                ActionCollectionEntry<RollbackPendingTrackForm> rollbackCopyEntry = trackDTOAssembler.toRollbackDTO(originalTrack);
                RollbackPendingTrackForm rollbackCopy = rollbackCopyEntry != null ? rollbackCopyEntry.getItem() : null;
                //Check if it can be accessed

                if (getTrackManager().checkAccessReviewPendingTrack(originalTrack)) {

                    BroadcastableTrack newTrack = copy(originalTrack);

                    //Now remove from the originalPendingTrack all programmes that have been copied to newTrack
                    /*for (Programme p : newTrack.getImmutableProgrammes()) {
                        originalTrack.removeProgramme(p);
                    }*/
                    originalTrack.clearTrackProgrammes();

                    if (newTrack != null && checkTrack(newTrack)) {
                        //Everything's fine, now make it persistent in the database
                        originalTrack.setReady(false);
                        newTrack.setReady(false);

                        modifiedTracks.add(originalTrack);
                        addedTracks.add(newTrack);
                        if (originalTrack.getImmutableProgrammes().isEmpty()) {
                            outputForRelocation.put(originalTrack.getRefID(), newTrack.getRefID());
                        } else {
                            outputForCopy.put(originalTrack.getRefID(), newTrack.getRefID());
                        }
                        trackIdsAndBackups.put(originalTrack.getRefID(), rollbackCopy);

                    }
                } else {
                    throw new AccessDeniedException();
                }

            }
            getTrackManager().updateTracks(modifiedTracks);
            try {
                getTrackManager().addTracks(addedTracks);
            } catch (DataIntegrityViolationException e) {
                throw new DuplicatedEntityException(e.getClass().getCanonicalName());
            }
        }

    }

    protected Collection<Integer> synchronousSafePostValidatePendingTracks(Map<String, RollbackPendingTrackForm> trackIdsAndBackups, Map<String, String> outputForRelocation, Map<String, String> outputForCopy) throws Exception {
        if (trackIdsAndBackups != null && outputForRelocation != null && outputForCopy != null) {
            Collection<Integer> transactions = new HashSet<Integer>();

            Collection<Track> tracksToDelete = new HashSet<Track>();
            Collection<Track> tracksToReset = new HashSet<Track>();

            //Reload everything
            //TODO try to do it in clusters of ids, even unrelated, then properly re-associate them
            HashMap<PendingTrack, BroadcastableTrack> tracksToCopy = new HashMap<PendingTrack, BroadcastableTrack>();
            HashMap<PendingTrack, BroadcastableTrack> tracksToRelocate = new HashMap<PendingTrack, BroadcastableTrack>();
            for (Entry<String, String> outputForCopyEntry : outputForCopy.entrySet()) {
                PendingTrack from = getTrackManager().loadPendingTrack(outputForCopyEntry.getKey());
                BroadcastableTrack to = getTrackManager().loadBroadcastTrack(outputForCopyEntry.getValue());
                if (from != null && to != null) {
                    tracksToCopy.put(from, to);
                }
            }
            for (Entry<String, String> outputForRelocationEntry : outputForRelocation.entrySet()) {
                PendingTrack from = getTrackManager().loadPendingTrack(outputForRelocationEntry.getKey());
                BroadcastableTrack to = getTrackManager().loadBroadcastTrack(outputForRelocationEntry.getValue());
                if (from != null && to != null) {
                    tracksToRelocate.put(from, to);
                }
            }

            //Do the dirty job of moving files from one track to another
            if (!tracksToCopy.isEmpty()) {
                transactions.addAll(getDatastoreService().copyTrackFilesWithinDatastore(tracksToCopy));
                for (PendingTrack trackToReset : tracksToCopy.keySet()) {

                    //reset its activation state
                    trackToReset.setReady(true);
                    tracksToReset.add(trackToReset);

                }
                for (BroadcastableTrack trackToReset : tracksToCopy.values()) {

                    //reset its activation state
                    trackToReset.setReady(true);
                    tracksToReset.add(trackToReset);
                    //Log it
                    log(TRACK_LOG_OPERATION.validate, trackToReset);
                    //Notify responsible users of any changes
                    //All linked entities are then new links
                    for (Programme boundProgramme : trackToReset.getImmutableProgrammes()) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.validate, trackToReset, boundProgramme);
                    }
                    //TimetableSlots and PlaylistEntries are handled elsewhere

                }
            }
            if (!tracksToRelocate.isEmpty()) {
                transactions.addAll(getDatastoreService().swapTrackFilesWithinDatastore(tracksToRelocate));
                for (PendingTrack trackToRemove : tracksToRelocate.keySet()) {

                    tracksToDelete.add(trackToRemove);

                }
                for (BroadcastableTrack trackToReset : tracksToRelocate.values()) {

                    //reset its activation state
                    trackToReset.setReady(true);
                    tracksToReset.add(trackToReset);
                    //Log it
                    log(TRACK_LOG_OPERATION.validate, trackToReset);
                    //Notify responsible users of any changes
                    //All linked entities are then new links
                    for (Programme boundProgramme : trackToReset.getImmutableProgrammes()) {
                        notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION.validate, trackToReset, boundProgramme);
                    }
                    //TimetableSlots and PlaylistEntries are handled elsewhere

                }
            }

            if (!tracksToReset.isEmpty()) {
                getTrackManager().updateTracks(tracksToReset);
            }
            if (!tracksToDelete.isEmpty()) {
                getTrackManager().deleteTracks(tracksToDelete);
            }
            return transactions;
        }
        return null;

    }

    /**
     * Transform a pendingTrack into a BroadcastableTrack
     * @param id
     * @throws Exception
     **/
    @Override
    public void asynchronousValidateSecuredPendingTrack(String pendingTrackId) throws Exception {
        if (pendingTrackId != null) {
            Collection<String> pendingTrackIds = new HashSet<String>();
            pendingTrackIds.add(pendingTrackId);
            asynchronousValidateSecuredPendingTracks(pendingTrackIds);
        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousValidateSecuredPendingTracks(Collection<String> pendingTrackIds) throws Exception {
        //No batch version of this method exists, process those tracks one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (pendingTrackIds != null) {

            AsynchronousValidateTrackTransactionInterface op = new AsynchronousValidateOperation();
            for (String pendingTrackId : pendingTrackIds) {
                op.add(pendingTrackId);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);
        }
    }

    /**
     * At the moment rejecting is equivalent to simple deletion
     * TODO: make it recoverable with edition (bind extra data to linked programmes) and notify the contributor
     * @param pendingTrackId
     * @param motiveText
     * @param isReady
     * @return
     * @throws Exception
     **/
    @Override
    public void asynchronousRejectSecuredPendingTrack(String pendingTrackId, String motiveText) throws Exception {
        if (pendingTrackId != null) {
            Collection<String> pendingTrackIds = new HashSet<String>();
            pendingTrackIds.add(pendingTrackId);
            asynchronousRejectSecuredPendingTracks(pendingTrackIds, motiveText);
        }
    }

    /**
     * Outside transactionnal scope. Will use transactional methods once embedded within an asynchronous workers.
     * @param trackForm
     * @param trackUploadId
     * @throws Exception
     **/
    @Override
    public void asynchronousRejectSecuredPendingTracks(Collection<String> pendingTrackIds, String motiveText) throws Exception {
        //No batch version of this method exists, process those tracks one after the other
        //Just make sure that ALL of them can be touched by the currently authenticated user first
        if (pendingTrackIds != null) {

            AsynchronousRejectTrackTransactionInterface op = new AsynchronousRejectOperation();
            for (String pendingTrackId : pendingTrackIds) {
                op.add(pendingTrackId, motiveText);
            }
            //Prepare the rollback structure
            startAsynchronousTask(op);

        }
    }

    @Override
    public CoverArtModule getSecuredTrackPictureModule(String trackId) throws Exception {
        Track track = getTrackManager().getTrack(trackId);
        //Check if the currently authenticated user can access this resource
        if (getTrackManager().checkAccessViewTrack(track)) {
            return getTrackDTOAssembler().toCoverArtDTO(track);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ContainerModule getSecuredTrackAudioModule(String trackId) throws Exception {
        Track track = getTrackManager().getTrack(trackId);
        //Check if the currently authenticated user can access this resource
        if (getTrackManager().checkAccessViewTrack(track)) {
            return getTrackDTOAssembler().toContainerDTO(track);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<? extends TrackForm> getSecuredTrack(String id) throws Exception {
        Track track = getTrackManager().getTrack(id);
        //Check if the currently authenticated user can access this resource
        ActionCollectionEntry<? extends TrackForm> trackFormEntry = trackDTOAssembler.toDTO(track);
        if (trackFormEntry == null || trackFormEntry.isReadable()) {
            return trackFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<PendingTrackForm> getSecuredPendingTrack(String id) throws Exception {
        PendingTrack track = getTrackManager().loadPendingTrack(id);
        //Check if the currently authenticated user can access this resource
        ActionCollectionEntry<PendingTrackForm> trackFormEntry = trackDTOAssembler.toDTO(track);
        if (trackFormEntry == null || trackFormEntry.isReadable()) {
            return trackFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<BroadcastTrackForm> getSecuredBroadcastTrack(String id) throws Exception {
        BroadcastableTrack track = getTrackManager().loadBroadcastTrack(id);
        //Check if the currently authenticated user can access this resource
        ActionCollectionEntry<BroadcastTrackForm> trackFormEntry = trackDTOAssembler.toDTO(track);
        if (trackFormEntry == null || trackFormEntry.isReadable()) {
            return trackFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<? extends PendingTrackForm> getSecuredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint) throws Exception {
        Collection<? extends PendingTrack> tracks = getTrackManager().getFilteredPendingTracks(constraint);
        return (ActionCollection<? extends PendingTrackForm>) trackDTOAssembler.toPendingDTO(tracks, constraint==null || constraint.getSortByFeature()==null);
    }

    @Override
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws Exception {
        Collection<? extends BroadcastableTrack> tracks = getTrackManager().getFilteredBroadcastTracks(constraint);
        return (ActionCollection<? extends BroadcastTrackForm>) trackDTOAssembler.toBroadcastDTO(tracks, constraint==null || constraint.getSortByFeature()==null);
    }

    @Override
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId) throws Exception {
        Collection<? extends BroadcastableTrack> tracks = getTrackManager().getFilteredBroadcastTracksForProgrammeId(constraint, programmeId);
        return (ActionCollection<? extends BroadcastTrackForm>) trackDTOAssembler.toBroadcastDTO(tracks, constraint==null || constraint.getSortByFeature()==null);
    }

    /*public ActionCollection<BroadcastSongForm> getSecuredBroadcastSongsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
    Collection<? extends BroadcastableSong> tracks = getTrackManager().getFilteredBroadcastSongsForProgrammeIds(author, title, programmeIds);
    return (ActionCollection<BroadcastSongForm>) trackDTOAssembler.toBroadcastDTO(tracks);
    }

    public ActionCollection<BroadcastAdvertForm> getSecuredBroadcastAdvertsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
    Collection<? extends BroadcastableAdvert> tracks = getTrackManager().getFilteredBroadcastAdvertsForProgrammeIds(author, title, programmeIds);
    return (ActionCollection<BroadcastAdvertForm>) trackDTOAssembler.toBroadcastDTO(tracks);
    }

    public ActionCollection<BroadcastJingleForm> getSecuredBroadcastJinglesForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
    Collection<? extends BroadcastableJingle> tracks = getTrackManager().getFilteredBroadcastJinglesForProgrammeIds(author, title, programmeIds);
    return (ActionCollection<BroadcastJingleForm>) trackDTOAssembler.toBroadcastDTO(tracks);
    }*/
    @Override
    public EmbeddedTagModule getSecuredTags(String id) throws Exception {
        Track track = getTrackManager().getTrack(id);
        //Check if the currently authenticated user can access this resource
        if (getTrackManager().checkAccessViewTrack(track)) {
            return trackDTOAssembler.toTagDTO(track);
        } else {
            throw new AccessDeniedException();
        }
    }

    /**
     * Unsecured method, no credentials checked
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public EmbeddedTagModule getSecuredDraftTags(String id) throws Exception {

        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().getDraftTags(id);
    }

    @Override
    public ContainerModule getSecuredDraftAudioFileInfo(String id) throws Exception {

        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().getDraftAudioFileInfo(id);
    }

    @Override
    public CoverArtModule getSecuredDraftPictureFileInfo(String id) throws Exception {

        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().getDraftPictureFileInfo(id);
    }

    @Override
    public PictureStream getSecuredPictureStreamFromDataStore(String trackId) throws Exception {
        //fileId must be a Long representing a track Id
        //Whoever can view (simple read) a track can download the coverart picture
        Track track = getTrackManager().getTrack(trackId);
        if (getTrackManager().checkAccessViewTrack(track)) {
            return getDatastoreService().getPictureStreamFromDataStore(track);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public TrackStream getSecuredTrackStreamFromDataStore(String trackId) throws Exception {
        //fileId must be a Long representing a track Id
        //Whoever can view (simple read) a track can download the coverart picture
        Track track = getTrackManager().getTrack(trackId);
        if (getTrackManager().checkAccessViewTrack(track)) {
            return getDatastoreService().getAudioStreamFromDataStore(track);
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public TrackStream getSecuredTrackStreamFromSessionPool(String trackUploadId) throws Exception {
        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().getTrackStreamFromSessionPool(trackUploadId);
    }

    @Override
    public PictureStream getSecuredPictureStreamFromSessionPool(String coverArtUploadId) throws Exception {
        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().getPictureStreamFromSessionPool(coverArtUploadId);
    }

    @Override
    public boolean setSecuredStreamToSessionPool(String uploadId, TypedStreamInterface file) throws Exception {
        //We cannot acertain whether the user has the rights to access the tags: just check if this user is not anonymous
        if (getSecurityManager().getCurrentlyAuthenticatedUser() == null) {
            throw new AccessDeniedException();
        }
        return getDatastoreService().setStreamToSessionPool(uploadId, file);
    }

    protected void log(TRACK_LOG_OPERATION operation, Collection<? extends Track> tracks) throws Exception {
        if (tracks != null && !tracks.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<TrackLog> logs = new HashSet<TrackLog>();
            for (Track track : tracks) {
                logs.add(new TrackLog(operation, track.getRefID(), track.getTitle(), track.getAuthor(), operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), track)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(TRACK_LOG_OPERATION operation, Track track) throws Exception {
        log(operation, track, null);
    }

    protected void log(TRACK_LOG_OPERATION operation, Track track, Collection<Channel> supplementaryChannels) throws Exception {
        if (track != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), track);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(new TrackLog(operation, track.getRefID(), track.getTitle(), track.getAuthor(), allBoundChannels));
        }
    }

    protected void notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION operation, BroadcastableTrack broadcastableTrack, Programme programme) throws Exception {
        notify(operation, broadcastableTrack, programme, operation.getRecipients(getSecurityManager(), programme, broadcastableTrack));
    }

    protected void notify(PROGRAMMEBROADCASTABLETRACK_NOTIFICATION_OPERATION operation, BroadcastableTrack broadcastableTrack, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        if (broadcastableTrack != null && programme != null && recipients != null) {
            Collection<ProgrammeBroadcastableTrackLinkNotification> notifications = new HashSet<ProgrammeBroadcastableTrackLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new ProgrammeBroadcastableTrackLinkNotification(operation, programme, broadcastableTrack, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack pendingTrack, Programme programme) throws Exception {
        notify(operation, pendingTrack, programme, (String) null);
    }

    protected void notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack pendingTrack, Programme programme, String message) throws Exception {
        notify(operation, pendingTrack, programme, operation.getRecipients(getSecurityManager(), programme, pendingTrack), message);
    }

    protected void notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack pendingTrack, Programme programme, Collection<RestrictedUser> recipients) throws Exception {
        notify(operation, pendingTrack, programme, recipients, (String) null);
    }

    protected void notify(PROGRAMMEPENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack pendingTrack, Programme programme, Collection<RestrictedUser> recipients, String message) throws Exception {
        if (pendingTrack != null && programme != null && recipients != null) {
            Collection<ProgrammePendingTrackLinkNotification> notifications = new HashSet<ProgrammePendingTrackLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                ProgrammePendingTrackLinkNotification notification = new ProgrammePendingTrackLinkNotification(operation, programme, pendingTrack, recipient);
                if (message != null) {
                    notification.setFreeText(message);
                }
                notifications.add(notification);
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(BROADCASTABLETRACK_NOTIFICATION_OPERATION operation, BroadcastableTrack track) throws Exception {
        notify(operation, track, operation.getRecipients(getSecurityManager(), track));
    }

    protected void notify(BROADCASTABLETRACK_NOTIFICATION_OPERATION operation, BroadcastableTrack track, Collection<RestrictedUser> recipients) throws Exception {
        if (track != null && recipients != null) {
            Collection<BroadcastableTrackContentNotification> notifications = new HashSet<BroadcastableTrackContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new BroadcastableTrackContentNotification(operation, track, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack track) throws Exception {
        notify(operation, track, operation.getRecipients(getSecurityManager(), track));
    }

    protected void notify(PENDINGTRACK_NOTIFICATION_OPERATION operation, PendingTrack track, Collection<RestrictedUser> recipients) throws Exception {
        if (track != null && recipients != null) {
            Collection<PendingTrackContentNotification> notifications = new HashSet<PendingTrackContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new PendingTrackContentNotification(operation, track, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected boolean isTrackTouchable(Track track, final boolean isAudioOrPictureModified) throws Exception {

        if (track == null) {
            throw new NoEntityException();
        }

        if (!track.isReady()) {
            throw new TrackNotReadyException();
        }

        //If the track is queued then you can't touch it
        TrackVisitor visitor = new TrackVisitor() {

            private void checkBroadcastable(BroadcastableTrack track) throws Exception {
                //You can modify a track at the last minute IFF it's not queued AND you didn't modify its audio or picture
                if (isAudioOrPictureModified) {
                    SortedSet<SimpleTrackQueueItem> queueItems = track.getQueueItems();
                    if (queueItems != null && !queueItems.isEmpty()) {
                        throw new TrackInQueueException();
                    }
                }
                //Same if it's playing right now
                //technically it's not mandatory this track exists anymore but it's helpful for webservices who'd like to gather precise "now playing" data by fetching the actual track
                Set<SimpleTrackQueueStreamItem> streamItems = track.getStreamItems();
                if (streamItems != null && !streamItems.isEmpty()) {
                    throw new TrackNowPlayingException();
                }
            }

            private void checkSong(BroadcastableSong track) throws Exception {
                //Don't forget to check within requests as well
                Collection<Request> requests = track.getRequests();
                if (requests != null) {
                    for (Request request : requests) {
                        //You can modify a track at the last minute IFF it's not queued AND you didn't modify its audio or picture
                        if (isAudioOrPictureModified) {
                            if (request.getQueueItem() != null) {
                                throw new TrackInQueueException();
                            }
                        }
                        if (request.getStreamItem() != null) {
                            throw new TrackNowPlayingException();
                        }
                    }
                }

            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                //Do nothing
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                checkBroadcastable(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                checkBroadcastable(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                checkBroadcastable(track);
                checkSong(track);
            }
        };
        track.acceptVisit(visitor);

        return true;
    }
}
