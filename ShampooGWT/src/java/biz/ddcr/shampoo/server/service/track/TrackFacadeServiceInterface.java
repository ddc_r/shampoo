/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
 *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.helper.AsynchronousUnmanagedTransactionUnitInterface;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import java.util.Collection;



/**
 *
 * @author okay_awright
 **/
public interface TrackFacadeServiceInterface {

    public interface AsynchronousDeleteTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(String id);
    }

    public interface AsynchronousAddTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm track, String trackUploadId, String coverArtUploadId);
        public void add(TrackForm track, String trackUploadId);
        public void add(TrackForm track);
    }

    public interface AsynchronousUpdateTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(TrackForm track, String trackUploadId, String coverArtUploadId);
        public void addPictureUnchanged(TrackForm track, String trackUploadId);
        public void addAudioUnchanged(TrackForm track, String coverArtUploadId);
        public void add(TrackForm track);
    }

    public interface AsynchronousValidateTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(String id);
    }

    public interface AsynchronousRejectTrackTransactionInterface extends AsynchronousUnmanagedTransactionUnitInterface<Track> {
        public void add(String id, String message);
    }

    //Editing objects
    
    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousAddSecuredBroadcastTrackAndAudioFile(BroadcastTrackForm trackForm, String trackUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousAddSecuredBroadcastTrackAndAudioAndPictureFile(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;
    
    public void addSecuredPendingTrack(PendingTrackForm trackForm) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousAddSecuredPendingTrackAndAudioFile(PendingTrackForm trackForm, String trackUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousAddSecuredPendingTrackAndAudioAndPictureFile(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;

    
    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm) throws Exception;
    public void updateSecuredBroadcastTracks(Collection<BroadcastTrackForm> trackForms) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredBroadcastTrackAndAudioFile(BroadcastTrackForm trackForm, String trackUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredBroadcastTrackAndPictureFile(BroadcastTrackForm trackForm, String coverArtUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredBroadcastTracksAndPictureFile(Collection<BroadcastTrackForm> trackForms, String coverArtUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredBroadcastTrackAndAudioAndPictureFile(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;
    
    public void updateSecuredPendingTrack(PendingTrackForm trackForm) throws Exception;
    public void updateSecuredPendingTracks(Collection<PendingTrackForm> trackForms) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredPendingTrackAndAudioFile(PendingTrackForm trackForm, String trackUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredPendingTrackAndPictureFile(PendingTrackForm trackForm, String coverArtUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredPendingTracksAndPictureFile(Collection<PendingTrackForm> trackForms, String coverArtUploadId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousUpdateSecuredPendingTrackAndAudioAndPictureFile(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception;

    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousValidateSecuredPendingTrack(String pendingTrackId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousValidateSecuredPendingTracks(Collection<String> pendingTrackIds) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousRejectSecuredPendingTrack(String pendingTrackId, String motiveText) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousRejectSecuredPendingTracks(Collection<String> pendingTrackIds, String motiveText) throws Exception;

    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousDeleteSecuredTrack(String trackId) throws Exception;
    /** Making it transaction-safe is the responsibility of the implementation; by default it should not be included in a transaction this time */
    public void asynchronousDeleteSecuredTracks(Collection<String> trackIds) throws Exception;

    //Fetching objects
    
    public ActionCollectionEntry<? extends TrackForm> getSecuredTrack(String id) throws Exception;
    
    public ActionCollectionEntry<PendingTrackForm> getSecuredPendingTrack(String id) throws Exception;

    public ActionCollectionEntry<BroadcastTrackForm> getSecuredBroadcastTrack(String id) throws Exception;

    //Helpers
    
    public ActionCollection<? extends PendingTrackForm> getSecuredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint) throws Exception;
    
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws Exception;
    
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId) throws Exception;
    
    
    public EmbeddedTagModule getSecuredDraftTags(String id) throws Exception;
    
    public EmbeddedTagModule getSecuredTags(String id) throws Exception;
    
    public ContainerModule getSecuredDraftAudioFileInfo(String id) throws Exception;

    public CoverArtModule getSecuredDraftPictureFileInfo(String id) throws Exception;
    
    public ContainerModule getSecuredTrackAudioModule(String trackId) throws Exception;
    
    public CoverArtModule getSecuredTrackPictureModule(String trackId) throws Exception;
    
    public PictureStream getSecuredPictureStreamFromDataStore(String trackId) throws Exception;
    
    public TrackStream getSecuredTrackStreamFromDataStore(String trackId) throws Exception;

    
    public TrackStream getSecuredTrackStreamFromSessionPool(String trackUploadId) throws Exception;
    
    public PictureStream getSecuredPictureStreamFromSessionPool(String coverArtUploadId) throws Exception;
    
    public boolean setSecuredStreamToSessionPool(String uploadId, TypedStreamInterface file) throws Exception;

    
    /*public ActionCollection<BroadcastSongForm> getSecuredBroadcastSongsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    
    public ActionCollection<BroadcastAdvertForm> getSecuredBroadcastAdvertsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    
    public ActionCollection<BroadcastJingleForm> getSecuredBroadcastJinglesForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;*/

}
