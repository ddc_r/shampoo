/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.dao.programme.ProgrammeDao;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.track.TrackDao;
import biz.ddcr.shampoo.server.domain.track.BroadcastableAdvert;
import biz.ddcr.shampoo.server.domain.track.BroadcastableJingle;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingAdvert;
import biz.ddcr.shampoo.server.domain.track.PendingJingle;
import biz.ddcr.shampoo.server.domain.track.PendingSong;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Track;
import biz.ddcr.shampoo.server.domain.track.TrackVisitor;
import biz.ddcr.shampoo.server.helper.RightsBundle;

import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public class TrackManager extends GenericService implements TrackManagerInterface {

    private interface BooleanTrackVisitor extends TrackVisitor {
        public boolean get();
    }

    private SecurityManagerInterface securityManager;
    private transient TrackDao trackDao;
    private transient ProgrammeDao programmeDao;

    public TrackDao getTrackDao() {
        return trackDao;
    }

    public void setTrackDao(TrackDao trackDao) {
        this.trackDao = trackDao;
    }

    public ProgrammeDao getProgrammeDao() {
        return programmeDao;
    }

    public void setProgrammeDao(ProgrammeDao programmeDao) {
        this.programmeDao = programmeDao;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public boolean checkAccessViewTracks() throws Exception {
        return (checkAccessViewBroadcastTracks() || checkAccessViewPendingTracks());
    }

    @Override
    public boolean checkAccessViewPendingTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessViewBroadcastTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = BroadcastableTrack.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessExtendedViewTracks() throws Exception {
        return (checkAccessExtendedViewBroadcastTracks() || checkAccessExtendedViewPendingTracks());
    }

    @Override
    public boolean checkAccessExtendedViewPendingTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getExtendedViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessExtendedViewBroadcastTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = BroadcastableTrack.getExtendedViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    /**
     *
     * Check if at least one programme bound to the track fullfills the provided necessary right requirements
     *
     * @param trackIdToCheck
     * @param rightsBundle
     * @return
     * @throws Exception
     **/
    protected boolean hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(Track trackToCheck, RightsBundle rightsBundle) throws Exception {
        if (trackToCheck != null) {
            Collection<Programme> boundProgrammes = trackToCheck.getImmutableProgrammes();
            //First see if at least one programme is bound to this track
            if (!boundProgrammes.isEmpty()) {
                for (Programme boundProgramme : boundProgrammes) {
                    if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRightsOverProgramme(boundProgramme, rightsBundle)) {
                        return true;
                    }
                }
            } else {
                //no programme is bound to the entity
                //Check first if the authenticated user has the minimal rights to touch the entity
                if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle)) {
                    //see if he's either its creator, or the latest editor, or if it has none
                    if (
                            (trackToCheck.getCreator() == null && trackToCheck.getLatestEditor() == null)
                            ||
                            (trackToCheck.getCreator() != null && getSecurityManager().isCurrentlyAuthenticatedUser(trackToCheck.getCreator()))
                            ||
                            (trackToCheck.getLatestEditor() != null && getSecurityManager().isCurrentlyAuthenticatedUser(trackToCheck.getLatestEditor()))
                            ) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewBroadcastTrack(String trackIdToCheck) throws Exception {
        return checkAccessViewBroadcastTrack(getBroadcastTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessViewBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the track to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = BroadcastableTrack.getViewRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewPendingTrack(String trackIdToCheck) throws Exception {
        return checkAccessViewPendingTrack(getPendingTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessViewPendingTrack(PendingTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the track to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = PendingTrack.getViewRights();

                RightsBundle restrictedRightsBundle = new RightsBundle();
                restrictedRightsBundle.setContributorRights(true);

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewTrack(String trackIdToCheck) throws Exception {
        return checkAccessViewTrack(getTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessViewTrack(Track trackToCheck) throws Exception {
        //Determine which kind of track is this
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;
            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = checkAccessViewPendingTrack(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = checkAccessViewPendingTrack(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = checkAccessViewPendingTrack(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = checkAccessViewBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = checkAccessViewBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = checkAccessViewBroadcastTrack(track);
            }
        };
        trackToCheck.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public boolean checkAccessExtendedViewBroadcastTrack(String trackIdToCheck) throws Exception {
        return checkAccessExtendedViewBroadcastTrack(getBroadcastTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessExtendedViewBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the track to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = BroadcastableTrack.getExtendedViewRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessExtendedViewPendingTrack(String trackIdToCheck) throws Exception {
        return checkAccessExtendedViewPendingTrack(getPendingTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessExtendedViewPendingTrack(PendingTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the track to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = PendingTrack.getExtendedViewRights();

                RightsBundle restrictedRightsBundle = new RightsBundle();
                restrictedRightsBundle.setContributorRights(true);

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessExtendedViewTrack(String trackIdToCheck) throws Exception {
        return checkAccessExtendedViewTrack(getTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessExtendedViewTrack(Track trackToCheck) throws Exception {
        //Determine which kind of track is this
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;
            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = checkAccessExtendedViewPendingTrack(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = checkAccessExtendedViewPendingTrack(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = checkAccessExtendedViewPendingTrack(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = checkAccessExtendedViewBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = checkAccessExtendedViewBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = checkAccessExtendedViewBroadcastTrack(track);
            }
        };
        trackToCheck.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public boolean checkAccessAddBroadcastTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = BroadcastableTrack.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessAddPendingTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdateBroadcastTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdatePendingTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessUpdateTrack(String trackIdToCheck) throws Exception {
        Track track = getTrack(trackIdToCheck);
        //Determine which kind of track is this
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;
            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = checkAccessUpdatePendingTrack(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = checkAccessUpdatePendingTrack(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = checkAccessUpdatePendingTrack(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = checkAccessUpdateBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = checkAccessUpdateBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = checkAccessUpdateBroadcastTrack(track);
            }
        };
        track.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public boolean checkAccessUpdateBroadcastTrack(String trackIdToCheck) throws Exception {
        return checkAccessUpdateBroadcastTrack(getBroadcastTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessUpdateBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = BroadcastableTrack.getUpdateRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdatePendingTrack(String trackIdToCheck) throws Exception {
        return checkAccessUpdatePendingTrack(getPendingTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessUpdatePendingTrack(PendingTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the track to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = PendingTrack.getUpdateRights();

                RightsBundle restrictedRightsBundle = new RightsBundle();
                restrictedRightsBundle.setContributorRights(true);

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeletePendingTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteBroadcastTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = BroadcastableTrack.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessDeleteTrack(String trackIdToCheck) throws Exception {
        Track track = getTrack(trackIdToCheck);
        //Determine which kind of track is this
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;
            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = checkAccessDeletePendingTrack(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = checkAccessDeletePendingTrack(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = checkAccessDeletePendingTrack(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = checkAccessDeleteBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = checkAccessDeleteBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = checkAccessDeleteBroadcastTrack(track);
            }
        };
        track.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public boolean checkAccessDeleteBroadcastTrack(String trackIdToCheck) throws Exception {
        return checkAccessDeleteBroadcastTrack(getBroadcastTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessDeleteBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = BroadcastableTrack.getDeleteRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeletePendingTrack(String trackIdToCheck) throws Exception {
        return checkAccessDeletePendingTrack(getPendingTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessDeletePendingTrack(PendingTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //If there are no bound programmes to the track to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle rightsBundle = PendingTrack.getDeleteRights();

                RightsBundle restrictedRightsBundle = new RightsBundle();
                restrictedRightsBundle.setContributorRights(true);

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteTrack(Track trackToCheck) throws Exception {
        //Determine which kind of track is this
        BooleanTrackVisitor visitor = new BooleanTrackVisitor() {

            private boolean result = false;
            @Override
            public boolean get() {
                return result;
            }

            @Override
            public void visit(PendingAdvert track) throws Exception {
                result = checkAccessDeletePendingTrack(track);
            }

            @Override
            public void visit(PendingJingle track) throws Exception {
                result = checkAccessDeletePendingTrack(track);
            }

            @Override
            public void visit(PendingSong track) throws Exception {
                result = checkAccessDeletePendingTrack(track);
            }

            @Override
            public void visit(BroadcastableAdvert track) throws Exception {
                result = checkAccessDeleteBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableJingle track) throws Exception {
                result = checkAccessDeleteBroadcastTrack(track);
            }

            @Override
            public void visit(BroadcastableSong track) throws Exception {
                result = checkAccessDeleteBroadcastTrack(track);
            }
        };
        trackToCheck.acceptVisit(visitor);
        return visitor.get();
    }

    @Override
    public boolean checkAccessReviewPendingTracks() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle rightsBundle = PendingTrack.getReviewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
        }
    }

    @Override
    public boolean checkAccessReviewPendingTrack(String trackIdToCheck) throws Exception {
        return checkAccessReviewPendingTrack(getPendingTrack(trackIdToCheck));
    }

    @Override
    public boolean checkAccessReviewPendingTrack(PendingTrack trackToCheck) throws Exception {
        if (trackToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                RightsBundle rightsBundle = PendingTrack.getReviewRights();

                return hasCurrentlyAuthenticatedRestrictedUserRightsOverTrack(trackToCheck, rightsBundle);

            }
        }
        return false;
    }

    @Override
    public void addTrack(final Track track) throws Exception {
        if (track != null) {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(track);
            trackDao.addTrack(track);
        }
    }

    @Override
    public void addTracks(Collection<? extends Track> tracks) throws Exception {
        if (tracks != null) {
            //Timestamp the creation
            getSecurityManager().setCreationTimestamp(tracks);
            trackDao.addBatchTracks(tracks);
            //No file from the datastore to update
        }
    }

    @Override
    public void updateTrack(Track track) throws Exception {
        if (track != null) {
            //Timestamp the edition
            getSecurityManager().setEditionTimestamp(track);
            trackDao.updateTrack(track);
            //No file from the datastore to update
        }
    }

    @Override
    public void updateTracks(Collection<? extends Track> tracks) throws Exception {
        if (tracks != null) {
            //Timestamp the edition
            getSecurityManager().setEditionTimestamp(tracks);
            trackDao.updateBatchTracks(tracks);
            //No file from the datastore to update
        }
    }

    @Override
    public void deleteTrack(Track track) throws Exception {
        trackDao.deleteTrack(track);
    }

    @Override
    public void deleteTracks(Collection<? extends Track> tracks) throws Exception {
        trackDao.deleteBatchTracks(tracks);
    }

    protected void deleteTrack(String id) throws Exception {
        Track track = getTrack(id);
        deleteTrack(track);
    }

    @Override
    public Track getTrack(String id) throws Exception {
        Track track = trackDao.getTrack(Track.class, id);
        if (track == null) {
            throw new NoEntityException();
        }
        return track;
    }

    @Override
    public Track loadTrack(String id) throws Exception {
        Track track = trackDao.loadTrack(Track.class, id);
        if (track == null) {
            throw new NoEntityException();
        }
        return track;
    }

    @Override
    public Collection<? extends Track> loadTracks(Collection<String> ids) throws Exception {
        return loadTracks(ids, Track.class);
    }

    @Override
    public Collection<BroadcastableTrack> loadBroadcastTracks(Collection<String> ids) throws Exception {
        return (Collection<BroadcastableTrack>) loadTracks(ids, BroadcastableTrack.class);
    }

    @Override
    public Collection<PendingTrack> loadPendingTracks(Collection<String> ids) throws Exception {
        return (Collection<PendingTrack>) loadTracks(ids, PendingTrack.class);
    }

    protected Collection<? extends Track> loadTracks(Collection<String> ids, Class<? extends Track> clazz) throws Exception {
        Collection<Track> tracks = trackDao.loadTracks(clazz, ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (tracks == null || ids.size() != tracks.size()) {
            throw new NoEntityException();
        }
        return tracks;
    }

    @Override
    public Collection<? extends Track> getTracks(Collection<String> ids) throws Exception {
        return getTracks(ids, Track.class);
    }

    @Override
    public Collection<BroadcastableTrack> getBroadcastTracks(Collection<String> ids) throws Exception {
        return (Collection<BroadcastableTrack>) getTracks(ids, BroadcastableTrack.class);
    }

    @Override
    public Collection<PendingTrack> getPendingTracks(Collection<String> ids) throws Exception {
        return (Collection<PendingTrack>) getTracks(ids, PendingTrack.class);
    }

    protected Collection<? extends Track> getTracks(Collection<String> ids, Class<? extends Track> clazz) throws Exception {
        Collection<Track> tracks = trackDao.getTracks(clazz, ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (tracks == null || ids.size() != tracks.size()) {
            throw new NoEntityException();
        }
        return tracks;
    }

    @Override
    public BroadcastableTrack getBroadcastTrack(String id) throws Exception {
        BroadcastableTrack track = (BroadcastableTrack) trackDao.getTrack(BroadcastableTrack.class, id);
        if (track == null) {
            throw new NoEntityException();
        }
        return track;
    }

    @Override
    public BroadcastableTrack loadBroadcastTrack(String id) throws Exception {
        BroadcastableTrack track = (BroadcastableTrack) trackDao.loadTrack(BroadcastableTrack.class, id);
        if (track == null) {
            throw new NoEntityException();
        }
        return track;
    }

    @Override
    public PendingTrack getPendingTrack(String id) throws Exception {
        PendingTrack track = (PendingTrack) trackDao.getTrack(PendingTrack.class, id);
        if (track == null) {
            throw new NoEntityException();
        }
        return track;
    }

    @Override
    public PendingTrack loadPendingTrack(String id) throws Exception {
        PendingTrack track = (PendingTrack) trackDao.loadTrack(PendingTrack.class, id);
        if (track == null) {
            throw new NoEntityException();
        }
        return track;
    }

    @Override
    public Collection<? extends BroadcastableTrack> getFilteredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws Exception {
        return getFilteredBroadcastTracksForProgrammeId(constraint, null);
    }

    @Override
    public Collection<? extends PendingTrack> getFilteredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint) throws Exception {
        return getFilteredPendingTracksForProgrammeId(constraint, null);
    }

    @Override
    public Collection<? extends BroadcastableTrack> getFilteredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId) throws Exception {
        Collection<? extends BroadcastableTrack> tracks;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (programmeId == null) {
                tracks = trackDao.getAllBroadcastTracks(constraint);
            } else {
                tracks = trackDao.getBroadcastTracksForProgramme(constraint, programmeId);
            }
        } else {

            //Gather all programmes visible by the authenticated user
            RightsBundle rightsBundle = BroadcastableTrack.getViewRights();

            if (programmeId == null)
                tracks = trackDao.getBroadcastTracksWithRightsByProgrammesForProgrammes(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername());
            else
                tracks = trackDao.getBroadcastTracksWithRightsByProgrammesForProgramme(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    programmeId,
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername());
        }
        return tracks;
    }

    @Override
    public Collection<? extends PendingTrack> getFilteredPendingTracksForProgrammeId(GroupAndSort<PENDING_TRACK_TAGS> constraint, String programmeId) throws Exception {
        Collection<? extends PendingTrack> tracks;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            if (programmeId == null) {
                tracks = trackDao.getAllPendingTracks(constraint);
            } else {
                tracks = trackDao.getPendingTracksForProgramme(constraint, programmeId);
            }
        } else {

            //Gather all programmes visible by the authenticated user
            RightsBundle rightsBundle = PendingTrack.getViewRights();

            if (programmeId == null)
                tracks = trackDao.getPendingTracksWithRightsByProgrammesForProgrammes(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername());
            else
                tracks = trackDao.getPendingTracksWithRightsByProgrammesForProgramme(
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    constraint,
                    programmeId,
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername());
        }
        return tracks;
    }

    /*protected Collection<Programme> getBroadcastTrackProgrammes(Class<? extends BroadcastableTrack> clazz, String author, String title) throws Exception {
        Collection<Programme> programmes = new HashSet<Programme>();
        Collection<BroadcastableTrack> tracks = (Collection<BroadcastableTrack>) trackDao.getTracks(clazz, author, title);
        if (tracks == null || tracks.length()==0) {
            throw new NoEntityException();
        }
        for (BroadcastableTrack track : tracks) {
            programmes.addAll(track.getImmutableProgrammes());
        }
        return programmes;
    }

    public Collection<Programme> getBroadcastTrackProgrammes(String author, String title) throws Exception {
        return getBroadcastTrackProgrammes(BroadcastableTrack.class, author, title);
    }

    public Collection<Programme> getBroadcastSongProgrammes(String author, String title) throws Exception {
        return getBroadcastTrackProgrammes(BroadcastableSong.class, author, title);
    }

    public Collection<Programme> getBroadcastAdvertProgrammes(String author, String title) throws Exception {
        return getBroadcastTrackProgrammes(BroadcastableAdvert.class, author, title);
    }

    public Collection<Programme> getBroadcastJingleProgrammes(String author, String title) throws Exception {
        return getBroadcastTrackProgrammes(BroadcastableJingle.class, author, title);
    }*/

    /*protected Collection<? extends BroadcastableTrack> getFilteredTracksForProgrammeIds(Class<? extends BroadcastableTrack> clazz, String author, String title, Collection<String> programmeIds) throws Exception {
        Collection<? extends BroadcastableTrack> tracks;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            tracks = trackDao.getBroadcastTracksForProgrammes(clazz, author, title, programmeIds);
        } else {

            //Gather all programmes visible by the authenticated user
            RightsBundle rightsBundle = BroadcastableTrack.getViewRights();

            tracks = trackDao.getBroadcastTracksWithRightsByProgrammesForProgrammes(
                    clazz,
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    author,
                    title,
                    programmeIds);
        }
        return tracks;
    }

    public Collection<BroadcastableSong> getFilteredBroadcastSongsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return (Collection<BroadcastableSong>) getFilteredTracksForProgrammeIds(BroadcastableSong.class, author, title, programmeIds);
    }

    public Collection<BroadcastableAdvert> getFilteredBroadcastAdvertsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return (Collection<BroadcastableAdvert>) getFilteredTracksForProgrammeIds(BroadcastableAdvert.class, author, title, programmeIds);
    }

    public Collection<BroadcastableJingle> getFilteredBroadcastJinglesForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return (Collection<BroadcastableJingle>) getFilteredTracksForProgrammeIds(BroadcastableJingle.class, author, title, programmeIds);
    }*/

    @Override
    public Collection<BroadcastableSong> getFuzzyBroadcastSongsForAuthorNames(final List<String> authorNames, boolean combineNames) throws Exception {
        return trackDao.getFuzzyBroadcastSongsForAuthors(authorNames, combineNames);
    }
}
