/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.track.BroadcastableSong;
import biz.ddcr.shampoo.server.domain.track.BroadcastableTrack;
import biz.ddcr.shampoo.server.domain.track.PendingTrack;
import biz.ddcr.shampoo.server.domain.track.Track;

import java.util.Collection;
import java.util.List;

/**
 *
 * @author okay_awright
 **/
public interface TrackManagerInterface {

    /** Internal struct **/
    public class TrackFiles {

        public TrackFiles(boolean doUpdateTrack, String trackUploadId, boolean doUpdatePicture, String coverArtUploadId) {
            this.doUpdateTrack = doUpdateTrack;
            this.trackUploadId = trackUploadId;
            this.doUpdatePicture = doUpdatePicture;
            this.coverArtUploadId = coverArtUploadId;
        }
        private boolean doUpdateTrack;
        private String trackUploadId;
        private boolean doUpdatePicture;
        private String coverArtUploadId;

        public String getCoverArtUploadId() {
            return coverArtUploadId;
        }

        public void setCoverArtUploadId(String coverArtUploadId) {
            this.coverArtUploadId = coverArtUploadId;
        }

        public boolean doUpdatePicture() {
            return doUpdatePicture;
        }

        public void doUpdatePicture(boolean doUpdatePicture) {
            this.doUpdatePicture = doUpdatePicture;
        }

        public boolean doUpdateTrack() {
            return doUpdateTrack;
        }

        public void doUpdateTrack(boolean doUpdateTrack) {
            this.doUpdateTrack = doUpdateTrack;
        }

        public String getTrackUploadId() {
            return trackUploadId;
        }

        public void setTrackUploadId(String trackUploadId) {
            this.trackUploadId = trackUploadId;
        }
    }

    //Check access to resource
    public boolean checkAccessViewTracks() throws Exception;
    public boolean checkAccessViewPendingTracks() throws Exception;
    public boolean checkAccessViewBroadcastTracks() throws Exception;
    public boolean checkAccessViewBroadcastTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessViewBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception;
    public boolean checkAccessViewPendingTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessViewPendingTrack(PendingTrack trackIoCheck) throws Exception;
    public boolean checkAccessViewTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessViewTrack(Track trackToCheck) throws Exception;
    public boolean checkAccessAddBroadcastTracks() throws Exception;
    public boolean checkAccessAddPendingTracks() throws Exception;
    public boolean checkAccessUpdateBroadcastTracks() throws Exception;
    public boolean checkAccessUpdatePendingTracks() throws Exception;
    public boolean checkAccessUpdateTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessUpdateBroadcastTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessUpdateBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception;
    public boolean checkAccessUpdatePendingTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessUpdatePendingTrack(PendingTrack trackToCheck) throws Exception;
    public boolean checkAccessDeletePendingTracks() throws Exception;
    public boolean checkAccessDeleteBroadcastTracks() throws Exception;
    public boolean checkAccessDeleteTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessDeleteBroadcastTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessDeleteBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception;
    public boolean checkAccessDeletePendingTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessDeletePendingTrack(PendingTrack trackToCheck) throws Exception;
    public boolean checkAccessDeleteTrack(Track trackToCheck) throws Exception;

    //Supplementary checks
    public boolean checkAccessReviewPendingTracks() throws Exception;
    public boolean checkAccessReviewPendingTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessReviewPendingTrack(PendingTrack trackToCheck) throws Exception;
    public boolean checkAccessExtendedViewTracks() throws Exception;
    public boolean checkAccessExtendedViewPendingTracks() throws Exception;
    public boolean checkAccessExtendedViewBroadcastTracks() throws Exception;
    public boolean checkAccessExtendedViewBroadcastTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessExtendedViewBroadcastTrack(BroadcastableTrack trackToCheck) throws Exception;
    public boolean checkAccessExtendedViewPendingTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessExtendedViewPendingTrack(PendingTrack trackIoCheck) throws Exception;
    public boolean checkAccessExtendedViewTrack(String trackIdToCheck) throws Exception;
    public boolean checkAccessExtendedViewTrack(Track trackToCheck) throws Exception;

    //Editing objects
    public void addTrack(Track track) throws Exception;
    public void addTracks(Collection<? extends Track> tracks) throws Exception;

    public void updateTrack(Track track) throws Exception;
    public void updateTracks(Collection<? extends Track> tracks) throws Exception;

    public void deleteTrack(Track track) throws Exception;
    public void deleteTracks(Collection<? extends Track> tracks) throws Exception;

    //Fetch objects
    public BroadcastableTrack loadBroadcastTrack(String id) throws Exception;
    public PendingTrack loadPendingTrack(String id) throws Exception;
    public Track loadTrack(String id) throws Exception;
    public BroadcastableTrack getBroadcastTrack(String id) throws Exception;
    public PendingTrack getPendingTrack(String id) throws Exception;
    public Track getTrack(String id) throws Exception;
    public Collection<? extends Track> loadTracks(Collection<String> ids) throws Exception;
    public Collection<BroadcastableTrack> loadBroadcastTracks(Collection<String> ids) throws Exception;
    public Collection<PendingTrack> loadPendingTracks(Collection<String> ids) throws Exception;
    public Collection<? extends Track> getTracks(Collection<String> ids) throws Exception;
    public Collection<BroadcastableTrack> getBroadcastTracks(Collection<String> ids) throws Exception;
    public Collection<PendingTrack> getPendingTracks(Collection<String> ids) throws Exception;

    //Unused: removed
    /*public BroadcastableSong getBroadcastSong(String id) throws Exception;
    public BroadcastableAdvert getBroadcastAdvert(String id) throws Exception;
    public BroadcastableJingle getBroadcastJingle(String id) throws Exception;
    public PendingSong getPendingSong(String id) throws Exception;
    public PendingAdvert getPendingAdvert(String id) throws Exception;
    public PendingJingle getPendingJingle(String id) throws Exception;*/

    //Fetching objects with associated access policy flags
    public Collection<? extends BroadcastableTrack> getFilteredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws Exception;
    public Collection<? extends PendingTrack> getFilteredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint) throws Exception;
    public Collection<? extends BroadcastableTrack> getFilteredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId) throws Exception;
    public Collection<? extends PendingTrack> getFilteredPendingTracksForProgrammeId(GroupAndSort<PENDING_TRACK_TAGS> constraint, String programmeId) throws Exception;

    //Helper
    /*public Collection<BroadcastableSong> getFilteredBroadcastSongsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    public Collection<BroadcastableAdvert> getFilteredBroadcastAdvertsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;
    public Collection<BroadcastableJingle> getFilteredBroadcastJinglesForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception;*/

    //Helper for the Discogs metadata merging CLI tool
    public Collection<BroadcastableSong> getFuzzyBroadcastSongsForAuthorNames(final List<String> authorNames, boolean combineNames) throws Exception;

}
