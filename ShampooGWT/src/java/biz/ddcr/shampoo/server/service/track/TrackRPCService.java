/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.track;

import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm;
import biz.ddcr.shampoo.client.form.track.BroadcastTrackForm.BROADCASTABLE_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.EmbeddedTagModule;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm;
import biz.ddcr.shampoo.client.form.track.PendingTrackForm.PENDING_TRACK_TAGS;
import biz.ddcr.shampoo.client.form.track.TrackForm;
import biz.ddcr.shampoo.client.form.track.format.ContainerModule;
import biz.ddcr.shampoo.client.form.track.format.CoverArtModule;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.serviceAsync.track.TrackRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class TrackRPCService extends GWTRPCLocalThreadService implements TrackRPCServiceInterface {

    private TrackFacadeServiceInterface trackFacadeService;

    public TrackFacadeServiceInterface getTrackFacadeService() {
        return trackFacadeService;
    }

    public void setTrackFacadeService(TrackFacadeServiceInterface trackFacadeService) {
        this.trackFacadeService = trackFacadeService;
    }

    @Override
    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm, String currentTrackUploadId, String currentCoverArtUploadId) throws Exception {
        trackFacadeService.asynchronousAddSecuredBroadcastTrackAndAudioAndPictureFile(trackForm, currentTrackUploadId, currentCoverArtUploadId);
    }

    @Override
    public void addSecuredBroadcastTrack(BroadcastTrackForm trackForm, String currentTrackUploadId) throws Exception {
        trackFacadeService.asynchronousAddSecuredBroadcastTrackAndAudioFile(trackForm, currentTrackUploadId);
    }

    @Override
    public void addSecuredPendingTrack(PendingTrackForm trackForm, String currentTrackUploadId, String currentCoverArtUploadId) throws Exception {
        trackFacadeService.asynchronousAddSecuredPendingTrackAndAudioAndPictureFile(trackForm, currentTrackUploadId, currentCoverArtUploadId);
    }

    @Override
    public void addSecuredPendingTrack(PendingTrackForm trackForm, String currentTrackUploadId) throws Exception {
        trackFacadeService.asynchronousAddSecuredPendingTrackAndAudioFile(trackForm, currentTrackUploadId);
    }

    public void deleteSecuredTrack(String trackId) throws Exception {
        trackFacadeService.asynchronousDeleteSecuredTrack(trackId);
    }

    public void deleteSecuredTracks(Collection<String> trackIds) throws Exception {
        trackFacadeService.asynchronousDeleteSecuredTracks(trackIds);
    }

    @Override
    public void rejectSecuredPendingTrack(String pendingTrackId, String comment) throws Exception {
        trackFacadeService.asynchronousRejectSecuredPendingTrack(pendingTrackId, comment);
    }

    @Override
    public void rejectSecuredPendingTracks(Collection<String> pendingTrackIds, String comment) throws Exception {
        trackFacadeService.asynchronousRejectSecuredPendingTracks(pendingTrackIds, comment);
    }

    @Override
    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredBroadcastTrackAndAudioAndPictureFile(trackForm, trackUploadId, coverArtUploadId);
    }

    @Override
    public void updateSecuredBroadcastTrack(BroadcastTrackForm trackForm) throws Exception {
        trackFacadeService.updateSecuredBroadcastTrack(trackForm);
    }

    @Override
    public void updateSecuredBroadcastTracks(Collection<BroadcastTrackForm> trackForms) throws Exception {
        trackFacadeService.updateSecuredBroadcastTracks(trackForms);
    }

    @Override
    public void updateSecuredBroadcastTrackAndAudioOnly(BroadcastTrackForm trackForm, String trackUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredBroadcastTrackAndAudioFile(trackForm, trackUploadId);
    }

    @Override
    public void updateSecuredBroadcastTrackAndPictureOnly(BroadcastTrackForm trackForm, String coverArtUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredBroadcastTrackAndPictureFile(trackForm, coverArtUploadId);
    }

    @Override
    public void updateSecuredBroadcastTracksAndPictureOnly(Collection<BroadcastTrackForm> trackForms, String coverArtUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredBroadcastTracksAndPictureFile(trackForms, coverArtUploadId);
    }

    @Override
    public void updateSecuredPendingTrack(PendingTrackForm trackForm, String trackUploadId, String coverArtUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredPendingTrackAndAudioAndPictureFile(trackForm, trackUploadId, coverArtUploadId);
    }

    @Override
    public void updateSecuredPendingTrack(PendingTrackForm trackForm) throws Exception {
        trackFacadeService.updateSecuredPendingTrack(trackForm);
    }

    @Override
    public void updateSecuredPendingTracks(Collection<PendingTrackForm> trackForms) throws Exception {
        trackFacadeService.updateSecuredPendingTracks(trackForms);
    }

    @Override
    public void updateSecuredPendingTrackAndAudioOnly(PendingTrackForm trackForm, String trackUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredPendingTrackAndAudioFile(trackForm, trackUploadId);
    }

    @Override
    public void updateSecuredPendingTrackAndPictureOnly(PendingTrackForm trackForm, String coverArtUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredPendingTrackAndPictureFile(trackForm, coverArtUploadId);
    }

    @Override
    public void updateSecuredPendingTracksAndPictureOnly(Collection<PendingTrackForm> trackForms, String coverArtUploadId) throws Exception {
        trackFacadeService.asynchronousUpdateSecuredPendingTracksAndPictureFile(trackForms, coverArtUploadId);
    }

    @Override
    public void validateSecuredPendingTrack(String pendingTrackId) throws Exception {
        trackFacadeService.asynchronousValidateSecuredPendingTrack(pendingTrackId);
    }

    @Override
    public void validateSecuredPendingTracks(Collection<String> pendingTrackIds) throws Exception {
        trackFacadeService.asynchronousValidateSecuredPendingTracks(pendingTrackIds);
    }

    @Override
    public ActionCollectionEntry<? extends TrackForm> getSecuredTrack(String id) throws Exception {
        return trackFacadeService.getSecuredTrack(id);
    }

    @Override
    public ActionCollectionEntry<PendingTrackForm> getSecuredPendingTrack(String id) throws Exception {
        return trackFacadeService.getSecuredPendingTrack(id);
    }

    @Override
    public ActionCollectionEntry<BroadcastTrackForm> getSecuredBroadcastTrack(String id) throws Exception {
        return trackFacadeService.getSecuredBroadcastTrack(id);
    }

    @Override
    public ActionCollection<? extends PendingTrackForm> getSecuredPendingTracks(GroupAndSort<PENDING_TRACK_TAGS> constraint) throws Exception {
        return trackFacadeService.getSecuredPendingTracks(constraint);
    }

    @Override
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracks(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint) throws Exception {
        return trackFacadeService.getSecuredBroadcastTracks(constraint);
    }

    @Override
    public ActionCollection<? extends BroadcastTrackForm> getSecuredBroadcastTracksForProgrammeId(GroupAndSort<BROADCASTABLE_TRACK_TAGS> constraint, String programmeId) throws Exception {
        return trackFacadeService.getSecuredBroadcastTracksForProgrammeId(constraint, programmeId);
    }

    @Override
    public EmbeddedTagModule getDraftTags(String id) throws Exception {
        return trackFacadeService.getSecuredDraftTags(id);
    }

    @Override
    public EmbeddedTagModule getTags(String id) throws Exception {
        return trackFacadeService.getSecuredTags(id);
    }

    @Override
    public ContainerModule getDraftAudioFileInfo(String id) throws Exception {
        return trackFacadeService.getSecuredDraftAudioFileInfo(id);
    }

    @Override
    public CoverArtModule getDraftPictureFileInfo(String id) throws Exception {
        return trackFacadeService.getSecuredDraftPictureFileInfo(id);
    }

    /*public ActionCollection<BroadcastSongForm> getSecuredBroadcastSongsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return trackFacadeService.getSecuredBroadcastSongsForProgrammeIds(author, title, programmeIds);
    }

    public ActionCollection<BroadcastAdvertForm> getSecuredBroadcastAdvertsForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return trackFacadeService.getSecuredBroadcastAdvertsForProgrammeIds(author, title, programmeIds);
    }

    public ActionCollection<BroadcastJingleForm> getSecuredBroadcastJinglesForProgrammeIds(String author, String title, Collection<String> programmeIds) throws Exception {
        return trackFacadeService.getSecuredBroadcastJinglesForProgrammeIds(author, title, programmeIds);
    }*/

    @Override
    public void deleteSecuredBroadcastTrack(String trackId) throws Exception {
        trackFacadeService.asynchronousDeleteSecuredTrack(trackId);
    }

    @Override
    public void deleteSecuredBroadcastTracks(Collection<String> trackIds) throws Exception {
        trackFacadeService.asynchronousDeleteSecuredTracks(trackIds);
    }

    @Override
    public void deleteSecuredPendingTrack(String trackId) throws Exception {
        trackFacadeService.asynchronousDeleteSecuredTrack(trackId);
    }

    @Override
    public void deleteSecuredPendingTracks(Collection<String> trackIds) throws Exception {
        trackFacadeService.asynchronousDeleteSecuredTracks(trackIds);
    }
}
