/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.service.user;

import biz.ddcr.shampoo.client.form.channel.CHANNEL_ACTION;
import biz.ddcr.shampoo.client.form.programme.PROGRAMME_ACTION;
import biz.ddcr.shampoo.client.helper.permissions.ACTION;
import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.AdministratorFormWithPassword;
import biz.ddcr.shampoo.client.form.user.RESTRICTEDUSER_ACTION;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserFormWithPassword;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.permissions.BASIC_ACTION;
import biz.ddcr.shampoo.client.helper.view.*;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.helper.DateDTOAssembler;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author okay_awright
 **/
public class UserDTOAssembler extends GenericService implements UserDTOAssemblerInterface {

    private interface FormUserVisitor extends UserVisitor {
        public ActionCollectionEntry<? extends UserForm> get();
    }
    private interface ActionMapStringsUserVisitor extends UserVisitor {
        public ActionMap<String> get();
    }

    private UserManagerInterface userManager;
    private ProgrammeManagerInterface programmeManager;
    private ChannelManagerInterface channelManager;

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    @Override
    public Administrator fromDTO(AdministratorForm userForm) throws Exception {
        if (userForm != null) {
            Administrator newUser = new Administrator();
            //Fill in all the fields
            newUser.setEnabled(userForm.isEnabled());
            newUser.setFirstAndLastName(userForm.getFirstAndLastName());
            newUser.setTimezone(userForm.getTimezone());
            if (ProxiedClassUtil.castableAs(userForm, AdministratorFormWithPassword.class)) {
                newUser.setPassword(((AdministratorFormWithPassword) userForm).getPassword());
            }
            newUser.setEmail(userForm.getEmail());
            newUser.setUsername(userForm.getUsername());
            return newUser;
        }
        return null;
    }

    /**
     * update an existing track with data from a DTO.
     * Returns true if attributes of the original track have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
     public boolean augmentWithDTO(Administrator user, AdministratorForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && user != null) {

            if (form.isEnabled()!=user.isEnabled()) {
                user.setEnabled(form.isEnabled());
                contentUpdated = true;
            }
            if ((form.getFirstAndLastName()!=null && !form.getFirstAndLastName().equals(user.getFirstAndLastName())
                    || (form.getFirstAndLastName()==null && user.getFirstAndLastName()!=null))) {
                user.setFirstAndLastName(form.getFirstAndLastName());
                contentUpdated = true;
            }
            if ((form.getTimezone()!=null && !form.getTimezone().equals(user.getTimezone())
                    || (form.getTimezone()==null && user.getTimezone()!=null))) {
                user.setTimezone(form.getTimezone());
                contentUpdated = true;
            }
            if (ProxiedClassUtil.castableAs(form, AdministratorFormWithPassword.class)) {
                if ((((AdministratorFormWithPassword)form).getPassword()!=null && ((AdministratorFormWithPassword)form).getPassword().equals(user.getPassword())
                        || (((AdministratorFormWithPassword)form).getPassword()==null && user.getPassword()!=null))) {
                    user.setPassword(((AdministratorFormWithPassword)form).getPassword());
                    contentUpdated = true;
                }
            }
            if ((form.getEmail()!=null && !form.getEmail().equals(user.getEmail())
                    || (form.getEmail()==null && user.getEmail()!=null))) {
                user.setEmail(form.getEmail());
                contentUpdated = true;
            }
            if ((form.getUsername()!=null && !form.getUsername().equals(user.getUsername())
                    || (form.getUsername()==null && user.getUsername()!=null))) {
                user.setUsername(form.getUsername());
                contentUpdated = true;
            }

        }
        return contentUpdated;
     }

    /**
     * update an existing track with data from a DTO.
     * Returns true if attributes of the original track have been changed.
     * @param channel
     * @param form
     * @return
     * @throws Exception
     */
    @Override
     public boolean augmentWithDTO(RestrictedUser user, RestrictedUserForm form) throws Exception {

        boolean contentUpdated = false;

        if (form != null && user != null) {

            if (form.isEnabled()!=user.isEnabled()) {
                user.setEnabled(form.isEnabled());
                contentUpdated = true;
            }
            if ((form.getFirstAndLastName()!=null && !form.getFirstAndLastName().equals(user.getFirstAndLastName())
                    || (form.getFirstAndLastName()==null && user.getFirstAndLastName()!=null))) {
                user.setFirstAndLastName(form.getFirstAndLastName());
                contentUpdated = true;
            }
            if ((form.getTimezone()!=null && !form.getTimezone().equals(user.getTimezone())
                    || (form.getTimezone()==null && user.getTimezone()!=null))) {
                user.setTimezone(form.getTimezone());
                contentUpdated = true;
            }
            if (ProxiedClassUtil.castableAs(form, RestrictedUserFormWithPassword.class)) {
                if ((((RestrictedUserFormWithPassword)form).getPassword()!=null && !((RestrictedUserFormWithPassword)form).getPassword().equals(user.getPassword())
                        || (((RestrictedUserFormWithPassword)form).getPassword()==null && user.getPassword()!=null))) {
                    user.setPassword(((RestrictedUserFormWithPassword)form).getPassword());
                    contentUpdated = true;
                }
            }
            if ((form.getEmail()!=null && !form.getEmail().equals(user.getEmail())
                    || (form.getEmail()==null && user.getEmail()!=null))) {
                user.setEmail(form.getEmail());
                contentUpdated = true;
            }
            if ((form.getUsername()!=null && !form.getUsername().equals(user.getUsername())
                    || (form.getUsername()==null && user.getUsername()!=null))) {
                user.setUsername(form.getUsername());
                contentUpdated = true;
            }
            if (form.isEmailNotification()!=user.isEmailNotification()) {
                user.setEmailNotification(form.isEmailNotification());
                contentUpdated = true;
            }

            //for (Channel channel : user.getChannelAdministratorRights()) {
            for (Iterator<Channel> i = user.getChannelAdministratorRights().iterator(); i.hasNext();) {
                Channel channel = i.next();
                if (getChannelManager().checkAccessUpdateChannel(channel)) {
                    i.remove();
                    user.removeChannelAdministratorRight(channel);
                }
            }
            for (ActionCollectionEntry<String> channelEntry : form.getChannelAdministratorRights()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessUpdateChannel(c))
                    user.addChannelAdministratorRight(c);
            }

            //for (Channel channel : user.getProgrammeManagerRights()) {
            for (Iterator<Channel> i = user.getProgrammeManagerRights().iterator(); i.hasNext();) {
                Channel channel = i.next();
                if (getChannelManager().checkAccessUpdateChannel(channel)) {
                    i.remove();
                    user.removeProgrammeManagerRight(channel);
                }
            }
            for (ActionCollectionEntry<String> channelEntry : form.getProgrammeManagerRights()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessUpdateChannel(c))
                    user.addProgrammeManagerRight(c);
            }

            //for (Channel channel : user.getListenerRights()) {
            for (Iterator<Channel> i = user.getListenerRights().iterator(); i.hasNext();) {
                Channel channel = i.next();
                if (getChannelManager().checkAccessUpdateChannel(channel)) {
                    i.remove();
                    user.removeListenerRight(channel);
                }
            }
            for (ActionCollectionEntry<String> channelEntry : form.getListenerRights()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessUpdateChannel(c))
                    user.addListenerRight(c);
            }

            //for (Programme programme : user.getAnimatorRights()) {
            for (Iterator<Programme> i = user.getAnimatorRights().iterator(); i.hasNext();) {
                Programme programme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                    i.remove();
                    user.removeAnimatorRight(programme);
                }
            }
            for (ActionCollectionEntry<String> programmeEntry : form.getAnimatorRights()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p))
                    user.addAnimatorRight(p);
            }

            //for (Programme programme : user.getCuratorRights()) {
            for (Iterator<Programme> i = user.getCuratorRights().iterator(); i.hasNext();) {
                Programme programme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                    i.remove();
                    user.removeCuratorRight(programme);
                }
            }
            for (ActionCollectionEntry<String> programmeEntry : form.getCuratorRights()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p))
                    user.addCuratorRight(p);
            }

            //for (Programme programme : user.getContributorRights()) {
            for (Iterator<Programme> i = user.getContributorRights().iterator(); i.hasNext();) {
                Programme programme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                    i.remove();
                    user.removeContributorRight(programme);
                }
            }
            for (ActionCollectionEntry<String> programmeEntry : form.getContributorRights()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p))
                    user.addContributorRight(p);
            }

        }
        return contentUpdated;
     }

    @Override
    public RestrictedUser fromDTO(RestrictedUserForm userForm) throws Exception {
        if (userForm != null) {
            RestrictedUser newUser = new RestrictedUser();
            //Fill in all the fields
            newUser.setEnabled(userForm.isEnabled());
            newUser.setTimezone(userForm.getTimezone());
            newUser.setFirstAndLastName(userForm.getFirstAndLastName());
            if (ProxiedClassUtil.castableAs(userForm, RestrictedUserFormWithPassword.class)) {
                newUser.setPassword(((RestrictedUserFormWithPassword) userForm).getPassword());
            }
            newUser.setEmail(userForm.getEmail());
            newUser.setUsername(userForm.getUsername());
            newUser.setEmailNotification(userForm.isEmailNotification());

            //Rights
            for (ActionCollectionEntry<String> channelEntry : userForm.getChannelAdministratorRights()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessUpdateChannel(c))
                    newUser.addChannelAdministratorRight(c);
            }
            for (ActionCollectionEntry<String> channelEntry : userForm.getProgrammeManagerRights()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessUpdateChannel(c))
                    newUser.addProgrammeManagerRight(c);
            }
            for (ActionCollectionEntry<String> channelEntry : userForm.getListenerRights()) {
                Channel c = getChannelManager().getChannel(channelEntry.getKey());
                if (getChannelManager().checkAccessUpdateChannel(c))
                    newUser.addListenerRight(c);
            }
            for (ActionCollectionEntry<String> programmeEntry : userForm.getAnimatorRights()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p))
                    newUser.addAnimatorRight(p);
            }
            for (ActionCollectionEntry<String> programmeEntry : userForm.getCuratorRights()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p))
                    newUser.addCuratorRight(p);
            }
            for (ActionCollectionEntry<String> programmeEntry : userForm.getContributorRights()) {
                Programme p = getProgrammeManager().getProgramme(programmeEntry.getKey());
                if (getProgrammeManager().checkAccessUpdateProgramme(p))
                    newUser.addContributorRight(p);
            }

            return newUser;
        }
        return null;
    }

    @Override
    public ActionCollectionEntry<AdministratorFormWithPassword> toDTOWithPassword(Administrator user) throws Exception {
        if (user==null) return null;
        return (ActionCollectionEntry<AdministratorFormWithPassword>) toDTO(user, true);
    }

    @Override
    public ActionCollectionEntry<RestrictedUserFormWithPassword> toDTOWithPassword(RestrictedUser user) throws Exception {
        if (user==null) return null;
        return (ActionCollectionEntry<RestrictedUserFormWithPassword>)toDTO(user, true);
    }

    @Override
    public ActionCollectionEntry<AdministratorForm> toDTO(Administrator user) throws Exception {
        if (user==null) return null;
        return (ActionCollectionEntry<AdministratorForm>) toDTO(user, false);
    }

    @Override
    public ActionCollectionEntry<RestrictedUserForm> toDTO(RestrictedUser user) throws Exception {
        if (user==null) return null;
        return (ActionCollectionEntry<RestrictedUserForm>) toDTO(user, false);
    }

    protected ActionCollectionEntry<? extends AdministratorForm> toDTO(Administrator user, boolean setPassword) throws Exception {
        AdministratorForm userForm = null;
        if (setPassword) {
            userForm = new AdministratorFormWithPassword();
        } else {
            userForm = new AdministratorForm();
        }
        
        //Fill in common properties
        userForm.setCreatorId(user.getCreator()!=null?user.getCreator().getUsername():null);
        userForm.setLatestEditorId(user.getLatestEditor()!=null?user.getLatestEditor().getUsername():null);
        userForm.setCreationDate(user.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(user.getCreationDate().getTime(), null)) : null);
        userForm.setLatestModificationDate(user.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(user.getLatestModificationDate().getTime(), null)) : null);

        //Fill in other properties
        userForm.setEnabled(user.isEnabled());
        userForm.setFirstAndLastName(user.getFirstAndLastName());
        userForm.setTimezone(user.getTimezone());
        //never output password to the Presentation layer if not explictly required
        if (ProxiedClassUtil.castableAs(userForm, AdministratorFormWithPassword.class)) {
            ((AdministratorFormWithPassword) userForm).setPassword(user.getPassword());
        }
        userForm.setEmail(user.getEmail());
        userForm.setUsername(user.getUsername());

        return new ActionMapEntry<AdministratorForm>(userForm, decorateEntryWithActions(user));
    }

    protected ActionCollectionEntry<? extends RestrictedUserForm> toDTO(RestrictedUser user, boolean setPassword) throws Exception {

        RestrictedUserForm userForm = null;

        if (setPassword) {
            userForm = new RestrictedUserFormWithPassword();
        } else {
            userForm = new RestrictedUserForm();
        }
        
        //Fill in common properties
        userForm.setCreatorId(user.getCreator()!=null?user.getCreator().getUsername():null);
        userForm.setLatestEditorId(user.getLatestEditor()!=null?user.getLatestEditor().getUsername():null);
        userForm.setCreationDate(user.getCreationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(user.getCreationDate().getTime(), null)) : null);
        userForm.setLatestModificationDate(user.getLatestModificationDate() != null ? DateDTOAssembler.toJS(new biz.ddcr.shampoo.server.helper.YearMonthWeekDayHourMinute(user.getLatestModificationDate().getTime(), null)) : null);

        //Fill in other properties
        userForm.setEnabled(user.isEnabled());
        userForm.setFirstAndLastName(user.getFirstAndLastName());
        userForm.setTimezone(user.getTimezone());
        //never output password to the Presentation layer if not explictly required
        if (ProxiedClassUtil.castableAs(userForm, RestrictedUserFormWithPassword.class)) {
            ((RestrictedUserFormWithPassword) userForm).setPassword(user.getPassword());
        }
        userForm.setEmail(user.getEmail());
        userForm.setUsername(user.getUsername());
        userForm.setEmailNotification(user.isEmailNotification());
        //Rights
        ActionMap<String> newChannelAdministratorRights = new ActionSortedMap<String>();
        for (Channel channel : user.getChannelAdministratorRights()) {
            newChannelAdministratorRights.set(channel.getLabel(), decorateEntryWithActions(channel));
        }
        userForm.setChannelAdministratorRights(newChannelAdministratorRights);
        ActionMap<String> newProgrammeManagerRights = new ActionSortedMap<String>();
        for (Channel channel : user.getProgrammeManagerRights()) {
            newProgrammeManagerRights.set(channel.getLabel(), decorateEntryWithActions(channel));
        }
        userForm.setProgrammeManagerRights(newProgrammeManagerRights);
        ActionMap<String> newListenerRights = new ActionSortedMap<String>();
        for (Channel channel : user.getListenerRights()) {
            newListenerRights.set(channel.getLabel(), decorateEntryWithActions(channel));
        }
        userForm.setListenerRights(newListenerRights);
        ActionMap<String> newAnimatorRights = new ActionSortedMap<String>();
        for (Programme programme : user.getAnimatorRights()) {
            newAnimatorRights.set(programme.getLabel(), decorateEntryWithActions(programme));
        }
        userForm.setAnimatorRights(newAnimatorRights);
        ActionMap<String> newCuratorRights = new ActionSortedMap<String>();
        for (Programme programme : user.getCuratorRights()) {
            newCuratorRights.set(programme.getLabel(), decorateEntryWithActions(programme));
        }
        userForm.setCuratorRights(newCuratorRights);
        ActionMap<String> newContributorRights = new ActionSortedMap<String>();
        for (Programme programme : user.getContributorRights()) {
            newContributorRights.set(programme.getLabel(), decorateEntryWithActions(programme));
        }
        userForm.setContributorRights(newContributorRights);

        return new ActionMapEntry<RestrictedUserForm>(userForm, decorateEntryWithActions(user));
    }

    @Override
    public ActionCollection<? extends UserForm> toUserDTO(Collection<? extends User> users, boolean doSort) throws Exception {
        FormUserVisitor visitor = new FormUserVisitor() {
            private ActionCollectionEntry<? extends UserForm> form;
            @Override
            public void visit(RestrictedUser user) throws Exception {
                form = toDTO(user);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                form = toDTO(user);
            }

            @Override
            public ActionCollectionEntry<? extends UserForm> get() {
                return form;
            }
        };
        ActionMap<UserForm> userForms = doSort ? new ActionSortedMap<UserForm>() : new ActionUnsortedMap<UserForm>();
        if (users!=null) {
            for (User user : users) {
                user.acceptVisit(visitor);
                ActionCollectionEntry<? extends UserForm> form = visitor.get();
                if (form!=null)
                    userForms.set(form);
            }
        }
        return userForms;
    }

    @Override
    public ActionCollection<RestrictedUserForm> toRestrictedUserDTO(Collection<RestrictedUser> users, boolean doSort) throws Exception {
        ActionMap<RestrictedUserForm> userForms = doSort ? new ActionSortedMap<RestrictedUserForm>() : new ActionUnsortedMap<RestrictedUserForm>();
        if (users!=null)
            for (RestrictedUser user : users) {
                userForms.set(toDTO(user));
            }
        return userForms;
    }

    @Override
    public ActionCollection<AdministratorForm> toAdministratorDTO(Collection<Administrator> users, boolean doSort) throws Exception {
        ActionMap<AdministratorForm> userForms = doSort ? new ActionSortedMap<AdministratorForm>() : new ActionUnsortedMap<AdministratorForm>();
        if (users!=null)
            for (Administrator user : users) {
                userForms.set(toDTO(user));
            }
        return userForms;
    }

    @Override
    public ActionCollection<String> toStringDTO(Collection<? extends User> users, boolean doSort) throws Exception {
        ActionMap<String> userStrings = doSort ? new ActionSortedMap<String>() : new ActionUnsortedMap<String>();
        ActionMapStringsUserVisitor visitor = new ActionMapStringsUserVisitor() {

            ActionMap<String> userStrings;
            @Override
            public ActionMap<String> get() {
                return userStrings;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                userStrings = new ActionSortedMap<String>();
                userStrings.set(user.getUsername(), UserDTOAssembler.this.decorateEntryWithActions(user));
            }

            @Override
            public void visit(Administrator user) throws Exception {
                userStrings = new ActionSortedMap<String>();
                userStrings.set(user.getUsername(), UserDTOAssembler.this.decorateEntryWithActions(user));
            }
        };
        for (User user : users) {
            user.acceptVisit(visitor);
            if (visitor.get()!=null)
                userStrings.set(visitor.get());
        }
        return userStrings;
    }

    protected Collection<ACTION> decorateEntryWithActions(RestrictedUser user) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getUserManager().checkAccessViewRestrictedUser(user)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(user)) {
                actions.add(RESTRICTEDUSER_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessUpdateRestrictedUser(user)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessDeleteRestrictedUser(user)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Administrator user) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getUserManager().checkAccessViewAdministrator(user)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessUpdateAdministrator(user)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getUserManager().checkAccessDeleteAdministrator(user)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Channel channel) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getChannelManager().checkAccessViewChannel(channel)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessLimitedProgrammeDependencyUpdateChannel(channel)) {
                actions.add(CHANNEL_ACTION.LIMITED_PROGRAMME_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessUpdateChannel(channel)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getChannelManager().checkAccessDeleteChannel(channel)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }

    protected Collection<ACTION> decorateEntryWithActions(Programme programme) throws Exception {
        HashSet<ACTION> actions = new HashSet<ACTION>();
        //By default if it's in the collection then the user can see it
        try {
            if (getProgrammeManager().checkAccessViewProgramme(programme)) {
                actions.add(BASIC_ACTION.READ);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        //ReadExtended is used by Curators and Contributors for linking programmes to tracks, they cannot edit a programme yet they can bind tracks
        try {
            if (getProgrammeManager().checkAccessLimitedTrackDependencyUpdateProgramme(programme)) {
                actions.add(PROGRAMME_ACTION.LIMITED_TRACK_DEPENDENCY_UPDATE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessUpdateProgramme(programme)) {
                actions.add(BASIC_ACTION.EDIT);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        try {
            if (getProgrammeManager().checkAccessDeleteProgramme(programme)) {
                actions.add(BASIC_ACTION.DELETE);
            }
        } catch (AccessDeniedException e) {
            //Do nothing, just don't add the flag
        }
        return actions;
    }
}
