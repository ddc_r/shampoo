/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.user;

import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.AdministratorFormWithPassword;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserFormWithPassword;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface UserDTOAssemblerInterface {

    //pure DTO to domain conversion
    public RestrictedUser fromDTO(RestrictedUserForm userForm) throws Exception;
    public boolean augmentWithDTO(RestrictedUser restrictedUser, RestrictedUserForm userForm) throws Exception;
    public Administrator fromDTO(AdministratorForm userForm) throws Exception;
    public boolean augmentWithDTO(Administrator administrator, AdministratorForm userForm) throws Exception;

    //Domain to DTO conversion
    public ActionCollectionEntry<RestrictedUserForm> toDTO(RestrictedUser user) throws Exception;
    public ActionCollectionEntry<AdministratorForm> toDTO(Administrator user) throws Exception;
    public ActionCollectionEntry<RestrictedUserFormWithPassword> toDTOWithPassword(RestrictedUser user) throws Exception;
    public ActionCollectionEntry<AdministratorFormWithPassword> toDTOWithPassword(Administrator user) throws Exception;

    //helpers
    public ActionCollection<? extends UserForm> toUserDTO(Collection<? extends User> users, boolean doSort) throws Exception;
    public ActionCollection<RestrictedUserForm> toRestrictedUserDTO(Collection<RestrictedUser> users, boolean doSort) throws Exception;
    public ActionCollection<AdministratorForm> toAdministratorDTO(Collection<Administrator> users, boolean doSort) throws Exception;
    public ActionCollection<String> toStringDTO(Collection<? extends User> users, boolean doSort) throws Exception;
    
}
