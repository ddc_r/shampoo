/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.user;

import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.AdministratorFormWithPassword;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserFormWithPassword;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import biz.ddcr.shampoo.client.form.user.UserFormWithPassword;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelChannelAdministratorLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelChannelAdministratorLinkNotification.CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelListenerLinkNotification.CHANNELLISTENER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeManagerLinkNotification;
import biz.ddcr.shampoo.server.domain.channel.notification.ChannelProgrammeManagerLinkNotification.CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeAnimatorLinkNotification.PROGRAMMEANIMATOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeContributorLinkNotification.PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification;
import biz.ddcr.shampoo.server.domain.programme.notification.ProgrammeCuratorLinkNotification.PROGRAMMECURATOR_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.log.UserLog.USER_LOG_OPERATION;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.domain.user.log.UserLog;
import biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification;
import biz.ddcr.shampoo.server.domain.user.notification.RestrictedUserContentNotification.RESTRICTEDUSER_NOTIFICATION_OPERATION;
import biz.ddcr.shampoo.server.helper.CollectionUtil;
import biz.ddcr.shampoo.server.helper.hibernate.ProxiedClassUtil;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.programme.ProgrammeManagerInterface;
import biz.ddcr.shampoo.server.service.channel.ChannelManagerInterface;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 **/
public class UserFacadeService extends GenericService implements UserFacadeServiceInterface {

    /** phony visitor pattern **/
    private interface UserClassVisitor extends UserVisitor {
        public ActionCollectionEntry<? extends UserForm> get();
    }
    
    private UserDTOAssemblerInterface userDTOAssembler;
    private UserManagerInterface userManager;
    private ChannelManagerInterface channelManager;
    private ProgrammeManagerInterface programmeManager;
    private SecurityManagerInterface securityManager;

    public UserDTOAssemblerInterface getUserDTOAssembler() {
        return userDTOAssembler;
    }

    public void setUserDTOAssembler(UserDTOAssemblerInterface userDTOAssembler) {
        this.userDTOAssembler = userDTOAssembler;
    }

    public UserManagerInterface getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManagerInterface userManager) {
        this.userManager = userManager;
    }

    public ProgrammeManagerInterface getProgrammeManager() {
        return programmeManager;
    }

    public void setProgrammeManager(ProgrammeManagerInterface programmeManager) {
        this.programmeManager = programmeManager;
    }

    public ChannelManagerInterface getChannelManager() {
        return channelManager;
    }

    public void setChannelManager(ChannelManagerInterface channelManager) {
        this.channelManager = channelManager;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void addSecuredUser(UserFormWithPassword userForm) throws Exception {
        if (userForm != null) {
            if (ProxiedClassUtil.castableAs(userForm, RestrictedUserFormWithPassword.class)) {
                addSecuredRestrictedUser((RestrictedUserFormWithPassword) userForm);
            } else if (ProxiedClassUtil.castableAs(userForm, AdministratorFormWithPassword.class)) {
                addSecuredAdministrator((AdministratorFormWithPassword) userForm);
            }
        }
    }

    /**
     *
     * Validation process for users
     * Incorrect values are replaced with default ones unless the problem is not recoverable, then an Exception is thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean checkUser(User user) throws Exception {
        //No two user can share the same login, casing is irrelevant!
        //No two users can share the same email, casing is irrelevant!

        boolean result = false;

        if (user != null) {

            //Do something useful here
            result = true;

        }
        return result;
    }

    /**
     *
     * Validation process for users
     * Incorrect values are replaced with default ones unless the problem is not recoverable, then an Exception is thrown
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean checkUserEmailAvailability(String newEmail) throws Exception {
        //No two users can share the same email, casing is irrelevant!

        boolean result = false;

        if (newEmail != null) {

            try {
                getUserManager().getUserByCaseInsensitiveEmail(newEmail);
            } catch (NoEntityException e) {
                result = true;
            }
        }
        return result;
    }

    private boolean checkUserLoginAvailability(String newUsername) throws Exception {
        //No two user can share the same login, casing is irrelevant!

        boolean result = false;

        if (newUsername != null) {

            try {
                getUserManager().getUserByCaseInsensitiveUsername(newUsername);
            } catch (NoEntityException e) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Add a user to the database if the authenticated user can access it
     * Check every rights belonging to the user to add and strip them down if the authenticated user cannot access them
     *
     * @param userForm
     * @throws Exception
     **/
    public void addSecuredRestrictedUser(RestrictedUserFormWithPassword userForm) throws Exception {

        if (userForm != null) {

            if (getUserManager().checkAccessAddRestrictedUsers()) {

                if (!checkUserLoginAvailability(userForm.getUsername())) {
                    throw new DuplicatedEntityException(userForm.getUsername());
                }
                if (!checkUserEmailAvailability(userForm.getEmail())) {
                    throw new DuplicatedEntityException(userForm.getEmail());
                }

                //hydrate the entity from the DTO
                RestrictedUser user = userDTOAssembler.fromDTO(userForm);

                //Ratings are not handled here

                if (checkUser(user)) {
                    //Everything's fine, now make it persistent in the database
                    try {

                        getUserManager().addUser(user);
                        //Log it
                        log(USER_LOG_OPERATION.add, user);
                        //Notify responsible users of any changes
                        //All linked entities are then new links
                        for (Channel boundChannel : user.getChannelAdministratorRights()) {
                            notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.add, boundChannel, user);
                        }
                        for (Channel boundChannel : user.getListenerRights()) {
                            notify(CHANNELLISTENER_NOTIFICATION_OPERATION.add, boundChannel, user);
                        }
                        for (Channel boundChannel : user.getProgrammeManagerRights()) {
                            notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.add, boundChannel, user);
                        }
                        for (Programme boundProgramme : user.getAnimatorRights()) {
                            notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.add, boundProgramme, user);
                        }
                        for (Programme boundProgramme : user.getContributorRights()) {
                            notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.add, boundProgramme, user);
                        }
                        for (Programme boundProgramme : user.getCuratorRights()) {
                            notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.add, boundProgramme, user);
                        }

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }
            } else {
                throw new AccessDeniedException();
            }
        }

    }

    public void addSecuredAdministrator(AdministratorFormWithPassword userForm) throws Exception {

        if (userForm != null) {

            if (getUserManager().checkAccessAddAdministrators()) {

                if (!checkUserLoginAvailability(userForm.getUsername())) {
                    throw new DuplicatedEntityException(userForm.getUsername());
                }
                if (!checkUserEmailAvailability(userForm.getEmail())) {
                    throw new DuplicatedEntityException(userForm.getEmail());
                }

                //Hydrate the entity from the DTO
                Administrator user = userDTOAssembler.fromDTO(userForm);

                if (checkUser(user)) {

                    //Everything's fine, now make it persistent in the database
                    try {

                        getUserManager().addUser(user);
                        //Log it
                        log(USER_LOG_OPERATION.add, user);

                    } catch (DataIntegrityViolationException e) {
                        throw new DuplicatedEntityException(e.getClass().getCanonicalName());
                    }
                }

            } else {
                throw new AccessDeniedException();
            }

        }
    }

    /**
     *
     * Pre-process a restricted user for removal
     * Outputs true if this entity must be removed, false for updating it instead
     *
     * @param channelForm
     * @return
     * @throws Exception
     **/
    private boolean processRestrictedUserForDeletion(RestrictedUser restrictedUser) throws Exception {
        //Flag to know whether to update or delete the channel regarding how the checks end up
        boolean mustBeRemoved = true;

        if (restrictedUser != null) {

            //Check every rights included
            //Don't throw an exception if the right cannot be accessed, just remove it from the list

            //for (Programme boundProgramme : restrictedUser.getAnimatorRights()) {
            for (Iterator<Programme> i = restrictedUser.getAnimatorRights().iterator(); i.hasNext();) {
                Programme boundProgramme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(boundProgramme)) {
                    i.remove();
                    boundProgramme.getAnimatorRights().remove(restrictedUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (Programme boundProgramme : restrictedUser.getCuratorRights()) {
            for (Iterator<Programme> i = restrictedUser.getCuratorRights().iterator(); i.hasNext();) {
                Programme boundProgramme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(boundProgramme)) {
                    i.remove();
                    boundProgramme.getCuratorRights().remove(restrictedUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (Programme boundProgramme : restrictedUser.getContributorRights()) {
            for (Iterator<Programme> i = restrictedUser.getContributorRights().iterator(); i.hasNext();) {
                Programme boundProgramme = i.next();
                if (getProgrammeManager().checkAccessUpdateProgramme(boundProgramme)) {
                    i.remove();
                    boundProgramme.getContributorRights().remove(restrictedUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (Channel boundChannel : restrictedUser.getChannelAdministratorRights()) {
            for (Iterator<Channel> i = restrictedUser.getChannelAdministratorRights().iterator(); i.hasNext();) {
                Channel boundChannel = i.next();
                if (getChannelManager().checkAccessUpdateChannel(boundChannel)) {
                    i.remove();
                    boundChannel.getChannelAdministratorRights().remove(restrictedUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (Channel boundChannel : restrictedUser.getProgrammeManagerRights()) {
            for (Iterator<Channel> i = restrictedUser.getProgrammeManagerRights().iterator(); i.hasNext();) {
                Channel boundChannel = i.next();
                if (getChannelManager().checkAccessUpdateChannel(boundChannel)) {
                    i.remove();
                    boundChannel.getChannelAdministratorRights().remove(restrictedUser);
                } else {
                    mustBeRemoved = false;
                }
            }
            //for (Channel boundChannel : restrictedUser.getListenerRights()) {
            for (Iterator<Channel> i = restrictedUser.getListenerRights().iterator(); i.hasNext();) {
                Channel boundChannel = i.next();
                if (getChannelManager().checkAccessUpdateChannel(boundChannel)) {
                    i.remove();
                    boundChannel.getChannelAdministratorRights().remove(restrictedUser);
                } else {
                    mustBeRemoved = false;
                }
            }

            //Whatever happens, someone who can remove a user can unlink its ratings: no need for extra check
            //Cascade delete will take care of their unlinking
            restrictedUser.getVotes().clear();
            //Same applies for the notifications bound to this user
            restrictedUser.getNotifications().clear();
            restrictedUser.getRequests().clear();

        }
        return mustBeRemoved;
    }

    @Override
    public void deleteSecuredUser(String id) throws Exception {
        if (id != null) {
            User user = getUserManager().loadUser(id);
            UserVisitor visitor = new UserVisitor() {

                @Override
                public void visit(RestrictedUser user) throws Exception {
                    deleteSecuredRestrictedUser(user);
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    deleteSecuredAdministrator(user);
                }
            };
            user.acceptVisit(visitor);
        }
    }

    @Override
    public void deleteSecuredUsers(Collection<String> ids) throws Exception {
        if (ids != null) {

            final Collection usersToUpdate = new HashSet<User>();
            final Collection usersToDelete = new HashSet<User>();

            for (String id : ids) {

                //First get a copy of the entity from a DTO
                User user = getUserManager().loadUser(id);

                UserVisitor visitor = new UserVisitor() {

                    @Override
                    public void visit(RestrictedUser restrictedUser) throws Exception {
                        if (getUserManager().checkAccessDeleteRestrictedUser(restrictedUser)) {
                            //Copy all originally bound channels before touching them
                            Collection originallyBoundChannels = USER_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), restrictedUser);
                            Set<Channel> originallyLinkedChannelAdministrators = new HashSet(restrictedUser.getChannelAdministratorRights());
                            Set<Channel> originallyLinkedListeners = new HashSet(restrictedUser.getListenerRights());
                            Set<Channel> originallyLinkedProgrammeManagers = new HashSet(restrictedUser.getProgrammeManagerRights());
                            Set<Programme> originallyLinkedAnimators = new HashSet(restrictedUser.getAnimatorRights());
                            Set<Programme> originallyLinkedContributors = new HashSet(restrictedUser.getContributorRights());
                            Set<Programme> originallyLinkedCurators = new HashSet(restrictedUser.getCuratorRights());
                            //Flag to know whether to update or delete the channel regarding how the checks end up
                            if (processRestrictedUserForDeletion(restrictedUser)) {

                                usersToDelete.add(restrictedUser);
                                //Log it
                                log(USER_LOG_OPERATION.delete, restrictedUser, originallyBoundChannels);
                                //Notify responsible users of any changes
                                //All links have been successfully removed
                                for (Channel boundChannel : originallyLinkedChannelAdministrators) {
                                    UserFacadeService.this.notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                                }
                                for (Channel boundChannel : originallyLinkedListeners) {
                                    UserFacadeService.this.notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                                }
                                for (Channel boundChannel : originallyLinkedProgrammeManagers) {
                                    UserFacadeService.this.notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                                }
                                for (Programme boundProgramme : originallyLinkedAnimators) {
                                    UserFacadeService.this.notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                                }
                                for (Programme boundProgramme : originallyLinkedContributors) {
                                    UserFacadeService.this.notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                                }
                                for (Programme boundProgramme : originallyLinkedCurators) {
                                    UserFacadeService.this.notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                                }

                            } else {

                                usersToUpdate.add(restrictedUser);
                                //Log it
                                log(USER_LOG_OPERATION.edit, restrictedUser, originallyBoundChannels);
                                //Notify responsible users of any changes
                                //Old linked users
                                for (Channel boundChannel : CollectionUtil.exclusion(restrictedUser.getChannelAdministratorRights(), originallyLinkedChannelAdministrators)) {
                                    UserFacadeService.this.notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                                }
                                //Old linked users
                                for (Channel boundChannel : CollectionUtil.exclusion(restrictedUser.getListenerRights(), originallyLinkedListeners)) {
                                    UserFacadeService.this.notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                                }
                                //Old linked users
                                for (Channel boundChannel : CollectionUtil.exclusion(restrictedUser.getProgrammeManagerRights(), originallyLinkedProgrammeManagers)) {
                                    UserFacadeService.this.notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                                }
                                //Old linked users
                                for (Programme boundProgramme : CollectionUtil.exclusion(restrictedUser.getAnimatorRights(), originallyLinkedAnimators)) {
                                    UserFacadeService.this.notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                                }
                                for (Programme boundProgramme : CollectionUtil.exclusion(restrictedUser.getContributorRights(), originallyLinkedContributors)) {
                                    UserFacadeService.this.notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                                }
                                for (Programme boundProgramme : CollectionUtil.exclusion(restrictedUser.getCuratorRights(), originallyLinkedCurators)) {
                                    UserFacadeService.this.notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                                }

                            }
                        } else {
                            throw new AccessDeniedException();
                        }
                    }

                    @Override
                    public void visit(Administrator administrator) throws Exception {
                        if (getUserManager().checkAccessDeleteAdministrator(administrator)) {

                            usersToDelete.add(administrator);
                            //Log it
                            log(USER_LOG_OPERATION.delete, administrator);

                        } else {
                            throw new AccessDeniedException();
                        }
                    }
                };
                user.acceptVisit(visitor);

            }

            //Everything's fine, now make it persistent in the database
            if (!usersToUpdate.isEmpty()) {
                getUserManager().updateUsers(usersToUpdate);
            }
            if (!usersToDelete.isEmpty()) {
                getUserManager().deleteUsers(usersToDelete);
            }

        }
    }

    /**
     *
     * Delete a user from the database if the authenticated user can access it
     * If the authenticated user cannot access all channels or programmes bound to this user then unlink those that can be accessed and do not actually remove the user, but update it
     * Otherwise fully drop the user from database
     *
     * @param userForm
     * @throws Exception
     **/
    public void deleteSecuredRestrictedUser(RestrictedUser restrictedUser) throws Exception {

        if (restrictedUser != null) {

            //Then check if it can be accessed

            if (getUserManager().checkAccessDeleteRestrictedUser(restrictedUser)) {

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = USER_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), restrictedUser);
                Set<Channel> originallyLinkedChannelAdministrators = new HashSet(restrictedUser.getChannelAdministratorRights());
                Set<Channel> originallyLinkedListeners = new HashSet(restrictedUser.getListenerRights());
                Set<Channel> originallyLinkedProgrammeManagers = new HashSet(restrictedUser.getProgrammeManagerRights());
                Set<Programme> originallyLinkedAnimators = new HashSet(restrictedUser.getAnimatorRights());
                Set<Programme> originallyLinkedContributors = new HashSet(restrictedUser.getContributorRights());
                Set<Programme> originallyLinkedCurators = new HashSet(restrictedUser.getCuratorRights());
                //Flag to know whether to update or delete the channel regarding how the checks end up
                if (processRestrictedUserForDeletion(restrictedUser)) {

                    //Everything's fine, now make it persistent in the database
                    getUserManager().deleteUser(restrictedUser);
                    //Log it
                    log(USER_LOG_OPERATION.delete, restrictedUser, originallyBoundChannels);
                    //Notify responsible users of any changes
                    //All links have been successfully removed
                    for (Channel boundChannel : originallyLinkedChannelAdministrators) {
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                    }
                    for (Channel boundChannel : originallyLinkedListeners) {
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                    }
                    for (Channel boundChannel : originallyLinkedProgrammeManagers) {
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                    }
                    for (Programme boundProgramme : originallyLinkedAnimators) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                    }
                    for (Programme boundProgramme : originallyLinkedContributors) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                    }
                    for (Programme boundProgramme : originallyLinkedCurators) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                    }

                } else {

                    getUserManager().updateUser(restrictedUser);
                    //Log it
                    log(USER_LOG_OPERATION.edit, restrictedUser, originallyBoundChannels);
                    //Notify responsible users of any changes
                    //Old linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(restrictedUser.getChannelAdministratorRights(), originallyLinkedChannelAdministrators)) {
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                    }
                    //Old linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(restrictedUser.getListenerRights(), originallyLinkedListeners)) {
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                    }
                    //Old linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(restrictedUser.getProgrammeManagerRights(), originallyLinkedProgrammeManagers)) {
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, boundChannel, restrictedUser);
                    }
                    //Old linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(restrictedUser.getAnimatorRights(), originallyLinkedAnimators)) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                    }
                    for (Programme boundProgramme : CollectionUtil.exclusion(restrictedUser.getContributorRights(), originallyLinkedContributors)) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                    }
                    for (Programme boundProgramme : CollectionUtil.exclusion(restrictedUser.getCuratorRights(), originallyLinkedCurators)) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, boundProgramme, restrictedUser);
                    }

                }

            } else {
                throw new AccessDeniedException();
            }

        }
    }

    public void deleteSecuredAdministrator(Administrator administrator) throws Exception {

        if (administrator != null) {

            //Then check if it can be accessed

            if (getUserManager().checkAccessDeleteAdministrator(administrator)) {

                getUserManager().deleteUser(administrator);
                //Log it
                log(USER_LOG_OPERATION.delete, administrator);

            } else {
                throw new AccessDeniedException();
            }

        }

    }

    @Override
    public void updateSecuredUser(UserForm userForm) throws Exception {
        if (userForm != null) {
            if (ProxiedClassUtil.castableAs(userForm, RestrictedUserForm.class)) {
                updateSecuredRestrictedUser((RestrictedUserForm) userForm);
            } else if (ProxiedClassUtil.castableAs(userForm, AdministratorForm.class)) {
                updateSecuredAdministrator((AdministratorForm) userForm);
            }
        }
    }

    /**
     *
     * Update a user if the authenticated user can access it
     * Do only add or remove rights bound to the user that the authenticated user can acccess
     *
     * @param userForm
     * @throws Exception
     **/
    public void updateSecuredRestrictedUser(RestrictedUserForm userForm) throws Exception {

        if (userForm != null) {

            //Get the original version of this user
            //It will be this version that will be modified with data coming from the DTO
            RestrictedUser originalRestrictedUser = getUserManager().loadRestrictedUser(userForm.getUsername());

            //Check if it can be accessed
            if (getUserManager().checkAccessUpdateRestrictedUser(userForm.getUsername())) {

                if ((userForm.getEmail() != null && !userForm.getEmail().equalsIgnoreCase(originalRestrictedUser.getEmail()))
                        && !checkUserEmailAvailability(userForm.getEmail())) {
                    throw new DuplicatedEntityException(userForm.getEmail());
                }

                //Copy all originally bound channels before touching them
                Collection originallyBoundChannels = USER_LOG_OPERATION.edit.getChannels(getSecurityManager().getSystemConfigurationHelper(), originalRestrictedUser);
                Set<Channel> originallyLinkedChannelAdministrators = new HashSet(originalRestrictedUser.getChannelAdministratorRights());
                Set<Channel> originallyLinkedListeners = new HashSet(originalRestrictedUser.getListenerRights());
                Set<Channel> originallyLinkedProgrammeManagers = new HashSet(originalRestrictedUser.getProgrammeManagerRights());
                Set<Programme> originallyLinkedAnimators = new HashSet(originalRestrictedUser.getAnimatorRights());
                Set<Programme> originallyLinkedContributors = new HashSet(originalRestrictedUser.getContributorRights());
                Set<Programme> originallyLinkedCurators = new HashSet(originalRestrictedUser.getCuratorRights());

                //Update the existing entity from the DTO
                boolean areAttributesUpdated = userDTOAssembler.augmentWithDTO(originalRestrictedUser, userForm);

                //Ratings are not managed here

                //Check and adjust its dependencies before updating
                if (checkUser(originalRestrictedUser)) {

                    //Everything's fine, now make it persistent in the database
                    getUserManager().updateUser(originalRestrictedUser);
                    //Log it
                    log(USER_LOG_OPERATION.edit, originalRestrictedUser, originallyBoundChannels);
                    //Notify responsible users of any changes
                    if (areAttributesUpdated) //Only users still linked to the channel after update will receive this notification
                    {
                        notify(RESTRICTEDUSER_NOTIFICATION_OPERATION.edit, originalRestrictedUser);
                    }
                    //New linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(originallyLinkedChannelAdministrators, originalRestrictedUser.getChannelAdministratorRights())) {
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.add, boundChannel, originalRestrictedUser);
                    }
                    //Old linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(originalRestrictedUser.getChannelAdministratorRights(), originallyLinkedChannelAdministrators)) {
                        notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete, boundChannel, originalRestrictedUser);
                    }
                    //New linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(originallyLinkedListeners, originalRestrictedUser.getListenerRights())) {
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.add, boundChannel, originalRestrictedUser);
                    }
                    //Old linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(originalRestrictedUser.getListenerRights(), originallyLinkedListeners)) {
                        notify(CHANNELLISTENER_NOTIFICATION_OPERATION.delete, boundChannel, originalRestrictedUser);
                    }
                    //New linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(originallyLinkedProgrammeManagers, originalRestrictedUser.getProgrammeManagerRights())) {
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.add, boundChannel, originalRestrictedUser);
                    }
                    //Old linked users
                    for (Channel boundChannel : CollectionUtil.exclusion(originalRestrictedUser.getProgrammeManagerRights(), originallyLinkedProgrammeManagers)) {
                        notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete, boundChannel, originalRestrictedUser);
                    }
                    //New linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedAnimators, originalRestrictedUser.getAnimatorRights())) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.add, boundProgramme, originalRestrictedUser);
                    }
                    //Old linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(originalRestrictedUser.getAnimatorRights(), originallyLinkedAnimators)) {
                        notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete, boundProgramme, originalRestrictedUser);
                    }
                    //New linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedContributors, originalRestrictedUser.getContributorRights())) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.add, boundProgramme, originalRestrictedUser);
                    }
                    //Old linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(originalRestrictedUser.getContributorRights(), originallyLinkedContributors)) {
                        notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete, boundProgramme, originalRestrictedUser);
                    }
                    //New linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(originallyLinkedCurators, originalRestrictedUser.getCuratorRights())) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.add, boundProgramme, originalRestrictedUser);
                    }
                    //Old linked users
                    for (Programme boundProgramme : CollectionUtil.exclusion(originalRestrictedUser.getCuratorRights(), originallyLinkedCurators)) {
                        notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete, boundProgramme, originalRestrictedUser);
                    }

                }

            } else {
                throw new AccessDeniedException();
            }
        }

    }

    public void updateSecuredAdministrator(AdministratorForm userForm) throws Exception {

        if (userForm != null) {

            //Get the original version of this user
            //It will be this version that will be modified with data coming from the DTO
            Administrator originalAdministrator = getUserManager().loadAdministrator(userForm.getUsername());

            //Check if it can be accessed
            if (getUserManager().checkAccessUpdateAdministrator(userForm.getUsername())) {

                if ((userForm.getEmail() != null && !userForm.getEmail().equalsIgnoreCase(originalAdministrator.getEmail()))
                        && !checkUserEmailAvailability(userForm.getEmail())) {
                    throw new DuplicatedEntityException(userForm.getEmail());
                }

                //First get a copy of the entity from a DTO
                userDTOAssembler.augmentWithDTO(originalAdministrator, userForm);

                //Ratings are not managed here

                //Check and adjust its dependencies before updating
                if (checkUser(originalAdministrator)) {

                    //Everything's fine, now make it persistent in the database
                    getUserManager().updateUser(originalAdministrator);
                    //Log it
                    log(USER_LOG_OPERATION.edit, originalAdministrator);

                }

            } else {
                throw new AccessDeniedException();
            }
        }
    }

    @Override
    public ActionCollectionEntry<? extends UserForm> getSecuredUser(String id) throws Exception {

        User user = getUserManager().loadUser(id);

        UserClassVisitor visitor = new UserClassVisitor() {

            private ActionCollectionEntry<? extends UserForm> form;
            @Override
            public ActionCollectionEntry<? extends UserForm> get() {
                return form;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                form = _getSecuredRestrictedUserForm(user);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                form = _getSecuredAdministratorForm(user);
            }
        };
        user.acceptVisit(visitor);
        return visitor.get();
    }

    /**
     *
     * Fetch a user from the database if the authenticated user can access it
     *
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public ActionCollectionEntry<RestrictedUserForm> getSecuredRestrictedUser(String id) throws Exception {
        return _getSecuredRestrictedUserForm(getUserManager().loadRestrictedUser(id));
    }

    private ActionCollectionEntry<RestrictedUserForm> _getSecuredRestrictedUserForm(RestrictedUser restrictedUser) throws Exception {
        ActionCollectionEntry<RestrictedUserForm> userFormEntry = userDTOAssembler.toDTO(restrictedUser);
        if (userFormEntry == null || userFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(USER_LOG_OPERATION.read, restrictedUser);
            return userFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<AdministratorForm> getSecuredAdministrator(String id) throws Exception {
        return _getSecuredAdministratorForm(getUserManager().loadAdministrator(id));
    }

    private ActionCollectionEntry<AdministratorForm> _getSecuredAdministratorForm(Administrator administrator) throws Exception {
        ActionCollectionEntry<AdministratorForm> userFormEntry = userDTOAssembler.toDTO(administrator);
        if (userFormEntry == null || userFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(USER_LOG_OPERATION.read, administrator);
            return userFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<? extends UserFormWithPassword> getSecuredUserWithPassword(String id) throws Exception {

        User user = getUserManager().loadUser(id);

        UserClassVisitor visitor = new UserClassVisitor() {

            private ActionCollectionEntry<? extends UserFormWithPassword> form;
            @Override
            public ActionCollectionEntry<? extends UserFormWithPassword> get() {
                return form;
            }

            @Override
            public void visit(RestrictedUser user) throws Exception {
                form = _getSecuredRestrictedUserFormWithPassword(user);
            }

            @Override
            public void visit(Administrator user) throws Exception {
                form = _getSecuredAdministratorFormWithPassword(user);
            }
        };
        user.acceptVisit(visitor);
        return (ActionCollectionEntry<? extends UserFormWithPassword>) visitor.get();
    }

    /**
     *
     * Fetch a restricted user (w\ its password) from the database if the authenticated user can access it
     *
     * @param id
     * @return
     * @throws Exception
     **/
    @Override
    public ActionCollectionEntry<RestrictedUserFormWithPassword> getSecuredRestrictedUserWithPassword(String id) throws Exception {
        return _getSecuredRestrictedUserFormWithPassword(getUserManager().loadRestrictedUser(id));
    }

    private ActionCollectionEntry<RestrictedUserFormWithPassword> _getSecuredRestrictedUserFormWithPassword(RestrictedUser user) throws Exception {
        ActionCollectionEntry<RestrictedUserFormWithPassword> userFormEntry = userDTOAssembler.toDTOWithPassword(user);
        if (userFormEntry == null || userFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(USER_LOG_OPERATION.read, user);
            return userFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollectionEntry<AdministratorFormWithPassword> getSecuredAdministratorWithPassword(String id) throws Exception {
        return _getSecuredAdministratorFormWithPassword(getUserManager().loadAdministrator(id));
    }

    private ActionCollectionEntry<AdministratorFormWithPassword> _getSecuredAdministratorFormWithPassword(Administrator administrator) throws Exception {
        ActionCollectionEntry<AdministratorFormWithPassword> userFormEntry = userDTOAssembler.toDTOWithPassword(administrator);
        if (userFormEntry == null || userFormEntry.isReadable()) {
            //Log it
            //UPDATE: Logging of 'reading' events has been disabled for performance sake
            //log(USER_LOG_OPERATION.read, administrator);
            return userFormEntry;
        } else {
            throw new AccessDeniedException();
        }
    }

    @Override
    public ActionCollection<AdministratorForm> getSecuredAdministrators(GroupAndSort<USER_TAGS> constraint) throws Exception {
        Collection<Administrator> administrators = getUserManager().getFilteredAdministrators(constraint);
        ActionCollection<AdministratorForm> forms = userDTOAssembler.toAdministratorDTO(administrators, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(USER_LOG_OPERATION.read, administrators);
        return forms;
    }

    @Override
    public ActionCollection<RestrictedUserForm> getSecuredRestrictedUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        Collection<RestrictedUser> restrictedUsers = getUserManager().getFilteredRestrictedUsers(constraint);
        ActionCollection<RestrictedUserForm> forms = userDTOAssembler.toRestrictedUserDTO(restrictedUsers, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(USER_LOG_OPERATION.read, restrictedUsers);
        return forms;
    }

    @Override
    public ActionCollection<? extends UserForm> getSecuredUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        Collection<? extends User> users = getUserManager().getFilteredUsers(constraint);
        ActionCollection<? extends UserForm> forms = userDTOAssembler.toUserDTO(users, constraint==null || constraint.getSortByFeature()==null);
        //Log it
        //UPDATE: Logging of 'reading' events has been disabled for performance sake
        //log(USER_LOG_OPERATION.read, users);
        return forms;
    }

    protected void log(USER_LOG_OPERATION operation, Collection<? extends User> users) throws Exception {
        if (users != null && !users.isEmpty() && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Collection<UserLog> logs = new HashSet<UserLog>();
            for (User user : users) {
                logs.add(new UserLog(operation, user.getUsername(), operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), user)));
            }
            getSecurityManager().addJournals(logs);
        }
    }

    protected void log(USER_LOG_OPERATION operation, User user) throws Exception {
        log(operation, user, null);
    }

    protected void log(USER_LOG_OPERATION operation, User user, Collection<Channel> supplementaryChannels) throws Exception {
        if (user != null && operation.canLog(getSecurityManager().getSystemConfigurationHelper())) {
            Set<Channel> allBoundChannels = operation.getChannels(getSecurityManager().getSystemConfigurationHelper(), user);
            if (supplementaryChannels != null) {
                allBoundChannels.addAll(supplementaryChannels);
            }
            getSecurityManager().addJournal(new UserLog(operation, user.getUsername(), allBoundChannels));
        }
    }

    protected void notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }

    protected void notify(CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelChannelAdministratorLinkNotification> notifications = new HashSet<ChannelChannelAdministratorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == CHANNELCHANNELADMINISTRATOR_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ChannelChannelAdministratorLinkNotification(operation, channel, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNELLISTENER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }

    protected void notify(CHANNELLISTENER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelListenerLinkNotification> notifications = new HashSet<ChannelListenerLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == CHANNELLISTENER_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ChannelListenerLinkNotification(operation, channel, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser) throws Exception {
        notify(operation, channel, restrictedUser, operation.getRecipients(getSecurityManager(), channel, restrictedUser));
    }

    protected void notify(CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION operation, Channel channel, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (channel != null && restrictedUser != null && recipients != null) {
            Collection<ChannelProgrammeManagerLinkNotification> notifications = new HashSet<ChannelProgrammeManagerLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == CHANNELPROGRAMMEMANAGER_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ChannelProgrammeManagerLinkNotification(operation, channel, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser) throws Exception {
        notify(operation, programme, restrictedUser, operation.getRecipients(getSecurityManager(), programme, restrictedUser));
    }

    protected void notify(PROGRAMMEANIMATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && restrictedUser != null && recipients != null) {
            Collection<ProgrammeAnimatorLinkNotification> notifications = new HashSet<ProgrammeAnimatorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == PROGRAMMEANIMATOR_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ProgrammeAnimatorLinkNotification(operation, programme, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser) throws Exception {
        notify(operation, programme, restrictedUser, operation.getRecipients(getSecurityManager(), programme, restrictedUser));
    }

    protected void notify(PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && restrictedUser != null && recipients != null) {
            Collection<ProgrammeContributorLinkNotification> notifications = new HashSet<ProgrammeContributorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == PROGRAMMECONTRIBUTOR_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ProgrammeContributorLinkNotification(operation, programme, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser) throws Exception {
        notify(operation, programme, restrictedUser, operation.getRecipients(getSecurityManager(), programme, restrictedUser));
    }

    protected void notify(PROGRAMMECURATOR_NOTIFICATION_OPERATION operation, Programme programme, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (programme != null && restrictedUser != null && recipients != null) {
            Collection<ProgrammeCuratorLinkNotification> notifications = new HashSet<ProgrammeCuratorLinkNotification>();
            for (RestrictedUser recipient : recipients) {
                //FIX obvious quirk, if the user is meant to be deleted do not notify him, otherwise he will be resaved by cascade
                if (!(operation == PROGRAMMECURATOR_NOTIFICATION_OPERATION.delete && recipient.equals(restrictedUser))) {
                    notifications.add(new ProgrammeCuratorLinkNotification(operation, programme, restrictedUser, recipient));
                }
            }
            getSecurityManager().addNotifications(notifications);
        }
    }

    protected void notify(RESTRICTEDUSER_NOTIFICATION_OPERATION operation, RestrictedUser restrictedUser) throws Exception {
        notify(operation, restrictedUser, operation.getRecipients(getSecurityManager(), restrictedUser));
    }

    protected void notify(RESTRICTEDUSER_NOTIFICATION_OPERATION operation, RestrictedUser restrictedUser, Collection<RestrictedUser> recipients) throws Exception {
        if (restrictedUser != null && recipients != null) {
            Collection<RestrictedUserContentNotification> notifications = new HashSet<RestrictedUserContentNotification>();
            for (RestrictedUser recipient : recipients) {
                notifications.add(new RestrictedUserContentNotification(operation, restrictedUser, recipient));
            }
            getSecurityManager().addNotifications(notifications);
        }
    }
}
