/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.service.user;

import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.DuplicatedEntityException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.dao.user.UserDao;
import biz.ddcr.shampoo.server.domain.GenericTimestampedEntity;
import biz.ddcr.shampoo.server.domain.channel.Channel;
import biz.ddcr.shampoo.server.domain.programme.Programme;
import biz.ddcr.shampoo.server.domain.user.Administrator;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.UserVisitor;
import biz.ddcr.shampoo.server.helper.RightsBundle;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.SecurityManagerInterface;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author okay_awright
 **/
public class UserManager extends GenericService implements UserManagerInterface {

    private interface BooleanUserVisitor extends UserVisitor {

        public boolean get();
    }
    private SecurityManagerInterface securityManager;
    private transient UserDao userDao;

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public SecurityManagerInterface getSecurityManager() {
        return securityManager;
    }

    public void setSecurityManager(SecurityManagerInterface securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public void addUser(User user) throws Exception {
        try {
            //Timestamp the edition
            getSecurityManager().setCreationTimestamp(user);
            userDao.addUser(user);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        }
    }

    @Override
    public void addSelf(RestrictedUser restrictedUser) throws Exception {
        try {
            //Timestamp the edition, but don't provide a creator
            getSecurityManager().setCreationTimestamp(restrictedUser, null);
            userDao.addUser(restrictedUser);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicatedEntityException(e.getClass().getCanonicalName());
        }
    }

    @Override
    public void updateUser(User user) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(user);
        userDao.updateUser(user);
    }

    @Override
    public void updateUsers(Collection<? extends User> users) throws Exception {
        //Timestamp the edition
        getSecurityManager().setEditionTimestamp(users);
        userDao.updateBatchUsers(users);
    }

    @Override
    public Administrator getAdministrator(String id) throws Exception {
        Administrator user = userDao.getAdministrator(id);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public RestrictedUser getRestrictedUser(String id) throws Exception {
        RestrictedUser user = userDao.getRestrictedUser(id);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public User getUser(String id) throws Exception {
        User user = userDao.getUser(id);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public Collection<? extends User> getUsers(Collection<String> ids) throws Exception {
        Collection<? extends User> users = userDao.getUsers(User.class, ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (users == null || ids.size() != users.size()) {
            throw new NoEntityException();
        }
        return users;
    }

    @Override
    public Collection<RestrictedUser> getRestrictedUsers(Collection<String> ids) throws Exception {
        Collection<RestrictedUser> users = (Collection<RestrictedUser>) userDao.getUsers(RestrictedUser.class, ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (users == null || ids.size() != users.size()) {
            throw new NoEntityException();
        }
        return users;
    }

    @Override
    public Collection<RestrictedUser> loadRestrictedUsers(Collection<String> ids) throws Exception {
        Collection<RestrictedUser> users = userDao.loadRestrictedUsers(ids);
        //Since each entry from the collection should be independant (either via the DISTINCT keyword within the SQL or the use of HashSet) the output collection must contain exactly the same nuber of entries as the input, otherwise there's been a loss somewhere
        if (users == null || ids.size() != users.size()) {
            throw new NoEntityException();
        }
        return users;
    }
    
    @Override
    public User loadUser(String id) throws Exception {
        User user = userDao.getUser(id);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public User getUserByCaseInsensitiveEmail(String emailAddress) throws Exception {
        User user = userDao.getUserByCaseInsensitiveEmail(emailAddress);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public User getUserByCaseInsensitiveUsername(String username) throws Exception {
        User user = userDao.getUserByCaseInsensitiveId(username);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public Administrator loadAdministrator(String id) throws Exception {
        Administrator user = userDao.getAdministrator(id);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    @Override
    public RestrictedUser loadRestrictedUser(String id) throws Exception {
        RestrictedUser user = userDao.loadRestrictedUser(id);
        if (user == null) {
            throw new NoEntityException();
        }
        return user;
    }

    public void deleteSecuredUser(User user) throws Exception {
        //Check if the currently authenticated user can access this resource
        UserVisitor visitor = new UserVisitor() {

            @Override
            public void visit(RestrictedUser user) throws Exception {
                if (checkAccessDeleteRestrictedUser(user.getUsername())) {
                deleteUser(user);
            } else {
                throw new AccessDeniedException();
            }
            }

            @Override
            public void visit(Administrator user) throws Exception {
                if (checkAccessDeleteAdministrator(user.getUsername())) {
                deleteUser(user);
            } else {
                throw new AccessDeniedException();
            }
            }
        };
        user.acceptVisit(visitor);

    }

    private void _unsetCreator(Collection<? extends GenericTimestampedEntity> entities) {
        if (entities != null) {
            for (GenericTimestampedEntity x : entities) {
                x.setCreator(null);
            }
            entities.clear();
        }
    }

    private void _unsetLatestEditor(Collection<? extends GenericTimestampedEntity> entities) {
        if (entities != null) {
            for (GenericTimestampedEntity x : entities) {
                x.setLatestEditor(null);
            }
            entities.clear();
        }
    }

    protected void unsetOwnership(User user) {
        _unsetCreator(user.getCreatedArchives());
        _unsetCreator(user.getCreatedChannels());
        _unsetCreator(user.getCreatedPlaylists());
        _unsetCreator(user.getCreatedProgrammes());
        _unsetCreator(user.getCreatedRequests());
        _unsetCreator(user.getCreatedTimetableSlots());
        _unsetCreator(user.getCreatedTracks());
        _unsetCreator(user.getCreatedUsers());
        _unsetCreator(user.getCreatedVotes());
        _unsetCreator(user.getCreatedWebservices());
        _unsetLatestEditor(user.getEditedArchives());
        _unsetLatestEditor(user.getEditedChannels());
        _unsetLatestEditor(user.getEditedPlaylists());
        _unsetLatestEditor(user.getEditedProgrammes());
        _unsetLatestEditor(user.getEditedRequests());
        _unsetLatestEditor(user.getEditedTimetableSlots());
        _unsetLatestEditor(user.getEditedTracks());
        _unsetLatestEditor(user.getEditedUsers());
        _unsetLatestEditor(user.getEditedVotes());
        _unsetLatestEditor(user.getEditedWebservices());
    }

    @Override
    public void deleteUser(User user) throws Exception {
        //remove all traces of 'creator' and 'latesteditor'
        unsetOwnership(user);
        userDao.deleteUser(user);
        //Deletion of Rights for Users are handled via Hibernate cascade
    }

    @Override
    public void deleteUsers(Collection<? extends User> users) throws Exception {
        //remove all traces of 'creator' and 'latesteditor'
        if (users != null) {
            for (User user : users) {
                unsetOwnership(user);
            }
        }
        userDao.deleteBatchUsers(users);
    }

    @Override
    public void deleteUser(String id) throws Exception {
        User user = getUser(id);
        deleteUser(user);
    }

    public void deleteSecuredUser(String id) throws Exception {
        User user = getUser(id);
        deleteSecuredUser(user);
    }

    @Override
    public boolean checkAccessViewUsers() throws Exception {
        return (checkAccessViewRestrictedUsers() | checkAccessViewAdministrators());
    }

    @Override
    public boolean checkAccessViewRestrictedUsers() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = RestrictedUser.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessViewAdministrators() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = Administrator.getViewRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    protected boolean checkAccessRestrictedUserCreator(RestrictedUser restrictedUserToCheck, RightsBundle rightsBundle) throws Exception {
        if (restrictedUserToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //Check first if the authenticated user has the minimal rights to touch the entity
                if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle)) //Then see if:
                //-no right is bound to the entity
                //-and either he's its creator or if it has none
                {
                    return (restrictedUserToCheck != null
                            && !restrictedUserToCheck.isAnimator()
                            && !restrictedUserToCheck.isContributor()
                            && !restrictedUserToCheck.isCurator()
                            && !restrictedUserToCheck.isListener()
                            && !restrictedUserToCheck.isProgrammeManager()
                            && !restrictedUserToCheck.isChannelAdministrator()
                            && (restrictedUserToCheck.getLatestEditor() == null
                            || getSecurityManager().isCurrentlyAuthenticatedUser(restrictedUserToCheck.getLatestEditor())));
                }
            }
        }
        return false;
    }

    protected boolean checkAccessRestrictedUserChannelsOrProgrammes(RestrictedUser userToCheck, RightsBundle rightsBundle) throws Exception {
        if (userToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //Check if any programmes or channels are found in common
                //First gather all first and second-level channels and programmes bound to the user to check
                Set<Channel> c = userToCheck.getImmutableChannels();
                Set<Programme> p = userToCheck.getImmutableProgrammes();
                Collection<Channel> c1 = Programme.getChannelsFromProgrammes(p);
                Collection<Programme> p1 = Channel.getProgrammesFromChannels(c);
                c.addAll(c1);
                p.addAll(p1);
                if ((c != null && p != null) && (!c.isEmpty() || !p.isEmpty())) {
                    //And look for matches with channels and programmes from the authenticated user
                    c.retainAll(getSecurityManager().getLinkedChannelsForCurrentlyAuthenticatedRestrictedUser(rightsBundle));
                    if (!c.isEmpty()) {
                        return true;
                    }
                    p.retainAll(getSecurityManager().getLinkedProgrammesForCurrentlyAuthenticatedRestrictedUser(rightsBundle));
                    if (!p.isEmpty()) {
                        return true;
                    }
                } else {
                    //no programme or channel is bound to the entity
                    //Check first if the authenticated user has the minimal rights to touch the entity
                    if (getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle)) {
                        //see if he's either its creator, or the latest editor, or if it has none
                        if ((userToCheck.getCreator() == null && userToCheck.getLatestEditor() == null)
                                || (userToCheck.getCreator() != null && getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck.getCreator()))
                                || (userToCheck.getLatestEditor() != null && getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck.getLatestEditor()))) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewUser(String userToCheckId) throws Exception {
        return checkAccessViewUser(getUser(userToCheckId));
    }

    @Override
    public boolean checkAccessViewUser(User userToCheck) throws Exception {
        if (userToCheck != null) {
            BooleanUserVisitor visitor = new BooleanUserVisitor() {

                private boolean result;

                @Override
                public boolean get() {
                    return result;
                }

                @Override
                public void visit(RestrictedUser user) throws Exception {
                    result = checkAccessViewRestrictedUser(user);
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    result = checkAccessViewAdministrator(user);
                }
            };
            userToCheck.acceptVisit(visitor);
            return visitor.get();
        }
        return false;
    }

    @Override
    public boolean checkAccessViewRestrictedUser(String userToCheckId) throws Exception {
        return checkAccessViewRestrictedUser(getRestrictedUser(userToCheckId));
    }

    @Override
    public boolean checkAccessViewRestrictedUser(RestrictedUser userToCheck) throws Exception {
        if (userToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //A user can always view itself
                if (getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck)) {
                    return true;
                }

                RightsBundle bundle = RestrictedUser.getViewRights();

                return checkAccessRestrictedUserChannelsOrProgrammes(userToCheck, bundle);
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessViewAdministrator(String userToCheckId) throws Exception {
        return checkAccessViewAdministrator(getAdministrator(userToCheckId));
    }

    @Override
    public boolean checkAccessViewAdministrator(Administrator userToCheck) throws Exception {
        if (userToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //At least one request must return true to grant access
                RightsBundle bundle = Administrator.getViewRights();

                return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessAddUsers() throws Exception {
        return (checkAccessAddRestrictedUsers() | checkAccessAddAdministrators());
    }

    @Override
    public boolean checkAccessAddRestrictedUsers() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = RestrictedUser.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessAddAdministrators() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = Administrator.getAddRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    /**
     * Special rights a bit broader than a simple View but less than Edit
     * Used by Programme Managers for linking programmes to restricted users
     *
     * @param programmeToCheckId
     * @return
     * @throws Exception
     **/
    @Override
    public boolean checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(String userIdToCheck) throws Exception {
        return checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(getRestrictedUser(userIdToCheck));
    }

    @Override
    public boolean checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(RestrictedUser userToCheck) throws Exception {
        if (userToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //A user cannot unlink/relink itself
                //FIX: unless he's a Channel Administrator, otherwise we're trap in a catch 22 situation where only a Super Administrator can do that if a Channle Administrator has been linked with other credentials to this programme
                if (getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck)) {
                    RightsBundle rightsBundle = Programme.getUpdateRights();
                    return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(rightsBundle);
                }
                //If there are no bound programmes to the user to check
                //Then it can be viewed only if the authenticated user created it
                RightsBundle bundle = RestrictedUser.getLimitedProgrammeBindingRights();

                return checkAccessRestrictedUserChannelsOrProgrammes(userToCheck, bundle);

            }
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateUsers() throws Exception {
        return (checkAccessUpdateRestrictedUsers() | checkAccessUpdateAdministrators());
    }

    @Override
    public boolean checkAccessUpdateRestrictedUsers() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = RestrictedUser.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessUpdateAdministrators() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = Administrator.getUpdateRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessUpdateUser(String userToCheckId) throws Exception {
        return checkAccessUpdateUser(getUser(userToCheckId));
    }

    @Override
    public boolean checkAccessUpdateUser(User userToCheck) throws Exception {
        if (userToCheck != null) {
            BooleanUserVisitor visitor = new BooleanUserVisitor() {

                private boolean result;

                @Override
                public boolean get() {
                    return result;
                }

                @Override
                public void visit(RestrictedUser user) throws Exception {
                    result = checkAccessUpdateRestrictedUser(user);
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    result = checkAccessUpdateAdministrator(user);
                }
            };
            userToCheck.acceptVisit(visitor);
            return visitor.get();
        }
        return false;
    }

    @Override
    public boolean checkAccessUpdateRestrictedUser(String userToCheckId) throws Exception {
        return checkAccessUpdateRestrictedUser(getRestrictedUser(userToCheckId));
    }

    @Override
    public boolean checkAccessUpdateRestrictedUser(RestrictedUser userToCheck) throws Exception {
        if (userToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //A user cannot update itself
                if (getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck)) {
                    return false;
                }

                RightsBundle bundle = RestrictedUser.getUpdateRights();

                return checkAccessRestrictedUserChannelsOrProgrammes(userToCheck, bundle);
            }
        }
        return false;

    }

    @Override
    public boolean checkAccessUpdateAdministrator(String userToCheckId) throws Exception {
        return checkAccessUpdateAdministrator(getAdministrator(userToCheckId));
    }

    @Override
    public boolean checkAccessUpdateAdministrator(Administrator userToCheck) throws Exception {
        //A user cannot update itself
        if (getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck)) {
            return false;
        } else {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //only admins can do that
                return false;
            }
        }
    }

    @Override
    public boolean checkAccessDeleteUsers() throws Exception {
        return (checkAccessDeleteRestrictedUsers() | checkAccessDeleteAdministrators());
    }

    @Override
    public boolean checkAccessDeleteRestrictedUsers() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = RestrictedUser.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessDeleteAdministrators() throws Exception {
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            //An administrator can do about everything: don't go any further, allow everything
            return true;
        } else {
            //At least one request must return true to grant access
            RightsBundle bundle = Administrator.getDeleteRights();

            return getSecurityManager().hasCurrentlyAuthenticatedRestrictedUserRights(bundle);
        }
    }

    @Override
    public boolean checkAccessDeleteUser(String userToCheckId) throws Exception {
        return checkAccessDeleteUser(getUser(userToCheckId));
    }

    @Override
    public boolean checkAccessDeleteRestrictedUser(String userToCheckId) throws Exception {
        return checkAccessDeleteRestrictedUser(getRestrictedUser(userToCheckId));
    }

    @Override
    public boolean checkAccessDeleteUser(User userToCheck) throws Exception {
        if (userToCheck != null) {
            BooleanUserVisitor visitor = new BooleanUserVisitor() {

                private boolean result;

                @Override
                public boolean get() {
                    return result;
                }

                @Override
                public void visit(RestrictedUser user) throws Exception {
                    result = checkAccessDeleteRestrictedUser(user);
                }

                @Override
                public void visit(Administrator user) throws Exception {
                    result = checkAccessDeleteAdministrator(user);
                }
            };
            userToCheck.acceptVisit(visitor);
            return visitor.get();
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteRestrictedUser(RestrictedUser userToCheck) throws Exception {
        if (userToCheck != null) {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //A user cannot update itself
                if (getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck)) {
                    return false;
                }

                RightsBundle bundle = RestrictedUser.getDeleteRights();

                return checkAccessRestrictedUserChannelsOrProgrammes(userToCheck, bundle);
            }
        }
        return false;
    }

    @Override
    public boolean checkAccessDeleteAdministrator(String userToCheckId) throws Exception {
        return checkAccessDeleteAdministrator(getAdministrator(userToCheckId));
    }

    @Override
    public boolean checkAccessDeleteAdministrator(Administrator userToCheck) throws Exception {
        //A user cannot update itself
        if (getSecurityManager().isCurrentlyAuthenticatedUser(userToCheck)) {
            return false;
        } else {
            if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
                //An administrator can do about everything: don't go any further, allow everything
                return true;
            } else {
                //only admins can do that
                return false;
            }
        }
    }

    @Override
    public Collection<Administrator> getFilteredAdministrators(GroupAndSort<USER_TAGS> constraint) throws Exception {
        Collection<Administrator> admins;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            admins = userDao.getAllAdmininistrators(constraint);
        } else {
            //Only admins can see, update and delete admins
            admins = new HashSet<Administrator>();
        }
        return admins;
    }

    @Override
    public Collection<RestrictedUser> getFilteredRestrictedUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        Collection<RestrictedUser> users;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            users = userDao.getAllRestrictedUsers(constraint);
        } else {

            RightsBundle rightsBundle = RestrictedUser.getViewRights();

            users = userDao.getAllLinkedRestrictedUsers(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername(),
                    constraint);
        }
        return users;
    }

    /*public Collection<RestrictedUser> getFilteredUnlinkedRestrictedUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
    if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
    return userDao.getAllRestrictedUsersWithNoRight(constraint);
    } else {
    return userDao.getAllRestrictedUsersWithNoRightForCreator(
    getSecurityManager().getCurrentlyAuthenticatedUserLogin(),
    constraint);
    }
    }*/
    @Override
    public Collection<? extends User> getFilteredUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        Collection<? extends User> users;
        if (getSecurityManager().isCurrentlyAuthenticatedUserAnAdministrator()) {
            users = userDao.getAllUsers(constraint);
        } else {

            RightsBundle rightsBundle = RestrictedUser.getViewRights();

            users = userDao.getAllLinkedRestrictedUsers(
                    getSecurityManager().getLinkedChannelIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    getSecurityManager().getLinkedProgrammeIdsForCurrentlyAuthenticatedRestrictedUser(rightsBundle),
                    getSecurityManager().getCurrentlyAuthenticatedUser().getUsername(),
                    constraint);
        }
        return users;
    }
}
