/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.user;

import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.server.domain.user.RestrictedUser;
import biz.ddcr.shampoo.server.domain.user.User;
import biz.ddcr.shampoo.server.domain.user.Administrator;

import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public interface UserManagerInterface {

    //Check access to ressources
    public boolean checkAccessViewUsers() throws Exception;
    public boolean checkAccessViewRestrictedUsers() throws Exception;
    public boolean checkAccessViewAdministrators() throws Exception;
    public boolean checkAccessViewUser(String userToCheckId) throws Exception;
    public boolean checkAccessViewUser(User userToCheck) throws Exception;
    public boolean checkAccessViewRestrictedUser(String userToCheckId) throws Exception;
    public boolean checkAccessViewRestrictedUser(RestrictedUser userToCheck) throws Exception;
    public boolean checkAccessViewAdministrator(String userToCheckId) throws Exception;
    public boolean checkAccessViewAdministrator(Administrator userToCheck) throws Exception;
    public boolean checkAccessAddUsers() throws Exception;
    public boolean checkAccessAddRestrictedUsers() throws Exception;
    public boolean checkAccessAddAdministrators() throws Exception;
    public boolean checkAccessUpdateUsers() throws Exception;
    public boolean checkAccessUpdateRestrictedUsers() throws Exception;
    public boolean checkAccessUpdateAdministrators() throws Exception;
    public boolean checkAccessUpdateUser(String userToCheckId) throws Exception;
    public boolean checkAccessUpdateUser(User userToCheck) throws Exception;
    public boolean checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(String userToCheckId) throws Exception;
    public boolean checkAccessLimitedProgrammeDependencyUpdateRestrictedUser(RestrictedUser userToCheck) throws Exception;
    public boolean checkAccessUpdateRestrictedUser(String userToCheckId) throws Exception;
    public boolean checkAccessUpdateRestrictedUser(RestrictedUser userToCheck) throws Exception;
    public boolean checkAccessUpdateAdministrator(String userToCheckId) throws Exception;
    public boolean checkAccessUpdateAdministrator(Administrator userToCheck) throws Exception;
    public boolean checkAccessDeleteUsers() throws Exception;
    public boolean checkAccessDeleteAdministrators() throws Exception;
    public boolean checkAccessDeleteRestrictedUsers() throws Exception;
    public boolean checkAccessDeleteUser(String userToCheckId) throws Exception;
    public boolean checkAccessDeleteUser(User userToCheck) throws Exception;
    public boolean checkAccessDeleteRestrictedUser(String userToCheckId) throws Exception;
    public boolean checkAccessDeleteRestrictedUser(RestrictedUser userToCheck) throws Exception;
    public boolean checkAccessDeleteAdministrator(String userToCheckId) throws Exception;
    public boolean checkAccessDeleteAdministrator(Administrator userToCheck) throws Exception;

    //Editing objects
    public void addUser(User user) throws Exception;
    public void addSelf(RestrictedUser restrictedUser) throws Exception;

    public void updateUser(User user) throws Exception;
    public void updateUsers(Collection<? extends User> users) throws Exception;

    public void deleteUser(User user) throws Exception;
    public void deleteUsers(Collection<? extends User> users) throws Exception;
    public void deleteUser(String id) throws Exception;

    //Fetching objects
    public User loadUser(String id) throws Exception;
    public RestrictedUser loadRestrictedUser(String id) throws Exception;
    public Administrator loadAdministrator(String id) throws Exception;
    public User getUser(String id) throws Exception;
    public Collection<? extends User> getUsers(Collection<String> ids) throws Exception;
    public RestrictedUser getRestrictedUser(String id) throws Exception;
    public Collection<RestrictedUser> getRestrictedUsers(Collection<String> ids) throws Exception;
    public Collection<RestrictedUser> loadRestrictedUsers(Collection<String> ids) throws Exception;
    public Administrator getAdministrator(String id) throws Exception;

    public User getUserByCaseInsensitiveEmail(String emailAddress) throws Exception;
    public User getUserByCaseInsensitiveUsername(String username) throws Exception;

    //Fetching objects with associated access policy flags
    public Collection<Administrator> getFilteredAdministrators(GroupAndSort<USER_TAGS> constraint) throws Exception;
    public Collection<RestrictedUser> getFilteredRestrictedUsers(GroupAndSort<USER_TAGS> constraint) throws Exception;
    public Collection<? extends User> getFilteredUsers(GroupAndSort<USER_TAGS> constraint) throws Exception;

}
