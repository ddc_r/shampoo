/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU Affero General Public License as published
  *  by the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU Affero General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.service.user;

import biz.ddcr.shampoo.client.form.user.AdministratorForm;
import biz.ddcr.shampoo.client.form.user.AdministratorFormWithPassword;
import biz.ddcr.shampoo.client.form.user.RestrictedUserForm;
import biz.ddcr.shampoo.client.form.user.RestrictedUserFormWithPassword;
import biz.ddcr.shampoo.client.form.user.UserForm;
import biz.ddcr.shampoo.client.form.user.UserForm.USER_TAGS;
import biz.ddcr.shampoo.client.form.user.UserFormWithPassword;
import biz.ddcr.shampoo.client.helper.view.ActionCollection;
import biz.ddcr.shampoo.client.helper.view.ActionCollectionEntry;
import biz.ddcr.shampoo.client.helper.view.GroupAndSort;
import biz.ddcr.shampoo.client.serviceAsync.user.UserRPCServiceInterface;
import biz.ddcr.shampoo.server.helper.gwt.GWTRPCLocalThreadService;
import java.util.Collection;

/**
 *
 * @author okay_awright
 **/
public class UserRPCService extends GWTRPCLocalThreadService implements UserRPCServiceInterface {

    private UserFacadeServiceInterface userFacadeService;

    public UserFacadeServiceInterface getUserFacadeService() {
        return userFacadeService;
    }

    public void setUserFacadeService(UserFacadeServiceInterface userFacadeService) {
        this.userFacadeService = userFacadeService;
    }

    @Override
    public void addSecuredUser(UserFormWithPassword userForm) throws Exception {
        userFacadeService.addSecuredUser(userForm);
    }

    @Override
    public void updateSecuredUser(UserForm userForm) throws Exception {
        userFacadeService.updateSecuredUser(userForm);
    }

    @Override
    public void deleteSecuredUser(String userId) throws Exception {
        userFacadeService.deleteSecuredUser(userId);
    }
    @Override
    public void deleteSecuredUsers(Collection<String> userIds) throws Exception {
        userFacadeService.deleteSecuredUsers(userIds);
    }

    @Override
    public ActionCollectionEntry<? extends UserForm> getSecuredUser(String id) throws Exception {
        return userFacadeService.getSecuredUser(id);
    }

    @Override
    public ActionCollectionEntry<RestrictedUserForm> getSecuredRestrictedUser(String id) throws Exception {
        return userFacadeService.getSecuredRestrictedUser(id);
    }

    @Override
    public ActionCollectionEntry<AdministratorForm> getSecuredAdministrator(String id) throws Exception {
        return userFacadeService.getSecuredAdministrator(id);
    }

    @Override
    public ActionCollectionEntry<? extends UserFormWithPassword> getSecuredUserWithPassword(String id) throws Exception {
        return userFacadeService.getSecuredUserWithPassword(id);
    }

    @Override
    public ActionCollectionEntry<RestrictedUserFormWithPassword> getSecuredRestrictedUserWithPassword(String id) throws Exception {
        return userFacadeService.getSecuredRestrictedUserWithPassword(id);
    }

    @Override
    public ActionCollectionEntry<AdministratorFormWithPassword> getSecuredAdministratorWithPassword(String id) throws Exception {
        return userFacadeService.getSecuredAdministratorWithPassword(id);
    }

    @Override
    public ActionCollection<AdministratorForm> getSecuredAdministrators(GroupAndSort<USER_TAGS> constraint) throws Exception {
        return userFacadeService.getSecuredAdministrators(constraint);
    }

    @Override
    public ActionCollection<RestrictedUserForm> getSecuredRestrictedUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        return userFacadeService.getSecuredRestrictedUsers(constraint);
    }

    @Override
    public ActionCollection<? extends UserForm> getSecuredUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        return userFacadeService.getSecuredUsers(constraint);
    }

    /*public ActionCollection<RestrictedUserForm> getSecuredUnlinkedRestrictedUsers(GroupAndSort<USER_TAGS> constraint) throws Exception {
        return userFacadeService.getSecuredUnlinkedRestrictedUsers(constraint);
    }

    public ActionCollection<RestrictedUserForm> getSecuredRestrictedUsersIncludingUnlinked() throws Exception {
        return userFacadeService.getSecuredRestrictedUsersIncludingUnlinked();
    }*/
}
