/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public enum ParameterMapping {

    METADATA_FORMAT("f", null),
    ITEM_ID("i", null),
    API_KEY("a", null),
    API_HMAC("h", "X-Shampoo-API-HMAC"),
    TIMESTAMP("t", "X-Shampoo-Timestamp"),
    CHANNEL("c", null),
    MASTER_KEY("k", null),
    PROTECTION_KEY("p", null),
    TIMETABLE_SLOT("s", null),
    QUEUE_MIN_ITEMS("mq", null),
    MIN_PLAYABLE_DURATION("md", null),
    QUEUEABLE_LIVE("ql", null),
    QUEUEABLE_LIVE_CHUNK_SIZE("qlz", null),
    QUEUEABLE_LIVE_CHUNK_SIZE_S("qlzf", null),
    QUEUEABLE_LIVE_URI("qlu", null),
    QUEUEABLE_BLANK("qb", null),
    QUEUEABLE_BLANK_CHUNK_SIZE("qbz", null),
    QUEUEABLE_BLANK_CHUNK_SIZE_S("qbzf", null),
    QUEUEABLE_BLANK_URI("qbu", null),
    STREAM_URI("u", null),
    LOGIN("g", null),
    LOGIN_HMAC("hg", "X-Shampoo-Login-HMAC"),
    STREAMER_UA("ua", null),
    SEAT_NUMBER("n", null),
    TTL("l", null),
    AS_ATTACHMENT("j", null),
    MAXIMUM_ITEM_NUMBER("x", null);
    
    private final String queryParameter;
    private final String headerParameter;

    private ParameterMapping(final String queryParameter, final String headerParameter) {
        this.queryParameter = queryParameter;
        this.headerParameter = headerParameter;
    }

    public String getQueryParameter() {
        return queryParameter;
    }

    public String getHeaderParameter() {
        return headerParameter;
    }
    
    public String find(final HttpServletRequest request) {
        String result = null;
        if (request!=null) {
            if (getQueryParameter()!=null)
                result = request.getParameter(getQueryParameter());
            if (result==null && getHeaderParameter()!=null)
                result = request.getHeader(getHeaderParameter());
        }
        return result;
    }
    
    /**
     * Detect whether the given timestamp representation is second-based or millisecond-based and make it a 64bit integer millisecond-based timestamp
     * Returns null if the timestamp representation is empty
     * @param timestamp
     * @return 
     */
    public static Long convertIntoMillisecondTimestamp(final String timestampText) throws NumberFormatException {
        if (timestampText != null && !timestampText.isEmpty()) {
            if (timestampText.contains(".")) {
                //Must be second-based
                //parseDouble() doesn't use locale for parsing, apparently
                return (long) (Double.parseDouble(timestampText) * 1000.0);
            }
            //Already millisecond-based
            return Long.decode(timestampText);
        }
        return null;
    }
    
}
