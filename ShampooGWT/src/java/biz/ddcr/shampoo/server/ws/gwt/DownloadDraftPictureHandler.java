/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.io.frontend.DownloadFileHandler;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.security.LoginFacadeServiceInterface;
import biz.ddcr.shampoo.server.service.track.TrackFacadeServiceInterface;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright
 **/
public class DownloadDraftPictureHandler extends DownloadFileHandler {

    private LoginFacadeServiceInterface loginFacadeService;
    private TrackFacadeServiceInterface trackFacadeService;

    public LoginFacadeServiceInterface getLoginFacadeService() {
        return loginFacadeService;
    }

    public void setLoginFacadeService(LoginFacadeServiceInterface loginFacadeService) {
        this.loginFacadeService = loginFacadeService;
    }

    public TrackFacadeServiceInterface getTrackFacadeService() {
        return trackFacadeService;
    }

    public void setTrackFacadeService(TrackFacadeServiceInterface trackFacadeService) {
        this.trackFacadeService = trackFacadeService;
    }

    @Override
    public boolean onCheckFile(TypedStreamInterface file) throws Exception {
        //Do nothing
        return true;
    }

    @Override
    public void preProcessFile(TypedStreamInterface file) throws Exception {
        //Do nothing
    }

    @Override
    public PictureStream getStream(String fileId) throws Exception {
        //Retrieve the handle to the temporary file thorugh the session using the specified upload tracking ID
        //And make sure this file really exists
        return getTrackFacadeService().getSecuredPictureStreamFromSessionPool(fileId);
    }

    @Override
    public boolean beforeAccess(String fileId, String remoteIP, HttpServletRequest request) throws Exception {
        //Don't go any further if no identifier has been specified
        if (fileId==null || fileId.length()==0)
            throw new BadRequestException();
        //We cannot ensure that the correct rights are enforced (too little available info to process) so just stick to the minimum for upload, i.e. authenticated user or not
        if (getLoginFacadeService().getCurrentlyAuthenticatedUserLogin() == null) {
            throw new AccessDeniedException();
        }
        return true;
    }

}
