/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionMalformedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionTimedOutException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingCacheUnchangedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingNotFoundException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingPreconditionFailedException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingWrongRangeForResumeException;
import biz.ddcr.shampoo.client.helper.errors.NoEntityException;
import biz.ddcr.shampoo.client.helper.errors.SessionClosedException;
import biz.ddcr.shampoo.client.helper.errors.WebserviceOutOfQuotaException;
import biz.ddcr.shampoo.server.helper.SimpleHTTPController;
import biz.ddcr.shampoo.server.io.frontend.DownloadFileHandlerInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright
 **/
public class FrontendDownMediaController extends SimpleHTTPController {

    private DownloadFileHandlerInterface downloadFileHandler;

    public DownloadFileHandlerInterface getDownloadFileHandler() {
        return downloadFileHandler;
    }

    public void setDownloadFileHandler(DownloadFileHandlerInterface downloadFileHandler) {
        this.downloadFileHandler = downloadFileHandler;
    }
    
    @Override
    public int delegatedHandleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Only get is supported
        //it's then the responsability of the downloadFileHandler to check if it's really a file upload or not
        if (request.getMethod().equalsIgnoreCase("GET")) {
            try {

                //Nope, values are not identifiers, keys only. Why? maybe because it's shorter and there will ever only be one GET parameter anyway
                //UPDATE: now use proper "i" parameter        
                //String fileId = (String) request.getParameterNames().nextElement();
                String fileId = ParameterMapping.ITEM_ID.find(request);
                return downloadFileHandler.processFileDownloadRequest(fileId, request, response)
                        ? HttpServletResponse.SC_OK
                        : HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            } catch (ConnectionTimedOutException e) {
                logger.warn("processFileDownloadRequest(): timed out");
                return HttpServletResponse.SC_REQUEST_TIMEOUT;
            } catch (DownloadingCacheUnchangedException e) {
                logger.info("processFileDownloadRequest(): not modified");
                return HttpServletResponse.SC_NOT_MODIFIED;
            } catch (NoEntityException e) {
                logger.warn("processRestQuery(): not found");
                return HttpServletResponse.SC_NOT_FOUND;
            } catch (DownloadingNotFoundException e) {
                logger.warn("processFileDownloadRequest(): not found");
                return HttpServletResponse.SC_NOT_FOUND;
            } catch (DownloadingPreconditionFailedException e) {
                logger.warn("processFileDownloadRequest(): precondition failed");
                return HttpServletResponse.SC_PRECONDITION_FAILED;
            } catch (DownloadingWrongRangeForResumeException e) {
                logger.warn("processFileDownloadRequest(): resume failed, wrong range");
                return HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE;
            } catch (ConnectionMalformedException e) {
                logger.warn("processFileDownloadRequest(): malformed");
                return HttpServletResponse.SC_CONFLICT;
            } catch (SessionClosedException e) {
                logger.warn("processFileDownloadRequest(): session closed");
                return HttpServletResponse.SC_UNAUTHORIZED;
            } catch (AccessDeniedException e) {
                logger.warn("processFileDownloadRequest(): access denied");
                return HttpServletResponse.SC_FORBIDDEN;
            /*} catch (ClientAbortException e) {
                logger.warn("processFileDownloadRequest(): client aborted");
                return HttpServletResponse.SC_ACCEPTED;*/
            } catch (BadRequestException e) {
                logger.warn("processRestQuery(): bad request");
                return HttpServletResponse.SC_BAD_REQUEST;
            } catch (WebserviceOutOfQuotaException e) {
                logger.warn("processRestQuery(): ws out of quota");
                return 429; //Too Many Requests (RFC 6585)
            } catch (Exception e) {
                logger.warn("processFileDownloadRequest(): unknown error", e);
                return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            }
        }
        return HttpServletResponse.SC_METHOD_NOT_ALLOWED;
    }
}
