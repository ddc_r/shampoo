/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionMalformedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionTimedOutException;
import biz.ddcr.shampoo.client.helper.errors.SessionClosedException;
import biz.ddcr.shampoo.client.helper.errors.TrackBitrateTooHighException;
import biz.ddcr.shampoo.client.helper.errors.TrackBitrateTooLowException;
import biz.ddcr.shampoo.client.helper.errors.TrackBrokenException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotCBRException;
import biz.ddcr.shampoo.client.helper.errors.TrackNotStereoException;
import biz.ddcr.shampoo.client.helper.errors.TrackSamplerateTooHighException;
import biz.ddcr.shampoo.client.helper.errors.TrackSamplerateTooLowException;
import biz.ddcr.shampoo.client.helper.errors.TrackTooLongException;
import biz.ddcr.shampoo.client.helper.errors.TrackTooShortException;
import biz.ddcr.shampoo.client.helper.errors.UploadingCanceledException;
import biz.ddcr.shampoo.client.helper.errors.UploadingNoDataException;
import biz.ddcr.shampoo.client.helper.errors.UploadingOversizedException;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFeaturesException;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import biz.ddcr.shampoo.client.helper.errors.WebserviceOutOfQuotaException;
import biz.ddcr.shampoo.server.helper.SimpleHTTPController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright
 *
 */
public class FrontendUpMediaController extends SimpleHTTPController {

    private UploadFileHandlerInterface uploadFileHandler;

    public UploadFileHandlerInterface getUploadFileHandler() {
        return uploadFileHandler;
    }

    public void setUploadFileHandler(UploadFileHandlerInterface uploadFileHandler) {
        this.uploadFileHandler = uploadFileHandler;
    }
    
    @Override
    public int delegatedHandleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //Only post is supportted
        //it's then the responsability of the upFileUploadSessionPoolManager to check if it's really a file upload or not
        if (request.getMethod().equalsIgnoreCase("POST")) {
            try {
                return uploadFileHandler.processFileUploadRequest(request)
                        ? HttpServletResponse.SC_CREATED
                        : HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            } catch (UploadingCanceledException e) {
                logger.warn("processFileUploadRequest(): canceled");
                return HttpServletResponse.SC_OK;
            } catch (ConnectionTimedOutException e) {
                logger.warn("processFileUploadRequest(): timed out");
                return HttpServletResponse.SC_REQUEST_TIMEOUT;
            } catch (UploadingOversizedException e) {
                logger.warn("processFileUploadRequest(): oversized");
                return HttpServletResponse.SC_REQUEST_ENTITY_TOO_LARGE;
            } catch (ConnectionMalformedException e) {
                logger.warn("processFileUploadRequest(): malformed");
                return HttpServletResponse.SC_CONFLICT;
            } catch (UploadingWrongFeaturesException e) {
                logger.warn("processFileUploadRequest(): wrong features: "+e.getSubErrors());
                if (e.getSubErrors() != null) //Ugly hack for getting fine-grain errors through HTTP code; RFC 2616 states that I am *allowed* to do that though
                {
                    if (e.getSubErrors().iterator().hasNext()) {
                        RuntimeException subE = e.getSubErrors().iterator().next();
                        if (subE instanceof TrackBrokenException) {
                            return 450;
                        } else if (subE instanceof TrackBitrateTooLowException) {
                            return 451;
                        } else if (subE instanceof TrackBitrateTooHighException) {
                            return 452;
                        } else if (subE instanceof TrackNotStereoException) {
                            return 453;
                        } else if (subE instanceof TrackSamplerateTooLowException) {
                            return 454;
                        } else if (subE instanceof TrackSamplerateTooHighException) {
                            return 455;
                        } else if (subE instanceof TrackNotCBRException) {
                            return 456;
                        } else if (subE instanceof TrackTooShortException) {
                            return 457;
                        } else if (subE instanceof TrackTooLongException) {
                            return 458;
                        }
                    }
                }
                return HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE;
            } catch (UploadingWrongFormatException e) {
                logger.warn("processFileUploadRequest(): wrong format");
                return HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE;
            } catch (UploadingNoDataException e) {
                logger.warn("processFileUploadRequest(): no data");
                return HttpServletResponse.SC_CONFLICT;
            } catch (SessionClosedException e) {
                logger.warn("processFileDownloadRequest(): session closed");
                return HttpServletResponse.SC_UNAUTHORIZED;
            } catch (AccessDeniedException e) {
                logger.warn("processFileUploadRequest(): access denied");
                return HttpServletResponse.SC_FORBIDDEN;
            } catch (BadRequestException e) {
                logger.warn("processRestQuery(): bad request");
                return HttpServletResponse.SC_BAD_REQUEST;
            } catch (WebserviceOutOfQuotaException e) {
                logger.warn("processRestQuery(): ws out of quota");
                return 429; //Too Many Requests (RFC 6585)
            } catch (Exception e) {
                logger.warn("processFileUploadRequest(): unknown error", e);
                return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
            }
        }
        return HttpServletResponse.SC_METHOD_NOT_ALLOWED;
    }
}
