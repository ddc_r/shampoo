/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.service.archive.ReportFacadeServiceInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ProtectedRestListener implements ProtectedRestListenerInterface {

    private ReportFacadeServiceInterface reportFacadeService;

    public ReportFacadeServiceInterface getReportFacadeService() {
        return reportFacadeService;
    }

    public void setReportFacadeService(ReportFacadeServiceInterface reportFacadeService) {
        this.reportFacadeService = reportFacadeService;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> fetchReport(String reportId, SERIALIZATION_METADATA_FORMAT metadataFormat) throws Exception {
        return getReportFacadeService().fetchReport(reportId, metadataFormat);
    }

}