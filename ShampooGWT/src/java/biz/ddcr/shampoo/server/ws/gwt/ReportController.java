/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ReportController extends ProtectedRestFrontendGetController<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> {

    @Override
    public boolean processRestQuery(HttpServletRequest request, final HttpServletResponse response) throws Exception {

        //Nope, values are not identifiers, keys only. Why? maybe because it's shorter and there will ever only be one GET parameter anyway
        //UPDATE: now use proper "i" parameter        
        //String fileId = (String) request.getParameterNames().nextElement();
        String fileId = ParameterMapping.ITEM_ID.find(request);
        SERIALIZATION_METADATA_FORMAT metadataFormat = SERIALIZATION_METADATA_FORMAT.mapFromFriendlyName(ParameterMapping.METADATA_FORMAT.find(request));
        String asAttachmentString = ParameterMapping.AS_ATTACHMENT.find(request);
        boolean asAttachment = (asAttachmentString != null) ? Boolean.parseBoolean(asAttachmentString) : true;

        GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> metadataStream = getProtectedRestListener().fetchReport(fileId, metadataFormat);

        if (metadataStream == null || metadataStream.getItem() == null) {
            throw new BadRequestException();
        }
        if (asAttachment) {
            getMarshaller().marshallAsAttachment(
                    metadataStream,
                    request,
                    response);
        } else {
            getMarshaller().marshall(
                    metadataStream,
                    request,
                    response);
        }
        return true;
    }
}
