/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.ConnectionException;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import biz.ddcr.shampoo.server.domain.track.format.AudioFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.AudioFormat.AUDIO_FORMAT;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.Tag;
import biz.ddcr.shampoo.server.io.helper.TrackConfigurationHelper;
import biz.ddcr.shampoo.server.io.util.TrackStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.io.parser.FileHeaderParser;

/**
 *
 * @author okay_awright
 **/
public class UploadAudioTrackHandler extends UploadFileHandler<AUDIO_FORMAT> {

    private transient TrackConfigurationHelper trackConfigurationHelper;
    private transient CoverArtConfigurationHelper coverArtConfigurationHelper;
    private SystemConfigurationHelper systemConfigurationHelper;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public TrackConfigurationHelper getTrackConfigurationHelper() {
        return trackConfigurationHelper;
    }

    public void setTrackConfigurationHelper(TrackConfigurationHelper trackConfigurationHelper) {
        this.trackConfigurationHelper = trackConfigurationHelper;
    }

    public CoverArtConfigurationHelper getCoverArtConfigurationHelper() {
        return coverArtConfigurationHelper;
    }

    public void setCoverArtConfigurationHelper(CoverArtConfigurationHelper coverArtConfigurationHelper) {
        this.coverArtConfigurationHelper = coverArtConfigurationHelper;
    }

    @Override
    public boolean beforeAccess() throws Exception {
        //We cannot ensure that the correct rights are enforced (too little available info to process) so just stick to the minimum for upload, i.e. authenticated user or not
        if (getLoginFacadeService().getCurrentlyAuthenticatedUserLogin() == null) {
            throw new AccessDeniedException();
        }
        return true;
    }

    @Override
    public AUDIO_FORMAT onCheckFile(UntypedStreamInterface stream, String originalFilename) throws Exception {
        //Don't trust the MIME types provided by the browser: they're usually inconsistent an can be be easily forged

        //Extensions alone are nothing short than useless except if they can help filtering out false positives that will be more thoroughly examined later on
        //predefined MIME types are automatically filled in if extensions appear to be valid
        AUDIO_FORMAT guessedFormat = getTrackConfigurationHelper().filterOutUnwantedExtensions(originalFilename);
        if (guessedFormat != null) {

            //more through checks are performed elsewhere

            return guessedFormat;
        }

        throw new UploadingWrongFormatException();

        //No need to return anything

    }

    @Override
    public String getUploadTempPath() {
        return getSystemConfigurationHelper().getTempPath();
    }

    @Override
    public long getUploadMaxSize() {
        return getTrackConfigurationHelper().getMaxUploadSize();
    }

    @Override
    public TypedStreamInterface postProcessFile(FILE_FORMAT format, UntypedStreamInterface stream) throws Exception {
        AUDIO_FORMAT guessedFormat = (AUDIO_FORMAT) format;
        //Now sniff the header for fingerprints, and even if it's oh-so slow

        AudioFileInfo fileHeader = null;
        try {
            fileHeader = FileHeaderParser.getHeader(guessedFormat, stream);
        } catch (Exception e) {
            throw new ConnectionException(e.getClass().getCanonicalName());
        }

        if (fileHeader == null || !getTrackConfigurationHelper().isAcceptFormat(fileHeader.getFormat())) {
            throw new UploadingWrongFormatException();
        }

        //Check if the appropriate restrictions regarding the allowed features are fullfilled
        if (getTrackConfigurationHelper().checkFeatures(fileHeader)) {

            //While we're at it, fetch the embedded tags too
            Tag fileTag = null;
            try {
                fileTag = FileHeaderParser.getTag(getCoverArtConfigurationHelper(), guessedFormat, stream);
            } catch (Exception e) {
                //Drop silently
            }

            return new TrackStream(fileTag, fileHeader, stream);

        }
        return null;
    }
}
