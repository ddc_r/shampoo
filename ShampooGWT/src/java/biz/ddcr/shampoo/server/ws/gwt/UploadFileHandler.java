/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.client.helper.errors.ConnectionException;
import biz.ddcr.shampoo.client.helper.errors.DownloadingNotFoundException;
import biz.ddcr.shampoo.client.helper.errors.UploadingNoDataException;
import biz.ddcr.shampoo.client.helper.errors.UploadingOversizedException;
import biz.ddcr.shampoo.server.helper.NumberUtil;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedLocalFileStream;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.service.GenericService;
import biz.ddcr.shampoo.server.service.security.LoginFacadeServiceInterface;
import biz.ddcr.shampoo.server.service.track.TrackFacadeServiceInterface;
import java.io.File;
import java.io.FileNotFoundException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import biz.ddcr.shampoo.server.helper.ServletUtils2;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.ExceededSizeException;
import com.oreilly.servlet.multipart.FileRenamePolicy;
import java.io.IOException;
import java.util.Enumeration;

/**
 *
 * @author okay_awright
 *
 */
public abstract class UploadFileHandler<T extends FILE_FORMAT> extends GenericService implements UploadFileHandlerInterface {

    private static final String UPLOAD_POOL_COOKIE_ID = "SHAMPOO.UPLOAD.ID";
    private LoginFacadeServiceInterface loginFacadeService;
    private TrackFacadeServiceInterface trackFacadeService;

    public LoginFacadeServiceInterface getLoginFacadeService() {
        return loginFacadeService;
    }

    public void setLoginFacadeService(LoginFacadeServiceInterface loginFacadeService) {
        this.loginFacadeService = loginFacadeService;
    }

    public TrackFacadeServiceInterface getTrackFacadeService() {
        return trackFacadeService;
    }

    public void setTrackFacadeService(TrackFacadeServiceInterface trackFacadeService) {
        this.trackFacadeService = trackFacadeService;
    }

    public void postProcessFile(UntypedStreamInterface file) throws Exception {
        //Do nothing; override it if you wish to resize a picture, access its metadata, etc
    }

    @Override
    public boolean processFileUploadRequest(HttpServletRequest controllerRequest) throws Exception {
        boolean result = false;

        if (beforeAccess()) {

            upload(extractUploadIdFromCookie(controllerRequest), controllerRequest);
            result = true;
        }
        return result;
    }

    private void resetUploadIdFromCookie() {
        if (ServletUtils2.getRequest() != null && ServletUtils2.getResponse() != null) {
            Cookie cookie = new Cookie(UPLOAD_POOL_COOKIE_ID, "");
            cookie.setMaxAge(0);
            //TODO: setDomain() too
            //TODO: adapt and customize setPath()
            cookie.setPath(ServletUtils2.getRequest().getContextPath());
            //Add cookies to the response
            ServletUtils2.getResponse().addCookie(cookie);
        }
    }

    private String getUploadIdFromCookie() {
        String currentUploadId = null;
        if (ServletUtils2.getRequest() != null) {
            //Browse cookies and gather the required id
            Cookie[] cookies = ServletUtils2.getRequest().getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    String cookieName = cookie.getName();
                    if (cookieName.equals(UPLOAD_POOL_COOKIE_ID)) {
                        currentUploadId = cookie.getValue();
                        break;
                    }
                }
            }
        }
        return currentUploadId;
    }

    protected String extractUploadIdFromCookie(final HttpServletRequest request) {
        //Try getting it from a cookie
        String currentUploadId = getUploadIdFromCookie();
        if (currentUploadId != null) {
            resetUploadIdFromCookie();
        }
        return currentUploadId;
    }

    protected void upload(final String uploadId, HttpServletRequest request) throws Exception {

        FileRenamePolicy namePolicy = new FileRenamePolicy() {
            @Override
            public File rename(String fileId, File file) {
                return new File(getUploadTempPath(), uploadId != null ? uploadId : fileId);
            }
        };

        // Create a new file upload handler
        MultipartRequest parser;
        try {
            //Limitation: post sizes cannot be larger than an Integer, even if Long dimensions are available
            int maxPOSTSize = getUploadMaxSize() > 0 && getUploadMaxSize() < Integer.MAX_VALUE ? NumberUtil.safeLongToInt(getUploadMaxSize()) : Integer.MAX_VALUE;
            parser = new MultipartRequest(//
                    request,//
                    getUploadTempPath(),//
                    maxPOSTSize,//
                    request.getCharacterEncoding(),//
                    namePolicy//
            );
        } catch (ExceededSizeException e) {
            logger.warn("Uploading oversized file", e);
            throw new UploadingOversizedException();
        } catch (IOException e) {
            throw new ConnectionException(e.getClass().getCanonicalName());
        }
        if (parser != null) {
            final Enumeration fileIds = parser.getFileNames();
            if (fileIds != null) {
                for (; fileIds.hasMoreElements();) {
                    final String fileId = (String) fileIds.nextElement();
                    try {
                        //Wrap the FileItem around an UntypedLocalFileStream thanks to its exposed File
                        UntypedStreamInterface streamItem = new UntypedLocalFileStream(uploadId != null ? uploadId : fileId, parser.getFile(fileId));
                        FILE_FORMAT guessedFormat = onCheckFile(streamItem, parser.getOriginalFileName(fileId));
                        //if guessedFormat is null then there's no reason to carry on
                        if (guessedFormat != null) {
                            try {
                                TypedStreamInterface typedFileItem = postProcessFile(guessedFormat, streamItem);
                                if (typedFileItem != null && getTrackFacadeService().setSecuredStreamToSessionPool(uploadId != null ? uploadId : fileId, typedFileItem)) {
                                    break;
                                } else {
                                    logger.warn("Cannot store uploaded file");
                                    //Remove any temp file associated with this bogus entry
                                    if (streamItem != null) {
                                        streamItem.clean();
                                    }
                                }
                            } catch (Exception e) {
                                //Remove any temp file associated with this bogus entry
                                if (streamItem != null) {
                                    streamItem.clean();
                                }
                                throw e;
                            }
                        } else {
                            logger.warn("Unexpected uploaded file format");
                            //Remove any temp file associated with this bogus entry
                            if (streamItem != null) {
                                streamItem.clean();
                            }
                        }
                    } catch (FileNotFoundException e) {
                        //Clean up, if necessary
                        if (fileId != null) {
                            try {
                                parser.getFile(fileId).delete();
                            } catch (Exception e1) {
                                //Drop silently
                                logger.info("cannot clean up uploaded file " + fileId);
                            }
                        }
                        throw new DownloadingNotFoundException();
                    } catch (Exception e) {
                        if (fileId != null) {
                            try {
                                parser.getFile(fileId).delete();
                            } catch (Exception e1) {
                                //Drop silently
                                logger.info("cannot clean up uploaded file " + fileId);
                            }
                        }
                        throw e;
                    }
                }
            } else {
                throw new UploadingNoDataException();
            }
        }

    }
}
