/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.server.helper.StaticSessionPoolInterface;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright
 **/
public interface UploadFileHandlerInterface<T extends FILE_FORMAT> extends StaticSessionPoolInterface {

    //HTTP Post handling mechanisms
    public boolean processFileUploadRequest(HttpServletRequest request) throws Exception;
    public boolean beforeAccess() throws Exception;
    /**
     * Returns null if the file type cannot be guessed for any other reason the procss must not continue
     * @param stream
     * @param originalFilename
     * @return
     * @throws Exception
     **/
    public T onCheckFile(UntypedStreamInterface stream, String originalFilename) throws Exception;
    /**
     * returns null if the file cannot be processed, for any valid reason
     * @param format
     * @param stream
     * @return
     * @throws Exception
     **/
    public TypedStreamInterface postProcessFile(T format, UntypedStreamInterface stream) throws Exception;

    public String getUploadTempPath();
    public long getUploadMaxSize();

}
