/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

package biz.ddcr.shampoo.server.ws.gwt;

import biz.ddcr.shampoo.server.domain.track.format.FILE_FORMAT;
import biz.ddcr.shampoo.client.helper.errors.AccessDeniedException;
import biz.ddcr.shampoo.client.helper.errors.UploadingWrongFormatException;
import biz.ddcr.shampoo.server.domain.track.format.PictureFileInfo;
import biz.ddcr.shampoo.server.domain.track.format.PictureFormat.PICTURE_FORMAT;
import biz.ddcr.shampoo.server.helper.OpendesktopMimeDetector2;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.CoverArtConfigurationHelper;
import biz.ddcr.shampoo.server.io.helper.MIME;
import biz.ddcr.shampoo.server.io.helper.MIME.CATEGORY;
import biz.ddcr.shampoo.server.io.helper.MIMEHelper;
import biz.ddcr.shampoo.server.io.util.PictureStream;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.io.util.UntypedStreamInterface;
import biz.ddcr.shampoo.server.io.parser.TagFormatter;
import eu.medsea.mimeutil.detector.MimeDetector;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 *
 * @author okay_awright
 **/
public class UploadPictureHandler extends UploadFileHandler<PICTURE_FORMAT> {

    private SystemConfigurationHelper systemConfigurationHelper;
    private transient CoverArtConfigurationHelper coverArtConfigurationHelper;
    private static MimeDetector mimeChecker;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }

    public CoverArtConfigurationHelper getCoverArtConfigurationHelper() {
        return coverArtConfigurationHelper;
    }

    public void setCoverArtConfigurationHelper(CoverArtConfigurationHelper coverArtConfigurationHelper) {
        this.coverArtConfigurationHelper = coverArtConfigurationHelper;
    }

    public static MimeDetector getMimeChecker() {
        if (mimeChecker == null) {
            mimeChecker = new OpendesktopMimeDetector2();
        }
        return mimeChecker;
    }

    @Override
    public boolean beforeAccess() throws Exception {
        //We cannot ensure that the correct rights are enforced (too little available info to process) so just stick to the minimum for upload, i.e. authenticated user or not
        if (getLoginFacadeService().getCurrentlyAuthenticatedUserLogin() == null) {
            throw new AccessDeniedException();
        }
        return true;
    }

    @Override
    public PICTURE_FORMAT onCheckFile(UntypedStreamInterface stream, String originalFilename) throws Exception {
        //Don't trust the MIME types provided by the browser: they're usually inconsistent an can be be easily forged

        //Extensions alone are nothing short than useless except if they can help filtering out false positives that will be more thoroughly examined later on
        //predefined MIME types are automatically filled in if extensions appear to be valid
        PICTURE_FORMAT guessedFormat = getCoverArtConfigurationHelper().filterOutUnwantedExtensions(originalFilename);
        if (guessedFormat != null) {
            //Now sniff the header for fingerprints, and even if it's oh so slow
            //WARNING: you must wrap the fileitem inputstream within a bufferedinputstream so as to enforce the fact mark() is supported! It defeats some of the NIO features of some UntypedStreamInterface components but only slightly
            Collection<String> mimeTypes;
            InputStream inputStream = null;
            try {
                inputStream = stream.getInputStream().getBufferedBackend();
                mimeTypes = getMimeChecker().getMimeTypes(
                        inputStream);
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
            }

            if (mimeTypes != null) {
                //Beware: no generics are used but looking at the source code it outputs Strings
                for (String mimeString : mimeTypes) {
                    MIME mime = new MIME(CATEGORY.coverArt, mimeString);

                    //Use IANA (http://www.iana.org/assignments/media-types) assignments for regular file types
                    PICTURE_FORMAT finalFormat = checkMIMEType(mime);
                    if (finalFormat != null) {
                        return finalFormat;
                    }
                }

            }
        }

        throw new UploadingWrongFormatException();

        //No need to return anything

    }

    private PICTURE_FORMAT checkMIMEType(MIME mime) {
        for (PICTURE_FORMAT format : PICTURE_FORMAT.values()) {
            try {
                if (getCoverArtConfigurationHelper().isAcceptFormat(format)) {
                    if (MIMEHelper.getMIMEsForFormat(format).contains(mime)) {
                        return format;
                    }
                }
            } catch (NoSuchElementException e) {
                //Do nothing
            }
        }
        return null;
    }

    /**
     * 
     * Rescale the image if required: quick (well, not so quick) & dirty
     * 
     * @param file
     * @throws Exception
     **/
    @Override
    public TypedStreamInterface postProcessFile(FILE_FORMAT format, UntypedStreamInterface stream) throws Exception {

        //Resize the picture if applicable
        try {
            if (getCoverArtConfigurationHelper().mustResizeIfApplicable()) {
                PICTURE_FORMAT newFormat = TagFormatter.rescalePicture(getCoverArtConfigurationHelper(), stream);
                if (newFormat!=null) format = newFormat;
            }
        } catch (IOException e) {
            throw new UploadingWrongFormatException();
        }

        //Then wrap the stream to be stored
        PictureFileInfo container = new PictureFileInfo();
        container.setFormat((PICTURE_FORMAT) format);
        container.setSize(stream.getSize());
        return new PictureStream(container, stream);

    }

    @Override
    public String getUploadTempPath() {
        return getSystemConfigurationHelper().getTempPath();
    }

    @Override
    public long getUploadMaxSize() {
        return getCoverArtConfigurationHelper().getMaxUploadSize();
    }
}
