/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.ws.module;

import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.io.frontend.DownloadFileHandler;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.module.PublicFacadeServiceInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright
 **/
public abstract class DownloadMediaHandler extends DownloadFileHandler {

    private PublicFacadeServiceInterface publicFacadeService;    

    /** Bad usage of global vars in bean, but hey it's so easy **/
    protected static final ThreadLocal<String> apiKey = new ThreadLocal<String>();
    protected static final ThreadLocal<String> hmac = new ThreadLocal<String>();
    protected static final ThreadLocal<Long> timestamp = new ThreadLocal<Long>();
    protected static final ThreadLocal<String> remoteIP = new ThreadLocal<String>();

    public PublicFacadeServiceInterface getPublicFacadeService() {
        return publicFacadeService;
    }

    public void setPublicFacadeService(PublicFacadeServiceInterface publicFacadeService) {
        this.publicFacadeService = publicFacadeService;
    }

    @Override
    public boolean onCheckFile(TypedStreamInterface file) throws Exception {
        //Do nothing
        return true;
    }

    @Override
    public void preProcessFile(TypedStreamInterface file) throws Exception {
        //Do nothing
    }

    @Override
    public boolean beforeAccess(String fileId, String remoteIP, HttpServletRequest request) throws Exception {
        //Don't go any further if no identifier has been specified
        if (fileId==null || fileId.length()==0)
            throw new BadRequestException();

        apiKey.set(ParameterMapping.API_KEY.find(request));
        if (apiKey.get() == null || apiKey.get().length() == 0) {
            throw new BadRequestException();
        }        
        hmac.set(ParameterMapping.API_HMAC.find(request));
        if (hmac.get() == null) {
            throw new BadRequestException();
        }
        try {            
            timestamp.set(ParameterMapping.convertIntoMillisecondTimestamp(ParameterMapping.TIMESTAMP.find(request)));
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        } catch (NullPointerException e) {
            throw new BadRequestException();
        }        
        if (timestamp.get() == null) {
            throw new BadRequestException();
        }
        DownloadMediaHandler.remoteIP.set(remoteIP);    
        
        //Actual credential checks are performed within getStream()
        return true;
    }

}
