/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.module;

import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.helper.ServletUtils2;
import biz.ddcr.shampoo.server.helper.SystemConfigurationHelper;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class HistoryController extends PublicRestFrontendGetController<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> {

    private SystemConfigurationHelper systemConfigurationHelper;

    public SystemConfigurationHelper getSystemConfigurationHelper() {
        return systemConfigurationHelper;
    }

    public void setSystemConfigurationHelper(SystemConfigurationHelper systemConfigurationHelper) {
        this.systemConfigurationHelper = systemConfigurationHelper;
    }
    
    @Override
    public boolean processRestQuery(HttpServletRequest request, final HttpServletResponse response) throws Exception {
        final String apiKey = ParameterMapping.API_KEY.find(request);
        final String hmac = ParameterMapping.API_HMAC.find(request);
        String timestamp = ParameterMapping.TIMESTAMP.find(request);
        final String itemNumber = ParameterMapping.MAXIMUM_ITEM_NUMBER.find(request);

        Integer itemNumberInt = itemNumber != null ? Integer.decode(itemNumber) : null;

        Long time = null;
        try {
            time = ParameterMapping.convertIntoMillisecondTimestamp(timestamp);
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        }
        SERIALIZATION_METADATA_FORMAT metadataFormat = SERIALIZATION_METADATA_FORMAT.mapFromFriendlyName(ParameterMapping.METADATA_FORMAT.find(request));

        if (apiKey == null || apiKey.length() == 0) {
            throw new BadRequestException();
        }

        if (hmac == null || time == null) {
            throw new BadRequestException();
        }

        final String remoteIP = ServletUtils2.getRemoteAddr(request, getSystemConfigurationHelper().isHttpProxiedIPCheckingEnabled());

        getMarshaller().marshall(
                getPublicRestListener().fetchHistory(remoteIP, apiKey, hmac, time, metadataFormat, itemNumberInt),
                request,
                response);

        //TODO Intercept business exceptions such as authentication problems, etc
        return true;
    }
}
