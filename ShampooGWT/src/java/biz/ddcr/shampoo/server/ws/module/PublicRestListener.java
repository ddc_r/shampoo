/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.ws.module;

import biz.ddcr.shampoo.server.domain.archive.metadata.StreamableArchiveMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.MinimalStreamableItemMetadataInterface;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.service.module.PublicFacadeServiceInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class PublicRestListener implements PublicRestListenerInterface {

    private PublicFacadeServiceInterface publicFacadeService;

    public PublicFacadeServiceInterface getPublicFacadeService() {
        return publicFacadeService;
    }

    public void setPublicFacadeService(PublicFacadeServiceInterface publicFacadeService) {
        this.publicFacadeService = publicFacadeService;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<MinimalStreamableItemMetadataInterface> fetchNowPlaying(String remoteIP, String apiKey, String hmac, long timestamp, SERIALIZATION_METADATA_FORMAT metadataFormat) throws Exception {
        return getPublicFacadeService().fetchNowPlaying(remoteIP, apiKey, hmac, timestamp, metadataFormat);
    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<MinimalStreamableItemMetadataInterface>> fetchComingNext(String remoteIP, String apiKey, String hmac, long timestamp, SERIALIZATION_METADATA_FORMAT metadataFormat, Integer maxNumberOfItems) throws Exception {
        return getPublicFacadeService().fetchComingNext(remoteIP, apiKey, hmac, timestamp, metadataFormat, maxNumberOfItems);
    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableArchiveMetadataInterface>> fetchHistory(String remoteIP, String apiKey, String hmac, long timestamp, SERIALIZATION_METADATA_FORMAT metadataFormat, Integer maxNumberOfItems) throws Exception {
        return getPublicFacadeService().fetchHistory(remoteIP, apiKey, hmac, timestamp, metadataFormat, maxNumberOfItems);
    }

}