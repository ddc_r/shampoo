/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.domain.streamer.metadata.StreamableChannelMetadataInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class ChannelMetadataController extends PrivateRestFrontendGetController<StreamableChannelMetadataInterface> {

    private DynamicStreamerConfigurationHelper dynamicStreamerConfigurationHelper;

    public DynamicStreamerConfigurationHelper getDynamicStreamerConfigurationHelper() {
        return dynamicStreamerConfigurationHelper;
    }

    public void setDynamicStreamerConfigurationHelper(DynamicStreamerConfigurationHelper dynamicStreamerConfigurationHelper) {
        this.dynamicStreamerConfigurationHelper = dynamicStreamerConfigurationHelper;
    }

    @Override
    public boolean processRestQuery(HttpServletRequest request, HttpServletResponse response) throws Exception {
        final String channelId = ParameterMapping.CHANNEL.find(request);
        final String seatID = ParameterMapping.SEAT_NUMBER.find(request);
        final String privateKey = ParameterMapping.MASTER_KEY.find(request);
        final String protectionKey = ParameterMapping.PROTECTION_KEY.find(request);

        if (seatID == null || channelId == null) {
            throw new BadRequestException();
        }

        //SeatNumber to long
        long seatNumber = 0;
        try {
            seatNumber = Long.decode(seatID);
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        }

        final DynamicStreamerData configuration = getDynamicStreamerConfigurationHelper().configureFromMetadata(seatNumber, request);

        getMarshaller().marshall(
                getStreamerRestListener().getChannel(seatNumber, privateKey, protectionKey, channelId, configuration),
                request,
                response);

        //TODO Intercept business exceptions such as authentication problems, etc
        return true;

    }
}
