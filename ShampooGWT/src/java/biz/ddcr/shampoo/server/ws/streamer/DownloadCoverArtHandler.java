/*
  *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
  * 
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  * 
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU Affero General Public License for more details.
  * 
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */


package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.server.io.util.PictureStream;

/**
 *
 * @author okay_awright
 **/
public class DownloadCoverArtHandler extends DownloadMediaHandler {
    
    @Override
    public PictureStream getStream(String fileId) throws Exception {
        //fileId must be a Long representing a track Id
        return getStreamerFacadeService().getSecuredCoverArtStreamFromDataStore(seatNumber.get(), privateKey.get(), protectionKey.get(), fileId, configuration.get());
    }

}
