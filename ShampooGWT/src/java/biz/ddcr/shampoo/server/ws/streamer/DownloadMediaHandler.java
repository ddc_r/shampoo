/*
 *  Copyright (C) 2010 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.io.frontend.DownloadFileHandler;
import biz.ddcr.shampoo.server.io.util.TypedStreamInterface;
import biz.ddcr.shampoo.server.service.streamer.StreamerFacadeServiceInterface;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author okay_awright
 *
 */
public abstract class DownloadMediaHandler extends DownloadFileHandler {

    private DynamicStreamerConfigurationHelper dynamicStreamerConfigurationHelper;

    public DynamicStreamerConfigurationHelper getDynamicStreamerConfigurationHelper() {
        return dynamicStreamerConfigurationHelper;
    }

    public void setDynamicStreamerConfigurationHelper(DynamicStreamerConfigurationHelper dynamicStreamerConfigurationHelper) {
        this.dynamicStreamerConfigurationHelper = dynamicStreamerConfigurationHelper;
    }
    private StreamerFacadeServiceInterface streamerFacadeService;
    /**
     * Bad usage of global vars in bean, but hey it's so easy *
     */
    protected static final ThreadLocal<Long> seatNumber = new ThreadLocal<Long>();
    protected static final ThreadLocal<String> privateKey = new ThreadLocal<String>();
    protected static final ThreadLocal<String> protectionKey = new ThreadLocal<String>();
    protected static final ThreadLocal<DynamicStreamerData> configuration = new ThreadLocal<DynamicStreamerData>();

    public StreamerFacadeServiceInterface getStreamerFacadeService() {
        return streamerFacadeService;
    }

    public void setStreamerFacadeService(StreamerFacadeServiceInterface streamerFacadeService) {
        this.streamerFacadeService = streamerFacadeService;
    }

    @Override
    public boolean onCheckFile(TypedStreamInterface file) throws Exception {
        //Do nothing
        return true;
    }

    @Override
    public void preProcessFile(TypedStreamInterface file) throws Exception {
        //Do nothing
    }

    @Override
    public boolean beforeAccess(String remoteIP, String fileId, HttpServletRequest request) throws Exception {
        //Don't go any further if no identifier has been specified
        if (fileId == null || fileId.length() == 0) {
            throw new BadRequestException();
        }

        try {
            seatNumber.set(Long.valueOf(ParameterMapping.SEAT_NUMBER.find(request)));
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        } catch (NullPointerException e) {
            throw new BadRequestException();
        }  
        if (seatNumber.get() == null) {
            throw new BadRequestException();
        }

        privateKey.set(ParameterMapping.MASTER_KEY.find(request));
        
        protectionKey.set(ParameterMapping.PROTECTION_KEY.find(request));
        
        configuration.set(getDynamicStreamerConfigurationHelper().configureFromMetadata(seatNumber.get(), request));

        //Remote IP is not used since it's already been filtered out by a ServletFilter upstream
        //Actual credential checks are performed within getStream()
        return true;
    }
}
