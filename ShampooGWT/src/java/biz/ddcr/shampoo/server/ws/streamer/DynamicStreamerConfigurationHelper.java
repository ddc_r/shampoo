/*
 *  Copyright (C) 2012 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.io.serializer.SERIALIZATION_METADATA_FORMAT;
import biz.ddcr.shampoo.server.service.streamer.ChannelStreamerConnectionStatusHelper;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * Gathers and interprets all parameters set from the private webservice client into internal resources
 * configureFromMetadata must be set as a mandatory entry point for deciphering all requests before any business jobs
 * 
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class DynamicStreamerConfigurationHelper {

    /**
     * Try to dynamically configure the streamer using parameters sent from the
     * request
     *
     * @param request
     * @return
     */
    public DynamicStreamerData configureFromMetadata(final String channelId, final HttpServletRequest request) {
        return configureFromMetadata(ChannelStreamerConnectionStatusHelper.getSeatNumber(channelId), request);
    }
    public DynamicStreamerData configureFromMetadata(final Long seatNumber, final HttpServletRequest request) {
        DynamicStreamerData result = null;
        if (request != null && seatNumber != null) {
            //Do only return an object if at least one attribut must be edited
            return _configure(
                    seatNumber,
                    SERIALIZATION_METADATA_FORMAT.mapFromFriendlyName(//
                        ParameterMapping.METADATA_FORMAT.find(request)),//
                        ParameterMapping.QUEUE_MIN_ITEMS.find(request),//
                        ParameterMapping.MIN_PLAYABLE_DURATION.find(request),//
                        ParameterMapping.QUEUEABLE_LIVE.find(request),//
                        ParameterMapping.QUEUEABLE_LIVE_CHUNK_SIZE.find(request),//
                        ParameterMapping.QUEUEABLE_LIVE_CHUNK_SIZE_S.find(request),//
                        ParameterMapping.QUEUEABLE_LIVE_URI.find(request),//
                        ParameterMapping.QUEUEABLE_BLANK.find(request),//
                        ParameterMapping.QUEUEABLE_BLANK_CHUNK_SIZE.find(request),//
                        ParameterMapping.QUEUEABLE_BLANK_CHUNK_SIZE_S.find(request),//
                        ParameterMapping.QUEUEABLE_BLANK_URI.find(request),//
                        ParameterMapping.STREAM_URI.find(request),//
                        ParameterMapping.STREAMER_UA.find(request),//
                        ParameterMapping.TTL.find(request)//
                    );
        }
        return result;
    }

    public DynamicStreamerData configureFromMetadata(final String channelId, final Map request) {
        return configureFromMetadata(ChannelStreamerConnectionStatusHelper.getSeatNumber(channelId), request);
    }
    public DynamicStreamerData configureFromMetadata(final Long seatNumber, final Map request) {
        DynamicStreamerData result = null;
        if (request != null && seatNumber != null) {
            String[] f = (String[]) request.get(ParameterMapping.METADATA_FORMAT.getQueryParameter());
            String[] mq = (String[]) request.get(ParameterMapping.QUEUE_MIN_ITEMS.getQueryParameter());
            String[] md = (String[]) request.get(ParameterMapping.MIN_PLAYABLE_DURATION.getQueryParameter());
            String[] ql = (String[]) request.get(ParameterMapping.QUEUEABLE_LIVE.getQueryParameter());
            String[] qlcs = (String[]) request.get(ParameterMapping.QUEUEABLE_LIVE_CHUNK_SIZE.getQueryParameter());
            String[] qlcsf = (String[]) request.get(ParameterMapping.QUEUEABLE_LIVE_CHUNK_SIZE_S.getQueryParameter());
            String[] qlu = (String[]) request.get(ParameterMapping.QUEUEABLE_LIVE_URI.getQueryParameter());
            String[] qb = (String[]) request.get(ParameterMapping.QUEUEABLE_BLANK.getQueryParameter());
            String[] qbcs = (String[]) request.get(ParameterMapping.QUEUEABLE_BLANK_CHUNK_SIZE.getQueryParameter());
            String[] qbcsf = (String[]) request.get(ParameterMapping.QUEUEABLE_BLANK_CHUNK_SIZE_S.getQueryParameter());
            String[] qbu = (String[]) request.get(ParameterMapping.QUEUEABLE_BLANK_URI.getQueryParameter());
            String[] su = (String[]) request.get(ParameterMapping.STREAM_URI.getQueryParameter());
            String[] si = (String[]) request.get(ParameterMapping.STREAMER_UA.getQueryParameter());
            String[] l = (String[]) request.get(ParameterMapping.TTL.getQueryParameter());
            //Do only return an object if at least one attribut must be edited
            return _configure(
                    seatNumber,
                    SERIALIZATION_METADATA_FORMAT.mapFromFriendlyName(//
                        f != null && f.length > 0 ? f[0] : null),//
                    mq != null && mq.length > 0 ? mq[0] : null,//
                    md != null && md.length > 0 ? md[0] : null,//
                    ql != null && ql.length > 0 ? ql[0] : null,//
                    qlcs != null && qlcs.length > 0 ? qlcs[0] : null,//
                    qlcsf != null && qlcsf.length > 0 ? qlcsf[0] : null,//
                    qlu != null && qlu.length > 0 ? qlu[0] : null,//
                    qb != null && qb.length > 0 ? qb[0] : null,//
                    qbcs != null && qbcs.length > 0 ? qbcs[0] : null,//
                    qbcsf != null && qbcsf.length > 0 ? qbcsf[0] : null,//
                    qbu != null && qbu.length > 0 ? qbu[0] : null,//
                    su != null && su.length > 0 ? su[0] : null,//
                    si != null && si.length > 0 ? si[0] : null,//
                    l != null && l.length > 0 ? l[0] : null//
                    );
        }
        return result;
    }

    public DynamicStreamerData _configure(
            final long seatNumber,
            final SERIALIZATION_METADATA_FORMAT metadataFormat,
            final String queueMinItemsString,
            final String minPlayableDurationString,
            final String queueableLiveString,
            final String queueableLiveChunkSizeString,
            final String queueableLiveChunkSizeSecondString,
            final String queueableLiveURIString,
            final String queueableBlankString,
            final String queueableBlankChunkSizeString,
            final String queueableBlankChunkSizeSecondString,
            final String queueableBlankURIString,
            final String streamURI,
            final String streamerID,
            final String ttlString) {
        DynamicStreamerData result = null;
        //Do only return an object if at least one attribut must be edited
        if (metadataFormat != null
                || queueMinItemsString != null
                || minPlayableDurationString != null
                || queueableLiveString != null
                || queueableLiveChunkSizeString != null
                || queueableLiveChunkSizeSecondString != null
                || queueableLiveURIString != null
                || queueableBlankString != null
                || queueableBlankChunkSizeString != null
                || queueableBlankChunkSizeSecondString != null
                || queueableBlankURIString != null
                || streamURI != null
                || streamerID != null
                || ttlString != null) {

            result = ChannelStreamerConnectionStatusHelper.getStreamerData(seatNumber);

            //Checking whether the connection is still alive or has already been open
            if (result==null) {
                result = new DynamicStreamerData();                
            }
            
            if (metadataFormat != null) {
                result.setMetadataFormat(metadataFormat);
            }

            try {
                if (queueMinItemsString != null) {
                    result.setQueueMinItems(Integer.decode(queueMinItemsString));
                }
            } catch (NumberFormatException e) {
                //Silently fails
            }

            try {
                if (minPlayableDurationString != null) {
                    result.setMinPlayableDuration(Long.decode(minPlayableDurationString));
                }
            } catch (NumberFormatException e) {
                //Silently fails
            }

            if (queueableLiveString != null) {
                result.setQueueableLive(Boolean.parseBoolean(queueableLiveString));
            }

            try {
                if (queueableLiveChunkSizeString != null) {
                    result.setQueueableLiveChunkSize(Long.decode(queueableLiveChunkSizeString));
                } else if (queueableLiveChunkSizeSecondString != null) {
                    result.setQueueableLiveChunkSize((long) (Double.parseDouble(queueableLiveChunkSizeSecondString) * 1000.));
                }
            } catch (NumberFormatException e) {
                //Silently fails
            }

            if (queueableLiveURIString != null) {
                result.setQueueableLiveURI(queueableLiveURIString);
            }
            
            if (queueableBlankString != null) {
                result.setQueueableBlank(Boolean.parseBoolean(queueableBlankString));
            }

            try {
                if (queueableBlankChunkSizeString != null) {
                    result.setQueueableBlankChunkSize(Long.decode(queueableBlankChunkSizeString));
                } else if (queueableBlankChunkSizeSecondString != null) {
                    result.setQueueableBlankChunkSize((long) (Double.parseDouble(queueableBlankChunkSizeSecondString) * 1000.));
                }
            } catch (NumberFormatException e) {
                //Silently fails
            }

            if (queueableBlankURIString != null) {
                result.setQueueableBlankURI(queueableBlankURIString);
            }
            
            if (streamURI != null) {
                result.setStreamURI(streamURI);
            }
            
            if (streamerID != null) {
                result.setUserAgentID(streamerID);
            }
            
            try {
                if (ttlString != null) {
                    result.setTtl(Long.decode(ttlString));
                }
            } catch (NumberFormatException e) {
                //Silently fails
            }

        }
        return result;
    }
}
