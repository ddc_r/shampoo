/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.client.helper.errors.BadRequestException;
import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.service.helper.DateHelper;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPileManager;
import biz.ddcr.shampoo.server.service.streamer.pile.StreamerEventPilePool.NullRunnableStreamerEvent;
import biz.ddcr.shampoo.server.ws.ParameterMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class LiveOffController extends PrivateRestFrontendPostController {

    private DynamicStreamerConfigurationHelper dynamicStreamerConfigurationHelper;

    public DynamicStreamerConfigurationHelper getDynamicStreamerConfigurationHelper() {
        return dynamicStreamerConfigurationHelper;
    }

    public void setDynamicStreamerConfigurationHelper(DynamicStreamerConfigurationHelper dynamicStreamerConfigurationHelper) {
        this.dynamicStreamerConfigurationHelper = dynamicStreamerConfigurationHelper;
    }

    private StreamerEventPileManager streamerEventPileManager;

    public StreamerEventPileManager getStreamerEventPileManager() {
        return streamerEventPileManager;
    }

    public void setStreamerEventPileManager(StreamerEventPileManager streamerEventPileManager) {
        this.streamerEventPileManager = streamerEventPileManager;
    }

    @Override
    public boolean processRestQuery(HttpServletRequest request, HttpServletResponse response) throws Exception {
        final String seatID = ParameterMapping.SEAT_NUMBER.find(request);
        final String privateKey = ParameterMapping.MASTER_KEY.find(request);
        final String protectionKey = ParameterMapping.PROTECTION_KEY.find(request);
        final String timetableSlotId = ParameterMapping.TIMETABLE_SLOT.find(request);
        String timestamp = ParameterMapping.TIMESTAMP.find(request);

        //Queue up this command so that multiple concurrent accesses for this queue cannot happen
        if (seatID == null) {
            throw new BadRequestException();
        }

        //SeatNumber to long
        long seatNumber = 0;
        try {
            seatNumber = Long.decode(seatID);
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        }

        final long _seatNumber = seatNumber;
        final DynamicStreamerData configuration = getDynamicStreamerConfigurationHelper().configureFromMetadata(_seatNumber, request);

        Long time = null;
        try {
            time = ParameterMapping.convertIntoMillisecondTimestamp(timestamp);
        } catch (NumberFormatException e) {
            throw new BadRequestException();
        }
        final Long _time = time != null ? time : DateHelper.getCurrentTime();

        getStreamerEventPileManager().synchronousBlockingEvent(_seatNumber, _time, new NullRunnableStreamerEvent() {

            @Override
            public Void synchronous() throws Exception {
                getStreamerRestListener().setLiveOffAir(_seatNumber, privateKey, protectionKey, timetableSlotId, _time, configuration);
                return null;
            }
        });
        return true;

    }
}
