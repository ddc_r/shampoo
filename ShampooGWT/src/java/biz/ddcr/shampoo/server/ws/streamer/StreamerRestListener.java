/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http:www.gnu.org/licenses/>.
 */
package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.*;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import biz.ddcr.shampoo.server.service.streamer.StreamerFacadeServiceInterface;
import biz.ddcr.shampoo.server.service.streamer.task.AsynchronousQueueRunnableHelperInterface;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public class StreamerRestListener implements StreamerRestListenerInterface {

    /**
     * Do NOT serialize this service, it's an heavyweight component that should
     * not be marshalled *
     */
    /**
     * Update: not an Hessian WS anymore, can scrap the 'transient' keyword*
     */
    private StreamerFacadeServiceInterface streamerFacadeService;
    private AsynchronousQueueRunnableHelperInterface asynchronousQueueRunnableHelper;

    public AsynchronousQueueRunnableHelperInterface getAsynchronousQueueRunnableHelper() {
        return asynchronousQueueRunnableHelper;
    }

    public void setAsynchronousQueueRunnableHelper(AsynchronousQueueRunnableHelperInterface asynchronousQueueRunnableHelper) {
        this.asynchronousQueueRunnableHelper = asynchronousQueueRunnableHelper;
    }

    public StreamerFacadeServiceInterface getStreamerFacadeService() {
        return streamerFacadeService;
    }

    public void setStreamerFacadeService(StreamerFacadeServiceInterface streamerFacadeService) {
        this.streamerFacadeService = streamerFacadeService;
    }

    /**
     * Feed the queue in the background, detached, in an event-based trigerring
     * fashion. It should happen outside a transaction and not introduce any
     * latency
     *
     * @param channelId
     * @throws Exception
     */
    protected void asynchronousPopulateQueue(long seatNumber) throws Exception {
        getAsynchronousQueueRunnableHelper().asynchronousRun(seatNumber);
    }

    @Override
    public void setChannelReserved(long seatNumber, String masterKey, String protectionKey, String channelId, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setChannelReserved(seatNumber, masterKey, protectionKey, channelId, time, configuration);
    }

    @Override
    public void setChannelFree(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setChannelFree(seatNumber, masterKey, protectionKey, time, configuration);
    }
       
    @Override
    public void setChannelOnAir(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setChannelOnAir(seatNumber, masterKey, protectionKey, time, configuration);
    }

    @Override
    public void setChannelOffAir(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setChannelOffAir(seatNumber, masterKey, protectionKey, time, configuration);
    }

    @Override
    public void setHeartbeat(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setHeartbeat(seatNumber, masterKey, protectionKey, time, configuration);
    }

    @Override
    public void setLiveOnAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setLiveOnAir(seatNumber, masterKey, protectionKey, timetableSlotId, time, configuration);
    }

    @Override
    public void setLiveOnAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setLiveOnAir(seatNumber, masterKey, protectionKey, timetableSlotId, null, configuration);
    }

    @Override
    public void setItemOnAir(long seatNumber, String masterKey, String protectionKey, String queueItemId, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setTrackOnAir(seatNumber, masterKey, protectionKey, queueItemId, time, configuration);
        asynchronousPopulateQueue(seatNumber);
    }

    @Override
    public void setItemOnAir(long seatNumber, String masterKey, String protectionKey, String queueItemId, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setTrackOnAir(seatNumber, masterKey, protectionKey, queueItemId, null, configuration);
        asynchronousPopulateQueue(seatNumber);
    }

    @Override
    public void setLiveOffAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setLiveOffAir(seatNumber, masterKey, protectionKey, timetableSlotId, time, configuration);
        asynchronousPopulateQueue(seatNumber);
    }

    @Override
    public void setLiveOffAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setLiveOffAir(seatNumber, masterKey, protectionKey, timetableSlotId, null, configuration);
        asynchronousPopulateQueue(seatNumber);
    }

    @Override
    public void setItemOffAir(long seatNumber, String masterKey, String protectionKey, String trackId, Long time, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setTrackOffAir(seatNumber, masterKey, protectionKey, trackId, time, configuration);
        asynchronousPopulateQueue(seatNumber);
    }

    @Override
    public void setItemOffAir(long seatNumber, String masterKey, String protectionKey, String trackId, DynamicStreamerData configuration) throws Exception {
        getStreamerFacadeService().setTrackOffAir(seatNumber, masterKey, protectionKey, trackId, null, configuration);
        asynchronousPopulateQueue(seatNumber);
    }

    @Override
    public GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> getLiveAt(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception {
        GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> item = getStreamerFacadeService().fetchLiveAt(seatNumber, masterKey, protectionKey, time, configuration);
        return item;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> getCurrentLive(long seatNumber, String masterKey, String protectionKey, DynamicStreamerData configuration) throws Exception {
        GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> item = getStreamerFacadeService().fetchLiveAt(seatNumber, masterKey, protectionKey, null, configuration);
        return item;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<StreamableQueueItemMetadataInterface> popNextItem(long seatNumber, String masterKey, String protectionKey, DynamicStreamerData configuration) throws Exception {
        GenericStreamableMetadataContainerInterface<StreamableQueueItemMetadataInterface> item = getStreamerFacadeService().popNextItem(seatNumber, masterKey, protectionKey, configuration);
        asynchronousPopulateQueue(seatNumber);
        return item;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<StreamableChannelMetadataInterface> getChannel(long seatNumber, String masterKey, String protectionKey, String channelId, DynamicStreamerData configuration) throws Exception {
        GenericStreamableMetadataContainerInterface<StreamableChannelMetadataInterface> item = getStreamerFacadeService().fetchChannel(seatNumber, masterKey, protectionKey, channelId, configuration);
        return item;
    }

    @Override
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableChannelMetadataInterface>> getAvailableChannels(long seatNumber, String masterKey, DynamicStreamerData configuration) throws Exception {
        GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableChannelMetadataInterface>> item = getStreamerFacadeService().fetchAvailableChannels(seatNumber, masterKey, configuration);
        return item;
    }
}
