/*
 *  Copyright (C) 2011 okay_awright <okay_awright AT ddcr DOT biz>
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 * 
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.ddcr.shampoo.server.ws.streamer;

import biz.ddcr.shampoo.server.domain.streamer.DynamicStreamerData;
import biz.ddcr.shampoo.server.io.serializer.GenericStreamableMetadataContainerInterface;
import biz.ddcr.shampoo.server.domain.streamer.metadata.*;
import biz.ddcr.shampoo.server.io.serializer.SerializableIterableMetadataInterface;
import java.io.Serializable;

/**
 *
 * @author okay_awright <okay_awright AT ddcr DOT biz>
 */
public interface StreamerRestListenerInterface extends Serializable {

    /**
     * The streamer has booked the given channel for streaming. Should only be sent out when the streamer is first launched or if it was previously off.
     * @param channelId
     * @param masterKey
     * @throws Exception 
     */
    public void setChannelReserved(long seatNumber, String masterKey, String protectionKey, String channelId, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * The streamer is currently unavailable and cannot answer requests for the given channel.
     * The channel if previously booked is now free.
     * @param channelId
     * @param masterKey
     * @throws Exception 
     */
    public void setChannelFree(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * The channel has started to stream
     * @param seatNumber
     * @param masterKey
     * @param protectionKey
     * @param channelId
     * @param time
     * @param configuration
     * @throws Exception 
     */
    public void setChannelOnAir(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * The channel has stopped streaming
     * @param channelId
     * @param masterKey
     * @throws Exception 
     */
    public void setChannelOffAir(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * The streamer is up and running for the given channel. Should be periodically sent out from the streamer to the backend so as to notify it everything's fine.
     * @param channelId
     * @param masterKey
     * @throws Exception 
     */
    public void setHeartbeat(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;        
    /**
     * A live stream has currently started and is being handled by the streamer for the given channel.
     * Time is a Unix timestamp for this event.
     * @param channelId
     * @param masterKey
     * @param time
     * @throws Exception 
     */
    public void setLiveOnAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * A live stream has currently started and is being handled by the streamer for the given channel.
     * Time is a Unix timestamp that specifies when this event was actually triggered.
     * No timestamp for this event is logged, a default one will be computed as soon as the message is received by the webservice.
     * @param channelId
     * @param masterKey
     * @throws Exception 
     */
    public void setLiveOnAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, DynamicStreamerData configuration) throws Exception;
    /**
     * A live stream has just stopped or is not being handled by the streamer any more for the given channel.
     * No timestamp for this event is logged, a default one will be computed as soon as the message is received by the webservice.
     * @param channelId
     * @param masterKey
     * @param time
     * @throws Exception 
     */
    public void setLiveOffAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * A live stream has just stopped or is not being handled by the streamer any more for the given channel.
     * Time is a Unix timestamp that specifies when this event was actually triggered.
     * @param channelId
     * @param masterKey
     * @param time
     * @throws Exception 
     */    
    public void setLiveOffAir(long seatNumber, String masterKey, String protectionKey, String timetableSlotId, DynamicStreamerData configuration) throws Exception;
    /**
     * Notifies that the specified queued item has started being played by the streamer. Mostly used by Shampoo to update the public webservices with 'currently on-air' info.
     * Time is a Unix timestamp that specifies when this event was actually triggered.
     * @param channelId
     * @param masterKey
     * @param queuedItemId
     * @param time
     * @throws Exception 
     */
    public void setItemOnAir(long seatNumber, String masterKey, String protectionKey, String queuedItemId, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * Notifies that the specified queued item has started being played by the streamer. Mostly used by Shampoo to update the public webservices with 'currently on-air' info.
     * No timestamp for this event is logged, a default one will be computed as soon as the message is received by the webservice. 
     * @param channelId
     * @param masterKey
     * @param queuedItemId
     * @throws Exception 
     */
    public void setItemOnAir(long seatNumber, String masterKey, String protectionKey, String queuedItemId, DynamicStreamerData configuration) throws Exception;
    /**
     * Notifies that the specified queued item that was previously played by the stremaer has just stopped. This track will be taken off from Shampoo's current queue and put into the Archives.
     * Time is a Unix timestamp that specifies when this event was actually triggered.
     * @param channelId
     * @param masterKey
     * @param queuedItemId
     * @param time
     * @throws Exception 
     */
    public void setItemOffAir(long seatNumber, String masterKey, String protectionKey, String queuedItemId, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * Notifies that the specified queued item that was previously played by the stremaer has just stopped. This track will be taken off from Shampoo's current queue and put into the Archives.
     * No timestamp for this event is logged, a default one will be computed as soon as the message is received by the webservice. 
     * @param channelId
     * @param masterKey
     * @param queuedItemId
     * @throws Exception 
     */
    public void setItemOffAir(long seatNumber, String masterKey, String protectionKey, String queuedItemId, DynamicStreamerData configuration) throws Exception;
    /**
     * Asks if a live is schedule for the given channel at the specified time, and returns its metadata.
     * @param channelId
     * @param masterKey
     * @param time
     * @return
     * @throws Exception 
     */
    public GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> getLiveAt(long seatNumber, String masterKey, String protectionKey, Long time, DynamicStreamerData configuration) throws Exception;
    /**
     * Asks if a live should currently be on-air for the given channel and returns its metadata. Mostly used to retrieve the login and password and check if streaming clients are allowed to push this live to Liquidsoap.
     * @param channelId
     * @param masterKey
     * @return
     * @throws Exception 
     */
    public GenericStreamableMetadataContainerInterface<StreamableLiveMetadataInterface> getCurrentLive(long seatNumber, String masterKey, String protectionKey, DynamicStreamerData configuration) throws Exception;
    /**
     * Returns the next item (track) to play for this channel. It will return the top track from Shampoo's internal queue that has not yet been queried by this method.
     * @param channelId
     * @param masterKey
     * @return
     * @throws Exception 
     */
    public GenericStreamableMetadataContainerInterface<StreamableQueueItemMetadataInterface> popNextItem(long seatNumber, String masterKey, String protectionKey, DynamicStreamerData configuration) throws Exception;
    /**
     * Returns misc. data about the queried channel, those data are also echoed in responses from getNextItem() and getCurrentLive(). Useful for feeding ICY metadata as soon as the connection is established.
     * @param channelId
     * @param masterKey
     * @return
     * @throws Exception 
     */
    public GenericStreamableMetadataContainerInterface<StreamableChannelMetadataInterface> getChannel(long seatNumber, String masterKey, String protectionKey, String channelId, DynamicStreamerData configuration) throws Exception;
    /**
     * For a given seat number, displays all channel labels that are not yet handled by a streamer
     * @param seatNumber
     * @return
     * @throws Exception 
     */
    public GenericStreamableMetadataContainerInterface<SerializableIterableMetadataInterface<StreamableChannelMetadataInterface>> getAvailableChannels(long seatNumber, String masterKey, DynamicStreamerData configuration) throws Exception;
}
