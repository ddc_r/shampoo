#!/bin/sh

# Fill in the classpath with every webapplication registered JARs
LOCAL_CLASSPATH=".:./WEB-INF/classes:./WEB-INF/conf:./WEB-INF/scripts/servlet-api-2.5.jar"
for jar in ./WEB-INF/lib/*.jar ; do LOCAL_CLASSPATH="${LOCAL_CLASSPATH}:${jar}" ; done

# Command line parameters are, in order: the Administrator username, the associated password, and the CSV file containing the tracks to import
java -Dlog4j.configuration=./WEB-INF/conf/log.properties -cp "$LOCAL_CLASSPATH" biz.ddcr.shampoo.cmdline.ImportTracks "$1" "$2" "$3"