@ECHO off
REM Fill in the classpath with every webapplication registered JARs
SET LOCAL_CLASSPATH=.;./WEB-INF/classes;./WEB-INF/conf;./WEB-INF/scripts/servlet-api-2.5.jar
FOR /R ".\WEB-INF\lib" %%G IN (*.jar) DO CALL :concat %%G
GOTO :java
:concat
SET "LOCAL_CLASSPATH=%LOCAL_CLASSPATH%;./WEB-INF/lib/%~nx1";
GOTO :eof

:java
REM No command line parameters
java -Dlog4j.configuration=./WEB-INF/conf/log.properties -cp %LOCAL_CLASSPATH% biz.ddcr.shampoo.cmdline.InitializeDatabase

:eof