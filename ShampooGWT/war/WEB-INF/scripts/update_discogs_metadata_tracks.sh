#!/bin/sh

# Fill in the classpath with every webapplication registered JARs
LOCAL_CLASSPATH=".:./WEB-INF/classes:./WEB-INF/conf:./WEB-INF/scripts/servlet-api-2.5.jar:./WEB-INF/scripts/discogs-api.jar"
for jar in ./WEB-INF/lib/*.jar ; do LOCAL_CLASSPATH="${LOCAL_CLASSPATH}:${jar}" ; done

# Command line parameters are, in order: the Administrator username, the associated password, and the gzipped Discogs release XML dump
java -Dlog4j.configuration=./WEB-INF/conf/log.properties -cp "$LOCAL_CLASSPATH" biz.ddcr.shampoo.cmdline.UpdateDiscogsTracks "$1" "$2" "$3" "$4"